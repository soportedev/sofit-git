﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace soporte
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {            
            routes.MapRoute(name: "Login",
                url: "Login",
                defaults: new { controller = "Login", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
                );
            routes.MapRoute(name: "Inicio",
                url: "Inicio",
                defaults: new { controller = "Login", action = "Login" }, namespaces: new[] { "soporte.Controllers" }
                );
            routes.MapRoute(name: "ReglasLog",
                url: "LoginReglas",
                defaults: new { controller = "Login", action = "LogReglas" }, namespaces: new[] { "soporte.Controllers" }
                );
            routes.MapRoute(
               name: "IndexReglas",
               url: "Reglas/{startIndex}",
               defaults: new { controller = "Reglas", action = "Index", startIndex = 0 }, 
               constraints: new { startIndex=@"\d+"},
               namespaces: new[] { "soporte.Controllers" }
               );
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.Ignore("{*allasmx}", new { allasmx = @".*\.asmx(/.*)?" });            
            routes.MapRoute(name: "Logout",
                url: "Logout",
                defaults: new { controller = "Logout", action = "Logout", id = UrlParameter.Optional }
            );
            routes.MapRoute(name: "LogoutAd",
                url: "LogoutAd",
                defaults: new { controller = "Logout", action = "LogoutAd", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                "SinAcceso",
                "NoAccess/{action}",
                defaults: new { controller = "NoAccess", action = "Index" }
            );
            routes.MapRoute(
                "Excel",
                "Excel",
                defaults: new { controller="Excel",action="getFile"}
                );

            routes.MapRoute(name: "MainConfig",
                url: "Config/DatosPersonales",
                defaults: new { controller = "Config", action = "DatosPersonales" }, namespaces: new[] { "soporte.Controllers" });
            routes.MapRoute(name:"temas",
                url:"setTemas",
                defaults: new { controller = "Config", action = "setTheme" }, namespaces: new[] { "soporte.Controllers" });
            routes.MapRoute(name: "GralConfig",
                url: "Config/{action}/{id}",
                defaults: new { controller = "Config", id=UrlParameter.Optional }, 
                namespaces: new[] { "soporte.Controllers" });
            routes.MapRoute(name: "Service",
                url: "Service/{action}/{id}",
                defaults: new { controller = "Service", id = UrlParameter.Optional },
                namespaces: new[] { "soporte.Controllers" });

            routes.MapRoute(name: "Default",
                url: "{controller}/{action}/{ids}",
                defaults: new { controller = "Login", action="Index", ids = UrlParameter.Optional });
            //namespaces: new[] { "soporte.Areas.Soporte.Controllers" });
            routes.MapRoute(name: "All",
            url: "{*catchall}",
            defaults: new { controller = "Login", action = "Index" }, namespaces: new[] { "soporte.Controllers" });
        }
    }
}