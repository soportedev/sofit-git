﻿using System.Web;
using System.Web.Optimization;


namespace soporte
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;
            /******************
             * Seguridad
             * ****************/
            //Scripts
            bundles.Add(new ScriptBundle("~/bundles/seguridadConfig").Include("~/Areas/Seguridad/Scripts/Config.js").Include("~/Scripts/DatosPers.js").Include("~/Scripts/ConfigComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/InventarioSeguridad").Include("~/Areas/Seguridad/Scripts/Inventario.js"));
            bundles.Add(new ScriptBundle("~/bundles/IncidenteSeguridad").Include("~/Scripts/Ventana.js", "~/Areas/Seguridad/Scripts/Incidentes.js", "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));                        
            //Styles
            bundles.Add(new StyleBundle("~/Css/ConfigSeguridad").Include("~/Areas/Seguridad/Content/Config.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/DepositoSeguridad").Include("~/Areas/Seguridad/Content/stock.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/Servicio8").Include("~/Areas/Seguridad/Content/Servicio8.css"));
          
            /******************
             * Infraestructura
             * ****************/            
            //Scripts
            bundles.Add(new ScriptBundle("~/bundles/infraConfig").Include("~/Areas/Infraestructura/Scripts/Config.js").Include("~/Scripts/DatosPers.js").Include("~/Scripts/ConfigComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/InventarioInfra").Include("~/Areas/Infraestructura/Scripts/Inventario.js"));
            bundles.Add(new ScriptBundle("~/bundles/IncInfra").Include("~/Scripts/Ventana.js","~/Areas/Infraestructura/Scripts/Incidentes.js", "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));            
            //Styles
            bundles.Add(new StyleBundle("~/Css/InfraConfig").Include("~/Areas/Telecomunicaciones/Content/Config.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/InfraDeposito").Include("~/Areas/Telecomunicaciones/Content/stock.css", new CssRewriteUrlTransform()));
            
            /**********************
             * Soporte
             * *******************/
            //Scripts            
            bundles.Add(new ScriptBundle("~/bundles/sopConfig").Include("~/Areas/Soporte/Scripts/Config.js").Include("~/Scripts/DatosPers.js").Include("~/Scripts/ConfigComun.js"));            
            bundles.Add(new ScriptBundle("~/bundles/IncSoporte").Include( "~/Areas/Soporte/Scripts/incidentes.js","~/Scripts/jquery.tablesorter.min.js", 
                "~/Scripts/Ventana.js", "~/Scripts/IncidentesComun.js","~/Areas/Soporte/Scripts/variosIncidente.js"));
            bundles.Add(new ScriptBundle("~/bundles/InventarioSoporte").Include("~/Areas/Soporte/Scripts/Inventario/*.js"));
            //Styles            
            bundles.Add(new StyleBundle("~/Css/soporteIncidentes").Include("~/Areas/Soporte/Content/incidente.css", new CssRewriteUrlTransform()));            
            bundles.Add(new StyleBundle("~/Css/InventarioSoporte").Include("~/Areas/Soporte/Content/Inventario/*.css"));
            /*****************
             * Operaciones
             * **************/
            //Styles
            bundles.Add(new StyleBundle("~/Css/InventarioOpe").Include("~/Areas/Operaciones/Content/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/IncOpe").Include("~/Areas/Operaciones/Content/jquery-ui-timepicker-addon.css"));
            //Scripts
            bundles.Add(new ScriptBundle("~/bundles/IncOperaciones").Include("~/Scripts/Ventana.js","~/Areas/Operaciones/Scripts/IncidentesOp.js","~/Areas/Operaciones/Scripts/jquery-ui-timepicker-addon.js", 
                "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/InventarioOpe").Include("~/Areas/Telecomunicaciones/Scripts/classie.js", "~/Areas/Telecomunicaciones/Scripts/modernizr.custom.js", 
                "~/Areas/Telecomunicaciones/Scripts/notificationFx.js", "~/Areas/Operaciones/Scripts/Inventario.js"));
            bundles.Add(new ScriptBundle("~/bundles/opeConfig").Include("~/Areas/Operaciones/Scripts/Config.js").Include("~/Scripts/DatosPers.js").Include("~/Scripts/ConfigComun.js"));            
            
             /**********************
             * Telecomunicaciones
             * ********************/
            //Scripts
            bundles.Add(new ScriptBundle("~/bundles/InventarioTele").Include("~/Areas/Telecomunicaciones/Scripts/Inventario.js"));
            bundles.Add(new ScriptBundle("~/bundles/IncTele").Include("~/Scripts/jquery.scrollTo.min.js", "~/Scripts/Ventana.js",
                "~/Areas/Telecomunicaciones/Scripts/Incidentes.js", "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/teleConfig").Include("~/Areas/Telecomunicaciones/Scripts/Config.js").Include("~/Scripts/DatosPers.js").Include("~/Scripts/ConfigComun.js"));            
            //Styles
            bundles.Add(new StyleBundle("~/Css/teleConfig").Include("~/Areas/Telecomunicaciones/Content/Config.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/teleDeposito").Include("~/Areas/Telecomunicaciones/Content/stock.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/todoTele").Include("~/Areas/Telecomunicaciones/Content/*.css",new CssRewriteUrlTransform()));
            /**********************
             * Mesa
             * ********************/
            //Scripts            
            bundles.Add(new ScriptBundle("~/bundles/mesaConfig").Include("~/Areas/Mesa/Scripts/MesaConfig.js").Include("~/Scripts/ConfigComun.js").Include("~/Scripts/DatosPers.js"));
            bundles.Add(new ScriptBundle("~/bundles/IncMesa").Include("~/Scripts/Ventana.js","~/Areas/Mesa/Scripts/incidentes.js", "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/InventarioMesa").Include("~/Areas/Mesa/Scripts/MesaInventario.js"));
            //Styles
            bundles.Add(new StyleBundle("~/Css/mesaConfig").Include("~/Areas/Mesa/Content/MesaConfig.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/InventarioMesa").Include("~/Areas/Mesa/Content/MesaInventario.css"));
             /**********************
             * Direccion
             * ********************/
            bundles.Add(new ScriptBundle("~/bundles/IncDireccion").Include("~/Scripts/Ventana.js", "~/Areas/Direccion/Scripts/incidentes.js", "~/Scripts/jquery.tablesorter.min.js", "~/Scripts/IncidentesComun.js"));            
            bundles.Add(new ScriptBundle("~/bundles/DireccionConfig").Include("~/Areas/Direccion/Scripts/Config.js","~/Scripts/ConfigComun.js"));
            /**********************
             * Comun
             * ********************/
            /**********Scripts***********/
            bundles.Add(new ScriptBundle("~/bundles/Servicio8").Include("~/Scripts/Servicio8/Servicio8.js"));
            bundles.Add(new ScriptBundle("~/bundles/Cambios").Include("~/Scripts/Cambios/Cambios.js"));
            bundles.Add(new ScriptBundle("~/bundles/LBase").Include("~/Scripts/LineaBase.js", "~/Scripts/timeline.js", "~/Scripts/cmdb-scripts.js"));
            bundles.Add(new ScriptBundle("~/bundles/Relaciones").Include("~/Scripts/relaciones.js"));
            bundles.Add(new ScriptBundle("~/bundles/Consultas").Include("~/Scripts/Consultas/*.js","~/Scripts/Material/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/usuarios").Include("~/Scripts/Usuarios.js"));            
            bundles.Add(new ScriptBundle("~/bundles/jurisdicciones").Include("~/Scripts/Jurisdicciones.js"));
            bundles.Add(new ScriptBundle("~/bundles/reglas").Include("~/Scripts/Reglas.js"));
            bundles.Add(new ScriptBundle("~/bundles/validate").Include("~/Scripts/jquery.validate.js"));            
            bundles.Add(new ScriptBundle("~/bundles/base").Include("~/Scripts/jquery-2.0.3.min.js", 
                "~/Scripts/jquery-migrate-1.2.1.min.js", "~/Scripts/utils.js", "~/Scripts/nprogress.js","~/Scripts/modernizr.custom.js",
                "~/Scripts/classie.js", "~/Scripts/notificationFx.js", "~/Scripts/jquery.nicescroll.js", "~/Scripts/jquery.marquee.min.js", "~/Scripts/jquery-ui.js"));
            bundles.Add(new ScriptBundle("~/bundles/Login").Include("~/Scripts/Material/*.js","~/Scripts/Login/jquery.reject.js", "~/Scripts/Login/CapsLock.src.js", "~/Scripts/Login/login.js"));            
            bundles.Add(new ScriptBundle("~/bundles/deposito").Include("~/Scripts/deposito.js", "~/Scripts/jquery.dataTables.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/nuevoIncidente").Include("~/Scripts/nuevoIncidente.js"));
            bundles.Add(new ScriptBundle("~/bundles/materialize").Include("~/Scripts/Materialize/materialize.js"));
            bundles.Add(new ScriptBundle("~/bundles/historicos").Include("~/Scripts/HistoricosComun.js"));
            bundles.Add(new ScriptBundle("~/bundles/Gs").Include("~/Areas/Gs/Scripts/*.js"));
            /***Styles****/
            bundles.Add(new StyleBundle("~/Css/historicos").Include("~/Content/Historicos/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/reglas").Include("~/Content/Reglas/*.css"));
            bundles.Add(new StyleBundle("~/Css/cambios").Include("~/Content/Cambios/*.css"));
            bundles.Add(new StyleBundle("~/Css/notifications").Include("~/Content/ns-default.css","~/Content/ns-style-growl.css"));
            bundles.Add(new StyleBundle("~/Css/base").Include("~/Content/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/login").Include("~/Content/login/*.css"));
            bundles.Add(new StyleBundle("~/Css/material").Include("~/Content/Material/*.css",new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/materialize").Include("~/Content/Materialize/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/consultas").Include("~/Content/Consultas/*.css"));
            bundles.Add(new StyleBundle("~/Css/Config").Include("~/Content/Config/Config.css"));
            bundles.Add(new StyleBundle("~/Css/deposito").Include("~/Content/Deposito/deposito.css"));
            bundles.Add(new StyleBundle("~/Css/nuevoIncidente").Include("~/Content/NuevoIncidente/nuevoIncidente.css"));
            bundles.Add(new StyleBundle("~/Css/fa").Include("~/Content/font-awesome-4.7.0/css/font-awesome.min.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/LBase").Include("~/Content/Cmdb/*.css"));
            bundles.Add(new StyleBundle("~/Css/Gs").Include("~/Areas/Gs/Content/*.css"));
            //Temas
            bundles.Add(new StyleBundle("~/Css/themes/1").Include("~/Content/themes12/blitzer/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/2").Include("~/Content/themes12/cupertino/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/3").Include("~/Content/themes12/dark-hive/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/4").Include("~/Content/themes12/eggplant/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/5").Include("~/Content/themes12/excite-bike/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/6").Include("~/Content/themes12/flick/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/7").Include("~/Content/themes12/hot-sneaks/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/8").Include("~/Content/themes12/humanity/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/9").Include("~/Content/themes12/le-frog/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/10").Include("~/Content/themes12/overcast/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/11").Include("~/Content/themes12/pepper-grinder/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/12").Include("~/Content/themes12/redmond/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/13").Include("~/Content/themes12/smoothness/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/14").Include("~/Content/themes12/south-street/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/15").Include("~/Content/themes12/start/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/16").Include("~/Content/themes12/sunny/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/17").Include("~/Content/themes12/ui-darkness/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/18").Include("~/Content/themes12/ui-lightness/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/19").Include("~/Content/themes12/vader/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/20").Include("~/Content/themes12/black-tie/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/21").Include("~/Content/themes12/dot-luv/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/22").Include("~/Content/themes12/mint-choc/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/23").Include("~/Content/themes12/swanky-purse/*.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Css/themes/24").Include("~/Content/themes12/trontastic/*.css", new CssRewriteUrlTransform()));
        }
    }
}