﻿using Sofit.DataAccess;
using Sofit.Models.ClasesVistas;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    [SessionActionFilter]
    public class LineaBaseController : Controller
    {
        // GET: /LineasBase/        
        public ActionResult Index()
        {
            //usuarioBean usuario = Session["usuario"] as usuarioBean;
            //Constantes.AccesosPagina acceso = usuario.getAccess("Cmdb");
            //string path = string.Empty;
            //if (acceso == Constantes.AccesosPagina.NoAccess)
            //{
            //    return RedirectToRoute("SinAcceso");
            //}
            return View();
        }
        public JsonResult CrearNueva()
        {
            Responses result = new Responses();
            NameValueCollection nvc=Request.Form;
            Regex pattern = new Regex(@"\d+");    
            string nombreBl=nvc.Get("nombre");
            nombreBl = nombreBl.ToUpper();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            using(var dbc=new IncidenciasEntities())
            {
                List<UCxBaseLine> unidades = new List<UCxBaseLine>();
                BaseLineGral baseline = new BaseLineGral
                {
                    fecha = DateTime.Now,
                    nombre = nombreBl,
                    id_creadopor = usuarioActual.IdUsuario
                };
                dbc.BaseLineGral.Add(baseline);
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("Elementos"))
                    {
                        Match val = pattern.Match(key);
                        string index = val.Value;
                        int idEquipo = Int32.Parse(nvc.GetValues(key)[0]);
                        var uc = dbc.UnidadConf.Where(c => c.id_uc == idEquipo).Single();
                        var bl = uc.BaseLines.OrderByDescending(c => c.fecha).First();
                        UCxBaseLine ucx = new UCxBaseLine
                        {
                            fecha_bl_uc = bl.fecha,
                            orden = Int32.Parse(index),
                            id_bl_gral=baseline.id,
                            id_bl_uc=bl.id
                        };
                        unidades.Add(ucx);
                    }
                }
                if (unidades.Count == 0)
                {
                    result.Info = "error";
                    result.Detail = "No se ingresaron Elementos de Configuración";
                }                
                else
                {
                    baseline.UCxBaseLine = unidades;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }                
            return Json(result);
        }
        public JsonResult CheckLineaBase(int idEq)
        {
            Responses result = new Responses();
            
            using (var dbc = new IncidenciasEntities())
            {
                var lineaBase = dbc.BaseLines.Where(c => c.master_id == idEq).OrderByDescending(d => d.fecha);
                if (lineaBase.Count() > 0)
                {
                    result.Info = "ok";
                    result.Detail = lineaBase.First().fecha.ToShortDateString();
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "No hay líneas base creadas para el equipo";
                }
            }
            return Json(result);
        }
        public class SS
        {
            public string Nombre { get; set; }
            public List<DatosAgregate> Datos = new List<DatosAgregate>();
        }
        public class DatosAgregate:IComparable
        {
            public int id { get; set; }
            public DateTime Fecha { get; set; }
            public string FechaStr { get { return Fecha.ToShortDateString(); } set { } }
            public string CreadoPor { get; set; }
            public string Nombre { get; set; }
            public List<ElementoLineaBase>Elementos { get; set; }

            public int CompareTo(object obj)
            {
                DatosAgregate other = obj as DatosAgregate;
                if (this.Fecha < other.Fecha) return 1;
                if (this.Fecha > other.Fecha) return -1;
                return 0;
            }
        }
        public JsonResult GetLineaBase()
        {
            Responses result = new Responses();
            List<SS> instancias = new List<SS>();
            
            using(var dbc=new IncidenciasEntities())
            {
                //lineaBase = dbc.BaseLineGral.Select(c => new LineaBaseGralVista
                //{
                //    CreadoPor = c.Usuarios.nombre,
                //    Fecha = c.fecha,
                //    Nombre = c.nombre,
                //    idLineaBase = c.id,
                //    Elementos=dbc.UCxBaseLine.Where(d => d.id_bl_gral == c.id).Select(f => new ElementoLineaBase
                //    {
                //        id = f.BaseLines.UnidadConf.id_uc,
                //        Nombre = f.BaseLines.UnidadConf.nombre,
                //        orden = f.orden
                //    }).OrderBy(g => g.orden).ToList()
                //}).OrderByDescending(d=>d.Fecha).GroupBy(p => p.Nombre, (key, body) => new SS { 
                //    Nombre=key,
                //    Datos = body
                //}).ToList();
                var lbas = from u in dbc.BaseLineGral
                           group u by u.nombre into newGroup
                           orderby newGroup.Key
                           select newGroup;
                
                foreach (var nameGroup in lbas)
                {
                    SS instancia = new SS
                    {
                        Nombre = nameGroup.Key
                    };
                    foreach (var inst in nameGroup)
                    {
                        DatosAgregate s = new DatosAgregate
                        {
                            CreadoPor = inst.Usuarios.nombre_completo,
                            Fecha = inst.fecha,
                            Nombre = inst.nombre,
                            id = inst.id,
                            Elementos = dbc.UCxBaseLine.Where(d => d.id_bl_gral == inst.id).Select(f => new ElementoLineaBase
                            {
                                id = f.BaseLines.UnidadConf.id_uc,
                                Nombre = f.BaseLines.UnidadConf.nombre,
                                orden = f.orden
                            }).OrderBy(g => g.orden).ToList()
                        };
                        instancia.Datos.Add(s);
                    }
                    instancia.Datos.Sort();
                    instancias.Add(instancia);
                }
                //foreach(LineaBaseGralVista lbg in lineaBase)
                //{
                //    lbg.Elementos = dbc.UCxBaseLine.Where(c => c.id_bl_gral == lbg.idLineaBase).Select(c => new ElementoLineaBase
                //    {
                //        id = c.BaseLines.UnidadConf.id_uc,
                //        Nombre = c.BaseLines.UnidadConf.nombre,
                //        orden = c.orden
                //    }).OrderBy(c => c.orden).ToList();                    
                //}
            }            
            return Json(instancias);
        }
        public ActionResult getExcel(int idTipo)
        {            
            using (var dbc = new IncidenciasEntities())
            {
                var tipo = dbc.Tipo_equipo.Where(c => c.id_tipo == idTipo);
                if (tipo != null)
                {
                    if (tipo.Count() > 0)
                    {
                        string nombretipo = tipo.Single().descripcion;
                        switch (nombretipo)
                        {
                            case "Switches":
                                List<SwitchListadoExcel> listadoswitch;
                                listadoswitch = dbc.EquipoTele.Select(d => new SwitchListadoExcel
                                {
                                    Nombre = d.UnidadConf.nombre,
                                    Jurisdiccion = d.UnidadConf.Jurisdiccion.descripcion,
                                    Observaciones = d.UnidadConf.observaciones,
                                    TipoEquipo = d.UnidadConf.Tipo_equipo.descripcion,
                                    Estado = d.Estado_UC.descripcion,
                                    NombreComun = d.nombre_comun,
                                    IP = d.Ip.Select(c => c.ip_v4).FirstOrDefault(),
                                    RelacionesLista = d.UnidadConf.UnidadConf1.Select(c => new RelationVista { Oblea = c.nombre }).ToList()
                                }).ToList();
                                DataTable tablaswitch = DataTableConverter.ToDataTable(listadoswitch);
                                Session["tableSource"] = tablaswitch;
                                break;
                            default:
                                List<ElementoListadoExcel> listado;
                                listado = dbc.UnidadConf.Where(c => c.id_tipo == idTipo).Select(d => new ElementoListadoExcel
                                {
                                    Nombre = d.nombre,
                                    Jurisdiccion = d.Jurisdiccion.descripcion,
                                    Observaciones = d.observaciones,
                                    TipoEquipo = d.Tipo_equipo.descripcion

                                }).ToList();
                                DataTable table = DataTableConverter.ToDataTable(listado);
                                Session["tableSource"] = table;
                                break;

                        }
                    }
                }
                
                
            }
            return RedirectToRoute("Excel");
        }
    }
}
