﻿using Sofit.DataAccess;
using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class LogoutController : Controller
    {
        //
        // GET: /Logout/

        public ActionResult Logout()
        {
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            HttpContext.Session.Abandon();
            if (usuarioActual != null)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuarioActual.IdUsuario & !c.fechaHasta.HasValue);
                    foreach (var x in viejoLogin)
                    {
                        x.fechaHasta = DateTime.Now;
                    }
                    dbc.SaveChanges();
                }
                //OnlineUsers.removeUser(usuarioActual);
            }
            return Redirect("Login");
        }
        public ActionResult LogoutAd()
        {
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            HttpContext.Session.Abandon();
            if (usuarioActual != null)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuarioActual.IdUsuario & !c.fechaHasta.HasValue);
                    foreach (var x in viejoLogin)
                    {
                        x.fechaHasta = DateTime.Now;
                    }
                    dbc.SaveChanges();
                }
                //OnlineUsers.removeUser(usuarioActual);
            }
            return Redirect("Login");
        }
        //
        // GET: /Logout/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Logout/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Logout/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Logout/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Logout/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Logout/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Logout/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
