﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class AllowCrossSiteJsonAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            base.OnActionExecuting(filterContext);

            //var domains = new List<string> { "domain2.com", "domain1.com" };

            //if (domains.Contains(filterContext.RequestContext.HttpContext.Request.UrlReferrer.Host))
            //{
            //    filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //}

            //base.OnActionExecuting(filterContext);
        }
    }
}