﻿using Sofit.DataAccess;
using Sofit.Models.Cambios;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Helpers;
using soporte.Models.Reglas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class CambiosController : Controller
    {
        private List<RelationVista> relations = new List<RelationVista>();
        // GET: Cambios
        public ActionResult Index(string startIndex)
        {
            int PageSize = 10;

            string estado = Request["estado"];
            int idEstado = 0;
            Int32.TryParse(estado, out idEstado);
            string numero = Request["nro"];
            int idCambio = 0;
            Int32.TryParse(numero, out idCambio);

            CambiosModel modelo = getModel(idEstado,idCambio);

            int cantidadPedidos = modelo.MisCambios.Count;

            double cantPagCambios = (double)cantidadPedidos / (double)PageSize;
            double cantidadPagPedidos = Math.Ceiling(cantPagCambios);
            modelo.CantidadPaginasCambios = Convert.ToInt32(cantidadPagPedidos);

            if (startIndex != null)
            {
                int stI = 0;
                Int32.TryParse(startIndex, out stI);
                int skip = 0;
                if (stI != 1)
                {
                    skip = (stI - 1) * PageSize;
                }
                var cambios = modelo.MisCambios.Skip(skip).Take(PageSize);

                modelo.PaginaActualCambios = stI;
                modelo.MisCambios = cambios.ToList();
            }
            else
            {
                var cambios = modelo.MisCambios.Take(PageSize);
                modelo.PaginaActualCambios = 1;
                modelo.MisCambios = cambios.ToList();            }
            return View(modelo);
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase fileA)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            CambiosVista nuevoPedido = new CambiosVista();
            bool flagFile = true;
            NameValueCollection nvc = Request.Form;
            PedidosCambiosManager manager = new PedidosCambiosManager();
            string asunto = string.Empty;
            List<EcCambiosVista> Ecs = new List<EcCambiosVista>();
            string idAutorizante = string.Empty;
            string nota = string.Empty;
            //getdata
            try
            {
                DateTime fechahoraresultante = DateTime.Now;
                string modo = nvc.GetValues("modo")[0];//para si nuevo o modif
                string idCambio = nvc.GetValues("idCambio")[0];//elemscambiados en el caso de modif
                try
                {
                    nota = nvc.GetValues("notas")[0];
                    asunto = nvc.GetValues("asunto")[0];
                    idAutorizante = nvc.GetValues("autorizante")[0];

                }
                catch (Exception e)
                {

                }
                //claves elem. conf.
                foreach (string k in nvc.Keys)
                {
                    if (k.StartsWith("idEC-n")) Ecs.Add(new EcCambiosVista { Nombre = nvc.GetValues(k)[0] });
                    if (k.StartsWith("nombreC")) Ecs.Add(new EcCambiosVista { NombreComun = nvc.GetValues(k)[0] });
                }                
                string descripcion = nvc.GetValues("descripcion")[0];

                string idTipo = nvc.GetValues("idTipo")[0];

                string idPrioridad = nvc.GetValues("prioridad")[0];
                DateTime fechaInicioEstimada = DateTime.Parse(nvc.GetValues("fechaInicioEstimada")[0]);
                string horainicioest = nvc.GetValues("horaInicioEstimada")[0];
                try
                {
                    DateTime horainicioEstimada = DateTime.Parse(horainicioest);
                    fechahoraresultante = fechaInicioEstimada.AddHours(horainicioEstimada.Hour).AddMinutes(horainicioEstimada.Minute);
                }
                catch (Exception e)
                {
                    DateTime horaini = DateTime.Parse(horainicioest);
                    fechahoraresultante = fechaInicioEstimada.Add(TimeSpan.Parse((horaini.Hour + horaini.Minute).ToString()));
                }

                string duracionEstimada = nvc.GetValues("duracionEstimada")[0];
                string razon = nvc.GetValues("razon")[0];
                string resultados = nvc.GetValues("resultados")[0];
                string riesgos = nvc.GetValues("riesgos")[0];
                string recursos = nvc.GetValues("recursos")[0];
                string serviciosAfectados = nvc.GetValues("serviciosAfectados")[0];
                string vueltaAtras = nvc.GetValues("rollBack")[0];
                string conclusion = nvc.GetValues("conclusion")[0];
                //creamos el objeto
                UsuarioVista solicitante = new UsuarioVista
                {
                    id = usuario.IdUsuario,
                    nombre = usuario.Nombre,
                    displayName = usuario.NombreCompleto
                };

                if (modo != "update")
                {
                    nuevoPedido.Asunto = asunto;
                    nuevoPedido.ElemConf = Ecs;
                    UsuarioVista autorizante = new UsuarioVista
                    {
                        id = Int32.Parse(idAutorizante)
                    };
                    nuevoPedido.Autorizante = autorizante;
                }
                nuevoPedido.fechaCreacion = DateTime.Now;
                nuevoPedido.Descripcion = descripcion;
                nuevoPedido.Solicitante = solicitante;
                nuevoPedido.EstadoNegocio = EstadoCambiosFactory.getEstadoBiz("Solicitud de Cambio");
                nuevoPedido.Tipo = new TipoCambio { Id = Int32.Parse(idTipo) };
                nuevoPedido.Prioridad = new PrioridadCambio { Id = Int32.Parse(idPrioridad) };
                nuevoPedido.fechaInicioEstimada = fechahoraresultante;
                nuevoPedido.duracionEstimada = Int32.Parse(duracionEstimada);
                nuevoPedido.Razon = razon;
                nuevoPedido.Resultados = resultados;
                nuevoPedido.Riesgos = riesgos;
                nuevoPedido.Recursos = recursos;
                nuevoPedido.ServiciosAfectados = serviciosAfectados;
                nuevoPedido.VueltaAtras = vueltaAtras;
                nuevoPedido.Conclusion = conclusion;
                nuevoPedido.UsuarioActual = new UsuarioVista { id = usuario.IdUsuario, displayName = usuario.NombreCompleto };
                nuevoPedido.Notas = nota;
                nuevoPedido.Archivos = new List<string>();
                if (fileA != null)//tiene archivo adjunto
                {
                    //----------------
                    string nombreFile = string.Empty;
                    string pathImagen = string.Empty;
                    string nombreArchivo = fileA.FileName;
                    string[] splitN = nombreArchivo.Split('.');
                    string ran = new Random().Next(10000).ToString("0000");
                    nombreFile = splitN.First() + ran + '.' + splitN.Last();
                    try
                    {
                        pathImagen = PathImage.getCambiosPath(nombreFile);
                        var stream = fileA.InputStream;
                        using (var fileStream = System.IO.File.Create(pathImagen))
                        {
                            stream.CopyTo(fileStream);
                        }
                        nuevoPedido.Archivos.Add(nombreFile);
                    }
                    catch (Exception e)
                    {
                        TempData["message"] = e.Message;
                        flagFile = false;
                    }
                }
                if (flagFile)//sin errores con el archivo
                {
                    if (modo.Equals("update"))
                    {
                        int idC = Int32.Parse(idCambio);
                        nuevoPedido.idCambio = idC;
                        result = manager.modificacion(nuevoPedido);
                        if (result.Info != "ok")
                        {
                            TempData["message"] = result.Detail;
                        }

                    }
                    else
                    {//es modificacion
                        //agregamos el nuevo pedido                    
                        result = manager.NuevoPedidoCambio(nuevoPedido);
                        if (result.Info != "ok")
                        {
                            TempData["message"] = result.Detail;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["message"] = e.Message;
            }
            HttpRequestBase a = Request;
            return Redirect(a.UrlReferrer.AbsolutePath);
        }
        
        private CambiosModel getModel(int estado, int idCambio)
        {
            CambiosModel modelo = new CambiosModel();
            List<UsuarioVista> aut = null;
            List<CambiosVista> cambios = new List<CambiosVista>();
            List<EstadoCambioVista> estados = new List<EstadoCambioVista>();
            List<TipoCambio> tipoCambio = new List<TipoCambio>();
            List<PrioridadCambio> prioridad = new List<PrioridadCambio>();
            using (var dbc = new IncidenciasEntities())
            {
                IEnumerable<GestionCambios> ped = null;
                usuarioBean logueado = Session["usuario"] as usuarioBean;
                string mirol = "";
                if(logueado.Roles.Contains(new RolUsuario { Nombre="Director Cambios" }))
                {
                    mirol = "Director Cambios";
                }
                if (idCambio != 0)
                {
                    ped = dbc.GestionCambios.Where(c => c.id == idCambio);
                }
                else
                {                    
                    if (estado != 0)
                    {
                        ped= dbc.GestionCambios.Where(d => d.id_estado == estado);
                    }
                    else
                    {
                        ped = dbc.GestionCambios.Where(d => d.fecha_fin == null);
                    }                       
                }
                estados = dbc.EstadoGC.Select(c => new EstadoCambioVista { Id = c.id, Nombre = c.nombre }).ToList();
                foreach (var d in ped)
                {
                    CambiosVista p = new CambiosVista
                    {
                        idCambio=d.id,
                        Asunto = d.asunto,
                        Solicitante = new UsuarioVista { id = d.Usuarios1.id_usuario, nombre = d.Usuarios1.nombre, displayName=d.Usuarios1.nombre_completo },
                        Autorizante = new UsuarioVista { id = d.Usuarios.id_usuario, nombre = d.Usuarios.nombre,displayName=d.Usuarios.nombre_completo },
                        Conclusion = d.conclusion,
                        Descripcion = d.descripcion,
                        duracionEstimada = d.duracion_estimada,
                        EstadoActual = new EstadoCambioVista { Id = d.EstadoGC.id, Nombre = d.EstadoGC.nombre },
                        fechaCreacion = d.fecha_creacion,
                        ElemConf = d.EcxGc.Select(c => new EcCambiosVista { Id = c.id_uc, Nombre = c.UnidadConf.nombre, ElemsRelacionados = related(c.UnidadConf.nombre) }).ToList(),
                        fechaInicioEstimada = d.fecha_inicio_estimada,
                        Prioridad = new PrioridadCambio { Id = d.PrioridadGC.id, Nombre = d.PrioridadGC.nombre },
                        Razon = d.razon,
                        Recursos = d.recursos,
                        Resultados = d.resultados,
                        Riesgos = d.riesgos,
                        ServiciosAfectados=d.servicios_afectados,
                        Tipo = new TipoCambio { Id = d.TipoGC.id, Nombre = d.TipoGC.nombre },                        
                        VueltaAtras = d.vuelta_atras,         
                        Logs=d.HistoricoGC.Select(c=>new Log { fechadt=c.fecha, Modificacion =  c.Mods.Select(f=>f.observaciones).ToList(), Usuario=c.Usuarios.nombre_completo}).ToList(),
                        Archivos=d.DocXGC.Select(c=> c.path_doc ).ToList(),
                        MiRol = mirol == "Director Cambios" ? "Director Cambios" : logueado.IdUsuario == d.Usuarios.id_usuario ? "Autorizante" : logueado.IdUsuario == d.Usuarios1.id_usuario ? "Solicitante" : "Observador"
                    };
                    cambios.Add(p);
                }                
                cambios.Sort();
                prioridad = dbc.PrioridadGC.Select(c => new PrioridadCambio { Id = c.id, Nombre = c.nombre }).ToList();
                tipoCambio = dbc.TipoGC.Select(c => new TipoCambio { Id = c.id, Nombre = c.nombre }).ToList();

                aut = dbc.Usuarios.Where(c => c.Roles.Any(d => d.Nombre == "Autorizante Cambios")).OrderBy(c => c.nombre_completo).
                    Select(c => new UsuarioVista { id = c.id_usuario, nombre = c.nombre, displayName = c.nombre_completo }).ToList();

                //pedidos del solicitante
            }
            modelo.Autorizantes = aut;
            modelo.MisCambios = cambios;
            modelo.Estados = estados;
            modelo.Prioridad = prioridad;
            modelo.Tipo = tipoCambio;
            return modelo;
        }
        public JsonResult Relaciones(string oblea)
        {           
            UnidadConf uc = null;
            relations = new List<RelationVista>();
            using (var dbc = new IncidenciasEntities())
            {
                uc = dbc.UnidadConf.Where(c => c.nombre == oblea).SingleOrDefault();
                foreach(UnidadConf u in uc.UnidadConf1)
                {
                    RelationVista nueva = new RelationVista { Id = u.id_uc, Oblea = u.nombre, TipoEquipo = u.Tipo_equipo.descripcion };
                    relations.Add(nueva);
                }
                foreach (UnidadConf u in uc.UnidadConf2)
                {
                    RelationVista nueva = new RelationVista { Id = u.id_uc, Oblea = u.nombre, TipoEquipo = u.Tipo_equipo.descripcion };
                    relations.Add(nueva);
                }
            }
            
            return Json(relations);
        }
        private List<RelationVista> related(string oblea)
        {
            UnidadConf uc = null;
            relations = new List<RelationVista>();
            using (var dbc = new IncidenciasEntities())
            {
                uc = dbc.UnidadConf.Where(c => c.nombre == oblea).SingleOrDefault();
                foreach (UnidadConf u in uc.UnidadConf1)
                {
                    RelationVista nueva = new RelationVista { Id = u.id_uc, Oblea = u.nombre, TipoEquipo = u.Tipo_equipo.descripcion };
                    relations.Add(nueva);
                }
                foreach (UnidadConf u in uc.UnidadConf2)
                {
                    RelationVista nueva = new RelationVista { Id = u.id_uc, Oblea = u.nombre, TipoEquipo = u.Tipo_equipo.descripcion };
                    relations.Add(nueva);
                }
            }
            return relations;
        }
        public JsonResult getPedido(int id)
        {
            Responses result = new Responses();
            CambiosVista p = null;
            using (var dbc = new IncidenciasEntities())
            {
                usuarioBean logueado = Session["usuario"] as usuarioBean;

                var d = dbc.GestionCambios.Where(c => c.id == id).Single();
                
                p = new CambiosVista
                {
                        Asunto = d.asunto,
                        Autorizante = new UsuarioVista { id = d.Usuarios.id_usuario, nombre = d.Usuarios.nombre },
                        Conclusion = d.conclusion,
                        Descripcion = d.descripcion,
                        duracionEstimada = d.duracion_estimada,
                        EstadoActual = new EstadoCambioVista { Id = d.EstadoGC.id, Nombre = d.EstadoGC.nombre },
                        fechaCreacion = d.fecha_creacion,
                        fechaInicioEstimada = d.fecha_inicio_estimada,
                        Prioridad = new PrioridadCambio { Id = d.PrioridadGC.id, Nombre = d.PrioridadGC.nombre },
                    ElemConf = d.EcxGc.Select(c => new EcCambiosVista { Id = c.id_uc, Nombre = c.UnidadConf.nombre, ElemsRelacionados = related(c.UnidadConf.nombre) }).ToList(),
                    Razon = d.razon,
                        Recursos = d.recursos,
                        Resultados = d.resultados,
                        Riesgos = d.riesgos,
                        ServiciosAfectados=d.servicios_afectados,
                        Tipo = new TipoCambio { Id = d.TipoGC.id, Nombre = d.TipoGC.nombre },
                        VueltaAtras = d.vuelta_atras,
                        Solicitante = new UsuarioVista { id = d.Usuarios1.id_usuario, nombre = d.Usuarios1.nombre_completo }                                               
                };               
            }
            return Json(p);
        }
        private void getFullRels(UnidadConf uc)
        {
            if (uc != null)
            {
                RelationVista nueva = new RelationVista { Id = uc.id_uc, Oblea = uc.nombre, TipoEquipo = uc.Tipo_equipo.descripcion };
                if (!relations.Contains(nueva))
                {
                    relations.Add(nueva);
                    foreach (UnidadConf u in uc.UnidadConf1)
                    {                        
                        getFullRels(u);
                    }
                    foreach (UnidadConf u in uc.UnidadConf2)
                    {
                        getFullRels(u);
                    }
                }
            }           
        }
        public JsonResult Aprobar(int id)
        {
            bool flag = false;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Aprobado").Single();
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    cambio.EstadoGC = estadoN;
                    mods.Add("Se cambió el estado a: " + estadoN.nombre);
                    flag = true;
                    if (flag)
                    {
                        try
                        {
                            HistoricoGC nuevo = new HistoricoGC
                            {
                                id_cambio = id,
                                fecha = DateTime.Now,
                                id_usuario = usuario.IdUsuario
                            };
                            foreach (var v in mods)
                            {
                                nuevo.Mods.Add(new Mods
                                {
                                    observaciones = v
                                });
                            }
                            dbc.HistoricoGC.Add(nuevo);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        catch (Exception e)
                        {
                            result.Detail = e.Message;
                        }
                    }
                    else
                    {
                        result.Detail = "Sin cambios registrados";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult Rechazar(int id)
        {
            bool flag = false;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Rechazado").Single();
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    cambio.EstadoGC = estadoN;
                    cambio.fecha_fin = DateTime.Now;
                    mods.Add("Se cambió el estado a: " + estadoN.nombre);
                    flag = true;
                    if (flag)
                    {
                        try
                        {
                            HistoricoGC nuevo = new HistoricoGC
                            {
                                id_cambio = id,
                                fecha = DateTime.Now,
                                id_usuario = usuario.IdUsuario
                            };
                            foreach (var v in mods)
                            {
                                nuevo.Mods.Add(new Mods
                                {
                                    observaciones = v
                                });
                            }
                            dbc.HistoricoGC.Add(nuevo);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        catch (Exception e)
                        {
                            result.Detail = e.Message;
                        }
                    }
                    else
                    {
                        result.Detail = "Sin cambios registrados";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult Cancelar(int id)
        {
            bool flag = false;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Cancelado").Single();
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    cambio.EstadoGC = estadoN;
                    cambio.fecha_fin = DateTime.Now;
                    mods.Add("Se cambió el estado a: " + estadoN.nombre);
                    flag = true;
                    if (flag)
                    {
                        try
                        {
                            HistoricoGC nuevo = new HistoricoGC
                            {
                                id_cambio = id,
                                fecha = DateTime.Now,
                                id_usuario = usuario.IdUsuario
                            };
                            foreach (var v in mods)
                            {
                                nuevo.Mods.Add(new Mods
                                {
                                    observaciones = v
                                });
                            }
                            dbc.HistoricoGC.Add(nuevo);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        catch (Exception e)
                        {
                            result.Detail = e.Message;
                        }
                    }
                    else
                    {
                        result.Detail = "Sin cambios registrados";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult Comite(int id)
        {
            bool flag = false;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Pendiente aprobación comité").Single();
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    cambio.EstadoGC = estadoN;
                    mods.Add("Se cambió el estado a: " + estadoN.nombre);
                    flag = true;
                    if (flag)
                    {
                        try
                        {
                            HistoricoGC nuevo = new HistoricoGC
                            {
                                id_cambio = id,
                                fecha = DateTime.Now,
                                id_usuario = usuario.IdUsuario
                            };
                            foreach (var v in mods)
                            {
                                nuevo.Mods.Add(new Mods
                                {
                                    observaciones = v
                                });
                            }
                            dbc.HistoricoGC.Add(nuevo);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        catch (Exception e)
                        {
                            result.Detail = e.Message;
                        }
                    }
                    else
                    {
                        result.Detail = "Sin cambios registrados";
                    }
                }catch(Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult Implementado(int id)
        {
            bool flag = false;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Implementado").Single();
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    cambio.EstadoGC = estadoN;
                    mods.Add("Se cambió el estado a: " + estadoN.nombre);
                    StringBuilder sb = new StringBuilder();
                    foreach (var v in cambio.EcxGc)
                    {
                        sb.Append(v.UnidadConf.nombre).AppendLine();
                    }
                    mods.Add("Los siguientes equipos requieren ser actualizados en la CMDB: ");
                    mods.Add(sb.ToString());
                    flag = true;
                    if (flag)
                    {
                        try
                        {
                            HistoricoGC nuevo = new HistoricoGC
                            {
                                id_cambio = id,
                                fecha = DateTime.Now,
                                id_usuario = usuario.IdUsuario
                            };
                            foreach (var v in mods)
                            {
                                nuevo.Mods.Add(new Mods
                                {
                                    observaciones = v
                                });
                            }
                            dbc.HistoricoGC.Add(nuevo);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        catch (Exception e)
                        {
                            result.Detail = e.Message;
                        }
                    }
                    else
                    {
                        result.Detail = "Sin cambios registrados";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }        
        public JsonResult GetCalendarEvents(string startDate,string endDate)
        {
            List<EventosCalendario> Eventos = new List<EventosCalendario>();
            using (var dbc = new IncidenciasEntities())
            {
                var evs = dbc.GestionCambios.Where(c => c.fecha_fin == null);
                foreach(var d in evs)
                {
                    EventosCalendario c = new EventosCalendario
                    {
                        Asunto = d.asunto,
                        Desc = d.asunto,
                        StartDate = d.fecha_inicio_estimada.ToString("yyyy-MM-dd HH:mm"),
                        EndDate = d.fecha_inicio_estimada.AddHours(d.duracion_estimada).ToString("yyyy-MM-dd HH:mm"),
                        NroCambio = d.id
                    };
                    Eventos.Add(c);
                }
                
            }
            return Json(Eventos);
        }
        public JsonResult Cerrar(int id)
        {
            bool flag = true;
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            List<string> mods = new List<string>();
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {                   
                    var cambio = dbc.GestionCambios.Where(c => c.id == id).Single();
                    //foreach(var u in cambio.EcxGc)
                    //{
                    //    if (u.fecha_actualizacion == null)
                    //    {
                    //        flag = false;
                    //        result.Detail = "El elemento de configuración " + u.UnidadConf.nombre + " tiene que ser actualizado por " + cambio.Usuarios.nombre_completo;
                    //    }
                    //}                   
                    if (flag)
                    {
                        var estadoN = dbc.EstadoGC.Where(c => c.nombre == "Cerrado").Single();
                        cambio.EstadoGC = estadoN;
                        cambio.fecha_fin = DateTime.Now;
                        mods.Add("Fue cerrada la gestión de cambios nro.: " + cambio.id);
                        flag = true;
                        if (flag)
                        {
                            try
                            {
                                HistoricoGC nuevo = new HistoricoGC
                                {
                                    id_cambio = id,
                                    fecha = DateTime.Now,
                                    id_usuario = usuario.IdUsuario
                                };
                                foreach (var v in mods)
                                {
                                    nuevo.Mods.Add(new Mods
                                    {
                                        observaciones = v
                                    });
                                }
                                dbc.HistoricoGC.Add(nuevo);
                                dbc.SaveChanges();
                                result.Info = "ok";
                            }
                            catch (Exception e)
                            {
                                result.Detail = e.Message;
                            }
                        }
                        else
                        {
                            result.Detail = "Sin cambios registrados";
                        }
                    }                    
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public class EventosCalendario
        {
            public int NroCambio { get; set; }
            public string Asunto { get; set; }
            public string Desc { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
        }
    }
}