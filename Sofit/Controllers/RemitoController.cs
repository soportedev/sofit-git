﻿using soporte.Areas.Soporte.Models.Displays;
using Sofit.DataAccess;
using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class RemitoController : Controller
    {
        //
        // GET: /Remito/

        public ActionResult Index(ICollection<int> id)
        {
            List<RemitoVista> remitos = new List<RemitoVista>();            
            usuarioBean usuarioActual=Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                foreach (int idEq in id)
                {
                    var equipo = (from u in dbc.EquiposXIncidencia
                                  where u.id_equipo == idEq
                                  select u).SingleOrDefault();
                    if (equipo != null)
                    {
                        RemitoVista remito = null;
                        string tipoEquipo = equipo.UnidadConf.Tipo_equipo.descripcion;

                        switch (tipoEquipo)
                        {
                            case "Pc": remito = (from u in dbc.Pc
                                                 where u.id_uc == equipo.id_equipo
                                                 select new RemitoVista
                                                 {
                                                     Marca = u.Marca.descripcion,
                                                     Modelo = u.modelo,
                                                     NroSerie = u.UnidadConf.nro_serie,
                                                     NroTicket = equipo.Incidencias.numero,
                                                     Oblea = u.UnidadConf.nombre,
                                                     TipoDispositivo = tipoEquipo,
                                                     UsuarioActual = usuarioActual.NombreCompleto
                                                 }).SingleOrDefault();
                                break;
                            case "Impresora": remito = (from u in dbc.Impresoras
                                                        where u.id_uc == equipo.id_equipo
                                                        select new RemitoVista
                                                        {
                                                            Marca = u.Modelo_Impresora.Marca.descripcion,
                                                            Modelo = u.Modelo_Impresora.descripcion,
                                                            NroSerie = u.UnidadConf.nro_serie,
                                                            NroTicket = equipo.Incidencias.numero,
                                                            Oblea = u.UnidadConf.nombre,
                                                            TipoDispositivo = tipoEquipo,
                                                            UsuarioActual = usuarioActual.NombreCompleto
                                                        }).SingleOrDefault();
                                break;
                            case "Monitor": remito = (from u in dbc.Monitores
                                                      where u.id_uc == equipo.id_equipo
                                                      select new RemitoVista
                                                      {
                                                          Marca = u.Modelo_Monitor.Marca.descripcion,
                                                          Modelo = u.Modelo_Monitor.descripcion,
                                                          NroSerie = u.UnidadConf.nro_serie,
                                                          NroTicket = equipo.Incidencias.numero,
                                                          Oblea = u.UnidadConf.nombre,
                                                          TipoDispositivo = tipoEquipo,
                                                          UsuarioActual = usuarioActual.NombreCompleto
                                                      }).SingleOrDefault();
                                break;
                            case "Varios": remito = (from u in dbc.Varios
                                                     where u.id_uc == equipo.id_equipo
                                                     select new RemitoVista
                                                     {
                                                         Marca = u.Marca.descripcion,
                                                         Modelo = u.modelo,
                                                         NroSerie = u.UnidadConf.nro_serie,
                                                         NroTicket = equipo.Incidencias.numero,
                                                         Oblea = u.UnidadConf.nombre,
                                                         TipoDispositivo = tipoEquipo,
                                                         UsuarioActual = usuarioActual.NombreCompleto
                                                     }).SingleOrDefault();
                                break;
                            default:remito= (from u in dbc.UnidadConf
                                             where u.id_uc == equipo.id_equipo
                                             select new RemitoVista
                                             {
                                                 Marca = "",
                                                 Modelo = "",
                                                 NroSerie = u.nro_serie,
                                                 NroTicket = equipo.Incidencias.numero,
                                                 Oblea = u.nombre,
                                                 TipoDispositivo = tipoEquipo,
                                                 UsuarioActual = usuarioActual.NombreCompleto
                                             }).SingleOrDefault();
                                break;
                        }
                        if(remito!=null) remitos.Add(remito);
                    }
                }//foreach                      
            }           
       
            return View(remitos);
        }

    }
}
