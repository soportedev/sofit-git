﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace soporte.Controllers
{
    public class SessionActionFilter:ActionFilterAttribute
    {        
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            HttpContext ctx = HttpContext.Current;
            // If the browser session has expired...


            if (ctx.Session["usuario"] == null|ctx.Session["clear"]!=null)
            {
                if (actionContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, we're overriding the returned JSON result with a simple string,
                    // indicating to the calling JavaScript code that a redirect should be performed.
                    Responses resp = new Responses();
                    JsonResult result= new JsonResult();
                    result.JsonRequestBehavior=JsonRequestBehavior.AllowGet;
                    result.Data=new Responses() { Info = "Login" };                    
                    actionContext.Result = result;                    
                }
                else
                {
                    // For round-trip posts, we're forcing a redirect to Home/TimeoutRedirect/, which
                    // simply displays a temporary 5 second notification that they have timed out, and
                    // will, in turn, redirect to the logon page.
                    actionContext.Result = new RedirectToRouteResult("Logout",new RouteValueDictionary());                    
                }
            }
           
            else {
                base.OnActionExecuting(actionContext);
            }            
            //base.OnActionExecuting(actionContext);
            //usuarioBean usuario = (usuarioBean)actionContext.HttpContext.Session["usuario"];
            //if (usuario == null)
            //{
            //    actionContext.HttpContext.Response.Redirect("Login", true);
            //}
        }
    }
}