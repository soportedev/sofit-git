﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class NvoIncController : Controller
    {
        //
        // GET: /NvoInc/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult getNroInc()
        {
            Responses result = new Responses();
            result = IncidenciasManager.getNextNro();
            result.Detail = "CBA" + result.Detail;
            return Json(result);
        }
    }
}
