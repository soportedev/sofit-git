﻿using soporte.Areas.Telecomunicaciones.Models;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Collections.Specialized;
using soporte.Models.Helpers;
using Sofit.Models.Helpers;

namespace soporte.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {
        //
        // GET: /Config/
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
        public ActionResult DatosPersonales()
        {
            return View(usuario);
        }
        public ActionResult Index()
        {
            return View("Config");
        }
        public JsonResult EditServicio(int id, string nombre, int dias, int horas, int diasM, int horasM, bool habilitado)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            using (var dbc = new IncidenciasEntities())
            {
                var serv = dbc.Servicios.Where(c => c.id_servicio == id).FirstOrDefault();
                serv.nombre = nombre;
                serv.tiempo_res = (int)totales.TotalHours;
                serv.tiempo_margen = (int)totalesMargen.TotalHours;
                serv.vigente = habilitado;
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public JsonResult getServicio(int id)
        {
            ServiciosVista servicio = null;
            using (var dbc = new IncidenciasEntities())
            {
                servicio = (from u in dbc.Servicios
                            where u.id_servicio == id
                            select new ServiciosVista
                            {
                                Id = u.id_servicio,
                                TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                Nombre = u.nombre,
                                Vigente = u.vigente
                            }).Single();
            }
            return Json(servicio);
        }
        public JsonResult recreateWindows()
        {
            Responses result = new Responses();
            usuarioBean usuario = ControllerContext.HttpContext.Session["usuario"] as usuarioBean;
            usuario.recreateWindows();
            return Json(result);
        }
        public JsonResult toggleStatus(int idUsuario)
        {
            Responses result = usuario.handler.togleStatusDelUsuario(idUsuario);
            return Json(result);
        }
        public JsonResult setPassword(string pass, int userid)
        {
            Responses result = usuario.handler.setPasswordFor(userid, pass);
            return Json(result);
        }
        public ActionResult Tiempos()
        {
            TiemposTelecomunicaciones model = new TiemposTelecomunicaciones();
            return PartialView(model);
        }
        public JsonResult setTiempos()
        {
            Responses result = new Responses();
            return Json(result);
        }
        public JsonResult setLogin(string idUser, string login)
        {
            usuarioBean usuario = HttpContext.Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            int idUsuario = Int32.Parse(idUser);
            result = usuario.handler.setLoginToThis(idUsuario, login);

            return Json(result);
        }
        public JsonResult quitarPerfilAlUsuario(int idUsuario, string perfil)
        {
            Responses result = usuario.handler.QuitarPerfilAlUsuario(idUsuario, perfil);
            return Json(result);
        }
        public JsonResult AgregarPerfilAUsuario(int idUsuario, int perfil)
        {
            Responses result = usuario.handler.AgregarPerfilAlUsuario(idUsuario, perfil);
            return Json(result);
        }
        public JsonResult setMyFirma(HttpPostedFileBase photo)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            string firma = nvc.Get("firma");
            string email = nvc.Get("email");
            bool hayImagen = photo != null;
            string nombreFile = string.Empty;
            string pathImagen = string.Empty;
            #region foto
            if (hayImagen)
            {
                try
                {
                    if (usuario.Avatar != null & usuario.Avatar != "")
                    {
                        string path = PathImage.getCustomImagenDefault(usuario.Avatar);
                        System.IO.File.Delete(path);
                    }
                    string nombreArchivo = photo.FileName;
                    string[] splitN = nombreArchivo.Split('.');
                    string ran = new Random().Next(10000).ToString("0000");
                    nombreFile = splitN.First() + ran + '.' + splitN.Last();

                    pathImagen = PathImage.getCustomImagenDefault(nombreFile);
                    var stream = photo.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion
            if (hayImagen & result.Info == "ok")
            {
                usuario.Avatar = nombreFile;
            }
            usuario.Firma = firma;
            usuario.email = email;
            result = usuario.updateFirma();
            return Json(result);
        }
        public JsonResult setMyPassword(string oldPassword, string newPassword)
        {
            Responses result = new Responses();
            usuarioBean usuario = HttpContext.Session["usuario"] as usuarioBean;
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "gobiernocba.gov.ar", usuario.Nombre, oldPassword))
                {
                    UserPrincipal up = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, usuario.Nombre);
                    if (up != null)
                    {
                        up.ChangePassword(oldPassword, newPassword);
                        up.Save();
                        result.Info = "ok";
                        result.Detail = "El password fue cambiado!!";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult getTipificacionesHijo(int id)
        {
            Responses result = new Responses();
            List<Tipificaciones> hijos = null;
            List<TipificacVista> tipVista = new List<TipificacVista>();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    hijos = (from u in dbc.Tipificaciones
                             where u.id_padre == id
                             select u).ToList();
                    foreach (Tipificaciones t in hijos)
                    {
                        tipVista.Add(new TipificacVista(t));
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
                return Json(result);
            }
            return Json(tipVista);
        }
        public JsonResult setTheme(string theme)
        {
            usuarioBean usuario = HttpContext.Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            int tema = Int32.Parse(theme);
            result = usuario.setTheme(tema);
            return Json(result);
        }
        public JsonResult getReferentesXJur(int idJur)
        {
            List<ReferentesVista> refer = new List<ReferentesVista>();
            List<Referentes> re = new List<Referentes>();
            using (var dbc = new IncidenciasEntities())
            {
                re = (from r in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                      where r.Jurisdiccion.Any(s => s.id_jurisdiccion == idJur)
                      select r).ToList();
                re.AddRange((from s in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                             where s.Dependencia.Any(o => o.id_jurisdiccion == idJur)
                             select s).ToList());
            }
            foreach (Referentes r in re)
            {
                ReferentesVista rm = new ReferentesVista();
                rm.id = r.id;
                rm.nombre = r.nombre;
                rm.email = r.email;
                rm.te = r.te;
                foreach (Dependencia dep in r.Dependencia)
                {
                    LocationVista l = new LocationVista() { id = dep.id_dependencia, nombre = dep.descripcion, tipo = "dependencia" };
                    rm.location.Add(l);
                }
                foreach (Jurisdiccion jur in r.Jurisdiccion)
                {
                    LocationVista l = new LocationVista() { id = jur.id_jurisdiccion, nombre = jur.descripcion, tipo = "jurisdiccion" };
                    rm.location.Add(l);
                }
                refer.Add(rm);
            }
            return Json(refer);
        }
        public JsonResult getReferentesXIdEquipo(int idEquipo)
        {
            List<ReferentesVista> refer = new List<ReferentesVista>();
            List<Referentes> re = new List<Referentes>();
            using (var dbc = new IncidenciasEntities())
            {
                string tipoequipo = dbc.UnidadConf.Where(c => c.id_uc == idEquipo).First().Tipo_equipo.descripcion;
                switch (tipoequipo)
                {
                    case "Pc":
                        ;
                        break;
                    case "Monitor":
                        ;
                        break;
                    case "Impresora":
                        ;
                        break;
                }
                var equi = (from u in dbc.UnidadConf where u.id_uc == idEquipo select u).SingleOrDefault();
                re = (from r in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                      where r.Dependencia.Any(s => s.id_dependencia == equi.id_jurisdiccion)
                      select r).ToList();
                re.AddRange((from s in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                             where s.Jurisdiccion.Any(c => c.id_jurisdiccion == equi.id_jurisdiccion)
                             //where s.Jurisdiccion.Any(o => o.Dependencia.Any(p => p.id_dependencia == equi.id_dependencia))
                             select s).ToList());
            }
            foreach (Referentes r in re)
            {
                ReferentesVista rm = new ReferentesVista();
                rm.id = r.id;
                rm.nombre = r.nombre;
                rm.email = r.email;
                rm.te = r.te;
                foreach (Dependencia dep in r.Dependencia)
                {
                    LocationVista l = new LocationVista() { id = dep.id_dependencia, nombre = dep.descripcion, tipo = "dependencia" };
                    rm.location.Add(l);
                }
                foreach (Jurisdiccion jur in r.Jurisdiccion)
                {
                    LocationVista l = new LocationVista() { id = jur.id_jurisdiccion, nombre = jur.descripcion, tipo = "jurisdiccion" };
                    rm.location.Add(l);
                }
                refer.Add(rm);
            }
            return Json(refer);
        }
        public JsonResult getReferentesXDep(int idDep)
        {
            List<Referentes> refer = new List<Referentes>();
            using (var dbc = new IncidenciasEntities())
            {
                refer = (from r in dbc.Referentes
                         where r.Dependencia.Any(s => s.id_dependencia == idDep)
                         select r).ToList();

            }
            return Json(refer);
        }
        [HttpPost]
        public JsonResult getReferentesXNombre(string prefix)
        {
            List<ReferentesVista> refer = new List<ReferentesVista>();
            using (var dbc = new IncidenciasEntities())
            {
                refer = (from r in dbc.Referentes
                         where r.nombre.IndexOf(prefix) >= 0
                         select new ReferentesVista
                         {
                             id = r.id,
                             nombre = r.nombre,
                             dni = r.dni,
                             Direccion = r.direccion,
                             email = r.email,
                             te = r.te,
                             Jurisdicciones = r.Jurisdiccion.Select(c => new JurisdiccionVista
                             {
                                 idJurisdiccion = c.id_jurisdiccion,
                                 Nombre = c.descripcion
                             }).ToList(),
                             Dependencias = r.Dependencia.Select(c => new DependenciaVista
                             {
                                 idDependencia = c.id_dependencia,
                                 Nombre = c.descripcion,
                                 Jurisdiccion = new JurisdiccionVista
                                 {
                                     idJurisdiccion = c.Jurisdiccion.id_jurisdiccion,
                                     Nombre = c.Jurisdiccion.descripcion
                                 }
                             }).ToList()

                         }).Take(5).ToList();

            }
            return Json(refer);
        }
        [HttpPost]
        public JsonResult getAdCuit(string prefix)
        {            
            List<ReferentesVista> refer = new List<ReferentesVista>();
            DirectoryEntry ent = null;
            DirectoryEntry entrhc = null;
            DirectorySearcher searcher = null;
            SearchResultCollection re = null;
            if (prefix.Length > 5)
            {
                try
                {
                    usuarioBean usuario = Session["usuario"] as usuarioBean;
                    if (GlobalVar.DEBUG)
                    {
                        ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuario.Nombre, usuario.Password);
                        entrhc = new DirectoryEntry("LDAP://rhc.gov.ar", "gobiernocba\\"+usuario.Nombre, usuario.Password);
                    }
                    else
                    {
                        ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuario.Nombre, usuario.Password);
                        entrhc = new DirectoryEntry("LDAP://rhc.gov.ar", "gobiernocba\\" + usuario.Nombre, usuario.Password);
                    }
                    object nativeObj = ent.NativeObject;

                    searcher = new DirectorySearcher(ent);
                    searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + prefix + "*))";
                    re = searcher.FindAll();
                    if (re.Count > 0)
                    {
                        for (int i = 0; i < 5 & i < re.Count; i++)
                        {
                            ReferentesVista nu = new ReferentesVista();
                            nu.Dominio = "Gobiernocba";
                            if (re[i].Properties["samaccountname"].Count > 0)
                            {
                                nu.nombre = re[i].Properties["samaccountname"][0].ToString();
                            }
                            else
                            {
                                nu.nombre = "Sin samaccountname";
                            }
                            if (re[i].Properties["mail"].Count > 0)
                            {
                                nu.email = re[i].Properties["mail"][0].ToString();
                            }
                            else
                            {
                                nu.email = "Sin mail cargado";
                            }
                            if (re[i].Properties["displayname"].Count > 0)
                            {
                                nu.displayname = re[i].Properties["displayname"][0].ToString();
                            }
                            else
                            {
                                nu.nombre = "Sin displayname";
                            }
                            //nu.Dependencias = new List<DependenciaVista>();
                            //nu.Jurisdicciones = new List<JurisdiccionVista>();
                            refer.Add(nu);
                        }
                    }
                    else
                    {
                        searcher = new DirectorySearcher(entrhc);
                        searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + prefix + "*))";
                        re = searcher.FindAll();
                        if (re.Count > 0)
                        {
                            for (int i = 0; i < 5 & i < re.Count; i++)
                            {
                                ReferentesVista nu = new ReferentesVista();
                                nu.Dominio = "Rhc";
                                if (re[i].Properties["samaccountname"].Count > 0)
                                {
                                    nu.nombre = re[i].Properties["samaccountname"][0].ToString();
                                }
                                else
                                {
                                    nu.nombre = "Sin samaccountname";
                                }
                                if (re[i].Properties["mail"].Count > 0)
                                {
                                    nu.email = re[i].Properties["mail"][0].ToString();
                                }
                                else
                                {
                                    nu.email = "Sin mail cargado";
                                }
                                if (re[i].Properties["displayname"].Count > 0)
                                {
                                    nu.displayname = re[i].Properties["displayname"][0].ToString();
                                }
                                else
                                {
                                    nu.nombre = "Sin displayname";
                                }
                                //nu.Dependencias = new List<DependenciaVista>();
                                //nu.Jurisdicciones = new List<JurisdiccionVista>();
                                refer.Add(nu);
                            }
                        }
                    }
                }
                catch (DirectoryServicesCOMException ex)
                {
                    string mess = ex.Message;

                }
                finally
                {
                    if (ent != null) ent.Dispose();
                    if (entrhc != null) entrhc.Dispose();
                    if (searcher != null) searcher.Dispose();
                }
            }
            return Json(refer);
        }
        [HttpPost]
        public JsonResult getAdNombres(string prefix)
        {            
            List<ReferentesVista> refer = new List<ReferentesVista>();
            DirectoryEntry ent = null;
            DirectorySearcher searcher = null;
            SearchResultCollection re = null;            
            if (prefix.Length > 5)
            {
                try
                {
                    usuarioBean usuario = Session["usuario"] as usuarioBean;
                    if (GlobalVar.DEBUG)
                    {
                        ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuario.Nombre, usuario.Password);
                    }
                    else ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuario.Nombre, usuario.Password);
                    object nativeObj = ent.NativeObject;

                    searcher = new DirectorySearcher(ent);

                    searcher.Filter = "(&(objectClass=user)(displayname=" + prefix + "*))";
                    re = searcher.FindAll();
                    if (re.Count > 0)
                    {                        
                        for (int i = 0; i < 5 & i <re.Count; i++)
                        {
                            ReferentesVista nu = new ReferentesVista();
                            if (re[i].Properties["samaccountname"].Count > 0)
                            {
                                nu.nombre = re[i].Properties["samaccountname"][0].ToString();
                            }
                            else
                            {
                                nu.nombre = "Sin samaccountname";
                            }
                            if (re[i].Properties["mail"].Count > 0)
                            {
                                nu.email = re[i].Properties["mail"][0].ToString();
                            }
                            else
                            {
                                nu.email = "Sin mail cargado";
                            }
                            if (re[i].Properties["displayname"].Count > 0)
                            {
                                nu.displayname = re[i].Properties["displayname"][0].ToString();
                            }
                            else
                            {
                                nu.nombre = "Sin displayname";
                            }
                            nu.Dependencias = new List<DependenciaVista>();
                            nu.Jurisdicciones = new List<JurisdiccionVista>();
                            refer.Add(nu);
                        }
                    }
                    if (refer.Count < 5)
                    {
                        ent = new DirectoryEntry("LDAP://rhc.gov.ar", "20250451408", "Cs059999");
                        searcher = new DirectorySearcher(ent);
                        searcher.Filter = "(&(objectClass=user)(displayname=" + prefix + "*))";
                        re = searcher.FindAll();
                        if (re.Count > 0)
                        {
                            for (int i = 0; i < 5 & i < re.Count; i++)
                            {
                                ReferentesVista nu = new ReferentesVista();
                                if (re[i].Properties["samaccountname"].Count > 0)
                                {
                                    nu.nombre = re[i].Properties["samaccountname"][0].ToString();
                                }
                                else
                                {
                                    nu.nombre = "Sin samaccountname";
                                }
                                if (re[i].Properties["mail"].Count > 0)
                                {
                                    nu.email = re[i].Properties["mail"][0].ToString();
                                }
                                else
                                {
                                    nu.email = "Sin mail cargado";
                                }
                                if (re[i].Properties["displayname"].Count > 0)
                                {
                                    nu.displayname = re[i].Properties["displayname"][0].ToString();
                                }
                                else
                                {
                                    nu.nombre = "Sin displayname";
                                }
                                nu.Dependencias = new List<DependenciaVista>();
                                nu.Jurisdicciones = new List<JurisdiccionVista>();
                                refer.Add(nu);
                            }
                        }
                    }
                }
                catch (DirectoryServicesCOMException ex)
                {
                    string mess = ex.Message;
                    
                }
                finally
                {
                    if (ent != null) ent.Dispose();
                    if (searcher != null) searcher.Dispose();
                }
            }
            return Json(refer);
        }
    }
}
