﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class PrintController : Controller
    {
        //
        // GET: /Print/

        public ActionResult Index()
        {
            string area=Request.QueryString["area"];
            ViewBag.Direccion = area;
            return View("Print");
        }

    }
}
