﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Controllers
{
    public class HtmlFormater
    {
        #region inf.Mensual
        internal static string toHtmlInformeMensual(Models.ClasesVistas.TicketsMensuales t)
        {
            StringBuilder sb = new StringBuilder();
            if (t != null)
            {
                sb.Append("<table class='info'>").Append("<thead><tr class='ui-state-active'>").
                   Append("<th colspan='9'>Totales Tickets " + t.PrioridadTicket + " del mes " + t.Mes +
                   " del año " + t.Anio + "</th></tr>").
                   Append("<tr class='ui-state-active'><th>Total Generados</th>").
                   Append("<th>Totales Específicos</th><th>Porcentaje Específico</th>").
                   Append("<th>En término</th><th>En el Umbral</th><th>Fuera del Umbral</th>").
                   Append("<th>Sin Cerrar</th><").
                   Append("</tr></thead>");
                sb.Append("<tbody><tr>");
               // sb.Append("<td>" + "70%" + "</td>");
                //sb.Append("<td>" + t.PorcObtenido + " %</td>");
                sb.Append("<td>" + t.TotalGenerados + "</td>");
                sb.Append("<td>" + t.TotalesFiltrados + "</td>");
                sb.Append("<td>" + t.PorcSobreTotal + " %</td>");
                sb.Append("<td>" + t.DentroDelTiempo + "</td>");
                sb.Append("<td>" + t.DentroDelMargen + "</td>");
                sb.Append("<td>" + t.FueraMargen + "</td>");
                sb.Append("<td>" + t.SinCerrar + "</td>");
                sb.Append("</tr></tbody>");
                sb.Append("</table>");
            }
            else
            {
                sb.Append("<table class='info'>").Append("<thead><tr class='ui-state-active'>").
                   Append("<th colspan='7'>Sin resultados para las condiciones actuales</th>").
                   Append("</tr></thead>");
            }
            return sb.ToString();
        }        

        internal static string toHtmlInformeMensualXJur(List<TicketsCreados> tickets)
        {
            StringBuilder sb = new StringBuilder();
            if (tickets.Count>0)
            {
                sb.Append("<table class='info'>").Append("<thead><tr class='ui-state-active'>").
                       Append("<th colspan='10'>Totales Tickets por prioridad del mes " + tickets.First().Mes +
                       " del año " + tickets.First().Anio + " de la Jurisdicción "+tickets.First().Jurisdiccion +" sobre un total de tickets de: "+tickets.First().Cantidad+"</th></tr>").
                       Append("<tr class='ui-state-active'><th>Prioridad</th>").
                       Append("<th>Objetivo</th><th>Obtenido</th>").
                       Append("<th>Tot. Jurisdicción</th><th>Tot. Específicos</th><th>Porcentaje Específico</th><th>En término</th>").
                       Append("<th>En el Umbral</th><th>Fuera del Umbral</th><th>Sin Cerrar</th>").
                       Append("</tr></thead>");
                sb.Append("<tbody><tr>");
                foreach (TicketsCreados t in tickets)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + t.PrioridadTicket + "</td>");                    
                    sb.Append("<td>" + t.Objetivo + "</td>");
                    sb.Append("<td>" + t.PorcObtenido + "%</td>");
                    sb.Append("<td>" + t.CantidadJurisdiccion + "</td>");
                    sb.Append("<td>" + t.CantidadEspecifica + "</td>");
                    sb.Append("<td>" + t.PorcentajeEspecifico + "%</td>");
                    sb.Append("<td>" + t.DentroDelTiempo + "</td>");
                    sb.Append("<td>" + t.DentroDelMargen + "</td>");
                    sb.Append("<td>" + t.FueraMargen + "</td>");                    
                    sb.Append("<td>" + t.SinCerrar + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</tr></tbody>");
                sb.Append("</table>");
            }
            else
            {
                sb.Append("<table class='info'>").Append("<thead><tr class='ui-state-active'>").
                   Append("<th colspan='7'>Sin resultados para las condiciones actuales</th>").
                   Append("</tr></thead>");
            }
            return sb.ToString();
        }
        #endregion
        internal static string toHtmlListado(List<Models.ClasesVistas.TicketsQuery> tickets)
        {
            StringBuilder sb = new StringBuilder();
            if (tickets.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'>").Append("<thead><tr class='ui-state-active'>").
                    Append("<th colspan='7'>Se encontraron " + tickets.Count + " Tickets que cumplen el criterio</th></tr>").
                    Append("<tr class='ui-state-active'><th>Fecha</th><th>Nombre</th><th>Tipo de Ticket</th>" +
                    "<th>Prioridad</th><th>Fecha Cierre</th><th>Dentro del tiempo?</th><th>Dentro del Márgen?</th></tr></thead>");
                sb.Append("<tbody>");
                foreach (TicketsQuery j in tickets)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.FechaGeneracion + "</td>");
                    sb.Append("<td class='verIncidente'>" + j.Numero + "</td>");
                    sb.Append("<td>" + j.TipoTicket + "</td>");
                    sb.Append("<td>" + j.PrioridadTicket + "</td>");
                    sb.Append("<td>" + j.FechaCierreStr + "</td>");
                    sb.Append("<td>" + (j.DentroDelTiempo?"Si": "<span class='pRight12'>No</span><i class='fa f13 fa-exclamation-circle aria-hidden=true'></i>") + "</td>");//< span class='pRight12'>No</span><i class='fa f13 fa-exclamation-circle aria-hidden=true'></i>
                    sb.Append("<td>" + (j.DentroDelMargen?"Si": "<span class='pRight12'>No</span><i class='fa f13 fa-exclamation-circle aria-hidden=true'></i>") + "</td>");

                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
            }
            return sb.ToString();
        }
        #region Tickets Creados Y Cerrados(Totales)
        internal static string toHtmlTotalesCerradosGeneralTotales(List<TicketsCreados> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'>" +
                    "<th>Tipo de Ticket</th><th>Prioridad</th><th>Creados</th><th>Cerrados en término</th>" +
                    "<th>Umbral 1</th><th>Umbral 2</th><th>Sin Cerrar</th></tr>");
                foreach (TicketsCreados j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.TipoTicket + "</td>");
                    sb.Append("<td>" + j.PrioridadTicket + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("<td>" + j.DentroDelTiempo + "<span> (" + j.DentroDelTiempoPorc + " %)</span></td>");
                    sb.Append("<td>" + j.DentroDelMargen + "<span> (" + j.DentroDelMargenPorc + " %)</span></td>");
                    sb.Append("<td>" + j.FueraMargen + "<span> (" + j.FueraDelMargenPorc + " %)</span></td>");
                    sb.Append("<td>" + j.SinCerrar + "<span> (" + j.SinCerrarPorc + " %)</span></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }

        internal static string toHtmlTotalesCerradosTotales(List<TicketsCreados> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Subdirección Generadora</th>" +
                    "<th>Tipo de Ticket</th><th>Prioridad</th><th>Creados</th><th>Cerrados en término</th>" +
                    "<th>Umbral 1</th><th>Umbral 2</th><th>Sin Cerrar</th></tr>");
                foreach (TicketsCreados j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.SubDirGeneradora + "</td>");
                    sb.Append("<td>" + j.TipoTicket + "</td>");
                    sb.Append("<td>" + j.PrioridadTicket + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("<td>" + j.DentroDelTiempo + "<span> (" + j.DentroDelTiempoPorc + " %)</span></td>");
                    sb.Append("<td>" + j.DentroDelMargen + "<span> (" + j.DentroDelMargenPorc + " %)</span></td>");
                    sb.Append("<td>" + j.FueraMargen + "<span> (" + j.FueraDelMargenPorc + " %)</span></td>");
                    sb.Append("<td>" + j.SinCerrar + "<span> (" + j.SinCerrarPorc + " %)</span></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        #endregion

       
    }
}