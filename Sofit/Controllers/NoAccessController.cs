﻿using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class NoAccessController : Controller
    {
        //
        // GET: /NoAccess/

        public ActionResult Index()
        {
            ViewBag.home = (HttpContext.Session["usuario"] as usuarioBean).getHomePage();
            return View();
        }

    }
}
