﻿using Sofit.DataAccess;
using Sofit.Models.Helpers;
using Sofit.Models.Reglas;
using soporte.Areas.Direccion.Models;
using soporte.Areas.Seguridad.Models;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Reglas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    [SessionActionFilter]
    public class Servicio8Controller : Controller
    {
        // GET: Servicio8
        public ActionResult Index(string startIndex)
        {
            HttpRequestBase req = Request;
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            bool esAdmin = false;
            bool esOperaciones = false;
            int PageSize = 10;
            //busqueda por nro
            string nroPedido = Request["nro"];
            int nroIntPedido = 0;
            Int32.TryParse(nroPedido, out nroIntPedido);
            //-->
            //busqueda por estados
            string estado = Request["es"];
            int idEstado = 0;
            Int32.TryParse(estado, out idEstado);
            //-->
            //busqueda por palabra clave
            string palabra = Request["palabra"];
            //-->
            //if (GlobalVar.DEBUG)
            //{
            //    usuarioActual = null;
            //}
            try
            {
                if (usuarioActual.Direccion.Nombre == "Operaciones") esOperaciones = true;
                if (usuarioActual.Direccion.Nombre == "Seguridad Informática") esAdmin = true;
                if (usuarioActual.Perfiles.Contains(new PerfilDireccion { nombre = "Dirección" })) esAdmin = true;
                if (usuarioActual.Perfiles.Contains(new PerfilDireccion { nombre = "Administrador Gral." })) esAdmin = true;
            }catch(Exception e)
            {
                return RedirectToRoute("Logout");
            }
            ReglasModel modelo = getModel(nroIntPedido, idEstado, esAdmin, esOperaciones, usuarioActual,palabra);
            int cantidadPedidos = modelo.MisPedidos.Count;
            double cantPagPedidos = (double)cantidadPedidos / (double)PageSize;
            double cantidadPagPedidos = Math.Ceiling(cantPagPedidos);
            modelo.CantidadPaginasPedidos = Convert.ToInt32(cantidadPagPedidos);
            if (esOperaciones)
            {
                modelo.esOperaciones = true;
            }
            if (esAdmin)
            {
                modelo.esAdmin = true;
            }
            if (startIndex != null)
            {
                int stI = 0;
                Int32.TryParse(startIndex, out stI);
                int skip = 0;
                if (stI != 1)
                {
                    skip = (stI - 1) * PageSize;
                }
                var pedidos = modelo.MisPedidos.Skip(skip).Take(PageSize);
                modelo.PaginaActualPedidos = stI;
                modelo.MisPedidos = pedidos.ToList();
            }
            else
            {
                var pedidos = modelo.MisPedidos.Take(PageSize);
                var tickets = modelo.MisTickets.Take(PageSize);
                modelo.PaginaActualPedidos = 1;
                modelo.PaginaActualTickets = 1;
                modelo.MisPedidos = pedidos.ToList();
                modelo.MisTickets = tickets.ToList();
            }

            return View(modelo);
        }
        private ReglasModel getModel(int nroPedido, int idEstado, bool esAdmin, bool esOperaciones, usuarioBean logueado,string palabra)
        {
            ReglasModel modelo = new ReglasModel();
            List<JurisdiccionVista> jur = null;
            List<UsuarioVista> aut = null;
            List<PedidosVista> pedidos = new List<PedidosVista>();
            IEnumerable<PedidosSeguridad> pedidosBase = null;
            using (var dbc = new IncidenciasEntities())
            {
                if (nroPedido != 0)
                {
                    pedidosBase = dbc.PedidosSeguridad.Where(c => c.id == nroPedido);
                }
                else
                {
                    if (idEstado != 0)
                    {
                        pedidosBase = dbc.PedidosSeguridad.Where(c => c.id_estado == idEstado);
                    }
                    else
                    {
                        if (palabra!=null && palabra != string.Empty)
                        {
                            Regex ip = new Regex(@"\d{1,3}\.");
                            Regex nro = new Regex(@"^\d{1,5}$");
                            if (ip.Match(palabra).Success)
                            {
                                pedidosBase = dbc.PedidosSeguridad.Where(c => c.ReglaPedido.Any(d => d.ExtremoPedido.ip_address.Contains(palabra)) | c.ReglaPedido.Any(f => f.ExtremoPedido1.ip_address.Contains(palabra)));
                            }
                            else
                            {
                                if (nro.Match(palabra).Success)
                                {
                                    int nrop = Int32.Parse(palabra);
                                    pedidosBase = dbc.PedidosSeguridad.Where(c => c.id == nrop);
                                }
                                if (pedidosBase != null) pedidosBase.Union(dbc.PedidosSeguridad.Where(c => c.descripcion.Contains(palabra) | c.asunto.Contains(palabra)));
                                else pedidosBase = dbc.PedidosSeguridad.Where(c => c.descripcion.Contains(palabra) | c.asunto.Contains(palabra));
                            }
                        }
                        else
                        {
                            pedidosBase = dbc.PedidosSeguridad.Where(c => c.fecha_fin == null);
                        }
                    }
                }
                foreach (var d in pedidosBase)
                {
                    PedidosVista p = new PedidosVista
                    {
                        esOperaciones = esOperaciones,
                        Asunto = d.asunto,
                        Descripcion = d.descripcion,
                        Autorizante = d.autorizante != null ? new UsuarioVista { id = d.Usuarios1.id_usuario, nombre = d.Usuarios1.nombre, displayName = d.Usuarios1.nombre_completo } : null,
                        Solicitante = new UsuarioVista { id = d.Usuarios2.id_usuario, nombre = d.Usuarios2.nombre, displayName = d.Usuarios2.nombre_completo },
                        Aprobador = d.aprobador.HasValue ? new UsuarioVista { id = d.Usuarios.id_usuario, nombre = d.Usuarios.nombre, displayName = d.Usuarios.nombre_completo } : null,
                        idEstado = d.id_estado,
                        FechaPedidoDT = d.fecha,
                        idPedido = d.id,
                        DiasVencimiento = d.vencimiento,
                        MiNombre = logueado.Nombre,
                        Jurisdiccion = d.Dependencia.Jurisdiccion.descripcion,
                        Dependencia = d.Dependencia.descripcion,
                        Ticket = d.id_ticket.HasValue ? new TicketVista { Id = d.id_ticket.Value, Numero = d.Incidencias.numero } : null,
                        TipoPedido = d.TipoPedido.nombre,
                        OrigenAcceso = d.tipoOrigenAcceso,
                        UsuarioAcceso = d.usuario_acceso.HasValue ? new UsuarioVista { nombre = d.Usuarios3.nombre, displayName = d.Usuarios3.nombre_completo, dominio = d.Usuarios3.dominio, id = d.usuario_acceso.Value } : null,
                        HostAcceso = d.host_acceso.HasValue ? new HostPedido { Nombre = d.UnidadConf.nombre, Ip = d.ip_host_acceso } : null,
                        DetalleAcceso = d.detalleAcceso,
                        TipoAcceso = d.id_tipo_acceso.HasValue ? d.Tipo_Acceso.nombre : null,
                        DetalleEstados = dbc.EstadosXPedido.Where(c => c.id_pedido == d.id).OrderBy(e => e.fecha_desde).Select(fd => new EstadosPedVista
                        {
                            Estado = fd.EstadosPedidos.nombre,
                            fdesde = fd.fecha_desde,
                            fhasta = fd.fecha_hasta.Value,
                            Observaciones = fd.observaciones,
                            Tecnico = fd.nombreTecnico
                        }).ToList()
                    };
                    //determinar el rol
                    if (esAdmin)
                    {
                        p.MiRol = "Administrador";
                    }
                    else
                    {
                        if (esOperaciones)
                        {
                            p.MiRol = "Operaciones";
                        }
                        else
                        {
                            if (d.solicitante == logueado.IdUsuario) p.MiRol = "Solicitante";
                            if (d.autorizante != null & d.autorizante == logueado.IdUsuario) p.MiRol = "Autorizante";
                            if (d.aprobador != null & d.aprobador == logueado.IdUsuario) p.MiRol = "Aprobador";
                        }
                    }
                    if (d.TipoPedido.nombre == "Regla de Acceso")
                    {
                        p.Reglas = new List<GrupoReglas>();
                        var rules = d.ReglaPedido;
                        foreach (var v in rules)
                        {
                            GrupoReglas gr = new GrupoReglas();
                            gr.Usuario = new UsuarioVista
                            {
                                id = v.Usuarios.id_usuario,
                                nombre = v.Usuarios.nombre,
                                displayName = v.Usuarios.nombre_completo,
                                dominio = v.dominio_usuario,
                                email = v.Usuarios.email
                            };
                            gr.Origen = new ExtremosPedido
                            {
                                Tipo = v.ExtremoPedido.TipoEcPedido.nombre,
                                Ip = v.ExtremoPedido.ip_address,
                                Nombre = v.ExtremoPedido.id_uc.HasValue ? v.ExtremoPedido.UnidadConf.nombre : "N/A"
                            };
                            gr.Destino = new ExtremosPedido
                            {
                                Tipo = v.ExtremoPedido1.TipoEcPedido.nombre,
                                Ip = v.ExtremoPedido1.ip_address,
                                Nombre = v.ExtremoPedido1.id_uc.HasValue ? v.ExtremoPedido1.UnidadConf.nombre : "N/A"
                            };
                            p.Reglas.Add(gr);
                        }
                        p.puertos = d.PuertosXPedido.Select(c => c.puerto.ToString()).ToList();
                        p.protocolos = d.ProtocolosXPedido.Select(c => c.protocolo).ToList();
                    }
                    if (d.TipoPedido.nombre == "Acceso Remoto")
                    {

                    }
                    pedidos.Add(p);
                }
                List<TicketVista> tickets = dbc.Incidencias.Where(c => c.Contactos.dni == logueado.Nombre).Select(d => new TicketVista
                {
                    Id = d.id_incidencias,
                    Numero = d.numero,
                    Estado = d.fecha_fin.HasValue ? "Cerrado" : "Activo",
                    Fecha = d.fecha_inicio
                }).ToList();
                modelo.Estados = dbc.EstadosPedidos.Select(c => new EstadoPedidoVista
                {
                    Id = c.id,
                    Nombre = c.nombre
                }).ToList();
                tickets.Sort();
                modelo.MisTickets = tickets;
                pedidos.Sort();
                modelo.Prioridad = dbc.PrioridadTicket.Select(c => new PrioridadesTicketVista { Id = c.id, Nombre = c.nombre }).ToList();
                aut = dbc.Usuarios.Where(c => c.Roles.Any(d => d.Nombre == "Autorizante Reglas")).OrderBy(c => c.nombre_completo).Select(c => new UsuarioVista { id = c.id_usuario, nombre = c.nombre_completo }).ToList();
                jur = dbc.Jurisdiccion.Select(c => new JurisdiccionVista { idJurisdiccion = c.id_jurisdiccion, Nombre = c.descripcion }).OrderBy(d => d.Nombre).ToList();
                modelo.TiposAcceso = dbc.Tipo_Acceso.Select(c => new Sofit.Models.Reglas.TipoAccesoVista { Id = c.id, Nombre = c.nombre }).ToList();
            }

            modelo.Autorizantes = aut;
            modelo.Jurisdicciones = jur;
            modelo.MisPedidos = pedidos;
            return modelo;
        }
        public JsonResult Cancelar(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.cancelar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Aprobar(int idPedido, string nombreUsuario, string obs, int prioridad)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            p.Prioridad = prioridad;
            try
            {
                result = p.EstadoActual.aprobar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Rechazar(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.rechazar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult PedirAutorizacion(int idPedido, string nombreUsuario, string obs, string autorizante2)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            int idAprobador = Int32.Parse(autorizante2);
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var pedido = dbc.PedidosSeguridad.Where(c => c.id == idPedido).Single();
                    pedido.aprobador = idAprobador;
                    dbc.SaveChanges();
                    p.Aprobador = new UsuarioVista
                    {
                        id = pedido.autorizante.Value,
                        displayName = pedido.Usuarios.nombre_completo,
                        email = pedido.Usuarios.email,
                        nombre = pedido.Usuarios.nombre
                    };                   
                    result.Info = "ok";
                }
                if (result.Info == "ok")
                {

                    result = p.EstadoActual.Autorizar(nombreUsuario, obs);

                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Autorizado(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);            
            try
            {
                result = p.EstadoActual.aprobar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Realizado(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.realizado(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult ParaAprobar(int idPedido, string nombreUsuario, string obs, string autorizante2, string tipoAcceso)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            
            int idaut = Int32.Parse(autorizante2);
            int tipoacceso = Int32.Parse(tipoAcceso);
            try
            {
                using(var dbc=new IncidenciasEntities())
                {
                    var pedido = dbc.PedidosSeguridad.Where(c => c.id == idPedido).Single();
                    pedido.autorizante = idaut;
                    pedido.id_tipo_acceso = tipoacceso;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                
                if (result.Info == "ok")
                {
                    PedidoAcceso p = pm.crear(idPedido) as PedidoAcceso;
                    result = p.EstadoActual.paraAprobar(nombreUsuario, obs);
                }
                else { }
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        [HttpPost]
        public ActionResult BuscarPalabraClave(string palabra)
        {
            Responses result = new Responses();
            HttpRequestBase a = Request;
            return Redirect(a.UrlReferrer.AbsolutePath);
        }
    }
}