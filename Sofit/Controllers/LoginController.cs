﻿using Sofit.DataAccess;
using Sofit.Models.Helpers;
using Sofit.Models.Reglas;
using Sofit.Models.SignalR;
using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        [RequireHttps]        
        public ActionResult Index()
        {
            
            string message = TempData["message"] as string;
            ViewBag.ErrorMessage = message;
            return View();
        }
        [HttpPost]
        public ActionResult Login()
        {            
            Responses result = new Responses();
            string usuarioFixed = "UsuarioSofit";
            string passUsuarioFixed = "Password01";
            string ipaddress = string.Empty;
            string Hostname = Request.UserHostName;
            string userAgent = Request.UserAgent;
            string addressReq = Request.UserHostAddress;
            String nombre = Request["tNombre"];
            string password = Request["tPassword"];
            string DisplayName = string.Empty;
            string mail = string.Empty;
            usuarioHandler user = new usuarioHandler();
            bool flag = true;            
            try
            {
                //obtener ip                
                IPHostEntry Host = Dns.GetHostEntry(IPAddress.Parse(addressReq));
                foreach (IPAddress IP in Host.AddressList)
                {
                    if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        ipaddress = Convert.ToString(IP);
                        Hostname = Host.HostName;
                    }
                }                
            }//catchs entity y exception
            catch (DbEntityValidationException e)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                            LogsLogin login = new LogsLogin
                            {
                                fechaHora = DateTime.Now,
                                userAddress = ve.ErrorMessage,
                                userAgent = ve.PropertyName,
                                userHost = Hostname,
                                id_usuario = 1
                            };
                            dbc.LogsLogin.Add(login);
                        }
                    }
                    dbc.SaveChanges();
                }
            }
            //catch
            catch (Exception ex)
            {                
                result.Detail = ex.Message;
            }
            if (GlobalVar.DEBUG)
            {               
                #region local login
                usuarioBean usuario = user.validarNombre(nombre);
                if (usuario != null)
                {
                    if (usuario.Activo)
                    {
                        usuario.Password = password;
                        using (var dbc = new IncidenciasEntities())
                        {
                            var config = dbc.configs.SingleOrDefault();
                            if (config != null)
                            {
                                string ver = config.vers;
                                if (ver.Length > 0)
                                {
                                    Session["version"] = ver;
                                }
                                else Session["version"] = "n.e";
                            }
                            else Session["version"] = "n.e";
                            var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuario.IdUsuario & !c.fechaHasta.HasValue);
                            foreach (var x in viejoLogin)
                            {
                                x.fechaHasta = DateTime.Now;
                            }
                            LogsLogin login = new LogsLogin
                            {
                                fechaHora = DateTime.Now,
                                userAddress = ipaddress,
                                userAgent = userAgent,
                                userHost = Hostname,
                                id_usuario = usuario.IdUsuario
                            };
                            dbc.LogsLogin.Add(login);
                            dbc.SaveChanges();
                        }
                        Session["usuario"] = usuario;
                        if (usuario.EsInterno)
                        {
                            string[] homePage = usuario.getHomePage().Split('/');
                            return RedirectToAction("", homePage[1], new { area = homePage[0] });
                        }
                        else
                        {
                            return RedirectToRoute("IndexReglas");
                        }
                    }
                    else
                    {
                        TempData["message"] = "El usuario está deshabilitado. Contacte al administrador.";
                    }                    
                    
                }//usuario existe
                else
                {//usuario no existe                   
                    
                    try
                    {
                        string nombreExterno = string.Empty;
                        string nombreExtCompleto = string.Empty;
                        string email = string.Empty;
                        bool flagd = false;
                        DirectoryEntry ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", "20250451408", "Cs059999");
                        DirectoryEntry entrhc = new DirectoryEntry("LDAP://rhc.gov.ar", "gobiernocba\\20250451408", "Cs059999");
                        DirectorySearcher searcher = new DirectorySearcher(ent);

                        searcher.Filter = "(&(objectClass=user)((sAMAccountName=" + nombre + ")))";
                        SearchResultCollection re = searcher.FindAll();
                        if (re.Count > 0)
                        {
                            DisplayName = re[0].Properties["displayname"][0].ToString();
                            try
                            {

                                mail = re[0].Properties["mail"][0].ToString();
                                flagd = true;
                            }
                            catch (Exception)
                            {
                                mail = "Sin email";
                            }
                        }
                        else
                        {
                            searcher = new DirectorySearcher(entrhc);
                            searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + nombre + "))";
                            re = searcher.FindAll();
                            if (re.Count > 0)
                            {
                                DisplayName = re[0].Properties["displayname"][0].ToString();
                                try
                                {

                                    mail = re[0].Properties["mail"][0].ToString();
                                    flagd = true;
                                }
                                catch (Exception)
                                {
                                    mail = "Sin email";
                                }
                            }
                            else
                            {
                                //authentic = false;
                            }
                        }
                        if (flagd)
                        {
                            nombreExterno = nombre;
                            nombreExtCompleto = DisplayName;
                            try
                            {
                                email = "david.ortega@cba.gov.ar";
                            }
                            catch (Exception)
                            {
                                email = "Sin email";
                            }
                            //agrego el usuario a la db
                            usuarioHandler usHand = new usuarioHandler();
                            usuario = usHand.CrearUsuarioExterno(nombreExterno, nombreExtCompleto, email);
                            if (usuario != null)
                            {
                                using (var dbc = new IncidenciasEntities())
                                {
                                    var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuario.IdUsuario & !c.fechaHasta.HasValue);
                                    foreach (var x in viejoLogin)
                                    {
                                        x.fechaHasta = DateTime.Now;
                                    }
                                    LogsLogin login = new LogsLogin
                                    {
                                        fechaHora = DateTime.Now,
                                        userAddress = ipaddress,
                                        userAgent = userAgent,
                                        userHost = Hostname,
                                        id_usuario = usuario.IdUsuario
                                    };
                                    dbc.LogsLogin.Add(login);
                                    dbc.SaveChanges();
                                }
                                Session["usuario"] = usuario;
                                return RedirectToRoute("IndexReglas");
                            }
                            else
                            {
                                TempData["message"] = "Error creando usuario";
                            }
                        }
                        else
                        {
                            TempData["message"] = "Error en login";
                        }
                    }
                    catch (DirectoryServicesCOMException e)
                    {
                        TempData["message"] = e.Message;
                    }                    
                }
            #endregion
            }//debug
            else//produccion
            {                
                //verificar si fallo 4 veces el login en los ultimos 2 min
                TimeSpan threeMin = new TimeSpan(0, 3, 0);
                DateTime lowerLimit = DateTime.Now - threeMin;
                using (var dbc = new IncidenciasEntities())
                {
                    var fallos = from u in dbc.LoginFailed
                                 where u.userAddress == ipaddress & u.fechaHora > lowerLimit
                                 select u;
                    int cantidad = fallos.Count();
                    if (cantidad > 4)
                    {
                        result.Detail = "El equipo está bloqueado. Espere un momento y vuelva a intentar.";
                        flag = false;
                    }
                   //flag 
                }
                if (flag)//no tuvo 4 intentos fallidos
                {
                    #region autenticacion en AD
                    bool authentic = false;
                    DirectoryEntry ent = null;
                    DirectoryEntry entrhc = null;
                    DirectoryEntry entDefault = null;
                    DirectorySearcher searcher = null;
                    SearchResult re = null;
                    try
                    {
                        using (ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", nombre, password))
                        {
                            object nativeObj = ent.NativeObject;
                            authentic = true;
                            searcher = new DirectorySearcher(ent);
                            searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + nombre + "))";
                            re = searcher.FindOne();
                        }
                        if (re != null)
                        {
                            DisplayName = re.Properties["displayname"][0].ToString();
                            try
                            {

                                mail = re.Properties["mail"][0].ToString();
                            }
                            catch (Exception)
                            {
                                mail = "Sin email";
                            }
                        }
                        else
                        {
                            using (entrhc = new DirectoryEntry("LDAP://rhc.gov.ar", nombre, password))
                            {
                                object nativeObj = entrhc.NativeObject;
                                authentic = true;
                                searcher = new DirectorySearcher(entrhc);
                                searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + nombre + "))";
                                re = searcher.FindOne();
                            }
                            if (re != null)
                            {
                                DisplayName = re.Properties["displayname"][0].ToString();
                                try
                                {

                                    mail = re.Properties["mail"][0].ToString();
                                }
                                catch (Exception)
                                {
                                    mail = "Sin email";
                                }
                            }
                            else
                            {
                                authentic = false;
                            }
                        }
                    }
                    catch (DirectoryServicesCOMException)
                    {
                        authentic = false;
                        try
                        {
                            //busco gobierno
                            using (entDefault = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuarioFixed, passUsuarioFixed))
                            {
                                object nativeObj = entDefault.NativeObject;
                                searcher = new DirectorySearcher(entDefault);
                                searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + nombre + "))";
                                re = searcher.FindOne();
                            }
                            //busco rhc
                            if (re == null)
                            {
                                using (entDefault = new DirectoryEntry("LDAP://rhc.gov.ar", usuarioFixed, passUsuarioFixed))
                                {
                                    object nativeObj = entDefault.NativeObject;
                                    searcher = new DirectorySearcher(entDefault);
                                    searcher.Filter = "(&(objectClass=user)(sAMAccountName=" + nombre + "))";
                                    re = searcher.FindOne();
                                }
                            }
                            if (re != null)
                            {
                                string dummy = string.Empty;
                                Double dummy2 = 0D;
                                try
                                {
                                    dummy = re.Properties["lockouttime"][0].ToString();
                                    dummy2 = Double.Parse(dummy);
                                }
                                catch (Exception e)
                                {

                                }
                                string bdpwtime = re.Properties["badpwdcount"][0].ToString();
                                if (dummy2 >= 1 & bdpwtime == "5") result.Detail = "La cuenta está bloqueada por demasiados intentos erróneos. Espere unos instantes y vuelva a intentarlo.";
                                if (dummy2 >= 1 & bdpwtime != "5") result.Detail = "La cuenta está bloqueada. Contacte a mesa de ayuda.";
                                if (dummy2 == 0) result.Detail = "La contraseña es incorrecta";
                            }
                            //no encontre usuario
                            else result.Detail = "El usuario es incorrecto. Ingrese sólo el cuil en el usuario";
                        }
                        catch (Exception)
                        {
                            result.Detail = "Error del sistema. Contacte al administrador.";
                        }
                    }
                    if (authentic)
                    {
                        usuarioBean usuario = user.validarNombre(nombre);
                        if (usuario != null)//existe en la bd
                        {
                            if (usuario.Activo)
                            {
                                usuario.Password = password;
                                using (var dbc = new IncidenciasEntities())
                                {
                                    var config = dbc.configs.SingleOrDefault();
                                    if (config != null)
                                    {
                                        string ver = config.vers;
                                        if (ver.Length > 0)
                                        {
                                            Session["version"] = ver;
                                        }
                                        else Session["version"] = "n.e";
                                    }
                                    else Session["version"] = "n.e";
                                    //actualizo el email
                                    if (usuario.email == null | usuario.email == string.Empty)
                                    {
                                        dbc.Usuarios.Where(c => c.id_usuario == usuario.IdUsuario).Single().email = mail;
                                    }
                                    var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuario.IdUsuario & !c.fechaHasta.HasValue);
                                    foreach (var x in viejoLogin)
                                    {
                                        x.fechaHasta = DateTime.Now;
                                    }
                                    LogsLogin login = new LogsLogin
                                    {
                                        fechaHora = DateTime.Now,
                                        userAddress = ipaddress,
                                        userAgent = userAgent,
                                        userHost = Hostname,
                                        id_usuario = usuario.IdUsuario
                                    };
                                    dbc.LogsLogin.Add(login);
                                    dbc.SaveChanges();
                                    Session["usuario"] = usuario;
                                    if (usuario.EsInterno)
                                    {
                                        string[] homepage = usuario.getHomePage().Split('/');
                                        return RedirectToAction("", homepage[1], new { area = homepage[0] });
                                    }
                                    else
                                    {
                                        return RedirectToRoute("IndexReglas");
                                    }
                                }
                            }//if no activo
                            else
                            {
                                result.Info = "error";
                                result.Detail = "El usuario está deshabilitado. Contacte al administrador.";
                            }
                        }//if match local base
                        else
                        {
                            usuarioHandler usHand = new usuarioHandler();
                            usuario = usHand.CrearUsuarioExterno(nombre, DisplayName, mail);
                            if (usuario != null)
                            {
                                using (var dbc = new IncidenciasEntities())
                                {
                                    var viejoLogin = dbc.LogsLogin.Where(c => c.id_usuario == usuario.IdUsuario & !c.fechaHasta.HasValue);
                                    foreach (var x in viejoLogin)
                                    {
                                        x.fechaHasta = DateTime.Now;
                                    }
                                    LogsLogin login = new LogsLogin
                                    {
                                        fechaHora = DateTime.Now,
                                        userAddress = ipaddress,
                                        userAgent = userAgent,
                                        userHost = Hostname,
                                        id_usuario = usuario.IdUsuario
                                    };
                                    dbc.LogsLogin.Add(login);
                                    dbc.SaveChanges();
                                }
                                Session["usuario"] = usuario;
                                return RedirectToRoute("IndexReglas");
                            }
                            else
                            {
                                TempData["message"] = "Error creando usuario";
                            }
                        }
                    }
                    else
                    {//sin validacion
                        using (var dbc = new IncidenciasEntities())
                        {
                            if (nombre.Length > 100)
                            {
                                nombre = nombre.Substring(0, 100);
                            }
                            if (password.Length > 100)
                            {
                                password = password.Substring(0, 100);
                            }
                            LoginFailed failed = new LoginFailed
                            {
                                fechaHora = DateTime.Now,
                                userAddress = ipaddress,
                                userAgent = userAgent,
                                userHost = Hostname,
                                usuario = nombre,
                                pass = password
                            };
                            dbc.LoginFailed.Add(failed);
                            dbc.SaveChanges();
                        }                        
                    }
                    #endregion
                }
                TempData["message"] = result.Detail;
            }//producccion
            
            return View("Index");
        }//login

        ////--------

        
    }    
}
