﻿using Sofit.DataAccess;
using Sofit.Models.ClasesVistas;
using Sofit.Models.Helpers;
using Sofit.Models.Historicos;
using soporte.Areas.Infraestructura.Models.ClasesVista;
using soporte.Areas.Operaciones.Models.ClasesVista;
using soporte.Areas.Seguridad.Models;
using soporte.Areas.Soporte.Controllers;
using soporte.Areas.Soporte.Models.Displays;
using soporte.Areas.Telecomunicaciones.Models.ClasesVistas;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.DirectoryServices;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Sofit.Models;

namespace soporte.Controllers
{
    public class ServiceController : Controller
    {
        [SessionActionFilter]
        public ActionResult NotasDeLaVersion()
        {
            return View();
        }
        public JsonResult enviarObservacion(string nombre, string cuil, string observaciones)
        {
            usuarioBean logued = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            EmailSender emailS = new EmailSender();
            string body = "Número de cuil: " + cuil + " \n Nombre: " + nombre + " \n Observaciones: " + observaciones+"\n Enviado por: "+logued.NombreCompleto;
            result = emailS.sendEmailCustom(logued.email, "david.ortega@cba.gov.ar", "Desde la pagina de usuarios externos", body);
            result = emailS.sendEmailCustom(logued.email, "gestiondeserviciosdgcit@cba.gov.ar", "Desde la pagina de usuarios externos", body);
            //result = emailS.sendEmail(new string[] { "Soporte.Tecnico.SecGral@cba.gov.ar", subject + "Enviado por: " + usuario.NombreCompleto, body });
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult GetNextOblea(int tipo)
        {
            Responses result = new Responses();
            using(var dbc=new IncidenciasEntities())
            {
                var number = dbc.UnidadConf.Where(c => c.id_tipo == tipo).OrderByDescending(d => d.nombre).Take(1);
                string ultimoNumero = string.Empty;
                string prefijo = string.Empty;
                bool flagFound = false;
                int numero  = 0;
                string sufijo = string.Empty;
                string obleaToReturn = string.Empty;
                if (number.Count() > 0)
                {
                    ultimoNumero = number.First().nombre;
                    Regex pattern = new Regex(@"(?<prefijo>\w{2,3})-{0,1}(?<nro>\d{5})-{0,1}(?<sufijo>\w{0,3})");
                    
                    try
                    {
                        Match match = pattern.Match(ultimoNumero);
                        numero = Int32.Parse(match.Groups["nro"].Value);
                        prefijo = match.Groups["prefijo"].Value;
                        sufijo = match.Groups["sufijo"].Value;
                        int probableNextNro = (int)(numero / 2);
                        while (probableNextNro>0)
                        {
                            string probNro = prefijo + probableNextNro.ToString("D5") + sufijo;
                            var ucfound = dbc.UnidadConf.Where(c => c.nombre == probNro).SingleOrDefault();
                            if(ucfound !=null)
                            {
                                probableNextNro = (int)probableNextNro / 2;                                
                            }
                            else
                            {
                                obleaToReturn = prefijo + probableNextNro.ToString("D5");
                                probableNextNro = 0;
                                flagFound = true;
                            }
                        }
                        if(!flagFound)
                        {
                            obleaToReturn = prefijo + (++numero).ToString("D5");
                        }
                        result.Info = "ok";
                        result.Detail = obleaToReturn;
                    }
                    catch (Exception) 
                    {
                           
                    }
                }
                else
                {
                    result.Detail = "No hay Elementos de Configuración cargados de este tipo. Puede usar el primer número";
                }
            }
            return Json(result);
        }       
        public JsonResult CantidadesPorTipo()
        {
            Responses result = new Responses();
            List<CantidadesPorTipo> cant = new List<CantidadesPorTipo>();
            using (var dbc = new IncidenciasEntities())
            {
                var db = from u in dbc.UnidadConf
                         group u by u.id_tipo into newgroup
                         select newgroup;
                foreach (var n in db)
                {
                    CantidadesPorTipo c = new CantidadesPorTipo
                    {
                        idTipo = n.Key,
                        Cantidad = n.Count(),
                        TipoElem = dbc.Tipo_equipo.Where(d => d.id_tipo == n.Key).Single().descripcion,
                        Imagen = ""
                    };
                    cant.Add(c);
                }
            }
            return Json(cant);
        }
        [HttpPost]
        public JsonResult updateEmail(string nombre, string email)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var usuario = dbc.Usuarios.Where(c => c.nombre == nombre).Single();
                    usuario.email = email;
                    dbc.SaveChanges();
                    result.Info = "ok";
                    result.Detail = email;
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult ListadoXTipo(int idTipo)
        {
            List<ElementoListado> listado = new List<ElementoListado>();
            using (var dbc = new IncidenciasEntities())
            {
                listado = dbc.UnidadConf.Where(c => c.id_tipo == idTipo).Select(d => new ElementoListado
                {
                    idEquipo = d.id_uc,
                    Jurisdiccion = new JurisdiccionVista
                    {
                        idJurisdiccion = d.id_jurisdiccion,
                        Nombre = d.Jurisdiccion.descripcion
                    },
                    NroSerie = d.nro_serie,
                    Oblea = d.nombre,
                    Observaciones = d.observaciones,
                    TipoEquipo = new TiposEquipoVista
                    {
                        id = d.id_tipo,
                        Nombre = d.Tipo_equipo.descripcion
                    }
                }).ToList();
            }
            var jsonResult = Json(listado);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [SessionActionFilter]
        public JsonResult GetEquipoInventarioSerie(string serie)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nro_serie == serie
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    return GetEquipoInventario(eqBase.nombre);
                }
                else return Json(new Responses { Detail = "No encontrado" });
            }
        }
        [SessionActionFilter]
        public JsonResult GetEquipoInventario(string oblea)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        //soporte
                        case "Pc":
                            equipo = new PcInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Impresora":
                            equipo = new ImpresoraInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Monitor":
                            equipo = new MonitorInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Varios":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Grupo Electrogeno":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "UPS":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Rack":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aire Acondicionado":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Tablero":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //operaciones
                        case "Enlace":
                            equipo = new EnlaceInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Software de Base":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aplicación de Usuarios":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //infraestructura
                        case "Servidor Físico":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Virtual":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Workstation Virtual":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Storage":
                            equipo = new StorageInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster":
                            equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //seguridad                        
                        case "Balanceador de carga":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Proxy-Cache de contenidos":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Firewall":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Dispositivo de Gestion FW":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Syslog":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aplicación":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Proyecto":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster Seguridad":
                            equipo = new ClusterSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //telecomunicaciones
                        default:
                            equipo = new EquipoTelecomunicaciones();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);
        } 
        public JsonResult setPrioridadTicket(int idTicket, int idPrioridad, string descripcion)
        {
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    var ticketActual = dbc.Incidencias.Where(c => c.id_incidencias == idTicket).Single();
                    if (ticketActual.id_prioridad == idPrioridad)
                    {
                        result.Detail = "El nuevo estado es el mismo que el actual";
                    }
                    else
                    {
                        CambiosPrioridad nuevo = new CambiosPrioridad
                        {
                            id_pr_actual = idPrioridad,
                            id_usuario = usuarioActual.IdUsuario,
                            id_ticket = ticketActual.id_incidencias,
                            fecha = DateTime.Now,
                            id_pr_previa = ticketActual.id_prioridad.Value,
                            descripcion = descripcion
                        };
                        ticketActual.id_prioridad = idPrioridad;
                        dbc.CambiosPrioridad.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }catch(Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult GetEquipoId(int id)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        //soporte
                        case "Pc":
                            equipo = new PcInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Impresora":
                            equipo = new ImpresoraInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Monitor":
                            equipo = new MonitorInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Varios":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Grupo Electrogeno":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "UPS":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Rack":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aire Acondicionado":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //operaciones
                        case "Enlace":
                            equipo = new EnlaceInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Software de Base":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aplicación de Usuarios":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //infraestructura
                        case "Servidor Físico":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Virtual":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Workstation Virtual":
                            equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Storage":
                            equipo = new StorageInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster":
                            equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //seguridad                        
                        case "Balanceador de carga":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Proxy-Cache de contenidos":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Firewall":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Dispositivo de Gestion FW":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Syslog":
                            equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Aplicación":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Proyecto":
                            equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster Seguridad":
                            equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //telecomunicaciones
                        default:
                            equipo = new EquipoTelecomunicaciones();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);
        }
        [SessionActionFilter]
        public JsonResult AgregarReferentes(int id, string nuevosRef)
        {
            Responses result = new Responses();
            string[] mailAd = nuevosRef.Split(',');
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var inci = dbc.Incidencias.Where(c => c.id_incidencias == id).SingleOrDefault();
                    result.Detail = "";
                    foreach (string mail in mailAd)
                    {
                        if (inci.NotificacionXIncidente.Any(c => c.email == mail))
                        {
                            result.Info = "conerror";
                            result.Detail += "El email " + mail + " ya está agregado!!!";
                        }
                        else
                        {
                            inci.NotificacionXIncidente.Add(new NotificacionXIncidente
                            {
                                email = mail,
                                id_incidencia = id
                            });
                        }
                    }
                    dbc.SaveChanges();
                    if(result.Info!="conerror") result.Info = "ok";
                }
            }catch(Exception e)
            {
                result.Info = "error";
                result.Detail = e.Message;
            }
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult getReferentes(int idTicket)
        {
            List<ReferentesVista> referentes = null;
            using(var dbc=new IncidenciasEntities())
            {
                referentes = dbc.Incidencias.Where(c => c.id_incidencias == idTicket).Single().
                    NotificacionXIncidente.Select(c => new ReferentesVista { email = c.email }).ToList();
            }
            return Json(referentes);
        }
        [SessionActionFilter]
        public JsonResult EquipoXOblea(string prefixText)
        {            
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre
                         }).Take(10).ToList();
            }            
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult EquipoXSerie(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nro_serie.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult EquipoXObleaYTipo(string prefixText,int idTipo)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText) & u.Tipo_equipo.id_tipo == idTipo
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre
                         }).Take(10).ToList();
            }
            return Json(items);
        }        
        class Destinatarios
        {
            public string Nombre { get; set; }
            public string email { get; set; }
        }
        [SessionActionFilter]        
        public JsonResult getAddBook(string prefixText)
        {
            Response.Charset = "utf-8";
            Responses result = new Responses();
            List<Destinatarios> outp = new List<Destinatarios>();
            //LdapConnection lcon = new LdapConnection(new LdapDirectoryIdentifier("LDAP://10.250.1.55:389"));
            if (prefixText.Length > 5)
            {
                usuarioBean usuario = Session["usuario"] as usuarioBean;
                //DirectoryEntry ent = new DirectoryEntry("LDAP://10.250.1.55:389");--Server de lotus
                if (usuario.Nombre.Length <= 5 | usuario.Password.Length <= 5|GlobalVar.DEBUG)//problema con variables
                {
                    return Json(new Responses() { Info = "Login" });
                }
                else
                {
                    DirectoryEntry ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar", usuario.Nombre, usuario.Password);
                    //ent.AuthenticationType = AuthenticationTypes.Anonymous;
                    try
                    {
                        object nativeObj = ent.NativeObject;
                        DirectorySearcher searcher = new DirectorySearcher(ent);
                        //searcher.Filter = "(cn=" + prefixText + "*)";
                        searcher.Filter = "(&(objectClass=user)(displayname=" + prefixText + "*))";
                        SearchResultCollection re = searcher.FindAll();
                        if (re.Count > 0)
                        {
                            int i = 0;
                            foreach (SearchResult r in re)
                            {
                                Destinatarios nu = new Destinatarios();
                                if (r.Properties["mail"].Count > 0)
                                {
                                    nu.email = r.Properties["mail"][0].ToString();
                                }
                                else
                                {
                                    continue;
                                }
                                if (r.Properties["displayname"].Count > 0)
                                {
                                    nu.Nombre = r.Properties["displayname"][0].ToString();
                                }
                                else
                                {
                                    continue;
                                }
                                if (i < 10)
                                {
                                    outp.Add(nu);
                                    i++;
                                }
                                else break;
                            }
                        }
                        if (outp.Count < 10)
                        {
                            DirectoryEntry ent2 = new DirectoryEntry("LDAP://rhc.gov.ar", usuario.Nombre, usuario.Password);
                            searcher = new DirectorySearcher(ent2);
                            searcher.Filter = "(&(objectClass=user)(displayname=" + prefixText + "*))";
                            re = searcher.FindAll();
                            int i = outp.Count;
                            foreach (SearchResult r in re)
                            {
                                Destinatarios nu = new Destinatarios();
                                if (r.Properties["mail"].Count > 0)
                                {
                                    nu.email = r.Properties["mail"][0].ToString();
                                }
                                else
                                {
                                    continue;
                                }
                                if (r.Properties["displayname"].Count > 0)
                                {
                                    nu.Nombre = r.Properties["displayname"][0].ToString();
                                }
                                else
                                {
                                    continue;
                                }
                                if (i < 10)
                                {
                                    outp.Add(nu);
                                    i++;
                                }
                                else break;
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }                    
            return Json(outp);

        }
        public JsonResult getUser(string usuario)
        {
            Response.Charset = "utf-8";
            Responses result = new Responses();
            //usuarioBean yo = Session["usuario"] as usuarioBean;
            //List<Destinatarios> outp = new List<Destinatarios>();
            ////LdapConnection lcon = new LdapConnection(new LdapDirectoryIdentifier("LDAP://10.250.1.55:389"));
            //try
            //{
                
            //    DirectoryEntry ent = new DirectoryEntry("LDAP://gobiernocba.gov.ar","20250451408","marad@nael10");
            //    //ent.AuthenticationType = AuthenticationTypes.Anonymous;
            //    DirectorySearcher searcher = new DirectorySearcher(ent);
            //    searcher.Filter = "(&(objectClass=user)(|(cn=" + usuario + ")(sAMAccountName=" + usuario + ")))";
            //    SearchResult re = searcher.FindOne();
            //    string DisplayName = re.Properties["displayname"][0].ToString();
            //    string  mail = re.Properties["mail"][0].ToString();
            //    usuarioHandler user = new usuarioHandler();
            //    usuarioBean u = user.validar(DisplayName, "", mail);
            //}
            //catch (Exception e)
            //{

            //}
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult pathImagen(int id)
        {
            string pathDirectorio = "/Content/img/Photo/";
            string pathImagen = string.Empty;
            using (var dbc = new IncidenciasEntities())
            {
                var imageninc = dbc.Incidencias.Where(c => c.id_incidencias == id).First().ImagenIncidencia;
                pathImagen = imageninc.First().path;
            }
            string nombreArchivo = pathImagen.Split('\\').Last();
            pathDirectorio += nombreArchivo;
            return Json(new Responses
            {
                Info = "ok",
                Detail = pathDirectorio
            });
        }
        public JsonResult pathImagenResolucion(string name)
        {
            string pathDirectorio = "/Content/img/Resoluciones/";
            string pathImagen = string.Empty;            
            pathDirectorio += name;
            return Json(new Responses
            {
                Info = "ok",
                Detail = pathDirectorio
            });
        }
        [SessionActionFilter]
        public JsonResult EquipoXDependencia(int idDep)
        {
            List<EquipoComplete> equipos = new List<EquipoComplete>();
            using(var dbc=new IncidenciasEntities())
            {
                equipos = dbc.Pc.Where(c => c.id_dependencia == idDep).Select(z => new EquipoComplete
                {
                     //idEquipo=z.id_uc,
                     Oblea=z.UnidadConf.nombre,
                     NroSerie=z.UnidadConf.nro_serie
                }).ToList();
                equipos.AddRange(dbc.Impresoras.Where(c => c.id_dependencia == idDep).Select(z => new EquipoComplete
                {
                     //idEquipo=z.id_uc,
                     Oblea=z.UnidadConf.nombre,
                     NroSerie = z.UnidadConf.nro_serie
                }).ToList());
                equipos.AddRange(dbc.Monitores.Where(c => c.id_dependencia == idDep).Select(z => new EquipoComplete
                {
                    //idEquipo=z.id_uc,
                    Oblea=z.UnidadConf.nombre,
                    NroSerie = z.UnidadConf.nro_serie
                }).ToList());
            }
            DataTable d = DataTableConverter.ToDataTable(equipos);
            Session["tablesource"] = d;
            return Json(equipos);
        }
        [SessionActionFilter]
        public FileResult pathFile(int id)
        {            
            string pathImagen = string.Empty;
            string file = string.Empty;
            string mime = string.Empty;
            using (var dbc = new IncidenciasEntities())
            {
                var archivoInc = dbc.Incidencias.Where(c => c.id_incidencias == id).First().AdjuntoIncidencia;
                pathImagen = archivoInc.First().path;
                file = pathImagen.Split('\\').Last();
                mime = archivoInc.First().mime;
            }
            string pathDirectorio = PathImage.getCustomFileDirectory(pathImagen);
            return File(pathDirectorio, mime!=null?mime:"text/plain", file);
        }       
        public FileResult adjuntoReglas(string id)
        {
            string pathFile = string.Empty;
            string file = string.Empty;
            string mime = string.Empty;           
            pathFile = "~/Content/Files/Reglas/" + id;
            mime =MimeMapping.GetMimeMapping(pathFile);            
            
            return File(pathFile, mime != null ? mime : "text/plain", id);
        }        
        public JsonResult Contacto(string nombre,string email, string body)
        {
            Responses result = new Responses();
            EmailSender emailS = new EmailSender();            
            result = emailS.sendEmailCustom(email,"david.ortega@cba.gov.ar","Desde el sistema de consultas",body);
            result = emailS.sendEmailCustom(email, "gestiondeserviciosdgcit@cba.gov.ar", "Desde el sistema de consultas", body);
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult GetIncHistoricoPorNro(string prefixText)
        {
            List<String> items = new List<String>();
            List<String> hist = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Incidencias
                         where u.numero.StartsWith(prefixText)
                         group u by u.numero into g
                         select g.Key).Take(7).ToList();
                items.AddRange(hist);
            }
            return Json(items);
        }
        #region BUSQUEDAHISTORICOS

        
        //BUSQUEDA POR OTROS CAMPOS
        public JsonResult getCoincidencias(string prefixText, string criteria)
        {
            List<CoincidenciasLista> resultados = new List<CoincidenciasLista>();
            using (var dbc = new IncidenciasEntities())
            {
                if (criteria == "descripcion")
                {
                    resultados = dbc.Incidencias.Where(c => c.descripcion.Contains(prefixText)).Select(d => new CoincidenciasLista
                    {
                        IdTicket = d.id_incidencias,
                        Descripcion = d.descripcion,
                        Texto=prefixText
                    }).Take(20).ToList();
                }
                if(criteria=="trabajos")
                {
                    resultados = dbc.Resoluciones.Where(c => c.observaciones.Contains(prefixText))
                        .Select(d => new CoincidenciasLista
                        {
                            IdTicket = d.id_incidencia.Value,
                            Descripcion = d.observaciones,
                            Texto = prefixText
                        }).Take(20).ToList();
                }
            }

            return Json(resultados);
        }
        public JsonResult getHistoricoOtros(string text, string criteria)
        {
            List<HistoricoIncidencia> resultados = new List<HistoricoIncidencia>();
            using (var dbc = new IncidenciasEntities())
            {
                if (criteria == "descripcion")
                {
                    resultados = dbc.Incidencias.Where(c => c.descripcion.Contains(text)).OrderByDescending(f=>f.fecha_inicio).Select(d => new HistoricoIncidencia
                    {
                        Numero = d.numero,
                        Dependencia = d.id_dependencia.HasValue ? d.Dependencia.descripcion : "N/A",
                        Jurisdiccion = d.id_dependencia.HasValue ? d.Dependencia.Jurisdiccion.descripcion : "N/A",
                        Cliente = d.id_contacto.HasValue ? d.Contactos.nombre : "N/A",
                        FechaInicio = d.fecha_inicio,
                        EstadoTicket = d.fecha_fin.HasValue ? "Cerrado" : "Activo",
                        IdIncidencia = d.id_incidencias,
                        Descripcion = d.descripcion
                    }).ToList();
                }
                if (criteria == "trabajos")
                {
                    var res = dbc.Resoluciones.Where(c => c.observaciones.Contains(text)).OrderByDescending(f => f.Incidencias.fecha_inicio).GroupBy(f => f.id_incidencia);
                    resultados = new List<HistoricoIncidencia>();
                    foreach (var v in res) {
                        var thisticket = v.FirstOrDefault();
                        resultados.Add(
                         new HistoricoIncidencia
                         {
                             Numero = thisticket.Incidencias.numero,
                             Dependencia = thisticket.Incidencias.id_dependencia.HasValue ? thisticket.Incidencias.Dependencia.descripcion : "N/A",
                             Jurisdiccion = thisticket.Incidencias.id_dependencia.HasValue ? thisticket.Incidencias.Dependencia.Jurisdiccion.descripcion : "N/A",
                             Cliente = thisticket.Incidencias.id_contacto.HasValue ? thisticket.Incidencias.Contactos.nombre : "N/A",
                             FechaInicio = thisticket.Incidencias.fecha_inicio,
                             EstadoTicket = thisticket.Incidencias.fecha_fin.HasValue ? "Cerrado" : "Activo",
                             IdIncidencia = thisticket.Incidencias.id_incidencias,
                             Descripcion = thisticket.Incidencias.descripcion
                         });
                    }
                }
            }

            return Json(resultados);
        }
        //FIN BUSQUEDA POR OTROS
        //BUSQUEDA HIST.XCLIENTE
        public JsonResult GetIncHistoricoPorCliente(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Incidencias
                         where u.Contactos.nombre.IndexOf(prefixText) != -1
                         group u by u.Contactos.nombre into g
                         select g.Key).Take(12).ToList();                
            }
            items.RemoveAll(item => item == null);
            return Json(items);
        }       
        public JsonResult getHistoricoCliente(string text)
        {
            HistClienteBuilder clBuilder = new HistClienteBuilder();
            HistoricoCliente historico = clBuilder.Construir(text);
            if (historico == null)
            {
                return Json(new Responses { Info = "error", Detail = "No se encontró el cliente" });
            }
            else
            {
                return Json(historico);
            }
        }
        //FIN BUSQUEDA X CLIENTE
        //BUSQUEDA X EC
        public JsonResult GetEquipoHistoricoPorNro(string prefixText)
        {
            List<String> items = new List<String>();
            List<String> hist = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = dbc.UnidadConf.Where(c => c.nombre.StartsWith(prefixText)).Take(7).Select(c => c.nombre).ToList();                       
            }
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult getHistoricoEquipo(string oblea)
        {
            HistEquipoBuilder builder = new HistEquipoBuilder();
            builder.Construir(oblea, true);
            HistoricoEquipo historicoEquipo = builder.getResult();
            if (historicoEquipo == null) historicoEquipo = new HistoricoEquipo();
            return Json(historicoEquipo);
        }
        //FIN BUSQUEDA EC
        #endregion
        [SessionActionFilter]
        public JsonResult buscarDatosIncidente(int idInc)
        {
            InfoTickets info = new InfoTickets();
            using (var dbc = new IncidenciasEntities())
            {
                var ticket = (from u in dbc.Incidencias
                              where u.id_incidencias == idInc
                              select u).First();
                info.NroTicket = ticket.numero;
                info.IdTicket = ticket.id_incidencias;
                info.idDependencia = ticket.id_dependencia.Value;
                info.resoluciones = (from u in dbc.Resoluciones
                                     where u.id_incidencia == idInc
                                     select new ResToDisplay
                                     {
                                         Fecha = u.fecha,
                                         Observ = u.observaciones,
                                         Tecnico = u.tecnico.descripcion,
                                         TipoRes = u.tipo_resolucion.descripcion
                                     }).ToList();
                info.referentes = (from r in dbc.NotificacionXIncidente
                                   where r.id_incidencia==idInc
                                   select new ReferentesVista
                                   {
                                       id = r.id,                                       
                                       email = r.email,
                                       nombre=r.email
                                   }).ToList();
                info.referentes.AddRange((from r in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                                     where r.Dependencia.Any(s => s.id_dependencia == info.idDependencia) |
                                     r.Jurisdiccion.Any(o => o.Dependencia.Any(p => p.id_dependencia == info.idDependencia))
                                     select new ReferentesVista
                                     {
                                         id = r.id,
                                         nombre = r.nombre,
                                         email = r.email,
                                         te = r.te
                                     }).ToList());
                info.Firma = (Session["usuario"] as usuarioBean).Firma;
            }

            return Json(info);        
        }
        [SessionActionFilter]
        public JsonResult notificarTickets(){
            usuarioBean user = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();            
            int idIncidencia = Int32.Parse(nvc.Get("IdTicket"));
            string nroIncidente = nvc.Get("NroTicket");
            string referentes = nvc.Get("referentes");
            referentes = referentes.Substring(referentes.IndexOf('=') + 1);
            string[] parsed = referentes.Split(',');
            string firma = user.Firma;
            string cuerpo = nvc.Get("body");
            string subject = nvc.Get("subject");
            //--
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, false, false, false);
            Incidencia incidencia = builder.getResult();
            //--
            result = incidencia.notificacionTicket(parsed, firma, cuerpo, subject);
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult reactivarIncidente(int idInc)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual=Session["usuario"] as usuarioBean;
            int idUsuario = usuarioActual.IdUsuario;            
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, false, true, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.reactivarTicket(idInc,idUsuario);
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult NotificacionesIncidentes(int idInc)
        {
            Responses result = new Responses();
            List<NotificacionTicketSusp> notis = null;           
            List<NotifDisplay> notificaciones = new List<NotifDisplay>();
            using (var dbc = new IncidenciasEntities())
            {
                notis = (from s in dbc.NotificacionTicketSusp
                         where s.id_ticket==idInc
                         select s).ToList();
                foreach (NotificacionTicketSusp n in notis)
                {
                    List<string> mails = (from m in n.DireccionMailXNotTicket
                                          where m.id_notif == n.id
                                          select m.direccion_mail).ToList();

                    NotifDisplay no = new NotifDisplay()
                    {
                        mailsAdd = mails,
                        fecha = n.fecha.ToShortDateString(),
                        mensaje = n.mensaje,
                        enviadoPor = n.Usuarios.nombre_completo
                    };

                    notificaciones.Add(no);
                }
            }
            return Json(notificaciones);      
        }
        [SessionActionFilter]
        public JsonResult TecnicosXDir(int idDir)
        {
            List<Tecnico> tecnicos = new List<Tecnico>();
            using(var dbc=new IncidenciasEntities())
            {
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.id_direccion == idDir & u.activo.Value
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
            }
            return Json(tecnicos);
        }
        #region Clientes
        [SessionActionFilter]
        public JsonResult getDatosCliente(string prefixText)
        {
           Regex pattern = new Regex(@"(?<dni>^\d+)");
            Match match = pattern.Match(prefixText);
            List<ReferentesVista> contactos = new List<ReferentesVista>();
            
            using(var dbc=new IncidenciasEntities())
                {
                    var con = dbc.Contactos.Where(c => match.Success ? c.dni.StartsWith(prefixText) : c.nombre.IndexOf(prefixText)!=-1).Take(5);
                    foreach(Contactos c in con)
                    {
                        ReferentesVista r = new ReferentesVista
                        {
                            id = c.id_contacto,
                            dni = c.dni,
                            Direccion = c.direccion,
                            email = c.email,
                            nombre = c.nombre,
                            te = c.te
                        };
                        if(c.id_dependencia.HasValue)
                        {
                            r.Dependencias = new List<DependenciaVista>();
                            DependenciaVista dv = new DependenciaVista
                            {
                                idDependencia = c.id_dependencia.Value,
                                Nombre = c.Dependencia.descripcion,
                                Jurisdiccion = new JurisdiccionVista
                                {
                                    idJurisdiccion = c.Dependencia.Jurisdiccion.id_jurisdiccion,
                                    Nombre = c.Dependencia.Jurisdiccion.descripcion
                                }
                            };
                            r.Dependencias.Add(dv);
                        }
                        contactos.Add(r);
                    }                 
                }           
            return Json(contactos);
        }        
        [SessionActionFilter]
        public JsonResult getCliente(string nombre)
        {            
            ReferentesVista r=null;
            using (var dbc = new IncidenciasEntities())
            {
                var con = dbc.Contactos.Where(c => c.nombre == nombre).FirstOrDefault();
                
                if(con!=null){
                    r = new ReferentesVista
                    {
                        id = con.id_contacto,
                        dni = con.dni,
                        Direccion = con.direccion,
                        email = con.email,
                        nombre = con.nombre,
                        te = con.te
                    };
                    if (con.id_dependencia.HasValue)
                    {
                        r.Dependencias = new List<DependenciaVista>();
                        DependenciaVista dv = new DependenciaVista
                        {
                            idDependencia = con.id_dependencia.Value,
                            Nombre = con.Dependencia.descripcion,
                            Jurisdiccion = new JurisdiccionVista
                            {
                                idJurisdiccion = con.Dependencia.Jurisdiccion.id_jurisdiccion,
                                Nombre = con.Dependencia.Jurisdiccion.descripcion
                            }
                        };
                        r.Dependencias.Add(dv);
                    }
                }                
            }
            return Json(r);
        }
        [SessionActionFilter]
        public JsonResult GetClientesActivos(string prefixText)
        {
            HashSet<ReferentesVista> items = new HashSet<ReferentesVista>();
            using (var dbc = new IncidenciasEntities())
            {
                List<ReferentesVista> referentes = new List<ReferentesVista>();
                referentes = (from u in dbc.IncidenciasActivas
                              where u.Incidencias.Contactos.nombre.StartsWith(prefixText) | u.Incidencias.Contactos.dni.StartsWith(prefixText)
                              select new ReferentesVista
                              {
                                  id = u.Incidencias.Contactos.id_contacto,
                                  nombre = u.Incidencias.Contactos.nombre
                              }).ToList();
                try
                {
                    foreach(ReferentesVista r in referentes)
                    {
                        items.Add(r);
                    }
                }
                catch (Exception e)
                {

                }

            }
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult incidenteXCliente(string id)
        {
            List<DatosUbicacion> resp = new List<DatosUbicacion>();
            int idC = 0;
            bool buscoPorNombre = false;
            try
            {
                idC = Int32.Parse(id);
            }
            catch (Exception e)
            {
                buscoPorNombre = true;
            }
            if (buscoPorNombre)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.Contactos.nombre == id
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente",
                                Fecha = u.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Desc = u.Incidencias.descripcion
                            }).ToList();
                }
            }
            else
            {
                using (var dbc = new IncidenciasEntities())
                {
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.Contactos.id_contacto == idC
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente",
                                Fecha = u.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Desc = u.Incidencias.descripcion
                            }).ToList();
                }
            }
            return Json(resp);
        }
        [SessionActionFilter]
        public JsonResult NuevoCliente(ReferentesVista cliente)
        {
            Responses result = new Responses();
            try
            {
                cliente.nombre = cliente.nombre.Trim();
   
                using (var dbc = new IncidenciasEntities())
                {
                    var cl = dbc.Contactos.Where(c => c.nombre == cliente.nombre | c.dni == cliente.dni);
                    if (cl.Count() > 0)
                    {
                        result.Detail = "Ya existe un cliente con ese nombre o dni";

                    }
                    else
                    {
                        Contactos conta = new Contactos
                        {
                            nombre = cliente.nombre,
                            direccion = cliente.Direccion,
                            te = cliente.te,
                            dni = cliente.dni,
                            email = cliente.email
                        };
                        if (cliente.Dependencias!=null&cliente.Dependencias.Count() > 0)
                        {
                            DependenciaVista d = cliente.Dependencias.First();
                            Dependencia dep = dbc.Dependencia.Where(c => c.id_dependencia == d.idDependencia).Single();
                            conta.Dependencia = dep;
                        }
                        dbc.Contactos.Add(conta);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }catch(Exception e)
            {
                result.Detail = e.Message + "--" + e.StackTrace+"\n"+"------------"+
                    e.Data;
            }
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult actualizarCl(ReferentesVista cliente)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var cl = dbc.Contactos.Where(c => c.id_contacto==cliente.id).SingleOrDefault();
                    if (cl != null)
                    {
                        cl.nombre = cliente.nombre;
                        cl.dni = cliente.dni;
                        cl.direccion = cliente.Direccion;
                        cl.te = cliente.te;
                        cl.dni = cliente.dni;
                        cl.email = cliente.email;
                        if (cliente.Dependencias != null & cliente.Dependencias.Count() > 0)
                        {
                            cl.id_dependencia = cliente.Dependencias[0].idDependencia;
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {
                        result.Detail = "No se encontró el cliente";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }        
        #endregion
        [SessionActionFilter]
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.numero.StartsWith(prefixText)
                         select u.Incidencias.numero).Take<string>(7).ToList();
            }
            return Json(items);
        }        
        public JsonResult UbicacionIncidente(string nroInc)
        {
            IncidentesActivosBuilder incBuilder = new IncidentesActivosBuilder();
            incBuilder.Construir(nroInc);
            return Json(incBuilder.getResult());          
        }
        [SessionActionFilter]
        public JsonResult getHistoricoIncidente(string text)
        {
            HistIncBuilder incBuilder = new HistIncBuilder();
            incBuilder.Construir(text);
            return Json(incBuilder.getResult());
        }
        [SessionActionFilter]
        public JsonResult getHistoricoIncEstados(string text)
        {
            HistIncEstadosBuilder incEstadosBuilder = new HistIncEstadosBuilder();
            incEstadosBuilder.Construir(text);
            return Json(incEstadosBuilder.getResult());
        }
        [SessionActionFilter]
        public JsonResult Jurisdicciones()
        {
            List<JurisdiccionVista> jurisdicciones = new List<JurisdiccionVista>();
            using (var dbc = new IncidenciasEntities())
            {
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new JurisdiccionVista
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
            }
            return Json(jurisdicciones);
        }
        class customc : IComparer<ResolucionesIncidentesVista>
        {
            public int Compare(ResolucionesIncidentesVista r1, ResolucionesIncidentesVista r2)
            {
                if (r1.Fecha > r2.Fecha) return 1;
                if (r1.Fecha < r2.Fecha) return -1;
                return 0;
            }
        }
        public JsonResult buscarSoluciones(int incidente)
        {
            List<ResolucionesIncidentesVista> encontrados;
            using (var dbc = new IncidenciasEntities())
            {
                encontrados = (from u in dbc.Resoluciones
                               where u.id_incidencia == incidente
                               select new ResolucionesIncidentesVista
                               {
                                   Direccion = u.area.Direcciones.nombre,                                   
                                   Uc = u.UnidadConf != null ? u.UnidadConf.nombre : null,
                                   Fecha = u.fecha,
                                   Observaciones = u.observaciones,
                                   Técnico = u.tecnico.descripcion,
                                   TipoResolucion = u.tipo_resolucion.descripcion,
                                   PathImagen=u.path_imagen
                               }).ToList();
                encontrados.Sort(new customc());
            }            
            //string html = string.Empty;
            //if (encontrados.Count > 0)
            //{
            //    encontrados.Sort();
            //    html = "<table><tr><th>Fecha</th><th>SubDirección</th><th>Equipo</th><th>Tipo Res.</th><th>Técnico</th><th>Observaciones</th></tr>";
            //    foreach (ResolucionesIncidentesVista r in encontrados)
            //    {
            //        html += "<tr><td>" + r.Fecha.ToShortDateString() + "</td>";
            //        html += "<td>" + r.Direccion + "</td>";
            //        html += "<td>" + r.Uc + "</td>";                    
            //        html += "<td>" + r.TipoResolucion + "</td>";
            //        html += "<td>" + r.Técnico + "</td>";
            //        html += "<td>" + r.Observaciones + "</td>";
            //        html += "<td>" + r.PathImagen!=null?r.PathImagen:"" + "</td></tr>";
            //    }
            //    html += "</table>";
            //}
            //else
            //{
            //    html = "No se han encontrado soluciones";
            //}
            return Json(encontrados);
        }
        [SessionActionFilter]
        public JsonResult Dependencias(int idJur)
        {
            List<DependenciaVista> dependencias = new List<DependenciaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                dependencias = (from u in dbc.Dependencia
                                where u.id_jurisdiccion == idJur
                                select new DependenciaVista
                                {
                                    idDependencia = u.id_dependencia,
                                    Nombre = u.descripcion
                                }).ToList();
            }
            return Json(dependencias);
        }
        [SessionActionFilter]
        public JsonResult EquipoXObleaTele(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoTele
                         where u.UnidadConf.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult IncidentesExisten(string prefixText)
        {
            List<IncidenteComplete> items = new List<IncidenteComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Incidencias
                         where u.numero.StartsWith(prefixText)
                         select new IncidenteComplete
                         {
                             idIncidente = u.id_incidencias,
                             Numero = u.numero
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        [SessionActionFilter]
        public JsonResult PersonalSize()
        {
            usuarioBean user = Session["usuario"]as usuarioBean;
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;            
            string width = nvc.Get("width");
            string height = nvc.Get("height");
            string id = nvc.Get("id");
            bool isVisible = Boolean.Parse(nvc.Get("isVisible"));
            VentanaUsuario v = new VentanaUsuario { width = Int32.Parse(width), height = Int32.Parse(height), id = id, visible = isVisible };
            result=user.updateVentana(v);
            return Json(result);
        }
        [SessionActionFilter]
        public JsonResult getUsuarioValues()
        {
            usuarioBean usuario = (usuarioBean)Session["usuario"];
            if (usuario.NecesitaReload)
            {
                usuario = usuario.handler.crear(usuario.IdUsuario);
                Session["usuario"] = usuario;
            }
            usuarioBean usuario2 = new usuarioBean();
            usuario2.ventanas = usuario.ventanas;
            usuario2.Perfiles = usuario.Perfiles;
            return Json(usuario2);
        }
        [SessionActionFilter]
        public void recreateWindows(string usuario,string ventanaId)
        {
            usuarioBean yo = (usuarioBean)Session["usuario"];
            yo.handler.crearVentana(ventanaId,yo);
            yo.NecesitaReload = true;
            Session["usuario"] = yo;
        }
        [SessionActionFilter]
        public JsonResult getDependencias(int idJur)
        {
            List<Dependencias> dependencias = new List<Dependencias>();
            using (var dbc = new IncidenciasEntities())
            {
                dependencias = (from u in dbc.Dependencia
                                where u.id_jurisdiccion == idJur
                                orderby u.descripcion
                                select new Dependencias
                                {
                                    idDependencia = u.id_dependencia,
                                    Nombre = u.descripcion
                                }).ToList();
            }
            return Json(dependencias);
        }
        [SessionActionFilter]
        public JsonResult GetDatosTicket()
        {
            HistoricosParametros param = new HistoricosParametros();
            param.FechaDesde = new DateTime(DateTime.Now.Year, 1, 1);
            param.FechaHasta = DateTime.Now;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            //obtengo datos
            if (nvc.Count > 0)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("anual"))
                    {
                        param.TipoReport = "anual";
                        DateTime fechaDesde = DateTime.ParseExact("01/01/"+nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime fechahasta = DateTime.ParseExact("31/12/"+nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        param.FechaDesde = fechaDesde;
                        param.FechaHasta = fechahasta;
                    }
                    if(key.StartsWith("anio"))
                    {
                        DateTime fecha = DateTime.ParseExact(nvc.GetValues(key)[0], "yyyy MMMM", CultureInfo.InvariantCulture);
                        param.anioReporte = fecha.Year;
                        param.mesReporte = fecha.Month;
                    }
                    if(key.StartsWith("xjurisdxmensual"))
                    {
                        param.xJurisdMensual = true;
                        param.idJurisdiccion = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if(key.StartsWith("xmensualTotal"))
                    {
                        param.xMensualTotal = true;
                    }
                    if (key.StartsWith("rangofechadesde"))
                    {
                        param.FechaDesde = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (key.StartsWith("xjurisdiccion"))
                    {
                        param.idJurisdiccion = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if (key.StartsWith("rangofechahasta"))
                    {
                        param.FechaHasta = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (key.StartsWith("tipoReport"))
                    {
                        param.TipoReport = nvc.GetValues(key)[0];
                    }
                    if (key.StartsWith("detalleSubdireccion"))
                    {
                        param.DetalleSubdir = nvc.GetValues(key)[0] == "on";
                    }
                    if (key.StartsWith("tipoTicket"))
                    {
                        param.idTipoTicket = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if (key.StartsWith("prioridadTicket"))
                    {
                        param.idPrioridadTicket = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if (key.StartsWith("servicio"))
                    {
                        param.idServicio = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if (key.StartsWith("jurisdiccion"))
                    {
                        param.idJurisdiccion = Int32.Parse(nvc.GetValues(key)[0]);
                    }
                    if (key.StartsWith("OfT"))
                    {
                        param.OfT = true;
                    }
                }
                ResolucionesVista controladorResoluciones = new ResolucionesTicket();

                result.Detail = controladorResoluciones.getResolucionesParametros(param);
                result.Info = "ok";


            }
            var jsonResult=Json(result);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [SessionActionFilter]
        public JsonResult MarcasPorTipo(int tipo)
        {
            List<MarcaVista> marcas = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                marcas = (from u in dbc.Marca
                          where u.dispositivo == tipo
                          orderby u.descripcion
                          select new MarcaVista
                          {
                              idMarca = u.id_marca,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(marcas);
        }
        #region relations
        [SessionActionFilter]
        public JsonResult GetRelation(int id1)
        {
            List<RelationVista> relations = new List<RelationVista>();
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var unidadc = dbc.UnidadConf.Where(c => c.id_uc == id1).SingleOrDefault();
                    if (unidadc != null)
                    {
                        relations = unidadc.UnidadConf1.Select(c => new RelationVista { Id = c.id_uc, Oblea = c.nombre, TipoEquipo = c.Tipo_equipo.descripcion }).ToList();
                        relations.AddRange(unidadc.UnidadConf2.Select(c => new RelationVista { Id = c.id_uc, Oblea = c.nombre, TipoEquipo = c.Tipo_equipo.descripcion }).ToList());
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(relations);
        }
            [SessionActionFilter]
            public JsonResult DropRelation(int id1, int id2)
            {
                Responses result = new Responses();
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var uc1 = dbc.UnidadConf.Where(c => c.id_uc == id1).Single();
                        var uc2 = uc1.UnidadConf1.Where(c => c.id_uc == id2).Single();
                        uc1.UnidadConf1.Remove(uc2);
                        dbc.SaveChanges();
                        result.Detail = "Relación quitada!!";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
                return Json(result);
            }
            [SessionActionFilter]
            public JsonResult CreateRelation(int id1, int id2)
            {
                Responses result = new Responses();
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var uc1 = dbc.UnidadConf.Where(c => c.id_uc == id1).Single();
                        var uc2 = dbc.UnidadConf.Where(c => c.id_uc == id2).Single();
                        uc1.UnidadConf1.Add(uc2);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
                return Json(result);
            }
        #endregion
        [SessionActionFilter]
        public JsonResult TiposEquipo(string Direccion)
        {
            List<TiposEquipoVista> tipos = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipos = (from u in dbc.Tipo_equipo
                         where Direccion == null | u.Direcciones.nombre == Direccion
                         orderby u.Direcciones.nombre
                         select new TiposEquipoVista
                         {
                             id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(tipos);
        }
        public JsonResult BaseLine(int id)
        {
            List<LineaBaseVista> lineaB = new List<LineaBaseVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var basel = from u in dbc.BaseLines
                            where u.master_id == id
                            orderby u.fecha descending
                            select u;
                foreach (var b in basel)
                {
                    lineaB.Add(new LineaBaseVista
                    {
                        CreadoPor = b.Usuarios.nombre_completo,
                        Data = b.data,
                        Fecha = b.fecha
                    });
                }
            }
            return Json(lineaB);
        }
        public JsonResult UltimosMjes(int idMio,int idOtro,int page)
        {
            List<MensajesChat> mensajes = null;
            int cant = 10 * page;
            using (var dbc = new IncidenciasEntities())
            {
                var m = dbc.LogsChat.Where(d => (d.id_from == idMio & d.id_to == idOtro) | (d.id_to == idMio & d.id_from == idOtro));
                bool hayMas = cant < m.Count();

               mensajes= m.OrderByDescending(c => c.fechaHora).Select(c => new MensajesChat
                    {
                        HayMas=hayMas,
                        Id = c.id,
                        fechahora = c.fechaHora,
                        Id_To = c.id_to,
                        Id_From = c.id_from,
                        Mensaje = c.descripcion,
                        Leido = c.leido.HasValue ? c.leido.Value : false
                    }).Take(10 * page ).ToList();
            }
            mensajes.Sort();
            return Json(mensajes);
        }
        public JsonResult SetMensajeLeido(int id)
        {
            Responses res = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                dbc.LogsChat.Where(c => c.id == id).Single().leido = true;
                dbc.SaveChanges();
                res.Info = "ok";
            }
            return Json(res);
        }
        public JsonResult Mensajes()
        {
            OnlinePersons result = new OnlinePersons();
            HttpSessionStateBase ses = Session;
            var keys = ses.Keys;
            return Json(result);
        }
        public JsonResult ServidorXNombreComun(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Servidor
                         where u.nombreComun.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             NombreComun = u.nombreComun
                         }).Take(10).ToList();
            }
            return Json(items);

        }
        #region Eventos
        public ActionResult Events(DateTime? start, DateTime? end, int idUser)
            {
                List<JsonEvent> events = new List<JsonEvent>();
                using (var dbc = new IncidenciasEntities())
                {
                    var evs = dbc.Eventos.Where(c => c.id_creador == idUser & !(c.fin <= start || c.comienzo >= end));
                    foreach(var v in evs)
                    {
                        events.Add(new JsonEvent
                        {
                            end = v.fin.ToString("s"),
                            start = v.comienzo.ToString("s"),
                            text = v.nombre,
                            id=v.id
                        });
                    }                
                }
                return Json(events);
            }
            public ActionResult CrearEv(DateTime? start, DateTime? end, string name, int idUser)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Eventos nuevoEv = new Eventos
                    {
                        comienzo = start.Value,
                        fin = end.Value,
                        id_creador = idUser,
                        nombre = name,
                        descripción=""
                    };
                    dbc.Eventos.Add(nuevoEv);
                    dbc.SaveChanges();
                    return new JsonResult { Data = new Dictionary<string, object> { { "id", nuevoEv.id } } };
                }
                
            }
            public ActionResult DeleteEv(int id)
            {
                using (var db = new IncidenciasEntities())
                {
                    var toBeDeeleted = (from ev in db.Eventos where ev.id == id select ev).First();
                    db.Eventos.Remove(toBeDeeleted);
                    db.SaveChanges();

                    return new JsonResult { Data = new Dictionary<string, object> { { "id", "" } } };
                }
            }
            public ActionResult MoverEv(DateTime? newStart, DateTime? newEnd, int id)
            {
                using (var db = new IncidenciasEntities())
                {
                    var toBeResized = (from ev in db.Eventos where ev.id == id select ev).First();
                    toBeResized.comienzo = Convert.ToDateTime(newStart);
                    toBeResized.fin = Convert.ToDateTime(newEnd);
                    db.SaveChanges();

                    return new JsonResult { Data = new Dictionary<string, object> { { "id", toBeResized.id } } };
                }
            }
            public ActionResult ResizeEv(DateTime? newStart, DateTime? newEnd, int id)
            {
                using (var db = new IncidenciasEntities())
                {
                    var toBeResized = (from ev in db.Eventos where ev.id == id select ev).First();
                    toBeResized.comienzo = Convert.ToDateTime(newStart);
                    toBeResized.fin = Convert.ToDateTime(newEnd);
                    db.SaveChanges();

                    return new JsonResult { Data = new Dictionary<string, object> { { "id", toBeResized.id } } };
                }
            }
            public class JsonEvent
            {
                public int id { get; set; }
                public string text { get; set; }
                public string start { get; set; }
                public string end { get; set; }
            }
        #endregion

    }
}
