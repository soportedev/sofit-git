﻿using Sofit.DataAccess;
using Sofit.Models.Reglas;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Reglas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sofit.Controllers
{
    public class ReglasController : Controller
    {
        [HttpGet]
        [SessionActionFilter]
        public ActionResult Index(string startIndex)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            Response.Cache.SetValidUntilExpires(false);
            Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            int PageSize = 10;
            ReglasModel modelo = getModel();
            int cantidadPedidos = modelo.MisPedidos.Count;
            int cantidadTickets = modelo.MisTickets.Count;
            double cantPagPedidos = (double)cantidadPedidos / (double)PageSize;
            double cantidadPagPedidos = Math.Ceiling(cantPagPedidos);
            modelo.CantidadPaginasPedidos = Convert.ToInt32(cantidadPagPedidos);

            double ticketsporpag = (double)cantidadTickets / (double)PageSize;
            double cantidadPagTickets = Math.Ceiling(ticketsporpag);
            modelo.CantidadPaginasTickets = Convert.ToInt32(cantidadPagTickets);

            if (startIndex != null)
            {
                int stI = 0;
                Int32.TryParse(startIndex, out stI);
                int skip = 0;
                if (stI != 1)
                {
                    skip = (stI - 1) * PageSize;
                }
                var pedidos = modelo.MisPedidos;
                var tickets = modelo.MisTickets.Skip(skip).Take(PageSize);
                modelo.PaginaActualPedidos = stI;
                modelo.MisPedidos = pedidos.ToList();
                modelo.MisTickets = tickets.ToList();

            }
            else
            {
                var pedidos = modelo.MisPedidos;
                var tickets = modelo.MisTickets.Take(PageSize);
                modelo.PaginaActualPedidos = 1;
                modelo.PaginaActualTickets = 1;
                modelo.MisPedidos = pedidos.ToList();
                modelo.MisTickets = tickets.ToList();
            }
            return View(modelo);
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase fileA)
        {
            //usuarioBean usuario = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            PedidoRegla nuevoPedido = new PedidoRegla();
            NameValueCollection nvc = Request.Form;
            string asunto = nvc.GetValues("asunto")[0];
            string descripcion = nvc.GetValues("descripcion")[0];
            string userAgent = Request.UserAgent;
            string addressReq = Request.UserHostAddress;
            string autorizante = nvc.GetValues("autorizante")[0];
            string jurisdiccion = nvc.GetValues("jurisdiccion")[0];
            string dependencia = nvc.GetValues("dependencia")[0];
            string[] spServ = nvc.GetValues("servicios");
            string[] spProt = nvc.GetValues("protocols");
            int vencimiento = Int32.Parse(nvc.GetValues("vencimiento")[0]);
            //reglas
            int i = 0;
            nuevoPedido.GReglas = new List<GrupoReglas>();
            foreach (string key in nvc.Keys)
            {
                string tipoOrigen = nvc.GetValues("tipoOrigen")[0];
                string tipoDestino = nvc.GetValues("tipoDestino")[0];
                
                if (key.StartsWith("cuilRgla" + i))
                {
                    GrupoReglas reglas = new GrupoReglas();
                    //usuario
                    string cuilRegla = nvc.Get("cuilRgla" + i);
                    UsuarioVista usuarioregla = new UsuarioVista();
                    if (cuilRegla == "Anonimo")
                    {
                        usuarioregla.nombre = cuilRegla;
                        usuarioregla.displayName = "Usuario Anónimo Para Pedido de Reglas";
                        usuarioregla.email = "";
                    }
                    else
                    {
                        string nombreUsuario = nvc.Get("userNameRegla" + i);
                        string dominio = nvc.Get("dominioregla" + i);
                        string email = nvc.Get("emailUsuarioRegla" + i);
                        usuarioregla.nombre = cuilRegla;
                        usuarioregla.dominio = dominio;
                        usuarioregla.displayName = nombreUsuario;
                        usuarioregla.email = email;                        
                    }
                    //origen ec
                    string obleaOrigen = nvc.Get("equipoOrigen" + i);
                    string ipOrigen = nvc.Get("ipOrigen" + i);
                    //destino ec
                    string obleadestino = nvc.Get("equipoDestino" + i);
                    string ipDestino = nvc.Get("ipDestino" + i);
                    //origen red
                    string redorigen = nvc.Get("redOrigen" + i);
                    //destino red                  
                    string redDestino = nvc.Get("redDestino" + i);                    
                    reglas.Usuario = usuarioregla;
                    //ifs
                    if (obleaOrigen != null)
                    {
                        //hay origen pc
                        ExtremosPedido origenec = new ExtremosPedido
                        {
                            Tipo = "Host",
                            Nombre = obleaOrigen,
                            Ip = ipOrigen
                        };
                        reglas.Origen = origenec;
                    }
                    if (redorigen != null)
                    {
                        //hay origen red
                        ExtremosPedido origenred = new ExtremosPedido
                        {
                            Tipo = "Red",
                            Ip = redorigen
                        };
                        reglas.Origen = origenred;
                    }
                    if (obleadestino != null)
                    {
                        ExtremosPedido destinoec = new ExtremosPedido
                        {
                            Tipo = "Host",
                            Nombre = obleadestino,
                            Ip = ipDestino
                        };
                        reglas.Destino = destinoec;
                    }
                    if (redDestino != null)
                    {
                        ExtremosPedido destinored = new ExtremosPedido
                        {
                            Tipo = "Red",
                            Ip = redDestino
                        };
                        reglas.Destino = destinored;
                    }
                    nuevoPedido.GReglas.Add(reglas);
                    i++;
                }
            }
            try
            {
                if (spServ.Length > 0)
                {
                    nuevoPedido.Puertos = new List<PuertosVista>();
                    foreach (var s in spServ)
                    {
                        if (s != "")//puede llegar vacíos
                        {
                            if (s.IndexOf(',') != -1)//puede llegar como vector
                            {
                                var splet = s.Split(',');
                                foreach (var d in splet)
                                {
                                    nuevoPedido.Puertos.Add(new PuertosVista { Numero = Int32.Parse(d) });
                                }
                            }
                            else nuevoPedido.Puertos.Add(new PuertosVista { Numero = Int32.Parse(s) });
                        }
                    }
                }
                if (spProt.Length > 0)
                {
                    nuevoPedido.Protocolos = new List<ProtocoloVista>();
                    foreach (var t in spProt)
                    {
                        if (t != "")
                        {
                            if (t.IndexOf(',') != -1)
                            {
                                var splet = t.Split(',');
                                foreach (var d in splet)
                                {
                                    nuevoPedido.Protocolos.Add(new ProtocoloVista { Nombre = d });
                                }
                            }
                            else nuevoPedido.Protocolos.Add(new ProtocoloVista { Nombre = t });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["message"] = e.Message;
            }
            usuarioBean usuarioSesion = Session["usuario"] as usuarioBean;
            UsuarioVista solicitante = new UsuarioVista
            {
                id = usuarioSesion.IdUsuario,
                nombre = usuarioSesion.Nombre,
                displayName = usuarioSesion.NombreCompleto
            };
            nuevoPedido.TipoDePedido = "Regla de Acceso";
            nuevoPedido.asunto = asunto;
            nuevoPedido.Vencimiento = vencimiento;
            nuevoPedido.Vencimiento = vencimiento;
            nuevoPedido.descripcion = descripcion;
            nuevoPedido.Autorizante = new UsuarioVista { id = Int32.Parse(autorizante) };
            nuevoPedido.Solicitante = solicitante;
            nuevoPedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Iniciado");
            nuevoPedido.Dependencia = new DependenciaVista { idDependencia = Int32.Parse(dependencia) };
            //creacion
            PedidoReglasManager manager = new PedidoReglasManager();
            result = manager.NuevoPedidoRegla(nuevoPedido);
            if (result.Info != "ok")
            {
                TempData["message"] = result.Detail;
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult PedidoAcceso(HttpPostedFileBase fileA)
        {
            Responses result = new Responses();
            PedidoAcceso nuevoPedido = new PedidoAcceso();
            usuarioBean usuarioSesion = Session["usuario"] as usuarioBean;
            string nombreHost = string.Empty;
            string ipHost = string.Empty;
            NameValueCollection nvc = Request.Form;
            string asunto = nvc.Get("asuntoP");
            string descripcion = nvc.Get("descripcionP");
            string userAgent = Request.UserAgent;
            string addressReq = Request.UserHostAddress;            
            UsuarioVista solicitante = new UsuarioVista
            {
                id = usuarioSesion.IdUsuario,
                nombre = usuarioSesion.Nombre,
                displayName = usuarioSesion.NombreCompleto
            };
            string cuilUsuario = nvc.Get("cuilusuarioAcceso");
            string nombreUsuario = nvc.Get("nombreusuarioAcceso");
            string domainAcceso = nvc.Get("domainAcceso");
            string emailUsuario = nvc.Get("emailUsuarioSeleccionadoAcceso");
            string tipoOrigen = nvc.Get("origenP");
            if (tipoOrigen == "Pc-Gobierno")
            {
                nombreHost = nvc.Get("nombreHostP");
                ipHost = nvc.Get("ipHostP");
            }
            string detalleS = nvc.Get("destinoSolP");
            string jurisdiccion = Request["jurisdiccionP"];
            string dependencia = Request["dependenciaP"];
            int vencimiento = Int32.Parse(Request["vencimientoP"]);

            nuevoPedido.asunto = asunto;
            nuevoPedido.descripcion = descripcion;            
            nuevoPedido.Solicitante = solicitante;
            nuevoPedido.Usuario = new UsuarioVista
            {
                nombre = cuilUsuario,
                displayName = nombreUsuario,
                dominio = domainAcceso,
                email = emailUsuario
            };
            nuevoPedido.TipoDePedido = "Acceso Remoto";
            nuevoPedido.TipoOrigen = tipoOrigen;
            nuevoPedido.HostOrigen = nombreHost;
            nuevoPedido.IpOrigen = ipHost;
            nuevoPedido.Detalle = detalleS;
            nuevoPedido.Vencimiento = vencimiento;
            nuevoPedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Iniciado");
            nuevoPedido.Dependencia = new DependenciaVista { idDependencia = Int32.Parse(dependencia) };            

            PedidoReglasManager manager = new PedidoReglasManager();
            result = manager.NuevoPedidoAcceso(nuevoPedido);
            if (result.Info != "ok")
            {
                TempData["message"] = result.Detail;
            }

            return RedirectToAction("Index");
        }
        private ReglasModel getModel()
        {
            ReglasModel modelo = new ReglasModel();
            List<JurisdiccionVista> jur = null;
            List<UsuarioVista> aut = null;
            List<PedidosVista> pedidos = new List<PedidosVista>();
            using (var dbc = new IncidenciasEntities())
            {
                usuarioBean logueado = Session["usuario"] as usuarioBean;
                var ped = dbc.PedidosSeguridad.Where(d => d.Usuarios.nombre == logueado.Nombre | d.Usuarios1.nombre == logueado.Nombre
                | d.Usuarios2.nombre == logueado.Nombre | d.Usuarios3.nombre == logueado.Nombre);

                foreach (var d in ped)
                {
                    //verifico mi rol
                    string mir = string.Empty;
                    if (d.autorizante.HasValue)
                    {
                        if (d.Usuarios1.nombre == logueado.Nombre & mir == string.Empty) mir = "Autorizante";
                    }
                    if (d.aprobador.HasValue)
                    {
                        if (d.Usuarios.nombre == logueado.Nombre) mir = "Aprobador";
                    }                    
                    if (d.Usuarios2.nombre == logueado.Nombre & mir == string.Empty)
                    {
                        mir = "Solicitante";
                    }
                    //usuario verificacion aqui
                    //foreach(var v in d.ReglaPedido)
                    //{
                    //    if (v.Usuarios.nombre == logueado.Nombre) mir = "Usuario";
                    //}
                    PedidosVista p = new PedidosVista
                    {                        
                        Asunto = d.asunto,
                        Descripcion = d.descripcion,
                        Autorizante = d.autorizante != null ? new UsuarioVista { id = d.Usuarios1.id_usuario, nombre = d.Usuarios1.nombre, displayName = d.Usuarios1.nombre_completo } : null,
                        Solicitante = new UsuarioVista { id = d.Usuarios2.id_usuario, nombre = d.Usuarios2.nombre, displayName = d.Usuarios2.nombre_completo },
                        Aprobador = d.aprobador.HasValue ? new UsuarioVista { id = d.Usuarios.id_usuario, nombre = d.Usuarios.nombre, displayName = d.Usuarios.nombre_completo } : null,
                        idEstado = d.id_estado,
                        FechaPedidoDT = d.fecha,
                        idPedido = d.id,
                        DiasVencimiento = d.vencimiento,
                        MiNombre = logueado.Nombre,
                        Jurisdiccion = d.Dependencia.Jurisdiccion.descripcion,
                        Dependencia = d.Dependencia.descripcion,
                        Ticket = d.id_ticket.HasValue ? new TicketVista { Id = d.id_ticket.Value, Numero = d.Incidencias.numero } : null,
                        TipoPedido = d.TipoPedido.nombre,
                        OrigenAcceso = d.tipoOrigenAcceso,
                        UsuarioAcceso = d.usuario_acceso.HasValue ? new UsuarioVista { nombre = d.Usuarios3.nombre, displayName = d.Usuarios3.nombre_completo, dominio = d.Usuarios3.dominio, id = d.usuario_acceso.Value } : null,
                        HostAcceso = d.host_acceso.HasValue ? new HostPedido { Nombre = d.UnidadConf.nombre, Ip = d.ip_host_acceso } : null,
                        DetalleAcceso = d.detalleAcceso,
                        TipoAcceso = d.id_tipo_acceso.HasValue ? d.Tipo_Acceso.nombre : null,
                        DetalleEstados = dbc.EstadosXPedido.Where(c => c.id_pedido == d.id).OrderBy(e => e.fecha_desde).Select(fd => new EstadosPedVista
                        {
                            Estado = fd.EstadosPedidos.nombre,
                            fdesde = fd.fecha_desde,
                            fhasta = fd.fecha_hasta.Value,
                            Observaciones = fd.observaciones,
                            Tecnico = fd.nombreTecnico
                        }).ToList()
                    };
                    p.MiRol = mir;
                    if (d.TipoPedido.nombre == "Regla de Acceso")
                    {
                        p.Reglas = new List<GrupoReglas>();
                        var rules = d.ReglaPedido;
                        foreach (var v in rules)
                        {
                            GrupoReglas gr = new GrupoReglas();
                            gr.Usuario = new UsuarioVista
                            {
                                id = v.Usuarios.id_usuario,
                                nombre = v.Usuarios.nombre,
                                displayName = v.Usuarios.nombre_completo,
                                dominio = v.dominio_usuario,
                                email = v.Usuarios.email
                            };
                            gr.Origen = new ExtremosPedido
                            {
                                Tipo = v.ExtremoPedido.TipoEcPedido.nombre,
                                Ip = v.ExtremoPedido.ip_address,
                                Nombre = v.ExtremoPedido.id_uc.HasValue ? v.ExtremoPedido.UnidadConf.nombre : "N/A"
                            };
                            gr.Destino = new ExtremosPedido
                            {
                                Tipo = v.ExtremoPedido1.TipoEcPedido.nombre,
                                Ip = v.ExtremoPedido1.ip_address,
                                Nombre = v.ExtremoPedido1.id_uc.HasValue ? v.ExtremoPedido1.UnidadConf.nombre : "N/A"
                            };
                            p.Reglas.Add(gr);
                        }
                        p.puertos = d.PuertosXPedido.Select(c => c.puerto.ToString()).ToList();
                        p.protocolos = d.ProtocolosXPedido.Select(c => c.protocolo).ToList();
                    }
                    if (d.TipoPedido.nombre == "Acceso Remoto")
                    {

                    }
                    pedidos.Add(p);
                }
                List<TicketVista> tickets = dbc.Incidencias.Where(c => c.Contactos.dni == logueado.Nombre).Select(d => new TicketVista
                {
                    Id = d.id_incidencias,
                    Numero = d.numero,
                    Estado = d.fecha_fin.HasValue ? "Cerrado" : "Activo",
                    Fecha = d.fecha_inicio
                }).ToList();
                tickets.Sort();
                modelo.MisTickets = tickets;
                pedidos.Sort();
                aut = dbc.Usuarios.Where(c => c.Roles.Any(d => d.Nombre == "Autorizante Reglas")).OrderBy(c => c.nombre_completo).Select(c => new UsuarioVista { id = c.id_usuario, nombre = c.nombre_completo }).ToList();
                jur = dbc.Jurisdiccion.Select(c => new JurisdiccionVista { idJurisdiccion = c.id_jurisdiccion, Nombre = c.descripcion }).OrderBy(d => d.Nombre).ToList();
                modelo.Prioridad = dbc.PrioridadTicket.Select(c => new PrioridadesTicketVista { Id = c.id, Nombre = c.nombre }).ToList();
                //pedidos del solicitante
            }
            modelo.Autorizantes = aut;
            modelo.Jurisdicciones = jur;
            modelo.MisPedidos = pedidos;
            return modelo;
        }
        public JsonResult Autorizado(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.aprobar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Aprobar(int idPedido, string nombreUsuario, string obs, int prioridad)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            p.Prioridad = prioridad;
            try
            {
                result = p.EstadoActual.aprobar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Rechazar(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.rechazar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult Cancelar(int idPedido, string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            PedidoReglasManager pm = new PedidoReglasManager();
            Pedidos p = pm.crear(idPedido);
            try
            {
                result = p.EstadoActual.cancelar(nombreUsuario, obs);
            }
            catch (NotImplementedException e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
    }
}