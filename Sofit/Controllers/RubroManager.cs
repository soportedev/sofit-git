﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Controllers
{
    public class RubroManager
    {
        #region Select

        public List<Rubro> GetRubrosSoporte()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                rubros = db.Rubro.Where(c=>c.Direcciones.nombre=="Soporte Técnico").ToList();
            }
            return rubros;
        }
        public List<Rubro> GetRubrosInfra()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                rubros = db.Rubro.Where(c => c.Direcciones.nombre == "Infraestructura").ToList();
            }
            return rubros;
        }
        public List<Rubro> GetRubrosPadreSoporte()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                var o = from s in db.Rubro
                        where s.id_padre == null & s.Direcciones.nombre=="Soporte Técnico"
                        select s;
                rubros = o.ToList();
            }
            return rubros;
        }
        public List<Rubro> GetRubrosPadreInfra()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                var o = from s in db.Rubro
                        where s.id_padre == null & s.Direcciones.nombre == "Infraestructura"
                        select s;
                rubros = o.ToList();
            }
            return rubros;
        }
        public List<Rubro> GetRubrosTele()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                rubros = db.Rubro.Where(c => c.Direcciones.nombre == "Telecomunicaciones").ToList();
            }
            return rubros;
        }
        public List<Rubro> GetRubrosPadreTele()
        {
            List<Rubro> rubros = new List<Rubro>();
            using (var db = new IncidenciasEntities())
            {
                var o = from s in db.Rubro
                        where s.id_padre == null & s.Direcciones.nombre == "Telecomunicaciones"
                        select s;
                rubros = o.ToList();
            }
            return rubros;
        }
        #endregion
        public List<RubrosVisualizer> GetRubrosHijos(int idPadre)
        {
            List<RubrosVisualizer> rubros = new List<RubrosVisualizer>();
            using (var db = new IncidenciasEntities())
            {
                db.Rubro.Include("id_rubro").Include("nombre").Include("id_padre");
                var o = from s in db.Rubro
                        where s.id_padre == idPadre
                        select new RubrosVisualizer { idPadre = s.id_padre.Value, Nombre = s.nombre, idRubro = s.id_rubro };
                rubros = o.ToList();
            }
            return rubros;
        }
        #region Insert
       
        public Responses nuevoRubro(Rubro rubro)
        {
            Responses result = new Responses();
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    var o = from s in db.Rubro
                            where s.nombre == rubro.nombre & s.id_direccion==rubro.id_direccion
                            select s;
                    if (o.Count() > 0)
                    {
                        result.Info = "Error";
                        result.Detail = "Ya existe un Rubro con ese nombre";
                    }
                    else
                    {                        
                        db.Rubro.Add(rubro);
                        db.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Info = "Error";
                result.Detail = ex.Message;
            }
            return result;
        }        

        #endregion

        internal Responses Delete(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var db = new IncidenciasEntities())
                {                    
                    var rub = db.Rubro.Find(id);
                    db.Rubro.Remove(rub);
                    db.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Info = "error";
                result.Detail = ex.Message;
            }
            return result;
        }

    }
}