﻿using soporte.Controllers;
using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sofit.Controllers
{
    [SessionActionFilter]
    public class AccesoController : Controller
    {
        // GET: Acceso
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            return View(usuario);
        }
    }
}