﻿using soporte.Areas.Soporte.Models.Statics;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Controllers
{
    public class ProductoManager
    {
        usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
        public Responses nuevoProducto(Producto p, HttpPostedFileBase file)
        {
            Responses result = new Responses();      
            
            Producto productoExistente = getProdByName(p.nombre);

            if (productoExistente!=null)
            {
                if (productoExistente.vigente == 0)
                {
                    Rubro rubro = null;
                    using (var ctx = new IncidenciasEntities())
                    {
                       rubro = ctx.Rubro.Where(s => s.id_rubro == p.id_rubro).FirstOrDefault<Rubro>();
                    }
                    
                    productoExistente.stock = p.stock;
                    productoExistente.path_imagen = p.path_imagen;
                    productoExistente.vigente = 1;
                    p.Rubro = rubro;
                    if (productoExistente.id_rubro != p.id_rubro) { productoExistente.codigo = generarCodigo(p); }
                    productoExistente.descripcion = p.descripcion;
                    result = actualizarProducto(productoExistente, file);
                }
                else
                {
                    result.Info = "Error";
                    result.Detail = "Ya existe un Producto con ese nombre";
                }
            }
            else
            {
                result = saveProduct(p, file);
            }
                    
            return result;            
        }

        private Responses actualizarProducto(Producto p, HttpPostedFileBase file)
        {
            Responses result = new Responses();
            using (var db = new IncidenciasEntities())
            {
                if (file != null) saveImage(file);                
                //db.Producto.Attach(p);
                db.Entry(p).State = EntityState.Modified;

                Movimiento mov = new Movimiento();
                mov.cantidad = p.stock;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from id in db.TipoMovimiento
                                   where id.nombre == "Nuevo"
                                   select id.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                db.Movimiento.Add(mov);

                db.SaveChanges();
                result.Info = "ok";
                result.Detail = p.codigo;
            }
            return result;
        }
        private string generarCodigo(Producto p)
        {
            StringBuilder codigoBuilder = new StringBuilder();            
            string s1 = p.Rubro.nombre.PadRight(3);
            s1 = s1.Substring(0, 3);
            codigoBuilder.Append(s1);
            codigoBuilder.Append(p.id_prod.ToString("D3"));
            return codigoBuilder.ToString();
        }
        private Responses saveProduct(Producto p, HttpPostedFileBase file)
        {
            Responses result = new Responses();
            using (var db = new IncidenciasEntities())
            {
                if (file != null) saveImage(file);
              
                db.Producto.Add(p);
                db.SaveChanges();
                db.Entry(p).Reference("Rubro").Load();
                
                p.codigo = generarCodigo(p);
                p.vigente = 1;
                db.Producto.Attach(p);
                db.Entry(p).State = EntityState.Modified;

                Movimiento mov = new Movimiento();
                mov.cantidad = p.stock;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from id in db.TipoMovimiento
                                   where id.nombre == "Nuevo"
                                   select id.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                db.Movimiento.Add(mov);

                db.SaveChanges();
                
                result.Info = "ok";
                result.Detail = p.codigo;
            }
            return result;
        }

        private void saveImage(HttpPostedFileBase file)
        {
            var path = PathImage.getCustomImagenDefault(file.FileName);
            var stream = file.InputStream;                       
            using (var fileStream = System.IO.File.Create(path))
            {
                stream.CopyTo(fileStream);
            }
        }
        private void dropImage(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception)
            {
                
            }
        }

        internal List<Producto> getProductosVigentesSoporte()
        {
            List<Producto> productos = new List<Producto>();
            using (var db = new IncidenciasEntities())
            {
                productos = (from pr in db.Producto.Include("UbicacionProducto").Include("Rubro")
                             where pr.vigente == 1 & pr.Direcciones.nombre=="Soporte Técnico"
                             select pr).ToList();
            }
            return productos;
        }
        internal List<Producto> getProductosVigentesInfra()
        {
            List<Producto> productos = new List<Producto>();
            using (var db = new IncidenciasEntities())
            {
                productos = (from pr in db.Producto.Include("UbicacionProducto").Include("Rubro")
                             where pr.vigente == 1 & pr.Direcciones.nombre == "Infraestructura"
                             select pr).ToList();
            }
            return productos;
        }
        private Producto getProdByName(string name)
        {
            Producto producto=null;
            using (var db = new IncidenciasEntities())
            {
                producto = db.Producto.Include("UbicacionProducto").Include("Rubro").Where(s=>s.nombre==name).FirstOrDefault<Producto>();
                //from pr in db.Producto.Include("UbicacionProducto").Include("Rubro")
                //             where pr.nombre.Equals(name)
                //             select pr).ToList();
                //if (productos.Count > 0) producto = productos.First();
            }
            
            return producto;
        }

        internal Producto getProdXId(int id)
        {
            Producto p;           
            using (var db = new IncidenciasEntities())
            {                
                p = (from o in db.Producto.Include("UbicacionProducto").Include("Rubro")
                         .Include("Dependencia").Include("ImagenProducto").Include("Movimiento")
                         where o.id_prod == id && o.vigente==1
                         select o).First();
            }            
            return p;
        }

        internal Responses dropProducto(int id)
        {
            Responses result = new Responses();
            Producto p = getProdXId(id);
            using (var db = new IncidenciasEntities())
            {               
                if (p.path_imagen != PathImage.getHtmlImageDefault())
                {
                    dropImage(p.path_imagen);
                }
                p.vigente = 0;
                db.Entry(p).State = EntityState.Modified;

                Movimiento mov = new Movimiento();
                mov.cantidad = 0;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from idM in db.TipoMovimiento
                                   where idM.nombre == "Eliminación"
                                   select idM.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                db.Movimiento.Add(mov);

                db.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }

        internal Responses updateStock(int id, int cantidad,int idArmario, HttpPostedFileBase file,string descripcion,string observacion)
        {
            Responses result = new Responses();
            string path = PathImage.getHtmlImageDefault();
            using (var db = new IncidenciasEntities())
            {
                Producto p = (from o in db.Producto
                              where o.id_prod == id
                              select o).First();
                p.stock = cantidad;
                p.descripcion = descripcion;
                p.id_ubicacion = idArmario;
                if (file != null)
                {
                    path = PathImage.getHtmlDirectoryDefault() + file.FileName;
                    saveImage(file);
                }
                if (!p.path_imagen.Equals(path))
                {                    
                    dropImage(p.path_imagen);
                    p.path_imagen = path;                   
                }
                Movimiento mov = new Movimiento();
                mov.cantidad = p.stock;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from idM in db.TipoMovimiento
                                   where idM.nombre == "Ajuste Manual"
                                   select idM.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                mov.descripcion = observacion;
                db.Movimiento.Add(mov);

                db.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }

        internal Responses addStock(int id, int cant, string observaciones)
        {
            Responses result = new Responses();           
            using (var db = new IncidenciasEntities())
            {
                Producto p = (from o in db.Producto
                              where o.id_prod == id
                              select o).First();
                p.stock += cant;
               
                Movimiento mov = new Movimiento();
                mov.cantidad = cant;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from idM in db.TipoMovimiento
                                   where idM.nombre == "Agregación"
                                   select idM.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                mov.descripcion = observaciones;
                db.Movimiento.Add(mov);

                db.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }

        internal Responses retirarProductos(int idProd, int cant, string idEquipo,string tecnico,string observaciones)
        {
            Responses result = new Responses();
            using (var db = new IncidenciasEntities())
            {
               
                Producto p = (from o in db.Producto
                              where o.id_prod == idProd
                              select o).First();
                p.stock -= cant;

                Movimiento mov = new Movimiento();
                mov.cantidad = cant;
                mov.fecha_hora = DateTime.Now;
                mov.id_tipo_mov = (from idM in db.TipoMovimiento
                                   where idM.nombre == "Retiro"
                                   select idM.id_tipo_mov).First();
                mov.id_prod = p.id_prod;
                mov.id_usuario = usuario.IdUsuario;
                mov.descripcion = observaciones;
                db.Movimiento.Add(mov);

                 if (idEquipo != "na")
                {
                    int idEqu = Int32.Parse(idEquipo);
                    var equipo = (from u in db.UnidadConf where u.id_uc == idEqu select u).Single();
                    
                    mov.UnidadConf.Add(equipo);                                                   
                }
                 if (tecnico != "na")
                 {
                     int idT = Int32.Parse(tecnico);
                     var tec = (from u in db.tecnico where u.id_tecnico == idT select u).Single();
                     mov.tecnico.Add(tec);
                 }
                db.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }

        internal List<Producto> getProductosVigentesTele()
        {
            List<Producto> productos = new List<Producto>();
            using (var db = new IncidenciasEntities())
            {
                productos = (from pr in db.Producto.Include("UbicacionProducto").Include("Rubro")
                             where pr.vigente == 1 & pr.Direcciones.nombre == "Telecomunicaciones"
                             select pr).ToList();
            }
            return productos;
        }
    }
}