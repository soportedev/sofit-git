﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class UbicacionManager
    {
        #region Select

        public List<UbicacionProducto> GetUbicacionesSoporte()
        {
            List<UbicacionProducto> ubicaciones = new List<UbicacionProducto>();
            using (var db = new IncidenciasEntities())
            {
                ubicaciones = db.UbicacionProducto.Where(c=>c.Direcciones.nombre=="Soporte Técnico").ToList();
            }
            return ubicaciones;
        }
        public List<UbicacionProducto> GetUbicacionesInfra()
        {
            List<UbicacionProducto> ubicaciones = new List<UbicacionProducto>();
            using (var db = new IncidenciasEntities())
            {
                ubicaciones = db.UbicacionProducto.Where(c => c.Direcciones.nombre == "Infraestructura").ToList();
            }
            return ubicaciones;
        }
        public List<UbicacionProducto> GetUbicacionesTele()
        {
            List<UbicacionProducto> ubicaciones = new List<UbicacionProducto>();
            using (var db = new IncidenciasEntities())
            {
                ubicaciones = db.UbicacionProducto.Where(c => c.Direcciones.nombre == "Telecomunicaciones").ToList();
            }
            return ubicaciones;
        }
        #endregion

        #region Insert
        [HttpPost]
        public Responses nuevoArmario(UbicacionProducto ubicacion)
        {
            Responses result=new Responses();
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    var o = from s in db.UbicacionProducto
                            where s.nombre == ubicacion.nombre & s.id_direccion==ubicacion.id_direccion
                            select s;
                    if (o.Count() > 0)
                    {
                        result.Info = "Error";
                        result.Detail = "Ya existe un Armario con ese nombre";
                    }
                    else
                    {                        
                        db.UbicacionProducto.Add(ubicacion);
                        db.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Info = ex.Message;
            }
            return result;
        }
        public Responses InsertTele(UbicacionProducto ubicacion)
        {
            Responses result = new Responses();
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    var o = from s in db.UbicacionProducto
                            where s.nombre == ubicacion.nombre & s.Direcciones.nombre == "Telecomunicaciones"
                            select s;
                    if (o.Count() > 0)
                    {
                        result.Info = "Error";
                        result.Detail = "Ya existe un Armario con ese nombre";
                    }
                    else
                    {
                        var direccion = (from s in db.Direcciones where s.nombre == "Telecomunicaciones" select s).Single();
                        ubicacion.Direcciones = direccion;
                        db.UbicacionProducto.Add(ubicacion);
                        db.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Info = ex.Message;
            }
            return result;
        }
        #endregion 
    
        internal object Delete(string id)
        {
             Responses result=new Responses();
             try
             {
                 using (var db = new IncidenciasEntities())
                 {
                     int ids = Int32.Parse(id);
                     var armary = db.UbicacionProducto.Find(ids);
                     db.UbicacionProducto.Remove(armary);
                     db.SaveChanges();
                     result.Info = "ok";
                 }
             }
             catch (Exception ex)
             {
                 result.Info = "error";
                 result.Detail = ex.Message;
             }
             return result;
        }

        internal Ubicacion GetUbicacion(int idArmario)
        {
            //Responses result = new Responses();
            Ubicacion u = new Ubicacion();
            try
            {
                using (var db = new IncidenciasEntities())
                {                    
                    var armary = db.UbicacionProducto.Find(idArmario);
                    u.Id = armary.id_ubicacion;
                    u.Nombre = armary.nombre;
                    u.Descripcion = armary.descripcion;                    
                }
            }
            catch (Exception )
            {
                
            }
            return u;
        }

        internal Responses ubicarEquipo(int idEquipo, int idArmario)
        {
            Responses result = new Responses();
            removeUbicacionEquipo(idEquipo);
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    var ubi = db.UbicacionProducto.Where(c => c.id_ubicacion == idArmario).Single();
                    var equipo=db.UnidadConf.Where(c => c.id_uc == idEquipo).Single();
                    if(equipo.UbicacionProducto.Any()){
                        equipo.UbicacionProducto = null;
                    }
                    equipo.UbicacionProducto.Add(ubi);
                    db.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Info = "Error";
                result.Detail = e.Message;
            }
            return result;
        }
        internal void removeUbicacionEquipo(int idEquipo)
        {
            try
            {
                using(var db=new IncidenciasEntities())
                {
                    var equipo=db.UnidadConf.SingleOrDefault(c => c.id_uc == idEquipo);
                    
                    if (equipo.UbicacionProducto.Any())
                    {
                        equipo.UbicacionProducto = null;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        internal Ubicacion GetUbicacionEquipo(int idEquipo)
        {   
            
            Ubicacion u = new Ubicacion();
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    var eq = from s in db.UnidadConf
                             where s.id_uc == idEquipo
                             select s;
                    var ubi=eq.Single().UbicacionProducto;
                    if(ubi.Any())
                    {
                        u.Id = ubi.Single().id_ubicacion;
                        u.Nombre = ubi.Single().nombre;
                    }
                    
                }
            }
            catch (Exception)
            {

            }
            return u;
        }
    }
}