﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace soporte.Controllers
{
    public class ExcelController : Controller
    {
        //
        // GET: /Excel/

        public void getFile()
        {
            Response.Clear();

            Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
            Response.AddHeader("Content-Type", "application/Excel");
            Response.Charset = "";

            // If you want the option to open the Excel file without saving than

            // comment out the line below

            // Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.xls";
            Response.ContentEncoding = Encoding.Unicode;
            Response.BinaryWrite(Encoding.Unicode.GetPreamble());
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            DataTable f = (DataTable)Session["tableSource"];
            
                DataGrid grid = new DataGrid();
                grid.Attributes.Add("htmlencode", "false");
                grid.DataSource = f;
                grid.DataBind();
                grid.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.End();
            
            //return View();
        }

    }
}
