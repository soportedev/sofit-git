﻿using soporte.Areas.Soporte.Models.Displays;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Controllers
{
    public class ImpresionController : Controller
    {
        //
        // GET: /Impresion/

        public ActionResult Index(int idInc)
        {
            IncidenteImpresion inc = null;
            using(var dbc=new IncidenciasEntities())
            {
                var incid = dbc.Incidencias.Where(c => c.id_incidencias == idInc).Single();
                string nombreCliente = "Sin cliente cargado";
                string direccionCliente = "Sin dirección cargada";
                string teCliente = "Sin TE cargado";
                string tipificacion = incid.Tipificaciones.nombre;
                if(incid.Contactos!=null){
                    nombreCliente = incid.Contactos.nombre;
                    direccionCliente = incid.Contactos.direccion != null ? incid.Contactos.direccion : "Sin dirección cargada";
                    teCliente = incid.Contactos.te != null ? incid.Contactos.te : "Sin TE cargado";
                }
                string pathImg = "";
                string pathFile= "";
                string pathImagen=incid.ImagenIncidencia.Count > 0 ? incid.ImagenIncidencia.Single().path : "";
                string pathArchivo=incid.AdjuntoIncidencia.Count > 0 ? incid.AdjuntoIncidencia.Single().path : "";
                if(pathImagen!=""){
                    string nombreImagen = pathImagen.Split('\\').Last();
                    pathImg=PathImage.getHtmlDirectoryDefault()+nombreImagen;
                }
                if(pathArchivo!=""){
                    string nombreArchivo=pathArchivo.Split('\\').Last();
                    pathFile=PathImage.getHtmlFileDirectory()+nombreArchivo;
                }
                inc = new IncidenteImpresion
                {
                    id = incid.id_incidencias,
                    Fecha = incid.fecha_inicio,
                    Cliente = nombreCliente + ", " + direccionCliente + ", " + teCliente,
                    Descripcion = incid.descripcion,
                    Generador = incid.Direcciones.nombre,
                    Numero = incid.numero,
                    Tipificacion = tipificacion,
                    TipoTicket = incid.TipoTicket.nombre,
                    PrioridadTicket = incid.PrioridadTicket.nombre,
                    Servicio=incid.Tipificaciones.Servicios.nombre,
                    TiempoServicio=incid.Tipificaciones.Servicios.tiempo_res.Value,
                    TiempoMargen=incid.Tipificaciones.Servicios.tiempo_margen.Value,
                    imgPath = pathImg,
                    filePath = pathFile,
                    Notificados = incid.NotificacionXIncidente.Select(c => new ReferentesVista { email = c.email }).ToList()
                };
                if (incid.Dependencia != null)
                {
                    inc.Dependencia = incid.Dependencia.descripcion;
                    inc.Jurisdiccion = incid.Dependencia.Jurisdiccion.descripcion;
                }
                inc.resoluciones = (from u in dbc.Resoluciones
                                    where u.id_incidencia == idInc
                                    select new ResToDisplay
                                    {
                                        Fecha = u.fecha,
                                        Observ = u.observaciones,
                                        Tecnico = u.tecnico.descripcion,
                                        Equipo = u.UnidadConf != null ? u.UnidadConf.nombre : "N/A",
                                        TipoRes = u.tipo_resolucion.descripcion
                                    }).ToList();
                inc.resoluciones.Sort();
            }
            return View(inc);
        }

    }
}
