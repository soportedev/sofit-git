//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class EstadoXEnlace
    {
        public int id_incidencia { get; set; }
        public int id_estado { get; set; }
        public int id_enlace { get; set; }
        public System.DateTime fecha_inicio { get; set; }
        public Nullable<System.DateTime> fecha_fin { get; set; }
        public int id_tecnico { get; set; }
    
        public virtual Estado_UC Estado_UC { get; set; }
        public virtual Incidencias Incidencias { get; set; }
        public virtual tecnico tecnico { get; set; }
        public virtual EnlaceComunicaciones EnlaceComunicaciones { get; set; }
    }
}
