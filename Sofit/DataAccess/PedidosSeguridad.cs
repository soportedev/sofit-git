//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class PedidosSeguridad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PedidosSeguridad()
        {
            this.EstadosXPedido = new HashSet<EstadosXPedido>();
            this.ProtocolosXPedido = new HashSet<ProtocolosXPedido>();
            this.PuertosXPedido = new HashSet<PuertosXPedido>();
            this.ReglaPedido = new HashSet<ReglaPedido>();
        }
    
        public int id { get; set; }
        public System.DateTime fecha { get; set; }
        public string asunto { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> autorizante { get; set; }
        public int solicitante { get; set; }
        public Nullable<int> aprobador { get; set; }
        public int id_estado { get; set; }
        public int id_dependencia { get; set; }
        public Nullable<int> id_tipo_pedido { get; set; }
        public Nullable<System.DateTime> fecha_fin { get; set; }
        public int vencimiento { get; set; }
        public Nullable<int> id_ticket { get; set; }
        public Nullable<int> id_prioridad { get; set; }
        public Nullable<int> usuario_acceso { get; set; }
        public Nullable<int> host_acceso { get; set; }
        public string ip_host_acceso { get; set; }
        public string detalleAcceso { get; set; }
        public string tipoOrigenAcceso { get; set; }
        public Nullable<int> id_tipo_acceso { get; set; }
    
        public virtual Dependencia Dependencia { get; set; }
        public virtual EstadosPedidos EstadosPedidos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstadosXPedido> EstadosXPedido { get; set; }
        public virtual Incidencias Incidencias { get; set; }
        public virtual UnidadConf UnidadConf { get; set; }
        public virtual PrioridadTicket PrioridadTicket { get; set; }
        public virtual TipoPedido TipoPedido { get; set; }
        public virtual Tipo_Acceso Tipo_Acceso { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProtocolosXPedido> ProtocolosXPedido { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PuertosXPedido> PuertosXPedido { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReglaPedido> ReglaPedido { get; set; }
        public virtual Usuarios Usuarios { get; set; }
        public virtual Usuarios Usuarios1 { get; set; }
        public virtual Usuarios Usuarios2 { get; set; }
        public virtual Usuarios Usuarios3 { get; set; }
    }
}
