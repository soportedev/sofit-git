//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class EnlaceComunicaciones
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EnlaceComunicaciones()
        {
            this.EnlaceXIncidencia = new HashSet<EnlaceXIncidencia>();
            this.EstadoXEnlace = new HashSet<EstadoXEnlace>();
        }
    
        public int id_enlace { get; set; }
        public int id_uc { get; set; }
        public Nullable<int> id_tipo_enlace { get; set; }
        public int id_estado { get; set; }
        public string descripcion { get; set; }
        public int id_proveedor { get; set; }
        public string nro_ref { get; set; }
        public string lan { get; set; }
        public Nullable<int> bw { get; set; }
        public string wan { get; set; }
        public string loopback { get; set; }
        public Nullable<int> id_contacto { get; set; }
        public Nullable<int> id_dependencia { get; set; }
    
        public virtual AnchoBanda AnchoBanda { get; set; }
        public virtual Dependencia Dependencia { get; set; }
        public virtual Estado_UC Estado_UC { get; set; }
        public virtual ProveedoresEnlace ProveedoresEnlace { get; set; }
        public virtual TipoEnlace TipoEnlace { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EnlaceXIncidencia> EnlaceXIncidencia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstadoXEnlace> EstadoXEnlace { get; set; }
        public virtual UnidadConf UnidadConf { get; set; }
        public virtual ContactosOperaciones ContactosOperaciones { get; set; }
    }
}
