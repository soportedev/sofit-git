//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Insumos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Insumos()
        {
            this.Modelo_Impresora = new HashSet<Modelo_Impresora>();
        }
    
        public int id_insumo { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> id_tipo_insumo { get; set; }
        public Nullable<int> id_marca { get; set; }
    
        public virtual Marca Marca { get; set; }
        public virtual Tipo_insumo Tipo_insumo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Modelo_Impresora> Modelo_Impresora { get; set; }
    }
}
