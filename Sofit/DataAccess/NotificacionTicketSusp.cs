//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class NotificacionTicketSusp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NotificacionTicketSusp()
        {
            this.DireccionMailXNotTicket = new HashSet<DireccionMailXNotTicket>();
        }
    
        public int id { get; set; }
        public string mensaje { get; set; }
        public string asunto { get; set; }
        public System.DateTime fecha { get; set; }
        public int id_usuario { get; set; }
        public int id_ticket { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DireccionMailXNotTicket> DireccionMailXNotTicket { get; set; }
        public virtual Incidencias Incidencias { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}
