//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sofit.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class area
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public area()
        {
            this.AreaXEquipoHistorico = new HashSet<AreaXEquipoHistorico>();
            this.AreaXEquipoN = new HashSet<AreaXEquipoN>();
            this.AreaXIncidencia = new HashSet<AreaXIncidencia>();
            this.AreaXIncidenciaHistorico = new HashSet<AreaXIncidenciaHistorico>();
            this.EquiposXIncidencia = new HashSet<EquiposXIncidencia>();
            this.IncidenciasActivas = new HashSet<IncidenciasActivas>();
            this.ResolucionesHistorico = new HashSet<ResolucionesHistorico>();
            this.tipo_resolucion = new HashSet<tipo_resolucion>();
            this.tipo_resolucion1 = new HashSet<tipo_resolucion>();
            this.Resoluciones = new HashSet<Resoluciones>();
            this.tecnico = new HashSet<tecnico>();
        }
    
        public int id_area { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> id_direccion { get; set; }
    
        public virtual Direcciones Direcciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AreaXEquipoHistorico> AreaXEquipoHistorico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AreaXEquipoN> AreaXEquipoN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AreaXIncidencia> AreaXIncidencia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AreaXIncidenciaHistorico> AreaXIncidenciaHistorico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EquiposXIncidencia> EquiposXIncidencia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IncidenciasActivas> IncidenciasActivas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ResolucionesHistorico> ResolucionesHistorico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tipo_resolucion> tipo_resolucion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tipo_resolucion> tipo_resolucion1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Resoluciones> Resoluciones { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tecnico> tecnico { get; set; }
    }
}
