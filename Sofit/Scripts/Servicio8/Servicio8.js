﻿$(function () {
    var elemModObs = document.getElementById('modalObs');    
    var options = {
        onOpenStart: function () {
            var action = this.el.attributes['data-url'].textContent;
            $(this.el).find('.modal-content h6').html('Ingrese un comentario si desea para ' + action + ' el pedido');
        }
    }
    var optionsColl = {}
    var obsModalInstance = M.Modal.init(elemModObs, options);    
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, optionsColl);
    $('.tooltipped').tooltip();
    var tooltEl = document.querySelectorAll('.tooltipped');
    var tooltInst = M.Tooltip.init(tooltEl, {});
    $('select').formSelect();
    $('body').on('click', '.btnAction', function (e) {
        var urlAttr = $(this).attr('data-url');
        var dataId = $(this).attr('data-idPedido');
        var userN = $('#user').text();       
        var priority = null;
        var autorizante2 = null;
        var aprobador = null;
        var tipoAcceso = null;
        var div = $(this).closest('div.collapsible-body');
        var estadoActual = $('.estadoActual', div);
        if (urlAttr == 'Aprobar' & (estadoActual.val() == "Iniciado" | estadoActual.val() == "Verificar")) {
            priority = $('.priority', div);          
            if (!priority.val()) {
                mostrarError("Seleccione la prioridad");
                return;
            }
            else {
                $('#modalObs').attr('data-prioridad', priority.val());
            }
        }
        if (urlAttr == "Para Aprobar") {
            autorizante2 = $('.selAutorizante',div).val();
            tipoAcceso = $('.tipoAcceso',div).val();
            if (!autorizante2) {
                mostrarError("Seleccione el autorizante");
                return;
            }
            if (!tipoAcceso) {
                mostrarError("Seleccione el tipo de Acceso");
                return;
            }
        }
        if (urlAttr == 'Para Autorizar') {            
            autorizante2 = $('select.autorizante2',div).val();
            if (!autorizante2) {
                mostrarError("Seleccione el autorizante");
                return;
            }            
        }
        $('#modalObs').attr('data-url', urlAttr).attr('data-idPedido', dataId).attr('data-user', userN);        
        if (autorizante2) {
            $('#modalObs').attr('data-autorizante2', autorizante2);
        }
        if (tipoAcceso) {
            $('#modalObs').attr('data-tipoAcceso', tipoAcceso);
        }
        $('div.errorTxtObs').html('');
        $('#observ').val('');
        obsModalInstance.open();  
    });
    $('body').on('click', '.btnMov', function (e) {
        var modal = $(this).siblings('div.modal');
        var modInst = M.Modal.init(modal[0], {});
        modInst.open();
    });
    $('#enviarObs').on('click', function (e) {
        var attribs = this;
        var observaciones = $('#observ').val();
        var modal = obsModalInstance;
        var url = modal.el.attributes['data-url'].textContent;
        if (url == 'Para Autorizar') {
            url = 'PedirAutorizacion';
        }
        if (url == 'Para Aprobar') {
            url = 'ParaAprobar';
        }
        var idPedido = modal.el.attributes['data-idPedido'].textContent;
        var userN = modal.el.attributes['data-user'].textContent;
        var prioridad = modal.el.attributes['data-prioridad'];
        var autorizante2 = modal.el.attributes['data-autorizante2'];
        var tipoAcc = modal.el.attributes['data-tipoAcceso'];
        var data = {
            'idPedido': idPedido,
            'nombreUsuario': userN,
            'obs': observaciones
        }
        if (prioridad) {
            data['prioridad'] = prioridad.textContent;
        }
        if (autorizante2) {
            data['autorizante2'] = autorizante2.textContent;
        }
        if (tipoAcc) {
            data['tipoAcceso'] = tipoAcc.textContent;
        }
        $.ajax({
            url: '/Servicio8/' + url,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.Info == 'ok') {

                    document.body.onbeforeunload = function () {
                        return null;
                    }

                    mostrarExito(data.Detail);
                    window.location.reload();
                } else {
                    mostrarError(data.Detail);
                }
            }
        });
    });
    /* CONTROLES */
    // SELECT
    var Filtros = document.querySelectorAll('.selectFiltro'); // Busca todos los select de filtros
    for (var i = 0; i < Filtros.length; i++) {
        Filtros[i].addEventListener('change', AplicarFiltros); // Listener para los filtros
    }    
    ActualizarSelects();
});
function ActualizarSelects() {
    var ifSetEstado = getParameterByName("es");
    var ifsetword = getParameterByName("palabra");
    $('select', '#estado').each(function () {
        if (this.id == "es") {
            var text = $(this).find('option:selected').text();
            if (ifSetEstado) $(this).val(ifSetEstado);
        }        
        $(this).formSelect();
    });
    if (ifsetword) {
        $('#palabra').val(ifsetword);
    }
}
function AplicarFiltros() {
    var form = $(this).closest('form');
    form.submit();
}