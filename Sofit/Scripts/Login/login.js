﻿$(function () {
    // Fondo aleatorio
    var num = 'bg' + (Math.floor(Math.random() * 5) + 1);    
    switch (num) {
        case "bg1":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-1.jpg)");
            break;
        case "bg2":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-2.jpg)");
            break;
        case "bg3":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-3.jpg)");
            break;
        case "bg4":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-4.jpg)");
            break;
        case "bg5":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-5.jpg)");
            break;
    }
    var errorMess = $('#errorMessage').val();
    if (errorMess.length > 0) LoginError(errorMess);
    //-->
    var navig = navigator.userAgent;
    var esEdge = window.navigator.userAgent.toLowerCase().indexOf("edge") > -1;
    var esOpera = window.navigator.userAgent.toLowerCase().indexOf("opera") > -1;   
    if (esEdge | esOpera) {
        $.reject({
            reject: {
                all:true     
            },
            header: 'Su navegador no está soportado', // Header Text  
            paragraph1: 'Está usando un navegador no soportado', // Paragraph 1  
            paragraph2: 'Puede instalar un navegador soportado haciendo click en el enlace. Para reintentar presione ' +
            '<a href="javascript:window.location=window.location.pathname;">Volver a cargar</a>',
            display: ['firefox', 'chrome'],
            close: false,
            imagePath: '/Content/Login/images/'
        }); // Customized Browsers  
    } else {
        $.reject({
            reject: {
                safari: true, // Apple Safari                
                msie: true, // Microsoft Internet Explorer  
                opera: true, // Opera  
                konqueror: true, // Konqueror (Linux)  
                unknown: true // Everything else              
            },
            header: 'Su navegador no está soportado', // Header Text  
            paragraph1: 'Está usando un navegador no soportado', // Paragraph 1  
            paragraph2: 'Puede instalar un navegador soportado haciendo click en el enlace. Para reintentar presione ' +
            '<a href="javascript:window.location=window.location.pathname;">Volver a cargar</a>',
            display: ['firefox', 'chrome'],
            close: false,
            imagePath: '/Content/Login/images/'
        }); // Customized Browsers  
    }
    // Deteccion de JavaScript deshabilitado
    //$('#outdatedBrowser').fadeOut(0);
    //-->
    $('button.logins').on('click', function () {
        var url = $(this).attr('data-url');
        //var url = '';
        $('#LoginForm').attr('action', url);
        
        $('#LoginErrorIcon, #LoginError').hide();
        var userName = $.trim($('#tNombre').val());
        var passw = $.trim($('#tPassword').val());
        if (userName.length > 7 & passw.length > 7) {
            $('#LoginForm').submit();            
        }else LoginError("Ingrese su usuario y contraseña")
    });
    
    $('input.mdl-textfield__input').on('keyup', function (e) {
        var thisk = e.keyCode;
        if (thisk == 13) $('#iniciar').trigger('click');       
    });
    CapsLock.addListener(function (isOn) {
        if (isOn) {
            $('#Caps').slideDown(400);
        }
        else $('#Caps').slideUp(400);
    });
    $('body').on('keyup', function (e) {        
        if (CapsLock.isOn()) {
            $('#Caps').slideDown(400);
        }
        else {
            $('#Caps').slideUp(400);
        }
    });
})
// Login ERROR
function LoginError(message) {
    $('#LoginError span').html(message);
    $('#LoginErrorIcon, #LoginError').show();
}
