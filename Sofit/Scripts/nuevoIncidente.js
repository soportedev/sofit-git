﻿//var Nice = false;//objeto para ns
$(function () {
    window.reqAuto=true;
    window.prevTerm='';
    window.imagenIncidente = null;
    $('.dJurisdiccion').on('change', function (event,dDep,idDependencia) {
        var context = $(this).parents('.dContent');
        var selDependencias = $('.dDependencia',context);
        var idJur = $(this).val();        
        $.post('/Service/getDependencias', { 'idJur': idJur }, function (data) {
            selDependencias.html('');
            $.each(data, function (i, v) {
                var option = $('<option>').attr('value', v.idDependencia).text(v.Nombre);
                selDependencias.append(option);
            });
            if (dDep != null) {
                setSelectByVal(dDep, idDependencia);
            }
        });        
    });
    $('#AceptarCl').click(function () {
        AceptarCl();
    })
    $('#EditarCl').click(function () {
        EditarCl();
    })
    $('#CancelarCl').click(function () {
        CancelarCl();
    })
    $("#NuevoCliente").button({ icons: { primary: "ui-icon-plus" } }).click(function () {
        $(this).hide(300)
        NuevoCliente();
    }).hide();

    $('#inputClientes').autocomplete({
        minLength: "2",
        delay: 800,
        search: function (e, ui) {
        },
        source: (function (request, response) {
            var term = { prefixText: request.term };
            $.ajax({
                type: "POST",
                url: '/Service/getDatosCliente',
                data: term,
                dataType: "json",
                success: function (data) {                    
                    raw = data;
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.id;
                        obj.desc = val.dni;
                        obj.label = val.nombre;
                        obj.obj = val;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            });

        }),
        response: function (event, ui) {
            if (ui.content.length === 0) {// ui.content is the array that's about to be sent to the response callback.
                CancelarCl();
                $("#NuevoCliente").show(300);// Muestra el boton de nuevo cliente
                
            } else {
                $("#NuevoCliente").hide(300);// Oculta el boton de nuevo cliente
            }
        },
        select: function (e, ui) {
            $('#cliente').val('');
            $(this).attr('data-id', ui.item.value);                        
            EditCliente(ui.item.obj);
            return false;
        },
        focus: function (e, ui) {
            $('#cliente').val('');
            return false;
        }
    });   
    //Carga de referentes     
    $('#inputDestinatarios').autocomplete({
        minLength: "6",
        delay: 300,
        search: function (e, ui) {
            var exist = $(this).parent().find('#gifW').remove();
            var div = $('<div id="gifW">');
            div.css({ 'width': '16px', 'height': '20px', 'display': 'inline-block' });
            var img = $('<img src="/Content/img/ajax-loader.gif">');
            img.css({ 'max-width': '100%', 'max-height': '100%' });
            div.append(img);
            $(this).parent().append(div);
        },        
        focus:function(e,ui){
            $(this).val(ui.item.label); return false;
        },
        open:function(e,ui){
            $('#gifW').remove();
        },
        source: (function (request, response) {            
            var term = '{ prefixText: "' + request.term + '" }';            
            if (reqAuto) {                
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    //contentType: 'application/json; charset=iso-8859-1',
                    url: '/Service/getAddBook',
                    data: term,
                    dataType: "json",
                    timeout:2000,
                    beforeSend:function(){
                        reqAuto=false;
                    },
                    success: function (data) {
                        reqAuto = true;
                        $('#gifW').remove();
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        var suggestions = [];
                        $.each(data, function (i, val) {
                            var obj = {};
                            var utfstring = encodeURIComponent(val.email);
                            var utf2 = unescape(utfstring);
                            obj.value = val.email;
                            obj.label = val.Nombre;
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    },
                    error: function (xmlhttpreq, textstatus, message) {
                        if (textstatus == 'timeout') {
                            mostrarInfo("El server no respondió a tiempo!!!");
                        }
                        $('#gifW').remove();
                    },
                    complete: function () {
                        reqAuto = true;
                    }
                });
            }
        }),
        select: function (e, ui) {
            addBlock(ui, $('#referentes'));
            $(this).val("");
            return false;
        }
    }).on('keyup', function (e) {
        if (e.keyCode == 13) {
            var ev = {};
            ev.item = new Array();
            ev.item.label = $(this).val();
            ev.item.value = $(this).val();
            addBlock(ev,$('#referentes'));
            $(this).val("");
        }
    });    
    //binds
    $(".show").button({ text: false, icons: { primary: "ui-icon-zoomin" } }).click(function (event) {
        $('.tipific').dialog('open');
        //initNiceScroll(_selfNice); // Ejecuta niceSroll
    });
    $('body').on('click', '.fa-close', function () {
        $(this).parent().dialog('close');
    });
    var dialog = $('.tipific').dialog(
           {
               autoOpen: false,
               closeOnEscape: true,
               modal: true,
               resizable: true,
               title:'Tipificación',
               open: function () {
                   var thisdialog = $(this);
                   var theparent = $(thisdialog).parent();
                   //theparent.find('div.ui-dialog-titlebar').remove();
                   thisdialog.css('max-height', '400px');
                   thisdialog.css('overflow', 'auto');
                   thisdialog.css('max-width', '700px');
                   theparent.css('top', '109px');
                   theparent.css('padding', '5px');
                   theparent.css('width', 'auto');
                   theparent.css('margin', '0 auto');
                   theparent.css('min-height', '200px');
                   theparent.css('min-width', '272px');
                   initNiceScroll(thisdialog);
               },
               close: function () {
                   $(this).getNiceScroll().remove();
               }
           });
    
    $(".nvoInc").button({ text: false, icons: { primary: "ui-icon-gear" } }).on('click', generarNroInc);
    $('.undo').button({ text: false, icons: { primary: 'ui-icon-arrowreturnthick-1-w' } }).on('click', undoNvoNro).css('display', 'none');
    $("#nvoInc").button({ label: "Agregar", icons: { primary: "ui-icon-check" } }).on({click: nuevoIncidente});
    $('.referentes').button({ text: false, icons: { primary: "ui-icon-zoomin" } }).on('click', buscarReferentes);
    $('input.tNroInc').on('keyup', function () {
        $(this).val($(this).val().toUpperCase());
    });
    //-->binds
    /**********
    ***ARBOL***
    ***********/
    $('.tipific li:has(ul)').click(function (event) {
        event.preventDefault();
        var parent = $(this).closest('.tipific');
        if (this == event.target) {
            $(this).children().toggle();
            $(this).css('list-style-image', ($(this).children().is(':hidden')) ? 'url(' + soportePath + 'Content/img/plus.gif)' : 'url(' + soportePath + 'Content/img/minus.gif)');
            initNiceScroll(parent);
        return false;
        }
       
    }).css('cursor', 'pointer').click().mousedown(function (e) {
        e.preventDefault();
        }); 
    // NiceScroll
    //window._selfNice = $('.tipific'); // Contenedor para niceScroll   
    //_selfNice.on('click', function () { initNiceScroll(_selfNice) });
    //_selfNice.mouseenter(function () { // Activa niceScroll cuando el mouse entra en el contenedor
    //    initNiceScroll(_selfNice);
    //}).mouseleave(function () { // Desactiva niceScroll cuando el mouse sale del contenedor
    //    Nice = false;
    //});
    $('.tipific li:not(:has(ul))').css({
        cursor: 'default',
        'list-style': 'outside none none'
    });
    $('.tipific li.base').each(function () {
        $(this).bind('click', function (event) {
            var id = $(this).find('span.hide').html();
            $('#idTipificacion').val(id);
            var clickeado = $(this).clone().find('span.hide').remove().end().text();
            var phrases = "";
            var phrase = [];
            var parents = $(this).parents('.top');
            parents.each(function (i, v) {
                var only = $(v).clone().children().remove().end();
                var text = only.html().trim();
                var current = only.text();
                var index = current.indexOf("\n");
                if (index != -1) {
                    var test = current.substring(0, index);
                }
                else test = current;
                phrase.push(text);
            });
            phrase.reverse();
            phrases = phrase.join('/') + '/' + clickeado;
            $("#tTipificacion").val(phrases);
            $('.tipific').dialog("close");
        }).css({
            cursor: 'pointer',
            'list-style-image': 'none'
        });
    });
    $('.tipific li:has(ul)').children().hide();
    //------------>>ARBOL
    //bind de selecction ref
    $('body').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
        },
        click: function () {
            if ($(this).hasClass('ui-state-highlight')) {
                $(this).removeClass('ui-state-highlight');
            }else $(this).addClass('ui-state-highlight');            
        }
    }, 'div.row');
    $('.divBlocks').on('click', '.blockAdd .trash', function () {
        $(this).parent().remove();
    });
    $('#form').on('change', '#uploadFile', function () {
        var _this = this;
        var spinner = $('<div style="width:50px;margin:0 auto"><img src="/Content/img/ajax-loader.gif"/></div>');
        if (this.disabled) {
            mostrarError('El Navegador no soporta carga de Archivos!', 'error');
            return;
        }
        var file = this.files[0];
        var reader = new FileReader();
        NProgress.start();
        reader.readAsDataURL(file);        
        reader.onload = function (_file) {
            NProgress.done();
            var s = file.size;
            var megas = (s / (1024 * 1024));
            if (megas > 100) {
                mostrarError('El tamaño del archivo supera el máximo permitido(100MB)', 'error');
                clearInputFile();
            }
            var t = file.type;
            var h = file.name;
            var extension = h.split('.');
            if (file.type === 'application/octet-stream' | extension[extension.length - 1] === 'exe') {
                mostrarError('Los ejecutables no están permitidos', 'error');
                clearInputFile();
            }
            else {
                $(_this).siblings('label').contents().filter(function () {
                    return this.nodeType == 3;
                })[0].nodeValue = h;
            }
        }
        reader.onerror = function (e) {
            mostrarError(e, 'error')
        }
    });
    //image changer
    $("#form").on('change', '#uploadPhoto', function () {
        var _this = this;
        if (this.disabled) {
            mostrarError('El Navegador no soporta carga de imágenes!', 'error');
            return;
        }
        var file = this.files[0];
        imagenIncidente = this.files[0];
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function (_file) {
            image.src = _file.target.result;
            image.onload = function () {
                var w = this.width,
                    h = this.height,
                    t = file.type,
                    n = file.name,
                    s = ~~(file.size / 1024) + 'KB';
                var frame = $('<div class="frame">');
                var div = $('<div class="removeImagen ui-icon ui-icon-circle-close">');
                var spanPhoto = $('<span>').addClass('photo');
                frame.append(div).append(spanPhoto);
                $('#image').removeClass('hide').find('.images').append(frame);
                if (file.size / 1024 > 5000) {
                    mostrarError('El tamaño máximo de la imágen es de 5MB. El actual es: ' + s);
                    clearInputImage();
                    $('#image').addClass('hide').find('div.frame').remove();

                } else {
                    $('#form').addClass('fifty left');
                    $(spanPhoto).css({ 'background-image': 'none' }).html('').append('<img src="' + this.src
                        + '" class="photo"> ' + w + 'x' + h + ' ' + s + ' ' + n + '<br>');
                    //nombre en el label
                    $(_this).siblings('label').contents().filter(function () {
                        return this.nodeType == 3;
                    })[0].nodeValue = n;
                }
                
                _this.disabled = "disabled";
            };
            image.onerror = function () {
                mostrarError('Tipo de archivo no admitido: ' + file.type, 'error');
                clearInputImage();
            };
        };
    });
    $('#image').on('click', 'div.removeImagen', removeImagen);
    function messaje() {
        $.ajax({
            url: '/Service/Mensajes',
            type: 'post',
            success: function (data) {
                
            }
        });
    }   
    setInterval(messaje, 100000);
    $.connection.hub.start().done(function () { });
    clearForm();
});//READY------------------------------------------------

function initNiceScroll(_selfNice) // Crea el niceScroll
{
    _selfNice.getNiceScroll().remove();
    Nice = _selfNice.niceScroll({
        background: "#7CD569",
        cursorcolor: "#24890D",
        cursorwidth: "10px",
        cursorborder: "0",
        cursorborderradius: "2px",
        horizrailenabled: true,
        enabletranslate3d: true,
        railoffset: { top: 0, left: 15 }
    }).resize();
}
//Clear
function clearForm() {
    var context = $('#form');
    $('input[type="text"]:not(.tFecha)',context).val('');
    $('input.tNroInc',context).attr('disabled', false).removeClass('nuevoNro');
    var option = $('<option value="0">');
    option.append('--Seleccione--');
    $('select.dDependencia',context).html(option);
    $('select',context).find('option[value="0"]').attr('selected', 'selected');
    $('textarea',context).val('').removeAttr('data-id');
    $('.divBlocks div.blockAdd',context).remove();
    undoNvoNro();
    clearInputImage();
    clearInputFile();
    removeImagen();
}
function clearInputImage() {
    var _this = document.getElementById('uploadPhoto');
    var fileInput = document.createElement('input');
    fileInput.setAttribute('id', 'uploadPhoto');
    fileInput.setAttribute('type', 'file');
    _this.parentNode.replaceChild(fileInput, _this);
    _this.removeAttribute("disabled");
    $(fileInput).siblings('label').contents().filter(function () {
        return this.nodeType == 3;
    })[0].nodeValue = "Imágen";
    //removeImagen();
}
function clearInputFile() {
    var _this = document.getElementById('uploadFile');
    var fileInput = document.createElement('input');
    fileInput.setAttribute('id', 'uploadFile');
    fileInput.setAttribute('type', 'file');
    _this.parentNode.replaceChild(fileInput, _this);
    _this.removeAttribute("disabled");
    $(fileInput).siblings('label').contents().filter(function () {
        return this.nodeType == 3;
    })[0].nodeValue = "Archivo";
}
//-->--
/*****************
*Quitar imagen
******************/
function removeImagen() {
    window.imagenIncidente = null;
    $('#image').addClass('hide').find('div.frame').remove();
    $('#form').removeClass('fifty left');
    clearInputImage();
}
/******************
*Buscar Referentes
*******************/
function buscarReferentes() {
    var idD = $('.dJurisdiccion').val();
    if (idD == 0) { mostrarError("Seleccione una Jurisdicción", 'error'); return; }
    $.ajax({
        url: '/Config/getReferentesXJur',
        data: { 'idJur': idD },
        type: 'POST',
        success: function (d) {            
            if (d.length>0) {
                var divCont = $('<div>');
                $.each(d, function (i, v) {
                    var divRowRef = $('<div>').attr('class', 'row');
                    var divId = $('<div>').attr('class', 'hide');
                    var divNombre = $('<div>').attr('class', 'nombre');                   
                    divId.html(v.id);
                    divNombre.html(v.nombre);
                    divRowRef.append(divId).append(divNombre);
                    divCont.append(divRowRef);
                });
                $(divCont).dialog({
                    title: 'Seleccione los referentes',
                    open: function () {
                        var thisdialog = $(this);
                        var theparent = $(thisdialog).parent();                       
                        theparent.find('div.ui-dialog-titlebar').remove();
                        theparent.css('max-height', '400px');
                        theparent.css('overflow', 'auto');
                        theparent.css('max-width', '500px');
                        theparent.css('top', '175px');
                        theparent.css('width', 'auto');
                        theparent.css('margin', '0 auto');
                        theparent.css('min-height', '200px');
                        theparent.css('min-width', '272px');
                        theparent.niceScroll(
                        {
                            cursorcolor: "#24890D",
                            zindex: "999",
                            cursorborder: "0",
                            horizrailenabled: "false",
                            bouncescroll: "true"
                        })
                    },
                    buttons: {
                        Aceptar: function () {
                            var rows = $(this).find('div.row.ui-state-highlight');                            
                            $.each(rows, function (i, v) {
                                var ref = {};
                                ref.item = {};
                                ref.item.value = $(v).find('div.hide').text();
                                ref.item.label = $(v).find('div.nombre').text();
                                addBlock(ref,$('#referentes'));
                            });
                            $(this).dialog('destroy')
                        },
                        Cancelar: function () {
                            $(this).dialog('destroy')
                        }
                    }
                });
            } else {
                mostrarError("No se encontraron referentes", 'error');
            }
        }
    });//ajax       
}
//addBlock
function addBlock(valor,divDest) {
    if (valor.item.value == '') return;    
    var ids=$(divDest).find('div.blockAdd div.id');
    var inputDest = $('input[type="text"]', divDest);
    var flagAgrego = true;
    $.each(ids, function (i, v) {
        if ($(v).html() == valor.item.value) {
            mostrarError("Ya se agregó " + valor.item.label, 'error');
            flagAgrego = false;
            return;
        }
    });
    if (flagAgrego) {
        var divId = $('<div>').addClass('hide id');
        var divNombre = $('<div>').attr('class', 'nombre');
        var divTrash = $('<div>').attr('class', 'icon trash');
        var div = $('<div>').attr('class', 'blockAdd');
        divId.html(valor.item.value);
        divNombre.html(valor.item.label);
        div.append(divId).append(divNombre).append(divTrash);
        if (inputDest == undefined) divDest.append(div);
        else div.insertBefore(inputDest);
    }

}
/*****************
*Deshacer nvo nro
******************/
function undoNvoNro() {
    var type = this.type;
    var inputNvoNro;
    var inputNroInc;
    if (type == 'button') {
        inputNroInc = $(this).siblings('input[type="text"]');
        inputNvoNro = $(this).siblings('button.nvoInc');
        $(this).hide();
    } else {
        inputNvoNro = $('button.nvoInc');
        inputNroInc = $(inputNvoNro).siblings('input[type="text"]');
        $(inputNvoNro).siblings('button').hide();
    }        
    inputNvoNro.show();
    inputNroInc.val("").removeClass('nuevoNro').attr('disabled',false).focus();
}
/*********************
*Generacion Nro Inc
**********************/
function generarNroInc() {
    var inputNroInc = $(this).siblings('input[type="text"]');
    var inputUndo = $(this).siblings('button.undo');
    $(this).hide();
    inputUndo.show();
    inputNroInc.val("Se genera nro. nuevo").removeClass('denied').addClass('nuevoNro').attr('disabled', 'disabled');
}
/***********************
*Nuevo Incidente Handler
************************/
function nuevoIncidente() {
    var context = $(this).parents('.ulForm');
    var button = $(this);    
    var nombreTipificacion = $('#tTipificacion');
    var nroIncidente = $('.tNroInc', context);
    var tipoTicket = $('.dTipoTicket', context).val();
    var prioridadTicket = $('.dPrioridad', context).val();
    var idTipificacion = $('#idTipificacion',context).val();
    var jurIncidente = $('.dJurisdiccion', context).val();
    var depIncidente = $('.dDependencia', context).val();
    var observaciones = $('#incObs', context).val();
    var divDest = $('.divReferente',context);
    var idCliente = $('.liClientes').find('div.blockAdd div.id').html();
    var idsReferentes = $('.liReferentes').find('div.blockAdd div.id');    
    $('input[type=text]').removeClass('denied');
    //nombreTipificacion.removeClass('denied');
    var fData = new FormData();    
    fData.append('numero', 'nuevo');
    //if (nroIncidente.hasClass('nuevoNro')) {
    //    fData.append('numero','nuevo');
    //} else {
    //    if (!regularIncidente.test(nroIncidente.val())) {
    //        $(nroIncidente).addClass('denied');
    //        mostrarError("El número de Incidente ingresado no parece válido", 'error');
    //        return;
    //    } else {
    //        fData.append('numero', nroIncidente.val());
    //    }
    //}
    if (tipoTicket == 0) {
        mostrarError("Seleccione el Tipo", 'error');
        return;
    }
    if (prioridadTicket == 0) {
        mostrarError("Seleccione una Prioridad", 'error');
        return;
    }   
    if (!regString.test(nombreTipificacion.val())) {
        $(nombreTipificacion).addClass('denied');
        mostrarError("Complete la tipificación", 'error');
        return;
    }
    if (jurIncidente == 0) {
        mostrarError("Seleccione una Jurisdicción", 'error');
        return;
    }
    if (depIncidente == 0) {
        mostrarError("Seleccione una Dependencia", 'error');
        return;
    }
    if (idCliente==undefined) {
        mostrarError("Especifique el cliente", 'error');
        return;
    }
    var refs = [];
    if (idsReferentes.length > 0) {
        $.each(idsReferentes, function (i, v) {
            refs.push($(v).html());
        });
        fData.append('referentes', refs);
    } else {
        mostrarError("Ingrese a quien se debería notificar!!!");
        return;
    }
    if (observaciones.length < 10) {
        mostrarError('Complete el campo de Observaciones (al menos 10 caracteres)', 'error');
        return;
    }
    var divImage = $('#image');
    var flag = divImage.is(':hidden');
    fData.append('cliente', idCliente);
    fData.append('idTipificacion', idTipificacion);
    fData.append('idDependencia', depIncidente);
    fData.append('observaciones', observaciones);
    fData.append('tipoTicket', tipoTicket);
    fData.append('prioridadTicket', prioridadTicket);
    var upload = $("#uploadPhoto").get(0);
    var image = upload.files[0];
    var photos = $('input.photo');
    fData.append('photo', window.imagenIncidente);
    var fileF = $('#uploadFile').get(0);
    var archive = fileF.files[0];
    fData.append('archivo', archive);
    $.ajax({
        url: "NuevoIncidente/nuevoIncidente",
        data: fData,        
        processData: false,
        contentType: false,
        type: 'POST',
        beforeSend:function(){
            button.button('disable');
            NProgress.start();
        },
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (data.Info == "ok") {                
                $('.inputNvoInc', '#pNvoInc').not('.tFecha').val('');
                mostrarMensajeFx('Se ha creado el Ticket: ' + data.Detail, 'success', 8000);
                //mostrarExito('Se ha agregado el incidente: ' + data.Detail, 'success');
                clearForm();
            }
            else {
                if (data.Info = "con errores") {
                    mostrarMensajeFx("Se ha generado el Ticket pero hubo errores enviando la notificación a: " + data.Detail + ", por lo que no será notificado.", 'notice', 15000);
                    //mostrarInfo("Se ha generado el incidente pero hubo errores enviando la notificación a: "+ data.Detail);
                    clearForm();
                }
                else {
                    mostrarMensajeFx(data.Detail, 'error', 10000);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError, 'error');
        },
        complete: function () {           
            button.button('enable');
            NProgress.done();
        }
    });    
}
/***************
*Clientes
**************/
function EditCliente(selected) {// Muestra la ficha de cliente y completa los datos
    $('#fichacliente').fadeIn(300).position({ my: "left", at: "right", of: "#cliente" }).data('mode','edit');
    if (selected != null) {
        $('#idCl').val(selected.id)
        $('#NombreCl').val(selected.nombre)
        $('#DNICl').val(selected.dni)
        $('#emailCl').val(selected.email)
        if (selected.Dependencias !=null) {
            $('#JurisdiccionCl').val(selected.Dependencias[0].Jurisdiccion.idJurisdiccion).trigger('change', [$('#DependenciaCl'),selected.Dependencias[0].idDependencia]);           
        }
        else {
            setSelectByVal($('#JurisdiccionCl'), 0);
            $('#DependenciaCl').html($('<option>').html('--Seleccione--'))
        }
        $('#DireccionCl').val(selected.Direccion)
        $('#TelefonoCl').val(selected.te)
    }
}
function NuevoCliente() {
    $('#fichacliente').fadeIn(300).position({ my: "left", at: "right", of: "#cliente" }).data('mode', 'nuevo');
    EnableControlsCl();
}

function CancelarCl() {// Oculta la ficha de cliente y vacia los campos
    $('#fichacliente').fadeOut(200, function () {
        $('#fichacliente input:text').each(function () {
            $(this).val('').attr('readonly', 'readonly');
            $(this).removeClass('onEdit');
        });
        $('#fichacliente select').each(function () {
            $(this).val('0').attr('disabled', 'disabled');
            $(this).removeClass('onEdit');
        });        
    })
}

function EditarCl() {// Habilita la edidion de la ficha de cliente
    $('#AceptarCl').attr('data-id', 'postb');
    EnableControlsCl();
}
function EnableControlsCl() {
    $('#fichacliente input:text').each(function () {
        $(this).removeAttr('readonly');
        $(this).addClass('onEdit');
    })
    $('#fichacliente select').each(function () {
        $(this).removeAttr('disabled');
        $(this).addClass('onEdit');
    })
}
function AceptarCl() {
    var mode = $('#fichacliente').data('mode');
    var uri;
    var datosCl = {};
    var DNICl = $('#DNICl').val();
    var JurisdiccionCl = $('#JurisdiccionCl').val();
    var DependenciaCl = $('#DependenciaCl').val();
    var DireccionCl = $('#DireccionCl').val();
    var NombreCL = $('#NombreCl').val();
    if ((NombreCL == '' | NombreCL.length < 4)) {
        mostrarError("Complete el Nombre!");
        return;
    }
    if ((DNICl == '' | DNICl.length < 8)) {
        mostrarError("Complete el DNI!");
        return;
    }
    if (JurisdiccionCl == 0 | DependenciaCl == 0) {
        mostrarError("Seleccione la Jurisdicción y Dependencia!");
        return;
    }
    datosCl.nombre = $('#NombreCl').val();
    datosCl.dni = $('#DNICl').val();
    datosCl.email = $('#emailCl').val();
    datosCl.Dependencias = new Array();
    datosCl.Dependencias.push({ idDependencia: $('#DependenciaCl').val() });
    datosCl.Direccion = $('#DireccionCl').val();
    datosCl.te = $('#TelefonoCl').val();
    if (mode == 'nuevo') {
        datosCl.uri = '/Service/NuevoCliente';
        enviarDatosCliente(datosCl);
    } else {
        var doPost = $('#AceptarCl').attr('data-id');
        if (doPost == "postb") {
            datosCl.id = $('#idCl').val();
            datosCl.uri = '/Service/actualizarCl';
            enviarDatosCliente(datosCl)
            
        } else {
            var cl = {};
            cl.item = {};
            cl.item = {};
            cl.item.value = $('#idCl').val();
            cl.item.label = NombreCL + '--' + DNICl;
            addBlock(cl, $('#cliente'));
            $('#inputClientes').val('' );
            CancelarCl();
        }
    }
}
function enviarDatosCliente(datosCl)
{
    $.ajax({// Envia los datos
        url: datosCl.uri,
        type: 'post',
        data: JSON.stringify(datosCl),
        dataType: 'json',
        contentType: "application/json",
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        complete: function () {
            $('#AceptarCl').removeAttr('data-id');
        },
        success: function (response) {
            NProgress.done();// Hide Loading
            if (response.Info == "ok") {
                mostrarExito("Datos actualizados!");
                CancelarCl();
            }
            else {
                mostrarError(response.Detail);
            }
        },
        error: function (response) {
            NProgress.done();// Hide Loading
        }
    });
}