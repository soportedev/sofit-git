﻿function bindJurisdicciones(event, ui) {
    $('button').button();
    
    $('#sJurisd').on('change', function (e) {
        var selVal = $(this).val();
        $.post('/Service/Dependencias', { 'idJur': selVal }, function (data) {
            $('#sDepend').html('');
            $.each(data, function (i, v) {
                var option = $('<option>').val(v.idDependencia).text(v.Nombre);
                $('#sDepend').append(option);
            });
            
        });
    });
    $('#agregarJurisdiccion').on('click', function () {
        var radioSelected = $('input[name=location]:checked', '.locRefContainer').val();
        var optionJur = $('option:selected', '#sJurisd');
        var optionDep = $('option:selected', '#sDepend');
        var div = $('<div>').attr('class', 'row');
        var spanId = $('<span>').attr('class', 'hide');
        var spanTipo = $('<span>').attr('class', 'hide');
        var span1 = $('<span>').attr('class', 'nomJur');
        var span2 = $('<span>').attr('class', 'icon trash');
        var tablaJur = $('.tablaJurisd');
        var flag = true;
        if (radioSelected == undefined) {
            mostrarError("Seleccione el tipo de Locación");
            return;
        }
        if (radioSelected == 'jurisdiccion') {
            spanId.html(optionJur.val());
            spanTipo.html('jurisdiccion');
            span1.html(optionJur.text());
        } else {
            spanId.html(optionDep.val());
            spanTipo.html('dependencia');
            span1.html(optionDep.text());
        }
        var rows = $('.tablaJurisd').find('div.row:not(:first)');
        if (rows.length > 0) {
            $.each(rows, function (i, v) {
                var spans = $(v).find('span');
                var id = $(spans).eq(0).html();
                var tipo = $(spans).eq(1).html();
                var actualtipo = radioSelected;
                var actualId = spanId.html();
                if (tipo == actualtipo & id == actualId) {
                    mostrarError("Ya está seleccionado.");
                    flag = false;
                }
            })
        }
        if (flag) {
            div.append(spanId).append(spanTipo).append(span1).append(span2);
            tablaJur.append(div);
        }
    });
    $('.locRefContainer', '#Referentes').on('click', '.row .trash', function () {
        var div = $(this).parents('div.row');
        div.remove();
    });
    $('.refContainer', '#Referentes').on('click', '.row .trash', function () {
        var div = $(this).parents('div.row');
        var IdRef = $(div).find('span:first').html();
        var IdJur = $(this).parents('div.tableContainer').find('span.idJur').html();
        $.ajax({
            url: 'Config/quitarReferente',
            data: { 'idRef': IdRef ,'idJur':IdJur},
            type: 'post',
            success: function (d) {
                if (d.Info == 'ok') {
                    div.remove()
                }
                else mostrarError(d.Detail);
            },
            error: function (d, e, f) {
                mostrarError(f);
            }
        });
        ;
    });
    
    /***********
       **nva depend
       ************/
    $('#Dependencia').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        width: 450,
        title: "Agregar Dependencia",
        close: function () {
            $('#tNombreDependencia').val('');
            $('#tNombreDependencia2').val('');
            $('.error', '#Dependencia').hide();
        },
        buttons: {
            Aceptar: function () {
                var nombre = $('#tNombreDependencia').val();
                var nombre2 = $('#tNombreDependencia2').val();
                var id = $('tr.ui-state-highlight td:eq(0)', '.jurisdicciones').text();
                var flag = false;
                if ((nombre.length == nombre2.length) && nombre.length > 2) {
                    for (i = 0; i < nombre.length && !flag; i++) {
                        var l1 = nombre.substring(i, (i + 1));
                        var l2 = nombre2.substring(i, (i + 1))
                        if (l1 != l2) {
                            flag = true;
                        }
                    }
                }
                else flag = true;
                if (flag) {
                    $('.message', '#Dependencia').html("Los Nombres no son iguales");
                    $('.error').show('fast');
                    return;
                }
                else {
                    $.ajax({
                        url: "Config/nvaDependencia",
                        data: { 'idJur': id, 'nombre': nombre },
                        type: 'POST',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            if (data == "out") mostrarFinSesion();
                            else {
                                if (data.Info == "Login") { mostrarFinSesion(); }
                                if (data.Info == 'ok') {
                                    $("#Dependencia").dialog('close');
                                    mostrarExito('Se dió de alta la Dependencia')
                                    reloadCurrentTab();
                                }
                                else mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        },
                        complete: function () {
                            $(".ajaxGif").css({ 'display': 'none' });
                        }
                    }); //fin ajax
                } //fin else
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    /*************
        jurisdicciones
        ************/
    $('#Jurisdicciones').dialog(
        {
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            resizable: true,
            //position: "center",
            width: 450,
            title: "Agregar Jurisdicción",
            close: function () {
                $('#tNombreJurisdiccion').val('');
                $('#tNombreJurisdiccion2').val('');
                $('.error', '#Jurisdicciones').hide();
            },
            buttons: {
                Aceptar: function () {
                    var nombre = $('#tNombreJurisdiccion').val();
                    var nombre2 = $('#tNombreJurisdiccion2').val();
                    var flag = false;
                    if ((nombre.length == nombre2.length) && nombre.length > 2) {
                        for (i = 0; i < nombre.length && !flag; i++) {
                            var l1 = nombre.substring(i, (i + 1));
                            var l2 = nombre2.substring(i, (i + 1))
                            if (l1 != l2) {
                                flag = true;
                            }
                        }
                    }
                    else flag = true;
                    if (flag) {
                        $('.message', '#Jurisdicciones').html("Los Nombres no son iguales");
                        $('.error').show('fast');
                        return;
                    }
                    else {
                        $.ajax({
                            url: "Config/nvaJurisdiccion",
                            data: { 'nombre': nombre },
                            type: 'POST',
                            beforeSend: function () {
                            },
                            success: function (data) {
                                if (data.Info == "Login") { mostrarFinSesion(); }
                                if (data.Info == 'ok') {
                                    $("#Jurisdicciones").dialog('close');
                                    mostrarExito('Se dió de alta la Jurisdiccion')
                                    reloadCurrentTab();
                                }
                                else mostrarError(data.Detail);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.status);
                                alert(thrownError);
                            },
                            complete: function () {
                                $(".ajaxGif").css({ 'display': 'none' });
                            }
                        }); //fin ajax
                    } //fin else
                },
                Cancelar: function () {
                    $(this).dialog('close');
                }
            }
        });
    $('#rJurisdiccion').dialog(
        {
            autoOpen: false,
            closeOnEscape: true,
            modal: true,
            resizable: true,
            //position: "center",
            width: 450,
            title: "Renombrar Jurisdicción",
            open: function () {
                $('#rNombreJurisdiccion').val('');
                $('#rNombreJurisdiccion2').val('');
                $('.error', '#rJurisdiccion').hide();
            },
            buttons: {
                Aceptar: function () {
                    var nombre = $('#rNombreJurisdiccion').val();
                    var nombre2 = $('#rNombreJurisdiccion2').val();
                    var flag = false;
                    var id = $(this).data('id');
                    if ((nombre.length == nombre2.length) && nombre.length > 2) {
                        for (i = 0; i < nombre.length && !flag; i++) {
                            var l1 = nombre.substring(i, (i + 1));
                            var l2 = nombre2.substring(i, (i + 1))
                            if (l1 != l2) {
                                flag = true;
                            }
                        }
                    }
                    else flag = true;
                    if (flag) {
                        $('.message', '#rJurisdiccion').html("Los Nombres no son iguales");
                        $('.error').show('fast');
                        return;
                    }
                    else {
                        $.ajax({
                            url: "Config/renameJurisdiccion",
                            data: { 'id': id, 'nombre': nombre },
                            type: 'POST',
                            beforeSend: function () {
                            },
                            success: function (data) {
                                if (data.Info == "Login") { mostrarFinSesion(); }
                                if (data.Info == 'ok') {
                                    $("#rJurisdiccion").dialog('close');
                                    mostrarExito('Se cambió el nombre de la Jurisdicción')
                                    reloadCurrentTab();
                                }
                                else mostrarError(data.Detail);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                $(".ajaxGif").css({ 'display': 'none' });
                            }
                        }); //fin ajax
                    } //fin else
                },
                Cancelar: function () {
                    $(this).dialog('close');
                }
            }
        });
    /**********
    * dependencias
     **********/
    $('#rDependencia').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        width: 450,
        title: "Renombrar Dependencia",
        open: function () {
            $('#rNombreDependencia').val('');
            $('#rNombreDependencia2').val('');
            $('.error', '#rDependencia').hide();
        },
        buttons: {
            Aceptar: function () {
                var nombre = $('#rNombreDependencia').val();
                var nombre2 = $('#rNombreDependencia2').val();
                var id = $(this).data('id');
                var flag = false;
                if ((nombre.length == nombre2.length) && nombre.length > 2) {
                    for (i = 0; i < nombre.length && !flag; i++) {
                        var l1 = nombre.substring(i, (i + 1));
                        var l2 = nombre2.substring(i, (i + 1))
                        if (l1 != l2) {
                            flag = true;
                        }
                    }
                }
                else flag = true;
                if (flag) {
                    $('.message', '#rDependencia').html("Los Nombres no son iguales");
                    $('.error').show('fast');
                    return;
                }
                else {
                    $.ajax({
                        url: "Config/renameDependencia",
                        data: { 'id': id, 'nombre': nombre },
                        type: 'POST',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            if (data.Info == "Login") { mostrarFinSesion(); }
                            if (data.Info == 'ok') {
                                $("#rDependencia").dialog('close');
                                mostrarExito('Se cambió el nombre de la Dependencia')
                                reloadCurrentTab();
                            }
                            else mostrarError(data.Detail);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            $(".ajaxGif").css({ 'display': 'none' });
                        }
                    }); //fin ajax
                } //fin else
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    /******
    **referentes
    ******/
    $('#verReferentes').on('click', function (e) {
        e.stopPropagation();
        var checkin = $('tr.ui-state-highlight td:eq(0)', '.jurisdicciones');
        var nombre = checkin.next('td').text();
        if (checkin.length == 0) {
            mostrarError("Seleccione primero la Jurisdiccion");
            return;
        }
        $('#Referentes').data('id', checkin.text()).data('nombre', nombre);
        switchVisualizer();
        return false;
    });
    $('#switchV').on('click', function () { switchVisualizer() });
    //selecction
    $('#Referentes').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
        },
        click: function () {
            $('.row', '.refContainer').each(function () {
                $(this).removeClass('ui-state-highlight');
            });
            $(this).addClass('ui-state-highlight');
            var id = $(this).find('td:eq(0)').text();
        }
    }, 'div.row');
    $('#agregarReferente').on('click', function () {
        clear();
        $('.infoContainer').css('display', 'inline-block');
    });
    $('#aceptarReferente').on('click', function (e) {
        e.preventDefault();
        sendData('Config/AgregarReferente');
    });

    $('#modificarReferente').on('click', function () {
        var obj = $('.infoContainer').data('datos');
        var rowSelected = $('.tableContainer div.ui-state-highlight', '.refContainer');
        if (rowSelected.length == 0) {
            mostrarError("Seleccione el Referente");
            return;
        }
        var idR = $(rowSelected).find('span:first').html();
        clear();
        $.each(obj, function (i, v) {
            if (v.id == idR) {
                loadDivDatos(v);
                loadDivJurisdiccion(v);
                $('.infoContainer').css('display', 'inline-block');
                $('#updateReferente').show();
                $('#tNombre').attr('disabled', 'disabled');
                $('#aceptarReferente').hide();
            }
        })
    });
    $('#updateReferente').on('click', function (e) {
        e.preventDefault();
        sendData('Config/updateReferente');
    });
}
function clear() {
    $('#updateReferente').hide();
    $('.infoContainer').hide();
    $('#aceptarReferente').show();
    var tablaJur = $('.tablaJurisd');
    $(tablaJur).find('div.row:not(:first)').remove();
    $('#tNombre', '.datosRefContainer').val('');
    $('#tEmail', '.datosRefContainer').val('');
    $('#tTe', '.datosRefContainer').val('');
    $('#tNombre').removeAttr('disabled');
}
function loadReferentes(idJ) {
    $.ajax({
        url: '/Config/getReferentesXJur',
        data: { 'idJur': idJ },
        type: 'post',
        success: function (d) {
            var data = d;
            var panel = $('#Referentes').find('div.refContainer>div>ul');
            $.each(data, function (i, v) {
                var spanId = $('<span>').attr('class', 'hide');
                var spanNombre = $('<span>').attr('class', 'nomJur');
                var spanTrash = $('<span>').attr('class', 'icon trash');
                var div = $('<div>').attr('class', 'row');
                spanId.html(v.id);
                spanNombre.html(v.nombre);
                div.append(spanId).append(spanNombre).append(spanTrash);
                panel.append(div);
                $('.infoContainer').data('datos', data);
            });
        }
    });
}
function loadDivJurisdiccion(obj) {
    var tablaJur = $('.tablaJurisd');
    $.each(obj.location, function (i, v) {
        var div = $('<div>').attr('class', 'row');
        var spanId = $('<span>').attr('class', 'hide');
        var spanTipo = $('<span>').attr('class', 'hide');
        var span1 = $('<span>').attr('class', 'nomJur');
        var span2 = $('<span>').attr('class', 'icon trash');
        spanId.html(v.id);
        span1.html(v.nombre);
        spanTipo.html(v.tipo);
        div.append(spanId).append(spanTipo).append(span1).append(span2);
        tablaJur.append(div);
    });
}
function loadDivDatos(obj) {
    var nombre = $('#tNombre', '.datosRefContainer');
    var email = $('#tEmail', '.datosRefContainer');
    var te = $('#tTe', '.datosRefContainer');
    nombre.val(obj.nombre);
    email.val(obj.email);
    te.val(obj.te);
}
function listadoEquiposXDependencia(idDep) {
    $.ajax({
        data: 'idDep=' + idDep,
        url: '/Service/EquipoXDependencia',
        type: 'POST',
        beforeSend: function () { },
        success: function (data) {
            if (data.Info == 'Login') { mostrarFinSession(); return; }
            var div = $('.equiposListado');
            div.html('');
            var p = $('<p>');
            $.each(data, function (i, item) {
                var em = $('<em>');
                em.html(item.Oblea);
                em.attr('class', 'verInventario');
                em.attr('data-o', item.Oblea)
                p.append(em);
            });
            var botonPrint = $('<button/>', {
                text: 'Excel',
                id: 'btnExcel',
                click: function () {
                    window.location = '/Excel/getFile';
                }
            });
            var pButon = $('<p>');
            pButon.append(botonPrint);
            div.append(p);
            div.append(pButon);
            $('.listadoContainer').show();
        }
    });
}
function buscarDependencias(idJur, panel) {
    var tabla = $('div#dependencias table');
    tabla.addClass('dependencias');
    var container = $('.tableContainer', '.rightContainer');
    $.ajax({
        data: 'idJur=' + idJur + '&formato=tabla',
        url: 'Config/DependenciasXJurisdiccionTabla',
        type: 'POST',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.Info == "Login") { mostrarFinSesion(); }
            $(tabla).html(data.Detail);
        }
    });
}
function listadoReferentes(idDep, texto) {
    $.ajax({
        data: 'idDep=' + idDep,
        url: '/Config/referentes',
        type: 'POST',
        beforeSend: function () { },
        success: function (data) {
            var div = $('.referentesListado');
            div.html('');
            var ul = $('<ul>');
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            if (data.length == 0) {
                var p = $('<p>');
                p.append('No hay referentes cargados');
                div.append(p);
            }
            else {
                $.each(data, function (i, item) {
                    var li = $('<em>');
                    li.html(item.nombre);
                    ul.append(li);
                });
                div.append(ul);
            }
            var container = $('.referentesContainer');
            var legend = $(container).find('legend');
            legend.html('Referentes para ' + texto);
            container.show();
        }
    });
}
function switchVisualizer() {
    var referentes = $('#Referentes');
    var nombres = $(referentes).data('nombre');
    var id = referentes.data('id');
    $(referentes).find('div.refContainer>div>ul>li:first').html('Referentes de ' + nombres);
    var hideSpan = $('<span class="hide idJur">').html(id).appendTo('div.refContainer>div>ul>li:first');
    var jurisdicciones = $('#wrapperJurisdicciones');
    if (jurisdicciones.is(':visible')) {
        referentes.show();
        jurisdicciones.hide();
        setSelectByText($('#sJurisd'), nombres);
        $('#sJurisd').trigger('change');
        loadReferentes(id);
    } else {
        $(referentes).find('div.refContainer>div>ul>.row').remove();
        referentes.hide();
        clear();
        jurisdicciones.show();
    }

}
function sendData(url) {
    var nombre = $('#tNombre');
    nombre.removeClass('denied');
    var te = $('#tTe');
    var email = $('#tEmail');
    email.removeClass('denied');
    var rows = $('.tablaJurisd').find('div.row:not(:first)');
    var flag = true;
    if (!regString.test(nombre.val().trim())) {
        $(nombre).addClass('denied');
        flag = false;
    }
    if (!regEmail.test(email.val().trim())) {
        $(email).addClass('denied');
        flag = false;
    }
    if (rows.length == 0) {
        mostrarError("Seleccione las Jurisdicicones.");
        flag = false;
    }
    if (flag) {
        var ref = new FormData();
        ref.nombre = nombre.val();
        ref.id = undefined;

        ref.email = email.val();
        ref.te = te.val();
        ref.location = new Array();
        $.each(rows, function (i, v) {
            var spans = $(v).find('span');
            var jur = {};
            jur.id = $(spans).eq(0).html();
            jur.tipo = $(spans).eq(1).html();
            ref.location.push(jur);
        });
        $.ajax({
            url: url,
            data: JSON.stringify(ref),
            cache: false,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            type: 'POST',
            success: function (d) {
                if (d.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (d.Info == 'ok') {
                    mostrarExito("Transacción exitosa.");
                    $('p.ajaxGif').hide();
                    switchVisualizer();
                }
                else {
                    mostrarError(d.Detail);
                }
            },
            error: function (i, v, r) {
                mostrarError(r);
            }
        });
    }
}