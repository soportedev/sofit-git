$(function () {
    window.timeline = document.querySelector(".timeline ol");    
    window.arrows = document.querySelectorAll(".timeline .arrows .arrow");
    window.arrowPrev = document.querySelector(".timeline .arrows .arrow__prev");
    window.arrowNext = document.querySelector(".timeline .arrows .arrow__next");   
    window.disabledClass = "disabled";
    for (let i = 0; i < arrows.length; i++) {
        arrows[i].addEventListener("click", animateTl);
    }
    $('section.timeline').on('change', initTimeline);
});
function initTimeline() {       // VARIABLES
    window.firstItem = document.querySelector(".timeline li:first-child");
    window.lastItem = document.querySelector(".timeline li:last-child");
    isElementInViewport(firstItem) ? setBtnState(arrowPrev, true) : setBtnState(arrowPrev, false);
    isElementInViewport(lastItem) ? setBtnState(arrowNext, true) : setBtnState(arrowNext, false);
}
	
	// ANIMATE TIMELINE
function animateTl() {
    var tl = window.timeline;

    if (!arrowPrev.disabled) {
        arrowPrev.disabled = true;
    }
    if (!arrowNext.disabled) {
        arrowNext.disabled = true;
    }
    var $this = this;
    var sign = (this.classList.contains("arrow__prev")) ? "" : "-";
    var amount = 200;
    this.classList.add(disabledClass)
    $(tl).animate({
        'left': '+=' + sign + amount
    }, 300, function () {
        $this.classList.remove(disabledClass);
        isElementInViewport(firstItem) ? setBtnState(arrowPrev, true) : setBtnState(arrowPrev, false);
        isElementInViewport(lastItem) ? setBtnState(arrowNext, true) : setBtnState(arrowNext, false);
    });
}
	// CHECK IF AN ELEMENT IS IN VIEWPORT
	function isElementInViewport(el) {
	    const rect = el.getBoundingClientRect();
	    return (
			rect.top >= 0 &&
			rect.left >= 200 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
			rect.right <= (window.innerWidth -200 || document.documentElement.clientWidth - 200)
		);
	}
	// SET STATE OF PREV/NEXT ARROWS
	function setBtnState(el, flag) {	    
	    if (flag) {
	        el.classList.add(disabledClass);
	    } else {
	        if (el.classList.contains(disabledClass)) {
	            el.classList.remove(disabledClass);
	        }
	        el.disabled = false;
	    }
	}