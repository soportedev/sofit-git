// JavaScript Document
$(function () {
    $('body').on('click', '.verLB', function () {
        var texto = $(this).siblings('p');
        if($(texto).is(':visible')){
            texto.hide();
        } else {
            texto.show();
        }        
    })
    $('#VerRelacion').on({
        click: function () {
            if (window.State != 'showing' & window.State != 'edit') {
                mostrarInfo("Seleccione el equipo!!");
                return;
            }
            MostrarRelaciones();                    
        }
    });
    $('#RelacionesWraper').on('click', 'span.inlineblock.ui-icon-trash', function () {
        var id = $(this).siblings('span.idUc').html();
        $.ajax({
            data: { id1: $('#idEquipo').val(), id2: id },
            url: '/Service/dropRelation',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                mostrarInfo(data.Detail);
                OcultarRelaciones();
            }
        })
    });
    $('#LineaBase').on('click', function (e) {
        $.ajax({
            data: { id: $('#idEquipo').val() },
            url: '/Service/BaseLine',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                var dialogs = $('.dialogBL');
                if (dialogs.length > 0) {
                    dialogs.dialog('destroy');
                }
                var divdialog = $('<div class="dialogBL">').dialog({
                    width: '350',
                    show: 'slide',
                    position: { my: "left top", at: "middle top", of: $('#content-box') },
                    open: function () {
                        var este = $(this);
                        elPadre = $(this).parents('.ui-dialog');
                        este.css('max-height', '350px');
                        elPadre.css('min-width', '350px');
                        losBotones = $(this).siblings('.ui-dialog-buttonpane');
                        elPadre.css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                        losBotones.css('font-size', '.8em');
                        este.css('font-size', '.8em');
                        if (Object.keys(data).length === 0) {
                            este.append($('<p>').append('No hay l&iacute;neas base definidas!!'));
                        }
                        else {
                            var ul = $('<ul class="lineasBase">');
                            este.append(ul);
                            $.each(data, function (i, v) {
                                var li = $('<li>');
                                var labelCreado = $('<label>').html('Creado Por: ');
                                var spanCreado = $('<span>').text(v.CreadoPor);
                                var labelFecha = $('<label>').html('Fecha: ');
                                var spanFecha = $('<span>').text(v.FechaStr);
                                var aVer = $('<a href="#" class="verLB">').append('Ver');
                                var texto = v.Data.replace(/\r?\n/g, '<br>');
                                var pInfo = $('<p class="hide border">').html(texto);
                                li.append(labelCreado).append(spanCreado).append('--').append(labelFecha).append(spanFecha).append('--').append(aVer).append(pInfo);
                                ul.append(li);
                            });

                        }
                    },
                    buttons: {
                        Crear: function () {
                            var ddialog = $('<div>').append("Se va a crear una l&iacute;nea base. Est&aacute; Seguro?").dialog({
                                width: '250px',
                                show: 'slide',
                                //position: 'center',
                                open: function () {
                                    $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                                },
                                buttons: {
                                    Aceptar: function () {
                                        $.ajax({
                                            url: "Inventario/SaveBaseLine",
                                            data: { id: $('#idEquipo').val() },
                                            type: 'POST',
                                            beforeSend: function () {
                                                ajaxReady = false;
                                            },
                                            success: function (data) {
                                                if (data.Info == "Login") {
                                                    mostrarFinSesion();
                                                    return;
                                                }
                                                if (data.Info == "ok") {
                                                    mostrarExito("Fue creada una l&iacute;nea base!!!");
                                                    $(divdialog).dialog('destroy');
                                                    $(ddialog).dialog('destroy');
                                                }
                                                else {
                                                    mostrarError(data.Detail, 'error');
                                                }
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                mostrarError(thrownError, 'error');
                                            },
                                            complete: function () {

                                            }
                                        });               //fin ajax
                                    },
                                    Cancelar: function () { $(this).dialog('destroy') }
                                }
                            })
                        },
                        Cancelar: function () { $(this).dialog('destroy') }
                    }
                });
            }
        });        
    });
    $('body').on('click', 'div.baseLine span.ui-icon-close', function () {
        $(this).parents('div.baseLine').remove();
    });
    $('#RelacionesWraper').on('click', 'div.ui-icon-close', OcultarRelaciones);
    $('#AgregarRelacion').on('click', function(){	    
        if (window.State == 'edit') {
            if ($('#NuevaRelForm').is(":hidden")) {
                OcultarRelaciones();
                $('#NuevaRelForm').removeClass('opacity0').slideDown(300)
                $('#NuevaRelForm .table').removeClass('opacity0')

            } else {
                $('#NuevaRelForm').addClass('opacity0').slideUp({
                    complete: function () { OcultarRelaciones() }
                }, 300)
                $('#NuevaRelForm .table').addClass('opacity0')
            }
        } else {
            mostrarInfo("Habilite la edici&oacuten primero!!")
        }
    });
	$('#TipoElementoConf').click(function(){
		var TipoElementoConf = $(this)
		// Ajax
		$.ajax({
			url: '/Service/TiposEquipo',
			type:  'post',
			dataType: 'json',
			beforeSend: function () {
				// Show Loading
				NProgress.start();
			},
			success:  function(response) {
				// Hide Loading
				NProgress.done();

				// Values
				var raw = response;
				var source = [];
				var mapping = {};
				for (var i = 0; i < raw.length; ++i) {
					source.push(raw[i].Nombre);
					mapping[raw[i].Nombre] = raw[i].id;
				}
				TipoElementoConf.autocomplete({
					source: source,
					minLength: 0,
					select: function (event, ui) {
						var dataId = mapping[ui.item.value]
						$(this).attr('data-id', dataId);
					},
					close: function () {
						// Finish selection and make a blur
						$(this).blur();
					}
				}).val('').autocomplete('search');
			},
			error:  function(response) {
				// Hide Loading
				NProgress.done();
			}
		})
		// Ajax
	})
	$('#ElementoRelacion').autocomplete({
	    source: (function (request, response) {
	        var tipoElem = $('#TipoElementoConf').attr('data-id');
	        var term = { prefixText: request.term, idTipo:tipoElem};
	        $.ajax({
	            data: term,
	            url: '/Service/EquipoXObleaYTipo',
	            type: 'post',
	            dataType: 'json',
	            success: function (data) {	                
	                var suggestions = [];
	                $.each(data, function (i, val) {
	                    var obj = {};
	                    obj.value = val.idEquipo;
	                    obj.label = val.Oblea;
	                    suggestions.push(obj);
	                })
	                response(suggestions);
	            }
	        })
	    }),
	    minLength: 2,
	    select: function (event, ui) {
	        $(this).val(ui.item.label);
	        var dataId = ui.item.value;	        
	        var id1, id2;
	        id1 = $('#idEquipo').val();
	        id2 = dataId;
	        $(this).attr('data-id', dataId);;
	        $.ajax({
	            data: { 'id1': id1, 'id2': id2 },
	            url: '/Service/CreateRelation',
	            type: 'post',
	            dataType: 'json',
	            success: function (data) {
	                if (data.Info == 'ok') {
	                    OcultarRelaciones();
	                    $(this).val('').attr('data-id', '');
	                    $('#TipoElementoConf').val('').attr('data-id', '');
	                    $('#AgregarRelacion').trigger('click');
	                }
	                else mostrarError(data.Detail);
	            }
	        })
	        return false;
	    }
	});
	$('.tag').on('dblclick', function(e){
	    if (!$(e.target).hasClass('taghover')) {
	        e.preventDefault()
	        $(this).parent().remove()
	    }
	});
	});//ready
function MostrarRelaciones() {
    var panel = $('#RelacionesWraper');
    if (!panel.hasClass('showing')) {        
        var GetCoords = panel.offset();
        var OffsetTop = GetCoords.top - $(document).scrollTop() - 1;
        var OffsetLeft = GetCoords.left - $(document).scrollLeft() + 2;
        var GetWidth = $('#Observaciones').css('width');       
        $(panel).appendTo('#body')
                .css('height', '250px')
                .css('width', GetWidth)
                .css('position', 'fixed')
                .css('left', (OffsetLeft) + 'px')
                .css('top', (OffsetTop) + 'px')
                .css('overflow', 'auto')
                .addClass('ui-widget-content showing')
        //.niceScroll({ cursorcolor: "#24890D", zindex: "999", cursorborder: "0", horizrailenabled: "false", bouncescroll: "true" });
        var id1 = $('#idEquipo').val();
        $.ajax({
            data: { 'id1': id1 },
            url: '/Service/GetRelation',
            type: 'post',
            dataType: 'json',
            success: function (data) {                
                var div = $('<div class="relsF">');
                var icon = $('<span class="inlineblock ui-icon ui-icon-trash cursor">');
                var xicon = $('<div class="ui-icon ui-icon-close cursor right">');
                div.append($(xicon).clone());
                panel.append(div);
                if (data.length > 0) {                                       
                    $.each(data, function (i, v) {                                              
                        var div2 = $('<div>');
                        var spanOblea = $('<span class="underline">');
                        var spanId = $('<span class="idUc hide">');
                        spanOblea.html(v.Oblea);
                        spanId.html(v.Id);
                        var spanTipo = $('<span>');
                        spanTipo.html(' - '+v.TipoEquipo);
                        div2.append(spanOblea).append(spanTipo).append(spanId);
                        if (window.State == 'edit') {
                            div2.append($(icon).clone());                            
                        }
                        div.append(div2);                       
                    });
                } else {
                    div.append($('<p>').append('No se encontraron relaciones!!'));
                }
            }
        });
    }
}
function OcultarRelaciones() {
    $('#RelacionesWraper').appendTo('#RelacionesRow')
        .removeClass('ui-widget-content showing')
        .css('overflow', 'hidden')
        .css('left', '0')
        .css('top', '0')
        .css('position', 'relative')
        .css('width', 'auto')
        .css('height', '25px').find('input').val('');
    $('#RelacionesWraper').find('div.relsF').remove();
}
