﻿function Ventana($ventana) {
    var $self = this;
    this.filtrar = function (boton, text, user) {
        var usuario;
        var celda;
        var textoDefault;
        var funcionamientoDefault = true;
        var claseId=$(this).hasClass('conEquipo')
        if ($(this.allWindow).hasClass('conIncidente')) {
            celda = '6';
            textoDefault = "Mis Incidentes";
        } else {
            if ($(this.allWindow).hasClass('enlaces')) {
                celda = '7';
                textoDefault = 'Mis Enlaces';
            }
            else {
                celda = '5';
                textoDefault = "Mis Equipos";
            }
        }
        if (text) textoDefault = text;

        if (boton) funcionamientoDefault = false;
        else {
            boton = $('<p>');
        }
        if (user) {
            usuario = user;
            $self.tecnicoSelec = user;
        }
        else {
            usuario = $("#user").text();
            if (!funcionamientoDefault) $self.tecnicoSelec = null;
        }
        if (!funcionamientoDefault) $self.filtro = !$self.filtro;
        if ($self.filtro) {
            this.trs.filter(function () {
                if ($self.tecnicoSelec) usuario = $self.tecnicoSelec;
                return $('td:eq(' + celda + ')', this).text() != usuario;
            }).addClass('hide');
            this.updateNumbers();
            $('span.ui-button-text>span', boton).html("Ver Todos");
        }
        else {
            this.trs.each(function () {
                $(this).removeClass('hide');
            });
            this.updateNumbers();
            $('span.ui-button-text>span', boton).html(textoDefault);
        }
    }
    this.ocultarVentana = function () {
        this.visible = false;
        setPersonalize(undefined, this);
        $(this.accContent).hide();
        $(this.allWindow).height($self.minimizeHeight);
        $(this.allWindow).width($self.minimizeWidth);
    }
    this.mostrarVentana = function () {
        this.visible = true;
        setPersonalize(undefined, this);
        $(this.allWindow).height($self.height);
        $(this.allWindow).width($self.width);
        $(this.accContent).show();
    }
    this.Nombre;
    this.tecnicoSelec = null;
    this.filtro = false;
    this.id = $ventana[0].id;
    this.width = $($ventana).width();
    this.minimizeWidth = 450;
    this.minimizeHeight = 40;
    this.height = $($ventana).height();
    this.visible = true;
    this.max = false;
    this.orden = [];
    this.contentHeight;
    this.allWindow = $ventana;
    this.header = $($ventana).find('h2').first();
    this.tabla = $($ventana).find('table');
    this.accContent = $($ventana).find('.accContent');
    this.accContent2 = $($ventana).find('.accContent2');
    this.grilla = $($ventana).find('.grilla');
    this.thead = this.tabla.find('thead tr');
    //this.th = this.thead.find('th').on('click', function () { $self.setOrden(this)});
    this.trs = $(this.grilla).find('tr:not(:first)');
    this.trVisibles = 0;
    this.trError = 0;
    this.trDelay = 0;
    this.trOk = 0;
    this.botones = $($ventana).find('.botones');
    this.botones.find('ul.icons li span').hover(
        function() { $(this).parent().addClass('ui-state-hover'); },
        function() { $(this).parent().removeClass('ui-state-hover'); }
    );
    this.botones.find("ul.icons>li>span").click(function (e) {
        var botonesPre = $self.botones.outerHeight();
        var botonesAfter;
        var difBotones;
        $(this).siblings('ul').toggle(500, function () {
            botonesAfter = $self.botones.outerHeight();
            difBotones = botonesAfter - botonesPre;
            $($self.grilla).animate({ bottom: botonesAfter }, { duration: 100 });
        });
        //$($self.allWindow).animate({ height: '+=' + difBotones }, 600);
        //$($self.accContent).animate({ height: '+=' + difBotones }, 600);
        //$($self.grilla).animate({ bottom: botonesAfter }, { duration: 500, queue: false });//css('bottom', botonesAfter);
    }).end().
        find('.notificar').on('click', notificar).end().
        find('.notificarIncidente').on('click', notificarIncidente).end().
        find(".derivar").on('click', derivar).end().
        //find('.derivarGar').on('click', derivarGar).end().
        find('.derivarDireccion').on('click', derivarDireccion).end().
        find('.setPrioridad').on('click', setPrioridad).end().
        find('.trabajos').on('click', cargarTrabajos).end().
        find('.derivarEq').on('click', derivarEq).end().
        find(".cerrarIncidente").on('click', cerrarIncidente).end().
        find(".bAsignarTec").on('click', asignarIncidente).end().
        find('.filtro').on('click', function () {
            $self.filtrar(this);
        }).end().
        find(".terminarEquipo").on('click', terminarEquipo).end().
        find('.asignar').on('click', asignarEquipo).end().
        find('.tomar').on('click', tomarEquipo).end().
        find(".tomarIncidente").on('click', tomarIncidente).end().
        find('.suspender').on('click', suspenderIncidente).end().
        find('.activar').on('click', activarIncidente).end().
        find('.filtrarTecnico').on('click', function () {
            var target = this;
            var tec = $(this).parents('li').siblings('.tecnicosAsignar').find('select option:selected').text();
            $self.filtrar(target, "Filtrar", tec);
        });
    this.header.click(function () {
        if (!$self.max ) {
            if ($self.visible) {
                $self.ocultarVentana();
                $self.visible = false;
            }
            else {
                $self.mostrarVentana();
                $self.visible = true;
            }
        }
    });
    this.header.on('click','div.maximize', function (e) {        
        $self.maximize();
        return false;
    });
}
Ventana.prototype.maximize = function () {
    if (this.visible) {
        if (this.max) {
            this.restore();
        } else {            
            var viewpheight = $(window).height();            
            //var content = $('.incidentesContent');
            //var parent = content.parent();
            //var of = $(parent).offset().top;
            //var cont = document.getElementsByClassName('incidentesContent');
            //var offTop = cont[0].offsetTop;            
            //var body = $('body');
            //var search = $('#formSearch');
            //var ofset = search.offset();
            //var ofsTop = ofset.top;
            var v = this.allWindow;            
            var alto = viewpheight - 155;            
            if (this.accContent2) {
                var w = this.accContent2;
                $(w).height(alto-80);
            }
            //var scrolltop = $(content).scrollTop();
            //var top = scrolltop - 30;
            $(v).css('background', '#ddd');
            $(v).css('position', 'fixed');
            $(v).css('top', 125);
            $(v).css('left', '-40px');
            $(v).css('width', '100%');
            $(v).css('z-index', '91');
            $(v).height(alto);
           
            //content.css('overflow', 'hidden');
            $('body').height(alto);
            $('body').css('overflow', 'hidden');

            //content.height(viewpheight);
            //$(window.document).height(viewpheight);        
            this.max = true;
        }
    }
}
Ventana.prototype.restore = function () {
    $(this.allWindow).css('position', 'relative');
    $(this.allWindow).css('top', '0');
    $(this.allWindow).css('left', '0');
    $(this.allWindow).css('width', this.width);
    $(this.allWindow).css('height', this.height);
    $(this.allWindow).css('z-index', '0');
    $(this.allWindow).css('background-color', 'transparent');
    $('.incidentesContent').css('overflow', 'auto');
    if (this.accContent2) {
        $(this.accContent2).css('height','');        
    }
    var docHeight = $(window.document).height();
    $('body').css('height', 'auto');
    $('body').css('overflow', 'auto');
    this.max = false;
}
Ventana.prototype.updateNumbers = function () {
    //if (this.id == 'DivIncIng') return;
    var $trVisibles = $(this.trs).filter(':not(.hide)');
    var $trError = new Array();
    var $trOk = new Array();
    var $trDelayed = new Array();
    $.each($trVisibles, function (i, v) {
        var lastTd = $('td:last-child', v);
        var divinLastTd = lastTd.find('div');
        if (divinLastTd.hasClass('ok')) $trOk.push(v);
        if (divinLastTd.hasClass('nok')) $trDelayed.push(v);
        if (divinLastTd.hasClass('nook')) $trError.push(v);
    });
    var trVisibles = $trVisibles.length;
    var trOk = $trOk.length;
    var trDelay = $trDelayed.length;
    var trError = $trError.length;
    var aHeader = $('a', this.header);
    aHeader.find('div').remove();
    aHeader.append('<div class="inline divHeader"> &nbsp &nbsp &nbsp' + '<span><em>Total: ' + '(' + trVisibles + ')</em></span>' +
        '<div class="ok iconized status"></div>' + '<span><em>(' + trOk + ')</em></span>' +
        '<div class="nok iconized status"></div>' + '<span><em>(' + trDelay + ')</em></span>' +
        '<div class="nook iconized status"></div>' + '<span><em>(' + trError + ')</em></span>' +
        '<div class="maximize iconized status"></div></div>' + '</div>');
}
Ventana.prototype.update = function (grilla) {
    var body = $(this.tabla).find('tbody');
    
    var newTrs = $(grilla).find('tbody tr');
    this.trs = newTrs;
    body.html(newTrs);
    $(this.tabla).trigger('update', [true]);
    this.filtrar();    
}
Ventana.prototype.setSize = function (width, height, visible) {
    this.width = width;
    this.height = height;
    this.visible = visible;
    $(this.allWindow).width(width);
    $(this.allWindow).height(height);
    if (!visible) {
        $(this.accContent).hide();
        $(this.allWindow).height(this.minimizeHeight);
        $(this.allWindow).width(this.minimizeWidth);
    }
}
Ventana.prototype.setDefaultSize = function () {
    this.width = 550;
    this.height = 300;
    this.visible = true;
    $(this.allWindow).width(this.width);
    $(this.allWindow).height(this.height);    
}
Ventana.prototype.noVisible = function () {
    $(this.allWindow).hide();
}
Ventana.prototype.Visible = function () {
    $(this.allWindow).show();
}
Ventana.prototype.ocultarBotones = function () {
    $(this.botones).hide();
    $(this.grilla).css('bottom', '0');
}
Ventana.prototype.mostrarBotones = function () {
    $(this.botones).show();
}
Ventana.prototype.ocultarNotificaciones = function () {
    $(this.botones).find('li.notificar').hide();
}
Ventana.prototype.mostrarNoti = function () {
    $(this.botones).find('li.notificar').show();
}
Ventana.prototype.ocultarUbicar = function () {
    $(this.botones).find('li#setUbicacion').hide();
}
Ventana.prototype.mostrarUbi = function () {
    $(this.botones).find('li#setUbicacion').show();
}
Ventana.prototype.toUi = function () {
    $(this.allWindow).accordion({ animate: false,heightStyle:'content' }).resizable({
        stop: $.proxy(function (event, ui) {
            setPersonalize(ui);
            this.width = ui.size.width;
            this.height = ui.size.height;
            if (this.max) { this.restore() };
        }, this)
    });
    $(this.grilla).css('width', '100%');
    $(this.botones).css('width', '100%');
    this.updateNumbers();
    this.sorter();
}
Ventana.prototype.sorter = function () {
    var ths;
    if ($(this.allWindow).hasClass('totales')) {
        ths = $('th', this.tabla).filter(':eq(0),:eq(1),:eq(4),:eq(8),:eq(9),:eq(10),:eq(11)');
        ths.addClass('sorter-false');
        ths.removeClass('headerNone');        
    }
    if ($(this.allWindow).hasClass('conIncidente') & !$(this.allWindow).hasClass('totales')) {
        ths = $('th', this.tabla).filter(':eq(0),:eq(1),:eq(4),:eq(7),:eq(8),:eq(9),:eq(10)');
        ths.addClass('sorter-false');
        ths.removeClass('headerNone');
    } else {
            ths = $('th', this.tabla).filter(':eq(0),:eq(1),:eq(8),:eq(9),:eq(10),:eq(11)');
            ths.addClass('sorter-false');
            ths.removeClass('headerNone');
        }

    this.tabla.tablesorter({
        cssAsc: 'headerSortUp',
        cssDesc: 'headerSortDown',
        cssNone: 'headerNone',
        headers: {
            'sorter-false': { sorter: false }
        }
    });
}
//Funcion llamada al resize
function setPersonalize(ui, object) {
    var element;
    var datos = {};
    if (ui != undefined) {
        element = ui.element[0];
        datos = ui.size;
        datos.id = element.id;
        datos.isVisible = $(element).is(":visible");
    }
    else {
        datos.id = object.id;
        datos.isVisible = object.visible;
        datos.width = object.width;
        datos.height = object.height;
    }
    $.ajax({
        url: '/Service/PersonalSize',
        data: datos,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        type: 'POST',
        success: function (res) {
            if (res.Info == "Login") {
                mostrarFinSesion();
                return;
            }
        },
        error: function (xhr, error, thrown) {
            alert(thrown);
        }
    });
}
function opcionesVentanas(nombre, accion) {
    this.nombre = nombre;
    this.visible = false;
    this.visibleBotones = false;
    this.visibleNoti = false;
    this.visibleUbi = false;
    this.setVisible = function (v) {
        if (!this.visible) this.visible = v;
    }
    this.setVisibleBotones = function (v) {
        if (!this.visibleBotones) this.visibleBotones = v;
    }
    this.setVisibleNoti = function (v) {
        if (!this.visibleNoti) this.visibleNoti = v;
    }
    this.setVisibleUbi = function (v) {
        if (!this.visibleUbi) this.visibleUbi = v;
    }
}
function Perfiles(perfiles) {
    var ventanaIncidentesIng = new opcionesVentanas('DivIncIng');
    var ventanaIncidentesAtUs = new opcionesVentanas('DivIncAt');
    var ventanaIncidentesLog = new opcionesVentanas('DivIncLog');
    var ventanaEquiposClon = new opcionesVentanas('DivEqClon');
    var ventanaEquiposLab = new opcionesVentanas('DivEqLab');
    var ventanaEquiposDel = new opcionesVentanas('DivEqDel');
    var ventanaEquiposSusp = new opcionesVentanas('DivEqSus');
    var ventanaEquiposParaGar = new opcionesVentanas('DivEqParaGar');
    var ventanaEquiposEnGar = new opcionesVentanas('DivEqEnGar');
    var v = new Array();
    v.push(ventanaIncidentesIng, ventanaIncidentesAtUs, ventanaIncidentesLog, ventanaEquiposClon, ventanaEquiposLab,
        ventanaEquiposDel, ventanaEquiposSusp, ventanaEquiposParaGar, ventanaEquiposEnGar);
    this.perfiles = perfiles;
    this.getVentanas = function () {

        for (var i = 0; i < perfiles.length; i++) {
            if (this.perfiles[i].nombreBase == "Administrador Soporte" | this.perfiles[i].nombreBase=="Administrador Gral." | this.perfiles[i].nombreBase == "Dirección") { v.length = 0; break; }
            if (this.perfiles[i].nombreBase == "Usuario Clonado") {
                ventanaEquiposClon.setVisible(true);
                ventanaEquiposClon.setVisibleBotones(true);
                ventanaEquiposLab.setVisible(true);
                ventanaEquiposLab.setVisibleBotones(true);
                ventanaEquiposSusp.setVisible(true);
                ventanaEquiposSusp.setVisibleBotones(true);
            };
            if (this.perfiles[i].nombreBase == "Usuario Logística") {
                ventanaIncidentesIng.setVisible(true);
                ventanaIncidentesIng.setVisibleBotones(true);
                ventanaIncidentesAtUs.setVisible(true);
                ventanaIncidentesAtUs.setVisibleBotones(true);
                ventanaIncidentesLog.setVisible(true);
                ventanaIncidentesLog.setVisibleBotones(true);
                ventanaEquiposClon.setVisible(true);
                ventanaEquiposLab.setVisible(true);
                ventanaEquiposSusp.setVisible(true);
                ventanaEquiposSusp.setVisibleBotones(true);
                ventanaEquiposSusp.setVisibleUbi(true);
                ventanaEquiposDel.setVisible(true);
                ventanaEquiposDel.setVisibleBotones(true);
                ventanaEquiposParaGar.setVisible(true);
                ventanaEquiposParaGar.setVisibleBotones(true);
                ventanaEquiposEnGar.setVisible(true);
                ventanaEquiposEnGar.setVisibleBotones(true);
            }
            if (this.perfiles[i].nombreBase == "Usuario At.Usuarios") {
                ventanaIncidentesAtUs.setVisible(true);
                ventanaIncidentesAtUs.setVisibleBotones(true);
                ventanaIncidentesLog.setVisible(true);
                ventanaIncidentesLog.setVisibleBotones(true);
                ventanaEquiposDel.setVisible(true);
                ventanaEquiposDel.setVisibleBotones(true);
            }
            if (this.perfiles[i].nombreBase == "Usuario Laboratorio") {
                ventanaEquiposClon.setVisible(true);
                ventanaEquiposClon.setVisibleBotones(true);
                ventanaEquiposLab.setVisible(true);
                ventanaEquiposLab.setVisibleBotones(true);
                ventanaEquiposSusp.setVisible(true);
                ventanaEquiposSusp.setVisibleBotones(true);
            }
            if (this.perfiles[i].nombreBase == "Inventario") {
                ventanaIncidentesAtUs.setVisible(true);
                ventanaIncidentesLog.setVisible(true);
                ventanaEquiposClon.setVisible(true);
                ventanaEquiposLab.setVisible(true);
                ventanaEquiposSusp.setVisible(true);
                ventanaEquiposDel.setVisible(true);
                ventanaEquiposParaGar.setVisible(true);
                ventanaEquiposEnGar.setVisible(true);
            }
            if (this.perfiles[i].nombreBase == "Guest") {
                ventanaIncidentesAtUs.setVisible(true);
                ventanaIncidentesLog.setVisible(true);
                ventanaEquiposClon.setVisible(true);
                ventanaEquiposLab.setVisible(true);
                ventanaEquiposSusp.setVisible(true);
                ventanaEquiposDel.setVisible(true);
                ventanaEquiposParaGar.setVisible(true);
                ventanaEquiposEnGar.setVisible(true);
            }
        }
        //var result = [];
        //$.each(v, function (i, e) {
        //    if ($.inArray(e, result) == -1) result.push(e);
        //});
        return v;
    }
}