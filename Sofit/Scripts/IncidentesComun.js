﻿/******************
*VARIABLES GLOBALES
*******************/
var status = "<div class='status'></div>";
var updates = true;
var timer;
var ajaxReady;
var UsuarioLogeado;
var has_focus = true; // Guarda el estado de la ventana - FOCUS / BLUR
window.FREQ = 180000;
window.ajaxReady = true;
window.updatesIncidente = true;
window.ventanas = new Array();
$(function () {
    // Actualiza el estado de la ventana
    window.onblur = function() // Cuando esta en foco
    {  
        has_focus=false;
        console.log('Pestana en foco:', has_focus);
    }  
    window.onfocus = function() // Cuando no esta en foco
    {
        has_focus=true;
        if (typeof cancelcoders !== 'undefined') cancelcoders(); // Cancela cambio de titulo
        console.log('Pestana en foco:', has_focus);
    }
    ///--->

    // SOLICITUD DE NOTIFIACIONES
    if (Notification)
    {
        if (Notification.permission !== 'granted')
        {
            Notification.requestPermission();
        }
        if (Notification.permission === "denied") {
            mostrarInfo("Las Notificaciones están bloqueadas. Habilítelas desde su navegador!!!");
        }
    }

    UsuarioLogeado = $('#idUser').val();
    /**********
    *Chars Counter
    ***********/
    $('textarea.observaciones').on('keyup', function (e) {
        var spanCounter = $(this).siblings('p.charCounter');
        var taLength = function (str) {
            if (!str)
                return 0;

            var lns = str.match(/[^\r]\n/g);

            if (lns) {
                var onlystr = str.length - lns.length;
                return onlystr + lns.length * 2;
            }
            else
                return str.length;
        }
        if (taLength(this.value) > 3999) {
            spanCounter.addClass('error2').html('Chars:' + taLength(this.value) + '. El máximo de caracteres es 4000');
        } else
            spanCounter.removeClass('error2').html('Chars:' + taLength(this.value));
    });
    $('textarea.tSolucion').on('keyup', function (e) {
        var spanCounter = $(this).siblings('p.charCounter');
        var taLength = function (str) {
            if (!str)
                return 0;

            var lns = str.match(/[^\r]\n/g);

            if (lns) {
                var onlystr = str.length - lns.length;
                return onlystr + lns.length * 2;
            }
            else
                return str.length;
        }
        if (taLength(this.value) > 3999) {
            spanCounter.addClass('error2').html('Chars:' + taLength(this.value) + '. El mail se envía completo, pero en la BD se registran los primeros 4000 chars');
        } else
            spanCounter.removeClass('error2').html('Chars:' + taLength(this.value));
    });
    /*****
    *BINDS
    ******/
    //Chat
    $(document).on("updateMsjStatusEv", updateMsjStatus); // Mensajes // Suscriptor para actualizar
    $('.badge-numbers').html('!'); // Crea el texto de notifiaciones minimizado

    $('div.iconContainer').on({
        click: function () {
            var chat = $('.chat-box');
            var chatlista = $('ul#Chat-Lista');
            var incActuales = $(this).parent();
            var incAqui = incActuales.siblings('div');
            var divIcon = $(this).find('div.ui-icon');
            var p = $(this).siblings('p');
            // h2.removeClass('rotated normal');
            divIcon.removeClass('ui-icon-circle-triangle-w ui-icon-circle-triangle-s');
            var w = incActuales.width();
            if (w > 200) { // Cierra
                incAqui.css('right', '35px');
                incActuales.css('width', '35px');
                $('.badge-numbers').html('!');
                p.switchClass( "normal", "rotated", 650 );
                chatlista.fadeOut(400);
                divIcon.addClass('ui-icon-circle-triangle-w');
                chat.css('right', '55px');
                // CleanBadges();
            } else { // Abre
                incAqui.css('right', '205px');
                incActuales.css('width', '205px');
                divIcon.addClass('ui-icon-circle-triangle-e');
                p.switchClass( "rotated", "normal", 650, function(){
                    $('.badge-numbers').html('Tiene mensajes nuevos');
                });
                chatlista.fadeIn(1500);
                chat.css('right', '225px');
            }
        },
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
        }
    });

    //Muestra / Oculata menu
    $('#Chat-Lista').on('click', '.acordeonChat', function(){
    // $('.acordeonChat').click(function(){
        if ( $(this).next().is( ":hidden" ) ) {
            $(this).next().slideDown('normal');
            $(this).find('.badge-contact').remove(); // Borra el badge del contacto
        } else {
            $(this).next().slideUp('normal');
        };
    });
    //-->
    $('li.referentes').on('click', function () {
        var nro = [];
        var exito = false;
        $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("Debe seleccionar UN ticket");
        }
        else {
            var id = $(nro[0]).find('td').eq(1).text();
            $('#dialogReferentes').data('datos', id).dialog('open')
        }

    });
    //drops
    $('.dTipos', '#nuevoEquipo').on('change', function () {
        var $TipoEquipo = $(this).val();
        var selectMarcas = $('.dMarcas', '#nuevoEquipo');
        $.ajax({
            url: '/Soporte/Inventario/MarcasPorTipo',
            data: { 'tipo': $TipoEquipo },
            type: 'POST',
            datatype: 'json',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                ajaxReady = true;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                selectMarcas.html('');
                if (data.length == 0) {
                    var option = $('<option>').val('0').html('--Seleccione--');
                    selectMarcas.append(option);
                }
                $.each(data, function (i, v) {
                    var option = $('<option>').val(v.idMarca).html(v.Nombre);
                    selectMarcas.append(option);
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                $(".ajaxGif").css({ 'display': 'none' });
            }
        });
    });
    $('.dJurisdiccion', '#nuevoEquipo').on('change', function () {
        var $idJur = $(this).val();
        var selectdep = $('.dDependencia', '#nuevoEquipo');
        $.ajax({
            url: '/Soporte/Inventario/Dependencias',
            data: { 'idJur': $idJur },
            type: 'POST',
            datatype: 'json',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                ajaxReady = true;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                selectdep.html('');
                if (data.length == 0) {
                    var option = $('<option>').val('0').html('--Seleccione--');
                    selectdep.append(option);
                }
                $.each(data, function (i, v) {
                    var option = $('<option>').val(v.idDependencia).html(v.Nombre);
                    selectdep.append(option);
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                $(".ajaxGif").css({ 'display': 'none' });
            }
        });
    });
    //-->end drops
    var inputOblea = $(".tObleaSolucion");
    inputOblea.autocomplete({
        minLength: "2",
        source: (
            function (request, response) {
                var idTipoEquipo = $('select.tipoEquipo').val();
                //if (idTipoEquipo == 0) {
                //    mostrarError("Seleccione el tipo de Elemento de Conf.");
                //    return;
                //}
                //var term = '{ prefixText: "' + request.term + '",tipoEquipo:"' + idTipoEquipo + '"}';
                var term = '{ prefixText: "' + request.term + '"}';
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/Service/EquipoXOblea",
                    data: term,
                    dataType: "json",
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        else {
                            var suggestions = [];
                            $.each(data, function (i, val) {
                                var obj = {};
                                obj.value = val.idEquipo;
                                obj.label = val.Oblea;
                                suggestions.push(obj);
                            })
                            response(suggestions);
                        }
                    }
                });
            }),
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        focus: function (e, ui) { $(this).val(ui.item.label); return false; },
        select: function (e, ui) {
            var spanId = $('<span class="hide idEquipo">').html(ui.item.value);
            var context = $(this).parents('.obleaCont');
            var tablaEquipos = $('#equiposSolucion', context);
            $(this).val('');
            $(this).focus();
            var idsf = $(tablaEquipos).find('td.idEquipo');
            var flag = false;
            $.each(idsf, function (i, v) {
                if ($(v).html() == ui.item.value) {
                    flag = true;
                }
            });
            if (flag) {
                mostrarError("Ya fue agregado!!!");
                return false;
            } else {
                var tdId = $('<td class="idEquipo hide">').html(ui.item.value);
                var td = $('<td class="oblea">').html(ui.item.label);
                var tr = $('<tr>').append(td).append(tdId).append('<td><div class="icon drop"></td>');
                $('tbody', tablaEquipos).append(tr);
                $('#bTrabajos').button("enable");
            }
            return false;
        }
    });
    //buttons en dialog trabajos
    $('button.helpName').button({ icons: { primary: "ui-icon-help" } }).on('click', function (event) {
        var html = "<p><em>WS</em> para Pc <br/><em>PR</em> para Impresoras <br/>" +
            "<em>MN</em> para Monitores <br/>" +
            "</p>";
        var div = $('<div>').append(html).dialog({
            show: 'slide',
            position: { my: "left middle", at: "right bottom", of: event.target },
            width: 'auto'
        }).click(function () { $(this).dialog('destroy') });
        $(".ui-dialog-titlebar", $(div).parent()).hide();
    });
    $('button.newEC').button({ icons: { primary: "ui-icon-plusthick" } }).on('click', function () {
        $('#nuevoEquipo').dialog('open')
    });
    $('.oblea', '#nuevoEquipo').on('keyup', function () {
        $(this).val($(this).val().toUpperCase());
    });
    //-->buttons
    //dialog nvo equipo
    $('#nuevoEquipo').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Nuevo Equipo",
        width: '470px',
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '109px');
            $('#Trabajos').dialog('close');
            $('select', thisdialog).val('0');
            $('input', thisdialog).val('');
        },
        close: function () {
            $('#Trabajos').dialog('open');
        },
        buttons: {
            Aceptar: {
                text: 'Aceptar',
                click: function () {
                    $('.message', '#nuevoEquipo').html('').parent().hide();
                    var idTipo = $('.dTipos', '#nuevoEquipo').val();
                    var idMarca = $('.dMarcas', '#nuevoEquipo').val();
                    var idJurisdiccion = $('.dJurisdiccion', '#nuevoEquipo').val();
                    var idDep = $('.dDependencia', '#nuevoEquipo').val();
                    var oblea = $('.oblea', '#nuevoEquipo').val();
                    if (!regularEquipo.test(oblea)) {
                        $('#nuevoEquipo').find('.message').html('La oblea no parece válida');
                        $('.error', '#nuevoEquipo').show();
                        return;
                    }
                    if (idTipo == 0) {
                        $('#nuevoEquipo').find('.message').html('Seleccione el tipo de Elemento de Conf.');
                        $('.error', '#nuevoEquipo').show();
                        return;
                    }
                    if (idMarca == 0) {
                        $('#nuevoEquipo').find('.message').html('Seleccione la Marca.');
                        $('.error', '#nuevoEquipo').show();
                        return;
                    }
                    if (idJurisdiccion == 0) {
                        $('#nuevoEquipo').find('.message').html('Seleccione la Jurisdicción.');
                        $('.error', '#nuevoEquipo').show();
                        return;
                    }
                    if (idDep == 0) {
                        $('#nuevoEquipo').find('.message').html('Seleccione la Dependencia.');
                        $('.error', '#nuevoEquipo').show();
                        return;
                    }
                    $.ajax({
                        url: '/Soporte/Inventario/nuevoEquipoSlim',
                        data: { 'idTipo': idTipo, 'idMarca': idMarca, 'idJur': idJurisdiccion, 'idDep': idDep, 'oblea': oblea },
                        type: 'POST',
                        datatype: 'json',
                        beforeSend: function () {
                            ajaxReady = false;
                        },
                        success: function (data) {
                            ajaxReady = true;
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarExito('Se agregó el equipo');
                                $('#nuevoEquipo').dialog('close');
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        }
                    });
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    //-->dialog nvo equipo
    //dialog referentes
    $('#dialogReferentes').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Referentes",
        width: '470px',
        open: function () {
            var thisdialog = $(this);
            var datos = $(this).data('datos');
            $.ajax({
                url: '/Service/getReferentes',
                data: { 'idTicket': datos },
                type: 'POST',
                datatype: 'json',
                success: function (data) {
                    ajaxReady = true;
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data == null) {
                        return;
                    }
                    var emails;
                    $.each(data, function (i, v) {
                        var divDest = $('#destActuales');
                        var spanNombre = $('<span>').attr('class', 'nomJur');
                        var div = $('<div>').attr('class', 'mailAdd');
                        spanNombre.html(v.email);
                        div.append(spanNombre);
                        divDest.append(div);
                        var widthspnom = spanNombre.width();
                        var total = widthspnom + 1;
                        div.css('width', total++);
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                }
            });
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('overflow', 'visible');
            $(thisdialog).parent().css('top', '109px');
            $('select', thisdialog).val('0');
            $('input', thisdialog).val('');
        },
        close: function () {
            var divDest = $('#destActuales');
            divDest.html('');
        },
        buttons: {
            Aceptar: {
                text: 'Aceptar',
                click: function () {
                    var datos = $(this).data('datos');
                    var spanMails = $('#destActuales').find('span.hide');
                    var nuevosMails = [];
                    $.each(spanMails, function (i, v) {
                        nuevosMails.push($(v).html());
                    });
                    if (nuevosMails.length > 0) {
                        $.ajax({
                            url: '/Service/AgregarReferentes',
                            data: 'id=' + datos + '&nuevosRef=' + nuevosMails,
                            type: 'POST',
                            datatype: 'json',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                ajaxReady = true;
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarExito('Referentes agregados!!!');
                                    $('#dialogReferentes').dialog('close');
                                }
                                else {
                                    if (data.Info == "conerror") {
                                        mostrarInfo("Se agregaron mail con algunos errores: " + data.Detail);
                                        $('#dialogReferentes').dialog('close');
                                    } else mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            }
                        });
                    } else {
                        $('#dialogReferentes').dialog('close');
                    }
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    //-->dialog referentes
    $('div.itemNro').on({
        click: function () {
            setColors(this);
            $('#searchBar').show();
            $('#searchClienteBar').hide();
        }
    });
    $('div.itemCliente').on({
        click: function () {
            setColors(this);
            $('#searchBar').hide();
            $('#searchClienteBar').removeClass('hidden').show();
        }
    });
    function setColors(div) {
        if (!div) div = $('div.itemNro');
        restoreDiv();
        var color = $('.body').css('color');
        var bcolor = $('.ui-state-active').eq(0).css('color');
        $(div).css({
            'border': 'none',
            'border-bottom-width': '3px',
            'border-bottom-style': 'solid',
            'border-bottom-color': bcolor,
            'background-color': 'transparent',
            'background-image': 'none',
            'color': color,
            'font-weight': 'bold'
        });
    }
    function restoreDiv() {
        $('div.itemNro , div.itemCliente').attr('style', '');
    }
    $('.grilla').on('click', '.printTicket', function () {
        var idInc = $(this).closest("tr").find("td").eq(1).html();
        var datos = "id=" + idInc;
        window.open('/Impresion/Index?idInc=' + idInc, '', 'height=600,width=930,menubar=no,toolbar=no,scrollbars=yes,status=no');
        window.focus();
    });
    var user = $('#user').html();
    var fecha = new Date();
    var hours = fecha.getHours();
    var turno = hours >= 14 ? 'Tarde' : 'Mañana';
    setDropText(user, $('.dTecnicos'));
    setDropText(turno, $('.dTurnos'));
    $('.bExcel').on('click', function obtenerExcel() {
        var win = $(this).parents('div.window');
        var context = win[0].id;
        window.location = 'Incidentes/getExcel?win=' + context;
    });
    $('.btnHelpSoporte').on('click', function (event) {
        window.open('/Areas/Soporte/AyudaSST/AyudaSST.html', '', 'height=600,width=930,menubar=no,toolbar=no,scrollbars=yes,status=no');
        window.focus();
    });
    $('.btnHelpOperaciones').on('click', function (event) {

    });
    $('.btnHelpTelecomunicaciones').on('click', function (event) {

    });
    $("#dUpdate").on('click', disableUpdates);
    $("#bIncToLive").on('click', incidenteToLife);
    $(".boton").button({ icons: { primary: "ui-icon-check" } });
    $('.botonNoIcon').button();
    $('.jQueryBtn').button();
    $("#bNvoEquipo").on('click', function (event) {
        addEquipos(event);
    });
    $("#bQuitarEquipo").on('click', function (event) {
        quitarEquipos(event);
    });
    $('.dialogTrabajos').on('click', '.drop', function () {
        $tr = $(this).parents('tr');
        $tr.remove();
    });
    $('.dialogTrabajos').on('change', '.esParaEquipo', function () {
        var checked = $(this).is(':checked');
        var oblea = $(this).siblings('.obleaCont');
        if (checked) {
            oblea.show();
        } else {
            $('tbody', '#equiposSolucion').html('');
            $('#tObleaSolucion').val('');
            $('tr:not(:first)', oblea).remove();
            $(oblea).hide();
        }
    });
    $('.grilla').on('click', '.notificInc', function (e) {
        mostrarNotificacInc(e);
    });
    //HOVER TRtd .
    $('div.window').on({
        mouseenter: function (e) {
            if (this == e.currentTarget) {
                $(this).addClass('ui-state-hover');
                e.stopPropagation();
            }
        },
        mouseleave: function (e) {
            if (this == e.currentTarget) {
                $(this).removeClass('ui-state-hover');
                e.stopPropagation();
            }
        },
        click: function (e) {
            var tr = $(e.currentTarget);
            var t = e.target;
            var st = t.type;
            if (st == "checkbox") {
                if ($(t).is(':checked')) {
                    tr.addClass('ui-state-highlight')
                }
                else {
                    tr.removeClass('ui-state-highlight');
                }
            }
        }
    }, 'tbody tr');
    /************
    *Notificaciones
    ************/
    $('.refContainer', '#NotifIncidentes').on('click', 'span.trash', function () {
        $(this).parents('.mailAdd').remove();
    });
    $('#destActuales', '#dialogReferentes').on('click', 'span.trash', function () {
        $(this).parents('.mailAdd').remove();
    });
    $('.inputDest', '#dialogReferentes').autocomplete({
        minLength: "3",
        delay: 800,
        source: (function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '/Service/getAddBook',
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.email;
                        obj.label = val.Nombre;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            });
        }),
        select: function (e, ui) {
            if (e.keyCode != 13) {
                addBlock2(ui);
                $(this).val("");
                return false;
            }
        }
    }).on('keyup', function (e) {
        if (e.keyCode == 13) {
            var valor = $(this).val();
            if (regEmail.test(valor)) {
                var ev = {};
                ev.item = new Array();
                ev.item.label = valor;
                ev.item.value = valor;
                addBlock2(ev);
                $(this).val("");
                return false;
            } else {
                mostrarError('No parece una dirección de email válida!!')
            }
        }
    });
    $('.inputDestinatarios', '#NotifIncidentes').autocomplete({
        minLength: "2",
        delay: 800,
        source: (function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '/Service/getAddBook',
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.email;
                        obj.label = val.Nombre;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            });
        }),
        select: function (e, ui) {
            addBlock(ui);
            $(this).val("");
            return false;
        }
    }).on('keyup', function (e) {
        if (e.keyCode == 13) {
            var ev = {};
            ev.item = new Array();
            ev.item.label = $(this).val();
            ev.item.value = $(this).val();
            addBlock(ev);
            $(this).val("");
        }
    });
    function addBlock(valor) {
        var divDest = $('.refContainer', '#NotifIncidentes');
        var inputDest = $('input[type="text"]', divDest);
        var spanId = $('<span>').attr('class', 'hide');
        var spanNombre = $('<span>').attr('class', 'nomJur');
        var spanTrash = $('<span>').attr('class', 'icon trash');
        var div = $('<div>').attr('class', 'mailAdd');
        spanId.html(valor.item.value);
        spanNombre.html(valor.item.label);
        div.append(spanId).append(spanNombre).append(spanTrash);
        div.insertBefore(inputDest);
        var widthspnom = spanNombre.width();
        var widthsptr = spanTrash.width();
        var total = widthspnom + widthsptr + 1;
        div.css('width', total++);
    }
    function addBlock2(valor, editable) {
        var divDest = $('#destActuales');
        var inputDest = $('input[type="text"]', divDest);
        var spanId = $('<span>').attr('class', 'hide');
        var spanNombre = $('<span>').attr('class', 'nomJur');
        var spanTrash = $('<span>').attr('class', 'icon trash');
        var div = $('<div>').attr('class', 'mailAdd');
        spanId.html(valor.item.value);
        spanNombre.html(valor.item.label);
        div.append(spanId).append(spanNombre).append(spanTrash);
        divDest.append(div);
        var widthspnom = spanNombre.width();
        var widthsptr = spanTrash.width();
        var total = widthspnom + widthsptr + 1;
        div.css('width', total++);

    }
    $("#NotifIncidentes").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        //position: ['center', 62],
        title: "Enviar Aviso",
        width: 800,
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '101');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '100');
            var datos = $(this).data('datos');
            var strData = 'Estimados:\nEl ticket: ' + datos.NroTicket + ', fue suspendido con las siguientes observaciones:\n';
            if (datos.resoluciones) {
                $.each(datos.resoluciones, function (i, v) {
                    strData += '-' + v.Observ + "\nDiagnosticado por: " + v.Tecnico + '\n';
                });

                strData += 'Saludos.\n';
                strData += (datos.Firma == null) ? '' : datos.Firma;
                $('.asuntoNotificacion', thisdialog).val('Atención - Aviso para el ticket ' + datos.NroTicket);

                $('.bodyMail').val(strData);
                var hei = $('.bodyMail', thisdialog)[0].scrollHeight;
                $('.bodyMail', thisdialog).css('height', hei);
            }
            $(thisdialog).find('.error').hide();
            var divDest = $('.refContainer', thisdialog);
            $('.mailAdd', divDest).remove();
            var inputDest = $('input[type="text"]', divDest);
            if (datos.referentes) {
                $.each(datos.referentes, function (i, v) {
                    var spanId = $('<span>').attr('class', 'hide');
                    var spanNombre = $('<span>').attr('class', 'nomJur');
                    var spanTrash = $('<span>').attr('class', 'icon trash');
                    var div = $('<div>').attr('class', 'mailAdd');
                    spanId.html(v.email);
                    spanNombre.html(v.email);
                    div.append(spanId).append(spanNombre).append(spanTrash);
                    div.insertBefore(inputDest);
                    var widthspnom = spanNombre.width();
                    var widthsptr = spanTrash.width();
                    var total = widthspnom + widthsptr + 1;
                    div.css('width', total);
                });
            }
            //chars
            var spanCounter = $(thisdialog).find('p.charCounter');
            var txtarea = $(thisdialog).find('.tSolucion');
            var str = txtarea[0].value;
            var taLength = 0;
            var lns = str.match(/[^\r]\n/g);
            if (lns) {
                var onlystr = str.length - lns.length;
                taLength = onlystr + lns.length * 2;
            }
            else
                taLength = str.length;
            if (taLength > 3999) {
                spanCounter.addClass('error2').html('Chars:' + taLength + '. El mail se envía completo, pero en la BD se registran los primeros 4000 chars');
            } else  spanCounter.removeClass('error2').html('Chars:' + taLength);
        },
        buttons: {
            Aceptar: function () {
                var dialog = $(this).parents('.ui-dialog');
                var panel = $('.refContainer', dialog);
                var datos = $(this).data('datos');
                datos.subject = $('.asuntoNotificacion', dialog).val();
                datos.body = '';
                var body = $('.bodyMail', dialog).val();
                var destinatarios = $('div.mailAdd>span.hide', panel);
                var jsonDest = [];
                if (destinatarios.length == 0) {
                    mostrarError("No hay referentes para enviar el mail!");
                    return;
                }
                destinatarios.each(function (i, v) {
                    jsonDest.push($(v).html());
                });
                datos.referentes = 'ids=' + jsonDest;
                //var lines = body.replace(/\n/, ',');
                datos.body = body;
                $.ajax({
                    url: "/Service/notificarTickets",
                    data: datos,
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        mostrarMensajeFx("Se esta enviando la Notificación", 'notice', 2000);
                    },
                    success: function (data) {
                        switch (data.Info) {
                            case "Login": mostrarFinSesion();
                                return;
                            case "ok": mostrarExito('Notificacion Exitosa');
                                $("#NotifIncidentes").dialog('close');
                                actualizarPaneles("suspendidos");
                                break;
                            case "algunos errores": mostrarExito("Hubo errores enviando la notificación a: " + data.Detail +
                                ", pero los otros se enviaron bien. Verifique la dirección de email e intente manualmente.");
                                $("#NotifIncidentes").dialog('close');
                                actualizarPaneles("suspendidos")
                                break;
                            default: mostrarError(data.Detail)
                                break;
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        mostrarError(thrownError);
                    },
                    complete: function () {
                        $("#infoInfo").remove();
                    }
                });
            }
            ,
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    //-->Fin notificaciones   
    /***********
    MOSTRAR Tiempos
    ************/
    $('.grilla').on({
        mouseenter: function (event) {
            if (this == event.currentTarget) {
                var info = $(infoDiv);
                $('.ajaxGif', info).hide();
                $('#infoInfo').remove();
                var tip = $(this).find('span').html();
                info.css({
                    "position": "absolute",
                    "top": event.pageY - 50,
                    "left": event.pageX - 270
                }).appendTo('body');
                $('p.mensaje', info).append(tip);
            }
        },
        mouseleave: function (e) {
            if (this == e.currentTarget) {
                $('#infoInfo').remove();
            }
        }
    }, 'div.status.nok, div.status.ok, div.status.nook');
    /*****************************
    *BUSQUEDA INCIDENTES O EQUIPOS
    *****************************/
    $("#searchInc").on('click', function () {
        var datos = "texto=" + $.trim($("#tBuscar").val());
        buscarEquiposInc(datos);
    });
    $("#tBuscar").autocomplete({
        minLength: "2",
        source: searchAutocomplete,
        select: function (e, ui) {
            $('#tBuscar').val(ui.item.value);
            var datos = "texto=" + ui.item.value;
            buscarEquiposInc(datos);
        }
    });
    /*****************
    *BUSQUEDA Clientes
    ******************/
    $("#searchCliente").on('click', function () {
        var datos = "id=" + $.trim($("#tBuscarCliente").val());
        buscarCliente(datos);
    });
    $("#tBuscarCliente").autocomplete({
        minLength: "2",
        source: searchAutocompleteCliente,
        select: function (e, ui) {
            $('#tBuscarCliente').val(ui.item.label);
            var datos = "id=" + ui.item.value;
            buscarCliente(datos);
            return false;
        }
    });
    /*****************
    *Busqueda Inc Activos
    *******************/
    $("#buscarActivos").focus(function (srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("defaultTextActive");
            $(this).val($(this)[0].title);
        }
    }).autocomplete({
        minLength: "2",
        source: (
            function (request, response) {
                var term = '{ prefixText: "' + request.term + '" }';
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/Service/GetIncidentesActivos",
                    data: term,
                    dataType: "json",
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        else {
                            var suggestions = [];
                            $.each(data, function (i, val) {
                                var obj = {};
                                obj.value = val;
                                obj.label = val;
                                suggestions.push(obj);
                            })
                            response(suggestions);
                        }
                    }
                });
            }),
        select: function (e, ui) {
            //$('#tBuscar').val(ui.item.value);
            getActivos(ui.item.value);
        }
    }).trigger('blur');
    $('#searchIncidente').on('click', function () {
        getActivos($('#buscarActivos').val())
    });
    $('body').on('click', '.fa-close', function () {
        $(this).parent().dialog('destroy');
    });
    /***********
    MOSTRAR Descripciones
    ************/
    $('.grilla').on('click', 'div.verObservIncidente', function (event) {
        var info = $.trim($(this).find('span').html());
        if (info.length < 3) info = "Sin Observaciones";
        var dialog = $('<div class="info observTicket">').append(info).dialog({
            show: 'slide',
            position: { my: "right top", at: "top", of: event.target },
            width: 'auto'
        }).click(function (e) {
            if (e.target !== this) return;
            $(this).dialog('destroy')
        });
        $(".ui-dialog-titlebar", $(dialog).parent()).hide();
    });
    //Obtener Imagen Incidente
    $('body').on('click', '.verImagen', function (e) {
        //e.preventDefault();
        var diag = $(this).parents('div.ui-dialog');
        //$(diag).dialog('destroy');
        var idInc = $(this).attr('data-idInc');
        $.post('/Service/pathImagen', { 'id': idInc }, function (data, textstatus, jqxhr) {
            var div = $('<div>');
            div.css({ 'max-width': '500px', 'max-height': '500px' });
            var img = $('<img src="' + data.Detail + '">');
            img.css({ 'width': '100%', 'height': '100%' });
            div.append(img);
            $(div).dialog({
                resize: function (event, ui) {
                    $(div).css({ 'max-width': 'none', 'max-height': 'none' });
                }
            });
        });
    });
    //Obtener imagen resolucion
    $('body').on('click', '.verImgRes', function (e) {
        //e.preventDefault();
        var pathImagen = $(this).attr('data-path');
        //$(diag).dialog('destroy');        
        $.post('/Service/pathImagenResolucion', { 'name': pathImagen }, function (data, textstatus, jqxhr) {
            var div = $('<div>');
            div.css({ 'max-width': '500px', 'max-height': '500px' });
            var img = $('<img src="' + data.Detail + '">');
            img.css({ 'width': '100%', 'height': '100%' });
            div.append(img);
            $(div).dialog({
                resize: function (event, ui) {
                    $(div).css({ 'max-width': 'none', 'max-height': 'none' });
                }
            });
        });
    });
    $('body').on('click', '.clienteName', function (e) {
        var $this = $(this).parents('.ui-dialog');
        var nombre = $(this).find('em span').html();
        if (nombre != '') {
            $.post('/Service/getCliente', { 'nombre': nombre }, function (data, textstatus, jqhr) {
                $('#fichacliente .NombreCl').val(data.nombre);
                $('#fichacliente .DNICl').val(data.dni);
                $('#fichacliente .email').val(data.email);
                if (data.Dependencias != null) {
                    $('#fichacliente .JurisdiccionCl').val(data.Dependencias[0].Jurisdiccion.Nombre);
                    $('#fichacliente .DependenciaCl').val(data.Dependencias[0].Nombre);
                }
                $('#fichacliente .DireccionCl').val(data.Direccion);
                $('#fichacliente .TeCl').val(data.te);
                $('#fichacliente').fadeIn(300).position({ my: "left bottom", at: "right bottom", of: e.target }).on('click', function () {
                    $(this).fadeOut(300);
                });
            });
        }
    });
    /*******************
    MOSTRAR Resoluciones
    ********************/
    $('body').on('click', '.buttonVer', verResolucion);
    /****************************
    VER LAS SOLUCIONES incidentes
    *****************************/
    $(".window").on("click", ".verSolucionIncidente", function (event) {
        if (ajaxReady) {
            var row = $(this).closest('tr');
            var grid = $(row).parents('.grilla');
            var rowsh = grid.find('tr.ui-state-highlight');
            rowsh.removeClass('ui-state-highlight');
            row.addClass('ui-state-highlight');
            var nroInc = row.find("td").eq(1).html();
            var datos = "incidente=" + nroInc;
            $.ajax({
                url: "/Service/buscarSoluciones",
                data: datos,
                type: 'POST',
                beforeSend: function () {
                    ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    var estaV = $(event.target).parents('.window');
                    var html;
                    if (data.length == 0) html = "<p>No se encontraron resoluciones</p>";
                    else {
                        var table = $('<table>');
                        table.append("<tr><th>Fecha</th><th>SubDirección</th><th>Equipo</th><th>Tipo Res.</th><th>Técnico</th><th>Observaciones</th><th>Archivo</th></tr>");
                        $.each(data, function (i, v) {
                            var tr = $('<tr>');
                            var tdFecha = $('<td>').append(v.FechaStr);
                            var tdDir = $('<td>').append(v.Direccion);
                            var tdUc = $('<td>').append(v.Uc);
                            var tdTR = $('<td>').append(v.TipoResolucion);
                            var tdTec = $('<td>').append(v.Técnico);
                            var tdObs = $('<td>').append(v.Observaciones);
                            tr.append(tdFecha).append(tdDir).append(tdUc).append(tdTR).append(tdTec).append(tdObs);
                            var tdImg = $('<td>');
                            if (v.PathImagen != null) {
                                //tdImg.append('<i class="fa fa-image cursor verImgRes" data-path="' + v.PathImagen + '">');
                                tdImg.append('<a target="_blank" rel="noopener noreferrer" href="/Content/img/Resoluciones/' + v.PathImagen + '"><i class="fa fa-file cursor" data-path="' + v.PathImagen + '"></a>');
                            } else {
                                tdImg.append('<i class="fa fa-eye-slash">');
                            }
                            tr.append(tdImg);
                            table.append(tr);
                        });
                        html = table;
                    }
                    var dialog = $('<div class="info"><span class="fa fa-close topright cursor"></span>').append(html).dialog({
                        show: 'slide',
                        open: function () {
                            $(this).parent().find(".ui-dialog-titlebar").hide();
                        },
                        position: {
                            my: "top ",
                            at: "center bottom",
                            of: event.target,
                            using: function (obj, info) {
                                var viewpheight = $(window).height();
                                var heithtml = $('html').height();
                                var widthhtml = $('html').width();
                                var dialogH = info.element.height;
                                var dialogw = info.element.width;
                                var overflowsV = dialogH > heithtml-105;
                                var overflowsH = dialogw > widthhtml;
                                var posclickX = event.clientX;
                                var posclickY = event.clientY;
                                if (posclickY + dialogH > viewpheight) {

                                    $(this).css({
                                        left: 0 + 'px',
                                        top: 105 + 'px',
                                        'max-height': viewpheight - 105,
                                        overflow: overflowsV?'auto':'none'
                                    });
                                } else {
                                    $(this).css({
                                        left: 0 + 'px',
                                        top: info.target.top,
                                    });
                                }
                            }
                        },
                        width: 'auto'
                    })
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    ajaxReady = true;
                }
            }); /*fin ajax*/
        } //fin ajaxready
    });
    $('.eliminarInc').on('click', eliminarIncidente);
    $('.reactivacion').on('click', reactivarIncidente);
    //file uploads   
    $('body').on('click', '.clearIF', function () {
        var iF = $(this).siblings('input[type="file"]')[0];
        clearInputFile(iF);
    });
    //$('#image').on('click', 'div.removeImagen', removeImagen);   
    $('#Trabajos').on('change', '#upFileRes', function () {
        var _this = this;
        var spinner = $('<div style="width:50px;margin:0 auto"><img src="/Content/img/ajax-loader.gif"/></div>');
        if (this.disabled) {
            mostrarError('El Navegador no soporta carga de Archivos!', 'error');
            return;
        }
        var file = this.files[0];
        if (file != null) {
            var reader = new FileReader();
            NProgress.start();
            reader.readAsDataURL(file);
            reader.onload = function (_file) {
                NProgress.done();
                var s = file.size;
                var megas = (s / (1024 * 1024));
                if (megas > 100) {
                    mostrarError('El tamaño del archivo supera el máximo permitido(100MB)', 'error');
                    clearInputFile(_this);
                }
                var t = file.type;
                var h = file.name;
                var extension = h.split('.');
                if (file.type === 'application/octet-stream' | extension[extension.length - 1] === 'exe') {
                    mostrarError('Los ejecutables no están permitidos', 'error');
                    clearInputFile(_this);
                }
                else {
                    //nombre en el label
                    var boxpadre = $(_this).parent();
                    $(_this).siblings('label').contents().filter(function () {
                        return this.nodeType == 3;
                    })[0].nodeValue = h;
                    boxpadre.find('i.clearIF').remove();
                    var labelR = $('<i class="fa fa-times clearIF cursor" aria-hidden="true"></i>');
                    boxpadre.append(labelR);
                }
            }
            reader.onerror = function (e) {
                mostrarError(e, 'error')
            }
        } else {
            var boxpadre = $(_this).parent();            
            boxpadre.find('i.clearIF').remove();
            clearInputFile();
        }
    });
    //end file upload
    //function connect
    //var connecting;
    var connecting = function () {
          
        var chat = $.connection.Chat;
        //create a function the hub can call        
        chat.client.ChatUsers = function (messaje) {
            // return false;

            $('#Chat-Lista').html('');

            $.each(messaje, function (i, v) {

                // var UsuarioLogeado = $('#idUser').val();

                var Grupo = document.createElement('div');
                Grupo.className = 'acordeonChat ui-widget-header';
                Grupo.textContent = v.Direccion;
                
                $('#Chat-Lista').append(Grupo);

                /* -> */    var AcDir = v.Direccion.replace(new RegExp(' ', 'g'), '-'); // Crea id del acordeon
                var divAcordeon = document.createElement('div');
                divAcordeon.setAttribute('id', 'Ac-'+AcDir)
                divAcordeon.setAttribute('style', 'display: none');

                for (i=0; i<v.Usuarios.length; i++)
                    {
                        if (v.Usuarios[i].Id != UsuarioLogeado)
                            {
                                // Objeto para la funcion AbrirChat
                                AbrirChatObj = {};
                                AbrirChatObj.id = v.Usuarios[i].Id;
                                AbrirChatObj.nombre = v.Usuarios[i].Nombre;
                                AbrirChatObj.avatar = v.Usuarios[i].Avatar;
                                AbrirChatObj.Direccion = v.Usuarios[i].Direccion;
                                AbrirChatObj.OnOffLine = v.Usuarios[i].Online;
                
                                // Item li
                                var li = document.createElement('li');
                                li.classList.add('contacto');
                                li.classList.add('ui-widget-content');
                                if (v.Usuarios[i].Online != true)
                                    {
                                        li.classList.add('offline');
                                    }
                                li.dataset['id'] = AbrirChatObj.id;
                                li.dataset['nombre'] = AbrirChatObj.nombre;
                                li.dataset['avatar'] = AbrirChatObj.avatar;
                                li.dataset['onoffline'] = AbrirChatObj.OnOffLine;
                                // li.dataset['page'] = '';

                                //li.setAttribute('onClick', 'AbrirChat("'+v.Usuarios[i].Id+'","'+v.Usuarios[i].Nombre+'","'+v.Usuarios[i].Avatar+'",'+v.Usuarios[i].Online+'", '+$(this)+')');
                                li.id = v.Usuarios[i].Id;
                
                                var div = document.createElement('div');
    
                                // Avatar
                                var cAvatar = document.createElement('avatar');
                                cAvatar.classList.add('avatar');
                                if (v.Usuarios[i].Avatar != '/Content/img/Photo/')
                                    {
                                        cAvatar.setAttribute('style', 'background-image:url("'+v.Usuarios[i].Avatar+'"');
                                    }
                                div.appendChild(cAvatar);
                                // Avatar
    
                                // Nombre
                                var textnode = document.createTextNode(v.Usuarios[i].Nombre);
                                // Nombre
    
                                // Adjuntar
                                div.appendChild(textnode);
                                li.appendChild(div);
                                divAcordeon.appendChild(li);
                            }

                        $('#Chat-Lista').append(divAcordeon);

                        // Si hay mensajes no leidos
                        if ( v.Usuarios[i].MensajesNoLeidos === true )
                            {
                                ContactBadge(v.Usuarios[i].Id);
                                UpdateBadges('sum');
                            }
                    }
            });
        }
        chat.client.Conected = function (messaje) {
            /* -> */    var AcDir = messaje.Direccion.replace(new RegExp(' ', 'g'), '-'); // Crea id del acordeon
            var li = $('#Ac-'+AcDir).find('li#'+messaje.Id).removeClass('offline').clone(); // Encuentra y clona
            $('#Ac-'+AcDir).find('li#'+messaje.Id).remove(); // Borra anterior
            $('#Ac-'+AcDir).prepend(li); // Mueve al final
            console.log('Usuario conectado');
        }
        chat.client.Disconected = function (messaje) {
            /* -> */    var AcDir = messaje.Direccion.replace(new RegExp(' ', 'g'), '-'); // Crea id del acordeon
            var li = $('#Ac-'+AcDir).find('li#'+messaje.Id).addClass('offline').clone(); // Encuentra y clona
            $('#Ac-'+AcDir).find('li#'+messaje.Id).remove(); // Borra anterior
            $('#Ac-'+AcDir).append(li); // Mueve al final
            console.log('Usuario desconectado');
        }
        chat.client.mensajeAmi = function (mensaje) {
            var Busqueda = $('#Chat-'+mensaje.Id_From);
            var isVisible = Busqueda.hasClass('chat-front');

            if (Busqueda.length) // Verifica si existe el chat y esta visible
                {                    
                    var cLI = document.createElement('li');
                    cLI.className = 'in';
                    cLI.setAttribute('data-msjid', mensaje.Id); // ID del mensaje
                    cLI.setAttribute('data-estado', mensaje.Leido); // Estado del mensaje
                    
                    var cAvatar = document.createElement('avatar');
                    cAvatar.className = 'avatar';
                    if (mensaje.Avatar != '/Content/img/Photo/')
                        {
                            cAvatar.setAttribute('style', 'background-image:url("'+mensaje.Avatar+'"');
                        }

                    cLI.appendChild(cAvatar); 

                    var cMessage = document.createElement('div');
                    cMessage.className = 'message';
    
                    var cMText = document.createTextNode(mensaje.Mensaje);
    
                    var gettingHora = mensaje.Hora;
                    gettingHora = gettingHora.split(':');

                    var cHora = document.createElement('span');
                    cHora.className = 'time';
                    cHora.textContent = gettingHora[0]+':'+gettingHora[1];
    
                    // Armando el body del chat
                    cMessage.appendChild(cMText); // Adjunta el texto
                    cMessage.appendChild(cHora); // Adjunta la hora
    
                    cLI.appendChild(cMessage); // Adjunta el mensaje

                    $('#Chat-'+mensaje.Id_From+' ul').append(cLI);
                    
                    if (isVisible == false) // Si este chat esta oculto
                        {
                            if (MostrarChat() == true) // Si hay algun chat abierto
                                {
                                    UpdateBadges('sum');
                                    ContactBadge(mensaje.Id_From);
                                }
                            else
                                {
                                    AbrirChat(mensaje.Id_From);
                                }
                        }
                    else
                        {
                            animateChat(mensaje.Id_From);
                        }
                    
                    // Actualizar estado de los mensajes
                    var ChatSelect = '#Chat-'+mensaje.Id_From; // Guardo el id de chat
                    // Evento personalizado para buscar mensajes no leidos
                    $.event.trigger({ // Lanza el evento personalizado
                        type: "updateMsjStatusEv", // Declaracion del evento personalizado
                        message: ChatSelect // Parametro enviado al evento personalizado
                    })
                }
            else // Si no existe el chat
                {
                    if (MostrarChat() == true) // Si hay algun chat abierto
                        {
                            UpdateBadges('sum');
                            ContactBadge(mensaje.Id_From);
                        }
                    else
                        {
                            // var nombre = $('#Chat-Lista li#'+mensaje.Id_From+' div').text();
                            AbrirChat(mensaje.Id_From, mensaje.NombreFrom, mensaje.Avatar);
                        }
                }

            if (has_focus === false)
            {
                var htmlNotif = {};
                htmlNotif.cNombre = mensaje.NombreFrom;
                htmlNotif.msj = mensaje.Mensaje;
                chat_html_notif(htmlNotif)
            }
                
            console.log('Nuevo mensaje de '+mensaje.Id_From+': ('+mensaje.Mensaje+' - '+mensaje.Hora+')');
        };
        chat.client.error = function (mensaje) {
            mostrarError(mensaje);
        }
        $.connection.hub.start().done(function () {
            chat.state.idUsuario = $('#idUser').val();
            chat.server.hello();
            $('.chat-box').on('click', '.button', function () {
                var gettingID = $('.chat-front').attr('id');
                gettingID = gettingID.split("-");

                var idContacto = gettingID[1];
                var idLoging = $('#idUser').val();
                var mensaje = $('.chat-front input[type=text]').val();
                var n = mensaje.length;
                var Notspace = false;

                for (i = 0; i < n; i++) {
                    if (!(mensaje[i] == ' ')) {
                        Notspace = true;
                        break;
                    }
                }

                if (mensaje && mensaje.length != 0 && Notspace == true) {
                    chat.server.enviarMensajeA(idLoging, idContacto, mensaje); // Envia el mensaje
                    $('.chat-front input[type=text]').val(''); // Vacia la caja de texto

                    // Adjunta el nuevo mensaje a la ventana de chat
                    var cLI = document.createElement('li');
                    cLI.className = 'out';

                    var cMessage = document.createElement('div');
                    cMessage.className = 'message';

                    var cMText = document.createTextNode(mensaje);

                    // Hora
                    function addZero(i) {
                        if (i < 10) {
                            i = "0" + i;
                        }
                        return i;
                    }
                    var d = new Date();
                    var h = addZero(d.getHours());
                    var m = addZero(d.getMinutes());

                    var cHora = document.createElement('span');
                    cHora.className = 'time';
                    cHora.textContent = h + ":" + m;

                    // Armando el body del chat
                    cMessage.appendChild(cMText); // Adjunta el texto
                    cMessage.appendChild(cHora); // Adjunta la hora

                    cLI.appendChild(cMessage); // Adjunta el mensaje

                    $('#Chat-' + idContacto + ' ul').append(cLI);
                    animateChat(idContacto);
                    
                    console.log('Mensaje enviado a '+idContacto+': ('+mensaje+' - '+h+":"+m+')');
                }
            });
            $('.chat-box').on('keydown', 'input[type=text]', function (e) {
                if (e.keyCode == 13) {
                    var chatID = $('.chat-front').attr('id');

                    $('#' + chatID + ' .button').trigger('click');
                }
                else if (e.keyCode == 32 ||
                        (e.keyCode >= 48 && e.keyCode <= 57) ||
                         e.keyCode == 60 ||
                        (e.keyCode >= 65 && e.keyCode <= 90) || 
                        (e.keyCode >= 96 && e.keyCode <= 111) || 
                         e.keyCode == 45 ||
                         e.keyCode == 59 ||
                         e.keyCode == 171 ||
                        (e.keyCode >= 172 && e.keyCode <= 175) ||
                        (e.keyCode >= 186 && e.keyCode <= 192) ||
                         e.keyCode == 219 ||
                        (e.keyCode >= 221 && e.keyCode <= 222)) {
                    var chars = $(this).val();
                    if(chars.length >= 1000)
                        {
                            e.preventDefault();
                            mostrarError('Ah llegado al limite de caracteres para un solo mensaje');
                        }
                }
            })
            $('.chat-box').on('click', '.CloseWindow', function () {
                var Chat = $(this).closest('.chat-front');
                CerrarChat(Chat);
            });
        });      
    };
    // Mas mensajes control
    $('.chat-box').on('click', '.more', function(){ // Al tocar un boton de MAS MENSAJES
        var _selfMore = $(this);
        AbrirChat(_selfMore.data('id'), _selfMore.data('nombre'), _selfMore.data('avatar'), _selfMore.data('onoffline'), _selfMore.data('page')); // Abre el chat pasando los parametros
    });
    // Abrir chat contacto control
    $('#Chat-Lista').on('click', 'li', function(){
        var _selfContact = $(this); // Guarda el contacto clickeado
        AbrirChat(_selfContact.data('id'), _selfContact.data('nombre'), _selfContact.data('avatar'), _selfContact.data('onoffline'), _selfContact.data('page')); // Abre el chat pasando los parametros
    })
    //declare a proxy   
    var args=[setColors,quitOverlays,connecting];
    setSize(args);
    startAJAXcalls();   
});/**READY**/
/**********
**Chat Users
***********/
function listUsuarios(usuarios)
{
    
}
function mensajeAMi(mensaje)
{
   
}
//clear input image
function clearInputFile(_this) {
    if (_this != undefined) {
        var fileInput = document.createElement('input');
        fileInput.setAttribute('id', 'upFileRes');
        fileInput.setAttribute('type', 'file');
        fileInput.setAttribute('class', 'uploadFile');
        _this.parentNode.replaceChild(fileInput, _this);
        _this.removeAttribute("disabled");
        $(fileInput).siblings('label').contents().filter(function () {
            return this.nodeType == 3;
        })[0].nodeValue = "Archivo";
        var pare=$(fileInput).siblings('i.clearIF');
        pare.remove();
    }
}
function removeImagen()
{

}
/*****************
**REACTIVACION INC
******************/
function reactivarIncidente() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN Ticket");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        $.ajax({
            url: "/Service/reactivarIncidente",
            data: {'idInc':id_inc},
            type: 'POST',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == "ok") {
                    mostrarExito('El ticket fue reactivado y se encuentra en el área ' + data.Detail);
                    exito = true;
                }
                else {
                    mostrarError(data.Detail);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                actualizarPaneles('todos');
            }
        });
    }
}
function mostrarNotificacInc(e) {    
    var row = $(e.target).parents('tr').find('td');    
    var idIncidente = row.eq(1).text();   
    $.ajax({
        data: { 'idInc': idIncidente },
        url: '/Service/NotificacionesIncidentes',
        type: 'POST',
        success: function (res) {
            var dialog = $('<div>');
            var result = JsonUl(res);
            dialog.append(result).dialog({
                width: '500px',
                show: 'slide',
                maxHeight: 300,
                position: { my: "right top", at: "right bottom", of: e.target }

            }).click(function () { $(this).dialog('destroy') });
            $(".ui-dialog-titlebar").hide();
        },
        error: function (xhr, error, thrown) {
            mostrarError(error);
        }
    });
}
function JsonUl(res) {
    var result = "<ul class='quickView2'>";
    if (res.length == 0) { return "Sin resultados" };

    var prefix = '';
    $.each(res, function (i, item) {
        result += "<li><label class='ui-widget-header'>E-mails: </label><label>";
        $.each(item.mailsAdd, function (j, k) {
            result += prefix + k;
            prefix = ', ';
        });
        result += "</label></li>";
        result += "<li><label class='ui-widget-header'>Fecha: </label><label>" + item.fecha + "</label></li>";
        result += "<li><label class='ui-widget-header'>Mensaje: </label><label>" + item.mensaje + "</label></li>";
        result += "<li><label class='ui-widget-header'>Enviado por: </label><label>" + item.enviadoPor + "</label></li>";
        result += "<li>-------------</li>"
    });
    result += "</ul></li></ul>";
    return result;
}
function searchAutocompleteCliente(request, response) {
    var texto = request.term;    
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Service/GetClientesActivos",
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val.id;
                    obj.label = val.nombre;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR Cliente*/
function buscarCliente(datos) {
    if (ajaxReady) {
        $.ajax({
            url: "/Service/incidenteXCliente",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("No hay Tickets para ese cliente");
                    $('p.ajaxGif').hide();
                }
                else {
                    var ul = $('<ul>');
                    $.each(data, function (i, v) {
                        var final = crearResumen(v.Desc)// Guarda el resumen de la descripcion
                        var li = $('<li>');
                        li.append(v.Numero).append(' - ');
                        li.append(v.SubDireccion).append(' - ');
                        li.append(v.Tipo).append(' - ');
                        li.append(v.FechaStr);
                        li.attr('data-Desc', final)
                        li.data('datos', v);
                        li.on('click', function () {
                            ubicarIncidente(v);
                            ul.dialog('destroy');
                        });
                        ul.append(li);
                    });
                    
                    $(ul).dialog({
                        create: function (e, ui) {
                            $(this).dialog('widget').removeClass('ui-corner-all').find('.ui-dialog-titlebar, .ui-button').removeClass('ui-corner-all')
                        },
                        title: 'Tickets encontrados',
                        dialogClass: 'TicketsXCliente',
                        width: '65%'
                    });
                    // Crea dialogo jQuery UI con los resultados
                    //-->
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
function crearResumen(resumen) {
    var original = resumen;
    var separador = ' ';
    var final;

    arreglo = original.split(separador, 20)

    final = arreglo.join(' ');

    return final;
}
// Crea un resumen de la descripcion
//-->
/******************
*Eliminar Incidente
******************/
function quitOverlays() {
    $('.content-box').removeClass('overlay');
}
function eliminarIncidente() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN Ticket");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);
        var divdialog = $('<div>').append("Se va a eliminar el Ticket " + $(trsel[1]).text() + ". Está Seguro?").dialog({
            width: '250px',
            show: 'slide',
            //position: 'center',
            open: function () {
                $(this).parents('.ui-dialog').css({ 'font-size': '.9em', 'z-index': '99' }).find('div.ui-dialog-titlebar').hide();
            },
            buttons: {
                Aceptar: function () {
                    $.ajax({
                        url: "Incidentes/eliminarIncidente",
                        data: { idInc: $(trsel[0]).text() },
                        type: 'POST',                       
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarInfo('Se ha Eliminado el Ticket');
                                exito = true;
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {                            
                            $(divdialog).dialog('destroy');
                            if (exito) {
                                actualizarPaneles('todos');
                            }
                        }
                    });               //fin ajax
                },
                Cancelar: function () { $(this).dialog('destroy') }
            }

        });
    }    
}
/*************
*GET ACTIVOS
**************/
function getActivos(nroInc) {    
    var resString = nroInc.toUpperCase();
    if (!regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }
    if (ajaxReady) {
        $.ajax({
            url: "/Service/UbicacionIncidente",
            data: {'nroInc':resString},
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                var json = data;
                var content = $("#incidenteContent");
                if (json == null) {
                    mostrarError("No se Encuentra el Ticket");
                    return;
                }
                if (json.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                content.show();
                content.find("label[for='lNroInc']").html(json.Numero);
                content.find("label[for='lTipificacion']").html(json.Tipificacion);
                content.find("label[for='lJurisdiccion']").html(json.Jurisdiccion);
                content.find("label[for='lDependencia']").html(json.Dependencia);
                toHtmlEquiposeIncidentes(json);
                content.css('display', 'inline');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
/***************
SCROLL DE BUSCAR
****************/
function doScroll(ventanaObj, celda) {
    if ($(ventanaObj).length > 0) {
        $.each(ventanas, function (i, v) {
            if (v.max) v.maximize();
        });
        if (!ventanaObj.visible) {
            ventanaObj.mostrarVentana();            
        }        
        var $div = ventanaObj.allWindow;
        try {           
            var $td = $($div).find('td').filter(function () { return $(this).text() == celda; })            
            if ($td.length == 0) {
                mostrarInfo("El Ticket o Equipo no está activo!!!");
                return;
            }
            var offsetDiv = $($div).offset();
            var positionDiv = $($div).position();
            var scrollCont = $('.incidentesContent').scrollTop();
            var positionTd = $($td).position();
            var grilla = $($div).find('.grilla');
            var scrollGrilla = $(grilla).scrollTop();
            //$('.incidentesContent').scrollTop(positionDiv.top);
            $('.incidentesContent').animate({
                scrollTop: positionDiv.top + scrollCont
            }, 500, function () {
                $(grilla).animate({
                    scrollTop: positionTd.top + scrollGrilla
                }, 500);
            });
            //$($div).animate({
            //    scrollTop: positionTd.top
            //}, 500);
            //$.scrollTo($div, 500, { offset: -230 });
            //$div.stop().scrollTo($td, 800);
            $($td).closest('tr').addClass('ui-state-highlight');
        }
        catch (e) {
            mostrarError("Ha habido un error inesperado. Presione F5 e intente de nuevo");
        }
    }
}
/***********
*clear trabajos
***********/
function clearTrabajosPanel(context) {
    $('.tipoSolucion', context).val(0);
    $('.observaciones', context).val('');
    $('input[type="text"]', context).val('');
    $('input[type="checkbox"]', context).prop('checked', false);
    $('.esParaEquipo', context).trigger('change');
    $('li.hide', context).removeClass('hide');
    $('.error', context).hide();
    $('li.ticketContainer', context).hide();
    $('li.enlaceContainer', context).hide();
    var inpFile = context[0].getElementsByClassName('uploadFile')[0];
    clearInputFile(inpFile);
}
/****************************
 HABILITAR O DESHABILITAR UPDATES
 ****************************/
function disableUpdates() {
    if (window.updatesIncidente) {
        window.updatesIncidente = false;
        $(this).find('span').html("Iniciar Actualizaciones");
        stopAjax();
    }
    else {
        window.updatesIncidente = true;
        $(this).find('span').html("Parar Actualizaciones");
        startAJAXcalls();
    }
}
window.stopAjax=function() {
    window.clearTimeout(timer);
}
function startAJAXcalls() {
    timer = setTimeout(function () {
        actualizarPaneles('todos');
        startAJAXcalls();
    },
		    FREQ)
}

/********************
FUNCTION Set Ventanas
*********************/
function setSize(callback) {
    $.ajax({
        url: "/Service/getUsuarioValues",
        dataType: 'json',
        type: 'post',
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            var ventanasUsuario = data.ventanas;
            var $ventanas = $('.window');
            $.each($ventanas, function (i, v) {
                var id = v.id;
                var ventanaObj = new Ventana($(v));
                /*asigno en vector*/
                switch (id) {
                    //Direccion
                    case "TotalesDireccion": ventanas[0] = ventanaObj;
                        break;
                    //-->fin direccion
                    //Operaciones
                    case 'OpeBandeja': ventanas[0] = ventanaObj;
                        break;
                    case 'OpeOperaciones': ventanas[1] = ventanaObj;
                        break;                    
                    case 'OpeCorreo': ventanas[2] = ventanaObj;
                        break;
                    case 'OpeSusp': ventanas[3] = ventanaObj;
                        break;
                    case 'OpeEnlaces': ventanas[4] = ventanaObj;
                        break;
                        //-->Fin Operaciones
                        //direccion
                    case "TotalesDireccion": ventanas[0] = ventanaObj;
                        break;
                        //-->Fin direccion
                        //Infraestructura
                    case "InfraBandeja": ventanas[0] = ventanaObj;
                        break;
                    case "InfraInfraestructura": ventanas[1] = ventanaObj;
                        break;
                    case "InfraSo": ventanas[2] = ventanaObj;
                        break;
                    case "InfraSusp": ventanas[3] = ventanaObj;
                        break;
                        //-->Fin Infra
                        //Mesa de Ayuda
                    case "MesaBandeja": ventanas[0] = ventanaObj;
                        break;
                    case "MesaSuspendidos": ventanas[1] = ventanaObj;
                        break;
                    case "MesaTotales": ventanas[2] = ventanaObj;
                        break;
                        //-->Fin Ayuda
                        //Telecomunicac
                    case "IncIng": ventanas[0] = ventanaObj;
                        break;
                    case "IncDatos": ventanas[1] = ventanaObj;
                        break;
                    case "IncTele": ventanas[2] = ventanaObj;
                        break;
                    case "TeleSusp": ventanas[3] = ventanaObj;
                        break;
                        //-->Fin Tele
                        //seguridad
                    case "BandejaSeguridad": ventanas[0] = ventanaObj;
                        break;
                    case "PendSeguridad": ventanas[1] = ventanaObj;
                        break;
                    case "SuspSeguridad": ventanas[2] = ventanaObj;
                        break;
                        //-->Fin seguridad
                        //soporte
                    case 'Bandeja': ventanas[0] = ventanaObj;
                        break;
                    case 'DivIncSus': ventanas[1] = ventanaObj;
                        break;
                    case 'DivIncTotales': ventanas[2] = ventanaObj;
                        break;
                    case 'DivIncIng': ventanaObj.Nombre = "Incidentes Ingresados";
                        ventanas[0] = ventanaObj;
                        break;
                    case 'DivIncAt': ventanaObj.Nombre = "Incidentes en Atención a Usuarios";
                        ventanas[1] = ventanaObj;
                        break;
                    case 'DivIncLog': ventanaObj.Nombre = "Incidentes en Logística";
                        ventanas[2] = ventanaObj;
                        break;
                    case 'DivEqClon': ventanaObj.Nombre = "Equipos en Clonado";
                        ventanas[3] = ventanaObj;
                        break;
                    case 'DivEqLab': ventanaObj.Nombre = "Equipos en Laboratorio";
                        ventanas[4] = ventanaObj;
                        break;
                    case 'DivEqDel': ventanaObj.Nombre = "Equipos Para Entregar";
                        ventanas[5] = ventanaObj;
                        break;
                    case 'DivEqSus': ventanaObj.Nombre = "Equipos Suspendidos";
                        ventanas[6] = ventanaObj;
                        break;
                    case 'DivEqParaGar': ventanaObj.Nombre = "Equipos Para Garantía";
                        ventanas[7] = ventanaObj;
                        break;
                    case 'DivEqEnGar': ventanaObj.Nombre = "Equipos En Garantía";
                        ventanas[8] = ventanaObj;
                        break;
                        //-->soporte                   
                }
                ventanaObj.toUi();
                /*busco el tamaño*/
                var result = $.grep(ventanasUsuario, function (e) { return e.id == id });
                if (result.length > 0) {
                    ventanaObj.setSize(result[0].width, result[0].height, result[0].visible);
                } else {
                    $.post("/Service/recreateWindows", { usuario: $('#user').html(), ventanaId: id });
                    ventanaObj.setDefaultSize();
                }                
            });//Fin each            

            var perfil = new Perfiles(data.Perfiles);
            var ventanasHabilitadas = perfil.getVentanas();
            for (var i = 0; i < ventanasHabilitadas.length; i++) {
                for (var j = 0; j < ventanas.length; j++) {
                    if (ventanasHabilitadas[i].nombre == ventanas[j].id) {
                        if (!ventanasHabilitadas[i].visible) ventanas[j].noVisible();
                        if (!ventanasHabilitadas[i].visibleBotones) ventanas[j].ocultarBotones();
                        if (!ventanasHabilitadas[i].visibleNoti) ventanas[j].ocultarNotificaciones();
                        if (!ventanasHabilitadas[i].visibleUbi) ventanas[j].ocultarUbicar();
                    }
                }
                
            }
            var user = $('#user').html();
            var fecha = new Date();
            var hours = fecha.getHours();
            var turno = hours >= 14 ? 'Tarde' : 'Mañana';
            setDropText(user, $('#dTecnicos3'));
            setDropText(turno, $('#dTurno'));
        },
        error: function (xhr, error, thrown) {
            mostrarError("Error en el tamaño de las ventanas.");
        },
        complete: function () {
            for (var i = 0; i < callback.length; i++) {
                if (callback[i] && typeof (callback[i]) === 'function') {
                    callback[i]();
                }
            }
        }
    });
}
/********************
FUNCTION Nuevo Incidente
*********************/
function derivarDireccion() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Ticket");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);
        var subdirId=$('.sDireccion option:selected',$(this).parents('.botones')).val();
        var subdirName=$('.sDireccion option:selected',$(this).parents('.botones')).text();
        var divdialog = $('<div>').append("Se va a derivar el Ticket " + $(trsel[1]).text() + " a "+subdirName+". Está Seguro?").dialog({
            width: '250px',
            show: 'slide',
            //position: 'center',
            open: function () {
                $(this).parents('.ui-dialog').css({ 'font-size': '.9em' ,'z-index':'99'}).find('div.ui-dialog-titlebar').hide();
            },
            buttons: {
                Aceptar: function () {
                    $.ajax({
                        url: "Incidentes/derivarADireccion",
                        data: { idInc: $(trsel[0]).text(), idDir: subdirId },
                        type: 'POST',
                        beforeSend: function () {
                            ajaxReady = false;
                        },
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarInfo('Se ha Derivado el Ticket');
                                exito = true;
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            ajaxReady = true;
                            $(divdialog).dialog('destroy');
                            if (exito) {
                                actualizarPaneles('todos');
                            }
                        }
                    });               //fin ajax
                },
                Cancelar: function () { $(this).dialog('destroy') }
            }

        });
    }
}
/*************************
FUNCTION Cambiar prioridad
**************************/
function setPrioridad() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN Ticket");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);
        var prioridadId = $('#prioridadTicket option:selected', $(this).parents('.botones')).val();
        var prioridadName = $('#prioridadTicket option:selected', $(this).parents('.botones')).text();
        var divdialog = $('<div>').append("Se va a cambiar la prioridad del ticket " + $(trsel[1]).text() + " de " + $(trsel[6]).text() +" a " + prioridadName + ". Está Seguro?").dialog({
            width: '250px',
            show: 'slide',
            //position: 'center',
            open: function () {
                $(this).parents('.ui-dialog').css({ 'font-size': '.9em', 'z-index': '99' }).find('div.ui-dialog-titlebar').hide();
            },
            buttons: {
                Aceptar: function () {
                    $.ajax({
                        url: "/Service/setPrioridadTicket",
                        data: { idTicket: $(trsel[0]).text(), idPrioridad: prioridadId, descripcion:"" },
                        type: 'POST',                        
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarInfo('Se ha Modificado la prioridad del Ticket!');
                                exito = true;
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            actualizarPaneles("ingresados")
                        }
                    });               //fin ajax
                    $(this).dialog('destroy')
                },
                Cancelar: function () { $(this).dialog('destroy') }
            }

        });
    }
}
/****************
*parse incidentes activos
***************/
function toHtmlEquiposeIncidentes(json) {
    var resolucionesIncidente;
    var resolucionesEquipo;
    var equipos = "";
    var areasXIncidente = "";
    $.each(json.Areas, function (i, item) {
        resolucionesIncidente = "<table class='hide info'><tr><th>Fecha</th><th>Técnico</th><th>T.Resolución</th><th>Equipo</th><th>Trabajo</th></tr>";
        if (item.Resoluciones.length > 0) {
            $.each(item.Resoluciones, function (i, item2) {
                resolucionesIncidente += '<tr><td>' + item2.FechaStr + '</td>' + '<td>' + item2.Tecnico + '</td>' +
            '<td>' + item2.TipoResolucion + '</td>' + '<td>' + item2.Equipo + '</td>' + '<td>' + item2.Observaciones + '</td></tr>';
            })
        }
        resolucionesIncidente += "</table>";
        var counter = $('tr', resolucionesIncidente).length;
        areasXIncidente += '<tr><td>' + item.Direccion + '</td><td>' + item.FechaDesdeShStr + '</td><td>' + item.FechaHastaShStr + '</td>'
                        + '<td class="tObs ' + (counter == 1 ? "buttonNa" : "buttonVer") + '">' + resolucionesIncidente + '</td></tr>';
    });
    $("#areasIncidente tbody").html(areasXIncidente);    
}
function verResolucion(event) {
    //parametros principales
    var html = $(this).find('table').clone().removeClass('hide');
    var dialog = $('<div class="info">').append(html).dialog({
        show: 'slide',
        position: { my: "right top", at: "top", of: event.target },
        width: 'auto'
    }).click(function () { $(this).dialog('destroy') });
    $(".ui-dialog-titlebar", $(dialog).parent()).hide();
}
function tomarIncidente() {
    var exito = false;
    var nro = [];
    var User = $('#user').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Ticket");
        return;
    }
    else {
        var id_inc = [];
        for (i = 0; i < nro.length; i++) {
            id_inc.push($.trim(nro[i].find('td:nth-child(2)').text()));
        }
    }
    setTecnico(User, id_inc, nombreGrilla);
}
function asignarIncidente() {
    var exito = false;
    var nro = [];
    var tecnico = $(this).parent().siblings().find('select.sTecnico option:selected').text();
    var idTecnico = $(this).parent().siblings().find('select.sTecnico option:selected').val();
    if (idTecnico == 0) {
        mostrarError('Seleccione el técnico');
        return;
    }
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Ticket");
        return;
    }
    else {
        var id_inc = [];
        for (i = 0; i < nro.length; i++) {
            id_inc.push($.trim(nro[i].find('td:nth-child(2)').text()));
        }
    }
    setTecnico(tecnico, id_inc, grilla);
}
function setTecnico(Tecnico, id_inc, grilla) {
    $.ajax({
        url: "Incidentes/asignarIncidente",
        data: "id_inc=" + id_inc + "&tecnico=" + Tecnico,
        datatype: 'json',
        type: 'POST',
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (data.Info == "ok") {
                mostrarExito('Ticket Asignado a: ' + Tecnico);
                actualizarPaneles('todos');
            }
            else {
                mostrarError(data.Detail);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            $('p.ajaxGif').hide();            
        }
    });              //fin ajax        
}
function suspenderIncidente()
{
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN Ticket");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);        
        var divdialog = $('<div>').append("Se va a Suspender el Ticket " + $(trsel[1]).text() + ". Está Seguro?").dialog({
            width: '250px',
            show: 'slide',
            //position: 'center',
            open: function () {
                $(this).parents('.ui-dialog').css({ 'font-size': '.9em', 'z-index': '99' }).find('div.ui-dialog-titlebar').hide();
            },
            buttons: {
                Aceptar: function () {
                    $.ajax({
                        url: "Incidentes/suspenderIncidentes",
                        data: { idInc: $(trsel[0]).text() },
                        type: 'POST',                        
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarInfo('Se ha Suspendido el Ticket!');
                                exito = true;
                            }
                            else {
                                mostrarError(data.Detail, 'error');
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError, 'error');
                        },
                        complete: function () {
                            ajaxReady = true;
                            $(divdialog).dialog('destroy');
                            if (exito) {
                                $('p.ajaxGif').hide();
                                actualizarPaneles('todos');
                            }
                        }
                    });               //fin ajax
                },
                Cancelar: function () { $(this).dialog('destroy') }
            }

        });
    }
}
function activarIncidente()
{
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN ticket");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);
        var divdialog = $('<div>').append("Se va a Activar el Ticket " + $(trsel[1]).text() + ". Está Seguro?").dialog({
            width: '250px',
            show: 'slide',
            //position: 'center',
            open: function () {
                $(this).parents('.ui-dialog').css({ 'font-size': '.9em', 'z-index': '99' }).find('div.ui-dialog-titlebar').hide();
            },
            buttons: {
                Aceptar: function () {
                    $.ajax({
                        url: "Incidentes/activarIncidentes",
                        data: { idInc: $(trsel[0]).text() },
                        type: 'POST',
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarInfo('Se ha Activado el Ticket!');
                                exito = true;
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            ajaxReady = true;
                            $(divdialog).dialog('destroy');
                            if (exito) {
                                $('p.ajaxGif').hide();
                                actualizarPaneles('todos');
                            }
                        }
                    });               //fin ajax
                },
                Cancelar: function () { $(this).dialog('destroy') }
            }

        });
    }
}
function notificarIncidente() {
    var nro = [];
    var datos;
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Debe seleccionar UN Ticket");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(5)').text();
        var qstr = {};
        var str = "";
        qstr.idInc = id_inc;
        $.ajax({
            url: "/Service/buscarDatosIncidente",
            data: qstr,
            async: false,
            type: 'POST',
            success: function (data) {
                datos = data;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    str = "No se encuentran resoluciones";
                }
                data.idIncidente = id_inc;
                data.nroIncidente = nroInc;
            }
        });
        $('#NotifIncidentes').data('datos', datos).dialog('open');
    }
}
/****************
Funciones de chat
****************/
window.MsjArray = {}
function getChatHeight(ID)
    {
        var alto = $('#Chat-'+ID+' ul').prop("scrollHeight") + 50;
        
        return alto;
    }
function AbrirChat(id,nombre,avatar,OnOffLine,page)
	{
        // Verifica si llega el numero de pagina
        if (!page) // Si no existe
        {
            var page = 1; // Es pagina 1
        }

        // Objeto para cargar mensajes
        var ObjloadMsjsChat = {}; // Objeto para enviar datos a la funcion de cargar msjs
        ObjloadMsjsChat.id = id; // Guarda el id de chat
        ObjloadMsjsChat.nombre = nombre; // Nombre de contacto
        ObjloadMsjsChat.avatar = avatar; // Avatar
        ObjloadMsjsChat.OnOffLine = OnOffLine; // Estado
        ObjloadMsjsChat.page = page; // Guarda la pagina

        // Ventana de chat
        var Busqueda = $('#Chat-'+id);

        $('.chat-front:not(#Chat-'+id+')').switchClass( "chat-front", "chat-back", 0 );

        if (Busqueda.length) // Verifica si existe el chat
            {
                var cBody = $('.chat-body', Busqueda); // Busca el body del chat

                var RcUL = loadMsjsChat(ObjloadMsjsChat); // Carga los mensajes

                cBody.html(RcUL); // Muestra los mensajes en el body

                Busqueda.switchClass( "chat-back", "chat-front", 0 ); // Muestra el chat
                if (page == 1) animateChat(id); // Si pagina es igual a 1 anima el scroll
                Busqueda.find('input:text').focus(); // Foco
            }
        else
            {
                // Contenedor
                var DivID = document.createElement('div');
                DivID.setAttribute('id', 'Chat-'+id);
                DivID.setAttribute('class', 'chat-front'); // Clase para mostrar al frente

                // Nombre de chat
                var cHeader = document.createElement('div');
                cHeader.className = 'ui-widget-header';
                cHeader.textContent = nombre;

                var cCloser = document.createElement('i');
                cCloser.className = 'CloseWindow fa fa-times';
                cCloser.setAttribute('aria-hidden', 'true');

                // Body
                var cBody = document.createElement('div');
                cBody.className = 'chat-body ui-widget-content';
                cBody.setAttribute('style', 'border: 0;')

                // Mensajes
                var RcUL = loadMsjsChat(ObjloadMsjsChat);

                cBody.appendChild(RcUL); // Adjunta la UL

                // Formulario
                var msjForm = document.createElement('div');
                msjForm.setAttribute('class', 'enter-message');

                var msjTextfield = document.createElement('div');
                msjTextfield.setAttribute('class', 'textfield');
                
                var msjInput = document.createElement('input');
                msjInput.setAttribute('type', 'text');
                msjInput.setAttribute('placeholder', 'Ingresar mensaje');
                msjInput.setAttribute('maxlength', '1000');

                var msjButton = document.createElement('button');
                msjButton.setAttribute('class', 'button');
                msjButton.innerHTML = '&#9658;';

                msjTextfield.appendChild(msjInput);
                msjForm.appendChild(msjTextfield);
                msjForm.appendChild(msjButton);

                // Adjuntando secciones del chat al contenedor
                cHeader.appendChild(cCloser); // Closer
                DivID.appendChild(cHeader); // Header
                DivID.appendChild(cBody); // Body
                DivID.appendChild(msjForm); // Form

                $('.chat-box').append(DivID); // Adjunta el chat

                $('#Chat-'+id+' ul').animate({ scrollTop: 15000 }, 150); // Anima el chat para mostrar el ultimo mensaje recibido
                $('#Chat-'+id+' input:text').focus(); // Foco al cuadro de texto
            }
        
        // Actualizar estado de los mensajes
        var ChatSelect = '#Chat-'+id; // Guardo el id de chat
        // Evento personalizado para buscar mensajes no leidos
        $.event.trigger({ // Lanza el evento personalizado
            type: "updateMsjStatusEv", // Declaracion del evento personalizado
            message: ChatSelect // Parametro enviado al evento personalizado
        })
    }

function loadMsjsChat(ObjDatos)
    {
        // Mensajes
        var cUL = document.createElement('ul');
        $.ajax({
            url: '/Service/UltimosMjes',
            data: { 'idMio': UsuarioLogeado, 'idOtro': ObjDatos.id, 'page': ObjDatos.page},
            type: 'POST',
            async: false,
            success: function(response) {
                // Boton para cargar mas mensajes
                if (response.length > 0 && response[0].HayMas === true)
                    {
                        var cLI = document.createElement('li'); // Crea el boton de mas msjs
                        cLI.className = 'more';
                        cLI.dataset['id'] = ObjDatos.id;
                        cLI.dataset['nombre'] = ObjDatos.nombre;
                        cLI.dataset['avatar'] = ObjDatos.avatar;
                        cLI.dataset['onoffline'] = ObjDatos.OnOffLine;
                        cLI.dataset['page'] = ObjDatos.page + 1;

                        $('#'+ObjDatos.id, '#Chat-Lista').data('page', ObjDatos.page); // Asigna la pagina actual al contacto para recordar que pagina debe cargar
                
                        var cMessage = document.createElement('div');
                        cMessage.className = 'message';
                
                        var cMText = document.createTextNode('Cargar más mensajes');
                
                        // Armando el body del chat
                        cMessage.appendChild(cMText); // Adjunta el texto
                        cLI.appendChild(cMessage); // Adjunta el mensaje
                        cUL.appendChild(cLI); // Adjunta el LI
                    }
                for(i=0;i<response.length;i++)
                    {
                        var cLI = document.createElement('li');
                        cLI.setAttribute('data-msjid', response[i].Id); // ID del mensaje
                        cLI.setAttribute('data-estado', response[i].Leido); // Estado del mensaje
        
                        if (response[i].Id_From == UsuarioLogeado)
                            {
                                cLI.className = 'out';
                            }
                        else
                            {
                                cLI.className = 'in';
        
                                var cAvatar = document.createElement('avatar');
                                cAvatar.className = 'avatar';
                                if (ObjDatos.avatar != '/Content/img/Photo/')
                                    {
                                        cAvatar.setAttribute('style', 'background-image:url("'+ObjDatos.avatar+'"');
                                    }
        
                                cLI.appendChild(cAvatar); // Adjunta el Avatar
                            }
        
                        var cMessage = document.createElement('div');
                        cMessage.className = 'message';
        
                        var cMText = document.createTextNode(response[i].Mensaje);
        
                        var gettingHora = response[i].Hora;
                        gettingHora = gettingHora.split(':');
                        var gettingFecha = response[i].Fecha;
                        var cHora = document.createElement('span');
                        cHora.className = 'time';
                        cHora.textContent = gettingFecha+'-'+ gettingHora[0] + ':' + gettingHora[1];
        
                        // Armando el body del chat
                        cMessage.appendChild(cMText); // Adjunta el texto
                        cMessage.appendChild(cHora); // Adjunta la hora
        
                        cLI.appendChild(cMessage); // Adjunta el mensaje
        
                        cUL.appendChild(cLI); // Adjunta el LI
                        
                    }
                // Notificacion de usuario offline
                if (ObjDatos.OnOffLine == false)
                    {
                        var cLI = document.createElement('li');
                        cLI.className = 'alert';
                
                        var cMessage = document.createElement('div');
                        cMessage.className = 'message';
                
                        var cMText = document.createTextNode('El usuario esta desconectado. Recibira una notificación al conectarse.');
                
                        // Armando el body del chat
                        cMessage.appendChild(cMText); // Adjunta el texto
                        cLI.appendChild(cMessage); // Adjunta el mensaje
                        cUL.appendChild(cLI); // Adjunta el LI
                    }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            }
        });
        // Devuelve div
        return cUL;
    }

function updateMsjStatus(e) // Actualiza estado de los mensajes
    {
        console.log('Chat abierto: ', e.message); // Logea el id del Chat
        GettIDContacto = e.message.split('-');
        $(e.message+' li').each(function(){ // Recorre los li
            var _self = $(this); // Guarda el elementro iterado en una variable
            if(_self.hasClass('in')) // Si el elemento iterrado tiene la clase .in
                {
                    console.log('Mensaje Entrante', $(this).attr('data-estado')); // Logea el estado del mensaje
                    if (_self.attr('data-estado') == 'false') // Si el mensaje no esta leido
                        {
                            var _self_ID = _self.attr('data-msjid'); // Obtiene le ID del mensaje
                            _self.attr('data-estado', true); // Actualiza el estado localmente
                            console.log('Mensaje leido: '+_self_ID); // Logea el ID del mensaje no leido
                            $.ajax({ // Envia el ID del mensaje para actualizar su estado
                                url: '/Service/setmensajeleido',
                                data: { 'Id': _self_ID}, // Objeto
                                type: 'POST',
                                success: function(response) {
                                    console.log(response.Info); // Respuesta de la consutla

                                    // Buscar, eliminar, restar badges
                                    // $('#Chat-Lista #'+id+' .badge-contact').each(function(){
                                        UpdateBadges('subtr'); // Resta una notificacion
                                        $('#Chat-Lista #'+GettIDContacto[1]).find('.badge-contact').remove(); // Borra el badge del contacto
                                    // })
                                }
                            });
                        }
                }
        });
    }

function MostrarChat() // Devuelve true si hay un chat abierto y false si no hay
    {
        var find = $('.chat-front'); // Busca un chat abierto
        if (find.length) // Valida la busqueda
            {
                var result = true; // Si hay chat abierto: true
            }
        else
            {
                var result = false; // Si no hay chat abierto: false
            }
        return result; // Devuelve el resultado
    }

function animateChat(IDchat)
    {
        $('#Chat-' + IDchat + ' ul').animate({ scrollTop: getChatHeight(IDchat) }, 275); // Anima el chat para mostrar el ultimo mensaje recibido
    }

function UpdateBadges(oper)
    {
        var cantidad = $('.badge-numbers').attr('data-cant'); // Obtiene la cantidad de notificaciones actuales
        cantidad = parseInt(cantidad); // Convierte el valor del atributo data en INT
        if (oper == 'sum') // Valida la operacion a ejecutar
            {
                cantidad += 1; // Suma una notificacion
            }
        else
            {
                cantidad -= 1; // Resta una notificacion
            }

        if (cantidad < 1) // Si no hay notificaciones
            {
                CleanBadges(cantidad); // Ejecuta la funcion de limpiar notificaciones y envia la cantidad actual
            }

        $('.badge-numbers').attr('data-cant', cantidad); // Actualiza la cantidad de notificaciones en el artributo DATA

        if (cantidad > 0) // Si hay notificaciones
            {
                $('.chat-badge').show(200); // Muestra el mensaje de notificaciones
            }
    }

function CleanBadges(cantidadBadge) // Limpia las notificaciones
    {
        if (cantidadBadge == 0) // Si no hay notificaciones
            {
                $('.chat-badge').hide(200); // Oculta el mensaje de notificaciones
            }
    }

function ContactBadge(ContactID) // Agrega el badge de mensaje nuevo al contacto
    {
        // Badge para contacto
        var Notif = document.createElement('badge'); // Crea el badge
        Notif.className = 'badge-contact'; // Asigna la clase badge

        var Busqueda =  $('#'+ContactID+' .avatar .badge-contact', '#Chat-Lista'); // Busca algun badge

        if (Busqueda.length < 1) // Si no hay badge
            {
                $('#'+ContactID+' .avatar', '#Chat-Lista').prepend(Notif); // Agrega el badge
            }

        // Badge para grupo
        var NotifG = document.createElement('badge'); // Crea el badge
        NotifG.className = 'badge-contact'; // Asigna la clase badge
        NotifG.textContent = '!'; // Agrega el texto

        $('#'+ContactID).parent().prev().prepend(NotifG); // Agrega el badge al Titulo del grupo
    }

function CerrarChat(ChatID)
    {
        ChatID.switchClass( "chat-front", "chat-back", 0 );
    }
// NOTIFICACIONES
function chat_html_notif(htmlNotif)
{
    if (Notification)
    {
        if (Notification.permission !== 'granted')
        {
            Notification.requestPermission();
        }
        var title = 'SOFIT - Nuevo mensaje de ' + htmlNotif.cNombre;
        var extra = {
            icon: 'https://sofit.cba.gov.ar/favicon.png',
            body: htmlNotif.msj
        };
        var noti = new Notification( title, extra)
        noti.onclick = {
            // Al hacer click
        }
        noti.onclose = {
            // Al cerrar
        }
        setTimeout( function() { noti.close() }, 10000)
    }

    // Notificacion por PAGE TITLE
    var original = document.title; // Guarda titutlo original
    var timeout; // Estable el tiempo de espera entre cada cambio

    window.coders = function (newMsg, howManyTimes) { // Ejecuta el cambio de titulo
        function step() { // Cada titulo
            document.title = (document.title == original) ? newMsg : original;

            if (--howManyTimes > 0) {
                timeout = setTimeout(step, 800);
            };
        };

        howManyTimes = parseInt(howManyTimes);

        if (isNaN(howManyTimes)) {
            howManyTimes = 50;
        };

        cancelcoders(timeout);
        step();
    };

    window.cancelcoders = function () {
        clearTimeout(timeout);
        document.title = original;
    };

    coders('Nuevo mensaje');
}