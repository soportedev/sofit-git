﻿///<reference path="utils.js"/>
var v;
var articulo;
var filterCount=0;
$(function () {
    //readOnly = $('#readOnly').val();
    //if (readOnly) { disableHandlers($('#panel')); }
    v = new Ventana($('#grillaArticulos'));    
    $("#Armarios").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        title: "Armarios",
        width: 500,
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '105px');
            $(thisdialog).parent().css('z-index', '101');
        },
        buttons: {
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $("#Rubros").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,              
        title: "Rubros",
        width: 500,
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '105px');
            $(thisdialog).parent().css('z-index', '101');
            $('#verRubrosP', this).attr('checked', false);
            $('li.add',this).remove();            
        },
        buttons: {
            Cancelar: function () {
                $(this).dialog('close');                
            }
        },
        close: function () {
            $('.mje', this).html('').hide();
        }
    });
    $('#verRubrosP').on('click', verRubrosP);
    $('button,.boton').button();
    $('#artNvo').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        width: 850,
        title: "Nuevo Artículo",
        open: function () {            
            var thisdialog = $(this);
            var titulo=$(thisdialog).data('title');
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '105px');
            $(thisdialog).parent().css('z-index', '101');
            $(thisdialog).parent().find("span.ui-dialog-title").text(titulo);
        },        
        close: function () {
            $(this).find('input[type="checkbox"]').removeAttr('checked');
            $(this).find('input[type="text"]').removeAttr('checked');            
        }
    });
    $('#bNvoArticulo').on('click', function () {
        restoreUI();
        var li = $('#artNvo').find('li:not(.nuevo)');
        li.hide();
        $("#artNvo").data('title', 'Nuevo Artículo').dialog('open');
    });
    $('#bCancelarNvoArt').on('click', function () {
        $('#artNvo').dialog('close')
    });
    $('body').on('click', '.help', function () { $(this).val('').removeClass('help') });
    $('#bNvoArmario').on('click', nuevoArmario);
    $('#bNvoRubro').on('click', nuevoRubro);
    $('#bAceptarNvoArt').on('click', agregarArticulo);
    $('#nvoArmario, #bArmarios').on('click', function () { $("#Armarios").dialog('open'); });
    $('#nvoRubro, #bRubros').on('click', function () { $("#Rubros").dialog('open') });
    $("#artNvo").on('change','#uploadPhoto', function () {
        var _this = this;
        if (this.disabled) return alert('El Navegador no soporta carga de imágenes!');
        var file = this.files[0];
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function (_file) {
            image.src = _file.target.result;
            image.onload = function () {
                var w = this.width,
                    h = this.height,
                    t = file.type,
                    n = file.name,
                    s = ~~(file.size / 1024) + 'KB';

                if (file.size / 1024 > 200) {
                    $('#spanPhoto').css({ 'background-image': 'none' }).html('').append('<img src="/Content/img/Photo/imageHere.jpg"'
                        + '" class="photo"> ' + '<p>El tamaño máximo de la imágen es de 200KB. El actual es: ' + s + '</p><br>');
                    var fileInput = document.createElement('input');
                    fileInput.setAttribute('id','uploadPhoto');
                    fileInput.setAttribute('type', 'file');
                    _this.parentNode.replaceChild(fileInput, _this);
                } else {
                    $('#spanPhoto').css({ 'background-image': 'none' }).html('').append('<img src="' + this.src
                        + '" class="photo"> ' + w + 'x' + h + ' ' + s + ' ' + t + ' ' + n + '<br>');
                }
            };
            image.onerror = function () {
                alert('Tipo de archivo no admitido: ' + file.type);
            };
        };
    });    
    $('#dJurisdiccion').on('change', function () {
        $.post('/Service/Dependencias', { 'idJur': $(this).val() }, function (e) {
            var r = e;
            var selectDep = $('#dDependencia');
            selectDep.html('');
            $.each(r, function (i, v) {
                var option = $('<option value=' + v.idDependencia + '>').html(v.Nombre);
                selectDep.append(option);
            })
        });
    }).trigger('change');
    //$('#bCancNvoArmario').on('click', function () { });
    $('body').on('change', '.cSubrubro', mostrarSubRubro);
    $('body').on('change', '.dRubrosPadre,#dRubro', mostrarSubrubroBySel);
    $('.inputCStatus').on('focus', function () {
        $(this).removeClass('denied');
    });
    $('#Armarios').on('click', '.drop', dropArmario);
    $('#Rubros').on('click', '.drop', dropRubro);
    $('#grillaArticulos').on('click', '.drop', eliminarArticulo);
    $('#grillaArticulos').on('click', '.reset', ajustarStock);
    $('#grillaArticulos').on('click', '.remove', retirarArticulo);
    $('#grillaArticulos').on('click', '.addArt', agregarStock);
    $('#grillaArticulos').on('click', '.moves', verMovimientos);
    $('#grillaArticulos').on({
        mouseenter: function () {
            v.selectedArticulo = v.dataTable.fnGetData(this);
            if (v.selectedArticulo != null) {
                $('#artDisplay').attr("src", v.selectedArticulo.pathImagen);
            }
        },
        mouseleave: function () {
            $('#artDisplay').attr('src', '/Content/img/Photo/imageHere.jpg');
        },
    }, 'table tbody tr', function () { });
    $("#tNroOblea").click(function (event, ui) { $('#bAceptarNvoArt').button('disable') }).autocomplete({
        minLength: "2",
        source: (
        function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Service/EquipoXOblea",
                data: term,
                dataType: "json",
                success: function (data) {
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.idEquipo;
                        obj.label = val.Oblea;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            });
        }),
        focus: function (e, ui) { $(this).val(ui.item.label); return false; },
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        select: function (e, ui) {
            $(e.target).val(ui.item.label);            
            $('#bAceptarNvoArt').button("enable").focus();
            return false;
        }
    });
    $('#cEquipo').on('change', function () {
        var checked = $(this).attr('checked');
        var nroOblea = $('#tNroOblea').val('');
        if (checked == 'checked') {
            nroOblea.removeAttr('disabled').removeClass('disabled');
            var otro = $('#cTecnico');
            otro.removeAttr('checked');
            otro.trigger('change');
        }
        else {
            nroOblea.attr('disabled', 'disabled').addClass('disabled');
            $('#bAceptarNvoArt').button("enable");
        }
    });
    $('#cTecnico').on('change', function () {
        var checked = $(this).attr('checked');
        var select=$('#dTecnicos')
        if (checked == 'checked') {
            select.removeAttr('disabled');
            var otro = $('#cEquipo');
            otro.removeAttr('checked');
            otro.trigger('change');
        }
        else {
            select.attr('disabled', 'disabled');
            $('#bAceptarNvoArt').button("enable");
        }
    });
    $('#sFiltro').on('change', function () {
        $('option[value="0"]', this).remove();
        $('#filterPane').find('.filterItem').remove();
        AddFiltros();        
    });
    $('#dRubro').on('change', function () {
        var option = $('option', this).filter(':selected');
        var text = $.trim(option.html());
        if (text == 'Equipamiento Informática') {
            $('#tNombreArticulo').autocomplete("enable");
            $('#bAceptarNvoArt').button("disable");
        }
        else {
            $('#tNombreArticulo').autocomplete({disabled:true});
            $('#bAceptarNvoArt').button("enable");
        }
    });
    $('#tNombreArticulo').autocomplete({
            minLength: "2",
            source: (
            function (request, response) {
                var term = '{ prefixText: "' + request.term + '" }';
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "../Models/handlers/autocomplete.asmx/GetEquipoPorOblea",
                    data: term,
                    dataType: "json",
                    success: function (data) {
                        var c = eval(data.d);
                        var suggestions = [];
                        $.each(c, function (i, val) {
                            var obj = {};
                            obj.value = val;
                            obj.label = val;
                            suggestions.push(obj);
                        })
                        response(suggestions);

                    }
                });
            }),
            open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
            select: function (e, ui) {
                $('#bAceptarNvoArt').button("enable").focus();
            }
        });    
});                         //READY
function autoComplete(textInput,buttonToDisable) {
    $(textInput).click(function (event, ui) { $(buttonToDisable).button('disable') }).autocomplete({
        minLength: "2",
        source: (
        function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Models/handlers/autocomplete.asmx/GetEquipoPorOblea",
                data: term,
                dataType: "json",
                success: function (data) {
                    var c = eval(data.d);
                    var suggestions = [];
                    $.each(c, function (i, val) {
                        var obj = {};
                        obj.value = val;
                        obj.label = val;
                        suggestions.push(obj);
                    })
                    response(suggestions);

                }
            });
        }),
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        select: function (e, ui) {
            $(buttonToDisable).button("enable").focus();
        }
    });
}
function clearInputText() {
    $('input[type=text]', '#artNvo').val('');
}
function AddFiltros() {
    var filterArea = $('#sFiltro option:selected').text();
    var filterItem = $('<div>')
            .addClass('filterItem')
            .data('suffix', '.' + (filterCount++));    
    switch (filterArea) {
        case "Por Armario": $('div.template.armarioChooser').children().clone().appendTo(filterItem).trigger('adjustName');
            break;
        case "Por Rubro": $('div.template.rubroChooser').children().clone().appendTo(filterItem).trigger('adjustName');
            break;
        default:;
            break;
    }
    filterItem.appendTo('#filterPane');
}
function adjustName(){
    var suffix = $(this).closest('.filterItem').data('suffix');
    if (/(\w)+\.(\d)+$/.test($(this).attr('name'))) return;
    $(this).attr('name', $(this).attr('name') + suffix);
}
function Ventana($ventana) {
    var _this = this;
    this.$tablaContainer = $ventana;
    this.$tabla = this.$tablaContainer.find('table');
    this.selectedArticulo = articulo;    
    this.id = this.$tablaContainer[0].id;
    this.width = this.$tablaContainer.width();
    this.height = this.$tablaContainer.height();
    this.updateTable = function () {
        this.dataTable.api().ajax.reload(function (json) {
            if (json.Info == "Login") { mostrarFinSesion(); }
        });
    }
    this.dataTable = this.$tabla.on('xhr.dt', function (e, settings, json, xhr) {
        var es = e;
        var set = settings;
        var js=json;
        var xh = xhr;
    }).dataTable({
        'ajax': {
            'url': 'Deposito/getTable',
            'dataSrc': function (json) {                
                if (json.Info == "Login") { mostrarFinSesion(); return; }
                return json.aaData;
            }                
        },
        'bJQueryUI': true,        
        columns: [
         { 'data': 'id' },
         { 'data': 'codigo' },
         { 'data': 'nombreRubro' },
         { 'data': 'nombre' },
         { 'data': 'descripcion' },
         { 'data': 'stock' },
         { 'data': 'nombreUbicacion' },         
         { 'data': 'idDependencia' },
         { 'data': 'idUbicacion' },
         { 'data': 'pathImagen' }
        ],
        "columnDefs": [
               {
                   "targets": [0],
                   "visible": false
               },
                {
                    "targets": [7],
                    "visible": false
                },
                {
                    "targets": [8],
                    "visible": false
                },
                {
                    "targets": [9],
                    "visible": false
                },
                {
                    "targets": [10],
                    "data": null,
                    "defaultContent": "<div class='icon reset'title='Ajuste manual del stock y otros'></div><div class='icon remove'title='Retiro de artículos'></div>" +
                        "<div class='icon addArt'title='Recarga de artículos'></div><div class='icon drop'title='Eliminar el artículo'></div>"+
                    "<div class='icon moves'title='Ver Movimientos'></div>"
                }
        ]
    });    
    this.toUi = function () {
        alert("toUI")
    }    
    this.getArticuloSeleccionado = function () {
        return _this.selectedArticulo;
    }
}
function eliminarArticulo() {

    var articulo = v.getArticuloSeleccionado();
    $.ajax({
        url: 'Deposito/dropArticulo',
        type: 'POST',
        dataType: "json",
        data: { 'id': articulo.id },
        async: false,
        success: function (com) {
            result = com;
            if (result.Info == 'ok') v.updateTable();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error eliminando articulo " + thrownError.toString());
        }
    });
}
function ajustarStock() {
    restoreUI();
    var selectedArt = v.getArticuloSeleccionado();    
    var lis = $('li', '#artNvo');
    lis.not('.ajustar').hide();
    $('#artNvo').data('title', 'Ajustar Stock').dialog('open');
    $('#tNombreArticulo').val(selectedArt.nombre).prop('disabled', true);
    $('#tDescArticulo').val(selectedArt.descripcion);
    $('#dArmario').val(selectedArt.idUbicacion);
    $('#tCantidad').val(selectedArt.stock);
    $('#tIdArticulo').val(selectedArt.codigo);
    $('#spanPhoto').css({ 'background-image': 'none' }).html('').append('<img src="' + selectedArt.pathImagen +
        '" class="photo"><br>');
    $('#bAceptarNvoArt').off('click').on('click', function () {
        var upload = $("#uploadPhoto").get(0);
        var image = upload.files[0];
        var cantidad = $('#tCantidad').val();
        var descripcion = $('#tDescArticulo').val();
        var idArmario = $('#dArmario').val();
        var observacion = $('#tObservaciones').val();
        var fd = new FormData();
        fd.append("id", selectedArt.id);
        fd.append("file", image);
        fd.append("cantidad", cantidad);
        fd.append("idArmario", idArmario);
        fd.append("descripcion", descripcion);
        fd.append("observacion", observacion);
        sendData(fd,'Deposito/ajustarStock');        
    });   
}
function retirarArticulo() {
    restoreUI();
    var selectedArt = v.getArticuloSeleccionado();
    var lis = $('li', '#artNvo');
    lis.not('.restar').hide();
    $('#artNvo').data('title', 'Retirar Artículo').dialog('open');
    $('#tNombreArticulo').val(selectedArt.nombre).prop('disabled', true);
    $('#tDescArticulo').val(selectedArt.descripcion).prop('disabled', true);
    $('#dArmario').val(selectedArt.idUbicacion);
    $('#tCantidad').val(selectedArt.stock).prop('disabled', true);
    $('#uploadPhoto').prop('disabled', true);
    $('#tIdArticulo').val(selectedArt.codigo);
    $('#spanPhoto').css({ 'background-image': 'none' }).html('').append('<img src="' + selectedArt.pathImagen +
        '" class="photo"><br>');
    
    $('#bAceptarNvoArt').off('click').on('click', function () {
        var ints = new RegExp(/^\d{1}/);
        
        var oblea = 'na';
        var cantidad = $('#tCantidad').val();
        var cantRetiro = $('#cRetiro');
        var cantidadRetiro=cantRetiro.val().trim();
        if (!ints.test(cantidadRetiro)) {
            $(cantRetiro).addClass('denied');
            return;
        }               
        var intsCant = parseInt(cantidad);
        var intscantretiro = parseInt(cantidadRetiro)
        if (intscantretiro > intsCant) {
            mostrarError("La cantidad de retiro no puede ser mayor a la existente");
            return;
        }
        var checkEquipo = $('#cEquipo').attr('checked');
        var checkTecnico = $('#cTecnico').attr('checked');
        var oblea = $('#tNroOblea').val();
        var tecnico = $('#dTecnicos').val();
        var fd = new FormData();
        if (checkEquipo == 'checked') fd.append("equipo", oblea);
        if (checkTecnico == 'checked') fd.append("tecnico", tecnico);
        
        fd.append('observacion', $('#tObservaciones').val());
        fd.append("id", selectedArt.id);
        fd.append("cantidad", cantidadRetiro);
        sendData(fd, 'Deposito/retirarArticulo');
        
    });    
}
function verMovimientos() {
    restoreUI();
    var selectedArt = v.getArticuloSeleccionado();
    $.post('Deposito/Movimientos', {'idArt':selectedArt.id}, function (data) {
        if (data.Info == "Login") { mostrarFinSesion(); return;}
        var div = $('<div>');
        var result = "<'table'><thead><tr><th>Fecha</th><th>Tipo Mov.</th><th>Cantidad</th><th>Usuario</th><th>Observaciones</th></tr></thead>" +
                    "<tbody>";
        $.each(data, function (i, v) {
            result += "<tr><td>" + v.FechaStr + "</td><td>" + v.Tipo + "</td><td>" + v.Cantidad + "</td><td>" + v.Usuario + "</td><td>" + v.Observaciones + "</td></tr>";

        });
        result += "</tbody></table>";
        $(div).append(result);
        $(div).dialog({
            closeOnEscape: true,
            modal: true,
            resizable: false,
            draggable: false,
            //position: ['center', 109],
            title: "Movimientos de "+selectedArt.nombre,
            width: '600px',
            open: function () {
                var thisdialog = $(this);
                $(thisdialog).parent().css('position', 'fixed');
                $(thisdialog).parent().css('top', '109px');
                $(thisdialog).parent().css('z-index', '99');
                $(thisdialog).parent().css('max-height', '400px');
                $(thisdialog).parent().css('overflow', 'auto');
                var hscr = $('html').height();
                $('.ui-widget-overlay').css('height', hscr);
                $('.ui-widget-overlay').css('z-index', '98');
                
            }          
        });
    });
}
function agregarStock() {
    restoreUI();
    var selectedArt = v.getArticuloSeleccionado();
    var lis = $('li', '#artNvo');
    lis.not('.sumar').hide();
    $('#artNvo').data('title', 'Agregar al Stock').dialog('open');
    $('#tNombreArticulo').val(selectedArt.nombre).prop('disabled', true);
    $('#tDescArticulo').val(selectedArt.descripcion).prop('disabled', true);
    $('#dArmario').val(selectedArt.idUbicacion);
    $('#tCantidad').val(selectedArt.stock).prop('disabled', true);
    $('#uploadPhoto').prop('disabled', true);
    $('#tIdArticulo').val(selectedArt.codigo);
    $('#spanPhoto').css({ 'background-image': 'none' }).html('').append('<img src="' + selectedArt.pathImagen +
        '" class="photo"><br>');

    $('#bAceptarNvoArt').off('click').on('click', function () {
        var cantidad = $('#cAgregar').val();
        var fd = new FormData();
        fd.append("id", selectedArt.id);        
        fd.append("cantidad", cantidad);
        fd.append("observaciones",$('#tObservaciones').val());
        sendData(fd, 'Deposito/addStock');
    });       
}
function agregarArticulo(e) {
    e.preventDefault();
    var strings = new RegExp(/[A-Za-z]{2}/);
    var ints = new RegExp(/^\d{1}/);
    var $stCheck = $('.stringToCheck');
    var $intToCheck = $('.intToCheck');
    var flag = true;
    $.each($stCheck, function () {
        if (!strings.test($(this).val().trim())) {
            $(this).addClass('denied');
            flag = false;
        }
    });
    $.each($intToCheck, function () {
        if (!ints.test($(this).val().trim())) {
            $(this).addClass('denied');
            flag = false;
        }
    });
    if (flag) {
        var upload = $("#uploadPhoto").get(0);
        var image = upload.files[0];
        var idRubro = $('select', 'li.last').val();
        var nombre = $('#tNombreArticulo').val();
        var descripcion = $('#tDescArticulo').val();
        var depend = $('#dDependencia').val();
        var idArmario = $('#dArmario').val();
        var cantidad = $('#tCantidad').val();
        var fd = new FormData();
        fd.append("idRubro", idRubro);
        fd.append("nombre", nombre);
        fd.append("desc", descripcion);
        fd.append("idDependencia", depend);
        fd.append("idArmario", idArmario);
        fd.append("file", image);
        fd.append("cantidad", cantidad);
        sendData(fd, 'Deposito/nuevoArticulo');
    }
}
function sendData(fData, url) {    
    jQuery.ajax({
        url: url,
        data: fData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
            if (data.Info == "Login") { mostrarFinSesion(); return;}
            if (data.Info == "ok") {                
                if (data.Detail) {
                    $('#tIdArticulo').val(data.Detail);
                }
                v.updateTable();
                mostrarExito("Operación completada!");
                $('p.ajaxGif').hide();
                $('#artNvo').dialog('close');
                restoreUI();
            }
            else mostrarError(data.Detail);
        },
        error: function (i, v, r) {
            mostrarError("Hubo un error en el servidor!!");
        }
    });
}
function restoreUI() {
    var context = $('#artNvo');
    $('.mje', context).hide();
    $('li.add', context).remove();
    $('input[type=text]', context).val('').removeClass('denied');
    var lis = $('li', context);
    lis.show();
    $('#uploadPhoto', context).val('').prop('disabled', false);
    $('#tNombreArticulo',context).val('').prop('disabled', false);
    $('#tDescArticulo',context).val('').prop('disabled', false);
    $('#tCantidad', context).val('').prop('disabled', false);
    $('#tIdArticulo',context).val('');
    $('#spanPhoto',context).html('').removeAttr('style');
    $('#bAceptarNvoArt', context).button('enable').off('click').on('click', agregarArticulo);    
    
}
/****************
*RUBROS
*****************/
function verRubrosP() {
    var checkbox = $(this);
    var checked = checkbox.attr('checked');
    var $liActual = checkbox.parents('li');
    var div = checkbox.parents('div.section');
    if (checked == 'checked') {       
        var template = $('<li>').append(div.find('.template1').clone().removeClass('hide template1')).addClass('add last');
        template.insertAfter($liActual);
    }else {
        div.find('.add').remove();
    }
}
function mostrarSubrubroBySel() {
    var checkbox = $(this).siblings('input[type="checkbox"]');
    var checked = checkbox.attr('checked');
    $('.mje').html('').hide();
    if (checked == 'checked') {
        var div = $(this).parents('div.section');
        var idPadre = $(this).val();
        var $liActual = $(this).parents('li');
        var $liToRemove = $($liActual).nextAll('.add');
        $liToRemove.remove();
        $liActual.addClass('last');
        var result = getRubrosHijos(idPadre);
        if (result.length == 0) {

        } else {
            template = $('<li>').append(div.find('.template').clone().removeClass('hide template')).addClass('add last');
            $selectSubRubro = template.find('select');
            $.each(result, function (i, v) {
                $selectSubRubro.append($('<option></option>').attr('value', v.idRubro).text(v.Nombre))
            })
            $liActual.removeClass('last');
            template.insertAfter($liActual);
        }
    }
}
function mostrarSubRubro() {
    var checkbox = $(this);
    var checked = checkbox.attr('checked');
    var div = checkbox.parents('div.section');
    //var esPadre = checkbox.hasClass('padre');
    var $liActual = checkbox.parents('li');
    var $liAdded = div.find('li.add');
    var $nextLi = $liActual.next('li.add');
    var $prevLi = $liActual.prev('li');        
    var idPadre = checkbox.siblings('select').val();
    var $mjeError = div.find('.mje.error');
    $mjeError.html('').hide();
    var template;
    var $selectSubRubro;
    var result;    
    if (checked == 'checked') {        
        result = getRubrosHijos(idPadre);
        if (result.length == 0) {
            $mjeError.html('Este Rubro no tiene subrubros.').show();
            checkbox.removeAttr('checked');
        } else {
            template = $('<li>').append(div.find('.template').clone().removeClass('hide template')).addClass('add last');
            $selectSubRubro = template.find('select');
            $.each(result, function (i, v) {
                $selectSubRubro.append($('<option></option>').attr('value', v.idRubro).text(v.Nombre))
            })           
            $liActual.removeClass('last');
            template.insertAfter($liActual);
        }        
    }
    else {
        $liToRemove = $($liActual).nextAll('.add');
        $liToRemove.remove();
        $liActual.addClass('last');
    }
}
function getRubrosPadre() {
    var result;
    $.ajax({
        url: 'Deposito/RubrosPadre',
        type: 'POST',
        dataType: "json",
        async: false,
        success: function (com) {
            result = com;
        },        
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error obteniendo Rubros Padre: ",thrownError.toString());
        }
    });
    return result;
}
function getRubrosHijos(id) {
    var result;
    $.ajax({
        url: 'Deposito/getHijos',
        type: 'POST',
        dataType: "json",
        data: { 'id': id },
        async: false,
        success: function (com) {
            result = com;
        },        
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Error obteniendo Rubros Hijos: " + thrownError.toString());
        }
    });
    return result;
}
function dropRubro() {
    var $tr = $(this).parents('tr');
    var $id = $tr.find('span.id').html();
    var $mjeInfo = $(this).parents('div.section').find('.mje.info');
    var $mjeError = $(this).parents('div.section').find('.mje.error');   
    $('.mje').html('').hide();
    $.ajax({
        url: 'Deposito/dropRubro',
        type: 'POST',
        dataType: "json",
        data: { 'idRubro': $id },        
        success: function (com) {            
            if (com.Info == 'ok') {                
                updateRubros();
                $mjeInfo.html("Rubro Quitado.").show();
            } else {
                $mjeError.html(com.Detail).show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError.toString());
        },
        complete: function () {
            //closeModal();
        }
    });
}

function nuevoRubro() {
    var $nomRubro = $('#tNombreNvoRubro');
    var $descRubro = $('#tDescNvoRubro');
    var tienePadre = $('#verRubrosP').attr('checked');
    var context = $(this).parents('.section');
    var padre = null;
    if (tienePadre=='checked') {
        var last = $('.last', context);
        padre = $('select', last).val();
    }
    var $mjeInfo = $(this).parents('div.section').find('.mje.info');
    var $mjeError = $(this).parents('div.section').find('.mje.error');
    //var $rubroPadre = $('#dRubroPadre');
    if (regularExpression(/[A-Za-z]{2}/, $nomRubro, 'Ingrese el Nombre')){
        $.ajax({
            url: 'Deposito/nvoRubro',
            type: 'POST',
            dataType: "json",
            data: { 'nombre': $nomRubro.val(),'desc':$descRubro.val(),'idPadre': padre},            
            success: function (com) {
                $('.mje').html('').hide();
                //var com = $.parseJSON(data)||JSON.parse(data);
                if (com.Info == 'ok') {
                    $('.mje').html('').hide();                    
                    $mjeInfo.html("Rubro Agregado!").show();
                    updateRubros();
                } else {
                    $mjeError.html(com.Detail).show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError.toString());
            },
            complete: function () {
                
            }
        });
    }   
}
function updateRubros() {
    $.ajax({
        url: 'Deposito/Index',
        type: 'GET',
        success: function (data) {
            var $tabla = $(data).find('#rubrosExistente').find('table');
            var $options = $(data).find('#dRubro').find('option');

            $('#rubrosExistente').html($tabla);
            $('#dRubro,.dRubrosPadre').html($options);
        }
    });
}
/*****************
*ARMARIOS
******************/
function nuevoArmario() {    
    var $nomArmario = $('#tNombreArmario');
    var $descArmario = $('#tDescArmario');
    var $mjeInfo = $(this).parents('div.section').find('.mje.info');
    var $mjeError = $(this).parents('div.section').find('.mje.error');
    $('.mje').html('').hide();
    if (regularExpression(/[A-Za-z]{3}/, $nomArmario, 'Ingrese el Nombre')&
        regularExpression(/[A-Za-z]{3}/, $descArmario, 'Ingrese la Descripcion')) {        
        $.ajax({
            url: 'Deposito/nvoArmario',
            type: 'POST',
            dataType: "json",
            data: { 'info': $nomArmario.val(), 'desc': $descArmario.val() },
            //beforeSend: bgModal($('#artNvo')),
            success: function (com) {
                $('.mje').html('').hide();
                //var com = $.parseJSON(data)||JSON.parse(data);
                if (com.Info == 'ok') {
                    $('.mje').html('').hide();
                    updateArmarios();
                    $mjeInfo.html("Armario Agregado.").show();
                }else{
                    $mjeError.html(com.Detail).show();                   
                }                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError.toString());
            },
            complete: function () {
                //closeModal();
            }
        });
    }
    else mostrarError("Complete los Datos!!");
}
function dropArmario() {
    var $tr = $(this).parents('tr');
    var $id = $tr.find('td.id').html();
    var $mjeInfo = $(this).parents('div.section').find('.mje.info');
    var $mjeError = $(this).parents('div.section').find('.mje.error');
    $('.mje').html('').hide();
    $.ajax({
        url: 'Deposito/dropArmario',
        type: 'POST',
        dataType: "json",
        data: { 'id': $id },
        //beforeSend: bgModal($('#artNvo')),
        success: function (com) {
            //var com = $.parseJSON(data)||JSON.parse(data);
            if (com.Info == 'ok') {                
                updateArmarios();
                $mjeInfo.html("Armario Quitado.").show();
            } else {
                $mjeError.html(com.Detail).show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError.toString());
        },
        complete: function () {
            //closeModal();
        }
    });
}
function updateArmarios() {
    $.ajax({
        url: 'Deposito/Index',
        type: 'GET',
        success: function (data) {
            var $tabla = $(data).find('#armarioContent').find('table');
            var $select = $(data).find('#dArmario');
            $('#armarioContent').html($tabla);
            $('#dRubro').html($select);
        }
    });
}
