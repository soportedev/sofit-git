﻿$(function () {
    // Fondo aleatorio
    var num = 'bg' + (Math.floor(Math.random() * 5) + 1);

    switch (num) {
        case "bg1":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-1.jpg)");
            break;
        case "bg2":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-2.jpg)");
            break;
        case "bg3":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-3.jpg)");
            break;
        case "bg4":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-4.jpg)");
            break;
        case "bg5":
            $('.body-blur').css("background-image", "url(/Content/Login/images/bg-5.jpg)");
            break;
    }
    //-->
    $('div.message button').on('click', function (evt) {
        var thisButton = this;
        var isOpen = $(this).hasClass('open');
        if (isOpen) {
            $({ deg: 45 }).animate({ deg: 0 }, {
                duration: 300,
                step: function (now) {
                    $(thisButton).css({
                        transform: 'rotate(' + now + 'deg)',
                        'border-radius': 100 - ((now + 5) * 2) + '% 100% 100%',
                        'background-color':'#1d2e51'
                    });
                },
                complete: function () {
                    $(thisButton).removeClass('open');
                    $('.mailTo').dialog('close');
                }
            });
        }
        else {
            $({ deg: 0 }).animate({ deg: 45 }, {
                duration: 300,
                step: function (now) {
                    $(thisButton).css({
                        transform: 'rotate(' + now + 'deg)',
                        'border-radius': 100 - (now + 5) * 2 + '% 100% 100%',
                        'background-color':'gray'
                    });
                },
                complete: function () {
                    $(thisButton).addClass('open');
                    $('.mailTo').dialog('open');
                    var position = $('.mailTo').dialog('option', 'position');
                    $('.mailTo').dialog('option', 'position', { my: 'right bottom', at: 'right top', of: thisButton })
                }
            });
        }
    });
    $('.mailTo').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,        
        open: function () {            
            $(this).siblings('.ui-dialog-titlebar').hide();
            $('#NombreUsuario').val('');
            $('#eMailUsuario').val('');
            $('#consultaTa').val('');  
            $('#NombreUsuario').css({ 'border-color': '' });
            $('#eMailUsuario').css({ 'border-color': '' });
            $('#consultaTa').css({ 'border-color': '' }); 
        },    
        width:'auto',
        buttons: {
            Aceptar: {
                id: '',
                text: 'Enviar',
                click: function () {
                    var nam = document.getElementById('NombreUsuario');
                    var ema = document.getElementById('eMailUsuario');
                    var cons = document.getElementById('consultaTa');
                    var nombre = $('#NombreUsuario').val();
                    var email = $('#eMailUsuario').val();
                    var consulta = $('#consultaTa').val();                    
                    $('#NombreUsuario').css({ 'border-color': '' });                    
                    $('#eMailUsuario').css({ 'border-color': '' });                    
                    $('#consultaTa').css({ 'border-color': '' });                    
                    if (nam.validity.valid & ema.validity.valid & consulta.length > 10) {
                        $.ajax({
                            url: '/Service/Contacto',
                            data: { 'nombre': nombre, 'email': email, 'body': consulta },
                            type: 'POST',
                            datatype: 'json',
                            success: function (data) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarExito("Mail Enviado!!. Será respondido a la brevedad");
                                    $('div.message button').trigger('click');
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            }
                        });
                    } else {
                        if (!nam.validity.valid) {
                            $('#NombreUsuario').css({ 'border-color': 'red' });
                        }
                        if (!ema.validity.valid) {
                            $('#eMailUsuario').css({ 'border-color': 'red' });
                        }
                        if (consulta.length < 10) {
                            $('#consultaTa').css({ 'border-color': 'red' });
                        }
                        mostrarError("Complete los campos obligatorios");
                    }
                }
            }
        }
    });    
    /*****************
    *Busqueda Inc Activos
    *******************/
    $('#searchIncidente').on('click', function () {
        getActivos($('#buscarActivos').val());
    });
    $("#buscarActivos").focus(function (srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("defaultTextActive");
            $(this).val($(this)[0].title);
        }
    }).keyup(function (e) {
        $(this).val(this.value.toUpperCase());
        if (e.keyCode == 13) {
            $('#searchIncidente').trigger('click');
        }
    }).trigger('blur');

    $('body').on('click', '.buttonVer', verResolucion);
    /*************
    *GET ACTIVOS
    **************/
    function getActivos(nroInc) {
        var resString = nroInc.toUpperCase();
        if (!regularIncidente.test(resString)) {
            mostrarError("El texto íngresado no parece válido");
            return;
        }
        $.ajax({
            url: "/Service/UbicacionIncidente",
            data: { 'nroInc': resString },
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (data) {
                var json = data;
                var content = $("#incidenteContent");
                if (json == null) {
                    mostrarError("No se Encuentra el Ticket");
                    content.hide();
                    $("#areasIncidente tbody").hide();
                    return;
                }
                content.show();
                content.find("label[for='lNroInc']").html(json.Numero);
                content.find("label[for='lTipificacion']").html(json.Tipificacion);
                content.find("label[for='lFecha']").html(json.FechaInicioString);
                content.find("label[for='lCliente']").html(json.Cliente);
                content.find("label[for='lJurisdiccion']").html(json.Jurisdiccion);
                content.find("label[for='lDependencia']").html(json.Dependencia);
                content.find("label[for='lTipo']").html(json.TipoTicket);
                content.find("label[for='lPrioridad']").html(json.PrioridadTicket);
                content.find("label[for='lFechaLimite']").html(json.FechaStrPrevista);
                content.find("label[for='lServicio']").html(json.Servicio);
                content.find("label[for='lDescripcion']").html(json.Descripcion);
                content.find("label[for='lEstado']").html(json.EstadoTicket);
                toHtmlEquiposeIncidentes(json);
                content.css('display', 'inline');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                NProgress.done();
            }
        });                          //fin ajax

    }
});
function toHtmlEquiposeIncidentes(json) {
    var resolucionesIncidente;
    var resolucionesEquipo;
    var equipos = "";
    var areasXIncidente = "";
    $.each(json.Areas, function (i, item) {
        resolucionesIncidente = "<table class='mdl-data-table hide infos'><thead><tr><th  class='mdl-data-table__cell--non-numeric'>Fecha</th><th>Técnico</th><th>T.Resolución</th><th>Equipo</th><th>Trabajo</th></tr></thead><tbody>";
        if (item.Resoluciones.length > 0) {
            $.each(item.Resoluciones, function (i, item2) {
                resolucionesIncidente += '<tr><td>' + item2.FechaStr + '</td>' + '<td>' + item2.Tecnico + '</td>' +
                    '<td>' + item2.TipoResolucion + '</td>' + '<td>' + item2.Equipo + '</td>' + '<td>' + item2.Observaciones + '</td></tr>';
            });
        }
        resolucionesIncidente += "</tbody></table>";
        var counter = $('tr', resolucionesIncidente).length;
        areasXIncidente += '<tr><td>' + item.Direccion + '</td><td>' + item.FechaDesdeShStr + '</td><td>' + item.FechaHastaShStr + '</td>'
            + '<td class="tObs ' + (counter == 1 ? "buttonNa" : "buttonVer") + '">' + resolucionesIncidente + '</td></tr>';
    });
    $("#areasIncidente thead").show();
    $("#areasIncidente tbody").html(areasXIncidente).show();
}
function verResolucion(event) {
    //parametros principales
    var html = $(this).find('table').clone().removeClass('hide');
    var dialog = $('<div class="infos">').append(html).dialog({
        show: 'slide',
        dialogClass: 'a',
        position: { my: "right center", at: "left center", of: event.target },
        width: 'auto'
    }).click(function () { $(this).dialog('destroy'); }).parent().removeClass('ui-widget-content');
    $(".ui-dialog-titlebar", $(dialog).parent()).hide();
}