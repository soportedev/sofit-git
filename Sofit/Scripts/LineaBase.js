﻿// Lista de lineas base
window.fakeobj = [];
$(function () {
    $('body').on('keypress', function (e) {
        if (e.keyCode == 8&e.target==this) {
            e.preventDefault();
        }
    })
    
    $('#tabset').tabs({
        activate: function (event, ui) {
            var tab = ui.newPanel;
            if (tab[0].id == 'inventario') {
                // Mostrar lista de tipos
                ECTotales();
            }
        }
    });
    $('.ui-corner-all').each(function(){
         $(this).removeClass('ui-corner-all');
    })
    $('.ui-corner-top').each(function(){
         $(this).removeClass('ui-corner-top');
    });

    // Mostrar creacion de linea base
    $('#lineasBase').on('click', '#nuevaLB', function () {
        $('.buscador').slideDown(250);
        $('#LBNombre').val('').removeAttr('readonly');
        $('#creador, #fechalineabase').hide();
        $('#fechalineabase').html('Fecha: ');
        $('.timeline ol').html('');
    });
    $('section.timeline').on('click', 'li div', function (e) {
        var id = $(this).parent('li').attr('id');
        $.ajax({
            url: '/Service/BaseLine',
            data: { 'id': id },
            type: 'post',
            dataType: 'json',
            success: function (data) {               
                var ficha = $('#fichaEc');                
                $('#FechaBl',ficha).val(data[0].FechaStr)
                $('#ObservacionesEl', ficha).val(data[0].Data).css('height','100px');
                ficha.fadeIn(300, function () {
                    var heiFicha = ficha.height();
                    var heiTextA = $('#ObservacionesEl').height();
                    var text = document.getElementById('ObservacionesEl');
                    
                    var adjustedHeight = text.scrollHeight;
                    text.style.height = adjustedHeight + "px";
                    
                }).position({ my: "left", at: "right", of: e });
                
            }
        })
    });
    $('.closeFicha').on('click', function () { $('#fichaEc').fadeOut() })
    /****************
    **agregado
    ***************/    
    //Arbol dinamic
    $('#lineasBase').on({
        click: function (event) {
            event.preventDefault();
            if (this == event.target|this==$(event.target).parent()[0]) {
                $(this).children('ul').toggle();
                var icon = $(this).children('div').find('i');
                icon.removeClass('fa-folder fa-folder-open');
                if ($(this).children('ul').length == 0) icon.addClass('fa-folder');
                else { ($(this).children('ul').is(':hidden')) ? icon.addClass('fa-folder') : icon.addClass('fa-folder-open') }
            }
            return false;
        }
    }, 'ul.topnav>li').css('cursor', 'pointer');
    $('#lineasBase').on('click', 'a.itemDescripcion', function (e) {
        $('a.itemDescripcion', '#menu').removeClass('ui-state-highlight');
        $(this).addClass('ui-state-highlight');
        var nombre = $(this).siblings('span.hide').html();
        var id = $(this).siblings('span.hide').attr('data-id');
        mostrarBl(nombre,id);
    });
    //-->
    
    // Guardar / Cancelar
    $('.aceptar').click(function () {
        var LineaBaseNombre = $('#LBNombre').val()

        if (LineaBaseNombre != '') {
            $('#LBNombre').removeClass('inputerror');

            var NuevaLB = {};
            NuevaLB.Nombre = LineaBaseNombre;
            NuevaLB.Elementos = {}
            var elementos = $('.timeline ol li');
            $.each(elementos, function (i, v) {
                NuevaLB.Elementos[i] = $(this).attr('id');
            });
            var dum = NuevaLB;
            $.ajax({
                //data: JSON.stringify(NuevaLB),
                data:NuevaLB,
                url: '/LineaBase/CrearNueva',
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if(data.Info=="Login")           
                    {
                        mostrarFinSesion();
                        return;
                    }
                    if(data.Info=="ok")
                    {
                        mostrarExito("Se agregó la nueva línea base");
                    }
                }
            })
        }
        else {
            $('#LBNombre').addClass('inputerror');
        }
    });

    $('.cancelar').click(function () {
        $('#LBNombre').val('');
        $('#fechalineabase').html('Fecha: ');
        $('.timeline ol').html('');
    })

    // Busqueda
    $('#TipoElementoConf').click(function () {
        var TipoElementoConf = $(this)
        // Ajax
        $.ajax({
            url: '/Service/TiposEquipo',
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Hide Loading
                NProgress.done();

                // Values
                var raw = response;
                var source = [];
                var mapping = {};
                for (var i = 0; i < raw.length; ++i) {
                    source.push(raw[i].Nombre);
                    mapping[raw[i].Nombre] = raw[i].id;
                }
                TipoElementoConf.autocomplete({
                    source: source,
                    minLength: 0,
                    select: function (event, ui) {
                        var dataId = mapping[ui.item.value]
                        $(this).attr('data-id', dataId);
                    },
                    close: function () {
                        // Finish selection and make a blur
                    }
                }).val('').autocomplete('search');
            },
            error: function (response) {
                // Hide Loading
                NProgress.done();
            }
        })
        // Ajax
    })
    $('#ElementoRelacion').autocomplete({
        source: (function (request, response) {
            var tipoElem = $('#TipoElementoConf').attr('data-id');
            var term = { prefixText: request.term, idTipo: tipoElem };
            $.ajax({
                data: term,
                url: '/Service/EquipoXObleaYTipo',
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.idEquipo;
                        obj.label = val.Oblea;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            })
        }),
        minLength: 2,
        focus:function(){
            return false;
        },
        select: function (event, ui) {
            $(this).val(ui.item.label);
            $.ajax({
                url: '/LineaBase/CheckLineaBase',
                data:{'idEq': ui.item.value},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var LiDiv = document.createElement('div');
                    var LiTime = document.createElement('time');
                    var addLi = document.createElement('li');
                    var spanT = document.createElement("span");
                    var br = document.createElement("br");
                    addLi.setAttribute('class', 'elemContent');
                    if(data.Info=="ok")
                    {
                        $(this).val(ui.item.label);
                        var dataId = ui.item.value;
                        // setAttribs                        
                        addLi.setAttribute('id', dataId);                        
                        addLi.setAttribute('draggable', true);
                        addLi.setAttribute('ondragstart', 'drag(event)');
                        addLi.setAttribute('ondrop', 'return false');
                        addLi.setAttribute('ondragover', 'return false');
                        var TextLabel = document.createTextNode(ui.item.label);
                        var textFecha = document.createTextNode("LB creada el "+data.Detail);
                        spanT.appendChild(TextLabel);
                        spanT.appendChild(br);
                        spanT.appendChild(textFecha);
                        LiTime.appendChild(spanT);                        
                        LiDiv.appendChild(LiTime);
                        addLi.appendChild(LiDiv);
                        //-->                       
                    }
                    else {
                        // setAttribs                        
                        addLi.setAttribute('id', dataId);
                        addLi.className += ' error';
                        addLi.setAttribute('draggable', false);
                        addLi.setAttribute('ondrop', 'return false');
                        addLi.setAttribute('ondragover', 'return false');
                        var TextLabel = document.createTextNode(ui.item.label);
                        var textFecha = document.createTextNode(data.Detail);
                        spanT.appendChild(TextLabel);
                        spanT.appendChild(br);
                        spanT.appendChild(textFecha);
                        LiTime.appendChild(spanT);
                        LiDiv.appendChild(LiTime);
                        addLi.appendChild(LiDiv);
                    }
                    // Agregando el nuevo elemento
                    $('#trash').html(addLi);
                    //-->
                }
            })//ajax
            return false;
        }
    });
    getArbol();
});
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
function drop(ev, el) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    //var thistar = ev.target.innerText;
    //var elm = String(thistar.lastChild);
    //ev.target.insertBefore(document.getElementById(data), ev.target.lastChild);
    el.appendChild(document.getElementById(data));

    $('.timeline').trigger('change');
}
//Arbol getting
function getArbol() {
    $.ajax({
        url: '/LineaBase/GetLineaBase',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            fakeobj = response;            
            var ul = $('#lineasBase ul');
            ul.html('');
            var liNueva=$('<li>').attr('id','nuevaLB');
            var aNueva=$('<a>').html('Nueva Línea Base');
            liNueva.append(aNueva);
            ul.append(liNueva);
            $.each(fakeobj, function (i, v) {
                var divi1 = $('<div class="icon">');
                var icon = $('<i>').addClass('fa');
                divi1.append(icon);
                var b = $('<a>').html(v.Nombre);
                var li = $('<li>').append(divi1).append(b);
                ul.append(li);
                $.each(v.Datos, function (j, m) {
                    var a = $('<a>').addClass('itemDescripcion');
                    var divi = $('<div class="icon">');
                    var i = $('<i>').addClass('fa fa-calendar');
                    divi.append(i);
                    var inputH = $('<span>').addClass('hide');
                    inputH.html(m.Nombre);
                    inputH.attr('data-id', m.id);
                    a.html(m.FechaStr);
                    var li2 = $('<li>').append(divi).append(a).append(inputH);
                    var newUl = $('<ul>');
                    newUl.append(li2);
                    li.append(newUl);
                })
            });
            $('#lineasBase>.lineasbase>ul>li').trigger('click');
        }
    });
}
function mostrarBl(nombre,id) {
    // Oculta creacion de linea base
    $('.buscador').slideUp(250);
    var bl;
    $.each(window.fakeobj, function (i, v) {
        if (v.Nombre == nombre) {
            $.each(v.Datos, function (j, m) {
                if (m.id == id) bl = m;
            });
        }            
    });
    // Borra linea anterior
    $('.timeline ol').html('');
    // Mostrar linea base
    $('#LBNombre').val(bl.Nombre);    
    $('#LBNombre').attr('readonly', 'readonly');

    for (i = 0; i < bl.Elementos.length; i++) {
        // Creando el nuevo elemeto
        var addLi = document.createElement('li');
        addLi.setAttribute('id', bl.Elementos[i].id);
        addLi.setAttribute('class', 'elemContent');

        var LiDiv = document.createElement('div');
        var LiTime = document.createElement('time');
        var TextNode = document.createTextNode(bl.Elementos[i].Nombre)

        LiTime.appendChild(TextNode);
        LiDiv.appendChild(LiTime);
        addLi.appendChild(LiDiv);
        //-->
        // Mostrando la linea base
        $('#fechalineabase').html('Fecha: ' + bl.FechaStr).show();
        $('#creador').html('Creada por: ' + bl.CreadoPor).show();
        $('.timeline ol').append(addLi);
    }
    $('.timeline').trigger('change')

}