﻿window.filterCount = 0;
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(function () {   
    $('button').button();
    $('#nvaOblea').on('click', agregarOblea);
    $('#rangoO').on('click', agregarRango);
    $('.error').hide();
    $('body').on('adjustName', '.sticker', function () {
        var suffix = $(this).data('suffix');
        var oblea = $(this).data('nro');
        $(this).find('.texto').html(oblea);
        $(this).find('.code').html('*'+oblea+'*');
        if (/(\w)+\.(\d)+$/.test($(this).attr('id'))) return;
        $(this).attr('id', $(this).attr('id') + suffix);
    });
    $('.texto').html(getParameterByName('id'));
    $('.code').html('*'+getParameterByName('id')+'*');
    $('#divNvaOblea').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        show: 'slide',
        close: function () { $('#nroOblea').val(''); $('.error').hide() },
        title: "Ingrese el Número de Oblea",
        buttons: {
            Aceptar: function () {                
                var nroOblea = $.trim($('#nroOblea').val());
                $('.error').hide();
                if (regularInfra.test(nroOblea) | regularEquipo.test(nroOblea) | regEquipoTele.test(nroOblea)) {
                    $(this).dialog('close');
                    agregarSticker(nroOblea);
                    window.filterCount++;
                }
                else {                    
                    $('.error').show('fast').find('.message').html('El texto ingresado no parece una oblea válida.');
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $('#divRango').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        show: 'slide',
        close: function () { 
            $('#nroDesde , #nroHasta').val(''); 
            $('.error').hide() 
        },
        title: "Ingrese el Rango",
        buttons: {
            Aceptar: function () {
                var desde = $('#nroDesde').val();
                var hasta = $('#nroHasta').val();
                var regD = /\d{5}/;
                if (!regularEquipo.test(desde)) {
                    $('.error').show('fast').find('.message').html('Ingrese la oblea "Desde".');
                    return;
                }
                if (!regularEquipo.test(hasta))
                {
                    $('.error').show('fast').find('.message').html('Ingrese la oblea "Hasta".');
                    return;
                }
                $('.error').hide();
                var xtDesde=regD.exec(desde);
                var xtHasta=regD.exec(hasta);
                var nroDesde = xtDesde[0];
                var nroHasta = xtHasta[0];                
                if (nroDesde != null && nroHasta != null) {
                    if (nroHasta < nroDesde) {
                        $('.error').show('fast').find('.message').html('El nro desde es mayor que el hasta');
                        return;
                    }
                    if (nroHasta - nroDesde > 50) {
                        $('.error').show('fast').find('.message').html('La máxima diferencia es de 50. Corrija los márgenes!!!');
                        return;
                    }
                    for (var i = nroDesde; i <= nroHasta; i++) {
                        var newoblea;
                        var prefix = desde.substring(0, xtDesde.index);
                        
                        if (desde.length > prefix.length + nroDesde.length) {
                            var sufix = desde.substring(prefix.length + 5, desde.length);
                            newoblea = prefix + pad(i, 5) + sufix;
                        }
                        else newoblea = prefix + pad(i, 5);
                        agregarSticker(newoblea);
                        window.filterCount++;
                    }                    
                    $(this).dialog('close');
                }                
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $('body').on('click','.button-close', function (e) {
        e.preventDefault();
        if (checkQuantity()) {
            if ($(this).parents('div.sticker').attr('id') != 'sticker') {
                $(this).parents('div.sticker').remove();
            }
        }        
    });
	
	// Transform text
	$('#nroOblea, #nroDesde, #nroHasta').keyup(function(){
		$(this).val(this.value.toUpperCase())
	})
});
function agregarSticker(nroOblea)
{
    var nvoDiv = $('#sticker').clone().data({ 'suffix': '.' + (window.filterCount), 'nro': nroOblea });
    $('.button-close', nvoDiv).removeAttr('disabled');
    nvoDiv.appendTo('#stickerContainer').trigger('adjustName');
}
function agregarRango(e) {
    e.preventDefault();
    var dir = $('i.direccion').html();
    if (dir == 'Soporte Técnico') {
        $('#divRango').dialog('open');
    }
}
function agregarOblea(e) {
    e.preventDefault();
    $('#divNvaOblea').dialog('open');
}
function checkQuantity() {
    if (window.filterCount < 1) return false;
    else {
        window.filterCount--;
        return true;
    }
}
function pad(num, size) { // Agrega los 0 necesarios para obleas menores a 10000
    var s = "0000" + num;
    return s.substr(s.length-size);
}
