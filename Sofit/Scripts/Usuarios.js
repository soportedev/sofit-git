﻿function bindUsuarios(event, ui) {
    $('button').button();
    $('#nvoUsuarioDialog').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: 'center',
        width: 450,
        title: 'Nuevo Usuario',
        buttons: {
            Aceptar: function () {
                var login = $('#tNvoLogin');
                login.removeClass('denied');
                var nombreCompleto = $('#tNombreCompleto');
                nombreCompleto.removeClass('denied');
                var password1 = $('#tPassword1');
                password1.removeClass('denied');
                var password2 = $('#tPassword2');
                password2.removeClass('denied');
                var este = $(this);
                var perfil = $('#dPerfil1').val();
                var passwordV1 = $.trim(password1.val());
                var passwordV2 = $.trim(password2.val());
                var flag = true;
                if (!regString.test(login.val().trim())) {
                    $(login).addClass('denied');
                    flag = false;
                }
                if (!regString.test(nombreCompleto.val().trim())) {
                    $(nombreCompleto).addClass('denied');
                    flag = false;
                }
                if (!compareStrings(passwordV1, passwordV2)) {
                    mostrarError("Las contraseñas no son iguales");
                    password2.addClass('denied');
                    password1.addClass('denied');
                    return;
                }
                if (passwordV1.length < 4) {
                    mostrarError("Las contraseñas son muy cortas.");
                    password2.addClass('denied');
                    password1.addClass('denied');
                    return;
                }
                if (flag) {
                    var log = login.val();
                    var name = nombreCompleto.val();
                    $.ajax({
                        data: { 'login': log, 'nombre': name, 'p1': passwordV1, 'perfil': perfil },
                        url: 'Config/NuevoUsuario',
                        type: 'post',
                        datatype: 'json',
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                este.dialog('close').find('input').val('');
                                mostrarExito('Se ha agregado el usuario');
                                reloadCurrentTab();
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (error, er, thrown) {
                            mostrarError(thrown);
                        },
                        complete: function () {
                            $(".ajaxGif").css({ 'display': 'none' });
                        }
                    });
                }
                else mostrarError("Revise los errores.");
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $("#ProfileChanger").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        width: 450,
        open: function () {
            var datos = $(this).data('perfiles');
            var id = $(this).data('idUsuario');
            var objeto = JSON.parse(datos);
            $('tbody', '#tPerfiles').find('tr').remove();
            $.each(objeto, function (i, v) {
                var td = $('<td>').append(v.nombre);
                var tdQuitar = $('<td>').append('<div class="icon trash profile">');
                var tb = $('#tPerfiles').find('tbody');
                var tr = $('<tr>').append(td).append(tdQuitar).appendTo(tb);
            })
        }
                            ,
        buttons: [{
            text: 'Cerrar',
            click: function () {
                $(this).dialog('close');
            }
        }]
    });//dialog profile   
    $("#PasswordChanger").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        //position: "center",
        width: 450,
        close: function () {
            $('#passwordNuevo').val('');
            $('#passwordNuevo2').val('');
        },
        buttons: {
            Aceptar: function () {
                var datos = $(this).data('datos');
                var pass = $('#passwordNuevo').val();
                var pass2 = $('#passwordNuevo2').val();
                var flag = false;
                if (pass.length < 4) {
                    mostrarError("Las contraseñas son muy cortas.");
                    return;
                }
                if (!compareStrings(pass, pass2)) {
                    mostrarError("Las contraseñas no son iguales");
                    return;
                }
                else {
                    $.ajax({
                        url: "/Config/setPassword",
                        data: { 'userid': datos.nro, 'pass': pass },
                        type: 'POST',
                        beforeSend: function () {
                        },
                        success: function (data) {

                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            else {
                                if (data.Info == "ok") {
                                    $("#PasswordChanger").dialog('close');
                                    mostrarExito('Se ha actualizado el password');
                                }
                                else {
                                    $("#PasswordChanger").dialog('close');
                                    mostrarError(data.Detail);
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        },
                        complete: function () {
                            $(".ajaxGif").css({ 'display': 'none' });
                        }
                    }); //fin ajax
                } //fin else
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
}
