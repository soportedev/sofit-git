﻿function bindDatosPers(event, ui) {
    $('button').button();
    $('#tabset').on('change', '#themeSwitcher', function () {
        $('#imgResult,#themeFrame').removeClass();
        $('#themeFrame').addClass('_' + $('#themeSwitcher').val());
    });
    $('#tabset').on('click', '#setFirma', function () {
        var fData = new FormData();
        var inpFile = $('#upPhRes');
        var img = inpFile.get(0).files[0];
        fData.append('photo', img);
        fData.append('email', $('input.email').val() );
        fData.append('firma', $('textarea[class="textarea"]').val());
        $.ajax({
            url: '/Config/setMyFirma',
            data: fData,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function (data) {
                switch (data.Info) {
                    case "ok": $('#imgResultFirma').addClass('ok block');
                        if (img != null) {
                            var srcImagen = $('img.artDisplay').attr('src');
                            $('img.avatar', '#header_user').attr('src', srcImagen);
                        } else {
                            $('img.avatar', '#header_user').attr('src', '#');
                        }
                        break;
                    default: $('#imgResultFirma').addClass('aviso block');
                        break;
                }
            }
        });       
    });
    $('#tabset').on('click', '#applyTheme', function () {
        $.post("/setTemas", { theme: $('#themeSwitcher').val() }, function (e, v, x) {
            var f = e;
            var g = v;
            var h = x;
            switch (f.Info) {
                case "ok": $('#imgResult').addClass('ok block');
                    location.reload();
                    break;
                default: $('#imgResult').addClass('aviso block');
                    break;
            }
        });
    });

    $('#tabset').on('click', '#cambioPassword', function () {
        if (validarNoVacio(document.getElementById("oldP"))) {
            var oldPass = $('#oldP').val();
            var newPass = $('#nvaP1').val();
            $.ajax({
                url: "/Config/setMyPassword",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ 'oldPassword': oldPass, 'newPassword': newPass }),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSession();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito("Cambio de password exitoso");
                        return;
                    }
                    else mostrarError(data.Detail);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    $('p.ajaxGif').hide();
                }
            }); //fin ajax
        }
    });
    $('#tabset').on('keyup', '#nvaP2,#nvaP1', function () {
        var datos = $(this).data('datos');
        var pass = $('#nvaP1').val();
        var pass2 = $('#nvaP2').val();
        var flag = true;
        $('#nvaP1').parent().find('span.error').remove();
        $('#pNva,#rPNva').removeClass('ok');
        $('#pNva,#rPNva').removeClass('aviso');

        if (pass.length < 8) {
            $('#nvaP1').parent().append('<span class="error">La contraseña debe tener más de siete caracteres</span>');
            flag = false;
        }
        else {
            if (pass.length != pass2.length) {
                $('#pNva,#rPNva').addClass('aviso');
                flag = false;
            }
            else {
                for (i = 0; i < pass.length && flag; i++) {
                    var l1 = pass.substring(i, (i + 1));
                    var l2 = pass2.substring(i, (i + 1))
                    if (l1 != l2) {
                        flag = false;
                    }
                }
            }
        }
        if (flag) {
            $('#pNva,#rPNva').addClass('ok');
            $('#cambioPassword').button({ disabled: false });

        }
        else {
            $('#pNva,#rPNva').addClass('aviso');
            $('#cambioPassword').button({ disabled: true });
        }
    });
    var personal = $('#idTemaPers').val();
    setSelectByVal($('#themeSwitcher'), personal);
    $('#themeSwitcher').trigger('change');
    $('#cambioPassword').button({ disabled: true });
}
