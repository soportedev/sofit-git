$(function () {
    window.tipoElemento = -1;
	// jQuery UI Estilizar
    EstilizarBotones()
    // Mostrar tabla de elementos por tipo
	$('#cmdb-body').on('click', 'nav a', function(){
		var TipoEle = $(this).attr('id');
        EClist(TipoEle);
        window.tipoElemento = TipoEle;
	})
    $('body').on('click', '#toExcel', function () {
        if (window.tipoElemento != -1) {
            window.location = '/LineaBase/getExcel?idTipo=' + window.tipoElemento;
        }
    });
    $('body').on({
        mouseenter: function (e) {
            if (this == e.currentTarget) {
                var text=$(this).find('p.hide').text();
                var info = $(infoDiv);
                $('#infoInfo').remove();                
                if (text != "null") {
                    info.css({
                        "position": "absolute",
                        "top": event.pageY - 50,
                        //"left": event.pageX - 270
                        "left":event.pageX + 30
                    }).appendTo('body');
                    $('p.mensaje', info).append(text);
                    e.stopPropagation();
                }
            }
        },
        mouseleave: function (e) {
            if (this == e.currentTarget) {
                $('#infoInfo').remove(); 
            }
        },
        click: function (e) {
            $('#infoInfo').remove();
            var tr = $(e.currentTarget);
            var oblea = tr.find('.pOblea').text();
            var textO = $('#tBuscar');
            textO.val(oblea);
            var button = $('#BuscarEquipoSubmit');
            button.trigger('click');
        }
    }, '.relacBlock');
	// Cambia de pagina
	$('#cmdb-body').on('keyup', '#inputPag', function (e) {
		var thisk = e.keyCode; // Captura la tecla
		var NPagina = $(this).val(); // Guarda pagina

		var Buscador = $('#BuscadorTabla').val(); // Captura la busqueda
		
		// Si es ENTER
		if (thisk == 13) ECxTipoListado(NPagina, Buscador); //Ejecuta la funcion
	});

	// Busca una lista
	$('#cmdb-body').on('keyup', '#BuscadorTabla', function (e) {
		var thisk = e.keyCode; // Captura la tecla
		var ValorBuscado = $(this).val(); // Guarda pagina

		// Si es ENTER
		if (thisk == 13) ECBuscadorEnLista(ValorBuscado); //ECxTipoListado(NPagina, Buscador); //Ejecuta la funcion
	});

	// Anula los links
	$('#cmdb-body').on('click', 'td a', function(e){
		e.preventDefault();
	})

	// Pestaña buscar
		// Desde la tabla
	$('#cmdb-body').on('click', '.cmdb-table', function(event){
		if(event.target.nodeName == 'A'){
			event.preventDefault();

			$('#input-buscador input:text').val(event.target.textContent); // Completa el valor de busqueda / Solo a efectos visuales
			$('#BuscarTab').trigger('click'); // Foco a la pestaña

			$('#RadioOblea').attr('checked', true); // Selecciona tipo Oblea

			BuscardorDeEC(); // Buscar
		}
	});
		// Desde la pestaña buscar
	$('#input-buscador #BuscarEquipoSubmit').click(function(){
		BuscardorDeEC(); // Buscar
	})
		// Buscar con tecla Enter
	$('#input-buscador input:text').keyup(function(e){
		var code = e.which; // recommended to use e.which, it's normalized across browsers
		if (code == 13)
			{
				BuscardorDeEC(); // Buscar
			}
	})

	// Autocomplete busqueda
	$("#input-buscador input:text").autocomplete({
		minLength: "3",
		source: searchAutocomplete,
		select: function (e, ui) {
			$('#input-buscador input:text').val(ui.item.value);      
			BuscardorDeEC();
			return false;
		},
		focus: function (e, ui) {  
			$('#input-buscador input:text').val(ui.item.value);        
			return false;
		}
	});
})
// Constructores
// Loading
function LoaderSpinner()
	{
		// Create loading
		var addSpinner = document.createElement('div'); // Div spinner
		addSpinner.setAttribute('class', 'spinner') // Clase

		for (i = 1; i <= 3; i++)
			{
				var addBounce = document.createElement('div'); // Div bounce
				addBounce.setAttribute('class', 'bounce'+i) // Clase
				addSpinner.appendChild(addBounce); // Adjunta bounce
			}

		// Muestra el loading
		$('#cmdb-body').html(addSpinner);
	}

// Lista de elementos por tipo
function ECxTipoListado(pagins, buscador)
	{
		// Crea la tabla
		var addTable = document.createElement('table');
		addTable.setAttribute('class', 'cmdb-table');

		// Crea cabecera de tabla
		var Theader = document.createElement('thead');

		// Crea tr de botones
		var addTRButt = document.createElement('tr');

		// Crea td de botones
		var addTDButt = document.createElement('td');
		addTDButt.setAttribute('colspan', '4');
        addTDButt.setAttribute('id', 'Botonera');
        addTDButt.setAttribute('class', 'ui-widget-header');
        var botonExcel = $('<button>').attr('id', 'toExcel');
        $(botonExcel).button({
            icon:'ui-icon-print'
        });
		// Boton volver
		var addButton = document.createElement('button'); // Crea boton
		addButton.setAttribute('id', '#cmdb-elements-back');
		addButton.setAttribute('onclick', 'ECTotalesConstructor();');

		var textNode = document.createTextNode('Volver'); // Texto del boton

		addButton.appendChild(textNode); // Adjunta texto al boton
		addTDButt.appendChild(addButton); // Adjunta boton al td

		if (!buscador)
			{
				var busqueda = ''; // Define busqueda vacia
			}
		else
			{
				var busqueda = buscador; // Define busqueda como el ultimo valor
				var addTotalEncontrado = document.createTextNode(' Total encontrados: '+objEC.length)
			}

		// Limita la lista 
		var tamanio_pagina = 20;
		if (!pagins)
			{
				var pagina = 1;
			}
		else
			{
				var pagina = pagins;
			}
		// Guarda en una variable la cantidad de paginas que requiere la consulta
		var total_paginas = Math.ceil(objEC.length / tamanio_pagina);
		var inicio = (pagina - 1) * tamanio_pagina;
		var limit = pagina * tamanio_pagina;

		// Si el numero de paginas es mayor a una, muestra indices
		if (total_paginas > 1)
			{
				// Boton inicial
				var textNode = document.createTextNode('Pagina: '); // Texto del boton
				
				// Adjunta el boton inicial
				addTDButt.appendChild(textNode); // Adjunta boton al td

				// Input de pagina
				var addInputPag = document.createElement('input'); // Crea input
				addInputPag.setAttribute('id', 'inputPag'); // ID
				addInputPag.setAttribute('type', 'text'); // Tipo texto
				addInputPag.setAttribute('value', pagina); // Muestra pagina acutal
				
				addTDButt.appendChild(addInputPag); // Adjunta input al td

				var textNode = document.createTextNode('/'+total_paginas); // Total de paginas

				addTDButt.appendChild(textNode);

				// Botones pagina + -
				var paginaActual = parseInt(pagina); // Convierte cadena en numero
					// Anterior
				var addRePag = document.createElement('button'); // Crea el boton
				var calculoVolver = paginaActual - 1; // Suma una pagina
				addRePag.setAttribute('onclick', 'ECxTipoListado('+calculoVolver+', "'+busqueda+'")'); // Cambia de pagina
				if (pagina == 1)
					{
						addRePag.setAttribute('disabled', 'disabled'); // Si es la pagina actual se deshabilita el boton
					}

				var textNode = document.createTextNode('Anterior'); // Texto del boton
				
				addRePag.appendChild(textNode); // Adjunta texto al boton

					// Siguiente
				var addAvPag = document.createElement('button'); // Crea el boton
				var calculoSeguir = paginaActual + 1; // Suma una pagina
				addAvPag.setAttribute('onclick', 'ECxTipoListado('+calculoSeguir+', "'+busqueda+'")'); // Cambia de pagina
				if (pagina == total_paginas)
					{
						addAvPag.setAttribute('disabled', 'disabled'); // Si es la pagina actual se deshabilita el boton
					}

				var textNode = document.createTextNode('Siguiente'); // Texto del boton
				
				addAvPag.appendChild(textNode); // Adjunta texto al boton

				addTDButt.appendChild(addRePag); // Adjunta boton al td
				addTDButt.appendChild(addAvPag); // Adjunta boton al td
			}

		// Busqueda
		var addBuscador = document.createElement('input'); // Input buscador
		addBuscador.setAttribute('id', 'BuscadorTabla'); // ID
		addBuscador.setAttribute('type', 'text'); // Tipo texto
		addBuscador.setAttribute('placeholder', 'Buscar por cualquier dato de la lista'); // Tipo texto
		addBuscador.setAttribute('value', busqueda); // Ultimo valor de busqueda
		
		addTDButt.appendChild(addBuscador); // Adjunta buscador al td
        if (buscador) addTDButt.appendChild(addTotalEncontrado); // Adjunta el total de elementos encontrados
        $(addTDButt).append(botonExcel);
		addTRButt.appendChild(addTDButt); // Adjunta td al tr
		Theader.appendChild(addTRButt); // Adjunta tr a la thead

		// Cabecera
		var addTR = document.createElement('tr'); // Fila de cabecera del head

		for ( var clave in objEC[0] )
			{
				if (objEC[0].hasOwnProperty(clave))
					{
						var addTH = document.createElement('th'); // Celda de cabecera head
						addTH.setAttribute('class', clave);

						var textNode = document.createTextNode(clave); // Cabecera

						addTH.appendChild(textNode); // Adjunta cabera en th
						addTR.appendChild(addTH); // Adjunta th en tr
					}
			}

		Theader.appendChild(addTR); // Adjunta cabecera en tbody
		addTable.appendChild(Theader); // Adjunta tbody a la tabla

		// Crea el tbody
		var Tbody = document.createElement('tbody');

		// Crea las filas
		for (var i = inicio; i < limit; i++)
			{
				// Creando el nuevo elemeto
				var addTR = document.createElement('tr'); // Fila

				// Columnas
				for ( var clave in objEC[i] )
					{
						if (objEC[i].hasOwnProperty(clave))
							{
								var TextNode = document.createTextNode('');
								var addTD = document.createElement('td'); // Celda
								addTD.setAttribute('class', clave)

								if (clave == 'Jurisdiccion' || clave == 'TipoEquipo') // Si es Juris o Tipo apunta a la clave del objeto
									{
										var TextNode = document.createTextNode(objEC[i][clave].Nombre); // Texto / Valor
									}
								else if (objEC[i][clave] != null) // Sino, verifica que no sea nullo, y sigue
									{
										var TextNode = document.createTextNode(objEC[i][clave]); // Texto / Valor
									}

								if (clave == 'Oblea') // Si la clave del objeto es Oblea
									{
										var addLink = document.createElement('a'); // Link
										addLink.setAttribute('href', objEC[i].idEquipo);

										addLink.appendChild(TextNode); // Adjunta texto en link
										addTD.appendChild(addLink); // Adjunta link en td
									}
								else
									{
										addTD.appendChild(TextNode); // Adjunta text en td
									}

								addTR.appendChild(addTD); // Adjunta td en tr
							}
					}

					Tbody.appendChild(addTR); // Adjunta tr en tbody
					addTable.appendChild(Tbody); // Adjunta tbody en tabla
				//-->
			}

		// Muestra el nav
		$('#cmdb-body').html(addTable);
		//-->

		// Estiliza los botones
		EstilizarBotones();
	}

// Buscador
function ECBuscadorEnLista(SCrit)
	{
		var NPagina = 1; // Captura la pagina

		if (SCrit) // Si se recibe el parametro de busqueda
			{
				console.log('Buscando '+SCrit) //

				var ObjTemp = []; // Objeto temporal

				// Busca las filas
				for (var i = 0; i < objECoriginal.length; i++)
					{
						// Columnas
						for ( var clave in objECoriginal[i] )
							{
								if (objECoriginal[i].hasOwnProperty(clave))
									{
										var str = '';
										var res = '';
										if (clave == 'Jurisdiccion' || clave == 'TipoEquipo') // Si es Juris o Tipo apunta a la clave del objeto
											{
												str += objECoriginal[i][clave].Nombre; // Crea el string donde buscar
												res = str.indexOf(SCrit);
												if (res >= 0)
													{
														console.log('Encontrado. Guarando nuevo objeto');
														ObjTemp.push(objECoriginal[i]); // Guarda el objeto en el nuevo array temporal
													}
											}
										else if (clave == 'Oblea' || clave == 'Observaciones') // Si es Juris o Tipo apunta a la clave del objeto
											{
												str += objECoriginal[i][clave]; // Crea el string donde buscar
												res = str.indexOf(SCrit);
												if (res >= 0)
													{
														console.log('Encontrado. Guarando nuevo objeto');
														ObjTemp.push(objECoriginal[i]); // Guarda el objeto en el nuevo array temporal
													}
											}
									}
							}
					}
				objEC = ObjTemp; // Sobreescribe objeto filtrado

				ECxTipoListado(NPagina, SCrit);
			}
		else
			{
				console.log('No hay nada para buscar') //
				objEC = objECoriginal; // Restaura objeto original
				
				ECxTipoListado(NPagina, SCrit);
			}
	}

// Lista los tipos de elementos
function ECTotalesConstructor()
	{
		// Crea el nav
		var addNav = document.createElement('nav');
		addNav.setAttribute('class', 'cmdb-elements');

		// Crea los links
		for (var i = 0; i < objTipos.length; i++)
			{
				// Creando el nuevo elemeto
				var addLink = document.createElement('a'); // Link
				addLink.setAttribute('id', objTipos[i].idTipo);

				var img = document.createElement('img'); // Icono del link
				img.setAttribute('src', objTipos[i].Imagen);

				var TextNode = document.createTextNode(objTipos[i].TipoElem+' ('+objTipos[i].Cantidad+')'); // Texto del link
				
				addLink.appendChild(img); // Adjunta icono en link
				addLink.appendChild(TextNode); // Adjunta texto en link
				addNav.appendChild(addLink); // Adjunta link en nav
				//-->
			}

		// Muestra el nav
		$('#cmdb-body').html(addNav);
		//-->
	}

// Tipos de elementos
function ECTotales()
{
	if (typeof objTipos === 'undefined' || objTipos === null) // Si no existe el objeto con los tipos de EC, es la primera vez que carga, entonces pide el objeto
	{
		console.log('Se requiere el listado');
		$.ajax({
			url: '/Service/CantidadesPorTipo',
			type: 'post',
			dataType: 'json',
			beforeSend: function () {
				// Show Loading
				NProgress.start();

				// Loader
				LoaderSpinner();
			},
			success: function (response) {
				// Hide Loading
				NProgress.done();

				window.objTipos = response; // Guarda el resultado
				ECTotalesConstructor(); // Lista los tipos de elementos
			},
			error: function (arg1, arg2, arg3) {
				// Hide Loading
				NProgress.done();

				// Muestra error
				message = '<p>Ocurrio un error al cargar los elementos. Por favor, intente de nuevo.</p><p>' + arg2 + ' - ' + arg3 + ' ' + arg1.responseText + '</p>';
				type = 'error';
				mostrarError(message, type);
			}
		});
	}
	else // Si existe el objeto, ya cargo antes, y solo muestra la lista
	{
		console.log('Ya existe el listado');
		ECTotalesConstructor();
	}
}
// Tipos de elementos
//-->
// Lista elem por tipo
function EClist(tipoEC)
{

		$('#cmdb-body').html('');
		$.ajax({
			url: '/Service/ListadoXTipo',
			type: 'post',
			data: { 'idTipo' : tipoEC },
			dataType: 'json',
			beforeSend: function () 
				{
					// Show Loading
					NProgress.start();

					// Loader
					LoaderSpinner();
				},
			success: function (objEC)
				{
					// Hide Loading
					NProgress.done();

					// Recuerda el objeto para se reenviado en cada pagina
					window.objEC = objEC;
					// Ejecuta la funcion listar
					ECxTipoListado();
			
					// Crea objeto de backup para la busqueda
					window.objECoriginal = objEC; // Backup del objeto
				},
			error: function(arg1, arg2, arg3)
				{
					// Hide Loading
					NProgress.done();

					// Muestra error
					message = '<p>Ocurrio un error al cargar los elementos. Por favor, intente de nuevo.</p><p>'+arg2+' - '+arg3+' '+arg1.responseText+'</p>';
					type = 'error';
					mostrarError(message, type);
				}
		})
	}
// Lista elem por tipo
//-->

// jQueryUI Estilo
function EstilizarBotones()
	{
		// Botonera de listado EC por Tipo
		$('#Botonera button').button().removeClass('ui-corner-all');
		//$('#Botonera input:text').button().removeClass('ui-corner-all').css({
		//	'font' : 'inherit',
		//	'color' : 'inherit',
		//	'text-align' : 'left',
		//	'outline' : 'none',
		//	'cursor' : 'text'
		//});

		// Botones del buscador de Elementos de configuracion
		$('#input-buscador button, #input-buscador .divRadio').button().removeClass('ui-corner-all');
		//$('#input-buscador input:text').button().removeClass('ui-corner-all').css({
		//	'font' : 'inherit',
		//	'color' : 'inherit',
		//	'text-align' : 'left',
		//	'outline' : 'none',
		//	'cursor' : 'text'
		//});
		$('#input-buscador .divRadio input[type=radio]').css({
			'margin' : '0',
			'vertical-align' : 'middle'
		});
	}

// Funciones de busqueda
// Buscar Equipo
function ParamBusquedaConstruc() // Identifica si es oblea o n° de serie y devuelve los paramtros de busqueda
	{
		var data = {}
		data.param = $('#input-buscador input').val(); // Obtiene el valor de busqueda

		if ($('#RadioOblea').is(':checked')) // Si es oblea
			{
				data.url = '/Service/GetEquipoInventario'; // Establece la url de busqueda
				data.uri = '/Service/EquipoXOblea'; // Establece url de Autocomplete
				data.srch = { 'Oblea' : data.param}; // Establece el parametro de busqueda
				data.srchi = { 'prefixText' : data.param }; // Establece el parametro para autcomplete
				data.autoClabel = 'oblea'; // Estable el nombre del label para autocomplete
			}
		else if ($('#RadioSerie').is(':checked')) // Si es n° serie
			{
				data.url = '/Service/GetEquipoInventarioSerie'; // Establece la url de busqueda
				data.uri = '/Service/EquipoXSerie'; // Establece url de Autocomplete
				data.srch = { 'Serie' : data.param}; // Establece el parametro de busqueda
				data.srchi = { 'prefixText' : data.param }; // Establece el parametro para autcomplete
				data.autoClabel = 'serie'; // Estable el nombre del label para autocomplete
			}
		else
			{
				// Si no esta seleccionado un tipo de parametro detiene la funcion
				message = '<p>Indique si es Oblea o N° de serie</p>';
				type = 'error';
				mostrarError(message, type);
				return;
			}
		return data;
	}

function BuscardorDeEC()
{
	$('#resultado-busqueda input, #resultado-busqueda textarea').each(function(){
		$(this).val(''); // Limpia los campos
	})

	var data = ParamBusquedaConstruc();

	// Contenido
	$.ajax({
		url: data.url,
		type: 'post',
		data: data.srch,
		dataType: 'json',
		beforeSend: function () 
			{
				// Show Loading
				NProgress.start();
				// Limpia la información adicional
				$('div#tabset #resultado-busqueda .adicionales').html('<h2>Detalles</h2>');
				$('div#tabset #resultado-busqueda .relacionesEC').html('<h2>Relaciones</h2>');
				$('div#tabset #resultado-busqueda .GestionCambioEC').html('<h2>Gestiones de Cambio</h2>');
			},
		success: function (response)
		{
			console.log(response);
					
			// Hide Loading
			NProgress.done();

			// Verifica que la ejecucion sea correcta
			if (response.Info && response.Info == 'No inicializado') // Si no se encontro o dio error
				{
					// Muestra error
					message = '<p>'+response.Detail+'</p>';
					type = 'error';
					mostrarError(message, type);
				}
			else // Si se encontro el equipo
				{
					if (response.Oblea)
						{
							$('#Oblea').val(response.Oblea);
						}
					else
						{
							$('#Oblea').val('Sin especificar')
						}

					if (response.NroSerie)
						{
							$('#NroSerie').val(response.NroSerie);
						}
					else
						{
							$('#NroSerie').val('Sin especificar')
						}

					if (response.TipoEquipo)
						{
							if (response.TipoEquipo.Nombre !== null)
								{
									$('#idTipo').val(response.TipoEquipo.Nombre);
								}
							else
								{
									$('#idTipo').val('Sin especificar')
								}
						}

					if (response.Marca)
						{
							var divMarca = document.createElement('div');
							divMarca.setAttribute('class', 'divBlock');

							var titulo = document.createElement('h5');
							var tituloT = document.createTextNode('Marca');

							if (response.Marca.Nombre !== null)
								{
									$('#Marca').val(response.Marca.Nombre);
								}
							else
								{
									$('#Marca').val('Sin especificar')
								}
						}

					if (response.Modelo)
						{
							$('#Modelo').val(response.Modelo);
						}

					if (response.Jurisdiccion)
						{
							if (response.Jurisdiccion.Nombre !== null)
								{
									$('#Jurisdiccion').val(response.Jurisdiccion.Nombre);
								}
							else
								{
									$('#Jurisdiccion').val('Sin especificar')
								}
						}

					if (response.Dependencia)
						{
							if (response.Dependencia.Nombre !== null)
								{
									$('#Ubicacion').val(response.Dependencia.Nombre);
								}
							else
								{
									$('#Ubicacion').val('Sin especificar')
								}
						}

					if (response.Estado)
						{
							if (response.Estado.Nombre !== null)
								{
									$('#Estado').val(response.Estado.Nombre);
								}
							else
								{
									$('#Estado').val('Sin especificar')
								}
						}

					if (response.Observaciones)
						{
							$('#Observaciones').val(response.Observaciones);
						}

					// Adicionales
					// Relaciones
					if (response.Relaciones)
					{
						for (i = 0; i < response.Relaciones.length; i++)
						{
							var newdiv = document.createElement('div');
                            newdiv.setAttribute('class', 'divBlock relacBlock cursor');
                            
							var titulo = document.createElement('h5');
                            var tituloT = document.createTextNode(response.Relaciones[i].TipoEquipo);
                            var pOblea = document.createElement('p');
                            pOblea.setAttribute('class', 'pOblea');
                            var textO = document.createTextNode(response.Relaciones[i].Oblea);
                            pOblea.appendChild(textO);
                            var pOvs = document.createElement('p');
                            pOvs.setAttribute('class', 'hide');
                            var textP = document.createTextNode(response.Relaciones[i].Observaciones);
                            pOvs.appendChild(textP);
							titulo.appendChild(tituloT);
                            newdiv.appendChild(titulo);
                            newdiv.appendChild(pOblea);
                            newdiv.appendChild(pOvs);
							$('div#tabset #resultado-busqueda .relacionesEC').append(newdiv); // Adjunta el contenido
						}
					}

					// Gestiones de Cambio
					if (response.GestionesCambio)
					{
						for (i = 0; i < response.GestionesCambio.length; i++)
						{
							var newdiv = document.createElement('div');
							newdiv.setAttribute('class', 'divBlock');


							var link = document.createElement('a');
                            link.setAttribute('href', '/Soporte/Cambios?nro=' + response.GestionesCambio[i].Nro);
                            link.setAttribute('target', '_blank');
                            link.setAttribute('rel', 'noopener noreferer');                            
							var titulo = document.createElement('h5');
							var tituloT = document.createTextNode('Gestion: #' + response.GestionesCambio[i].Nro);

							var textNode = document.createTextNode(response.GestionesCambio[i].FechaStr);

							titulo.appendChild(tituloT);
							newdiv.appendChild(titulo);
							newdiv.appendChild(textNode);
							link.appendChild(newdiv);

							$('div#tabset #resultado-busqueda .GestionCambioEC').append(link); // Adjunta el contenido
						}
					}

					// PC
					if (response.TipoEquipo.Nombre == 'Pc')
						{
							if (response.Os)
							{
								var newdiv = document.createElement('div');
								newdiv.setAttribute('class', 'divBlock');

								var titulo = document.createElement('h5');
								var tituloT = document.createTextNode('OS');

								var textNode = document.createTextNode(response.Os.Nombre);

								titulo.appendChild(tituloT);
								newdiv.appendChild(titulo);
								newdiv.appendChild(textNode);

								$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
							}

							if (response.Cpu)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('CPU');

									var textNode = document.createTextNode(response.Cpu.Marca.Nombre+' '+response.Cpu.Modelo+' - '+response.Cpu.Frecuencia);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.Ram)
								{
									for (i=0; i<response.Ram.length; i++)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');
						
											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('RAM');
						
											var textNode = document.createTextNode(response.Ram[i].Capacidad+response.Ram[i].UM+' '+response.Ram[i].Tipo+' '+response.Ram[i].Marca);
						
											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);
						
											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
								}

							if (response.Hd)
								{
									for (i=0; i<response.Hd.length; i++)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');
						
											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('Disco');
						
											var textNode = document.createTextNode(response.Hd[i].Capacidad+response.Hd[i].UM+' '+response.Hd[i].Interfaz+' '+response.Hd[i].Marca);
						
											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);
						
											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
								}		
						}

					// Impresoras
					if (response.TipoEquipo.Nombre == 'Impresora')
						{
							if (response.Tipo)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Tipo');

									var textNode = document.createTextNode(response.Tipo);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.Tecnologia)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Tecnologia');

									var textNode = document.createTextNode(response.Tecnologia);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.Conectividad)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Conectividad');

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);

									for (i=0; i<response.Conectividad.length; i++)
										{
											if (i == 0)
												{
													var textNode = document.createTextNode(response.Conectividad[i].Nombre);
												}
											else
												{
													var textNode = document.createTextNode(' - '+response.Conectividad[i].Nombre);
												}

											newdiv.appendChild(textNode);
										}

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
						}

					// Enlaces
					if (response.TipoEquipo.Nombre == 'Enlace')
						{
							if (response.Proveedor)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Proveedor');

									var textNode = document.createTextNode(response.Proveedor.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.TipoEnlace)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Tipo de Enlace');

									var textNode = document.createTextNode(response.TipoEnlace.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.AnchoBanda)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Ancho de banda');

									var textNode = document.createTextNode(response.AnchoBanda.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.descripcion)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Descripción');

									var textNode = document.createTextNode(response.descripcion);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
						}
								
					// Servidor Físico - Workstation Virtual - Servidor Virtual - Servidor Virtual - Balanceador - Proxy - Firewall - Gestion FW - Syslog
					if (response.TipoEquipo.Nombre == 'Servidor Físico' || response.TipoEquipo.Nombre == 'Workstation Virtual' || response.TipoEquipo.Nombre == 'Servidor Virtual' || response.TipoEquipo.Nombre == 'Balanceador de carga' || response.TipoEquipo.Nombre == 'Proxy-Cache de contenidos' || response.TipoEquipo.Nombre == 'Firewall' || response.TipoEquipo.Nombre == 'Dispositivo de Gestion FW' || response.TipoEquipo.Nombre == 'Syslog')
						{
							if (response.Os)
								{
									if (response.Os.FamiliaOs)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');

											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('Familia SO');

											var textNode = document.createTextNode(response.Os.FamiliaOs.Nombre);

											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);

											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
											
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Versión SO');

									var textNode = document.createTextNode(response.Os.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
									
							if (response.Cpu)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('CPU');

									var textNode = document.createTextNode(response.Cpu);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.CantidadCpu > 0)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Cantidad CPU');

									var textNode = document.createTextNode(response.CantidadCpu);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
									
							if (response.Memoria)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Memoria');

									var textNode = document.createTextNode(response.Memoria.Capacidad+' '+response.Memoria.Um.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
									
							if (response.Disco)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Almacenamiento');

									var textNode = document.createTextNode(response.Disco.Capacidad+' '+response.Disco.Um.Nombre);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido

									if (response.Disco.Cantidad > 0)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');

											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('Cantidad de Discos');

											var textNode = document.createTextNode(response.Disco.Cantidad);

											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);

											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
								}
						}

					// Central Telefonica - Switches - AP - Router
					if (response.TipoEquipo.Nombre == 'Central Telefónica' || response.TipoEquipo.Nombre == 'Switches' || response.TipoEquipo.Nombre == 'Access Point' || response.TipoEquipo.Nombre == 'Router')
						{
							if (response.NComun)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Nombre');

									var textNode = document.createTextNode(response.NComun);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}

							if (response.IP)
								{
									// V4
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('IPv4');

									var textNode = document.createTextNode(response.IP.IpV4);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido

									// V6
									if (response.IP.IpV6)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');

											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('IPv6');

											var textNode = document.createTextNode(response.IP.IpV6);

											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);

											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}

									// LoopBack
									if (response.IP.IpLoopback)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');

											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('IP Loopback');

											var textNode = document.createTextNode(response.IP.IpLoopback);

											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);

											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
								}

							if (response.Puertos)
								{
									var newdiv = document.createElement('div');
									newdiv.setAttribute('class', 'divBlock');

									var titulo = document.createElement('h5');
									var tituloT = document.createTextNode('Cantidad de Bocas');

									var textNode = document.createTextNode(response.Puertos);

									titulo.appendChild(tituloT);
									newdiv.appendChild(titulo);
									newdiv.appendChild(textNode);

									$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
								}
						}
					// Cluster
					if (response.TipoEquipo.Nombre == 'Cluster')
						{
							if (response.Hosts)
								{
									for (i=0; i<response.Hosts.length; i++)
										{
											var newdiv = document.createElement('div');
											newdiv.setAttribute('class', 'divBlock');

											var titulo = document.createElement('h5');
											var tituloT = document.createTextNode('Host');

											var textNode = document.createTextNode(response.Hosts[i].Nombre);

											titulo.appendChild(tituloT);
											newdiv.appendChild(titulo);
											newdiv.appendChild(textNode);

											$('div#tabset #resultado-busqueda .adicionales').append(newdiv); // Adjunta el contenido
										}
								}
						}
					// Fin else / Equipo encontrado

					// Relaciones

				}
		},
		error: function(arg1, arg2, arg3)
		{

			// Hide Loading
			NProgress.done();

			// Muestra error
			message = '<p>Ocurrio un error al cargar los elementos. Por favor, intente de nuevo.</p><p>'+arg2+' - '+arg3+' '+arg1.responseText+'</p>';
			type = 'error';
			mostrarError(message, type);
		}
	})
}
// Buscar Equipo
//-->

// Autcomplete funciton
function searchAutocomplete(request, response) {

	var data = ParamBusquedaConstruc();

	if (!data) return false; // Si data obtiene false termina la funcion / Esto es porque no esta seleccionada una opcion en Olbea o Serie

	$.ajax({
		type: "POST",
		url: data.uri,
		data: data.srchi,
		dataType: "json",
		success: function (respuesta) {
			if (respuesta.Info == 'Login')
				{
					mostrarFinSesion();
					return;
				}
			else
				{
					var suggestions = [];
					$.each(respuesta, function (i, val) {
						var obj = {};
						if (data.autoClabel == 'oblea')
							{
								obj.label = val.Oblea;
								obj.value = val.Oblea;
							}
						else if (data.autoClabel == 'serie')
							{
								obj.label = val.NroSerie;
								obj.value = val.NroSerie;
							}
						suggestions.push(obj);
					});
					response(suggestions);
				}
		}
	});
}