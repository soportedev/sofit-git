﻿var elemModJobs;
var jobsModalInstance;
var diagMes;
var instDiagMes;
var regEmailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var mailModalInstance, email;
var autoUsuario;
var autoHostOrigen;
var autoHostDestino;
var autoHostAcceso;
var hostOrigen, hostDestino, hostAcceso, usuarioSeleccionado;
var arrayUsuario = [];
$(function () {
	//init
	$('select').formSelect();
	$('.tooltipped').tooltip();
	$('.dropdown-trigger').dropdown();
	$('.jurisdiccion').on('change', function (e) {
		var idJur = $(this).val();
		var row = $(this).closest('.row');
		var selDependencias = row.find('.dependencia');
		$.post('/Service/getDependencias', { 'idJur': idJur }, function (data) {
			selDependencias.html('');
			$.each(data, function (i, v) {
				var option = $('<option>').attr('value', v.idDependencia).text(v.Nombre);
				selDependencias.append(option);
			});

			$(selDependencias).formSelect();

		});
	});
	//tab
	var tabElement = document.getElementById('tab');
	var optionsTab = {
		onShow: function (e, v) {

		}
    }
    $('#usuarioAnon').on('change', function(){
        var checked = $(this).is(':checked');
        var inputs = $(this).closest('.rowUsuario').find('input[type="text"]');
        if (checked) {
            $.each(inputs, function () {
                $(this).prop('disabled', true)
                $(this).val('')
            })
        }
        else {
            $.each(inputs, function () {
                $(this).prop('disabled', false)
            })
        }
        M.updateTextFields();
    });
	var globalTab = M.Tabs.init(tabElement, optionsTab);
	//fin tab
	//autocomp usuario
	var optionsAutoCompUsuario = {
		limit: 5,
		//data: { 'Anónimo': null },
		onAutocomplete: function (valor, ob) {
			var id = this.$el[0].id;
			var selec = valor;
			usuarioSeleccionado = valor;
			var found = arrayUsuario.find(x => x.nrocuil === selec);
			if (id === 'cuilusuarioRegla') {
				$('#nombreusuarioRegla').val(found.nombre);
				$('#emailUsuarioSeleccionadoRegla').val(found.email);
				$('#domainRegla').val(found.Dominio);
			} else {
				$('#nombreusuarioAcceso').val(found.nombre);
				$('#emailUsuarioSeleccionadoAcceso').val(found.email);
				$('#domainAcceso').val(found.Dominio);
				pedidoAcceso.element('#cuilusuarioAcceso');
			}
			$('input[type=text]').each(function (index, element) {
				if ($(element).val().length > 0) {
					$(this).siblings('label, i').addClass('active');
				}
			});
		}
	};
	autoUsuario = M.Autocomplete.init(document.querySelectorAll('.cuilUsuario'), optionsAutoCompUsuario);
	//fin usuario
	//hosts origen y destino
	var optionsHost = {
		limit: 5,
		//data: { 'Seleccione': null },
		onAutocomplete: function (valor) {
			var idA = this.$el[0].id;
			var selec = valor;
			if (idA == 'hOrigen') {
				hostOrigen = selec;
			}
			else {
				if (idA == 'hDestino') hostDestino = selec;
				else {
					hostAcceso = selec;
					pedidoAcceso.element('#nombreHostP');
				}
			}
		}
	}
	autoHostOrigen = M.Autocomplete.init(document.getElementById('hOrigen'), optionsHost);
	autoHostDestino = M.Autocomplete.init(document.getElementById('hDestino'), optionsHost);
	autoHostAcceso = M.Autocomplete.init(document.getElementById('nombreHostP'), optionsHost);
	//fin hosts
	//modal problemas enviar mail
	var optionsDialogM = {
		onOpenStart: function () {
			var nombre = $('#nombreComp');
			var cuil = $('#cuilComp');
			var desc = $('#descripcionComp');
			nombre.siblings('.errorTxt').html('');
			desc.siblings('.errorTxt').html('');
			cuil.siblings('.errorTxt').html('');
			nombre.val('');
			desc.val('');
			cuil.val('');
		}
	}
	diagMes = document.getElementById('modalQuestion');
	instDiagMes = M.Modal.init(diagMes, optionsDialogM);
	$('#enviarMensaje').on('click', function (e) {
		var nombre = $('#nombreComp');
		var cuil = $('#cuilComp');
		var desc = $('#descripcionComp');
		var alfa10 = /\w{5,}/;
		var pattCuil = /\d{11}/;
		var flag = true;
		nombre.siblings('.errorTxt').html('');
		desc.siblings('.errorTxt').html('');
		cuil.siblings('.errorTxt').html('');
		if (!alfa10.test(nombre.val())) {
			nombre.siblings('.errorTxt').html('Ingrese el nombre!!');
			flag = false;
		}
		if (!alfa10.test(desc.val())) {
			desc.siblings('.errorTxt').html('Ingrese la descripción!!');
			flag = false;
		}
		if (!pattCuil.test(cuil.val())) {
			cuil.siblings('.errorTxt').html('Ingrese el Cuil!!');
			flag = false;
		}
		if (flag) {
			$.ajax({
				url: '/Service/enviarObservacion',
				data: { 'nombre': nombre.val(), 'cuil': cuil.val(), 'observaciones': desc.val() },
				type: 'post',
				success: function (resp) {
					if (resp.Info == "ok") {
						mostrarInfo("Información Enviada!!");
					}
				},
				error: function (resp, e, t) {
					mostrarError(t);
				}
			});
		} else {
			return false;
		}
	});
	//fin modal problemas
	//modal ver trabajos
	elemModJobs = document.getElementById('modalJobs');
	jobsModalInstance = M.Modal.init(elemModJobs, {});
	$('#questSend').on('click', function () {
		instDiagMes.open();
	});
	//modal cargar observaciones
	var elemModObs = document.getElementById('modalObs');
	var options = {
		onOpenStart: function () {
			var action = this.el.attributes['data-url'].textContent;
			$(this.el).find('.modal-content h6').html('Ingrese un comentario si desea para ' + action + ' el pedido');
		}
	}
	var obsModalInstance = M.Modal.init(elemModObs, options);
	//fin modal observaciones
	//modal falta el email
	var elemModEmail = document.getElementById('modal1');
	mailModalInstance = M.Modal.init(elemModEmail, { dismissible: false });
	email = $('#emailUsuario').val();
	if (!regEmailPattern.test(email)) {
		$('div.errorTxtEmail').html('');
		$('#nuevoEmail').val('');
		mailModalInstance.open();
	}
	$('#enviarMail').on('click', function (e) {
		$('div.errorTxtEmail').html('');
		var email = $('#nuevoEmail').val();
		if (regEmailPattern.test(email)) {
			var nombre = $('#nombreUsuario').val();
			$.ajax({
				url: '/Service/updateEmail',
				data: { 'nombre': nombre, 'email': email },
				type: 'post',
				dataType: 'json',
				success: function (resp) {
					if (resp.Info == "ok") {
						$('#emailUsuario').val(resp.Detail);
						mostrarExito("Se actualizó la dirección de email");
					}
				},
				error: function (resp, e, t) {
					mostrarError(t);
				}
			});
		} else {
			var div = $('<div class="invalid">').html('Ingrese una dirección email válida');
			$('div.errorTxtEmail').append(div);
			e.preventDefault();
			return false;
		}
	});
	//fin modal falta el email      
	//signalr
	//var cc = $.connection.adUser;
	//$.connection.hub.qs = { 'usuario': $('#nombreUsuario').val() };
	//create a function the hub can call
	//cc.client.disconnect = function () {
	//    window.location = '/Login';
	//};
	//$.connection.hub.qs = { 'idUsuario': $('#idUser').val() };    
	//$.connection.hub.start().done(function () {
	//}).fail(function () { });
	//end signalr
	//mostrando error
	var errorMess = $('#errorMessage').val();
	if (errorMess.length > 0) LoginError(errorMess);
	//fin mostrando error    
	//accordion tickets
	var optionsCollapsible = {
		onOpenStart: function (e) {
			var id = $(e).attr('data-id');
			var nro = $(e).attr('data-nro');
			var spinner = '<div class="preloader-wrapper small active">' +
				'<div class="spinner-layer spinner-green-only" >' +
				'<div class="circle-clipper left">' +
				'<div class="circle"></div>' +
				'</div> <div class="gap-patch">' +
				'<div class="circle"></div>' +
				'</div> <div class="circle-clipper right">' +
				'<div class="circle"></div>' +
				'</div></div ></div>';
			var body = $(e).find('div.collapsible-body');
			body.append(spinner);
			$.ajax({
				url: '/Service/getHistoricoIncidente',
				type: 'post',
				data: { text: nro },
				dataType: 'json',
				success: function (json) {
					body.find("label[for='lNroInc']").html(json.Numero);
					body.find("label[for='lCliente']").html(json.Cliente);
					body.find("label[for='lTipo']").html(json.TipoTicket);
					body.find("label[for='lPrioridad']").html(json.PrioridadTicket);
					body.find("label[for='lServicio']").html(json.Servicio);
					body.find("label[for='lJurisdiccion']").html(json.Jurisdiccion);
					body.find("label[for='lDependencia']").html(json.Dependencia);
					body.find("label[for='lTipificacion']").html(json.Tipificacion);
					body.find("label[for='lDescripcion']").html(json.Descripcion);
					toHtmlEquiposeIncidentes(json);
				},
				complete: function () {
					disableBotonVer();
					$(body).find('.preloader-wrapper').remove();
				}
			});
		}
	};
	var elemsMisTCol = document.getElementById('ulMisTickets');
	var misTCol = M.Collapsible.init(elemsMisTCol, optionsCollapsible);
	//fin accordion tickets
	//accordion pedidos
	$('.misPedidos').collapsible();
	//fin acc pedidos
	//nav. pagination    
	$('body').on('click', 'ul.pagination a', function () {
		document.body.onbeforeunload = function () {
			return null;
		}
	})
	//validator
	$.validator.setDefaults({
		ignore: []
	});
	$('.download').on('click', function (e) {
		e.preventDefault();

		document.body.onbeforeunload = function () {
			return null;
		}
		window.location = e.currentTarget.href;
	});
	//$.validator.addMethod("ipVerif", function (value, element) {
	//    return this.optional(element) || /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/d{1,2}/.test(value);
	//}, 'Ingrese una dirección IP con el formato adecuado.');
	$.validator.addMethod("usuarioVer", function (value, element) {
		return usuarioSeleccionado != null;
	});
	$.validator.addMethod('IfGobierno', function (value, element) {
		var origen = $('#origenP').val();
		if (origen == "Pc-Gobierno") {
			if (element.id == 'origenP') {

			}
			if (element.id == 'nombreHostP') {
				if (hostAcceso != null) return true;
				else return false;
			}
			if (element.id == 'ipHostP') {
				var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
				if (regIp.test(value)) return true;
				else return false;
			}
			if (value.length > 3) return true;
			else return false;
		}
		else return true;
	});
	$.validator.addMethod('elemsAdded', function (value, element) {
		var lis = $(element).closest('div.origen-destino').siblings('div.elemsWrapper').find('ul.collection li');
		if (lis.length > 0) return true;
		else return false;
	});
	var pedidoRegla = $('#pedidoReglas').validate({
		rules: {
			asunto: {
				required: true,
				minlength: 5
			},
			descripcion: {
				required: true,
				minlength: 10
			},
			autorizante: {
				required: true
			},
			jurisdiccion: {
				required: true
			},
			dependencia: {
				required: true
			},
			usuario: {
				required: true,
				usuarioVer: true,
			},
			tipoOrigen: {
				required: true,
				elemsAdded: true
			},
			tipoDestino: {
				required: true,
				elemsAdded: true
			},

			//hOrigen: {
			//    required: true,
			//    hostVer: true
			//},
			//hDestino: {
			//    required: true,
			//    hostVer: true
			//},
			vencimiento: {
				required: true
			}

		},
		submitHandler: function (form) {
			$('div.invalidai').remove();
			var f = form;
			var cServicios = $('.cServicios:checked');
			var cProtocolos = $('.cProtocol:checked');
			var flag = true;
			if (cServicios.length > 0) {
				var reqC = $(cServicios).hasClass('cServicioRequireInput');
				if (reqC) {
					var input = $('#requiredPort').val();
					var reg = new RegExp(/(\d{2,5})\,{0,1}/);
					if (!reg.test(input)) {
						var divError = $('#requiredPort').siblings('div.errorTxt');
						var errorM = $('<div class="invalid">').html('Ingrese un nro. de puerto válido!');
						divError.append(errorM);
						flag = false;
					}
				}
			} else {
				var divError = $('.errorTxt8');
				var errorM = $('<div class="invalidai">').html('Seleccione al menos un servicio');
				divError.append(errorM);
				flag = false;
			}
			if (cProtocolos.length > 0) {
				var reqC = $(cProtocolos).hasClass('cRequiredProtocol');
				if (reqC) {
					var input = $('#requiredProt').val();
					var reg = new RegExp(/(?:(^[a-z]{2,6})(?:\,{1}[a-z]{2,6})*)$/);
					if (!reg.test(input)) {
						var divError = $('#requiredProt').siblings('div.errorTxt');
						var errorM = $('<div class="invalid">').html('Ingrese el protocolo! </br> (hasta 6 letras separadas por coma)');
						divError.append(errorM);
						flag = false;
					}
				}
			} else {
				var divError = $('.errorTxt9');
				var errorM = $('<div class="invalidai">').html('Seleccione al menos un protocolo');
				divError.append(errorM);
				flag = false;
			}
			if (flag) {
				document.body.onbeforeunload = function () {
					return null;
				}
				form.submit();
			}
		},
		errorClass: "invalid",
		messages: {
			asunto: {
				required: "Ingrese el asunto",
				minlength: "Ingrese al menos 5 caracteres"
			},
			descripcion: {
				required: "Ingrese la descripción",
				minlength: "Ingrese al menos 10 caracteres"
			},
			autorizante: {
				required: "Seleccione el Responsable de autorizar"
			},
			jurisdiccion: {
				required: "Seleccione la jurisdicción"
			},
			dependencia: {
				required: "Seleccione la dependencia"
			},
			usuario: {
				required: "Seleccione un usuario",
				usuarioVer: "Seleccione un usuario válido"
			},
			tipoOrigen: {
				required: "Seleccione el tipo del orígen",
				elemsAdded: "Ingrese el orígen"
			},
			tipoDestino: {
				required: "Seleccione el tipo del destino",
				elemsAdded: "Ingrese el destino"
			},
			serviciosConocidos: {
				servicios: "Seleccione un servicio"
			},
			//hOrigen: {
			//    required: "Seleccione el host origen",
			//    hostVer: "Seleccione un host válido"
			//},
			//hDestino: {
			//    required: "Seleccione el host destino",
			//    hostVer: "Seleccione un host válido"
			//},
			vencimiento: {
				required: "Seleccione el vencimiento"
			}
		},
		errorElement: 'div',
		errorPlacement: function (error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}

	});
	var pedidoAcceso = $('#pedidoAcceso').validate({
		onfocusout: function (e, v) {
			var el = e;
			var va = v;
			//if (e.id == 'cuilusuarioAcceso') {
			//    pedidoAcceso.element('#cuilusuarioAcceso')
			//}
		},
		onclick: function (e, v) {
			var el = e;
			var va = v;
		},
		onkeyup: function (e, v) {
			var el = e;
			var va = v;
		},
		rules: {
			asuntoP: {
				required: true,
				minlength: 5
			},
			descripcionP: {
				required: true,
				minlength: 10
			},
			autorizanteP: {
				required: true
			},
			cuilusuarioAcceso: {
				required: true,
				usuarioVer: true
			},
			jurisdiccionP: {
				required: true
			},
			origenP: {
				required: true,
				IfGobierno: true
			},
			nombreHostP: {
				IfGobierno: true
			},
			ipHostP: {
				IfGobierno: true
			},
			dependenciaP: {
				required: true
			},
			vencimientoP: {
				required: true
			},
			destinoSolP: {
				required: true
			}
		},
		submitHandler: function (form) {
			var flag = true;
			if (flag) {
				document.body.onbeforeunload = function () {
					return null;
				}
				form.submit();
			}
		},
		errorClass: "invalid",
		messages: {
			asuntoP: {
				required: "Ingrese el asunto",
				minlength: "Ingrese al menos 5 caracteres"
			},
			descripcionP: {
				required: "Ingrese la descripción",
				minlength: "Ingrese al menos 10 caracteres"
			},
			autorizanteP: {
				required: "Seleccione el Responsable de autorizar"
			},
			cuilusuarioAcceso: {
				required: "Seleccione el usuario",
				usuarioVer: "Seleccione un usuario válido"
			},
			origenP: {
				required: "Seleccione el orígen del acceso",
				ifGobierno: "Seleccione el host"
			},
			jurisdiccionP: {
				required: "Seleccione la jurisdicción"
			},
			dependenciaP: {
				required: "Seleccione la dependencia"
			},
			vencimientoP: {
				required: "Seleccione el vencimiento"
			},
			nombreHostP: {
				IfGobierno: "Seleccione el Host de gobierno"
			},
			ipHostP: {
				IfGobierno: "Seleccione el ip de gobierno"
			},
			destinoSolP: {
				required: "Seleccione el destino"
			}
		},
		errorElement: 'div',
		errorPlacement: function (error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}
	});
	//handlers
	$('#ipHostP').on('keyup', function () {
		pedidoAcceso.element('#ipHostP')
	});
	$('body').on('click', '.btnAction', function (e) {
		var urlAttr = $(this).attr('data-url');
		var dataId = $(this).attr('data-idPedido');
		var userN = $('#dispNameUsuario').val();
        var priority = null;
        var div = $(this).closest('div.collapsible-body');
        var estadoActual = $('.estadoActual', div);
        // if (urlAttr == 'Aprobar') {
        //  var cont = $(this).closest('.collapsible-body');
		//	priority = $('.priority',cont).val();
		//	var estadoActual = $('.estadoActual',cont).val();
		//	if (!priority & estadoActual == 'Iniciado') {
		//		mostrarError("Seleccione la prioridad");
		//		return;
		//	}
		//}
        if (urlAttr == 'Aprobar' & (estadoActual.val() == "Iniciado" | estadoActual.val() == "Verificar")) {
            priority = $('.priority', div);
            if (!priority.val()) {
                mostrarError("Seleccione la prioridad");
                return;
            }
            else {
                $('#modalObs').attr('data-prioridad', priority.val());
            }
        }
		$('#modalObs').attr('data-url', urlAttr).attr('data-idPedido', dataId).attr('data-user', userN);
		
		$('div.errorTxtObs').html('');
		$('#observ').val('');
		obsModalInstance.open();
	});
	$('body').on('click', '.btnMov', function (e) {
		var modal = $(this).siblings('div.modal');
		var modInst = M.Modal.init(modal[0], {});
		modInst.open();
	});
	$('#enviarObs').on('click', function (e) {
		var attribs = this;
		var modal = obsModalInstance;
		var url = modal.el.attributes['data-url'].textContent;
		var idPedido = modal.el.attributes['data-idPedido'].textContent;
		var userN = modal.el.attributes['data-user'].textContent;
		var prioridad = modal.el.attributes['data-prioridad'];
		var observaciones = $('#observ').val();
		var data = {
			'idPedido': idPedido,
			'nombreUsuario': userN,
			'obs': observaciones
		}
		if (prioridad) {
			data['prioridad'] = prioridad.textContent;
		}
		$.ajax({
			url: 'Reglas/' + url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function (data) {
				if (data.Info == 'ok') {

					document.body.onbeforeunload = function () {
						return null;
					}

					mostrarExito(data.Detail);
					window.location.reload();
				} else {
					mostrarError(data.Detail);
				}
			}
		});

	});
	$('#hOrigen,#hDestino,#nombreHostP').on('click', function () {
		$(this).val('');
		var idA = $(this).attr('id');
		if (idA == 'hOrigen') hostOrigen = null;
		else {
			if (idA == 'hDestino') hostDestino = null;
			else hostAcceso = null;
		}
		var autoho = document.getElementById('hOrigen');
		var autohd = document.getElementById('hDestino');
		var autoHostOrigen = M.Autocomplete.getInstance(autoho);
		var autoHostDestino = M.Autocomplete.getInstance(autohd);
		autoHostOrigen.close();
		autoHostDestino.close();
	});
	$('.cuilUsuario').on('keyup', function (e) {
		var ter = $(this).val();
		var parDiv = $(this).closest('div.row');
		var id = e.currentTarget.id;
		if (ter.length > 5) {
			var term = '{ prefix: "' + ter + '"}';
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "/Config/getAdCuit",
				timeout: 2000,
				data: term,
				dataType: "json",
				beforeSend: function () {
					$('<div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left">' +
						'<div class="circle"></div></div><div class="gap-patch"><div class="circle"></div>' +
						'</div><div class="circle-clipper right"><div class="circle"></div></div></div ></div >').appendTo(parDiv);
				},
				complete: function () {
					$('div.preloader-wrapper', parDiv).remove();
				},
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						//autoUsuario.close();
                        var obj = {};                        
						arrayUsuario.length = 0;
						$.each(data, function (i, val) {
							var obj2 = {};

							var cuil = val.nombre;
							var nombre = val.displayname;
							var email = val.email;
							obj2.nrocuil = cuil;
							obj2.nombre = nombre;
							obj2.email = email;
							obj2.Dominio = val.Dominio;
							obj[cuil] = null;
							arrayUsuario.push(obj2);
							//var cuil = val.nombre;
							//var nombre = val.displayname;
							//obj['id'] = nombre;
							//obj['text'] = cuil;
							//obj=[{ 'id': '5284', 'text': 'Salazar' }, { 'id': '31241', 'text': 'Salamanca' }, { 'id': '82241', 'text': 'Saladface' }]
						});
						$.each(autoUsuario, function (i, v) {
							if (v.el.id == id) {
								v.updateData(obj);
								v.open();
							}
						})

					}
				}
			})
		}
	});
	$('.cuilUsuario').on('click', function (e) {
		usuarioSeleccionado = null;
		$(this).closest('div.tooltipped').find('input').not(this).val('');
	});
	$('#hOrigen,#hDestino,#nombreHostP').on('keyup', function (e) {
		var ter = $(this).val();
		var id = $(this).attr('id');
		if (ter.length > 2) {
			var term = '{ prefixText: "' + ter + '"}';
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "/Service/EquipoXOblea",
				data: term,
				dataType: "json",
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						var obj = {};
						$.each(data, function (i, val) {
							obj[val.Oblea] = null;
						});
						if (id == 'hOrigen') {
							//autoHostOrigen.close();
							autoHostOrigen.updateData(obj);
							autoHostOrigen.open();

						} else {
							if (id == 'hDestino') {
								//autoHostDestino.close();
								autoHostDestino.updateData(obj);
								autoHostDestino.open();
							} else {
								autoHostAcceso.updateData(obj);
								autoHostAcceso.open();
							}
						}
					}
				}
			})
		}
	});
	$('input.radioSelection').on('change', function (e) {
		var value = $(this).val();
		if (value == 'host') {
			var divhost = $(this).parents('div.tooltipped').find('div.hostSelector');
			var divred = $(this).parents('div.tooltipped').find('div.redSelector');
			divhost.removeClass('hide');
			divred.addClass('hide');
		}
		if (value == 'red') {
			var divhost = $(this).parents('div.tooltipped').find('div.hostSelector');
			var divred = $(this).parents('div.tooltipped').find('div.redSelector');
			divred.removeClass('hide');
			divhost.addClass('hide');
		}
		//$(this).parents('div.tooltipped').find('ul.collection li').remove();
		//resetDestino();
		//resetOrigen();
	});
	$('body').on('click', '.removeColl', function () {
		$(this).closest('li.collection-item').remove();
	});
	$('div.agregarElem').on('click', function (e) {
		var ODrow = $('.origen-destino');
		var Origen = ODrow.find('.rowOrigen');
		var Destino = ODrow.find('.rowDestino');
		var Usuario = ODrow.find('.rowUsuario');
		var HostRedswitch;
		var wraperItems = $('div.elemsWrapper');
		var ul = wraperItems.find('ul');
		var i = $(ul).find('li').length;
		var inputs = $(ul).find('input');
		var li = $('<li class="collection-item">');
		var div = $('<div class="row">');
        var icon = $('<a href="#!" class="col s1 right-align secondary-content removeColl"><i class="material-icons">cancel</i></a>');
        var checkOrigen = $("input[name=tipoOrigen]:checked");
        var divErOrigen = $('div.errorTxt20');
        var checkDestino = $("input[name=tipoDestino]:checked");
        var divErDestino = $('div.errorTxt21');
        if (checkOrigen.length == 0) {            
            divErOrigen.html('Seleccione el origen').addClass('invalid');
        }
        else {
            divErOrigen.html('').removeClass('invalid');
        }
        if (checkDestino.length == 0) {
            divErDestino.html('Seleccione el destino').addClass('invalid');
        }
        else {
            divErDestino.html('').removeClass('invalid');
        }
		if (Origen) // Es origen
		{
			var hostInput, errorHost, ipInput, errorIp, usuarioInput, errorUsuario, redOrigen, errorRed, hasHost;
			var flag = true;
			HostRedswitch = Origen.find('input[type=radio]:checked').val();
			console.log('Origen switcher', HostRedswitch);
			if (HostRedswitch == 'host') // Es host
			{
				console.log('es host');
				hostInput = Origen.find('#hOrigen');
				ipInput = Origen.find('#ipOrigen');
				errorHost = hostInput.siblings('div.error');
				errorIp = ipInput.siblings('div.error');
				errorHost.html('');
				errorIp.html('');
				// Host seleccionado
				hasHost = hostOrigen != null;
				// Verif. host repetidos
				/*$.each(inputs, function (i, v) {
				//	if ($(v).val() == hostInput.val())
				//	{
				//		errorHost.html('Ese host ya fue agregado').addClass('invalid');
				//		flag = false;
					}
				//});*/
				var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
				if (!hasHost)
				{
					errorHost.html('Seleccione un host').addClass('invalid').show();
					flag = false;
				}
				if (!regIp.test(ipInput.val()))
				{
					errorIp.html('Escriba un IP válido').addClass('invalid').show();
					flag = false;
				}
			}
			else // Es red
			{
				console.log('es red');
				redOrigen = Origen.find('#redOrigen');
				errorRed = redOrigen.siblings('div.error');
				errorRed.html('');
				// usuarioInput = ODrow.find('#cuilenred');
				// errorUsuario = usuarioInput.siblings('div.error');
				// errorUsuario.html('');
				var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/;
				//if (inputs.length > 0)
				//{
				//	errorRed.html('Ingrese una única red por pedido').addClass('invalid');
				//	flag = false;
				//}
				if (!regIp.test(redOrigen.val()))
				{
					errorRed.html('Escriba una dirección de red válida').addClass('invalid');
					flag = false;
				}
			}
			// FLAG
			if (flag)
			{
				var col = $('<div class="col s3">');
				var divisor = $('<div class="col s1"><i class="material-icons">chevron_right</i></div>');
				var inp1, inp2;
				if (HostRedswitch == 'host') // Agrego host
				{
					inp1 = $('<input type="text" name="equipoOrigen' + i + '" value="' + hostInput.val() + '" class="col s6" readonly>');
					inp2 = $('<input type="text" name="ipOrigen' + i + '" value="' + ipInput.val() + '" class="col s6" readonly>');
					col.append(inp1).append(inp2);
				}
				else // Agrego red
				{
					inp1 = $('<input type="text" name="redOrigen' + i + '" value="' + redOrigen.val() + '"class="col s12" readonly>')
					col.append(inp1);
				}
				div.append(col).append(divisor);
			}
			else // Con errores salgo
			{
				return;
			}
		} // Fin es origen

		if (Destino) // Es destino
		{
			var hostInput, errorHost, ipInput, errorIp, redDestino, errorRed, hasHost;
			flag = true;
			HostRedswitch = Destino.find('input[type=radio]:checked').val();
			console.log('Destino switcher', HostRedswitch);
			if (HostRedswitch == 'host') // Es host
			{
				hostInput = Destino.find('#hDestino');
				ipInput = Destino.find('#ipDestino');
				errorHost = hostInput.siblings('div.error');
				errorIp = ipInput.siblings('div.error');
				errorHost.html('');
				errorIp.html('');
				// Host seleccionado
				hasHost = hostDestino != null;
				// Verif. host repetidos
				//$.each(inputs, function (i, v) {
				//	if ($(v).val() == hostInput.val())
				//	{
				//		errorHost.html('Ese host ya fue agregado').addClass('invalid');
				//		flag = false;
				//	}
				//});
				var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
				if (!hasHost)
				{
					errorHost.html('Seleccione un host').addClass('invalid').show();
					flag = false;
				}
				if (!regIp.test(ipInput.val()))
				{
					errorIp.html('Escriba un IP válido').addClass('invalid').show();
					flag = false;
				}
			}
			else // Es red
			{
				redDestino = Destino.find('#redDestino');
				errorRed = redDestino.siblings('div.error');
				errorRed.html('');
				var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/;
				//if (inputs.length > 0)
				//{
				//	errorRed.html('Ingrese una única red por pedido').addClass('invalid');
				//	flag = false;
				//}
				if (!regIp.test(redDestino.val()))
				{
					errorRed.html('Escriba una dirección de red válida').addClass('invalid').show();
					flag = false;
				}
			}
			if (flag)
			{
				var col = $('<div class="col s3">');
				var divisor = $('<div class="col s1"><i class="material-icons">code</i></div>');
				var inp1, inp2;
				if (HostRedswitch == 'host') // Agrego host
				{
					inp1 = $('<input type="text" name="equipoDestino' + i + '" value="' + hostInput.val() + '"class="col s6" readonly>');
					inp2 = $('<input type="text" name="ipDestino' + i + '" value="' + ipInput.val() + '"class="col s6" readonly>');
					col.append(inp1).append(inp2);
				}
				else // Agrego red
				{
					inp1 = $('<input type="text" name="redDestino' + i + '" value="' + redDestino.val() + '"class="col s12" readonly>')
					col.append(inp1);
				}
				div.append(col).append(divisor);
			}
			else // Con errores salgo
			{
				return;
			}
		}

		if (Usuario) // Es Usuario
		{
			var cuilInput, errorUser, nameUserInput, dominioInput, UserOK, emailInput, usuarioAnon;
            flag = true;
            usuarioAnon = Usuario.find('#usuarioAnon').prop('checked');
			cuilInput = Usuario.find('#cuilusuarioRegla');
			nameUserInput = Usuario.find('#nombreusuarioRegla');
            dominioInput = Usuario.find('#domainRegla');
            emailInput = Usuario.find('#emailUsuarioSeleccionadoRegla')
                
			errorUser = cuilInput.siblings('div.error');
			errorUser.html('');
            // Usuario seleccionado
            UserOK = usuarioSeleccionado != null;

            if (!UserOK & !usuarioAnon) {
                errorUser.html('Seleccione un usuario').addClass('invalid').show();
				flag = false;
			}

			if (flag) {
				var col = $('<div class="col s3">');
                var inp1, inp2, inp3, inp4;
                if (usuarioAnon) {
                    inp1 = $('<input type="text" name="cuilRgla' + i + '" value="Anonimo" class="col s12" readonly>');
                    col.append(inp1);
                } else {
                    inp1 = $('<input type="text" name="cuilRgla' + i + '" value="' + cuilInput.val() + '" class="col s12" readonly>');
                    inp2 = $('<input type="hidden" name="userNameRegla' + i + '" value="' + nameUserInput.val() + '" readonly>');
                    inp3 = $('<input type="hidden" name="dominioregla' + i + '" value="' + dominioInput.val() + '" readonly>');
                    inp4 = $('<input type="hidden" name="emailUsuarioRegla' + i + '" value="' + emailInput.val() + '" readonly>');
                    col.append(inp1).append(inp2).append(inp3).append(inp4);
                }
				div.append(col).append(icon);
			}
			else // Con errores salgo
			{
				return;
			}
		}

		li.append(div);
		ul.append(li);
		resetOrigen();
		resetDestino();
        resetUsuario();
		if (wraperItems.find('ul.collection').length == 0)
		{

		}
	});
	$('#origenP').on('change', function (e) {
		var sel = this;
		var valor = $(sel).val();
		var nombreHost = $('#nombreHostP');
		var ipHost = $('#ipHostP');
		if (valor == 'Pc-Personal') {
			nombreHost.attr('readonly', 'readonly').val('');
			ipHost.attr('readonly', 'readonly').val('');
		} else {
			nombreHost.removeAttr('readonly');
			ipHost.removeAttr('readonly');
		}
		pedidoAcceso.element('#origenP');
		pedidoAcceso.element('#nombreHostP');
		pedidoAcceso.element('#ipHostP');
	});

});//READY
function LoginError(message) {
	$('#LoginError span').html(message);
	$('#LoginError').removeClass('hide');
}
function resetOrigen() {
	var hostInput = $('#hOrigen');
	var redOrigen = $('#redOrigen');
	var ipInput = $('#ipOrigen');
	var usuarioInput = $('#cuilenhost, #cuilenred');
	hostInput.val('').siblings('div.error').html('');
	ipInput.val('').siblings('div.error').html('');
	usuarioInput.val('').siblings('div.error').html('');
	redOrigen.val('').siblings('div.error').html('');
	usuarioSeleccionado = null;
	hostOrigen = null;
}
function resetDestino() {
	var hostdes = $('#hDestino');
	var redDestino = $('#redDestino');
	var ipdest = $('#ipDestino');
	hostdes.val('').siblings('div.error').html('');
	redDestino.val('').siblings('div.error').html('');
	ipdest.val('').siblings('div.error').html('');
	hostDestino = null;
}
function resetUsuario() {    
    usuarioSeleccionado = null;  
    var inputs = $('#usuarioAnon').closest('.rowUsuario').find('input[type="text"]');   
        $.each(inputs, function () {
            $(this).prop('disabled', false);
            $(this).val('');
        })
        
    $('#usuarioAnon').prop('checked', false);
    // M.updateTextFields();
}
function toHtmlEquiposeIncidentes(json) {
	var resolucionesIncidente;
	var resolucionesEquipo;
	var equipos = "";
	var areasXIncidente = "";
	$.each(json.Areas, function (i, item) {
		resolucionesIncidente = "<table class='hide striped'><tr><th>Fecha</th><th>Técnico</th><th>T.Resolución</th><th>Equipo</th><th>Trabajo</th><th>Adjunto</th></tr>";
		if (item.Resoluciones.length > 0) {
			$.each(item.Resoluciones, function (i, item2) {
				resolucionesIncidente += '<tr><td>' + item2.FechaStr + '</td>' + '<td>' + item2.Tecnico + '</td>' +
                    '<td>' + item2.TipoResolucion + '</td>' + '<td>' + item2.Equipo + '</td>' + '<td>' + item2.Observaciones + '</td><td>' +
                    (item2.PathImagen != null ? '<a target="_blank" href="' + item2.PathImagen + '"><i class="material-icons">visibility</i></a>' : '<a href="#!"><i class="material-icons">visibility_off</i></a>')
					+ '</td></tr>';
			})
		}
		resolucionesIncidente += "</table>";

		areasXIncidente += '<tr><td>' + item.Direccion + '</td><td>' + item.Area.NombreArea + '</td>' + '<td>' + item.FechaDesdeShStr + '</td>' + '<td>' + item.FechaHastaShStr + '</td>'
			+ '<td>' + item.UsuarioQEnvio + '</td><td class="tObs">' + resolucionesIncidente + '</td></tr>';
	});
	$("#areasIncidente tbody").html(areasXIncidente);
}
function disableBotonVer() {
	$('.tObs').each(function () {
		var a = $('<a href="#!">');
		var test = $(this).find('tr').length;
		//$(this).css('background-color', 'red');
		if (test > 1) {
			var iel = $('<i class="material-icons">visibility</i>');
			a.append(iel);
			$(a).on('click', verResolucion);
		}
		else {
			var iel = $('<i class="material-icons">visibility_off</i>');
			a.append(iel);
		}
		$(this).append(a);
	});
}
function verResolucion(event) {
	//parametros principales
	var html = $(this).closest('.tObs').find('table').clone().removeClass('hide');
	$('#modalJobs').find('.modal-content').html(html);
	jobsModalInstance.open();

}