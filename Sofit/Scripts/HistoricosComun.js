﻿window.filterCount=0;
$(function () {
    
    $('button:not(.searchButton)').button(
        {
            'ui-button':'highlight'
        });
    $('input','#inputCheck').checkboxradio();
    $("#tabset").tabs({ selected: "0" }); 
    $('#searchIncidente').on('click', buscarHistoriaIncidente);
    $('#bBuscarEquipo').on('click', buscarHistoriaEquipo);
    $('#bBuscarCliente').on('click', buscarHistoriaCliente);
    $('#bBuscarOtros').on('click', buscarHistoriaOtros);    
    $('body').on('click', '.addFilterTicket', function () {
        var filterPane = $(this).parents('.buttonBar').siblings('.filterPane');
        var filterItem = $('<div>')
            .addClass('filterItem')
            .data('suffix', '.' + (filterCount++));
        $('div.template.optionsChooser')
            .children().clone().appendTo(filterItem)
            .trigger('adjustName');

        filterItem.appendTo(filterPane);
    });
    $('body').on('click', '.verCambios', verCambios);
    /**********
    *Ver Info
    ***********/
    $('#pEstadistica').on('click', '.verInfo', function (e) {
        var res = $(this).attr('data-res');

        $('<div>').append(res).dialog({
            show: 'slide',
            width: 'auto',
            position: { my: "right top", at: "right bottom", of: e.target }

        }).click(function () { $(this).dialog('destroy') });
        $(".ui-dialog-titlebar").hide();
    });
    $('body').on('click', 'i.verNotificaciones', function (e) {
        var ul = $(this).siblings('ul.notis').clone();
        ul.removeClass('hide');
        var dialog = $('<div style="white-space: pre-wrap;">');
        dialog.append(ul).dialog({
            width: '500px',
            show: 'slide',
            maxHeight: 350,
            position: { my: "right middle", at: "right bottom", of: e.target },
            close: function () {
                $(this).dialog('destroy')
            }

        });
    });
    /************
    **Autocompletes
    *************/
    $('body').on('click','.verIncidente', function () {
        var nroInc = $(this).html();
        $("#tabset").tabs({ active: 0 });
        $('#tBuscar').val(nroInc);
        $('#searchIncidente').click();
    });
    //$('.searchButton').on('click', function (e) {
    //    var tipo = $(this).data('tipo');
    //    var val = $(this).val();
    //    switch (tipo) {
    //        case "": ;
    //            break;
    //        case "": ;
    //            break;

    //    }
    //});
    //autocomplete menos para busqueda por otros
    $(".inputB").focus(function (srcc) {        
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("defaultTextActive");
            //$(this).val($(this)[0].title);
        }
    }).autocomplete({
        minLength: "2",
        source: (
            function (request, response) {
                var term = '{ prefixText: "' + request.term + '" }';
                var element = this.element.context;
                var url = $(element).data('url');
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: url,
                    data: term,
                    dataType: "json",
                    beforeSend: function (e) {
                        NProgress.configure({ parent: 'body' })
                        NProgress.start();
                    },
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        else {
                            var suggestions = [];
                            $.each(data, function (i, val) {
                                var obj = {};
                                obj.value = val;
                                obj.label = val;
                                suggestions.push(obj);
                            })
                            response(suggestions);
                        }
                    },
                    complete: function () {
                        NProgress.done();
                    }
                });
            }),
        select: function (e, ui) {
            var tipo = $(this).data('tipo');
            var text = ui.item.value;
            $(this).val(text);
            switch (tipo) {
                case "ticket":
                    buscarHistoriaIncidente();
                    break;
                case "cliente": buscarHistoriaCliente();
                    break;
                case "ec": buscarHistoriaEquipo();
                    break;
            }          
        }
        });    
    //autocomplete para otros
    $('#tBuscarxOtros').autocomplete({
        minLength: "5",
        source: (
            function (request, response) {
                var criteria = $('input[name="radio-criteria"]:checked').val();
                var selectedPanel = $('#busquedaContent');
                var sbar = selectedPanel.find('div.searchBar');
                if (!criteria) {
                    mostrarError("Elija el criterio de búsqueda!!")
                    return;
                }
                var term = '{ prefixText: "' + request.term + '" , criteria:"' + criteria + '"}';
                var element = this.element.context;
                var url = '/Service/getCoincidencias';
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: url,
                    data: term,
                    dataType: "json",
                    beforeSend: function () {
                        NProgress.configure({ parent:'#pIncXOtros .searchCont'})
                        NProgress.start();
                    },
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        else {
                            var suggestions = [];
                            $.each(data, function (i, val) {
                                var req = request;
                                var obj = {};
                                obj.value = val.IdTicket;
                                obj.label = val.Descripcion;                                
                                suggestions.push(obj);
                            })
                            response(suggestions);
                        }
                    },
                    complete: function () {
                        NProgress.done();
                    }
                });
            }),
        select: function (e, ui) {            
            var text = ui.item.label;
            $(this).val(text);
            var idTicket = ui.item.value;
            var criteria = $('input[name="radio-criteria"]:checked').val();
            $(this).attr('data-criteria', criteria);
            buscarHistoriaOtros();
            return false;
        }
    });
    /*************
    *Tickets
    ************/
    $('.subDirSel').on('change', function () {
        var context = $(this).parents('div.ui-tabs-panel');
        var filterPane = $(context).find('.filterPane');
        $('option[value=""]', this).remove();
        $(filterPane).find('.filterItem').remove();        
        var filterItem = $('<div>')
                .addClass('filterItem')
                .data('suffix', '.' + (filterCount++));
        $('div.template.ticketChooser')
                  .children().clone().appendTo(filterItem)
                  .trigger('adjustName');
        $(filterPane).append(filterItem);
    });
    /**********
    *-->Tickets
    ***********/
    /*************
    Trabajos
    *************/
    $('.applyFilterButton').click(aplicarFiltros);
    $('body').on('click', '.addFilterButton', function () {
        var ctx = $(this).parents('div.ui-tabs-panel');
        AddFiltros(ctx);
    });
    $('.mainSel').on('change', function () {
        var context = $(this).parents('div.ui-tabs-panel');
        $('option[value=""]', this).remove();
        $('.filterPane', context).find('.filterItem').remove();
        AddFiltros(context);        
    });     
    $('div.filterPane').on('change', 'select.filterChooser', function () {
        var filterType = $(':selected', this).attr('data-filter-type');
        var value = $(':selected', this).val();//para tickets
        if (filterType == 'xsolucion') {
            var pref = $(this).parents('div.filterItem').siblings('div.template').find('select.mainSel>option:selected').attr('data-filter-type');
            if (pref != null) {
                pref = pref.split('.').join("");
                filterType += pref;
            }
        }        
        var filterItem = $(this).closest('.filterItem');
        $('.qualifier', filterItem).remove();
        $(this).siblings('span').remove();
        //paratickets
        if (value == 'mensual') {
            var hidInput = $('<input type="hidden" class="qualifier">').val('mensual').attr('data-qualifier','tipoReport');
            filterItem.append(hidInput);
        }
        if (value == 'anual') {
            var hidInput = $('<input type="hidden" class="qualifier">').val('anual').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        if (value == 'mensualjurisdiccion') {
            var hidInput = $('<input type="hidden" class="qualifier">').val('mensualjurisdiccion').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        if (value == 'mensualTotal') {
            var hidInput = $('<input type="hidden" class="qualifier">').val('mensualTotal').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        if (value == 'listado') {
            var hidInput = $('<input type="hidden" class="qualifier">').val('listado').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        if (value == "creados") {
            var hidInput = $('<input type="hidden" class="qualifier">').val('creados').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        if (value == "creadosycerradostotales") {
            var hidInput = $('<input type="hidden" class="qualifier">').val('creadosycerradostotales').attr('data-qualifier', 'tipoReport');
            filterItem.append(hidInput);
        }
        $('div.template.' + filterType)
              .children().clone().not('span').addClass('qualifier').attr('data-qualifier',filterType).end()
              .appendTo(filterItem)
              .trigger('adjustName')
              .trigger('addAutoComplete')
              .trigger('IncAutoComplete');
        $('option[value=""]', this).remove();       
        
    });
    $('body').on('focus', '.date,.dateAnio', function () {
        $(this).datepicker({
            beforeShow: function( input, inst){
                if ($(this).hasClass('dateAnio')) {
                    $(inst.dpDiv).addClass('sinCal');
                    $(this).datepicker('option','dateFormat','yy MM')
                }
                else {
                    $(inst.dpDiv).removeClass('sinCal');
                    $(this).datepicker('option', 'dateFormat', 'dd/mm/yy')
                }
            },
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,            
            onClose: function (dateText, inst) {
                if ($(this).hasClass('dateAnio')) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
            }
        });
    });
    $('body').on('focus', '.dateAnual', function () {
        $(this).datepicker({
            beforeShow: function (input, inst) {
                $(inst.dpDiv).addClass('sinCal');
                $(this).datepicker('option','dateFormat','yy')
            },
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy',
            onClose: function (datetext, inst) {
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, 1, 1));
            }
        });
    });
    $('select.filterChooser2').live('change', function () {
        var filterType = $(':selected', this).attr('data-filter-type');
        var filterItem = $(this).closest('.filterItem');
        $('.qualifier2', filterItem).remove();
        $('div.template.' + filterType)
              .children().clone().addClass('qualifier2')
              .appendTo(filterItem)
              .trigger('adjustName');
        $('option[value=""]', this).remove();
    });
    $('button.filterRemover').live('click', function () {
        var div=$(this).closest('div.filterItem');
        if (div.hasClass('filterChooser')) return false;
        else div.remove();
    });
    //custom adjustname
    $('.filterItem [id]').on('adjustName', function () {
        var suffix = $(this).closest('.filterItem').data('suffix');
        if (/(\w)+\.(\d)+$/.test($(this).attr('id'))) return;
        $(this).attr('id', $(this).attr('id') + suffix);
    });
    /*******************    
    ********************/
    $("#tOblea").autocomplete({
        minLength: "2", source: (
            function (request, response) {
                var term = '{ prefixText: "' + request.term + '" }';
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/Service/EquipoXOblea",
                    data: term,
                    dataType: "json",
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        else {
                            var suggestions = [];
                            $.each(data, function (i, val) {
                                var obj = {};
                                obj.value = val.idEquipo;
                                obj.label = val.Oblea;
                                suggestions.push(obj);
                            })
                            response(suggestions);
                        }
                    }
                }); //ajax
            }),
        select: function (e, ui) {
            $input.val(ui.item.label);
            return false;
        },
        focus: function (e, ui) {
            return false;
        }
    }); //autocomplete     
    //parametros
    window.par = getParameterByName('nroInc');
    if (par != '') {
        $('#header_fixed,#formSearch').remove();
        var tabset = $('#tabset');
        var thei = tabset.height();
        var whei = $(window).height();
        tabset.css('margin-top', 0).height(whei).tabs('option', 'disabled', true);
        $('#tBuscar').val(par);
        $('#searchIncidente').trigger('click');        
        if (par == 'nroEq') {

        }
    }
    //imagenes y archivos
    $('body').on('click', '.verImagen', function (e) {
        //e.preventDefault();
        var diag = $(this).parents('div.ui-dialog');
        //$(diag).dialog('destroy');
        var idInc = $(this).attr('data-idInc');
        $.post('/Service/pathImagen', { 'id': idInc }, function (data, textstatus, jqxhr) {
            var div = $('<div>');
            div.css({ 'max-width': '500px', 'max-height': '500px' });
            var img = $('<img src="' + data.Detail + '">');
            img.css({ 'width': '100%', 'height': '100%' });
            div.append(img);
            $(div).dialog({
                resize: function (event, ui) {
                    $(div).css({ 'max-width': 'none', 'max-height': 'none' });
                }
            });
        });
    });
    //imagenes en resoluciones
    $('body').on('click', '.verImgRes', function (e) {
        var deta = $(this).attr('data-path');
        var div = $('<div>');
        div.css({ 'max-width': '500px', 'max-height': '500px' });
        var img = $('<img src="' + deta + '">');
        img.css({ 'width': '100%', 'height': '100%' });
        div.append(img);
        $(div).dialog({
            resize: function (event, ui) {
                $(div).css({ 'max-width': 'none', 'max-height': 'none' });
            }
        });
    });
    $('body').on('click', '.printTicket', function () {
        var idInc = $("p[data-name='lIdInc']").html();
        var datos = "id=" + idInc;
        window.open('/Impresion/Index?idInc=' + idInc, '', 'height=600,width=930,menubar=no,toolbar=no,scrollbars=yes,status=no');
        window.focus();
    });
    $('body').on('click', 'span.fa-close', function ()
    {
        $('#areasIncidente').find('td.ui-state-highlight').removeClass('ui-state-highlight');
        $(this).parent().dialog('destroy');

    });
    $('body').on('click', '.historial-estados', function ()
    {
        var $link = $(this).data();
        $.post('/Service/getHistoricoIncEstados', { 'text': $link.NTicket }, function (data, textstatus, jqxhr) {
            var $dialog = $('<div>')
                .dialog({
                    autoOpen: true,
                    title: 'Historial de cambios de estados',
                    width: '70%',
                    minHeight: 400,
                    modal: true
                });


            var $hrow = $('<div class="row">');
            var $hcol1 = $('<div class="col s2"><em><b>Usuario</b></em></div>');
            var $hcol2 = $('<div class="col s3"><em><b>Estado</b></em></div>');
            var $hcol3 = $('<div class="col s2"><em><b>Desde</b></em></div>');
            var $hcol4 = $('<div class="col s5"><em><b>Hasta</b></em></div>');

            $hrow.append($hcol1).append($hcol2).append($hcol3).append($hcol4);
            $dialog.append($hrow);

            for (i = 0; i < data.length; i++)
            {
                var $row = $('<div class="row">');
                var $col1 = $('<div class="col s2">' + data[i].TecnicoNombre + '</div>');
                var $col2 = $('<div class="col s3">' + data[i].Estado + '</div>');
                var $col3 = $('<div class="col s5">' + data[i].FechaIString + '</div>');
                if (data[i].FechaFString != '01/01/0001 00:00:00')
                {
                    var FechaFIN = data[i].FechaFString;
                }
                else
                {
                    var FechaFIN = '';
                }
                var $col4 = $('<div class="col s2">' + FechaFIN + '</div>');

                $row.append($col1).append($col2).append($col3).append($col4);
                $dialog.append($row);
            }

            $($dialog).dialog({
                resize: function (event, ui) {
                    $($dialog).css({ 'max-width': 'none', 'max-height': 'none' });
                }
            });
            $dialog.dialog('open');
        });

        //$link.click(function () {
        //    $dialog.dialog('open');

        //    return false;
        //});
    });
});
/*************
FIN READY
*************/
function verResolucion(event) {
    //parametros principales
    var html = $(this).find('table').clone().removeClass('hide');
    var tr = $(this).parents('tr').find('td:not(:last)');
    tr.addClass('ui-state-highlight');
    var div = $('<div class="tCont">');    
    div.append(html);
    var dialog=$('<div class="info"><span class="fa fa-close topright cursor"></span>').append(div).dialog({
        show: 'slide',
        position: { my: "right top", at: "top", of: event.target },
        width: 'auto',
        resizable: false,
        open: function () {
            var viewpheight = $(window).height();
            $(this).find('div.tCont').css({
                'max-height': viewpheight - 150 + 'px',
                'padding-right': '15px',
                'overflow':'auto'
            });               
        },
        close: function () {
            $('#areasIncidente').find('td.ui-state-highlight').removeClass('ui-state-highlight');
        },
        position: {
            my: "top ",
            at: "center bottom",
            of: event.target,
            using: function (obj, info) {
                var viewpheight = $(window).height();
                var heithtml = $('html').height();
                var widthhtml = $('html').width();
                var dialogH = info.element.height;
                var dialogw = info.element.width;
                var overflowsV = dialogH > heithtml;
                var overflowsH = dialogw > widthhtml;
                var posclickX = event.clientX;
                var posclickY = event.clientY;
                var top = (viewpheight - dialogH) < 105 ? 105 : posclickY - dialogH;
                if (top < 105) top = 105;
                $(this).css(
                    {
                        left: 0 + 'px',
                        top:top + 'px'
                    });
                //if (posclickY + dialogH > viewpheight) {
                //    $(this).css({
                //        left: 0 + 'px',
                //        top: 105 + 'px',
                //        'max-height': viewpheight - 110
                //    });                    
                //} else {
                //    $(this).css({
                //        left: 0 + 'px',
                //        top: info.target.top,
                //    });                    
                //}
            }
        }
    });    
    $(".ui-dialog-titlebar", $(dialog).parent()).hide();
}
function verCambios() {
    //parametros principales
    var html = $(this).siblings('table').clone().removeClass('hide');
    //var tr = $(this).parents('tr').find('td:not(:last)');
    //tr.addClass('ui-state-highlight');
    var div = $('<div class="tCont">');
    div.append(html);
    var dialog = $('<div class="info"><span class="fa fa-close topright cursor"></span>').append(div).dialog({
        show: 'slide',
        position: { my: "right top", at: "top", of: event.target },
        width: 'auto',
        resizable: false,
        open: function () {
            var viewpheight = $(window).height();
            $(this).find('div.tCont').css({
                'max-height': viewpheight - 150 + 'px',
                'padding-right': '15px',
                'overflow': 'auto'
            });
        },
        close: function () {
            $('#areasIncidente').find('td.ui-state-highlight').removeClass('ui-state-highlight');
        },
        position: {
            my: "top ",
            at: "center bottom",
            of: event.target,
            using: function (obj, info) {
                var viewpheight = $(window).height();
                var heithtml = $('html').height();
                var widthhtml = $('html').width();
                var dialogH = info.element.height;
                var dialogw = info.element.width;
                var overflowsV = dialogH > heithtml;
                var overflowsH = dialogw > widthhtml;
                var posclickX = event.clientX;
                var posclickY = event.clientY;
                var top = (viewpheight - dialogH) < 105 ? 105 : posclickY - dialogH;
                if (top < 105) top = 105;
                $(this).css(
                    {
                        left: 0 + 'px',
                        top: top + 'px'
                    });
                //if (posclickY + dialogH > viewpheight) {
                //    $(this).css({
                //        left: 0 + 'px',
                //        top: 105 + 'px',
                //        'max-height': viewpheight - 110
                //    });                    
                //} else {
                //    $(this).css({
                //        left: 0 + 'px',
                //        top: info.target.top,
                //    });                    
                //}
            }
        }
    });
    $(".ui-dialog-titlebar", $(dialog).parent()).hide();
}
function disableBotonVer() {
    $('.tObs').each(function () {
        var test = $(this).find('tr').length;
        //$(this).css('background-color', 'red');
        if (test > 1)
            $(this).addClass('buttonVer').bind('click', verResolucion);
        else {
            $(this).addClass('buttonNa')
        }
    });
}
function buscarHistoriaOtros() {
    var criteria = $('input[name="radio-criteria"]:checked').val();
    var texto = $('#tBuscarxOtros').val();
    var content = $("#busquedaContent");
    content.hide();
    $.ajax({
        url: "/Service/getHistoricoOtros",
        type: 'POST',
        data: { 'text': texto, 'criteria': criteria },
        dataType: 'json',
        beforeSend: function () {
            NProgress.start();
        },
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }            
            else {
                content.show();
                otrosToHTML(data);
                content.css('display', 'inline');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            NProgress.done();
        }
    });
}
function buscarHistoriaCliente() {  
    var text = $.trim($('#tBuscarCliente').val());
    var content = $("#clientesContent");
    content.hide();
    $.ajax({
        url: "/Service/getHistoricoCliente",
        type: 'POST',
        data: 'text=' + text,
        dataType: 'json',
        beforeSend: function () {
            NProgress.start();
        },
        success: function (data) {            
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (data.Info == "error") {                
                mostrarError("Se encontró más de un cliente con ese nombre.");
                return;
            }
            else {
                content.show();
                clientesToHTML(data);
                content.css('display', 'inline');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            NProgress.done();
        }
    });
}
function buscarHistoriaIncidente() {  
    var text = $('#tBuscar').val();
    var content = $("#incidenteContent");
    content.hide();
    $.ajax({
        url: "/Service/getHistoricoIncidente",
        type: 'POST',
        data: 'text=' + text,
        dataType: 'json',
        beforeSend: function () {            
            var selectedPanel = $('#pHistIncidente');
            var sbar = selectedPanel.find('div.searchBar');            
            $('<div style="width:50px;margin:0 auto" id="spin"><img src="/Content/img/ajax-loader.gif"/></div>').insertAfter(sbar);
        },
        success: function (data) {            
            var json = data;            
            if (json == null) {
                mostrarError("No se Encuentra el Ticket");
                content.hide();
                return;
            }
            if (json.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            content.show();            
            content.find("p[data-name='lIdInc']").html(json.IdIncidencia);
            content.find("p[data-name='lNroInc']").html(json.Numero);
            content.find("div.historial-estados").data('NTicket', json.Numero);
            content.find("p[data-name='lFecha']").html(json.FechaInicioString);
            content.find("p[data-name='lCliente']").html(json.Cliente);
            content.find("p[data-name='lTipo']").html(json.TipoTicket);
            content.find("p[data-name='lPrioridad']").html(json.PrioridadTicket);
            content.find("p[data-name='lEstado']").html(json.EstadoTicket);
            content.find("p[data-name='lFechaLimite']").html(json.FechaStrPrevista);
            content.find("p[data-name='lServicio']").html(json.Servicio);
            content.find("p[data-name='lJurisdiccion']").html(json.Jurisdiccion);
            content.find("p[data-name='lDependencia']").html(json.Dependencia);
            content.find("p[data-name='lTipificacion']").html(json.Tipificacion);           
            content.find("p[data-name='lImagen']").html((json.pathImg != '' ? '<a href="#" class="verImagen" data-idInc="' + json.IdIncidencia + '" data-path="' + json.pathImg + '">Ver Imágen</a>' : "Sin imágen"));
            content.find("p[data-name='lFile']").html((json.pathFile != '' ? '<a href="/Service/pathFile/' + json.IdIncidencia + '">Get File</a>' : "Sin adjuntos"));
            content.find("p[data-name='lDescripcion']").html(json.Descripcion);
            toHtmlCambiosPrioridadYNotificacion(json);
            toHtmlEquiposeIncidentes(json);
            content.css('display', 'inline');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            disableBotonVer();
            $('div#spin').remove();
        }
    });
}
function toHtmlCambiosPrioridadYNotificacion(json) {
    var content = $("#incidenteContent");
    if (json.CambiosPrioridad.length == 0) {
        content.find("p[data-name='lCambioPrioridad']").html("Sin cambios realizados");
    }
    else {
        content.find("p[data-name='lCambioPrioridad']").html('<i class="fa fa-eye cursor verCambios">');
        var tablaCambios;
        tablaCambios = "<table class='info hide'><tr><th>Fecha</th><th>Prioridad Previa</th><th>Prioridad Posterior</th><th>Usuario</th></tr>";
        $.each(json.CambiosPrioridad, function (i, item) {

            tablaCambios += '<tr><td>' + item.FechaStr + '</td>' + '<td>' + item.EstadoPrevio + '</td>' +
                        '<td>' + item.EstadoNuevo + '</td>' + '<td>' + item.Usuario + '</td>' 
                        + '</tr>';            
            
        });
        tablaCambios += "</table>";
        content.find("p[data-name='lCambioPrioridad']").append(tablaCambios);
    }
    if (json.Notificaciones != null) {
        var result = "<ul class='hide notis'>";
        var elem = content.find("p[data-name='lNotificacion']");        
        var prefix = '';
        $.each(json.Notificaciones, function (i, item) {
            result += "<li><label class='ui-widget-header'>E-mails: </label><label>";
            $.each(item.mailsAdd, function (j, k) {
                result += prefix + k;
                prefix = ', ';
            });
            result += "</label></li>";
            result += "<li><label class='ui-widget-header'>Fecha: </label><label>" + item.fecha + "</label></li>";
            result += "<li><label class='ui-widget-header'>Mensaje: </label><label>" + item.mensaje + "</label></li>";
            result += "<li><label class='ui-widget-header'>Enviado por: </label><label>" + item.enviadoPor + "</label></li>";
            result += "<li>-------------</li>"
        });
        result += "</ul></li></ul>";
        elem.html('<i class="fa fa-eye cursor verNotificaciones">').append(result);
    } else {
        content.find("p[data-name='lNotificacion']").html("N/A");
    }
}
function toHtmlEquiposeIncidentes(json) {
    var resolucionesIncidente;
    var resolucionesEquipo;
    var equipos = "";
    var areasXIncidente = "";
    $.each(json.Areas, function (i, item) {
        resolucionesIncidente = "<table class='hide info'><tr><th>Fecha</th><th>Técnico</th><th>T.Resolución</th><th>Equipo</th><th>Trabajo</th><th>Archivo</th></tr>";
        //if (item.Resoluciones.length > 0) {
            //$.each(item.Resoluciones, function (i, item2) {
        if (item.AccionXArea.length > 0) {
            $.each(item.AccionXArea, function (i, item2) {
                if (item2.TipoAccion == 1)
                {
                    resolucionesIncidente += '<tr><td>' + item2.FechaIString + '<br>-<br>' + item2.FechaFString + '</td>' + '<td>' + item2.TecnicoNombre + '</td>' +
                        '<td>' + item2.Estado + '</td>' + '<td></td>' + '<td></td><td>' +
                        (item2.PathImagen != null ? '<a target="_blank" rel="noopener noreferrer" href="' + item2.PathImagen +
                            '"><i class="fa fa-file cursor"> ' : ' <i class="fa fa-eye-slash">')
                        + '</td></tr>';
                }
                else if (item2.TipoAccion == 2)
                {
                    resolucionesIncidente += '<tr><td>' + item2.FechaIString + '</td>' + '<td>' + item2.Tecnico + '</td>' +
                        '<td>' + item2.TipoResolucion + '</td>' + '<td>' + item2.Equipo + '</td>' + '<td>' + item2.Observaciones + '</td><td>' +
                        (item2.PathImagen != null ? '<a target="_blank" rel="noopener noreferrer" href="' + item2.PathImagen +
                        '"><i class="fa fa-file cursor"> ' : ' <i class="fa fa-eye-slash">')
                        + '</td></tr>';
                }
            })
        }
        resolucionesIncidente += "</table>";

        areasXIncidente += '<tr><td>' + item.Direccion + '</td><td>' + item.Area.NombreArea + '</td>' + '<td>' + item.FechaDesdeShStr + '</td>' + '<td>' + item.FechaHastaShStr + '</td>'
                        + '<td>' + item.UsuarioQEnvio + '</td><td class="tObs">' + resolucionesIncidente + '</td></tr>';
    });
    $("#areasIncidente tbody").html(areasXIncidente);
}
function buscarHistoriaEquipo() {    
    var text = $('#tBuscarEquipo').val();
    $.ajax({
        url: "/Service/getHistoricoEquipo",
        type: 'POST',
        data: 'oblea=' + text,
        dataType: 'json',
        beforeSend: function () {
            var selectedPanel = $('#pHistEquipo');
            var sbar = selectedPanel.find('div.searchBar');
            $('<div style="width:50px;margin:0 auto" id="spin"><img src="/Content/img/ajax-loader.gif"/></div>').insertAfter(sbar);
        },
        success: function (data) {            
            var json = data;
            var content = $("#equipoContent");
            if (data.Incidentes.length==0) {
                mostrarError("No hay tickets relacionados con ese equipo!!");
                content.hide();
            }
            else {
                content.show();
                equiposToHTML(json);
                content.css('display', 'inline');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            $('div#spin').remove();
        }
    });
}
function getListadoEquipoHistorico(request, response) {
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Service/GetEquipoHistoricoPorNro",
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
function equiposToHTML(json) {
    var content = $("#equipoContent").find('div.result');
    var div = $('<div class="container">');
    var innerDiv = $('<div>');
    var ecD = $('<div class="cld">')
    ecD.append('<div><label>Nombre del Elem. de Conf.:</label>' + json.Oblea + '</div>');
    ecD.append('<div><label>Tipo de Elem. Conf.:</label>' + json.Tipo + '</div>');    

    innerDiv.append(ecD);
    innerDiv.append('<p>Tickets</p>');

    $.each(json.Incidentes, function (i, v) {
        var nestedDiv = $('<div class="g">');
        nestedDiv.append('<div><a href="JavaScript:void(0)" class="verIncidente">' + v.Numero + '</a></div>');
        nestedDiv.append('<div><label>Fecha Inicio:</label>' + v.FechaInicioString + '</div>');
        nestedDiv.append('<div><label>Jurisdicción:</label>' + v.Jurisdiccion + '</div>');
        nestedDiv.append('<div><label>Dependencia:</label>' + v.Dependencia + '</div>');
        innerDiv.append(nestedDiv);
    });
    div.append(innerDiv);
    content.html(div);  
}
function clientesToHTML(json) {
    var content = $("#clientesContent").find('div.result');
    var div = $('<div class="container">');
    var innerDiv = $('<div>');
    var clienteD = $('<div class="cld">')   
    clienteD.append('<div><label>Nombre:</label>' + json.Nombre + '</div>');
    clienteD.append('<div><label>CUIL:</label>' + (json.Dni != null ? json.Dni : "No cargado") + '</div>');
    clienteD.append('<div><label>Dirección:</label>' + (json.Direccion != null ? json.Direccion : "No cargado") + '</div>');
    clienteD.append('<div><label>Email:</label>' + (json.Email != null ? json.Email : "No cargado") + '</div>');
    clienteD.append('<div><label>Jurisdicción:</label>' + (json.Jurisdiccion != null ? json.Jurisdiccion : "No cargado") + '</div>');
    clienteD.append('<div><label>Dependencia:</label>' + (json.Dependencia != null ? json.Dependencia : "No cargado") + '</div>');

    innerDiv.append(clienteD);
    innerDiv.append('<p>Tickets</p>');
    
    $.each(json.Incidentes, function (i, v) {
        var nestedDiv = $('<div class="g">');
        nestedDiv.append('<div><a href="JavaScript:void(0)" class="verIncidente">' + v.Numero + '</a></div>');
        nestedDiv.append('<div><label>Fecha Inicio:</label>' + v.FechaInicioString + '</div>');
        nestedDiv.append('<div><label>Descripción:</label>' + v.Descripcion + '</div>');
        //nestedDiv.append('<div><label>Dependencia:</label>' + v.Dependencia + '</div>');        
        innerDiv.append(nestedDiv);
    });
    div.append(innerDiv);
    content.html(div);
}
function otrosToHTML(json) {
    var content = $("#busquedaContent").find('div.result');
    var div = $('<div class="container">');
    var innerDiv = $('<div>');
    var itemsxpage = 10;
    var total = json.length;
    var pages = Math.ceil(total / itemsxpage);
    $.each(json, function (i, v) {
        var nestedDiv = $('<div class="g">');
        nestedDiv.append('<div><a href="JavaScript:void(0)" class="verIncidente">' + v.Numero + '</a></div>');
        nestedDiv.append('<div><label>Fecha Inicio:</label>' + v.FechaInicioString + '</div>');
        nestedDiv.append('<div><label>Cliente:</label>' + v.Cliente + '</div>');
        nestedDiv.append('<div><label>Estado del Ticket:</label>' + v.EstadoTicket + '</div>');
        nestedDiv.append('<div><label>Jurisdicción:</label>' + v.Jurisdiccion + '</div>');
        nestedDiv.append('<div><label>Dependencia:</label>' + v.Dependencia + '</div>');
        nestedDiv.append('<div class="ellipsis"><label>Descripción:</label>' + v.Descripcion + '</div>');        
        innerDiv.append(nestedDiv);
    });
    div.append(innerDiv);
    content.html(div);
}
/**********************
 FUNCIONES ESTADISTICAS
***********************/
function AddFiltros(context) {
    var apButon = $(context).find('.applyFilterButton');
    var filterPane = $(context).find('.filterPane');
    var filterArea = $('select.mainSel option:selected', context).val();
    var filterItem = $('<div>')
        .addClass('filterItem')
        .data('suffix', '.' + (filterCount++));
    $('div.template.filterChooser')
        .children().clone().appendTo(filterItem)
        .trigger('adjustName');
    filterItem.appendTo(filterPane);
}
function aplicarFiltros() {
    var context = $(this).parents('div.ui-tabs-panel');
    var url;
    switch (context.attr('id')) {
        case "pTickets": url = '/Service/GetDatosTicket';
            break;
        default: url = 'Historicos/GetDatos';
            break;
    }
    $('div.error').hide('fast');
    var areaSelected = $('select.select.mainSel option:selected', context).val();
    if (areaSelected == '0') {
        $('.message', context).html('Seleccione el área deseada');
        $('div.error', context).show('fast');
        return;
    }
    var datos = {};
    var flag = true;
    var elems = $('.filterPane', context).find('.qualifier').not('.notform');
    $.each(elems, function (i, v) {
        $(v).removeClass('error');
        var attrib;
        var important = $(v).attr('data-important');
        if (important) atrib = important;
        else atrib = $(v).attr('data-qualifier');
        var valu;
        if (v.type == 'checkbox') {
            if (v.checked) valu = "on";
            else valu = "off";
        } else {
            valu = $(v).val();
        }
        if (valu == "") {
            $(v).addClass('error padding0');
            flag = false;
        }
        //if (valu == '0' || valu == "") {
        //    $(v).addClass('error padding0');
        //    flag = false;
        //}
        datos[atrib] = valu;
    });
    if (flag) {
        $.ajax({
            beforeSend: function () {
                var selectedPanel = $(".resultsPane", context);
                selectedPanel.html('<div style="width:50px;margin:0 auto"><img src="/Content/img/ajax-loader.gif"/></div>');
            },
            url: url,
            data: datos,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                else {
                    if (data.Info == "ok") {
                        if (data.Detail == "") {
                            $(".resultsPane", context).html("Sin resultados");
                        } else $(".resultsPane", context).html(data.Detail);
                    }
                    else mostrarError(data.Detail);
                }

            },
            complete: function () {

            },
            error: function (data, thrown) {
                mostrarError(data.responseText);
            }
        });
    } else {
        mostrarError("Verifique los errores");
    }
}

