﻿var index;
var $tabs;
var readOnly;
$(function () {    
    //-->Usuarios
    $('body').on('click', '#nvoUsuario',function () {
        $('#nvoUsuarioDialog').dialog('open');
    });
    $('#tabset').on('click', 'div.password', function () {
        var tr = $(this).parents('tr');
        var nro = tr.find('td:eq(0)').text();
        var nombre = tr.find('td:eq(1)').text();
        var titulo = $("#PasswordChanger").siblings('.ui-dialog-titlebar').find('span');
        titulo.html('Cambio de Password para: ' + nombre);
        $("#PasswordChanger").data('datos', { 'nro': nro, 'nombre': nombre }).dialog('open');
    });
    $('#tabset').on('click', 'div.perfil', function () {
        var perfiles = $(this).attr('data-perfiles');
        var idUsuario = $(this).parents('tr').find('td:eq(0)').text();
        $("#ProfileChanger").data({ 'perfiles': perfiles, 'idUsuario': idUsuario }).dialog('open');
    });
    $('#tabset').on('click', 'div.user', function () {
        var tr = $(this).parents('tr');
        var td = tr.find('td').has('div.user');
        var div = td.find('div.user');
        var idUsuario = tr.find('td:eq(0)').text();
        $.ajax({
            url: '/Config/toggleStatus',
            type: 'post',
            data: { 'idUsuario': idUsuario },
            success: function (data) {
                if (data.Info == "Login") { mostrarFinSesion(); }
                if (data.Info == 'ok') {
                    var estado = data.Detail;
                    var clase;
                    estado == 'Habilitado' ? clase = 'userOk' : clase = 'userNok';
                    div.removeClass('userOk userNok').addClass(clase);
                    tr.find('td:eq(3)').html(estado);
                }
                else mostrarError(data.Detail);
            }
        });
    });
    $('body').on('click', '#bAgregarPerfil', function () {
        var perfil = $('#sPerfiles').val();
        var id = $(this).parents('div#ProfileChanger').data('idUsuario');
        $.ajax({
            url: '/Config/AgregarPerfilAUsuario',
            data: { 'idUsuario': id, 'perfil': perfil },
            type: 'post',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    var td = $('<td>').append(data.Detail);
                    var tdQuitar = $('<td>').append('<div class="icon trash profile">');
                    var tr = $('<tr>').append(td).append(tdQuitar).appendTo('tbody', '#tPerfiles');
                    reloadCurrentTab();
                    $('p.ajaxGif').hide();
                }
                else mostrarError(data.Detail);
            },
            error: function (e, v, h) {
                mostrarError(h);
            }
        });
    });
    $('body').on('click', '#tPerfiles div.trash', function () {
        var id = $(this).parents('div#ProfileChanger').data('idUsuario');
        var perfil = $(this).parent().prev().html();
        $.ajax({
            url: '/Config/quitarPerfilAlUsuario',
            data: { 'idUsuario': id, 'perfil': perfil },
            type: 'post',
            success: function (data) {
                if (data.Info == "Login") { mostrarFinSesion(); }
                if (data.Info == 'ok') {
                    $("#ProfileChanger").dialog('close');
                    mostrarExito("Se ha quitado el perfil.");
                    reloadCurrentTab();
                    $('p.ajaxGif').hide();
                }
                else mostrarError(data.Detail);
            },
            error: function (e, v, h) {
                mostrarError(h);
            }
        });
    });
    //Usuarios<--
    //-->Datos Personales
    //file uploads
    //image changer
    $('body').on('change', '.uploadPhoto', function () {
        var _this = this;
        if (this.disabled) {
            mostrarError('El Navegador no soporta carga de imágenes!', 'error');
            return;
        }
        var file = this.files[0];
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function (_file) {
            image.src = _file.target.result;
            image.onload = function () {
                var w = this.width,
                    h = this.height,
                    t = file.type,
                    n = file.name,
                    s = ~~(file.size / 1024) + 'KB';
                if (file.size / 1024 > 5000) {
                    mostrarError('El tamaño máximo de la imágen es de 5MB. El actual es: ' + s);
                    clearInputImage(_this);
                } else {
                    //nombre en el label
                    var boxpadre = $(_this).parent();
                    $(_this).siblings('label').contents().filter(function () {
                        return this.nodeType == 3;
                    })[0].nodeValue = n;
                    boxpadre.find('i.clearIF').remove();
                    var labelR = $('<i class="fa fa-times clearIF cursor" aria-hidden="true"></i>');
                    boxpadre.append(labelR);
                    //image on avatar
                    
                    $('img.artDisplay').attr("src", this.src);                  
                }
                //_this.disabled = "disabled";
            };
            image.onerror = function () {
                mostrarError('Tipo de archivo no admitido: ' + file.type, 'error');
                clearInputImage(_this);
            };
        };
    });
    $('body').on('click', '.clearIF', function () {
        var iF = $(this).siblings('input[type="file"]')[0];
        clearInputImage(iF);
    });
    $('#image').on('click', 'div.removeImagen', removeImagen);
    function clearInputImage(_this) {
        if (_this != undefined) {
            var fileInput = document.createElement('input');
            fileInput.setAttribute('id', 'upPhRes');
            fileInput.setAttribute('type', 'file');
            fileInput.setAttribute('class', 'uploadPhoto');
            _this.parentNode.replaceChild(fileInput, _this);
            _this.removeAttribute("disabled");
            $(fileInput).siblings('label').contents().filter(function () {
                return this.nodeType == 3;
            })[0].nodeValue = "Imágen";
            var pare = $(fileInput).siblings('i.clearIF');
            pare.remove();
            $('img.artDisplay').attr("src", "#"); 
        }
    }
    function removeImagen() {

    }
    //end image upload
    //<--Datos Personales
    $('body').on('change', '#verTipifP', verTipifP);
    $('body').on('click', '#bNvaTipif', nuevaTipif);
    $('body').on('click', '#bNvoServicio', nuevoServicio);
    $('body').on('change', '.cSubTipo', mostrarSubTipificacion);
    $('body').on('click', '#tipifExistente .trash', dropTipificacion);
    $('body').on('click', '#servicios .trash', dropServicio);
    $('body').on('click', '#bEditServicio', editarServicio);
    $('body').on('change', '.sPadres', mostrarSubTipBySel);
    $('body').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover').css('cursor','pointer')
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover')
        },
        click: function (e) {
            //if (e.target != this) return;
            var context = $('.nuevoServicio');
            if ($(this).hasClass('ui-state-highlight'))
            {
                $(this).removeClass('ui-state-highlight');
                $('input', context).val('');
                $('.bNvo', context).show();
                $('.bEdit', context).hide();
            } else {
                $(this).addClass('ui-state-highlight');
                var id=$('span.id',this).html();
                $.ajax({
                    url: '/Config/getServicio',
                    type: 'post',
                    data: { 'id': id },
                    success: function (data) {
                        if (data.Info == "Login") {
                            mostrarFinSesion();
                            return;
                        }
                        $('#tNombreServicio').val(data.Nombre);
                        $('#tDiaServicio').val(data.Dias);
                        $('#tHoraServicio').val(data.Horas);
                        $('#tDiaMargen').val(data.DiasMargen);
                        $('#tHoraMargen').val(data.HorasMargen);
                        $('#idServicio').html(data.Id);
                        var check = $('#habilitacion');
                        if (data.Vigente) {
                            check.prop('checked', true);
                        } else {
                            check.prop('checked',false)
                        }
                    }
                });
                $('.bNvo', context).hide();
                $('.bEdit', context).show();
            }
            
        }
    },'div.rowS');
    $('#tOblea').on('keyup', function () {
        $(this).val($(this).val().toUpperCase())
    });
    index = getParameterByName('index');
    $tabs = $('#tabset').tabs({
        activate: function (e, u) {

        },
        load: function (event, ui) {
            var panel = $(ui.panel).find('div#userPane');
            if (panel.length > 0) { bindUsuarios(event, ui); return;}
            panel = $(ui.panel).find('div#wrapperJurisdicciones');
            if (panel.length > 0) { bindJurisdicciones(event, ui); return; }
            panel = $(ui.panel).find('input#idTemaPers');
            if (panel.length > 0) { bindDatosPers(event, ui); return; }
            panel = $(ui.panel).find('div#Autorizantes');
            if (panel.length > 0) { bindAut(); return; }
        },//fin load
        beforeLoad: function (event, ui) {
            var data;
            ui.jqXHR.success(function (d) {
                data = d;
                if (data.Info == "Login") { mostrarFinSesion() }
            });
            var selectedPanel = ui.panel;
            selectedPanel.html('<div style="width:50px;margin:0 auto"><img src="/Content/img/ajax-loader.gif"/></div>');

        }
    });   
    setTab();
    $('#bDelEquipo').on('click', deleteEquipo);
    $('#tabset').on('click', '#bSetTimes', actualizarTiempos);   
});//READY
function setTab() {
    $tabs.tabs("option", "active", index == "" ? 0 : parseInt(index)); //index == "" ? 1 :
}
function loadTab(index) {
    $tabs.tabs('load', index);
}
function reloadCurrentTab() {
    //var dialogs = $('.ui-dialog-content').dialog('destroy');
    var selected = $('#tabset').tabs('option', 'active');
    $("#tabset").tabs('load', selected);
}
function verTipifP() {
    var checkbox = $(this);
    var checked = checkbox.attr('checked');
    var $liActual = checkbox.parents('li');
    var divTemplates = $('#Tipificaciones div.templates');
    var divPanel = $(checkbox).parents('div');
    if (checked == 'checked') {
        var template = $('<li>').append(divTemplates.find('.template1').clone().removeClass('hide template1')).addClass('add last');
        template.insertAfter($liActual);
    } else {
        divPanel.find('.add').remove();
    }
}
function mostrarSubTipBySel() {
    var checkbox = $(this).siblings('input[type="checkbox"]');
    var checked = checkbox.attr('checked');
    if (checked == 'checked') {
        var div = $(this).parents('div.section');
        var idPadre = $(this).val();
        var $liActual = $(this).parents('li');
        var $liToRemove = $($liActual).nextAll('.add');
        $liToRemove.remove();
        $liActual.addClass('last');
        var result = getTipificacionesHijo(idPadre);
        if (result.length == 0) {

        } else {
            template = $('<li>').append(div.find('.template').clone().removeClass('hide template')).addClass('add last');
            $selectSubRubro = template.find('select');
            $.each(result, function (i, v) {
                $selectSubRubro.append($('<option></option>').attr('value', v.idTipificacion).text(v.Nombre))
            })
            $liActual.removeClass('last');
            template.insertAfter($liActual);
        }
    }
}
function mostrarSubTipificacion() {
    var checkbox = $(this);
    var checked = checkbox.attr('checked');
    var div = checkbox.parents('div.section');
    //var esPadre = checkbox.hasClass('padre');
    var $liActual = checkbox.parents('li');
    var $liAdded = div.find('li.add');
    var $nextLi = $liActual.next('li.add');
    var $prevLi = $liActual.prev('li');
    var idPadre = checkbox.siblings('select').val();
    var $mjeError = div.find('.mje.error');
    $mjeError.html('').hide();
    var template;
    var $selectSubRubro;
    var result;
    if (checked == 'checked') {
        result = getTipificacionesHijo(idPadre);
        if (result.length == 0) {
            mostrarError('Esta Tipificación no tiene subtipos', 'error');
            checkbox.removeAttr('checked');
        } else {
            template = $('<li>').append(div.find('.template').clone().removeClass('hide template')).addClass('add last');
            $selectSubRubro = template.find('select');
            $.each(result, function (i, v) {
                $selectSubRubro.append($('<option></option>').attr('value', v.idTipificacion).text(v.Nombre))
            })
            $liActual.removeClass('last');
            template.insertAfter($liActual);
        }
    }
    else {
        $liToRemove = $($liActual).nextAll('.add');
        $liToRemove.remove();
        $liActual.addClass('last');
    }
}
function getTipificacionesHijo(id) {
    var result;
    $.ajax({
        url: '/Config/getTipificacionesHijo',
        type: 'POST',
        dataType: "json",
        data: { 'id': id },
        async: false,
        success: function (com) {
            result = com;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError("Error obteniendo Tipificaciones: " + thrownError.toString(), 'error');
        }
    });
    return result;
}
function dropTipificacion() {
    var $tr = $(this).parents('div.rowT');
    var $id = $tr.find('span.id').html();
    var $mjeInfo = $(this).parents('div.section').find('.mje.info');
    var $mjeError = $(this).parents('div.section').find('.mje.error');
    $('.mje').html('').hide();
    $.ajax({
        url: 'Config/quitarTipificacion',
        type: 'POST',
        dataType: "json",
        data: { 'id': $id },
        success: function (com) {
            if (com.Info == 'Login') { mostrarFinSession(); return;}
            if (com.Info == 'ok') {
                updateTipif();
                mostrarExito("Tipificación quitada", 'success');
            } else {
                mostrarError(com.Detail, 'error');
                return;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError.toString(), 'error');
        },
        complete: function () {
            $('p.ajaxGif').hide();
        }
    });
}
function dropServicio(e) {
    e.stopPropagation();
    var $tr = $(this).parents('div.rowS');
    var $id = $tr.find('span.id').html();    
    
    $.ajax({
        url: 'Config/quitarServicio',
        type: 'POST',
        dataType: "json",
        data: { 'id': $id },
        success: function (com) {
            if (com.Info == 'Login') { mostrarFinSession(); return; }
            if (com.Info == 'ok') {
                updateTipif();
                mostrarExito("Servicio quitado",'error');
            } else {
                mostrarError(com.Detail, 'error');
                return;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError.toString(), 'error');
        },
        complete: function () {
            $('p.ajaxGif').hide();
        }
    });
}
function editarServicio() {
    var digits = new RegExp("^[0-9]+$");
    var $nombre = $('#tNombreServicio');
    var $horas = $('#tHoraServicio');
    var $dias = $('#tDiaServicio');
    var $diasM = $('#tDiaMargen');
    var $horasM = $('#tHoraMargen');
    $nombre.removeClass('denied');
    $horas.removeClass('denied');
    $dias.removeClass('denied');
    $horasM.removeClass('denied');
    $diasM.removeClass('denied');
    var $container = $('.container');
    var $mjeInfo = $container.find('.mje.info');
    var $mjeError = $container.find('.mje.error');
    //var $rubroPadre = $('#dRubroPadre');
    var nombre = $nombre.val();
    var hora = $horas.val();
    var dias = $dias.val();
    if ($nombre.val().length < 2) {
        $nombre.addClass('denied');
        mostrarError("Ingrese el Nombre del Servicio", 'error');
        return;
    }
    var valorDia = $dias.val();
    if (valorDia.length < 1 | !digits.test(valorDia)) {
        $dias.addClass('denied');
        mostrarError("Ingrese los días. (0 si corresponde)", 'error');
        return;
    }
    if ($horas.val().length < 1 | !digits.test($horas.val())) {
        $horas.addClass('denied');
        mostrarError("Ingrese la hora. (0 si corresponde)", 'error');
        return;
    }
    var valorDiaM = $diasM.val();
    if (valorDiaM.length < 1 | !digits.test(valorDiaM)) {
        $diasM.addClass('denied');
        mostrarError("Ingrese los días de Márgen. (0 si corresponde)", 'error');
        return;
    }
    if ($horasM.val().length < 1 | !digits.test($horasM.val())) {
        $horasM.addClass('denied');
        mostrarError("Ingrese la hora de Márgen. (0 si corresponde)", 'error');
        return;
    }
    $.ajax({
        url: '/Config/EditServicio',
        type: 'POST',
        dataType: "json",
        data: {'nombre': $nombre.val(), 'dias': $dias.val(), 'horas': $horas.val(), 'diasM': $diasM.val(), 'horasM': $horasM.val(), 'id': $('#idServicio').html(), 'habilitado':$('#habilitacion').prop('checked')},
        success: function (com) {
            if (com.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (com.Info == 'ok') {
                mostrarExito("Servicio actualizado.", 'success');
                updateTipif();
            } else {
                mostrarError(com.Detail, 'error');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError, 'error');
        },
        complete: function () {
            $('p.ajaxGif').hide();
        }
    });
}
function nuevoServicio() {
    var digits = new RegExp("^[0-9]+$");
    var $nombre = $('#tNombreServicio');
    var $horas = $('#tHoraServicio');    
    var $dias = $('#tDiaServicio');
    var $diasM = $('#tDiaMargen');
    var $horasM = $('#tHoraMargen');
    $nombre.removeClass('denied');
    $horas.removeClass('denied');
    $dias.removeClass('denied');
    $horasM.removeClass('denied');
    $diasM.removeClass('denied');
    var $container = $('.container');
    var $mjeInfo = $container.find('.mje.info');
    var $mjeError = $container.find('.mje.error');
    //var $rubroPadre = $('#dRubroPadre');
    var nombre = $nombre.val();
    var hora = $horas.val();
    var dias = $dias.val();
    if ($nombre.val().length < 2) {
        $nombre.addClass('denied');
        mostrarError("Ingrese el Nombre del Servicio", 'error');
        return;
    }
    var valorDia = $dias.val();
    if (valorDia.length < 1 | !digits.test(valorDia)) {
        $dias.addClass('denied');
        mostrarError("Ingrese los días. (0 si corresponde)", 'error');
        return;
    }
    if ($horas.val().length < 1 | !digits.test($horas.val())) {
        $horas.addClass('denied');
        mostrarError("Ingrese la hora. (0 si corresponde)", 'error');
        return;
    }
    var valorDiaM = $diasM.val();
    if (valorDiaM.length < 1 | !digits.test(valorDiaM)) {
        $diasM.addClass('denied');
        mostrarError("Ingrese los días de Márgen. (0 si corresponde)", 'error');
        return;
    }
    if ($horasM.val().length < 1 | !digits.test($horasM.val())) {
        $horasM.addClass('denied');
        mostrarError("Ingrese la hora de Márgen. (0 si corresponde)", 'error');
        return;
    }
    $.ajax({
        url: 'Config/NuevoServicio',
        type: 'POST',
        dataType: "json",
        data: { 'nombre': $nombre.val(), 'dias': $dias.val(), 'horas': $horas.val(), 'diasM': $diasM.val(), 'horasM': $horasM.val() },
        success: function (com) {
            if (com.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (com.Info == 'ok') {
                mostrarExito("Servicio agregado", 'success');
                updateTipif();
            } else {
                mostrarError(com.Detail, 'error');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError, 'error');
        },
        complete: function () {
            $('p.ajaxGif').hide();
        }
    });
    
}
function nuevaTipif() {    
    var context = $(this).parents('ul.nuevaTipificacion');
    var $nombre = $('#tNombreNvaTipif');
    var $descripcion = $('#tDescNvaTipif');
    var $servAsoc = $('.dServicio', context);
    var tienePadre = $('#verTipifP').attr('checked');    
    var padre = null;
    $nombre.removeClass('denied');
    if (tienePadre == 'checked') {
        var last = $('.last', context);
        padre = $('select', last).val();
    }    
    if ($nombre.val().length < 2) {
        $nombre.addClass('denied');
        mostrarError("Ingrese el Nombre de la Tipificación", 'error');
        return;
    }
    if ($servAsoc.val() == 0 & tienePadre == 'checked') {
        mostrarError("Seleccione el Servicio Asociado", 'error');
        return;
    }
        $.ajax({
            url: 'Config/nvaTipificacion',
            type: 'POST',
            dataType: "json",
            data: { 'nombre': $nombre.val(), 'desc': $descripcion.val(), 'idPadre': padre,'idServicio':$servAsoc.val() },
            success: function (com) {
                if (com.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (com.Info == 'ok') {
                    mostrarExito("Tipificación agregada",'success');
                    updateTipif();
                } else {
                    mostrarError(com.Detail, 'error');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError.toString());
            },
            complete: function () {
                $('p.ajaxGif').hide();
            }
        });
    
}
function updateTipif() {
    var url = window.location.href;    
    $.ajax({
        url: url+'/Tipificaciones',
        type: 'GET',
        success: function (data) {
            var $fieldTipif = $(data).find('fieldset#tipifExistente');
            var $options = $(data).find('select.dTipificacionesPadre', 'div.template1').find('option');
            var $optionsServ = $(data).find('select.dServicio').find('option');
            var $fieldServ = $(data).find('fieldset#servicios');
            $('div.showing div.fieldsetContainerLeft').html($fieldTipif);
            $('div.showing div.fieldsetContainerRight').html($fieldServ);
            $('select.dTipificacionesPadre', 'div.template1').html($options);
            $('.dServicio').html($optionsServ).find('option[value=0]').attr('selected','selected');
            $('input', '.nuevoServicio').val('');
            $('input', '.nuevaTipificacion').val('');
            $('.bNvo', '.nuevoServicio').show();
            $('.bEdit', '.nuevoServicio').hide();
        }
    });
}