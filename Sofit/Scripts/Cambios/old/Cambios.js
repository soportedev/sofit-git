﻿var Nice = false; // Objeto para niceScroll

$(function () {
	// NiceScroll
	window._selfNice = $('.container'); // Contenedor para niceScroll
	initNiceScroll(_selfNice); // Ejecuta niceSroll al inicio

	_selfNice.mouseenter(function(){ // Activa niceScroll cuando el mouse entra en el contenedor
		initNiceScroll(_selfNice);
	}).mouseleave(function(){ // Desactiva niceScroll cuando el mouse sale del contenedor
		Nice = false;
	});

	// Formulario de nueva gestion de cambio
	window.NGForm = $('#fNGCambios'); // Guarda el form en una variable

	// Tabs
	var tabElement = document.getElementById('tab');
	var optionsTab = {
        onShow: function (e, v) {
            $('#schedule').fullCalendar('render');
		}
	}
	var globalTab = M.Tabs.init(tabElement, optionsTab);

	// Activa calendario
    $('#schedule').fullCalendar({
        defaultView: 'month',
        height: "parent",
        header: {
            center:'month, agendaWeek'
        },
        views: {
            month: {

            }
        },
        locale: 'es',
        events: function (start, end, timezone, callback)
        {
            $.ajax({
                url: '/Cambios/GetCalendarEvents',
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    var events = [];
                    $.each(result, function (i, v) {
                        events.push(
                            {
                                title: v.Asunto,
                                descripcion: v.Asunto,
                                start: moment(v.StartDate).format('YYYY-MM-DD HH:mm'),
                                end: moment(v.EndDate).format('YYYY-MM-DD HH:mm'),
                                backgroundColor: '#9501fc',
                                borderColor: '#fc0101'
                            }
                        );
                    });
                    callback(events);
                }
            });
        },
        eventRender: function (event, element) {
            //element.qtip({
            //    content: event.description
            //});
        },
        editable:false
    });    
	//mensaje error
	var errorMess = $('#errorMessage').val();
	if (errorMess.length > 0) LoginError(errorMess);
	//-->fin mensaje

	//signalr
	var cc = $.connection.adUser;
	$.connection.hub.qs = { 'usuario': $('#nombreUsuario').val() };
	//create a function the hub can call
	//cc.client.disconnect = function () {
	//    window.location = '/Login';
	//};
	//$.connection.hub.qs = { 'idUsuario': $('#idUser').val() };

	//collapsible
	var optionsCollapsible = {
		onOpenStart: function (e) {
		},
		onOpenEnd: function(e) { // Activa niceScroll cuando se abre un colapsable
			initNiceScroll(_selfNice);
		},
		onCloseStart: function(e) { // Desactiva niceScroll cuando se cierra un colapsable
			Nice = false;
		}
	};
	var elemsMisCambios = document.getElementById('ulMisCambios');
	var miscambiosCol = M.Collapsible.init(elemsMisCambios, optionsCollapsible);    
	//fin collapsible

	$.connection.hub.start().done(function () {
	});

	// Iniciando el form
	$('select').formSelect();
	$('.datepicker').datepicker();
	$('.timepicker').timepicker();
	$('.contador').characterCounter();
	$('.tooltipped').tooltip();
	$('#EditDesc').hide();
	$('#DivNotas').hide();

	// Iniciando Modal
	$('#modalConfirm').modal();

	// Autocomplete
	var optionsHost = {
		limit: 5,
		data: { 'Seleccione': null },
		onAutocomplete: function (valor) {
			var _thisF = $(this); // Guarda el objeto de la funcion
			var inputEl = _thisF[0].$el[0]; // Guarda el input
			$(inputEl).attr('data-valid', 1); // Confirma item valido

			var selec = valor;
			$.ajax({
				type: "POST",
				//contentType: "application/json; charset=utf-8",
				url: "/Cambios/Relaciones",
				data: { oblea: selec },
				dataType: "json",
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						MostrarRelaciones(data);
					}
				}
			});
		}
	};
	var autoHostOrigen = M.Autocomplete.init(document.getElementById('idEc'), optionsHost);
	$('#idEc').on('keyup', function (e) {
		var ter = $(this).val();
		var id = $(this).attr('id');
		if (ter.length > 2) {
			var term = '{ prefixText: "' + ter + '"}';
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "/Service/EquipoXOblea",
				data: term,
				dataType: "json",
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						var obj = {};
						$.each(data, function (i, val) {                           
								obj[val.Oblea] = null;
								
						});
						
						autoHostOrigen.updateData(obj);
						autoHostOrigen.open();
					}
				}
			});
		}
	});

	// Validar campos
	$.validator.setDefaults({
		ignore: []
	});
	$.validator.addMethod('autoCvalid', function (value, element) {
		var lis = $(element).data('valid');
		if (lis == 1) return true;
		else return false;
	});
	var fGestionCambios = $('#fNGCambios').validate({
		rules: {
			idTipo: {
				required: true
			},
			asunto: {
				required: true,
				minlength: 20
			},
			descripcion: {
				required: true,
				minlength: 150
			},
			idEc: {
				required: true,
				minlength: 7,
				autoCvalid: true
			},
			autorizante: {
				required: true
			},
			prioridad: {
				required: true
			},
			fechaInicioEstimada: {
				required: true
			},
			horaInicioEstimada: {
				required: true
			},
			razon: {
				required: true,
				minlength: 100
			},
			resultados: {
				required: true,
				minlength: 100
			},
			riesgos: {
				required: true,
				minlength: 100
			},
			rollback: {
				required: true,
				minlength: 100
			},
			recursos: {
				required: true
			},
			serviciosAfectados: {
				required: true
			},
			// conclusion: {
			// 	required: true,
			// 	minlength: 100
			// }

		},
		submitHandler: function (form) {
			var flag = true;
			if (flag) {
				document.body.onbeforeunload = function () {
					return null;
				}
				form.submit();
			}
		},
		errorClass: "invalid",
		messages: {
			idTipo: {
				required: "Seleccione el tipo de solicitud"
			},
			asunto: {
				required: "Ingrese el titulo",
				minlength: "El titulo debe tener al menos 20 caracteres"
			},
			descripcion: {
				required: "Describa detalladamente la gestion de cambio",
				minlength: "La descripcion debe tener al menos 250 caracteres"
			},
			idEc: {
				required: "Seleccione el elemento de configuración afectado",
				minlength: "Debe SELECCIONAR un elemento",
				autoCvalid: "Debe SELECCIONAR un elemento"
			},
			autorizante: {
				required: "Seleccione el responsable autorizante"
			},
			prioridad: {
				required: "Seleccione la prioridad"
			},
			fechaInicioEstimada: {
				required: "Seleccione la fecha de inicio"
			},
			horaInicioEstimada: {
				required: "Seleccione la hora de inicio"
			},
			razon: {
				required: "Describa las razones de la solicitud del cambio",
				minlength: "La razon debe tener al menos 100 caracteres"
			},
			resultados: {
				required: "Describa los resultados esperados",
				minlength: "La razon debe tener al menos 100 caracteres"
			},
			riesgos: {
				required: "Describa los riesgos posibles",
				minlength: "El riesgo debe tener al menos 100 caracteres"
			},
			rollback: {
				required: "Describa las tareas de prevencion posibles",
				minlength: "La descripción de rollback debe tener al menos 100 caracteres"
			},
			recursos: {
				required: "Ingrese los recursos necesarios"
			},
			serviciosAfectados: {
				required: "Ingrese los servicios afectados"
			},
			conclusion: {
				required: "Ingrese la conclusión",
				minlength: "La conclusión debe tener al menos 100 caracteres"
			}
		},
		errorElement: 'div',
		errorPlacement: function (error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}

	});

	// BOTONES
	var ActionsBarBtn =  document.querySelectorAll('.btnAction'); // Busca todos los botones en Gestiones de Cambios
	for (var i = 0; i < ActionsBarBtn.length; i++) {
		ActionsBarBtn[i].addEventListener('click', GCambioAcciones); // Listener por cada boton de acción
	}

	var EditarDesc = document.getElementById('btnEditDesc'); // Boton para activar la edición de la descripción
		EditarDesc.addEventListener('click', fEditDesc); // Listener para el boton

	var CancelarForm = document.getElementById('CancelarForm'); // Boton para cancelar formulario
		CancelarForm.addEventListener('click', fCancelarForm);
});

function LoginError(message)
	{
		$('#LoginError span').html(message);
		$('#LoginError').removeClass('hide');
	}

function initNiceScroll(_selfNice) // Crea el niceScroll
	{
		Nice = _selfNice.niceScroll({
		background: "#7CD569",
		cursorcolor: "#24890D",
		cursorwidth: "10px",
		cursorborder: "0",
		cursorborderradius: "2px",
		horizrailenabled: false,
		enabletranslate3d: true,
		railoffset: { top: 0, left: 15 }
		}).resize();
	}

function GCambioAcciones()
	{
		var _elem = $(this); // Guarda el boton clickeado

		// Verifica la accion del boton
		switch (_elem[0].dataset.url)
			{
				case "Cancelar":
					// function()
						var Action = "cancelar";
						
								$.ajax({
									type: "POST",
									dataType: "json",
									data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
									url: '/Cambios/Cancelar',
								}).done(function(ObjCambio){
									
								});
							
                                break;
                case "Cerrar":
                    // function()
                    var Action = "cancelar";

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
                        url: '/Cambios/Cerrar',
                    }).done(function (ObjCambio) {

                    });

                    break;
                case "Implementado":
                    // function()
                    var Action = "";

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
                        url: '/Cambios/Implementado',
                    }).done(function (ObjCambio) {

                    });

                    break;
				case "Comite":
					var Action = "enviar al comité";
					
							$.ajax({
								type: "POST",
								dataType: "json",
								data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
								url: '/Cambios/Comite',
							}).done(function(ObjCambio){
								
							});
						
					break;
				case "Rechazar":
					var Action = "rechazar";
					
							$.ajax({
								type: "POST",
								dataType: "json",
								data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
								url: '/Cambios/Rechazar',
							}).done(function(ObjCambio){
								
							});
						
					break;
				case "Aprobar":
					var Action = "aprobar";
					
							$.ajax({
								type: "POST",
								dataType: "json",
								data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
								url: '/Cambios/Aprobar',
							}).done(function(ObjCambio){
								
							});
						
					break;
				case "Modificar":
					$.ajax({
						type: "POST",
						dataType: "json",
						data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
						url: '/Cambios/getpedido'
					}).done(function(ObjCambio){
						modificarGC(ObjCambio, _elem[0].dataset.idpedido);
					});
					break;
			}
	}

function modificarGC(ObjCambio, idCamio)
	{
		NGForm.find('#idCambio').val(idCamio);
		NGForm.find('#Modo').val("update");
		NGForm.find('#tipocambio').find("option[value="+ObjCambio.Tipo.Id+"]").attr('selected','selected');
		NGForm.find('#asunto').val(ObjCambio.Asunto).attr('disabled', true);
		NGForm.find('#descripcion').val(ObjCambio.Descripcion);
		NGForm.find('#idEc').val(ObjCambio.ElemConf.Nombre).attr('disabled', true);
		NGForm.find('#autorizante').attr('disabled', true).find("option[value="+ObjCambio.Autorizante.id+"]").attr('selected','selected');
		NGForm.find('#prioridad').find("option[value="+ObjCambio.Prioridad.Id+"]").attr('selected','selected');
		NGForm.find('#finicio').val(ObjCambio.FInicioEstimada);
		NGForm.find('#horaInicio').val(ObjCambio.HoraInicioEstimada);
		NGForm.find('#duracionestimada').val(ObjCambio.duracionEstimada);
		NGForm.find('#razon').val(ObjCambio.Razon);
		NGForm.find('#resultados').val(ObjCambio.Resultados);
		NGForm.find('#resultados').val(ObjCambio.Resultados);
		NGForm.find('#riesgos').val(ObjCambio.Riesgos);
		NGForm.find('#rollback').val(ObjCambio.VueltaAtras);
		NGForm.find('#recursos').val(ObjCambio.Recursos);
		NGForm.find('#serviciosafectados').val(ObjCambio.ServiciosAfectados);
		NGForm.find('#DivDesc').hide();
		NGForm.find('#EditDesc').show();
		NGForm.find('#DivNotas').show();

		// Relaciones
		MostrarRelaciones(ObjCambio.ElemsRelacionados);

		// Actualiza el materialize del from
		ActualizarForm();

		// Cambia de pestaña
		var tabs = document.getElementById('tab');
		var instance = M.Tabs.getInstance(tabs);
		instance.select('NCambio');

		// Mover scroll al top
		var ScrollToTop = _selfNice.getNiceScroll(0);
		ScrollToTop.doScrollTop(0, 1000);
	}

function MostrarRelaciones(data)
	{
		var _Relaciones = $('.relaciones');

		_Relaciones.html('');

		$.each(data, function(i, val){
				var aRel = document.createElement('a');
					aRel.className = 'waves-effect waves-light btn-small green darken-2 z-depth-0';

				var textNode = document.createTextNode(val.Oblea);
				var textNodeWhite = document.createTextNode(' ');

				aRel.appendChild(textNode);
				_Relaciones.append(aRel);
				_Relaciones.append(textNodeWhite);
		});
	}

function ActualizarForm()
	{
		M.updateTextFields();

		$('select', NGForm).each(function(){
			$(this).formSelect();
		});

		$('textarea', NGForm).each(function(i){
			var _id = $(this).attr('id');
			M.textareaAutoResize($('#'+_id));
		});
	}

function fEditDesc()
	{
		var NGForm = $('#fNGCambios');

		NGForm.find('#EditDesc').hide(200);
		NGForm.find('#DivDesc').show(200);
	}

function fCancelarForm()
	{
		NGForm.find('#DivDesc').show();
		NGForm.find('#EditDesc').hide();
		NGForm.find('#DivNotas').hide();

		NGForm.on('reset', function(event) {
			// executes before the form has been reset
			setTimeout(function() {
			  // executes after the form has been reset
				ActualizarForm();
				var ScrollToTop = _selfNice.getNiceScroll(0);
				ScrollToTop.doScrollTop(0, 1000);
			}, 1);
		 
		 });
	}

function accionConfirm (action)
	{
		var _confirmModal = $('#modalConfirm');

		_confirmModal.find('#modalAccionVar').html(action);

		var instanceModal = M.Modal.getInstance(_confirmModal);
		instanceModal.open();
	}