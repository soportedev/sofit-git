﻿var Nice = false; // Objeto para niceScroll
var _confirmModal; // Objeto del modal
var accionConfirmObj = {}; // Objeto confirmar/cancelar cambio
var ElemClength; // Objeto para contar EC seleccionado en Nueva Gestion de Cambios
var idforBtnEC = 'EC-n-'; // Variable para generar el nombre del boton de cada EC seleccionado en Nueva Gestion de Cambios
var idforInputEC = 'idEC-n-'; // Variable para generar el nombre del input de cada EC seleccionado en Nueva Gestion de Cambios

$(function () {       
	// NiceScroll
	window._selfNice = $('.container'); // Contenedor para niceScroll
	initNiceScroll(_selfNice); // Ejecuta niceSroll al inicio

	_selfNice.mouseenter(function(){ // Activa niceScroll cuando el mouse entra en el contenedor
		initNiceScroll(_selfNice);
	}).mouseleave(function(){ // Desactiva niceScroll cuando el mouse sale del contenedor
		Nice = false;
	});

	// Formulario de nueva gestion de cambio
	window.NGForm = $('#fNGCambios'); // Guarda el form en una variable
    window.ShowGC = $('#GCambios');
	// Tabs
	var tabElement = document.getElementById('tab');
	var optionsTab = {
        onShow: function (e, v) {
            $('#schedule').fullCalendar('render');
		}
	}
	var globalTab = M.Tabs.init(tabElement, optionsTab);

	// Activa calendario
    $('#schedule').fullCalendar({
        defaultView: 'month',
        height: "parent",
        header: {
            center:'month, agendaWeek'
        },
        views: {
            month: {

            }
        },
        locale: 'es',
        events: function (start, end, timezone, callback)
        {
            $.ajax({
                url: '/Cambios/GetCalendarEvents',
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    var events = [];
                    $.each(result, function (i, v) {
                        events.push(
                            {
                                title: v.Asunto,
                                descripcion: v.Asunto,
                                start: moment(v.StartDate).format('YYYY-MM-DD HH:mm'),
                                end: moment(v.EndDate).format('YYYY-MM-DD HH:mm'),
                                backgroundColor: '#9501fc',
                                borderColor: '#fc0101'
                            }
                        );
                    });
                    callback(events);
                }
            });
        },
        eventRender: function (event, element) {
            //element.qtip({
            //    content: event.description
            //});
        },
        editable:false
    });    
	//mensaje error
	var errorMess = $('#errorMessage').val();
	if (errorMess.length > 0) LogError(errorMess);
	//-->fin mensaje

	//signalr
    //$.connection.hub.start().done(function () {
    //});
	//var cc = $.connection.adUser;
	//$.connection.hub.qs = { 'usuario': $('#nombreUsuario').val() };
	//create a function the hub can call
	//cc.client.disconnect = function () {
	//    window.location = '/Login';
	//};
	//$.connection.hub.qs = { 'idUsuario': $('#idUser').val() };

	//collapsible
	var optionsCollapsible = {
		onOpenStart: function (e) {
		},
		onOpenEnd: function(e) { // Activa niceScroll cuando se abre un colapsable
			initNiceScroll(_selfNice);
		},
		onCloseStart: function(e) { // Desactiva niceScroll cuando se cierra un colapsable
			Nice = false;
		}
	};
	var elemsMisCambios = document.getElementById('ulMisCambios');
	var miscambiosCol = M.Collapsible.init(elemsMisCambios, optionsCollapsible);    
	//fin collapsible

	

	// Obtiene la fecha de hoy
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
		dd = '0'+dd
	}
	if(mm<10) {
		mm = '0'+mm
	}
	today = mm + '/' + dd + '/' + yyyy;
	// Obtiene la fecha de hoy

	// Iniciando el form
	//$('select').formSelect();
	$('.datepicker').datepicker({
		i18n: {
			cancel: 'Cancelar',
			done: 'Aceptar',
			weekdaysAbbrev: ['D','L','M','X','J','V','S'],
			months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Deciembre'],
			monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
			weekdaysShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
		},
		minDate: new Date(today)
	});
	$('.timepicker').timepicker({
		i18n: {
			cancel: 'Cancelar',
			done: 'Aceptar',
		}
	});
	$('.contador').characterCounter();
	$('.tooltipped').tooltip();
	$('#EditDesc').hide();
	$('#DivNotas').hide();

	// Iniciando Modal 
	_confirmModal = $('#modalConfirm');
	_confirmModal .modal();

	// Autocomplete
	var optionsHost = {
		limit: 5,
		data: { 'Seleccione': null },
		onAutocomplete: function (valor) {
			// var _thisF = $(this); // Guarda el objeto de la funcion
			// var inputEl = _thisF[0].$el[0]; // Guarda el input
			// $(inputEl).data('valid', 1); // Confirma item valido
            var inputEc;
            var esNombreComun = $('#nombreComun').is(':checked');
            if (esNombreComun) {
                inputEC = document.createElement('input'); // Crea el input de EC
                inputEC.setAttribute('type', 'hidden'); // Tipo de input
                inputEC.id = 'nombreC-' + (ElemClength + 1); // ID para enviar
                inputEC.name = 'nombreC' + (ElemClength + 1); // Name para enviar
                inputEC.value = valor; // Guarda el valor del EC
            }
            else {
                inputEC = document.createElement('input'); // Crea el input de EC
                inputEC.setAttribute('type', 'hidden'); // Tipo de input
                inputEC.id = idforInputEC + (ElemClength + 1); // ID para enviar
                inputEC.name = idforInputEC + (ElemClength + 1); // Name para enviar
                inputEC.value = valor; // Guarda el valor del EC
            }
			ElemClength = $('.ECcount').length;
            var inputsAgregados = NGForm.find('.Elementos-Config-List').find('input[value="' + valor + '"]');
            if (inputsAgregados.length > 0) {
                mostrarError("Ya fue agregado!!!")
                return;
            }
			var bttnEC = document.createElement('div'); // Crea el boton del EC
				bttnEC.id = idforBtnEC + (ElemClength + 1); // ID para identificar el boton EC
				bttnEC.className = 'btn-small green darken-3 z-depth-0 elementoC ECcount deleteEC'; // Estilo para el boton // Clase para contar los EC seleccionados

			

			var bttnECtext = document.createTextNode(valor);

			var relacionesEC = document.createElement('div'); // Crea el div de relaciones
				relacionesEC.className = 'relacionesEC left-align grey lighten-4 grey-text text-darken-3 z-depth-1'; // Estilo para el contenedor de las relaciones

			var noRelacionesText = document.createTextNode('No hay elementos relacionados');

			var textNodeWhite = document.createTextNode(' ');

			relacionesEC.appendChild(noRelacionesText); // Adjunta el texto por defecto de las relaciones
			bttnEC.appendChild(inputEC); // Adjunta el input
			bttnEC.appendChild(bttnECtext); // Adjunta el texto del boton
			bttnEC.appendChild(relacionesEC); // Adjunta las relaciones al boton

			bttnEC.addEventListener('dblclick', deleteElemC); // Crea el listener para el boton
			
			NGForm.find('.Elementos-Config-List').append(bttnEC).append(textNodeWhite);

			fGestionCambios.element( "#idEc" ); // Valida el campo autocomplete

			$.ajax({
				type: "POST",
				//contentType: "application/json; charset=utf-8",
				url: "/Cambios/Relaciones",
				data: { oblea: valor },
				dataType: "json",
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						var idElementC = '#EC-n-' + ElemClength;
						MostrarRelaciones(data, idElementC);
					}
				},
			});
		}
	};
	var autoHostOrigen = M.Autocomplete.init(document.getElementById('idEc'), optionsHost);
	$('#idEc').on('keyup', function (e) {
        var ter = $(this).val();
        if (ter.length < 3) return;
        var id = $(this).attr('id');
        var check = $('#nombreComun').is(':checked');
        var uri;
        if (check) uri = '/Service/ServidorXNombreComun';
        else uri = "/Service/EquipoXOblea";
		if (ter.length > 2) {
			var term = '{ prefixText: "' + ter + '"}';
			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: uri,
				data: term,
				dataType: "json",
				success: function (data) {
					if (data.Info == "Login") {
						mostrarFinSesion();
						return;
					}
					else {
						var obj = {};
                        $.each(data, function (i, val) {
                            if (check) obj[val.NombreComun] = null;
								obj[val.Oblea] = null;								
						});
						
						autoHostOrigen.updateData(obj);
						autoHostOrigen.open();
					}
				}
			});
		}
	});

	// Validar campos
	$.validator.setDefaults({
		ignore: []
	});
	$.validator.addMethod('autoCvalid', function (value, element) {
		ElemClength = $('.ECcount').length;
		if (ElemClength > 0) return true;
		else return false;
	});
	var fGestionCambios = $('#fNGCambios').validate({
		rules: {
			idTipo: {
				required: true,
				min: 1
			},
			asunto: {
				required: true,
				minlength: 10
			},
			descripcion: {
				required: true,
				minlength: 50
			},
			idEc: {
				// required: true,
				// minlength: 7,
				autoCvalid: true
			},
			autorizante: {
				required: true,
				min: 1
			},
			prioridad: {
				required: true,
				min: 1
			},
			fechaInicioEstimada: {
				required: true
			},
			horaInicioEstimada: {
				required: true
			},
			duracionestimada: {
				required: true,
				min: 1
			},
			razon: {
				required: true,
				minlength: 10
			},
			resultados: {
				required: true,
				minlength: 10
			},
			riesgos: {
				required: true,
				minlength: 10
			},
			rollback: {
				required: true,
				minlength: 10
			},
			recursos: {
				required: true
			},
			serviciosAfectados: {
				required: true
			},
			// conclusion: {
			// 	required: true,
			// 	minlength: 100
			// }

		},
		submitHandler: function (form) {
			var flag = true;
			if (flag) {
				document.body.onbeforeunload = function () {
					return null;
				}
				form.submit();
			}
		},
		errorClass: "invalid",
		messages: {
			idTipo: {
				required: "Seleccione el tipo de solicitud"
			},
			asunto: {
				required: "Ingrese el titulo",
				minlength: "El titulo debe tener al menos 10 caracteres"
			},
			descripcion: {
				required: "Describa detalladamente la gestion de cambio",
				minlength: "La descripcion debe tener al menos 50 caracteres"
			},
			idEc: {
				// required: "Seleccione el elemento de configuración afectado",
				// minlength: "Debe SELECCIONAR uno o más elemento",
				autoCvalid: "SELECCIONE uno, o más, elemento/s de configuración afectado/s"
			},
			autorizante: {
				required: "Seleccione el responsable autorizante"
			},
			prioridad: {
				required: "Seleccione la prioridad"
			},
			fechaInicioEstimada: {
				required: "Seleccione la fecha de inicio"
			},
			horaInicioEstimada: {
				required: "Seleccione la hora de inicio"
			},
			duracionestimada: {
				required: "Ingrese la duración estaimada, en horas"
			},
			razon: {
				required: "Describa las razones de la solicitud del cambio",
				minlength: "La razon debe tener al menos 10 caracteres"
			},
			resultados: {
				required: "Describa los resultados esperados",
				minlength: "La razon debe tener al menos 10 caracteres"
			},
			riesgos: {
				required: "Describa los riesgos posibles",
				minlength: "El riesgo debe tener al menos 10 caracteres"
			},
			rollback: {
				required: "Describa las tareas de prevencion posibles",
				minlength: "La descripción de rollback debe tener al menos 10 caracteres"
			},
			recursos: {
				required: "Ingrese los recursos necesarios"
			},
			serviciosAfectados: {
				required: "Ingrese los servicios afectados"
			},
			conclusion: {
				required: "Ingrese la conclusión",
				minlength: "La conclusión debe tener al menos 10 caracteres"
			}
		},
		errorElement: 'div',
		errorPlacement: function (error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}

	});

	/* CONTROLES */
	// SELECT
	var Filtros = document.querySelectorAll('.selectFiltro'); // Busca todos los select de filtros
	for (var i = 0; i < Filtros.length; i++)
	{
		Filtros[i].addEventListener('change', AplicarFiltros); // Listener para los filtros
	}
	// BOTONES
	var ActionsBarBtn =  document.querySelectorAll('.btnAction'); // Busca todos los botones en Gestiones de Cambios
	for (var i = 0; i < ActionsBarBtn.length; i++)
	{
		ActionsBarBtn[i].addEventListener('click', GCambioAcciones); // Listener por cada boton de acción
	}

	var EditarDesc = document.getElementById('btnEditDesc'); // Boton para activar la edición de la descripción
		EditarDesc.addEventListener('click', fEditDesc); // Listener para el boton

	var AccUserConfirm = document.getElementById('AccUserConfirm'); // Boton para Aceptar cambio de estado de la gestion
		AccUserConfirm.addEventListener('click', ConfirmarAccionM);

	var AccUserCancel = document.getElementById('CancelarForm'); // Boton para Cancelar cambio de estado de la gestion
    AccUserCancel.addEventListener('click', fCancelarForm);
    ActualizarSelects();
    // Cierra el preloader
	$('.pre-loader-gc').remove();
});//end ready

function LogError(message) // Login Error?
	{
		$('#LogError span').html(message);
		$('#LogError').removeClass('hide');
	}

function initNiceScroll(_selfNice) // Crea el niceScroll
	{
		Nice = _selfNice.niceScroll({
		background: "#7CD569",
		cursorcolor: "#24890D",
		cursorwidth: "10px",
		cursorborder: "0",
		cursorborderradius: "2px",
		horizrailenabled: false,
		enabletranslate3d: true,
		railoffset: { top: 0, left: 15 }
		}).resize();
	}

function GCambioAcciones() // Identifica el boton clickeado y determina la accion a realizar
	{
		var _elem = $(this); // Guarda el boton clickeado
		// var accionConfirmObj = {};
		accionConfirmObj.data = { id: _elem[0].dataset.idpedido };

		// Verifica la accion del boton
		switch (_elem[0].dataset.url)
			{
				case "Cancelar":
					accionConfirmObj.Action = "cancelar";
					accionConfirmObj.url = "/Cambios/Cancelar";
					accionConfirm(accionConfirmObj);
					break;
				case "Comite":
					accionConfirmObj.Action = "enviar al comité";
					accionConfirmObj.url = "/Cambios/Comite";
					accionConfirm(accionConfirmObj);
					break;
				case "Rechazar":
					accionConfirmObj.Action = "rechazar";
					accionConfirmObj.url = "/Cambios/Rechazar";
					accionConfirm(accionConfirmObj);
					break;
				case "Aprobar":
					accionConfirmObj.Action = "aprobar";
					accionConfirmObj.url = "/Cambios/Aprobar";
					accionConfirm(accionConfirmObj);
					break;
				case "Implementado":
					accionConfirmObj.Action = "cambiar a implementado";
					accionConfirmObj.url = "/Cambios/Implementado";
					accionConfirm(accionConfirmObj);
					break;
				case "Cerrar":
					accionConfirmObj.Action = "cerrar";
					accionConfirmObj.url = "/Cambios/Cerrar";
					accionConfirm(accionConfirmObj);
					break;
				case "Modificar":
					$.ajax({
						type: "POST",
						dataType: "json",
						data: { id: _elem[0].dataset.idpedido }, // Envia el id de la gestion de cambio
						url: '/Cambios/getpedido'
					}).done(function(ObjCambio){
						modificarGC(ObjCambio, _elem[0].dataset.idpedido);
					});
					break;
			}
	}

function modificarGC(ObjCambio, idCamio) // Configurar el form para hacer una modificacion
	{
		console.log(ObjCambio);
		NGForm.find('#idCambio').val(idCamio);
		NGForm.find('#Modo').val("update");
		NGForm.find('#tipocambio').find("option[value="+ObjCambio.Tipo.Id+"]").attr('selected','selected');
		NGForm.find('#asunto').val(ObjCambio.Asunto).attr('disabled', true);
		NGForm.find('#descripcion').val(ObjCambio.Descripcion);
		NGForm.find('#idEc').val(ObjCambio.ElemConf[0].Nombre).attr('disabled', true);
		NGForm.find('#autorizante').attr('disabled', true).find("option[value="+ObjCambio.Autorizante.id+"]").attr('selected','selected');
		NGForm.find('#prioridad').find("option[value="+ObjCambio.Prioridad.Id+"]").attr('selected','selected');
		NGForm.find('#finicio').val(ObjCambio.FInicioEstimada);
		NGForm.find('#horaInicio').val(ObjCambio.HoraInicioEstimada);
		NGForm.find('#duracionestimada').val(ObjCambio.duracionEstimada);
		NGForm.find('#razon').val(ObjCambio.Razon);
		NGForm.find('#resultados').val(ObjCambio.Resultados);
		NGForm.find('#resultados').val(ObjCambio.Resultados);
		NGForm.find('#riesgos').val(ObjCambio.Riesgos);
		NGForm.find('#rollback').val(ObjCambio.VueltaAtras);
		NGForm.find('#recursos').val(ObjCambio.Recursos);
		NGForm.find('#serviciosafectados').val(ObjCambio.ServiciosAfectados);
		NGForm.find('#DivDesc').hide();
		NGForm.find('#EditDesc').show();
		NGForm.find('#DivNotas').show();

		// Relaciones
		MostrarRelaciones(ObjCambio.ElemConf[0].ElemsRelacionados);

		// Actualiza el materialize del from
		ActualizarForm();

		// Cambia de pestaña
		var tabs = document.getElementById('tab');
		var instance = M.Tabs.getInstance(tabs);
		instance.select('NCambio');

		// Mover scroll al top
		var ScrollToTop = _selfNice.getNiceScroll(0);
		ScrollToTop.doScrollTop(0, 1000);
	}

function MostrarRelaciones(data, idElementC) // Mostrar relaciones
	{
		var _Relaciones = $(idElementC).find('.relacionesEC'); // Guarda el div relaciones

		if (data.length > 0) // Si existe realaciones
		{
			_Relaciones.html(''); // Elimina el texto por defecto
			$.each(data, function(i, val) // Por cada relacion crea un elemento
			{
					var aRel = document.createElement('a');
						aRel.className = 'waves-effect waves-light btn-small green darken-3 z-depth-0';

					var textNode = document.createTextNode(val.Oblea);
					var textNodeWhite = document.createTextNode(' ');

					aRel.appendChild(textNode);
					_Relaciones.append(aRel);
					_Relaciones.append(textNodeWhite);
			});
		}
	}

function deleteElemC() // Borrar elemento de configuracion
	{
		var _thiEC = $(this); // Obtiene el EC clickeado
		_thiEC.remove(); // Borra el EC

		$('.ECcount').each(function(i){
			$(this).attr('id', idforBtnEC + (i + 1)).find('input').attr('id', idforInputEC + (i + 1)); // Renombra los EC al borrar
		})
}
function ActualizarSelects() {
    var ifSetEstado = getParameterByName("estado");
    var ifSetMisG = getParameterByName("misGestiones");
    $('select', ShowGC).each(function () {               
        if (this.id == "es") {
            var text = $(this).find('option:selected').text();
            if (ifSetEstado) $(this).val(ifSetEstado);
        }
        if (this.id == "mis") {
            var text = $(this).find('option:selected').text();
            if (ifSetMisG) $(this).val(ifSetMisG);
        }
        $(this).formSelect();
    });
    $('select', NGForm).each(function () {
        $(this).formSelect(); 
    })
}
function ActualizarForm()
	{
		M.updateTextFields();

		$('select', NGForm).each(function(){
			$(this).formSelect();
		});

		$('textarea', NGForm).each(function(i){
			var _id = $(this).attr('id');
			M.textareaAutoResize($('#'+_id));
		});
	}

function fEditDesc()
	{
		var NGForm = $('#fNGCambios');

		NGForm.find('#EditDesc').hide(200);
		NGForm.find('#DivDesc').show(200);
	}

function fCancelarForm()
	{
		NGForm.find('#idCambio').val('');
		NGForm.find('#Modo').val('');
		NGForm.find('#asunto').attr('disabled', false);
		NGForm.find('#DivDesc').show();
		NGForm.find('#EditDesc').hide();
		NGForm.find('#DivNotas').hide();
		NGForm.find('#idEc').attr('disabled', false);
		NGForm.find('.Elementos-Config-List').html('');
		NGForm.find('#autorizante').attr('disabled', false).find("option[value!=0]").attr('selected', false);
		NGForm.find('#prioridad').find("option[value!=0]").attr('selected', false);
		NGForm.find('#tipocambio').find("option[value!=0]").attr('selected', false);
		$("#idEc").data('valid', 0); // Confirma item valido

		NGForm.on('reset', function(event) {
			// executes before the form has been reset
			setTimeout(function() {
			  // executes after the form has been reset
				ActualizarForm();
				var ScrollToTop = _selfNice.getNiceScroll(0);
				ScrollToTop.doScrollTop(0, 1000);
			}, 1);
		});
	}

function accionConfirm (accionConfirmObj)
	{
		var RespuestaUsuario = _confirmModal.data('confirm');
		_confirmModal.find('#modalAccionVar').html(accionConfirmObj.Action);

		var instanceModal = M.Modal.getInstance(_confirmModal);
		
		if (RespuestaUsuario == false)
		{
			instanceModal.open();
		}
		else
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				data: accionConfirmObj.data, // Envia el id de la gestion de cambio
				url: accionConfirmObj.url,
			}).done(function(ObjCambioRes){
				_confirmModal.data('confirm', false);
				// Espera y actualiza
				if (ObjCambioRes.Info == 'ok')
				{
					setTimeout(function() {
						location.reload();
					}, 250);
				}
				else
				{
					mostrarError(ObjCambioRes.Detail);
				}
			});
		}
	}

function ConfirmarAccionM ()
	{
		_confirmModal.data('confirm', true);
		accionConfirm(accionConfirmObj);
	}

function AplicarFiltros ()
	{
    var form = $(this).closest('form');
    form.submit();
	}