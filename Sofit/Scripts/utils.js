//var errorDiv = "<div id='errorInfo' class='ui-widget' style='z-index:110;'><div class='ui-state-error ui-corner-all' style='" +
//"padding: 0 .7em; width:241px; min-height:52px; font-size:90%;z-index:110;'>" +
//"<p class='mensaje'><span class='ui-icon ui-icon-alert' style='float: left;" +
//" margin-right: .3em;'></span><strong>Error:</strong></p></div></div>";
//var highlightDiv = "<div id='highlightInfo' class='ui-widget' style='z-index:110;'><div class='ui-state-highlight ui-corner-all'" +
//" style='margin-top: 20px; padding: 0 .7em; width:241px; min-height:52px; font-size:90%;text-align: center;'><p class='mensaje'>" +
//"<span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span><strong>\&Eacute;xito!</strong></p>" +
//"<p class='ajaxGif'><img class='ajaxGif' src='" + imgPath + "ajax-loader.gif'/></p></div></div>";
//var finSesionDiv = "<div class='ui-widget' style='z-index:110;'><div class='ui-state-error ui-corner-all'" +
//" style='padding: 0 .7em; width:241px; min-height:52px; font-size:0.9em;z-index:110;'><p class='mensaje'>" +
//"<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span><strong>Error:</strong>" +
//"Ha estado inactivo por un largo periodo. Inicie nuevamente la sesion.</p></div></div>";
//var imgPath = '/Content/img/';
var infoDiv = "<div id='infoInfo' class='tip ui-widget' style='z-index:110;'><div class='ui-state-default ui-corner-all'" +
    " style='margin-top: 20px; padding: 0 .7em; width:241px; min-height:52px; font-size:90%;z-index:110;'><p class='mensaje'>" +
    "<span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span><strong>Info!</strong></p>" +
    "</div></div>";
var regString = new RegExp(/[A-Za-z]{2}/);
var regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var regularEquipo = /^WS\d{5}$|^WS-\d{5}-RHC$|^NB\d{5}$|^NB-\d{5}-RHC$|^PR\d{5}$|^MN\d{5}$|^VS\d{5}$|^SV\d{5}$|^FR\d{5}$|^KS\d{5}$|^GE\d{5}$|^RCK\d{5}$|^UPS\d{5}$|^AAC\d{5}$|^TBL\d{5}$/;
var regularInfra = /^(D|d)(\d{3})(\w{2,8})(\d{0,3})(\w{0,1})$|^STOR\d{3}$|^CLU\d{5}$|^WSV\d{3}$|^MSL\d{4}(SC|CC)\d{2}$|^DNS\d{3}$|^NIFI\d{3}$|^\w{4}\d{3}$/;
var regularInfraStart = /^D(\d{2,3})+(\w{2,3})*|^STOR\d{0,5}$|^CLU\d{0,5}|^WSV\d{0,3}|^MSL\d{0,4}|^DNS\d{0,3}|^NIFI\d{0,3}|^\w{4}\d{0,3}$/;
var regularIncidente = /^CBA\d{7}$|^LN\w{12}$|^SOL\d{7}$|^INC\d{7}$|^PRB\d{7}$/;
var comienzoIncidente = /^CBA|^LN|^SOL|^INC|^PRB/;
var regEquipoTele = /^SWI\d{5}$|^APO\d{5}$|^PID\d{5}$|^RTR\d{5}$|^RTW\d{5}$|^MAD\d{5}$|^CTE\d{5}$|^MFI\d{5}$|^MPE\d{5}$|^PIA\d{5}$|^POW\d{5}$|^PTR\d{5}$|^TAN\d{5}$|^TCE\d{5}$|^TDI\d{5}$|^TIP\d{5}$|^CAM\d{5}$/;
var regTeleStart = /^SWI\d{1,5}|^APO\d{1,5}|^PID\d{1,5}|^RTR\d{1,5}|^RTW\d{1,5}|^MAD\d{1,5}|^CTE\d{1,5}|^MFI\d{1,5}|^MPE\d{1,5}|^PIA\d{1,5}|^POW\d{1,5}|^PTR\d{1,5}|^TAN\d{1,5}|^TCE\d{1,5}|^TDI\d{1,5}|^TIP\d{1,5}|^CAM\d{1,5}/;
var regSeguridad = /^BAL\d{5}$|^PRX\d{5}$|^FRW\d{5}$|^FWS\d{5}$|^SYS\d{5}$|^APP\d{5}$|^PRY\d{5}$|^CLS\d{5}$/;
var regSegStart = /^BAL\d{0,5}|^PRX\d{0,5}|^FRW\d{0,5}|^FWS\d{0,5}|^SYS\d{0,5}|^APP\d{0,5}|^PRY\d{0,5}|^CLS\d{0,5}/;
var regEquipoLaboratorio = /^PR\d{5}$|^MN\d{5}$|^VS\d{5}$|^SV\d{5}$/;
var regEquipoOperaciones = /^LN\d{6}$/;
var regStartUc = /^SWI\d{1,5}|^APO\d{1,5}|^PID\d{1,5}|^RTR\d{1,5}|^RTW\d{1,5}|^MAD\d{1,5}|^CTE\d{1,5}|^MFI\d{1,5}|^MPE\d{1,5}|^PIA\d{1,5}|^POW\d{1,5}|^PTR\d{1,5}|^TAN\d{1,5}|^TCE\d{1,5}|^TDI\d{1,5}|^TIP\d{1,5}|^CAM\d{1,5}|^BAL\d{1,5}|^PRX\d{1,5}|^FRW\d{1,5}|^FWS\d{1,5}|^SYS\d{1,5}|^APP\d{1,5}|^PRY\d{1,5}|^D(\d{2,3})+(\w{2,3})*|^STO\d{0,5}$|^CLU\d{0,5}$|^WSV\d{0,5}$|^MSL\d{0,4}|^WS|^NB|^PR|^MN|^VS|^SV|^GE|^UPS|^RCK"/
var regTime = /^\d{1,3}$/;
var regDateTime = /^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/
function mostrarMensajeFx(text, type,ttl1) {
    var notification = new NotificationFx({
        wrapper: document.body,
        message: text,
        layout: 'growl',
        effect: 'genie',
        type: type, // success, notice, warning or error
        ttl: ttl1,
        onClose: function () {
            if (type == 'warning') {
                window.location = '/Login';
            }
        }
    });
    // show the notification
    notification.show();
}
function createRequest() {
  try {
    request = new XMLHttpRequest();
  } catch (tryMS) {
    try {
      request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (otherMS) {
      try {
        request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (failed) {
        request = null;
      }
    }
  }	
  return request;
}
function disableHandlers(container) {    
    container[0].addEventListener('click', handler, true);
    container[0].addEventListener('change', handler, true);
}
function compareStrings(string1, string2) {
    var flag = true;
    if (string1.length == string2.length) {        
        for (i = 0; i < string1.length && flag; i++) {
            var l1 = string1.substring(i, (i + 1));
            var l2 = string2.substring(i, (i + 1))
            if (l1 != l2) {
                flag = false;
                continue;
            }
        }       
    } else flag = false;
    return flag
}
function handler(e) {
    e.stopPropagation();
    e.preventDefault();
}
function setSelectByText($drop, text) {
    $($drop).find('option').each(function () {
        if ($(this).text() == text) {
            $(this).attr('selected', 'selected');
        }
    });
}
function setSelectByVal($drop, val) {
    $($drop).find('option').each(function () {
        if ($(this).val() == val) {
            $(this).attr('selected', 'selected');
        }
    });
}
function openDependency() {
    window.open('Config?index=5', '', 'height=600,width=930,menubar=no,toolbar=no,scrollbars=yes,status=no');
    window.focus();
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}
function validarNoVacio(inputField) {
    var input = $.trim($(inputField).val());
    var parent = $(inputField).parent();
    if (input.length == 0) {
        $(parent).find('span.errorInline').remove();
        $(parent).append('<span class="errorInline">Ingrese un valor</span>');
        return false;
    }
    else {
        $(parent).find('span.errorInline').remove();
        return true;
    }
}    
function validarIsNum(inputField) {
    var input = $.trim($(inputField).val());
    var parent = $(inputField).parent();
    if (isNaN(input)|input.length==0) {
        $(parent).find('span.errorInline').remove();
        $(parent).append('<span class="errorInline">Debe ingresar un n&uacutemero</span>');
        return false;        
    }
    else {
        $(parent).find('span.errorInline').remove();
        return true;
        }   
}
function regularExpression(regex, inputStr, helpMessage) {
    var input = inputStr.val();
    if (!regex.test(input)) {
        inputStr.val(helpMessage);
        inputStr.addClass('help', 1500);
        return false;
    }
    else {
        inputStr.removeClass('help',1000);
        return true;
    }
}
function regExTest(regex, inputStr) {
    if (regex.test(inputStr)) return true;
    else return false;
}
function getActivatedObject(e) {
  var obj;
  if (!e) {
    // early version of IE
    obj = window.event.srcElement;
  } else if (e.srcElement) {
    // IE 7 or later
    obj = e.srcElement;
  } else {
    // DOM Level 2 browser
    obj = e.target;
  }
  return obj;
}
function addEventHandler(obj, eventName, handler) {
  if (document.attachEvent) {
    obj.attachEvent("on" + eventName, handler);
  } else if (document.addEventListener) {
    obj.addEventListener(eventName, handler, false);
  }
}
(function($) {
    $.fn.styleTable = function(options) {
        var defaults = {
            css: 'styleTable'
        };
        options = $.extend(defaults, options);
        return this.each(function() {
            input = $(this);
            input.addClass(options.css);

//            input.find("tr").live('mouseover mouseout', function(event) {
//                if (event.type == 'mouseover') {
//                    $(this).children("td").addClass("ui-state-hover");
//                } else {
//                    $(this).children("td").removeClass("ui-state-hover");
//                }
//            });

            input.find("th").addClass("ui-state-default");
            input.find("td").addClass("ui-widget-content");
            input.find("tr").each(function() {
                $(this).children("td:not(:first)").addClass("first");
                $(this).children("th:not(:first)").addClass("first");
            });
        });
    };
})(jQuery);
(function($) {
    $.fn.serializeAnything = function() {
        var toReturn = [];
        var els = $(this).find(':input').get();
        $.each(els, function() {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                toReturn.push(encodeURIComponent(this.name) + "=" + encodeURIComponent(val));
            }
        });
        return toReturn.join("&").replace(/%20/g, "+");
    }
})(jQuery);
(function ($) {
    $.fn.serializeById = function () {
        var toReturn = [];
        var els = $(this).find(':input').get();
        $.each(els, function () {
            if (this.id && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                toReturn.push(encodeURIComponent(this.id) + "=" + encodeURIComponent(val));
            }
        });
        return toReturn.join("&").replace(/%20/g, "+");
    }
})(jQuery);
(function($) {
    $.fn.serializeToObject = function() {
        var toReturn = [];
        function g(nombre, valor) {
            this.nombre = nombre;
            this.valor = valor;
        }
        var els = $(this).find(':input').get();
        $.each(els, function() {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                var name = this.name;
                toReturn.push(new g(name, val));
            }
        });
        return toReturn;
    }
})(jQuery);
(function($) {
    $.fn.appendSerialToObject = function(obj) {
        var els = $(this).find(':input').get();
        $.each(els, function() {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                //tener en cuenta el ID!!!
                var name = this.id;
                obj[name] = val;
            }
        });
        els = $(this).find('.oblea');
        var sufix = 0;
        $.each(els, function () {
            obj['oblea.' + sufix] = $(this).html();
            sufix++;
        });
        els = $(this).find('.idEquipo');
        var sufix = 0;
        $.each(els, function () {            
            obj['idEquipo.' + sufix] = $(this).html();
            sufix++;
        });
        return obj;
    }
})(jQuery);
/*
PONER AL CENTRO
*/
jQuery.fn.center = function() {
    this.css("position", "absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
}
/*
BROWSER DETECTION
*/
var BrowserDetect = {
    init: function() {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function(data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function(dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
		{
		    string: navigator.userAgent,
		    subString: "Chrome",
		    identity: "Chrome"
		},
		{ string: navigator.userAgent,
		    subString: "OmniWeb",
		    versionSearch: "OmniWeb/",
		    identity: "OmniWeb"
		},
		{
		    string: navigator.vendor,
		    subString: "Apple",
		    identity: "Safari",
		    versionSearch: "Version"
		},
		{
		    prop: window.opera,
		    identity: "Opera",
		    versionSearch: "Version"
		},
		{
		    string: navigator.vendor,
		    subString: "iCab",
		    identity: "iCab"
		},
		{
		    string: navigator.vendor,
		    subString: "KDE",
		    identity: "Konqueror"
		},
		{
		    string: navigator.userAgent,
		    subString: "Firefox",
		    identity: "Firefox"
		},
		{
		    string: navigator.vendor,
		    subString: "Camino",
		    identity: "Camino"
		},
		{		// for newer Netscapes (6+)
		    string: navigator.userAgent,
		    subString: "Netscape",
		    identity: "Netscape"
		},
		{
		    string: navigator.userAgent,
		    subString: "MSIE",
		    identity: "Explorer",
		    versionSearch: "MSIE"
		},
		{
		    string: navigator.userAgent,
		    subString: "Gecko",
		    identity: "Mozilla",
		    versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
		    string: navigator.userAgent,
		    subString: "Mozilla",
		    identity: "Netscape",
		    versionSearch: "Mozilla"
		}
	],
    dataOS: [
		{
		    string: navigator.platform,
		    subString: "Win",
		    identity: "Windows"
		},
		{
		    string: navigator.platform,
		    subString: "Mac",
		    identity: "Mac"
		},
		{
		    string: navigator.userAgent,
		    subString: "iPhone",
		    identity: "iPhone/iPod"
		},
		{
		    string: navigator.platform,
		    subString: "Linux",
		    identity: "Linux"
		}
	]
};
//BrowserDetect.init();
function mostrarExito(data) {
    mostrarMensajeFx(data, 'success',4000);
    //var divActual=$(highlightDiv);
    //$('#highlightInfo').remove();
    //divActual.click(function(event) {
    //    $(this).fadeOut('800', function() { $(this).remove() });
    //})
    //.appendTo('body').center();
    //$('.mensaje',divActual).append(data);
}
function mostrarError(data) {
    mostrarMensajeFx(data, 'error',4000);
    //var divActual=$(errorDiv);
    //$('#errorInfo').remove();
    //divActual.click(function(event) {
    //    $(this).fadeOut('800', function() { $(this).remove() });
    //})
    //.appendTo('body').center();
    //$('.mensaje', divActual).append(data);
}
function mostrarInfo(data) {
    mostrarMensajeFx(data, 'notice',4000);
    //var divActual=$(infoDiv);
    //$('#infoInfo').remove();
    //divActual.click(function (event) {
    //    $(this).fadeOut('800', function () { $(this).remove() });
    //})
    //.appendTo('body').center();
    //$('.mensaje', divActual).append(data);
}
function mostrarFinSesion() {
    mostrarMensajeFx('La sesi�n ha caducado', 'warning',4000);
}
/*****************
VEEER PARA MODERNIZAR
********************/
function verificarOblea(inputStr) {
    inputStr.className = "thinking";
    obleaRequest = createRequest();
    if (obleaRequest == null)
        alert("Unable to create request");
    else {
        var theOblea = inputStr.value;
        var oblea = escape(theOblea);

        var url = "../Models/handlers/checkEquipoCompleto.ashx?oblea=" + oblea;
        obleaRequest.onreadystatechange = showObleaStatus;
        obleaRequest.open("GET", url, false);
        obleaRequest.send(null);
        return obleaRequest.responseText;
    }
}
function showObleaStatus() {
    if (obleaRequest.readyState == 4) {
        if (obleaRequest.status == 200) return obleaRequest.responseText;
        else return "error";
    }
}
function setDrop(val, $drop) {
    var debug = $drop.find('option:selected').text();
    $drop.find('option:selected').removeAttr("selected");
    $drop.find('option').each(function () {
        if ($(this).val() == val) {
            $(this).attr('selected', 'selected');
        }
    });
}
function setDropText(val, $drop) {
    var debug = $drop.find('option:selected').text();
    $drop.find('option:selected').removeAttr("selected");
    $drop.find('option').each(function () {
        if ($(this).text() == val) {
            $(this).attr('selected', 'selected');
        }
    });
}