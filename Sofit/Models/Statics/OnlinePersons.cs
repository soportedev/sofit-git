﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Statics
{
    public class OnlinePersons
    {
        public int Id { get; set; }
        public string Info { get; set; }
        public string Detail { get; set; }
        public List<Persons> Online { get; set; }
    }
}