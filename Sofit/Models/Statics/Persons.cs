﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Statics
{
    public class Persons
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Avatar { get; set; }
    }
}