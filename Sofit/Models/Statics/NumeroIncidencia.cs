﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Statics
{
    public class NumeroIncidencia
    {
        private static volatile NumeroIncidencia instance;
        private string ultimoNro;
        private static object syncRoot = new Object();

        private NumeroIncidencia() {
            ultimoNro = LoadNro();
        }
        private string LoadNro()
        {
            using (var dbc = new IncidenciasEntities())
            {
                var u = (from i in dbc.UltimoNroIncidencia
                         select i).Single();
                return u.numero;
            }
        }
        private void saveNro()
        {
            using (var dbc = new IncidenciasEntities())
            {               
                var u = (from i in dbc.UltimoNroIncidencia
                         select i).Single();
                u.numero = ultimoNro;
                dbc.SaveChanges();
            }
        }
        public static NumeroIncidencia Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new NumeroIncidencia();
                    }
                }
                return instance;
            }
        }
        public string getNext(){
            string result = string.Empty;
            //datos previos
            DateTime fecha = DateTime.Now;
            string año = fecha.Year.ToString("YY");
            string mes = fecha.Month.ToString("m");
            string dia = fecha.Day.ToString("d");
            int ultimosDig = Int16.Parse(ultimoNro.Substring(8, 4));
            result = "CBA" + año + mes + dia+ ++ultimosDig;
            saveNro();
            return result;
        }
    }
}