﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace soporte.Models.Statics
{
    public class Constantes
    {
        public enum AccesosPagina
        {
            Full,
            ReadOnly,
            NoAccess
        }
        public enum UM
        {
            GB,
            MB,
            KB,
            TB,
            Mhz,
            Ghz,
            Khz
        }
        public enum PuertosPerifericos
        {
            PS2,
            USB
        }
    }
}