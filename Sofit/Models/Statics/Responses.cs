﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Statics
{
    [Serializable]
    public class Responses
    {
        public string Info { get; set; }
        public string Detail { get; set; }
        public Responses()
        {
            this.Info = "No inicializado";
            this.Detail = "No inicializado";
        }
    }
}