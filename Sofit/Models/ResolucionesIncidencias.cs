﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace soporte.Models
{
    public class ResolucionesIncidencias:IComparable
    {
        [ScriptIgnore]
        public DateTime Fecha { get; set; }
        private string equipo;
        public int IdUsuario { get; set; }
        public string FechaStr 
        {
            get
            {
                return Fecha.ToString();
            }
            set
            {

            }
        }
        public string Tecnico { get; set; }
        public string Turno { get; set; }
        public string Observaciones { get; set; }
        public string Equipo 
        {
            get
            {
                if (equipo == null) equipo = "N/A";
                return equipo;
            }
            set
            {
                equipo = value;
            }
        }
        public int ? IdEquipo { get; set; }
        public string TipoResolucion { get; set; }
        public int idResolucion { get; set; }
        public int idTipoResolucion { get; set; }
        public int idTecnico { get; set; }
        public string PathImagen { get; set; }

        public int CompareTo(object obj)
        {
            ResolucionesIncidencias other = (ResolucionesIncidencias)obj;
            if (Fecha > other.Fecha)
                return 1;
            else if (Fecha < other.Fecha)
                return -1;
            else return 0;
        }
    }
}