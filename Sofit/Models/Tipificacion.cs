﻿using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class Tipificacion
    {
        private string nombreCompleto;        
        BuilderTipifTree arbolBuilder = new BuilderTipifTree();
        private int backId { get; set; }
        public int idTipificacion { 
            get
            {
                return backId;
            }
            set 
            {
                backId = value;
                arbolBuilder.construir("Todos");                
            }            
        }
        public string Nombre { get; set; }
        public string getNombreCompleto()
        {            
            return arbolBuilder.getNombreCompleto(backId);
        }
    }
}