﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace soporte.Models.Helpers
{
    public class DataTableConverter
    {
        public static DataTable ToDataTable<T>(IEnumerable<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties((BindingFlags.Public | BindingFlags.Instance));
            //columnas
            Props = Props.Where(c => !c.PropertyType.IsGenericType).ToArray();
            foreach (PropertyInfo prop in Props)
            {                
                if (prop.PropertyType.IsGenericType)
                {
                    continue;
                }
                else
                {
                    dataTable.Columns.Add(prop.Name);
                }
            }
            //filas
            foreach (T item in items)
            {
                
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (Props[i].PropertyType.IsGenericType)
                    {
                        continue;
                    }
                    else
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}