﻿using Sofit.DataAccess;
using Sofit.Models.ClasesVistas;
using Sofit.Models.Helpers;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Models.Helpers
{
    public class BuilderTipifTree
    {
        ArbolTipificaciones arbol = null;
        public BuilderTipifTree(){
            
        }
        public void construir(string direccion)
        {
            List<TipificacVista> tipificaciones = new List<TipificacVista>();
            //List<Tipificaciones> listaBase = new List<Tipificaciones>();
            switch (direccion)
            {
                case "Soporte Técnico": using (var dbc = new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                          where tip.Direcciones.nombre == "Soporte Técnico"
                                          select new TipificacVista
                                          {
                                              idTipificacion = tip.id_tipificacion,
                                              Nombre = tip.nombre,
                                              Descripcion = tip.descripcion,
                                              idPadre = tip.id_padre,
                                              idDireccion = tip.id_direccion,
                                              idServicio = tip.id_servicio.HasValue?tip.id_servicio.Value:0,
                                              Servicio=tip.id_servicio.HasValue?tip.Servicios.nombre:""
                                          }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones); 
                    }
                    break;
                case "Telecomunicaciones": using (var dbc = new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                     where tip.Direcciones.nombre == "Telecomunicaciones"
                                     select new TipificacVista
                                     {
                                         idTipificacion = tip.id_tipificacion,
                                         Nombre = tip.nombre,
                                         Descripcion = tip.descripcion,
                                         idPadre = tip.id_padre,
                                         idDireccion = tip.id_direccion,
                                         idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                         Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                     }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
                case "Mesa de Ayuda": using (var dbc = new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                          where tip.Direcciones.nombre == "Mesa de Ayuda"
                                          select new TipificacVista
                                          {
                                              idTipificacion = tip.id_tipificacion,
                                              Nombre = tip.nombre,
                                              Descripcion = tip.descripcion,
                                              idPadre = tip.id_padre,
                                              idDireccion = tip.id_direccion,
                                              idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                              Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                          }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
                case "Operaciones":using(var dbc=new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                          where tip.Direcciones.nombre == "Operaciones"
                                          select new TipificacVista
                                          {
                                              idTipificacion = tip.id_tipificacion,
                                              Nombre = tip.nombre,
                                              Descripcion = tip.descripcion,
                                              idPadre = tip.id_padre,
                                              idDireccion = tip.id_direccion,
                                              idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                              Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                          }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
                case "Todos":using(var dbc=new IncidenciasEntities())
                    {
                         tipificaciones = (from tip in dbc.Tipificaciones                                    
                                     select new TipificacVista
                                     {
                                         idTipificacion = tip.id_tipificacion,
                                         Nombre = tip.nombre,
                                         Descripcion = tip.descripcion,
                                         idPadre = tip.id_padre,
                                         idDireccion = tip.id_direccion,
                                         idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                         Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                     }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
                case "Infraestructura":using(var dbc=new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                          where tip.Direcciones.nombre == "Infraestructura"
                                          select new TipificacVista
                                          {
                                              idTipificacion = tip.id_tipificacion,
                                              Nombre = tip.nombre,
                                              Descripcion = tip.descripcion,
                                              idPadre = tip.id_padre,
                                              idDireccion = tip.id_direccion,
                                              idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                              Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                          }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
                case "Seguridad Informática": using (var dbc = new IncidenciasEntities())
                    {
                        tipificaciones = (from tip in dbc.Tipificaciones
                                          where tip.Direcciones.nombre == "Seguridad Informática"
                                          select new TipificacVista
                                          {
                                              idTipificacion = tip.id_tipificacion,
                                              Nombre = tip.nombre,
                                              Descripcion = tip.descripcion,
                                              idPadre = tip.id_padre,
                                              idDireccion = tip.id_direccion,
                                              idServicio = tip.id_servicio.HasValue ? tip.id_servicio.Value : 0,
                                              Servicio = tip.id_servicio.HasValue ? tip.Servicios.nombre : ""
                                          }).ToList();
                        arbol = new ArbolTipificaciones(tipificaciones);
                    }
                    break;
            }            
            arbol.Construir();            
        }
        public List<TipifEnServicio> getEnServicios()
        {
            if (arbol != null) return arbol.ConstruirAsServicios();
            else return null;
        }
        public List<TipificacVista> getResult(){
            if (arbol != null) return arbol.getResult();
            else return null;
        }
        public class ArbolTipificaciones
        {
            List<TipificacVista> arbolTipificaciones = new List<TipificacVista>();
            Stack<TipificacVista> noEncontrados = new Stack<TipificacVista>();
            List<TipificacVista> listaBase = new List<TipificacVista>();
            public ArbolTipificaciones(List<TipificacVista> lBase)
            {
                this.listaBase = lBase;
            }
            public List<TipifEnServicio> ConstruirAsServicios()
            {
                List<TipifEnServicio> tipifi = new List<TipifEnServicio>();
                List<TipificacVista> listado = getResult();
                tipifi = listado.GroupBy(c => c.Servicio).Select(d => new TipifEnServicio
                {
                    Servicio = d.Key,
                    Tipificaciones = d.ToList()
                }).ToList();
                return tipifi;
            }
            public void Construir()
            {
                TipificacVista padreEncontrado;
                foreach (TipificacVista t in listaBase)
                {
                    //si tiene padre busco su padre
                    if (t.idPadre.HasValue)
                    {
                        padreEncontrado = Buscar(arbolTipificaciones, t);
                        if (padreEncontrado == null)
                        {
                            //si no encuentro el padre lo pongo en cola
                            noEncontrados.Push(t);
                        }
                        else
                        {
                            t.Padre = padreEncontrado.Nombre;
                            padreEncontrado.Hijos.Add(t);
                        }
                    }
                    else arbolTipificaciones.Add(t);
                }
                //los que no se encontro el padre
                //foreach(TipificacVista t in noEncontrados){
                //    padreEncontrado = Buscar(arbolTipificaciones, t);
                //    if (padreEncontrado != null) padreEncontrado.Hijos.Add(t);
                //}
                bool sigo = false;
                for (int i = 0; i < noEncontrados.Count; i++)
                {

                    TipificacVista tip = noEncontrados.ElementAt(i);
                    if (!tip.Agregado)
                    {
                        padreEncontrado = Buscar(arbolTipificaciones, tip);
                        if (padreEncontrado == null) sigo = true;
                        else
                        {
                            padreEncontrado.Hijos.Add(tip);
                            tip.Padre = padreEncontrado.Nombre;
                            tip.Agregado = true;
                        }
                    }
                    if (i + 1 == noEncontrados.Count & sigo)
                    {
                        i = -1;
                        sigo = false;
                    }
                }
            }
            private TipificacVista Buscar(IEnumerable<TipificacVista>lista,TipificacVista item)
            {                
                TipificacVista result = null;
                foreach (TipificacVista t1 in lista)
                {
                    if (t1.idTipificacion==item.idPadre)
                    {
                        result = t1;
                        break;
                    }
                    else
                    {
                        if (t1.tieneHijos)
                        {
                            result=Buscar(t1.Hijos, item);
                            if (result != null)
                                break;
                        }
                    }                    
                }
                return result;
            }

            internal List<TipificacVista> getResult()
            {
                return arbolTipificaciones;
            }

            internal List<TipificacVista> getPadres()
            {
                List<TipificacVista> result = new List<TipificacVista>();
                foreach (TipificacVista t1 in arbolTipificaciones)
                {
                    if (!t1.idPadre.HasValue)
                    {
                        result.Add(t1);
                    }                    
                }
                return result;
            }
        }

        internal List<TipificacVista> getPadres()
        {
            if (arbol != null)
            {
                return arbol.getPadres();
            }
            else return null;
        }

        internal string getNombreCompleto(int backId)
        {            
            StringBuilder sb = new StringBuilder();           
            return sb.ToString();
        }
         
    }
}