﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace soporte.Models.Helpers
{
    public static class PathImage
    {        
        
        public static string getImagenDirectory()
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/img/Photo"),"");
        }
        public static string getFileDirectory()
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Files/"), "");
        }
        public static string getResolucionDirectorio()
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/img/Resoluciones"), "");
        }       
        public static string getResolucionCustom(string resNro)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/img/Resoluciones/"), resNro);
        }
        public static string getReglasPath(string nombre)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Files/Reglas/"), nombre);
        }
        public static string getCambiosPath(string nombre)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Files/Cambios/"), nombre);
        }
        public static string getCustomFileDirectory(string filename)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Files/"), filename);
        }
        public static string getImagenDefault()
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/img/Photo/"),"imageHere.jpg");
        }
        public static string getCustomImagenDefault(string fileName)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/img/Photo"), fileName);
        }              
        public static string getHtmlImageDefault()
        {
            return "/Content/img/Photo/imageHere.jpg";
        }
        public static string getHtmlDirectoryDefault()
        {
            return "/Content/img/Photo/";
        }        
        public static string getHtmlFileDirectory()
        {
            return "/Content/Files";
        }
        
    }
}