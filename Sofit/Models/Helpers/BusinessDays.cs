﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Helpers
{
    public class BusinessDays
    {
        public static TimeSpan GetBusinessTime(DateTime startDate, DateTime endDate)
        {
            //double totalD = (endD - startD).TotalDays;
            //double calcBusinessDays =
            //    1 + ((endD - startD).TotalDays * 5 -
            //    (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            //if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            //if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            //return calcBusinessDays;            
            TimeSpan timeLength = endDate - startDate;
            for (DateTime index = startDate; index < endDate; index = index.AddDays(1))
            {
                if (index.DayOfWeek == DayOfWeek.Sunday | index.DayOfWeek == DayOfWeek.Saturday)
                {
                    timeLength = timeLength.Add(new TimeSpan(-1, 0, 0, 0));
                }
            }
            return timeLength;
        }
    }
}