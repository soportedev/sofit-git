﻿using Sofit.DataAccess;
using Sofit.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace soporte.Models.Helpers
{
    public class EmailSender
    {
        //Set server
        SmtpClient client = null;
        MailAddress from = null;
        MailAddress to = null;        
        public EmailSender()
        {
            client= new SmtpClient("10.250.1.239");
            client.Port = 25;
            client.UseDefaultCredentials = true;
            from = new MailAddress("Sofit@cba.gov.ar", "Dirección General de IT");           
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        }
        ///<summary>
        /// arg[0]=address, arg[1]=subject, arg[2]=body
        ///</summary>
        public Responses sendEmail(string[] args)
        {
            Responses result = new Responses();
            string bhtml = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/TempSt.html")))
            {
                bhtml = reader.ReadToEnd();
            }           
            try
                {
                    if (GlobalVar.DEBUG) to = new MailAddress("david.ortega@cba.gov.ar");
                    else to = new MailAddress(args[0]);
                    MailMessage mail = new MailMessage(from, to);
                    mail.IsBodyHtml = true;
                    //string attachmentPath = Environment.CurrentDirectory + @"\test.png";
                    //Attachment inline = new Attachment(attachmentPath);
                    //inline.ContentDisposition.Inline = true;
                    //inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                    //inline.ContentId = contentID;
                    //inline.ContentType.MediaType = "image/png";
                    //inline.ContentType.Name = Path.GetFileName(attachmentPath);
                    //message.Attachments.Add(inline);

                    mail.Priority = MailPriority.High;
                    //client.Timeout=1000;
                    //client.Credentials=CredentialCache.DefaultNetworkCredentials;
                    mail.Subject = args[1];
                    string body = args[2];
                    bhtml = bhtml.Replace("{Fecha}", DateTime.Now.ToShortDateString());
                    bhtml = bhtml.Replace("{Body}", body);
                //mail.Body = args[2];                
                    mail.Body = bhtml;
                    client.Send(mail);
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Info = "error";
                    result.Detail = e.Message;
                    usuarioBean esteUsuario = HttpContext.Current.Session["usuario"] as usuarioBean;
                    using (var dbc = new IncidenciasEntities())
                    {
                        LogsEmail nuevo = new LogsEmail
                        {
                            bodyLength = args[2].Length,
                            direccion = args[0],
                            error = e.Message,
                            idUser = esteUsuario.IdUsuario,
                            timest = DateTime.Now
                        };
                        dbc.LogsEmail.Add(nuevo);
                        dbc.SaveChanges();
                    }
                }
            return result;
        }
        public Responses sendEmailCustom(string from, string to,string subject,string body)
        {
            Responses result = new Responses();
            try
            {
                if (GlobalVar.DEBUG) to = "david.ortega@cba.gov.ar";
                MailMessage mail = new MailMessage(from, to);             

                //string attachmentPath = Environment.CurrentDirectory + @"\test.png";
                //Attachment inline = new Attachment(attachmentPath);
                //inline.ContentDisposition.Inline = true;
                //inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                //inline.ContentId = contentID;
                //inline.ContentType.MediaType = "image/png";
                //inline.ContentType.Name = Path.GetFileName(attachmentPath);
                //message.Attachments.Add(inline);               
                mail.Priority = MailPriority.High;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;                
                //client.Timeout=1000;
                //client.Credentials=CredentialCache.DefaultNetworkCredentials;
                mail.Subject = subject;
                mail.Body = body;

                client.Send(mail);
                result.Info = "ok";
            }
            catch (Exception e)
            {
                result.Info = "error";
                result.Detail = e.Message;                
                using (var dbc = new IncidenciasEntities())
                {
                    LogsEmail nuevo = new LogsEmail
                    {
                        bodyLength = body.Length,
                        direccion = to,
                        error = subject+"--"+e.Message,
                        idUser = 1,
                        timest = DateTime.Now
                    };
                    dbc.LogsEmail.Add(nuevo);
                    dbc.SaveChanges();
                }
            }
            return result;
        }

        #region Notificaciones pedidos acceso
        public Responses NotificarCreacionPedidoReglas(string[]tos)
        {
            Responses result = new Responses();

            return result;
        }
        #endregion
    }
}