﻿using soporte.Areas.Infraestructura.Models;
using soporte.Areas.Mesa.Models;
using soporte.Areas.Operaciones.Models;
using soporte.Areas.Seguridad.Models;
using soporte.Areas.Soporte.Models;
using soporte.Areas.Telecomunicaciones.Models;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Factory
{
    public class AreaIncidenciaFactory
    {
        private static List<Area> areas;
        static AreaIncidenciaFactory()
        {
            using (var db = new IncidenciasEntities())
            {
                areas = (from u in db.area
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion,
                             idDireccion = u.id_direccion.Value,
                             NombreDirecion = u.Direcciones.nombre
                         }).ToList();
            }
        }
        public static IAreaIncidencia getArea(int idArea)
        {
            return get(areas.Where(c => c.idArea == idArea).Single());
        }
        public static IAreaIncidencia getArea(string nombre)
        {
            return get(areas.Where(c => c.Nombre == nombre).Single());
        }     
        private static IAreaIncidencia get(Area area)
        {
            IAreaIncidencia ainc = null;
            switch (area.NombreDirecion)
            {
                case "Seguridad Informática":
                    if(area.Nombre.Equals("Seguridad"))
                    {
                        ainc = new AreaSeguridad(area.idArea, area.Nombre)
                        {
                            NombreDireccion=area.NombreDirecion,
                            idDireccion=area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Entrada Seguridad"))
                    {
                        ainc = new AreaEntradaSeguridad(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    break;
                case "Infraestructura":
                    if(area.Nombre.Equals("Infraestructura"))
                    {
                        ainc = new AreaInfraestructura(area.idArea, area.Nombre)
                        {
                            NombreDireccion=area.NombreDirecion,
                            idDireccion=area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Entrada Infraestructura"))
                    {
                        ainc = new AreaEntradaInfraestructura(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }                    
                    if (area.Nombre.Equals("Sistemas Operativos"))
                    {
                        ainc = new AreaSistOperativos(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    break;
                case "Operaciones": 
                    if(area.Nombre.Equals("Operaciones"))
                    {
                        ainc = new AreaOperaciones(area.idArea, area.Nombre)
                        {
                            NombreDireccion=area.NombreDirecion,
                            idDireccion=area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Entrada Operaciones"))
                    {
                        ainc = new AreaEntradaOperaciones(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Correo Electrónico"))
                    {
                        ainc = new AreaCorreoElectronico(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    break;
                case "Mesa de Ayuda":
                    if (area.Nombre.Equals("Mesa de Ayuda"))
                    {
                        ainc = new AreaMesa(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion=area.idDireccion
                        };                                                
                    }                    
                    break;
                case "Telecomunicaciones":
                    if (area.Nombre.Equals("Telecomunicaciones"))
                    {
                        ainc = new AreaTelecomunicaciones(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Datos"))
                    {
                        ainc = new AreaDatosTelecomunicaciones(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    if(area.Nombre.Equals("Telefonía"))
                    {
                        ainc = new AreaTelefoniaTelecomunicaciones(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    break;
                case "Soporte Técnico":
                    if (area.Nombre.Equals("At.Usuarios"))
                    {
                        ainc = new AreaIncAtUs(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Soporte"))
                    {
                        ainc = new AreaIncSoporte(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }
                    if (area.Nombre.Equals("Logística"))
                    {
                        ainc = new AreaIncLog(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }                   
                    if (area.Nombre.Equals("Depósito"))
                    {
                        ainc = new AreaIncDeposito(area.idArea, area.Nombre)
                        {
                            NombreDireccion = area.NombreDirecion,
                            idDireccion = area.idDireccion
                        };
                    }                    
                    break;
                default: throw new NotImplementedException();
            }
            return ainc;
        }
    }
}