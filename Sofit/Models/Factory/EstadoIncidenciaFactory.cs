﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Factory
{
    public class EstadoIncidenciaFactory
    {
        public static IEstadoIncidencia getEstado(int idEstado)
        {
            using (var dbc = new IncidenciasEntities())
            {
                EstadoIncidenciaVista estadoEncontrado = (from u in dbc.Estado_incidente
                                                          where u.id_estado == idEstado
                                                          select new EstadoIncidenciaVista
                                                          {
                                                              IdEstado = u.id_estado,
                                                              Nombre = u.descripcion
                                                          }).First();
                if (estadoEncontrado != null) return get(estadoEncontrado);
                else return null;
            }
        }
        public static IEstadoIncidencia getEstado(string nombre)
        {
            using (var dbc = new IncidenciasEntities())
            {
                EstadoIncidenciaVista estadoEncontrado = (from u in dbc.Estado_incidente
                                                          where u.descripcion == nombre
                                                          select new EstadoIncidenciaVista
                                                          {
                                                              IdEstado = u.id_estado,
                                                              Nombre = u.descripcion
                                                          }).First();
                if (estadoEncontrado != null) return get(estadoEncontrado);
                else return null;
            }            
        }
        private static IEstadoIncidencia get(EstadoIncidenciaVista estad)
        {
            IEstadoIncidencia einc = null;
            switch (estad.Nombre)
            {
                case "Para Cerrar":einc=new EstadoIncidenciaParaCerrar {
                    IdEstado=estad.IdEstado,
                    NombreEstado=estad.Nombre
                };
                    break;
                case "Esperando Retiro Equipo":
                    einc = new EstadoIncidenciaEsperandoRetiro
                    {
                        IdEstado = estad.IdEstado,
                        NombreEstado = estad.Nombre
                    };
                    break;
                case "Ingresado":
                    einc = new EstadoIncidenciaIngresado
                    {
                        IdEstado = estad.IdEstado,
                        NombreEstado = estad.Nombre
                    };
                    break;
                case "Derivado":
                    einc = new EstadoIncidenciaDerivado
                    {
                        IdEstado = estad.IdEstado,
                        NombreEstado = estad.Nombre
                    };
                    break;
                case "Suspendido":
                    einc = new EstadoIncidenciaSuspendido
                    {
                        IdEstado = estad.IdEstado,
                        NombreEstado = estad.Nombre
                    };
                    break;
                case "Cerrado": einc = new EstadoIncidenciaCerrado
                {
                    IdEstado = estad.IdEstado,
                    NombreEstado = estad.Nombre
                };
                    break;
                default: throw new NotImplementedException();
            }
            return einc;
        }
    }
}