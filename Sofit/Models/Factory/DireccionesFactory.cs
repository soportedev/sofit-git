﻿using Sofit.DataAccess;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Factory
{
    public class DireccionesFactory
    {
        public static IDirecciones getDireccion(int id)
        {
            IDirecciones direccion = null;
            using (var dbc = new IncidenciasEntities())
            {
                var direc = (from u in dbc.Direcciones
                             where u.id_direccion == id
                             select u).First();
                switch (direc.nombre)
                {
                    case "Seguridad Informática": direccion = new SubDireccionSeguridad
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Soporte Técnico": direccion = new SubDireccionSoporte { 
                         IdDireccion=direc.id_direccion,
                          Nombre=direc.nombre
                    };
                        break;
                    case "Telecomunicaciones": direccion = new SubDireccionTelecomunicaciones
                    {
                         IdDireccion=direc.id_direccion,
                         Nombre=direc.nombre
                    };
                        break;
                    case "Mesa de Ayuda": direccion = new SubDireccionMesa
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Operaciones": direccion = new SubDireccionOperaciones
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Infraestructura": direccion = new SubDireccionInfraestructura
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                }
            }
            return direccion;
        }
        public static IDirecciones getDireccion(string nombre)
        {
            IDirecciones direccion = null;
            using (var dbc = new IncidenciasEntities())
            {
                var direc = (from u in dbc.Direcciones
                             where u.nombre == nombre
                             select u).First();
                switch (direc.nombre)
                {
                    case "Seguridad Informática": direccion = new SubDireccionSeguridad
                     {
                         IdDireccion=direc.id_direccion,
                         Nombre=direc.nombre
                     };
                        break;
                    case "Soporte Técnico": direccion = new SubDireccionSoporte
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Telecomunicaciones": direccion = new SubDireccionTelecomunicaciones
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Mesa de Ayuda": direccion = new SubDireccionMesa
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Operaciones": direccion = new SubDireccionOperaciones
                    {
                        IdDireccion = direc.id_direccion,
                        Nombre = direc.nombre
                    };
                        break;
                    case "Infraestructura": direccion = new SubDireccionInfraestructura
                     {
                         IdDireccion = direc.id_direccion,
                         Nombre = direc.nombre
                     };
                        break;
                }
            }
            return direccion;
        }
    }
}