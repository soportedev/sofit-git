﻿using Sofit.DataAccess;
using soporte.Areas.Direccion.Models;
using soporte.Areas.Infraestructura.Models;
using soporte.Areas.Mesa.Models;
using soporte.Areas.Operaciones.Models;
using soporte.Areas.Seguridad.Models;
using soporte.Areas.Soporte.Models;
using soporte.Areas.Telecomunicaciones.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace soporte.Models
{
    public class PerfilFactory
    {
        private static PerfilFactory factory;
        private List<Perfiles> perfilesList = new List<Perfiles>();
        private Dictionary<int, string> perfilesIdKey = new Dictionary<int, string>();
        private PerfilFactory()
        {
           using(var dbc=new IncidenciasEntities()){
               var perfiles=from o in dbc.Perfiles.Include("Direcciones")
                            select o;
               perfilesList=perfiles.ToList();
           }              
        }
        public static PerfilFactory getInstancia()
        {
            if (factory == null) factory = new PerfilFactory();
            return factory;
        }
        public Perfil crear(int id)
        {
            Perfiles result = (from o in perfilesList where o.id_perfil == id select o).Single();
            return get(result);
        }
        public Perfil crear(string nombre)
        {
            Perfiles result = (from o in perfilesList where o.descripcion==nombre select o).Single();
            return get(result);
        }
        public List<Perfiles> getAll()
        {
            return perfilesList;
        }
        private Perfil get(Perfiles perfilBase){
            Perfil perfil = null;
            switch (perfilBase.Direcciones.nombre)
            {
                case "Gerencia":
                    if(perfilBase.descripcion.Equals("Dirección"))
                    {
                        perfil = new PerfilDireccion
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    break;
                case "Seguridad Informática":
                    if(perfilBase.descripcion.Equals("Administrador Seguridad"))
                    {
                        perfil = new AdministradorSeguridad
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Operador Seguridad"))
                    {
                        perfil = new OperadorSeguridad
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }                    
                    break;
                case "Infraestructura":
                    if(perfilBase.descripcion.Equals("Administrador Infraestructura"))
                    {
                        perfil = new PerfilAdminInfra
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Operador Infraestructura"))
                    {
                        perfil = new PerfilOperadorInfra
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    break;
                case "Operaciones":
                    if(perfilBase.descripcion.Equals("Administrador Operaciones"))
                    {
                        perfil = new PerfilAdministradorOperaciones()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }

                        };
                    }
                    if (perfilBase.descripcion.Equals("Operador Operaciones"))
                    {
                        perfil = new PerfilOperadorOperaciones()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }

                        };
                    }
                    break;
                case "Mesa de Ayuda":
                    if (perfilBase.descripcion.Equals("Administrador Mesa"))
                    {
                        perfil = new PerfilAdministradorMesa()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Operador Mesa"))
                    {
                        perfil = new PerfilOperadorMesa()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    break;
                case "Telecomunicaciones": 
                    if(perfilBase.descripcion.Equals("Administrador Telecomunicaciones"))
                    {
                        perfil = new PerfilAdministradorTele()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if(perfilBase.descripcion.Equals("Operador Telecomunicaciones"))
                    {
                        perfil = new PerfilOperadorTele()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    break;
                case "Soporte Técnico":
                    if (perfilBase.descripcion.Equals("Administrador Soporte"))
                    {
                        perfil = new PerfilAdministradorSoporte()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Stock"))
                    {
                        perfil = new PerfilStock()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Historicos"))
                    {
                        perfil = new PerfilHistoricos()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Inventario"))
                    {
                        perfil = new PerfilInventario()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Usuario Clonado"))
                    {
                        perfil = new PerfilClonado()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }
                    if (perfilBase.descripcion.Equals("Usuario Logística"))
                    {
                        perfil = new PerfilLogistica()
                        {
                            id = perfilBase.id_perfil,
                            nombre = perfilBase.descripcion,
                            direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                        };
                    }

                    if (perfilBase.descripcion.Equals("Usuario At.Usuarios"))
                    {
                        perfil = new PerfilAtUsuarios()
                    {
                        id = perfilBase.id_perfil,
                        nombre = perfilBase.descripcion,
                        direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                    };
                    }

                    if (perfilBase.descripcion.Equals("Usuario Laboratorio"))
                    {
                        perfil = new PerfilLaboratorio()
                     {
                         id = perfilBase.id_perfil,
                         nombre = perfilBase.descripcion,
                         direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                     };
                    }
                    break;
                case "Administración":perfil = new PerfilAdministradorGral()
                {
                    id = perfilBase.id_perfil,
                    nombre = perfilBase.descripcion,
                    nombreBase=perfilBase.descripcion,
                    direccion = new Direccion() { id = perfilBase.Direcciones.id_direccion, nombre = perfilBase.Direcciones.nombre }
                };
                    break;
                default: throw new NotImplementedException();                   
            }
            return perfil;
        }
        internal List<Perfiles> getPerfilesSoporte()
        {
            List<Perfiles> result = (from o in perfilesList
                                     where o.Direcciones.nombre == "Soporte Técnico"
                                     select o).ToList();
            return result;
        }
        internal List<Perfiles> getPerfilesTelecomunicaciones()
        {
            List<Perfiles> result = (from o in perfilesList 
                                     where o.Direcciones.nombre== "Telecomunicaciones" 
                                     select o).ToList();
            return result;
        }
        internal List<Perfiles> getPerfilesMesa()
        {
            List<Perfiles> result = (from o in perfilesList
                                     where o.Direcciones.nombre == "Mesa de Ayuda"
                                     select o).ToList();
            return result;
        }
        internal List<Perfiles> getPerfilesOperaciones()
        {
            List<Perfiles> result = (from o in perfilesList
                                     where o.Direcciones.nombre == "Operaciones"
                                     select o).ToList();
            return result;
        }
        internal List<Perfiles> getPerfilesInfraestructura()
        {
            List<Perfiles> result = (from o in perfilesList
                                     where o.Direcciones.nombre == "Infraestructura"
                                     select o).ToList();
            return result;
        }
        internal List<Perfiles> getPerfilesSeguridad()
        {
            List<Perfiles> result = (from o in perfilesList
                                     where o.Direcciones.nombre == "Seguridad Informática"
                                     select o).ToList();
            return result;
        }
    }
}