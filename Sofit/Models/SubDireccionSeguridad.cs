﻿using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class SubDireccionSeguridad:IDirecciones
    {
        public override int getIdTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                var tec = (from u in dbc.tecnico
                           where u.descripcion == "Seguridad"
                           select u.id_tecnico).First();
                idTecnico = tec;
            }
            return idTecnico;
        }

        public override IAreaIncidencia getAreaEntrada()
        {
            int idArea = 0;
            using (var dbc = new IncidenciasEntities())
            {
                var area = (from u in dbc.area
                            where u.descripcion == "Entrada Seguridad"
                            select u).First();
                idArea = area.id_area;
            }
            return AreaIncidenciaFactory.getArea(idArea);
        }
    }
}