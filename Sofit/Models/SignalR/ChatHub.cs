﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using Sofit.DataAccess;
using soporte.Models;
using Sofit.Models.ClasesVistas;

namespace Sofit.Models.SignalR
{
    [HubName("Chat")]
    public class ChatHub : Hub
    {
        public void Hello()
        {
            int id = Int32.Parse(Context.QueryString["idUsuario"]);
            string connectid = Context.ConnectionId;
            using (var dbc = new IncidenciasEntities())
            {
                var viejoLogin = dbc.ChatUsers.Where(c => c.id_usuario == id & !c.fechaHoraHasta.HasValue);
                foreach (var x in viejoLogin)
                {
                    x.fechaHoraHasta = DateTime.Now;
                }
                ChatUsers nuevo = new ChatUsers
                {
                    id_usuario = id,
                    fechaHoraDesde = DateTime.Now,
                    connectionId = connectid
                };
                dbc.ChatUsers.Add(nuevo);
                dbc.SaveChanges();
                //List<OnUsers> users = null;
                var IdsToFind = dbc.ChatUsers.Where(c => c.fechaHoraHasta == null).Select(d => d.id_usuario).ToList();
                var grap = dbc.Usuarios.Where(c => IdsToFind.Contains(c.id_usuario) & c.activo.Value == true & c.es_interno == true).Select(d => new OnUsers
                {
                    Id = d.id_usuario,
                    Avatar = "/Content/img/Photo/" + d.avatar,
                    Direccion = d.Direcciones.nombre,
                    Nombre = d.nombre_completo,
                    Online = true,
                    MensajesNoLeidos = dbc.LogsChat.Any(f => f.id_from==d.id_usuario & f.id_to == id & !f.leido.HasValue)
                }).ToList();
                var grep = dbc.Usuarios.Where(c => !IdsToFind.Contains(c.id_usuario) & c.activo.Value == true & c.es_interno==true).Select(d => new OnUsers
                {
                    Id = d.id_usuario,
                    Avatar = "/Content/img/Photo/" + d.avatar,
                    Direccion = d.Direcciones.nombre,
                    Nombre = d.nombre_completo,
                    Online = false,
                    MensajesNoLeidos = dbc.LogsChat.Any(f => f.id_from == d.id_usuario & f.id_to == id & !f.leido.HasValue)
                }).ToList();
                grap.AddRange(grep);
                var group = grap.GroupBy(c => c.Direccion).Select(d => new UserByDir
                {
                    Direccion = d.Key,
                    Usuarios = d.Select(f => new OnUsers { Id = f.Id, Avatar = f.Avatar, Direccion = f.Direccion, Nombre = f.Nombre, Online = f.Online, MensajesNoLeidos=f.MensajesNoLeidos }).ToList()
                });
                var usuarioO = dbc.Usuarios.Where(c => c.id_usuario == id).Single();
                OnUsers wentOn = new OnUsers
                {
                    Avatar = "/Content/img/Photo/" + usuarioO.avatar,
                    Direccion = usuarioO.Direcciones.nombre,
                    Id = usuarioO.id_usuario,
                    Nombre = usuarioO.nombre_completo,
                    Online = true
                };                
                Clients.Client(connectid).ChatUsers(group);
                Clients.All.Conected(wentOn);
            }
        }
        class UserByDir
        {
            public string Direccion { get; set; }
            public List<OnUsers> Usuarios { get; set; }
        }
        public override Task OnConnected()
        {
            
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            int id = Int32.Parse(Context.QueryString["idUsuario"]);            
            using (var dbc = new IncidenciasEntities())
            {
                var user=dbc.ChatUsers.Where(c =>c.id_usuario==id & !c.fechaHoraHasta.HasValue);
                if (user.Count() > 0)
                {
                    var usuario = user.First().Usuarios;

                    OnUsers wentOff = new OnUsers
                    {
                        Avatar = "/Content/img/Photo/" + usuario.avatar,
                        Direccion = usuario.Direcciones.nombre,
                        Id = usuario.id_usuario,
                        Nombre = usuario.nombre_completo,
                        Online = false
                    };
                    Clients.All.Disconected(wentOff);
                }
                foreach (var x in user)
                {                  
                    x.fechaHoraHasta = DateTime.Now;                                     
                }
                dbc.SaveChanges();
                
            }            
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            return Clients.All.broadcastMessage("User reconnected with id: " + Context.ConnectionId);
        }
        public void EnviarMensajeA(int id_from,int id_to,string mensaje)
        {            
            using (var dbc = new IncidenciasEntities())
            {

                var usuarioFrom = dbc.Usuarios.Where(c => c.id_usuario == id_from).Single();
                var usuarioTo = dbc.Usuarios.Where(c => c.id_usuario == id_to).Single();
                var newc = new LogsChat
                {
                    descripcion = mensaje,
                    fechaHora = DateTime.Now,
                    id_from = id_from,
                    id_to = id_to

                };
                dbc.LogsChat.Add(newc);
                dbc.SaveChanges();
                MensajesChat mChat = new MensajesChat();
                mChat.Id = newc.id;
                mChat.Id_From = id_from;
                mChat.NombreFrom = usuarioFrom.nombre_completo;
                mChat.NombreTo = usuarioTo.nombre_completo;
                mChat.Id_To = id_to;
                mChat.Mensaje = mensaje;
                mChat.fechahora = DateTime.Now;
                mChat.Avatar= "/Content/img/Photo/" + usuarioFrom.avatar;
                //busco el idconnection
                var cliente = dbc.ChatUsers.Where(c => c.id_usuario == id_to & c.fechaHoraHasta == null).SingleOrDefault();                
                if (cliente != null)
                {                    
                    var connectionId = cliente.connectionId;
                    //envío el mje al destino
                    Clients.Client(connectionId).mensajeAmi(mChat);
                }
                //else Clients.Caller.error("No se encontró el usuario");
                
    
            }
        }
    }
}