﻿using Microsoft.AspNet.SignalR;
using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace Sofit.Models.SignalR
{
    public class BackgroundMessageTimer : IRegisteredObject
    {
        private Timer _timer,_timer2;
        private readonly IHubContext _MessageHub;
        private readonly IHubContext _ChatHub;
        public BackgroundMessageTimer()
        {
            _MessageHub = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
            _ChatHub = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            StartTimer();
        }

        private void StartTimer()
        {
            var delayStartby = TimeSpan.FromMilliseconds(90000);
            var repeatEvery = TimeSpan.FromMilliseconds(60000);
            _timer = new Timer(BroadCastMessage, null, delayStartby, repeatEvery);
            //desconectar inactivos
            var delayStartby2 = TimeSpan.FromHours(1);
            var repeatEvery2 = TimeSpan.FromHours(1);
            //var delayStartby2 = TimeSpan.FromMilliseconds(10000);
            //var repeatEvery2 = TimeSpan.FromMilliseconds(30000);
            _timer2 = new Timer(Disconnection, null, delayStartby2, repeatEvery2);
        }
        private void Disconnection(object state)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var ou = dbc.ChatUsers.Where(c => c.fechaHoraHasta == null);
                foreach(var us in ou)
                {
                    var dif = DateTime.Now - us.fechaHoraDesde;
                    if (dif.TotalHours > 6)
                    {
                        _MessageHub.Clients.Client(us.connectionId).disconnect();
                        Logs newlog = new Logs
                        {
                            descripcion = "Se desconectó " + us.Usuarios.nombre_completo + " cuando pasaron " + dif.Hours + " horas y " + dif.Minutes + " minutos a las " + DateTime.Now.ToString()
                        };
                        dbc.Logs.Add(newlog);
                        
                    }
                }
                dbc.SaveChanges();
            }
        }
        private void BroadCastMessage(object state)
        {
            string message = string.Empty;
            using (var dbc = new IncidenciasEntities())
            {
                var c = dbc.configs.FirstOrDefault();
                message = c.mensaje;
            }
           
            //message += new Random().Next(1, 9).ToString();
            _MessageHub.Clients.All.broadcastMessage(message);           
        }
        public IHubContext getChatContext()
        {
            return _ChatHub;
        }
        public void Stop(bool immediate)
        {
            _timer.Dispose();
            _timer2.Dispose();
            HostingEnvironment.UnregisterObject(this);
        }

    }
}