﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Sofit.DataAccess;
using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;

namespace Sofit.Models.SignalR
{
    [HubName("Mensajes")]
    public class MessageHub:Hub
    {       
        public void Hello()
        {
            string id = Clients.Caller.idUsuario;            
            string connectid = Context.ConnectionId;                       
        }       
        public override Task OnConnected()
        {
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {            
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
    }
}