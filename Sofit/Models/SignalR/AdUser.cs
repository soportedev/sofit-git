﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Sofit.DataAccess;

namespace Sofit.Models.SignalR
{
    public class AdUser : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }
        public override Task OnConnected()
        {
            //string nombre = Context.QueryString["usuario"];
            //using (var dbc = new IncidenciasEntities())
            //{
            //    var actualLogin = dbc.LoginAD.Where(c => c.nombre == nombre & !c.fechaHoraHasta.HasValue);
            //    if (actualLogin.Count() != 1)
            //    {
            //        Clients.Caller.disconnect();
            //    }
            //    dbc.SaveChanges();
            //}
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            //string nombre = Context.QueryString["usuario"];
            //using (var dbc = new IncidenciasEntities())
            //{
            //    var viejoLogin = dbc.LoginAD.Where(c => c.nombre == nombre & !c.fechaHoraHasta.HasValue);
            //    foreach (var x in viejoLogin)
            //    {
            //        x.fechaHoraHasta = DateTime.Now;
            //    }
            //    dbc.SaveChanges();
            //}
            return base.OnDisconnected(stopCalled);
        }
    }
}