﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using soporte.Models.Statics;
using soporte.Models;
using soporte.Models.DB;
using soporte.Areas.Soporte.Models.Statics;
using soporte.Areas.Soporte.Models;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Interface;

namespace soporte.Models
{
    public class usuarioHandler
    {        
        public UsuariosDB dbAccess;
        public usuarioBean usuario;
        public usuarioHandler()
        {
            this.dbAccess = new UsuariosDB();            
        }       
        internal bool validarPassword(int id, string passActual)
        {
            return dbAccess.validarPassword(id, passActual);
        }
        //internal usuarioBean validar(string displayn,string nombre,string mail)
        //{
        //    usuarioBean user = crear(displayn,nombre,mail);
        //    if (user!=null&&user.Activo)
        //    {                
        //        this.usuario = user;
        //        return user;                
        //    }
        //    return null;
        //}        
        /**
         * Se actualizan las tablas usuario y perfiles
         **/
        public Responses saveDB(usuarioBean usuario)
        {
            Responses result = new Responses();            
                           
                using (var dbc = new IncidenciasEntities())
                {
                    Usuarios usuarioBase = (from us in dbc.Usuarios
                                        where us.id_usuario == usuario.IdUsuario
                                        select us).Single();
                    usuarioBase.nombre = usuario.Nombre;
                    usuarioBase.nombre_completo = usuario.NombreCompleto;
                    //usuarioBase.password = usuario.Password;
                    usuarioBase.personalizacion = usuario.Personalizacion;
                    usuarioBase.activo = usuario.Activo;
                    usuarioBase.Perfiles.Clear();
                    foreach (Perfil perfil in usuario.getPerfiles())
                    {
                        Perfiles p = (from per in dbc.Perfiles
                                      where per.id_perfil == perfil.id
                                      select per).Single();
                        usuarioBase.Perfiles.Add(p);
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                return result;
           
        }       

        internal Responses setLoginToThis(int idUsuario, string login)
        {
            Responses result = new Responses();
            if (result.Info.Equals("ok"))
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Usuarios usuarioBase = (from us in dbc.Usuarios
                                            where us.id_usuario == usuario.IdUsuario
                                            select us).Single();
                    usuarioBase.nombre = usuario.Nombre;
                    usuarioBase.nombre_completo = usuario.NombreCompleto;                    
                    usuarioBase.personalizacion = usuario.Personalizacion;
                    usuarioBase.activo = usuario.Activo;
                    usuarioBase.Perfiles.Clear();
                    foreach (Perfil perfil in usuario.getPerfiles())
                    {
                        Perfiles p = (from per in dbc.Perfiles
                                      where per.id_perfil == perfil.id
                                      select per).Single();
                        usuarioBase.Perfiles.Add(p);
                    }
                    dbc.SaveChanges();
                }
                return result;
            }
            else return result;
        }

        //internal usuarioBean crear(string DispN, string nombre, string mail)
        //{
        //    usuarioBean usuario = crearUsuarioPorNombre(DispN,nombre,mail);
        //    if(usuario!=null)usuario.handler = this;
        //    return usuario;
        //}
        public usuarioBean validarNombre(string nombre)
        {
            usuarioBean user = materializar(nombre);            
            this.usuario = user;
            return user;       
            
        }
        private usuarioBean materializar(string nombre)
        {
            usuarioBean usuario = null;
            using (var dbc = new IncidenciasEntities())
            {
                var r = (from us in dbc.Usuarios
                         where us.nombre == nombre
                         select us).SingleOrDefault();
                if (r != null)
                {
                    usuario = new usuarioBean();
                    usuario.Activo = r.activo.Value;
                    usuario.Firma = r.firma;
                    usuario.IdUsuario = r.id_usuario;
                    usuario.Nombre = r.nombre;
                    usuario.NombreCompleto = r.nombre_completo;
                    usuario.Personalizacion = r.personalizacion;
                    usuario.email = r.email;
                    usuario.Avatar = r.avatar;
                    usuario.EsInterno = r.es_interno.Value;
                    usuario.Roles = r.Roles.Select(c => new RolUsuario { Id = c.Id, Nombre = c.Nombre }).ToList();
                    if (usuario.EsInterno)
                    {
                        usuario.idDireccion = r.Direcciones.id_direccion;
                        usuario.Direccion = new DireccionVista { idDireccion = r.Direcciones.id_direccion, Nombre = r.Direcciones.nombre };
                        usuario.Acceso = r.acceso;
                        foreach (Perfiles p in r.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }
                        foreach (Ventana v in r.Ventana)
                        {
                            VentanaUsuario ventana = new VentanaUsuario();
                            ventana.id = v.id_ventana;
                            ventana.height = v.height;
                            ventana.width = v.width;
                            ventana.visible = v.visible;
                            usuario.AddVentana(ventana);
                        }
                        
                    }
                }
            }

            if (usuario != null) usuario.handler = this;
            return usuario;
        }
        public usuarioBean CrearUsuarioExterno(string nombre, string nombreCompleto,string email)
        {
            usuarioBean usuario = new usuarioBean();
            usuario.handler = this;
            usuario.Nombre = nombre;
            usuario.email = email;
            usuario.NombreCompleto = nombreCompleto;
            using (var dbc = new IncidenciasEntities())
            {
                Usuarios nuevo = new Usuarios
                {
                    nombre = nombre,                    
                    nombre_completo = nombreCompleto,
                    activo = true,
                    acceso = 1,
                    personalizacion = 2,
                    es_interno = false,
                    email=email
                };
                dbc.Usuarios.Add(nuevo);
                dbc.SaveChanges();
                usuario.IdUsuario = nuevo.id_usuario;
            }
            return usuario;
        }
        
        internal usuarioBean crear(int userId)
        {
            usuarioBean usuario = new usuarioBean();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.id_usuario == userId
                             select us).SingleOrDefault();

                    usuario.Acceso = r.acceso;
                    usuario.Activo = r.activo.Value;
                    usuario.Firma = r.firma;
                    usuario.IdUsuario = r.id_usuario;
                    usuario.Nombre = r.nombre;
                    usuario.NombreCompleto = r.nombre_completo;                    
                    usuario.Personalizacion = r.personalizacion;
                    usuario.Avatar = r.avatar;
                    usuario.handler = this;
                    usuario.EsInterno = r.es_interno.Value;
                    usuario.Roles = r.Roles.Select(c => new RolUsuario { Id = c.Id, Nombre = c.Nombre }).ToList();
                    foreach (Perfiles p in r.Perfiles)
                    {
                        usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                    }
                    foreach (Ventana v in r.Ventana)
                    {
                        VentanaUsuario ventana = new VentanaUsuario();
                        ventana.id = v.id_ventana;
                        ventana.height = v.height;
                        ventana.width = v.width;
                        ventana.visible = v.visible;
                        usuario.AddVentana(ventana);
                    }
                }                
            }
            catch (Exception)
            {
                usuario = null;
            }
            return usuario;
        }

        internal Responses nvoUsuario(string nombre, string displayName, int  idPerfil,IDirecciones subdireccion,bool esInterno)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var encontrado = dbc.Usuarios.Where(c => c.nombre == nombre | c.nombre_completo == displayName);
                if (encontrado.Count() >0)
                {
                    result.Detail = "Ya existe otro usuario con ese Login o Nombre";
                }
                else
                {
                    try
                    {                        
                        Usuarios nuevo = new Usuarios
                        {
                            id_direccion = subdireccion.IdDireccion,
                            firma = "",
                            nombre = nombre,
                            nombre_completo = displayName,                            
                            activo = true,
                            acceso = 1,
                            personalizacion=2,
                            es_interno=esInterno
                        };
                        if (esInterno)
                        {
                            Perfiles p = dbc.Perfiles.Where(c => c.id_perfil == idPerfil).Single();
                            nuevo.Perfiles.Add(p);
                            tecnico nuevoT = new tecnico
                            {
                                activo = true,
                                descripcion = nuevo.nombre_completo,
                                id_area = subdireccion.getAreaEntrada().IdArea,
                                id_usuario = nuevo.id_usuario
                            };
                            dbc.tecnico.Add(nuevoT);

                        }
                        dbc.Usuarios.Add(nuevo);                       
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    catch (Exception e)
                    {
                        result.Detail = e.Message;
                    }
                }
            }
            return result;
        }       
        internal void saveVentanaDB(VentanaUsuario ventana, usuarioBean usuario)
        {
            
            using(var dbc=new IncidenciasEntities())
            {
                var vent = dbc.Ventana.Where(c => c.id_usuario == usuario.IdUsuario & c.id_ventana == ventana.id).Single();
                vent.visible = ventana.visible;
                vent.height = ventana.height;
                vent.width = ventana.width;
                dbc.SaveChanges();
            }            
        }
        internal void crearVentana(string p,usuarioBean usuario)
        {
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Ventana nueva = new Ventana
                    {
                        width = 550,
                        height = 300,
                        visible = true,
                        id_usuario = usuario.IdUsuario,
                        id_ventana = p
                    };
                    dbc.Usuarios.Where(c => c.id_usuario == usuario.IdUsuario).Single().Ventana.Add(nueva);
                    dbc.SaveChanges();
                }
            }catch(Exception e)
            {

            }
        }
        public usuarioBean crearUsuarioPorNombre(string DispN, string nombre, string email)
        {
            return dbAccess.getUsuarioXNombre(DispN, nombre, email);           
        }        
        internal List<usuarioBean> getTodosDeDireccion(IDirecciones direccion)
        {            
            return dbAccess.getTodos(direccion);
        }
        internal List<usuarioBean> getTodos()
        {
            return dbAccess.getTodos();
        }
        internal Responses QuitarPerfilAlUsuario(int idUsuario, string perfil)
        {
            usuarioBean usuario = crear(idUsuario);
            return usuario.QuitarPerfil(PerfilFactory.getInstancia().crear(perfil));            
        }
        internal Responses AgregarPerfilAlUsuario(int idUsuario, int perfil)
        {
            usuarioBean esteUsuario = crear(idUsuario);
            return esteUsuario.AgregarPerfil(PerfilFactory.getInstancia().crear(perfil));
        }
        internal Responses togleStatusDelUsuario(int p)
        {
            return dbAccess.toggleUsuario(p);
        }       
        internal Responses updateFirma(usuarioBean usuarioBean)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.id_usuario == usuarioBean.IdUsuario
                             select us).SingleOrDefault();
                    r.firma = usuarioBean.Firma;
                    r.email = usuarioBean.email;
                    r.avatar = usuarioBean.Avatar;
                    dbc.SaveChanges();
                    result.Info = "ok";                    
                }
                reloadUsuario();
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return result;
        }

        private void reloadUsuario()
        {            
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.id_usuario == usuario.IdUsuario
                             select us).SingleOrDefault();

                    usuario.Acceso = r.acceso;
                    usuario.Activo = r.activo.Value;
                    usuario.Firma = r.firma;
                    usuario.IdUsuario = r.id_usuario;
                    usuario.Nombre = r.nombre;
                    usuario.NombreCompleto = r.nombre_completo;                    
                    usuario.Personalizacion = r.personalizacion;
                    usuario.Avatar = r.avatar;
                    foreach (Perfiles p in r.Perfiles)
                    {
                        usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                    }
                    foreach (Ventana v in r.Ventana)
                    {
                        VentanaUsuario ventana = new VentanaUsuario();
                        ventana.id = v.id_ventana;
                        ventana.height = v.height;
                        ventana.width = v.width;
                        ventana.visible = v.visible;
                        usuario.AddVentana(ventana);
                    }
                }
                HttpContext.Current.Session["usuario"] = usuario;
            }
            catch (Exception)
            {
                usuario = null;
            }            
        }
        internal List<usuarioBean> getUsuariosTelecomunicaciones()
        {
            return this.dbAccess.getUsuariosTele();
        }
        internal Responses setPasswordFor(int userid, string pass)
        {
            return dbAccess.setPasswordFor(userid, pass);
        }

        internal List<usuarioBean> getUsuariosMesa()
        {
            return this.dbAccess.getUsuariosMesa();
        }

        internal List<usuarioBean> getUsuariosOperaciones()
        {
            return this.dbAccess.getUsuariosOperaciones();
        }        
    }
}
