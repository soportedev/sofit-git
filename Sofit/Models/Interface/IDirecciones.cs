﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Interface
{
    public abstract class IDirecciones
    {
        public int IdDireccion { get; set; }
        public string Nombre { get; set; }
        public abstract int getIdTecnicoGenerico();
        public abstract IAreaIncidencia getAreaEntrada();
    }
}