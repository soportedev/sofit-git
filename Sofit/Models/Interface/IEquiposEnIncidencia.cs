﻿using soporte.Areas.Operaciones.Models;
using soporte.Areas.Soporte.Models;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Interface
{
    public abstract class IEquiposEnIncidencia
    {
        public Incidencia ticket;
        public List<EquipoEnIncidente> equiposSoporte;
        public List<EnlaceOperaciones> enlaces;
        public abstract Responses agregarEquipoAIncidencia(string oble, int id_area, int idTecnico);
        public abstract Responses derivarEquipo(int idEq, AreaEquipo area, string observaciones, int idUsuario, int idTecnico);
        public abstract Responses buscarEqPendientes(string texto);
        public abstract Responses quitarEquipoAIncidencia(string oblea);
        public abstract Responses cargarSolucionEquipo(int idUsuario, int idEquipo, int tipoResolucion, int tecnico, int turno, int? imagen, string nombrefile, string observacion, bool updateResolucion);
        public abstract Responses terminarEquipo(int id_eq, int idUsuario);
        public abstract EquipoEnIncidente getEquipoXOblea(string oblea);
        public abstract Responses notificarEquipo(int idEquipo, string[]referentes,string firma,string cuerpo,string subject);
        public abstract EquipoEnIncidente getEquipoXId(int idEquipo);
        public abstract Responses cambioEstadoEquipo(int idEquipo);
        public abstract Responses cambioAreaEquipo(int idEquipo);
        internal abstract bool tieneEquiposActivos();
        internal abstract string getEquiposSinTerminar();
        internal abstract Responses puedeReactivarTicket();
        
    }
}