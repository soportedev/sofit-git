﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Models.Interface
{
    public abstract class IAreaIncidencia
    {
        public int IdArea { get; set; }
        public string NombreArea { get; set; }
        public string  NombreDireccion { get; set; }
        public int idDireccion { get; set; }
        public Incidencia Incidencia { get; set; }
        public abstract int getIdTecnicoGenerico();        
        public abstract Responses derivar(int idArea, int tecnico, int usuario);
        public abstract Responses solucionar(int idUsuario);
        public abstract Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo,DateTime fechaActualizar, string imagen);

    }
}
