﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Models.Interface
{
    public abstract class IEstadoIncidencia
    {
        public int IdEstado { get; set; }
        public string NombreEstado { get; set; }
        public Incidencia Incidencia { get; set; }
        public abstract Responses derivar(string area, int idTecnico,int idUsuario);
        public abstract Responses solucionar(int idUsuario);
        public abstract Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo,DateTime fechaActualizar, string imagen);
    }
}
