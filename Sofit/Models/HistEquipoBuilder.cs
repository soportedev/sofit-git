﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class HistEquipoBuilder
    {
        private HistoricoEquipo historico;
        public void Construir(string nroOblea, bool movAreas)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var eq = from u in dbc.UnidadConf
                         where u.nombre == nroOblea
                         select u;
                if (eq != null & eq.Count() > 0)
                {
                    UnidadConf primero = eq.Single();
                    historico = new HistoricoEquipo
                    {
                        idEquipo = primero.id_uc,
                        Oblea = primero.nombre,
                        Tipo = primero.Tipo_equipo.descripcion,
                        PasswordAdmin = (primero.Pc.Count > 0 ? primero.Pc.Where(z => z.id_uc == primero.id_uc).Single().clave_adm : "N/A"),
                        PasswordBios = (primero.Pc.Count > 0 ? primero.Pc.Where(z => z.id_uc == primero.id_uc).Single().clave_bios : "N/A")
                    };
                    //codigo de areaxequipo
                    //Equipo primero = eq.First();
                    //HistoricoEquipo historico = new HistoricoEquipo
                    //{
                    //    idEquipo = primero.id_equipo,
                    //    Oblea = primero.nro_oblea,
                    //    Tipo = primero.Tipo_equipo.descripcion,
                    //    PasswordAdmin = (primero.Pc.Count > 0 ? primero.Pc.Where(z => z.id_equipo == primero.id_equipo).Single().clave_adm : "N/A"),
                    //    PasswordBios = (primero.Pc.Count >0 ? primero.Pc.Where(z => z.id_equipo == primero.id_equipo).Single().clave_bios : "N/A")
                    //};                    

                    //         var axe= from u in dbc.AreaXEquipoN
                    //                  where u.id_equipo == historico.idEquipo
                    //                  group u by u.id_incidencia into g
                    //                  orderby g.Key
                    //                  select g;
                    //         var axeH = from u in dbc.AreaXEquipoHistorico
                    //                    where u.id_equipo == historico.idEquipo
                    //                    group u by u.id_incidencia into g
                    //                    orderby g.Key
                    //                    select g;

                    //if (axe.Count() > 0)
                    //{
                    //    foreach (var movimiento in axe)
                    //    {
                    //        var firstMov = movimiento.First();
                    //        HistoricoIncidencia incidencia = new HistoricoIncidencia
                    //        {
                    //            Numero = firstMov.Incidencias.numero,
                    //            FechaInicio = firstMov.fecha_inicio,
                    //            FechaFin = firstMov.Incidencias.fecha_fin!=null?firstMov.Incidencias.fecha_fin.Value:(DateTime?)null,
                    //            Dependencia = firstMov.Incidencias.Dependencia != null ? firstMov.Incidencias.Dependencia.descripcion : "N/A",
                    //            Jurisdiccion = firstMov.Incidencias.Dependencia != null ? firstMov.Incidencias.Dependencia.Jurisdiccion.descripcion : firstMov.Incidencias.Jurisdiccion.descripcion

                    //        };
                    //        historico.Incidentes.Add(incidencia);
                    //        //Debug.WriteLine("Key: {0}", movimiento.Key);
                    //        //foreach (var mov in movimiento)
                    //        //{
                    //        //    Debug.WriteLine("\t{0}, {1}", mov.id_incidencia, mov.fecha_inicio);
                    //        //}
                    //    }
                    //}
                    //else
                    //{

                    var resoluciones = from u in dbc.Resoluciones
                                       where u.id_uc == primero.id_uc
                                       select u;
                    var resHistorico = from u in dbc.ResolucionesHistorico
                                       where u.id_uc == primero.id_uc
                                       select u;
                    if (resoluciones != null)
                    {
                        foreach (var res in resoluciones)
                        {
                            HistoricoIncidencia incidencia = new HistoricoIncidencia
                            {
                                Numero = res.Incidencias.numero,
                                FechaInicio = res.Incidencias.fecha_inicio,
                                FechaFin = res.Incidencias.fecha_fin.HasValue ? res.Incidencias.fecha_fin.Value : (DateTime?)null,
                                Dependencia = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.descripcion : "N/A",
                                Jurisdiccion = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.Jurisdiccion.descripcion : res.Incidencias.Jurisdiccion.descripcion
                            };
                            if (!historico.Incidentes.Contains(incidencia))
                                historico.Incidentes.Add(incidencia);
                        }
                    }
                    if (resHistorico != null)
                    {
                        foreach (var res in resHistorico)
                        {
                            HistoricoIncidencia incidencia = new HistoricoIncidencia
                            {
                                Numero = res.Incidencias.numero,
                                FechaInicio = res.Incidencias.fecha_inicio,
                                FechaFin = res.Incidencias.fecha_fin.HasValue ? res.Incidencias.fecha_fin.Value : (DateTime?)null,
                                Dependencia = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.descripcion : "N/A",
                                Jurisdiccion = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.Jurisdiccion.descripcion : res.Incidencias.Jurisdiccion.descripcion
                            };
                            if (!historico.Incidentes.Contains(incidencia))
                                historico.Incidentes.Add(incidencia);
                        }
                    }
                }
            }

        }
        internal HistoricoEquipo getResult()
        {
            return historico;
        }
        public void ConstruirHistoricoCorto(int idEquipo)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var eq = from u in dbc.UnidadConf
                         where u.id_uc == idEquipo
                         select u;
                if (eq != null & eq.Count() > 0)
                {
                    UnidadConf primero = eq.Single();
                    historico = new HistoricoEquipo
                    {
                        idEquipo = primero.id_uc,
                        Oblea = primero.nombre,
                        Tipo = primero.Tipo_equipo.descripcion,
                        PasswordAdmin = (primero.Pc.Count > 0 ? primero.Pc.Where(z => z.id_uc == primero.id_uc).Single().clave_adm : "N/A"),
                        PasswordBios = (primero.Pc.Count > 0 ? primero.Pc.Where(z => z.id_uc == primero.id_uc).Single().clave_bios : "N/A")
                    };
                    historico.Incidentes = dbc.AreaXEquipoN.Where(c => c.id_equipo == primero.id_uc).GroupBy(c => new { c.Incidencias.numero,c.Incidencias.fecha_inicio }).Select(c=>new HistoricoIncidencia { Numero=c.Key.numero, FechaInicio=c.Key.fecha_inicio}).ToList();
                    historico.Incidentes.AddRange(dbc.AreaXEquipoHistorico.Where(c => c.id_equipo == primero.id_uc).GroupBy(c => new { c.Incidencias.numero, c.Incidencias.fecha_inicio }).Select(c => new HistoricoIncidencia { Numero = c.Key.numero, FechaInicio = c.Key.fecha_inicio }).ToList());
                    historico.Incidentes.Sort();                  

                }

            }
        }
    }
}