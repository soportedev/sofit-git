﻿using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class EquipoEnIncidenciaNull:IEquiposEnIncidencia
    {
        public EquipoEnIncidenciaNull()
        {

        }        

        public override Statics.Responses derivarEquipo(int idEq, AreaEquipo area, string observaciones, int idUsuario, int idTecnico)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses buscarEqPendientes(string texto)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses quitarEquipoAIncidencia(string oblea)
        {
            throw new NotImplementedException();
        }


        public override Statics.Responses terminarEquipo(int id_eq, int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override Areas.Soporte.Models.EquipoEnIncidente getEquipoXOblea(string oblea)
        {
            throw new NotImplementedException();
        }        

        public override Areas.Soporte.Models.EquipoEnIncidente getEquipoXId(int idEquipo)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses cambioEstadoEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses cambioAreaEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        internal override bool tieneEquiposActivos()
        {
            return false;
        }

        internal override string getEquiposSinTerminar()
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses agregarEquipoAIncidencia(string oble, int id_area, int idTecnico)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses cargarSolucionEquipo(int idUsuario, int idEquipo, int tipoResolucion, int tecnico, int turno,int?imagen, string file, string observacion, bool update)
        {
            throw new NotImplementedException();
        }

        public override Statics.Responses notificarEquipo(int idEquipo, string[] referentes, string firma, string cuerpo, string subject)
        {
            throw new NotImplementedException();
        }

        internal override Statics.Responses puedeReactivarTicket()
        {
            return new Statics.Responses { Info = "ok" };
        }
    }
}