﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models
{
    public class OnUsers
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Avatar { get; set; }
        public string Direccion { get; set; }
        public bool Online { get; set; }
        public bool MensajesNoLeidos { get; set; }
    }
}