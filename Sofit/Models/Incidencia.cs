﻿using soporte.Areas.Soporte.Models;
using soporte.Areas.Soporte.Models.Interface;
using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace soporte.Models
{
    public class Incidencia
    {
        public string Numero { get; set; }
        public int Id { get; set; }
        private IAreaIncidencia backAreaActual;
        private IEstadoIncidencia backEstadoActual;
        private Tipificacion backTipificacion = new Tipificacion();
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int idTecnicoActual { get; set; }
        public List<MovAreaIncidencia> MovAreas { get; set; }
        public string Tipificacion {
            get
            {
                return backTipificacion.getNombreCompleto();
            }
            set
            {
                backTipificacion.idTipificacion = Int16.Parse(value);
            }
        }
        public List<MovAreaIncidencia> AreasActuantes { get; set; }
        public IAreaIncidencia AreaActual
        {
            get
            {
                return backAreaActual;
            }
            set
            {
                backAreaActual = value;
                backAreaActual.Incidencia = this;
            }
        }
        public IEquiposEnIncidencia EquiposDecorator { get; set; }
        public IEstadoIncidencia EstadoActual
        {
            get
            {
                return backEstadoActual;
            }
            set
            {
                backEstadoActual = value;
                backEstadoActual.Incidencia = this;
            }
        }        
        internal Responses cambioEstado(IEstadoIncidencia nuevoEstado, int idTecnico, int idUsuario)
        {
            this.EstadoActual = nuevoEstado;
            return this.RegistrarBaseCambioEstado(idTecnico, idUsuario);
        }
        private Responses RegistrarBaseCambioEstado(int idTecnico, int idUsuario)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var incAct = (from u in dbc.IncidenciasActivas
                                  where u.Incidencias.numero == this.Numero
                                  select u).Single();
                    incAct.id_estado = EstadoActual.IdEstado;
                    incAct.id_tecnico = idTecnico;
                    EstadoXIncidencia ax = (from u in dbc.EstadoXIncidencia
                                          where u.id_incidencia == this.Id & !u.fecha_fin.HasValue
                                          select u).Single();
                    ax.fecha_fin = DateTime.Now;
                    EstadoXIncidencia axi = new EstadoXIncidencia
                    {
                        id_estado = this.EstadoActual.IdEstado,
                        id_incidencia = this.Id,
                        fecha_inicio = DateTime.Now,
                        id_usuario = idUsuario
                    };
                    dbc.EstadoXIncidencia.Add(axi);
                    dbc.SaveChanges();
                }

                result.Info = "ok";
            }
            catch (Exception e)
            {
                result.Info = "Error";
                result.Detail = e.Message;
            }
            return result;
        }
        public Responses cambioArea(IAreaIncidencia area, int idTecnico, int idUsuario)
        {
            this.AreaActual = area;
            return this.RegistrarBaseMovimientoArea(idTecnico, idUsuario);
        }
        private Responses RegistrarBaseMovimientoArea(int idTecnico, int idUsuario)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var incAct = (from u in dbc.IncidenciasActivas
                                  where u.Incidencias.numero == this.Numero
                                  select u).Single();
                    incAct.id_area = AreaActual.IdArea;
                    incAct.id_tecnico = idTecnico;
                    AreaXIncidencia ax = (from u in dbc.AreaXIncidencia
                                          where u.id_incidencia == this.Id & !u.fecha_fin.HasValue
                                          select u).Single();
                    ax.fecha_fin = DateTime.Now;
                    AreaXIncidencia axi = new AreaXIncidencia
                    {
                        id_area = this.AreaActual.IdArea,
                        id_incidencia = this.Id,
                        fecha_inicio = DateTime.Now,
                        id_usuario = idUsuario
                    };
                    dbc.AreaXIncidencia.Add(axi);
                    dbc.SaveChanges();
                }

                result.Info = "ok";
            }
            catch (Exception e)
            {
                result.Info = "Error";
                result.Detail = e.Message;
            }
            return result;
        }
        internal Responses derivar(int idAreaDestino, int idTecnico, int idUsuario)
        {
            if (EstadoActual is EstadoIncidenciaSuspendido)
            {
                return new Responses { Info = "error", Detail = "No se puede derivar un ticket estando suspendido!!" };
            }
            else
            {
                return AreaActual.derivar(idAreaDestino, idTecnico, idUsuario);
            }
        }        
        internal Responses derivarIncidenciaADireccion(int idDireccion, int idUsuario)
        {
            Responses result = new Responses();
            //con la condicion de que solo se deriva a direccion desde la bandeja de entrada
            AreasActuantes.Sort();
            MovAreaIncidencia ultimaArea = AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "La Incidencia no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {

                    IDirecciones SubDirDestino = DireccionesFactory.getDireccion(idDireccion);
                    IAreaIncidencia areaDestino = SubDirDestino.getAreaEntrada();
                    result = cambioArea(areaDestino, SubDirDestino.getIdTecnicoGenerico(), idUsuario);
                    if (this.EstadoActual is EstadoIncidenciaIngresado)
                    {
                        cambioEstado(EstadoIncidenciaFactory.getEstado("Derivado"), SubDirDestino.getIdTecnicoGenerico(), idUsuario);
                    }
                    //if (result.Info == "ok")
                    //{
                    //    using (var dbc = new IncidenciasEntities())
                    //    {
                    //        var nombreUsuario = dbc.Usuarios.Where(c => c.id_usuario == idUsuario).Single().nombre_completo;
                    //        string nombreDestino = SubDirDestino.Nombre;
                    //        string subject = "Ticket  " + this.Numero + "  derivado";
                    //        string body = "El Ticket identificado como " + this.Numero + "  fue derivado a " + nombreDestino + "  por " +
                    //            nombreUsuario + " el día " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() +
                    //           " hrs. " + ", y se encuentra pendiente de resolución." +
                    //             ".\n El detalle se encuentra en el siguiente enlace: https://sofit.cba.gov.ar"  +
                    //        ".\n Dirección General de IT.";
                    //        EmailSender sender = new EmailSender();

                    //        var notificaciones = dbc.Incidencias.Where(c => c.id_incidencias == this.Id).Single().NotificacionXIncidente;
                    //        foreach (var notif in notificaciones)
                    //        {
                    //            string direccionMail = notif.email;
                    //            sender.sendEmail(new string[] { direccionMail, subject, body });
                    //        }
                    //    }
                    //}
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar una resolución.";
                }
            }
            return result;
        }        
        //telecomunicaciones lo usa de una en el metodo cerrar...
        public Responses cerrarIncidencia(int idUsuario)
        {
            Responses result = new Responses();
            result = verificarResoluciones();
            if (result.Info == "ok")
            {
                if (EquiposDecorator.tieneEquiposActivos())
                {
                    result.Info = "no";
                    result.Detail = "El Ticket tiene Elementos Activos: " + EquiposDecorator.getEquiposSinTerminar();
                }
                else
                {
                    result = verificarPedidosVigentes(this.Id);
                    if (result.Info == "ok")
                    {
                        IncidenciasManager mgr = new IncidenciasManager();
                        result = mgr.cerrarIncidencia(idUsuario, this);
                    }
                }
            }            
            return result;            
        }
        private Responses verificarPedidosVigentes(int Id)
        {
            Responses result = new Responses();
            bool flagCheck = true;
            using (var dbc = new IncidenciasEntities())
            {
                var pV = dbc.PedidosSeguridad.Where(c => c.id_ticket == Id);
                foreach (var p in pV)
                {
                    if (p.EstadosPedidos.nombre != "Rechazado" & p.EstadosPedidos.nombre != "Realizado") flagCheck = false;
                }
            }
            if (flagCheck) result.Info = "ok";
            else result.Detail = "Este ticket tiene pedidos de acceso a servicios informáticos que aún no han sido resueltos";
            return result;
        }
        public Responses verificarResoluciones()
        {
            Responses result = new Responses();
            AreasActuantes.Sort();
            MovAreaIncidencia ultimaArea = AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "La Incidencia no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    result.Info = "ok";
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar la resolución del Área: " + ultimaArea.Area.NombreArea;
                }
            }
            return result;
        }
        internal Responses derivarEquipo(int id_eq, AreaEquipo area, string observaciones, int usuario, int tecnico)
        {
            return EquiposDecorator.derivarEquipo(id_eq, area, observaciones, usuario, tecnico);
        }

        internal Responses terminarEquipo(int id_eq, int idUsuario)
        {
            return EquiposDecorator.terminarEquipo(id_eq, idUsuario);
        }

        internal Areas.Soporte.Models.EquipoEnIncidente getEquipoXId(int idEquipo)
        {
            return EquiposDecorator.getEquipoXId(idEquipo);
        }
        /// <summary>
        /// Carga resolucion en un ticket. La carga la realiza el area actual
        /// </summary>
        /// <param name="idUsuario">usuario actual</param>
        /// <param name="idEquipo">equipo si corresponde</param>
        /// <param name="tipoResolucion">tipo resolucion</param>
        /// <param name="tecnico">tecnico seteado</param>
        /// <param name="turno">turno seteado</param>
        /// <param name="observacion">descripcion del trabajo</param>
        /// <param name="fechaActualizar">fecha actual</param>
        /// <param name="imagen">path imagen si corresponde</param>
        /// <param name="sobrescribir">si corresonde sobrescribir el trabajo</param>
        /// <returns></returns>
        internal Responses cargarSolucion(int idUsuario, int?idEquipo, int tipoResolucion, int tecnico, int turno, string observacion,DateTime fechaActualizar,string imagen, bool sobrescribir)
        {
            Responses result = new Responses();
            if (sobrescribir)
            {
                ResolucionesIncidencias ultima;
                MovAreaIncidencia ultimoMov = AreasActuantes.Last();
                ultimoMov.Resoluciones.Sort();
                if (ultimoMov.Resoluciones.Count > 0)
                {
                    ultima = ultimoMov.Resoluciones.Last();
                    //List<ResolucionesIncidencias> resParaReescribir = ultimoMov.Resoluciones.Where(c => c.idTipoResolucion == tipoResolucion & c.idTecnico == tecnico).ToList();
                    if (ultima.IdUsuario == idUsuario)
                    {
                        string pathActual = ultima.PathImagen;
                        if (pathActual != string.Empty&pathActual!=null)
                        {
                            try
                            {
                                string pathImagen = PathImage.getResolucionCustom(pathActual);
                                File.Delete(pathImagen);
                            }
                            catch (Exception e)
                            {
                                result.Detail = e.Message;
                            }
                        }
                        try
                        {
                            using (var dbc = new IncidenciasEntities())
                            {
                                var res = (from u in dbc.Resoluciones
                                           where u.id_resolucion == ultima.idResolucion
                                           select u).Single();
                                res.fecha = DateTime.Now;
                                res.observaciones = observacion;
                                res.id_tipo_resolucion = tipoResolucion;
                                res.id_uc = idEquipo;
                                res.id_tecnico = tecnico;
                                res.path_imagen = imagen == string.Empty ? null : imagen;
                                res.id_turno = turno;
                                dbc.SaveChanges();
                                result.Info = "ok";
                            }
                        }
                        catch (Exception e) { result.Detail = e.Message; }
                    }
                    else result.Detail = "El técnico que cargó la última resolución es " + ultima.Tecnico + ". Esta operación no está permitida!!!";
                }
                else
                {
                    result.Detail = "No hay resoluciones previas cargadas!!!";
                }
            }
            else
            {
                result = this.AreaActual.cargarSolucion(observacion, tecnico, turno, idUsuario, tipoResolucion, idEquipo, fechaActualizar, imagen == string.Empty ? null : imagen);
            }            
            return result;
        }
        internal Responses cargarSolucionEquipo(int idUsuario, int idEquipo, int tResolucion, int tecnico, int turno,int?imagen, string file, string observacion, bool update)
        {
            Responses result = new Responses();
            return EquiposDecorator.cargarSolucionEquipo(idUsuario, idEquipo, tResolucion, tecnico, turno, imagen, file, observacion, update);
        }
        internal Responses cargarSolucionEnlace(int idUsuario, int idE, int idTipoResolucion, int idTecnico, int idTurno, int? imagen, string observacion,int area, DateTime fecha){
            Responses result = new Responses();            
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    int idArea = (dbc.area.Where(c => c.descripcion == "Operaciones").First()).id_area;
                    Resoluciones res = new Resoluciones
                    {
                        id_uc = idE,
                        fecha = DateTime.Now,
                        id_incidencia = this.Id,
                        id_tecnico = idTecnico,
                        id_tipo_resolucion = idTipoResolucion,
                        id_turno = idTurno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_area = idArea
                    };
                    //cargo el trabajo
                    dbc.Resoluciones.Add(res);
                    AreasActuantes.Sort();
                    AreasActuantes.Last().Resoluciones.Add(new ResolucionesIncidencias
                    {                        
                        IdEquipo = idE,
                        Fecha = DateTime.Now,                        
                        Observaciones = observacion,                       
                        idTipoResolucion = idTipoResolucion
                    });
                    EnlaceComunicaciones enlace = (from u in dbc.EnlaceComunicaciones where u.id_uc == idE select u).First();
                    int idEstadoEnlace = 0;                    
                    string trabajo = (dbc.tipo_resolucion.Where(c => c.id_tipo_resolucion == idTipoResolucion).First()).descripcion;

                        switch (trabajo)
                        {
                            case "Fin Falla":
                                string nroEnlace=dbc.UnidadConf.Where(c=>c.id_uc==idE).Single().nombre;
                                idEstadoEnlace = (dbc.Estado_UC.Where(c => c.descripcion == "Productivo").First()).id_estado;
                                enlace.id_estado = idEstadoEnlace;
                                dbc.EstadoXEnlace.Where(c => c.id_enlace == enlace.id_enlace & c.id_incidencia == this.Id).First().fecha_fin = fecha;
                                EnlaceXIncidencia exi = dbc.EnlaceXIncidencia.Where(c => c.id_enlace == enlace.id_enlace & c.id_incidencia == this.Id).First();
                                dbc.EnlaceXIncidencia.Remove(exi);
                                EquiposDecorator.quitarEquipoAIncidencia(nroEnlace);
                                this.cerrarIncidencia(idUsuario);
                                break;
                            case "No Productivo":
                                dbc.EstadoXEnlace.Where(c => c.id_enlace == idE & c.id_incidencia == this.Id).First().fecha_fin = fecha;
                                idEstadoEnlace = (dbc.Estado_UC.Where(c => c.descripcion == "No Productivo").First()).id_estado;
                                EstadoXEnlace nuevoEstado = new EstadoXEnlace
                                {
                                    id_enlace = idE,
                                    id_estado = idEstadoEnlace,
                                    id_incidencia = this.Id,
                                    id_tecnico = idTecnico,
                                    fecha_inicio = fecha
                                };
                                enlace.id_estado = idEstadoEnlace;
                                dbc.EstadoXEnlace.Add(nuevoEstado);
                                break;
                            default: ;
                                break;
                        }                   
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }
        internal Responses quitarEquipoaIncidente(string equipo)
        {
            return EquiposDecorator.quitarEquipoAIncidencia(equipo);
        }

        internal Responses agregarEquipoaIncidente(string oblea, int id_area,int idTecnico)
        {
            EquiposDecorator = new EquiposEnIncidenciaSoporte();
            EquiposDecorator.ticket = this;
            return EquiposDecorator.agregarEquipoAIncidencia(oblea, id_area,idTecnico);
        }
        internal Responses notificacionEquipo(int idEquipo, string[] parsed, string firma, string cuerpo, string subject)
        {
            return EquiposDecorator.notificarEquipo(idEquipo, parsed, firma, cuerpo, subject);            
        }

        internal Responses suspenderIncidencia(int idUsuario)
        {
            Responses result = new Responses();
            if (EstadoActual is EstadoIncidenciaSuspendido)
            {
                result.Detail = "Ya está suspendido";
            }
            else
            {
                AreasActuantes.Sort();
                MovAreaIncidencia ultimaArea = AreasActuantes.LastOrDefault();
                if (ultimaArea == null)
                {
                    result.Detail = "La Incidencia no tiene movimientos";
                }
                else
                {
                    if (ultimaArea.tieneResolucion())
                    {
                        IEstadoIncidencia suspendido = EstadoIncidenciaFactory.getEstado("Suspendido");
                        result = cambioEstado(suspendido, this.idTecnicoActual, idUsuario);
                        if (result.Info == "ok")
                        {
                            using (var dbc = new IncidenciasEntities())
                            {
                                var nombreUsuario = dbc.Usuarios.Where(c => c.id_usuario == idUsuario).Single().nombre_completo;
                                string subject = "Ticket  " + this.Numero + "  suspendido";
                                string body = "El Ticket identificado como <b>" + this.Numero + "</b>  fue suspendido por <b>" +
                                    nombreUsuario + "</b> el día " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() +
                                   " hrs. " + ", y se encuentra pendiente de resolución.";
                                    
                                EmailSender sender = new EmailSender();

                                var notificaciones = dbc.Incidencias.Where(c => c.id_incidencias == this.Id).Single().NotificacionXIncidente;
                                foreach (var notif in notificaciones)
                                {
                                    string direccionMail = notif.email;
                                    sender.sendEmail(new string[] { direccionMail, subject, body });
                                }
                            }
                        }
                    }
                    else
                    {
                        result.Info = "error";
                        result.Detail = "Falta cargar la resolución del Área: " + ultimaArea.Area.NombreArea;
                    }
                }
            }
            return result;
        }
        internal void forceSuspenderIncidencia(int idUsuario)
        {
            IEstadoIncidencia suspendido = EstadoIncidenciaFactory.getEstado("Suspendido");
            cambioEstado(suspendido, this.idTecnicoActual, idUsuario);
        }
        internal Responses notificacionTicket(string[] referentes, string firma, string cuerpo, string subject)
        {
            Responses result = new Responses();
            EmailSender sender = new EmailSender();
            bool flagNotificacion = true;
            bool huboError = false;
            StringBuilder sb = new StringBuilder();
            NotificacionTicketSusp noti = new NotificacionTicketSusp();            
            usuarioBean usuarioActual = HttpContext.Current.Session["usuario"] as usuarioBean;
            foreach (string referente in referentes)
            {
                Referentes referenteBuscado;
                string direccionMail = string.Empty;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(referente);
                if (match.Success)
                {
                    direccionMail = referente;
                }
                else
                {
                    try
                    {
                        int idRef = 0;
                        Int32.TryParse(referente, out idRef);
                        using (var dbc = new IncidenciasEntities())
                        {
                            referenteBuscado = (from t in dbc.Referentes
                                                where t.id == idRef
                                                select t).Single();
                        }
                        direccionMail = referenteBuscado.email;
                    }
                    catch (Exception e)
                    {
                        huboError = true;
                        sb.Append(referente).Append(". ");
                        continue;
                    }
                }
                //envio         
                result = sender.sendEmail(new string[] { direccionMail, subject, cuerpo });
                if (result.Info == "ok")
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        if (flagNotificacion)
                        {
                            //inicializo la noti  
                            noti.asunto = subject.Length<1000?subject:subject.Substring(0,999);
                            noti.mensaje = cuerpo.Length<4000?cuerpo:cuerpo.Substring(0,3999);                            
                            noti.id_ticket = this.Id;
                            noti.id_usuario = usuarioActual.IdUsuario;
                            noti.fecha = DateTime.Now;
                            try
                            {
                                dbc.NotificacionTicketSusp.Add(noti);
                                dbc.SaveChanges();
                                result.Info = "ok";
                                //para no guardar mas de una vez la noti
                                flagNotificacion = false;
                            }
                            catch (Exception e)
                            {
                                result.Info = "error";
                                result.Detail = e.Message;
                            }
                            
                        }
                        DireccionMailXNotTicket mailxTicket = new DireccionMailXNotTicket
                        {
                            direccion_mail = direccionMail,
                            id_notif = noti.id
                        };
                        dbc.DireccionMailXNotTicket.Add(mailxTicket);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }//contexto dbc
                }//envio exitoso

                else
                {
                    //error envío
                    huboError = true;
                    sb.Append(referente).Append(". ");
                }
            }//iteracion referentes            
            if (huboError & result.Info == "ok")
            {
                result.Info = "algunos errores";
                result.Detail = sb.ToString();
            }
            return result;
        }

        internal Responses reactivarTicket(int idInc, int idUsuario)
        {
            Responses result = new Responses();
            if (!(this.EstadoActual is EstadoIncidenciaDerivado))
            {
                result = EquiposDecorator.puedeReactivarTicket();
                if (result.Info == "ok")
                {
                    result = this.cambioEstado(EstadoIncidenciaFactory.getEstado("Derivado"), this.idTecnicoActual, idUsuario);
                    result.Detail = this.AreaActual.NombreArea;
                }
            }
            else
            {
                result.Detail = "El Ticket no está suspendido!!";
            }
            return result;         
        }
        internal Responses ponerEnEsperaRetirar(int idInc, int idUsuario)
        {
            Responses result = new Responses();
            if (!(this.EstadoActual is EstadoIncidenciaEsperandoRetiro))
            {               
                if (result.Info == "ok")
                {
                    result = this.cambioEstado(EstadoIncidenciaFactory.getEstado("Derivado"), this.idTecnicoActual, idUsuario);
                    result.Detail = this.AreaActual.NombreArea;
                }
            }
            else
            {
                result.Detail = "El Ticket no está suspendido!!";
            }
            return result;
        }       
    }
}