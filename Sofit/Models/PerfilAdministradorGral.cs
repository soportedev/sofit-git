﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Statics;

namespace soporte.Models
{
    public class PerfilAdministradorGral : Perfil
    {
        public override Constantes.AccesosPagina getAcceso(string page)
        {
            return Constantes.AccesosPagina.Full;
        }

        public override string getHomePage()
        {
            return "Soporte/Incidentes";
        }
    }
}