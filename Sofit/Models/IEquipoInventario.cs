﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System.Collections.Generic;
using System.Linq;
using Sofit.Models.Cambios;

namespace soporte.Models.Displays
{
    public abstract class IEquipoInventario
    {
        public int idEquipo { get; set; }
        public string Oblea { get; set; }
        public string NroSerie { get; set; }
        public JurisdiccionVista Jurisdiccion { get; set; }
        public TiposEquipoVista TipoEquipo { get; set; }
        public string Observaciones { get; set; }
        public List<RelationVista> Relaciones { get; set; }
        public List<PedidosSeguridadVista> Servicio8 { get; set; }
        public List<GestionCambiosVista> GestionesCambio { get; set; }
        public virtual void construir(int id)
        {
            this.Relaciones = new List<RelationVista>();
            this.Servicio8 = new List<PedidosSeguridadVista>();
            this.GestionesCambio = new List<GestionCambiosVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var equipo = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).First();
                this.idEquipo = id;
                this.Oblea = equipo.nombre;
                this.idEquipo = equipo.id_uc;
                this.TipoEquipo = new TiposEquipoVista
                {
                    id = equipo.Tipo_equipo.id_tipo,
                    Nombre = equipo.Tipo_equipo.descripcion
                };
                this.Relaciones = equipo.UnidadConf1.Select(c => new RelationVista { Id = c.id_uc, Oblea = c.nombre, TipoEquipo = c.Tipo_equipo.descripcion, Observaciones=c.observaciones }).ToList();
                this.Relaciones.AddRange(equipo.UnidadConf2.Select(c => new RelationVista { Id = c.id_uc, Oblea = c.nombre, TipoEquipo = c.Tipo_equipo.descripcion, Observaciones=c.observaciones }).ToList());
                this.Observaciones = equipo.observaciones;
                this.NroSerie = equipo.nro_serie;
                this.Jurisdiccion = new JurisdiccionVista
                {
                    idJurisdiccion = equipo.Jurisdiccion.id_jurisdiccion,
                    Nombre = equipo.Jurisdiccion.descripcion
                };
                this.GestionesCambio = dbc.EcxGc.Where(c => c.id_uc == this.idEquipo).Select(c => new GestionCambiosVista { Nro = c.id_gc, Fecha = c.GestionCambios.fecha_creacion }).ToList();
            }
        }
        public virtual Responses saveDB()
        {
            Responses result = new Responses();
            return result;
        }
        internal abstract Responses SaveLineaBase();
    }
}