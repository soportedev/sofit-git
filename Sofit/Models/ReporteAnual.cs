﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace Sofit.Models
{
    public class ReporteAnual : Reporte
    {
        public override string ObtenerReporte(HistoricosParametros param)
        {
            return Anual(param);
        }
        private string Anual(HistoricosParametros param)
        {
            return ToHtml(lookupAnualTotal(param));
        }
        private string ToHtml(TicketsMensualJurisdiccion tickets)
        {
            StringBuilder sb = new StringBuilder();
            if (tickets.CantidadGenerada == 0)
            {
                sb.Append("<p>Sin resultados encontrados</p>");
            }
            else//se encontraron resultados
            {
                sb.Append("<table class='repMensual'>").Append("<thead><tr class='ui-state-active'>").
                           Append("<tr class='ui-state-active'><th colspan=5>Año:" + tickets.Año + "</th><th colspan=4>Total Generado:" + tickets.CantidadGenerada + "</th></tr>").
                           Append("<tr><th></th><th>Tipo</th><th>Totales Específicos</th><th>Porcentaje Sobre Total</th><th>En Término</th>").
                           Append("<th>En el Umbral</th><th>Fuera del Umbral</th><th>Sin Cerrar</th></tr></thead>").
                           Append("<tbody>");

                if (tickets.Prioridad.Count > 0)
                {

                    for (int i = 0; i < tickets.Prioridad.Count; i++)
                    {
                        sb.Append("<tr>");
                        if (i == 0)
                        {
                            sb.Append("<td  class='spaned' rowspan='" + tickets.Prioridad.Count + "'>Por Prioridad</td>");
                        }

                        sb.Append("<td class='alignLeft'>" + tickets.Prioridad[i].PrioridadTicket + "</td>");
                        sb.Append("<td>" + tickets.Prioridad[i].CantidadEspecifica + "</td>");
                        tickets.Prioridad[i].Cantidad = tickets.CantidadGenerada;
                        sb.Append("<td>" + tickets.Prioridad[i].PorcentajeSobreTotal + "%</td>");
                        sb.Append("<td>" + tickets.Prioridad[i].DentroDelTiempo + " (" + tickets.Prioridad[i].PorcObtenido + " %)" + "</td>");
                        sb.Append("<td>" + tickets.Prioridad[i].DentroDelMargen + "</td>");
                        sb.Append("<td>" + tickets.Prioridad[i].FueraMargen + "</td>");
                        sb.Append("<td>" + tickets.Prioridad[i].SinCerrar + "</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("<tr><td colspan='9'><div></div></td></tr>");
                if (tickets.Servicio.Count > 0)
                {

                    for (int i = 0; i < tickets.Servicio.Count; i++)
                    {
                        sb.Append("<tr>");
                        if (i == 0)
                        {
                            sb.Append("<td  class='spaned' rowspan='" + tickets.Servicio.Count + "'>Por Servicios</td>");
                        }

                        sb.Append("<td class='alignLeft'>" + tickets.Servicio[i].Servicio + "</td>");
                        sb.Append("<td>" + tickets.Servicio[i].CantidadEspecifica + "</td>");
                        tickets.Servicio[i].Cantidad = tickets.CantidadGenerada;
                        sb.Append("<td>" + tickets.Servicio[i].PorcentajeSobreTotal + "%</td>");
                        sb.Append("<td>" + tickets.Servicio[i].DentroDelTiempo + " (" + tickets.Servicio[i].PorcObtenido + " %)" + "</td>");
                        sb.Append("<td>" + tickets.Servicio[i].DentroDelMargen + "</td>");
                        sb.Append("<td>" + tickets.Servicio[i].FueraMargen + "</td>");
                        sb.Append("<td>" + tickets.Servicio[i].SinCerrar + "</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("<tr><td colspan='9'><div></div></td></tr>");
                if (tickets.Tipo.Count > 0)
                {

                    for (int i = 0; i < tickets.Tipo.Count; i++)
                    {
                        sb.Append("<tr>");
                        if (i == 0)
                        {
                            sb.Append("<td  class='spaned' rowspan='" + tickets.Tipo.Count + "'>Por Tipología</td>");
                        }

                        sb.Append("<td class='alignLeft'>" + tickets.Tipo[i].TipoTicket + "</td>");
                        sb.Append("<td>" + tickets.Tipo[i].CantidadEspecifica + "</td>");
                        tickets.Tipo[i].Cantidad = tickets.CantidadGenerada;
                        sb.Append("<td>" + tickets.Tipo[i].PorcentajeSobreTotal + "%</td>");
                        sb.Append("<td>" + tickets.Tipo[i].DentroDelTiempo + " (" + tickets.Tipo[i].PorcObtenido + " %)" + "</td>");
                        sb.Append("<td>" + tickets.Tipo[i].DentroDelMargen + "</td>");
                        sb.Append("<td>" + tickets.Tipo[i].FueraMargen + "</td>");
                        sb.Append("<td>" + tickets.Tipo[i].SinCerrar + "</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</tbody></table>");
            }//else
            return sb.ToString();
        }
        private TicketsMensualJurisdiccion lookupAnualTotal(HistoricosParametros par)
        {
            List<TicketsCreados> ticketsPrioridad = new List<TicketsCreados>();
            List<TicketsCreados> ticketsServicio = new List<TicketsCreados>();
            List<TicketsCreados> ticketsPorTipo = new List<TicketsCreados>();
            List<TicketsQuery> tickets = new List<TicketsQuery>();
            using (var dbc = new IncidenciasEntities())
            {
                ((IObjectContextAdapter)dbc).ObjectContext.CommandTimeout = 400;
                tickets = (dbc.sp_listado_tickets(par.FechaDesde, par.FechaHasta, 0,
                    0, 0, 0, 0).Select(c =>                   
                   new TicketsQuery
                   {
                       Servicio = c.Servicio,
                       DireccionGeneracion = c.Direccion,
                       TipoTicket = c.TipoTicket,
                       Numero = c.Numero,
                       IdTicket = c.IdTicket.Value,
                       FechaGeneracion = c.FechaGeneracion.Value,
                       HoursOffline = c.TiempoSuspendido.Value,
                       HoursWeekend = c.TiempoWeekend.Value,
                       PrioridadTicket = c.PrioridadTicket,
                       HorasResolucion = c.HorasResolucion.Value,
                       HorasMargen = c.HorasMargen.Value,
                       FechaCierre = c.FechaCierrre,
                       Jurisdiccion = c.Jurisdiccion
                   })).ToList();

                //List<TicketsQuery> ticketsFiltrados = tickets.Where(c => c.FechaResolucion <= par.FechaHasta).ToList();
                ticketsPrioridad = (from u in tickets
                                    group u by new
                                    {
                                        u.PrioridadTicket
                                    } into g
                                    //orderby g.Count() descending
                                    select new TicketsCreados
                                    {
                                        CantidadEspecifica = g.Count(),
                                        PrioridadTicket = g.Key.PrioridadTicket,
                                        DentroDelTiempo = tickets.Where(c => c.DentroDelTiempo & c.PrioridadTicket == g.Key.PrioridadTicket).Count(),
                                        DentroDelMargen = tickets.Where(c => c.DentroDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket).Count(),
                                        FueraMargen = tickets.Where(c => c.FueraDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket).Count(),
                                        SinCerrar = tickets.Where(c => c.SinCerrar & c.DentroDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket).Count()
                                    }).ToList();
                ticketsServicio = (from u in tickets
                                   group u by new
                                   {
                                       u.Servicio
                                   } into g
                                   //orderby g.Count() descending
                                   select new TicketsCreados
                                   {
                                       CantidadEspecifica = g.Count(),
                                       Servicio = g.Key.Servicio,
                                       DentroDelTiempo = tickets.Where(c => c.DentroDelTiempo & c.Servicio == g.Key.Servicio).Count(),
                                       DentroDelMargen = tickets.Where(c => c.DentroDelMargen & c.Servicio == g.Key.Servicio).Count(),
                                       FueraMargen = tickets.Where(c => c.FueraDelMargen & c.Servicio == g.Key.Servicio).Count(),
                                       SinCerrar = tickets.Where(c => c.SinCerrar & c.Servicio == g.Key.Servicio).Count()
                                   }).ToList();
                ticketsPorTipo = (from u in tickets
                                  group u by new
                                  {
                                      u.TipoTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      CantidadEspecifica = g.Count(),
                                      TipoTicket = g.Key.TipoTicket,
                                      DentroDelTiempo = tickets.Where(c => c.DentroDelTiempo & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      DentroDelMargen = tickets.Where(c => c.DentroDelMargen & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      FueraMargen = tickets.Where(c => c.FueraDelMargen & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      SinCerrar = tickets.Where(c => c.SinCerrar & c.TipoTicket == g.Key.TipoTicket).Count()
                                  }).ToList();
            }
            TicketsMensualJurisdiccion ticketsM = new TicketsMensualJurisdiccion();
            //ticketsM.Mes = new DateTime(par.anioReporte, par.mesReporte, 1).ToString("MMMM", CultureInfo.CreateSpecificCulture("es")).ToUpper();
            ticketsM.Año = par.FechaDesde.Year.ToString();
            ticketsM.CantidadGenerada = tickets.Count();
            ticketsM.Jurisdiccion = tickets.Count > 0 ? tickets[0].Jurisdiccion : "N/A";
            ticketsM.Prioridad = ticketsPrioridad;
            ticketsM.Servicio = ticketsServicio;
            ticketsM.Tipo = ticketsPorTipo;
            return ticketsM;
        }
    }
}