﻿using soporte.Models.Factory;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace soporte.Models
{
    public class MovAreaIncidencia:IComparable
    {
        private int backidArea = 0;
        public IAreaIncidencia Area { get; set; }
        public int idArea { 
            get
            {
                return backidArea;
            }
            set
            {
                backidArea=value;
                Area = AreaIncidenciaFactory.getArea(value);
            }
        }
        public string Direccion { get; set; }
        
        [ScriptIgnore]
        public DateTime FechaDesde { get; set; }
        [ScriptIgnore]
        public DateTime? FechaHasta
        {
            get;
            set;
        }
        public string FechaDesdeShStr
        {
            get
            {
                return FechaDesde.ToString();
            }
            set { }
        }
        public string FechaHastaShStr
        {
            get
            {
                if (FechaHasta == null) return "";
                else return FechaHasta.Value.ToString();
            }
            set { }

        }
        public string UsuarioQEnvio { get; set; }
        
        public List<ResolucionesIncidencias> Resoluciones;
        public List<AccionXArea> AccionXArea;
        
        public bool tieneResolucion()
        {
            if (Resoluciones.Count > 0) return true;
            else return false;
        }

        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            MovAreaIncidencia other = (MovAreaIncidencia)obj;
            if (FechaDesde > other.FechaDesde)
                return 1;
            else if (FechaDesde < other.FechaDesde)
                return -1;
            else return 0;
        }
        
        #endregion
    }
}