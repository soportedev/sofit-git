﻿using soporte.Areas.Soporte.Models;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Helpers;
using soporte.Areas.Soporte.Controllers;

namespace soporte.Models
{
    public class HistIncBuilder
    {
        private HistoricoIncidencia historico;

        internal void Construir(string nroInc)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var incid = (from u in dbc.Incidencias
                             where u.numero == nroInc
                             select u).SingleOrDefault();
                if (incid != null)
                {
                    var imagenIncidencia=dbc.ImagenIncidencia.Where(c=>c.Incidencias.Any(x=>x.id_incidencias==incid.id_incidencias)).SingleOrDefault();
                    var fileIncidencia=dbc.AdjuntoIncidencia.Where(c=>c.Incidencias.Any(x=>x.id_incidencias==incid.id_incidencias)).SingleOrDefault();
                    historico = new HistoricoIncidencia
                    {
                        Numero = incid.numero,
                        IdIncidencia = incid.id_incidencias,
                        Descripcion = incid.descripcion,
                        Dependencia = incid.Dependencia != null ? incid.Dependencia.descripcion : "N/A",
                        Jurisdiccion = incid.Dependencia != null ? incid.Dependencia.Jurisdiccion.descripcion : incid.Jurisdiccion.descripcion,
                        Tipificacion = incid.Tipificaciones.nombre,
                        pathImg = imagenIncidencia != null ? imagenIncidencia.path : "",
                        pathFile = fileIncidencia != null ? fileIncidencia.path : "",
                        Servicio = incid.Tipificaciones.Servicios.nombre,
                        TipoTicket = incid.TipoTicket.nombre,
                        PrioridadTicket = incid.PrioridadTicket.nombre,
                        FechaInicio = incid.fecha_inicio,
                        TiempoServicio = incid.Tipificaciones.Servicios.tiempo_res.Value,
                        TiempoMargen = incid.Tipificaciones.Servicios.tiempo_margen.Value,
                        EstadoTicket = incid.fecha_fin != null ? "Cerrado" : incid.IncidenciasActivas.Estado_incidente.descripcion,
                        Cliente = incid.Contactos != null ? incid.Contactos.nombre : "N/A"
                    };
                    var notis = (from s in dbc.NotificacionSoporte
                             where s.id_incidencia == historico.IdIncidencia
                             select s).ToList();
                    if (notis.Count > 0)
                    {
                        historico.Notificaciones = new List<NotifDisplay>();
                        foreach (NotificacionSoporte n in notis)
                        {
                            List<string> mails = (from m in n.NotMailRefSoporte
                                                  where m.id_notificacion == n.id
                                                  select m.direccion_mail).ToList();

                            NotifDisplay no = new NotifDisplay()
                            {
                                mailsAdd = mails,
                                fecha = n.fecha.ToShortDateString(),
                                mensaje = n.mensaje,
                                enviadoPor = n.Usuarios.nombre_completo
                            };
                            historico.Notificaciones.Add(no);
                        }                        
                    }
                    BuilderTipifTree bder = new BuilderTipifTree();
                    string nombreTip = bder.getNombreCompleto(incid.id_tipificacion);
                    historico.Areas = new List<MovAreaIncidencia>();
                    //actual año
                    var mov = (from u in dbc.AreaXIncidencia
                               where u.id_incidencia == historico.IdIncidencia
                               select new MovAreaIncidencia
                                  {
                                      idArea=u.id_area,
                                      FechaDesde = u.fecha_inicio,
                                      FechaHasta = u.fecha_fin,
                                      UsuarioQEnvio = u.Usuarios.nombre_completo,
                                      Direccion = u.area.Direcciones.nombre
                                  }).ToList();                    
                    foreach (var v in mov)
                    {
                        DateTime fechaHastaParsed = (v.FechaHasta != null ? v.FechaHasta.Value : DateTime.Now);
                        if (v.FechaHasta != null)
                        {
                            bool sinTime = fechaHastaParsed.TimeOfDay.TotalSeconds == 0;
                            if (sinTime)
                            {
                                fechaHastaParsed = fechaHastaParsed.AddHours(23).AddMinutes(59).AddSeconds(59);
                            }
                        }

                        //List<ResolucionesIncidencias> resoluciones = (from s in dbc.Resoluciones
                        List<AccionXArea> accionesXArea = (from s in dbc.Resoluciones
                                                                where s.id_incidencia == historico.IdIncidencia && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.FechaDesde) > 0
                                                                //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                select new AccionXArea
                                                                {
                                                                    TipoAccion = 2,
                                                                    FechaInicio = s.fecha,
                                                                    // Resolucion / TipoAccion 1
                                                                    //Fecha = s.fecha,
                                                                    Equipo = s.UnidadConf.nombre,
                                                                    Observaciones = s.observaciones,
                                                                    Tecnico = s.tecnico.descripcion,
                                                                    TipoResolucion = s.tipo_resolucion.descripcion,
                                                                    Turno = s.turno.descripcion,
                                                                    PathImagen = s.path_imagen != null ? "/Content/img/Resoluciones/" + s.path_imagen : null,
                                                                    // Estado / TipoAccion 2
                                                                    TecnicoNombre = "",
                                                                    Estado = "",
                                                                    //FechaInicio = DateTime.MinValue,
                                                                    FechaFin = DateTime.MinValue
                                                                })
                                                        .Concat(from eh in dbc.EstadoXIncidencia
                                                                where eh.id_incidencia == historico.IdIncidencia && DateTime.Compare(eh.fecha_inicio, fechaHastaParsed) < 0 && DateTime.Compare(eh.fecha_inicio, v.FechaDesde) > 0
                                                                orderby eh.fecha_inicio ascending
                                                                select new AccionXArea
                                                                {
                                                                    TipoAccion = 1,
                                                                    FechaInicio = eh.fecha_inicio,
                                                                    // Resolucion / TipoAccion 1
                                                                    //Fecha = DateTime.MinValue,
                                                                    Equipo = null,
                                                                    Observaciones = null,
                                                                    Tecnico = null,
                                                                    TipoResolucion = null,
                                                                    Turno = null,
                                                                    PathImagen = null,
                                                                    // Estado / TipoAccion 2
                                                                    TecnicoNombre = eh.Usuarios.nombre_completo,
                                                                    Estado = eh.Estado_incidente.descripcion,
                                                                    //FechaInicio = eh.fecha_inicio,
                                                                    FechaFin = eh.fecha_fin ?? DateTime.MinValue
                                                                }).ToList();

                        //v.Resoluciones = resoluciones;
                        //v.Resoluciones.Sort();
                        v.AccionXArea = accionesXArea;
                        v.AccionXArea.Sort();

                    };
                    //historicos
                    var movHist=((from u in dbc.AreaXIncidenciaHistorico
                                  where u.id_incidencia == historico.IdIncidencia
                                  select new MovAreaIncidencia
                                  {
                                      idArea = u.id_area,
                                      FechaDesde = u.fecha_inicio,
                                      FechaHasta = u.fecha_fin,
                                      UsuarioQEnvio = u.Usuarios.nombre_completo,
                                      Direccion = u.area.Direcciones.nombre
                                  }).ToList());
                    foreach (var v in movHist)
                    {
                        DateTime fechaHastaParsed = (v.FechaHasta != null ? v.FechaHasta.Value : DateTime.Now);
                        if (v.FechaHasta != null)
                        {
                            bool sinTime = fechaHastaParsed.TimeOfDay.TotalSeconds == 0;
                            if (sinTime)
                            {
                                fechaHastaParsed = fechaHastaParsed.AddHours(23).AddMinutes(59).AddSeconds(59);
                            }
                        }

                        List<ResolucionesIncidencias> resoluciones = (from s in dbc.ResolucionesHistorico
                                                                      where s.id_incidencia == historico.IdIncidencia && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.FechaDesde) > 0
                                                                      //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                      select new ResolucionesIncidencias
                                                                      {
                                                                          Fecha = s.fecha,
                                                                          Equipo = s.UnidadConf.nombre,
                                                                          Observaciones = s.observaciones,
                                                                          Tecnico = s.tecnico.descripcion,
                                                                          TipoResolucion = s.tipo_resolucion.descripcion,
                                                                          Turno = s.turno.descripcion,
                                                                          PathImagen = "/Content/img/Resoluciones/" + s.path_imagen
                                                                      }).ToList();
                        v.Resoluciones = resoluciones;
                        v.Resoluciones.Sort();
                    };
                    mov.AddRange(movHist);
                    historico.Areas = mov;
                    historico.CambiosPrioridad = dbc.CambiosPrioridad.Where(c => c.id_ticket == incid.id_incidencias).Select(c => new CambiosPrioridadVista
                    {
                        Descripcion = c.descripcion,
                        EstadoPrevio = c.PrioridadTicket.nombre,
                        EstadoNuevo = c.PrioridadTicket1.nombre,
                        Fecha = c.fecha,
                        Usuario = c.Usuarios.nombre_completo
                    }).ToList();
                }                
            }
        }       
        internal HistoricoIncidencia getResult()
        {
            if (historico != null)
            {
                historico.Areas.Sort();
            }

            return historico;
        }
    }
}