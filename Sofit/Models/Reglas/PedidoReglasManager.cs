﻿using Sofit.DataAccess;
using soporte.Models.Displays;
using soporte.Models.Helpers;
using soporte.Models.Reglas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Sofit.Models.Reglas
{
    public class PedidoReglasManager
    {
        private string emailSolicitante = string.Empty;
        private string emailAutorizante = string.Empty;
        private string emailOperaciones = "operaciones.noc@cba.gov.ar";
        private string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
        private string bodySolicitante = string.Empty;
        private string bodyAutorizante = string.Empty;
        private string bodyOperaciones = string.Empty;
        public Responses NuevoPedidoRegla(PedidoRegla pedido)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                using (var dbcTransaction = dbc.Database.BeginTransaction())
                {
                    try
                    {
                        PedidosSeguridad nuevoPedido = new PedidosSeguridad
                        {
                            fecha = DateTime.Now,
                            asunto = pedido.asunto,
                            descripcion = pedido.descripcion,
                            autorizante = pedido.Autorizante.id,
                            solicitante = pedido.Solicitante.id,
                            id_estado = pedido.EstadoActual.Id,
                            id_dependencia = pedido.Dependencia.idDependencia,
                            vencimiento = pedido.Vencimiento,
                            id_tipo_pedido = dbc.TipoPedido.Where(c => c.nombre == pedido.TipoDePedido).Single().id
                        };
                        dbc.PedidosSeguridad.Add(nuevoPedido);
                        //reglas
                        foreach (var v in pedido.GReglas)
                        {
                            //usuario
                            var usuarioExiste = dbc.Usuarios.Where(c => c.nombre == v.Usuario.nombre);
                            if (usuarioExiste.Count() > 0)
                            {
                                Usuarios usuario = usuarioExiste.First();
                                v.Usuario.id = usuario.id_usuario;
                            }
                            else
                            {
                                Usuarios nuevo = new Usuarios
                                {
                                    nombre = v.Usuario.nombre,
                                    email = v.Usuario.email,
                                    nombre_completo = v.Usuario.displayName,
                                    acceso = 0,
                                    personalizacion = 2,
                                    es_interno = false
                                };
                                dbc.Usuarios.Add(nuevo);
                                dbc.SaveChanges();
                                v.Usuario.id = nuevo.id_usuario;
                            }
                            ReglaPedido nuevaR = new ReglaPedido
                            {
                                id_pedido = nuevoPedido.id,
                                id_usuario = v.Usuario.id,
                                dominio_usuario = v.Usuario.dominio
                            };
                            string tipoorigen = v.Origen.Tipo;
                            string tipodestino = v.Destino.Tipo;
                            int idTipoOrigen = 0, idTipoDestino = 0;
                            ExtremoPedido origen = new ExtremoPedido();
                            ExtremoPedido destino = new ExtremoPedido();
                            if (tipoorigen == "Host")
                            {
                                idTipoOrigen = dbc.TipoEcPedido.Where(c => c.nombre == "Host").Single().id;
                                int idUc = dbc.UnidadConf.Where(c => c.nombre == v.Origen.Nombre).Single().id_uc;
                                origen.id_uc = idUc;
                            }
                            if (tipoorigen == "Red")
                            {
                                idTipoOrigen = dbc.TipoEcPedido.Where(c => c.nombre == "Red").Single().id;
                                origen.tipo_pedido = idTipoOrigen;
                            }
                            if (tipodestino == "Host")
                            {
                                idTipoDestino = dbc.TipoEcPedido.Where(c => c.nombre == "Host").Single().id;
                                int idUc = dbc.UnidadConf.Where(c => c.nombre == v.Destino.Nombre).Single().id_uc;
                                destino.id_uc = idUc;
                            }
                            if (tipodestino == "Red")
                            {
                                idTipoDestino = dbc.TipoEcPedido.Where(c => c.nombre == "Red").Single().id;
                                destino.tipo_pedido = idTipoDestino;
                            }
                            origen.tipo_pedido = idTipoOrigen;
                            destino.tipo_pedido = idTipoDestino;
                            destino.ip_address = v.Destino.Ip;
                            origen.ip_address = v.Origen.Ip;
                            nuevaR.ExtremoPedido = origen;
                            nuevaR.ExtremoPedido1 = destino;
                            //save changes
                            dbc.ReglaPedido.Add(nuevaR);
                        }

                        EstadosXPedido nuevEs = new EstadosXPedido
                        {
                            id_estado = pedido.EstadoActual.Id,
                            id_pedido = nuevoPedido.id,
                            fecha_desde = DateTime.Now,
                            observaciones = "Creación del pedido",
                            nombreTecnico = pedido.Solicitante.displayName
                        };
                        foreach (var port in pedido.Puertos)
                        {
                            nuevoPedido.PuertosXPedido.Add(new PuertosXPedido { id_pedido = nuevoPedido.id, puerto = port.Numero });
                        }
                        foreach (var prot in pedido.Protocolos)
                        {
                            nuevoPedido.ProtocolosXPedido.Add(new ProtocolosXPedido { id_pedido = nuevoPedido.id, protocolo = prot.Nombre });
                        }
                        //dbc.PedidosSeguridad.Add(nuevoPedido);
                        dbc.EstadosXPedido.Add(nuevEs);
                        dbc.SaveChanges();
                        //transaction
                        dbcTransaction.Commit();
                        //region mails
                        var solicitante = dbc.Usuarios.Where(c => c.id_usuario == pedido.Solicitante.id).Single();
                        var autorizante = dbc.Usuarios.Where(c => c.id_usuario == pedido.Autorizante.id).Single();
                        emailAutorizante = autorizante.email;
                        emailSolicitante = solicitante.email;
                        bodySolicitante = "Se ha registrado una solicitud de acceso a los servicios informáticos del tipo <b>Regla de Acceso</b> " +
                            "que provee esta dirección con el número: <b>" + nuevoPedido.id + "</b>, solicitada por <b>" + solicitante.nombre_completo +
                            "</b> y se encuentra a la espera de que sea aprobada por <b>" + autorizante.nombre_completo + "</b>";
                        bodyAutorizante = "Se ha registrado una solicitud de acceso a los servicios informáticos del tipo <b>Regla de Acceso</b> " +
                            "que provee esta dirección con el número: <b>" + nuevoPedido.id + "</b>, solicitada por <b>" + solicitante.nombre_completo +
                            "</b> y se encuentra a la espera de que sea aprobada por usted.";
                        result.Info = "ok";
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                result.Detail += "- Property: \"{0}\", Error: \"{1}\"" +
                                    ve.PropertyName + ve.ErrorMessage;
                            }
                        }                        
                        dbcTransaction.Rollback();
                    }
                    catch (Exception e)
                    {
                        result.Detail = e.Message;
                        dbcTransaction.Rollback();
                    }
                }//transaction
            }            
            if (result.Info == "ok")
            {
                //envio de mails de creación del pedido
                EmailSender emSend = new EmailSender();
                emSend.sendEmail(new string[] { emailSolicitante, subject, bodySolicitante });
                emSend.sendEmail(new string[] { emailAutorizante, subject, bodyAutorizante });
                //emSend.sendEmail(new string[] { "david.ortega@cba.gov.ar", subject, body });
            }
            return result;
        }
        internal Responses NuevoPedidoAcceso(PedidoAcceso pedido)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    int? idHostAcceso = null;
                    string ipHost = null;
                    #region usuario
                    var usuarioExiste = dbc.Usuarios.Where(c => c.nombre == pedido.Usuario.nombre);
                    if (usuarioExiste.Count() > 0)
                    {
                        pedido.Usuario.id = usuarioExiste.First().id_usuario;
                    }
                    else
                    {
                        Usuarios nuevo = new Usuarios
                        {
                            nombre = pedido.Usuario.nombre,
                            nombre_completo = pedido.Usuario.displayName,
                            email = pedido.Usuario.email,                            
                            acceso = 0,
                            personalizacion = 2,
                            es_interno=false,
                            dominio=pedido.Usuario.dominio
                        };
                        dbc.Usuarios.Add(nuevo);
                        dbc.SaveChanges();
                        pedido.Usuario.id = nuevo.id_usuario;
                    }
                    #endregion fin region usuario
                    
                    PedidosSeguridad nuevoPedido = new PedidosSeguridad
                    {
                        fecha = DateTime.Now,
                        id_estado = pedido.EstadoActual.Id,
                        asunto = pedido.asunto,
                        descripcion = pedido.descripcion,                        
                        solicitante = pedido.Solicitante.id,                        
                        tipoOrigenAcceso = pedido.TipoOrigen,
                        id_dependencia = pedido.Dependencia.idDependencia,
                        vencimiento = pedido.Vencimiento,
                        id_tipo_pedido = dbc.TipoPedido.Where(c => c.nombre == pedido.TipoDePedido).Single().id,
                        usuario_acceso=pedido.Usuario.id,
                        detalleAcceso=pedido.Detalle
                    };
                    //region uc
                    if (pedido.HostOrigen != string.Empty)
                    {
                        var uc = dbc.UnidadConf.Where(c => c.nombre == pedido.HostOrigen).Single();
                        idHostAcceso = uc.id_uc;
                        ipHost = pedido.IpOrigen;
                        nuevoPedido.ip_host_acceso = ipHost;
                        nuevoPedido.host_acceso = idHostAcceso;
                    }
                    dbc.PedidosSeguridad.Add(nuevoPedido);

                    EstadosXPedido nuevEs = new EstadosXPedido
                    {
                        id_estado = pedido.EstadoActual.Id,
                        id_pedido = nuevoPedido.id,
                        fecha_desde = DateTime.Now,
                        observaciones = "Creación del pedido",
                        nombreTecnico = pedido.Solicitante.displayName
                    };
                    dbc.PedidosSeguridad.Add(nuevoPedido);
                    dbc.EstadosXPedido.Add(nuevEs);
                    dbc.SaveChanges();
                    //region emails
                    var solicitante = dbc.Usuarios.Where(c => c.id_usuario == pedido.Solicitante.id).Single();
                    //var autorizante = dbc.Usuarios.Where(c => c.id_usuario == pedido.Autorizante.id).Single();
                    //emailAutorizante = autorizante.email;
                    emailSolicitante = solicitante.email;
                    bodySolicitante = "Se ha registrado una solicitud de acceso a los servicios informáticos del tipo <b>Acceso Remoto</b> que provee esta dirección con el número: <b>" + nuevoPedido.id + "</b>, solicitada por <b>" +
                        solicitante.nombre_completo + " </b> y se encuentra a la espera de revisión por la Subdirección de Operaciones.";
                    bodyOperaciones = "Se ha registrado una solicitud de acceso a los servicios informáticos del tipo <b>Acceso Remoto</b> con el número: <b>" + nuevoPedido.id + "</b>, solicitada por <b>" +
                        solicitante.nombre_completo + "</b> y se encuentra a la espera de revisión por la Subdirección de Operaciones.";
                    //fin región emails
                    result.Info = "ok";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        result.Detail += "- Property: \"{0}\", Error: \"{1}\"" +
                            ve.PropertyName + ve.ErrorMessage;
                    }
                }
            }
            catch (Exception e)
            {

                result.Detail = e.Message;
            }
            if (result.Info == "ok")
            {
                EmailSender emSend = new EmailSender();
                emSend.sendEmail(new string[] { emailSolicitante, subject, bodySolicitante });
                emSend.sendEmail(new string[] { emailOperaciones, subject, bodyOperaciones });
                //   emSend.sendEmail(new string[] { "david.ortega@cba.gov.ar", subject, body });
            }
            return result;
        }
        public Pedidos crear(int idPedido)
        {
            Pedidos pedido = null;
            using (var dbc = new IncidenciasEntities())
            {
                var p = dbc.PedidosSeguridad.Where(c => c.id == idPedido).SingleOrDefault();
                if (p != null)
                {
                    if (p.TipoPedido.nombre == "Regla de Acceso")
                    {
                        PedidoRegla pedidor = new PedidoRegla();
                        pedidor.Id = p.id;
                        pedidor.asunto = p.asunto;
                        pedidor.Solicitante = new UsuarioVista { id = p.solicitante, nombre = p.Usuarios2.nombre, displayName = p.Usuarios2.nombre_completo, email = p.Usuarios2.email };
                        pedidor.Autorizante = p.autorizante.HasValue ? new UsuarioVista { id = p.Usuarios1.id_usuario, email = p.Usuarios1.email, nombre = p.Usuarios1.nombre, displayName = p.Usuarios1.nombre_completo } : null;
                        pedidor.Aprobador = p.aprobador.HasValue ? new UsuarioVista { id = p.Usuarios.id_usuario, email = p.Usuarios.email, nombre = p.Usuarios.nombre } : null;
                        pedidor.Dependencia = new soporte.Models.ClasesVistas.DependenciaVista { idDependencia = p.id_dependencia, Nombre = p.Dependencia.Jurisdiccion.descripcion };
                        pedidor.Jurisdiccion = new soporte.Models.ClasesVistas.JurisdiccionVista { idJurisdiccion = p.Dependencia.Jurisdiccion.id_jurisdiccion, Nombre = p.Dependencia.Jurisdiccion.descripcion };
                        pedidor.descripcion = p.descripcion;
                        pedidor.EstadoActual = EstadoReglaFactory.getEstadoBiz(p.id_estado);
                        pedidor.EstadoActual.Pedido = pedidor;

                        pedidor.Puertos = p.PuertosXPedido.Select(c => new PuertosVista { Numero = c.puerto }).ToList();
                        pedidor.Protocolos = p.ProtocolosXPedido.Select(c => new ProtocoloVista { Nombre = c.protocolo }).ToList();
                        pedidor.TicketAsociado = p.id_ticket.HasValue ? new TicketVista { Id = p.id_ticket.Value, Numero = p.Incidencias.numero } : null;
                        pedido = pedidor;
                    }
                    if (p.TipoPedido.nombre == "Acceso Remoto")
                    {
                        PedidoAcceso pedidoa = new PedidoAcceso();
                        pedidoa.Id = p.id;

                        pedidoa.asunto = p.asunto;
                        pedidoa.Autorizante = p.autorizante.HasValue ? new UsuarioVista { id = p.Usuarios1.id_usuario, email = p.Usuarios1.email, nombre = p.Usuarios1.nombre, displayName = p.Usuarios1.nombre_completo } : null;
                        pedidoa.Dependencia = new soporte.Models.ClasesVistas.DependenciaVista { idDependencia = p.id_dependencia, Nombre = p.Dependencia.Jurisdiccion.descripcion };
                        pedidoa.Jurisdiccion = new soporte.Models.ClasesVistas.JurisdiccionVista { idJurisdiccion = p.Dependencia.Jurisdiccion.id_jurisdiccion, Nombre = p.Dependencia.Jurisdiccion.descripcion };
                        pedidoa.descripcion = p.descripcion;
                        pedidoa.EstadoActual = EstadoReglaFactory.getEstadoBiz(p.id_estado);
                        pedidoa.EstadoActual.Pedido = pedidoa;
                        pedidoa.Solicitante = new UsuarioVista { id = p.solicitante, nombre = p.Usuarios2.nombre, displayName = p.Usuarios2.nombre_completo, email = p.Usuarios2.email };
                        pedidoa.TicketAsociado = p.id_ticket.HasValue ? new TicketVista { Id = p.id_ticket.Value, Numero = p.Incidencias.numero } : null;
                        pedidoa.Vencimiento = p.vencimiento;
                        pedido = pedidoa;
                    }

                }
            }
            return pedido;
        }


    }
}