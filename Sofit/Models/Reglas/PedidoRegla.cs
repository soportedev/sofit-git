﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.Models.Reglas;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class PedidoRegla:Pedidos
    {
        public List<GrupoReglas> GReglas { get; set; }
        public List<PuertosVista> Puertos { get; set; }
        public List<ProtocoloVista> Protocolos { get; set; }


    }
}