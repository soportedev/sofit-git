﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoRealizado : EstadoPedido
    {
        public EstadoRealizado(bool look)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var est = dbc.EstadosPedidos.Where(c => c.nombre == "Realizado").Single();
                base.Id = est.id;
                base.Nombre = est.nombre;
            }
        }
        public EstadoRealizado(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }

        public override Responses realizado(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }

        public override Responses rechazar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }

        public override bool requiereAutorizacion()
        {
            return false;
        }

        public override Responses revisar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está realizado");
        }
    }
}