﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Helpers;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoAprobado : EstadoPedido
    {        
        public EstadoAprobado(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("No se puede aprobar un pedido ya aprobado!!");
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            string emailAprobador = Pedido.Aprobador.email;
            string emailSolicitante = Pedido.Solicitante.email;
            string nombreAprobador = Pedido.Aprobador.displayName;
            Responses result = new Responses();
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Autorizar");
            result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);            
            
            if (result.Info == "ok")
            {                
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string bodySolicitante = "El pedido nro.<b> " + this.Pedido.Id + "</b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + " </b> fue derivado para que lo apruebe <b>"
                     + nombreAprobador + "</b>. Revise en los movimientos las observaciones.";
                          
                string bodyAutoriz2 = "Se ha registrado una solicitud de acceso a los servicios informáticos del tipo <b>Regla de Acceso</b> que provee esta dirección con el número: <b>"
                         + Pedido.Id + "</b>, solicitada por <b>" + Pedido.Solicitante.displayName +
                         "</b> y se encuentra a la espera de que sea aprobada por usted.<br> Puede hacerlo desde 'Mis Pedidos de Acceso' ingresando en Sofit";
                sender.sendEmail(new string[] { emailSolicitante, subject, bodySolicitante });
                sender.sendEmail(new string[] { emailAprobador, subject, bodyAutoriz2 });
                //sender.sendEmail(new string[] { email3, subject, bodySeg });
                result.Detail = "Pedido Derivado Para Autorizar.";
            }
            return result;
        }

        public override Responses cancelar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("No se puede cancelar un pedido ya aprobado!!");
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses realizado(string nombreUsuario,string obs)
        {

            Responses result = new Responses { Info = "ok" };
            
            if (result.Info == "ok")
            {
                //cierro el ticket todo
                using (var dbc = new IncidenciasEntities())
                {
                    var usuario = dbc.Usuarios.Where(c => c.nombre_completo == nombreUsuario).Single();
                    var tecnico = dbc.tecnico.Where(c => c.id_usuario == usuario.id_usuario).Single();
                    var pedidoDeBase = dbc.PedidosSeguridad.Where(c => c.id == Pedido.Id).Single();
                    var ticket = pedidoDeBase.Incidencias;
                    if (ticket != null)
                    {
                        ticket.Resoluciones.Add(new Resoluciones
                        {
                            fecha = DateTime.Now,
                            id_tecnico = tecnico.id_tecnico,
                            id_turno = DateTime.Now.Hour >= 14 ? 2 : 1,
                            usuario = usuario.id_usuario,
                            id_area = dbc.area.Where(c => c.descripcion == "Seguridad").Single().id_area,
                            observaciones = "Se cierra el ticket por haber completado el pedido de accesos correspondiente",
                            id_tipo_resolucion = dbc.tipo_resolucion.Where(c => c.descripcion == "Pedido de Reglas ABM.Resuelto").Single().id_tipo_resolucion
                        });
                        ticket.fecha_fin = DateTime.Now;
                        var estadosxticket = ticket.EstadoXIncidencia.Where(c => c.fecha_fin == null);
                        var areasxticket = ticket.AreaXIncidencia.Where(c => c.fecha_fin == null);
                        foreach (var v in estadosxticket)
                        {
                            v.fecha_fin = DateTime.Now;
                        }
                        foreach (var v in areasxticket)
                        {
                            v.fecha_fin = DateTime.Now;
                        }
                        var incactiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == ticket.id_incidencias).Single();
                        dbc.IncidenciasActivas.Remove(incactiva);
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                if (result.Info == "ok")
                {
                    this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Realizado");
                    result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
                    //notificamos (no seria necesario)
                    string emailAutorizante = this.Pedido.Autorizante.email;
                    string emailSolicitante = this.Pedido.Solicitante.email;
                    EmailSender sender = new EmailSender();
                    string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                    string body = "El pedido nro. <b>" + this.Pedido.Id + " </b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + "</b> fue realizado el día " + DateTime.Now.ToShortDateString() +
                        " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>. Revise en los movimientos las observaciones." +
                              ".<br> Dirección General de IT.";
                    string bodySeg = "El pedido nro. <b>" + this.Pedido.Id + " </b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + " </b>fue realizado el día " + DateTime.Now.ToShortDateString() +
                        " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>. El ticket fue cerrado." +
                              ".<br> Dirección General de IT.";
                    sender.sendEmail(new string[] { emailSolicitante, subject, body });
                    sender.sendEmail(new string[] { emailAutorizante, subject, body });
                    sender.sendEmail(new string[] { EmailSeguridad, subject, bodySeg });
                    result.Detail = "Pedido Realizado.";
                }
            }
            return result;
        }
        public override Responses rechazar(string nombreUsuario,string obs)
        {
            Responses result = new Responses { Info = "ok" };
            
            if (result.Info == "ok")
            {
                //cierro el ticket todo
                using (var dbc = new IncidenciasEntities())
                {
                    var usuario = dbc.Usuarios.Where(c => c.nombre_completo == nombreUsuario).Single();
                    var tecnico = dbc.tecnico.Where(c => c.id_usuario == usuario.id_usuario).Single();
                    var pedidoDeBase = dbc.PedidosSeguridad.Where(c => c.id == Pedido.Id).Single();
                    var ticket = pedidoDeBase.Incidencias;
                    if (ticket != null)
                    {
                        ticket.Resoluciones.Add(new Resoluciones
                        {
                            fecha = DateTime.Now,
                            id_tecnico = tecnico.id_tecnico,
                            id_turno = DateTime.Now.Hour >= 14 ? 2 : 1,
                            usuario = usuario.id_usuario,
                            id_area = dbc.area.Where(c => c.descripcion == "Seguridad").Single().id_area,
                            observaciones = "Se cierra el ticket por haber sido rechazado el pedido de accesos correspondiente",
                            id_tipo_resolucion = dbc.tipo_resolucion.Where(c => c.descripcion == "Pedido de Reglas ABM.Rechazado").Single().id_tipo_resolucion
                        });
                        ticket.fecha_fin = DateTime.Now;
                        var estadosxticket = ticket.EstadoXIncidencia.Where(c => c.fecha_fin == null);
                        var areasxticket = ticket.AreaXIncidencia.Where(c => c.fecha_fin == null);
                        foreach (var v in estadosxticket)
                        {
                            v.fecha_fin = DateTime.Now;
                        }
                        foreach (var v in areasxticket)
                        {
                            v.fecha_fin = DateTime.Now;
                        }
                        var incactiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == ticket.id_incidencias).Single();
                        dbc.IncidenciasActivas.Remove(incactiva);
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                if (result.Info == "ok")
                {
                    this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Rechazado");
                    result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
                    result.Detail = "El pedido fue rechazado";
                    //notificamos
                    string emailAutorizante = this.Pedido.Autorizante.email;
                    string emailSolicitante = this.Pedido.Solicitante.email;
                    EmailSender sender = new EmailSender();
                    string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                    string body = "El pedido nro. " + this.Pedido.Id + " relacionado con el Ticket nro. " + this.Pedido.TicketAsociado.Numero + " fue rechazado el día " + DateTime.Now.ToShortDateString() +
                        " " + DateTime.Now.ToShortTimeString() + " hrs. por " + nombreUsuario + ". Revise en los movimientos las observaciones." +
                              "";
                    string bodySeg = "El pedido nro. <b>" + this.Pedido.Id + "</b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + "</b> fue rechazado el día " + DateTime.Now.ToShortDateString() +
                        " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>.";
                    sender.sendEmail(new string[] { emailSolicitante, subject, body });
                    sender.sendEmail(new string[] { emailAutorizante, subject, body });
                    sender.sendEmail(new string[] { EmailSeguridad, subject, bodySeg });
                }
            }
            return result;
        }

        public override bool requiereAutorizacion()
        {
            return false;
        }

        public override Responses revisar(string nombreUsuario,string obs)
        {
            //Responses result = new Responses();
            //this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("ParaRevisar");
            //result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);

            //if (result.Info == "ok")
            //{//notificamos
            //    string email1=this.Pedido.Autorizante.email;
            //    string email2 = this.Pedido.Solicitante.email;
            //    string email3 = "david.ortega@cba.gov.ar";
            //    EmailSender sender = new EmailSender();
            //    string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
            //    string body= "El pedido nro. " + this.Pedido.Id + " relacionado con el Ticket nro. " + this.Pedido.TicketAsociado.Numero +
            //        " fue derivado a fin de que sea revisado por el autorizante el día " + DateTime.Now.ToShortDateString() +
            //        " " + DateTime.Now.ToShortTimeString() + " hrs. Revise en los movimientos del pedido el motivo." +
            //              ".\n Dirección General de IT.";
            //    string bodySeg = "El pedido nro. " + this.Pedido.Id + " relacionado con el Ticket nro. " + this.Pedido.TicketAsociado.Numero +
            //        " fue derivado a fin de que sea revisado por el autorizante el día " + DateTime.Now.ToShortDateString() +
            //        " " + DateTime.Now.ToShortTimeString() + " hrs. Recuerde suspender el ticket nro. " +this.Pedido.TicketAsociado.Numero+
            //              ".\n Dirección General de IT.";
            //    sender.sendEmail(new string[] { email1, subject, body } );
            //    sender.sendEmail(new string[] { email2, subject, body });
            //    sender.sendEmail(new string[] { email3, subject, bodySeg });
            //}
            //return result;
            throw new NotImplementedException("No implementado");
        }
    }
}