﻿using Sofit.Models.Reglas;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class ReglasModel
    {
        public ReglasModel()
        {
            this.Autorizantes = new List<UsuarioVista>();
            this.MisPedidos = new List<PedidosVista>();
        }
        public bool esOperaciones { get; set; }
        public bool esAdmin { get; set; }
        public int PaginaActualPedidos { get; set; }
        public int CantidadPaginasPedidos { get; set; }
        public int CantidadPaginasTickets { get; set; }
        public int PaginaActualTickets { get; set; }
        public int CantidadPorPagina { get; set; }
        public string FiltroAplicado { get; set; }
        public List<TicketVista> MisTickets { get; set; }
        public List<PedidosVista>MisPedidos  { get; set; }
        public List<UsuarioVista> Autorizantes { get; set; }
        public List<JurisdiccionVista> Jurisdicciones { get; set; }
        public List<EstadoPedidoVista> Estados { get; set; }
        public List<TipoAccesoVista> TiposAcceso { get; set; }
        public List<PrioridadesTicketVista> Prioridad { get; set; }
    }
}