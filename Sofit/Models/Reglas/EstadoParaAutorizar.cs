﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Helpers;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoParaAutorizar : EstadoPedido
    {
        public EstadoParaAutorizar(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Aprobado");
            Responses result=this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
            return result;
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses realizado(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses rechazar(string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            //cierro el ticket todo
            using (var dbc = new IncidenciasEntities())
            {
                var usuario = dbc.Usuarios.Where(c => c.nombre_completo == nombreUsuario).Single();
                int idTecnico = 0;
                try
                {
                    idTecnico = dbc.tecnico.Where(c => c.id_usuario == usuario.id_usuario).Single().id_tecnico;
                }catch(Exception e)
                {
                    idTecnico = dbc.tecnico.Where(c => c.descripcion == "Externos").Single().id_tecnico;
                }               
                //var tecnico = dbc.tecnico.Where(c => c.id_usuario == usuario.id_usuario).Single();
                var pedidoDeBase = dbc.PedidosSeguridad.Where(c => c.id == Pedido.Id).Single();
                var ticket = pedidoDeBase.Incidencias;
                if (ticket != null)
                {
                    ticket.Resoluciones.Add(new Resoluciones
                    {
                        fecha = DateTime.Now,
                        id_tecnico = idTecnico,
                        id_turno = DateTime.Now.Hour >= 14 ? 2 : 1,
                        usuario = usuario.id_usuario,
                        id_area = dbc.area.Where(c => c.descripcion == "Seguridad").Single().id_area,
                        observaciones = "Se cierra el ticket por haber sido rechazado el pedido de accesos correspondiente",
                        id_tipo_resolucion = dbc.tipo_resolucion.Where(c => c.descripcion == "Pedido de Reglas ABM.Rechazado").Single().id_tipo_resolucion
                    });
                    ticket.fecha_fin = DateTime.Now;
                    var estadosxticket = ticket.EstadoXIncidencia.Where(c => c.fecha_fin == null);
                    var areasxticket = ticket.AreaXIncidencia.Where(c => c.fecha_fin == null);
                    foreach (var v in estadosxticket)
                    {
                        v.fecha_fin = DateTime.Now;
                    }
                    foreach (var v in areasxticket)
                    {
                        v.fecha_fin = DateTime.Now;
                    }
                    var incactiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == ticket.id_incidencias).Single();
                    dbc.IncidenciasActivas.Remove(incactiva);
                }
                dbc.SaveChanges();
                result.Info = "ok";
            }
            /*cierre de ticket*/
            if (result.Info == "ok")
            {
                this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Rechazado");
                result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
                result.Detail = "El pedido fue rechazado";
                string emailautorizante = this.Pedido.Autorizante.email;
                string emailsolicitante = this.Pedido.Solicitante.email;
                string emailautorizante2 = this.Pedido.Aprobador.email;
                string emailSegInf = "Seguridad.Informatica@cba.gov.ar";
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string body = "El pedido nro. <b>" + this.Pedido.Id + "</b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + "</b> fue rechazado el día " + DateTime.Now.ToShortDateString() +
                    " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>. Revise en los movimientos las observaciones.";

                string bodySeg = "El pedido nro. <b>" + this.Pedido.Id + "</b> relacionado con el Ticket nro. <b>" + this.Pedido.TicketAsociado.Numero + "</b> fue rechazado el día " + DateTime.Now.ToShortDateString() +
                    " " + DateTime.Now.ToShortTimeString() + " hrs. por <b> " + nombreUsuario + "</b>.";
                sender.sendEmail(new string[] { emailsolicitante, subject, body });
                sender.sendEmail(new string[] { emailautorizante, subject, body });
                sender.sendEmail(new string[] { emailautorizante2, subject, body });
                sender.sendEmail(new string[] { emailSegInf, subject, bodySeg });
                result.Detail = "Pedido rechazado.";
            }
            return result;
        }

        public override bool requiereAutorizacion()
        {
            throw new NotImplementedException();
        }

        public override Responses revisar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }
    }
}