﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class EstadoReglaFactory
    {
        private static List<EstadoPedidoVista> estados = null;
        static EstadoReglaFactory()
        {
            using (var db = new IncidenciasEntities())
            {
                estados = (from u in db.EstadosPedidos
                           select new EstadoPedidoVista
                           {
                               Id = u.id,
                               Nombre = u.nombre
                           }).ToList();                          
            }
        }       
        public static EstadoPedidoVista getEstadoVista(string nombre)
        {
            return estados.Where(c => c.Nombre == nombre).Single();
        }
        public static EstadoPedidoVista getEstadoVista(int id)
        {
            return estados.Where(c => c.Id == id).Single();
        }
        public static EstadoPedido getEstadoBiz(string nombre)
        {
            EstadoPedidoVista ev = estados.Where(c => c.Nombre == nombre).SingleOrDefault();
            if (ev == null) return null;
            else return getEpn(ev);
        }
        public static EstadoPedido getEstadoBiz(int id)
        {
            EstadoPedidoVista ev = estados.Where(c => c.Id == id).SingleOrDefault();
            if (ev == null) return null;
            else return getEpn(ev);
        }
        
        private static EstadoPedido getEpn(EstadoPedidoVista epv)
        {
            EstadoPedido estado = null;
            switch(epv.Nombre)
            {
                case "Iniciado":
                    estado = new EstadoIniciado(epv.Nombre, epv.Id);
                    break;
                case "Aprobado":
                    estado = new EstadoAprobado(epv.Nombre, epv.Id);
                    break;
                case "Verificar":
                    estado = new EstadoParaVerificar(epv.Nombre, epv.Id);
                    break;                
                case "Realizado":
                    estado = new EstadoRealizado(epv.Nombre, epv.Id);
                    break;
                case "Autorizar":
                    estado = new EstadoParaAutorizar(epv.Nombre, epv.Id);
                    break;
                case "Rechazado":
                    estado = new EstadoRechazado(epv.Nombre, epv.Id);
                    break;
                case "Cancelado":
                    estado = new EstadoCancelado(epv.Nombre, epv.Id);
                    break;                
            }
            return estado;
        }        
    }
}