﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.Areas.Seguridad.Models;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoIniciado : EstadoPedido
    {        
        public EstadoIniciado(string nombre,int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            
            Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Aprobado");
            result=Pedido.registrarBaseCambioEstado(nombreUsuario, obs);                
            if (result.Info == "ok")
            {                
                result = Pedido.generarTicket();
            }
            return result;
        }
        
        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario,string obs)
        {
            Responses result = new Responses();
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Cancelado");
            result = this.Pedido.registrarBaseCambioEstado(nombreUsuario,obs);
            if (result.Info == "ok")
            {                
                result.Info = "ok";
                result.Detail = "Se ha cancelado el pedido";
                //notificamos
                string email1 = this.Pedido.Autorizante != null ? this.Pedido.Autorizante.email : null;
                string email2 = this.Pedido.Solicitante.email;                
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string body = "El pedido nro. <b>" + this.Pedido.Id  + "</b> fue cancelado el día " + DateTime.Now.ToShortDateString() +
                    " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>. Revise en los movimientos las observaciones." +
                          ".<br> Dirección General de IT.";
                if (email1 != null) sender.sendEmail(new string[] { email1, subject, body });
                sender.sendEmail(new string[] { email2, subject, body });
                             
            }
            return result;
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            Responses result = new Responses();
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Verificar");
            result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
            if (result.Info == "ok")
            {                
                //notificamos
                string emailAutorizante = this.Pedido.Autorizante.email;
                string emailSolicitante = this.Pedido.Solicitante.email;
                //string email3 = "david.ortega@cba.gov.ar";
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string bodyAutorizante = "Se ha registrado un pedido de acceso remoto solicitado por <b>" + Pedido.Solicitante.displayName +
                    "</b> con el nro. <b>" + Pedido.Id + "</b> y fue derivado para que usted lo autorice. <br>" +
                    "Puede hacerlo desde <a href='https://sofit.cba.gov.ar'>Aquí</a> ingresando en 'Mis Pedidos de Acceso' o a través " +
                        " del portal de la dirección: <a href='https://portaldgcit.cba.gov.ar/'>Aquí</a><br>Dirección General de IT.";
                string bodySolicitante = "El pedido de acceso remoto solicitado por usted con el nro. <b>" + this.Pedido.Id + "</b> fue derivado para que lo autorice <b>" + this.Pedido.Autorizante.displayName +
                    "</b>. Revise en los movimientos las observaciones." +
                          ".<br> Dirección General de IT.";
                sender.sendEmail(new string[] { emailAutorizante, subject, bodyAutorizante });
                sender.sendEmail(new string[] { emailSolicitante, subject, bodySolicitante });            
                
            }
            return result;
        }

        public override Responses paraRealizar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses realizado(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("No se puede terminar un pedido que no ha sido aprobado");
        }

        public override Responses rechazar(string nombreUsuario,string obs)
        {
            Responses result = new Responses();
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Rechazado");
            result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
            if (result.Info == "ok")
            {
                result.Detail = "El pedido fue rechazado";
                //notificamos
                string emailAutorizante = this.Pedido.Autorizante != null ? Pedido.Autorizante.email : null;
                string emailSolicitante = this.Pedido.Solicitante.email;
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string body = "El pedido nro.<b> " + this.Pedido.Id + "</b> fue rechazado el día " + DateTime.Now.ToShortDateString() +
                    " " + DateTime.Now.ToShortTimeString() + " hrs. por <b>" + nombreUsuario + "</b>. Revise en los movimientos las observaciones.";
                                         
                sender.sendEmail(new string[] { emailSolicitante, subject, body });
                if (emailAutorizante != null) sender.sendEmail(new string[] { emailAutorizante, subject, body });
            }
            return result;
        }

        public override bool requiereAutorizacion()
        {
            return true;
        }

        public override Responses revisar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("No Implementado");
        }
    }
}