﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Reglas
{
    public class TipoAccesoVista
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}