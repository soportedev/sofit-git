﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoCancelado : EstadoPedido
    {
        public EstadoCancelado(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("No se puede aprobar un pedido Cancelado");
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("No se puede cancelar un pedido Cancelado");
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("El pedido está Cancelado");
        }

        public override Responses realizado(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("El pedido está Cancelado");
        }

        public override Responses rechazar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("El pedido no se puede rechazar si ya está cancelado");
        }

        public override bool requiereAutorizacion()
        {
            throw new NotImplementedException();
        }

        public override Responses revisar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException("El pedido está Cancelado");
        }
    }
}