﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class UsuarioVista
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string displayName { get; set; }
        public string email { get; set; }
        public string dominio { get; set; }
    }
}