﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class ExtremosPedido
    {
        public string Tipo { get; set; }        
        public string Ip { get; set; }        
        public string Nombre { get; set; }
    }
}