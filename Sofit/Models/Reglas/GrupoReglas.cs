﻿using soporte.Models.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Reglas
{
    public class GrupoReglas
    {
        public ExtremosPedido Origen { get; set; }
        public ExtremosPedido Destino { get; set; }
        public UsuarioVista Usuario { get; set; }
    }
}