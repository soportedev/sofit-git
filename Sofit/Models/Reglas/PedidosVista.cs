﻿using Sofit.Models.Reglas;
using soporte.Areas.Soporte.Models.Displays;
using soporte.Models.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class PedidosVista : IComparable
    {
        public bool esOperaciones { get; set; }
        public bool esAdmin { get; set; }
        public int idPedido { get; set; }
        public string TipoPedido { get; set; }
        public string Asunto { get; set; }
        public string Descripcion { get; set; }
        public UsuarioVista Solicitante { get; set; }
        public UsuarioVista Aprobador { get; set; }
        public UsuarioVista Autorizante { get; set; }
        public UsuarioVista UsuarioAcceso { get; set; }
        public string DetalleAcceso { get; set; }
        public List<GrupoReglas> Reglas { get; set; }
        public string Prioridad { get; set; }
        public List<EstadosPedVista> DetalleEstados { get; set; }
        public EstadoPedidoVista Estado
        {
            get
            {
                return EstadoReglaFactory.getEstadoVista(this.idEstado);
            }
            set { }
        }
        public string[] Acciones
        {
            get
            {
                string[] Acciones = null;
                switch (Estado.Nombre)
                {
                    case "Iniciado":
                        if (MiRol == "Administrador")
                        {
                            if (TipoPedido == "Acceso Remoto")
                            {
                                Acciones = new string[] { "Para Aprobar", "Rechazar" };
                            }
                            else Acciones = new string[] { "Aprobar", "Rechazar" };
                        }
                        if (MiRol == "Operaciones" & TipoPedido == "Acceso Remoto")
                        {
                            Acciones = new string[] { "Para Aprobar", "Rechazar" };
                        }
                        if (MiRol == "Solicitante") Acciones = new string[] { "Cancelar" };
                        if (MiRol == "Autorizante") Acciones = new string[] { "Aprobar", "Rechazar" };
                        ;
                        break;
                    case "Aprobado":
                        if (MiRol == "Administrador")
                        {
                            Acciones = new string[] { "Para Autorizar", "Realizado", "Rechazar" };

                        }
                        break;
                    case "Autorizar":
                        if (MiRol == "Administrador")
                        {
                            Acciones = new string[] { "Autorizado", "Rechazar" };
                        }
                        if (MiRol == "Aprobador") Acciones = new string[] { "Autorizado", "Rechazar" };
                        break;
                    case "Verificar":
                        if (MiRol == "Autorizante" | MiRol=="Administrador")
                        {
                            Acciones = new string[] { "Aprobar", "Rechazar" };
                        }
                        break;
                    case "Rechazado":
                        break;
                    case "Cancelado":
                        break;
                }
                return Acciones;
            }
            set { }
        }
        public List<string> puertos { get; set; }
        public List<string> protocolos { get; set; }
        public string Jurisdiccion { get; set; }
        public string Dependencia { get; set; }
        public int idEstado { get; set; }
        public DateTime FechaPedidoDT { get; set; }
        public string MiNombre { get; set; }
        public string FechaPedidoStr
        {
            get
            {
                return FechaPedidoDT.ToShortDateString();
            }
            set { }
        }
        public DateTime FechaFinPedido { get; set; }
        public string FechaFinPedidoString { get; set; }
        private int diasvenc = 0;
        public int DiasVencimiento
        {
            get
            {
                return diasvenc;
            }
            set
            {
                this.diasvenc = value;
            }
        }
        private DateTime FechaVencimiento { get; set; }
        public string FechaVencimientoString
        {
            get
            {
                if (FechaFinPedido != null)
                {
                    return FechaVencimiento.ToShortDateString();
                }
                else
                {
                    if (Estado.Nombre == "Cancelado" | Estado.Nombre == "Realizado" | Estado.Nombre == "Rechazado")
                    {
                        TimeSpan periodo = new TimeSpan(this.DiasVencimiento, 0, 0, 0);
                        DateTime fec = this.FechaFinPedido.Add(periodo);
                        return fec.ToShortDateString();
                    }
                    else
                    {
                        return "N/A";
                    }
                }
            }
            set { }
        }
        private string mirol;
        public string MiRol
        {
            get
            {
                return mirol;
            }
            set { mirol = value; }
        }
        public int CompareTo(object obj)
        {
            PedidosVista otro = obj as PedidosVista;
            if (this.FechaPedidoDT > otro.FechaPedidoDT) return -1;
            if (this.FechaPedidoDT < otro.FechaPedidoDT) return 1;
            return 0;
        }
        public string Archivo { get; set; }
        public TicketVista Ticket { get; set; }
        public string TipoAcceso { get; set; }
        public string OrigenAcceso { get; set; }
        public HostPedido HostAcceso { get; set; }
    }
}