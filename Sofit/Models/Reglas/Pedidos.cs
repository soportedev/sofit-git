﻿using Sofit.Areas.Seguridad.Models;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Models.Reglas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public abstract class Pedidos
    {
        public int Id { get; set; }
        public string asunto { get; set; }
        public string TipoDePedido { get; set; }
        public string descripcion { get; set; }
        public int Vencimiento { get; set; }
        public int Prioridad { get; set; }
        public EstadoPedido EstadoActual { get; set; }
        //public UsuarioVista Usuario { get; set; }
        public UsuarioVista Solicitante { get; set; }
        public UsuarioVista Autorizante { get; set; }
        public UsuarioVista Aprobador { get; set; }
        public JurisdiccionVista Jurisdiccion { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public Responses registrarBaseCambioEstado(string usuario, string observaciones)
        {
            Responses result = new Responses();
            if (EstadoActual != null)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    PedidosSeguridad pedido = dbc.PedidosSeguridad.Where(c => c.id == Id).Single();
                    pedido.id_estado = EstadoActual.Id;
                    EstadosXPedido exp = dbc.EstadosXPedido.Where(c => c.id_pedido == this.Id & !c.fecha_hasta.HasValue).Single();
                    exp.fecha_hasta = DateTime.Now;
                    EstadosXPedido expn = new EstadosXPedido
                    {
                        id_estado = this.EstadoActual.Id,
                        fecha_desde = DateTime.Now,
                        id_pedido = this.Id,
                        nombreTecnico = usuario,
                        observaciones = observaciones
                    };
                    if (EstadoActual.Nombre == "Rechazado" | EstadoActual.Nombre == "Cancelado" | EstadoActual.Nombre == "Realizado")
                    {
                       expn.fecha_hasta = DateTime.Now;
                       pedido.fecha_fin = DateTime.Now;
                    }
                    dbc.EstadosXPedido.Add(expn);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            return result;
        }
        public Responses generarTicket()
        {
            Responses result = new Responses();
            int idTipificacion = 0;
            int idtipoticket = 0;
            int idprioridadticket = 0;
            int cliente = 0;

            GeneracionTicket gen = new GeneracionTicket();
            
            IncidenciasManager im = new IncidenciasManager();
            string observaciones = "Ticket generado a través del Servicio 8 asociado al pedido número :" + Id + ", \n " +
                "el cual fue originado por " + Solicitante.displayName + " y aprobado por " + Autorizante.displayName + ".";
            IDirecciones subDirSeg = DireccionesFactory.getDireccion("Seguridad Informática");
            int idUsuario = Solicitante.id;
            List<string> referentes = new List<string> { Autorizante.email, Solicitante.email };
            using (var dbc = new IncidenciasEntities())
            {
                dbc.PedidosSeguridad.Where(c => c.id == Id).Single().id_prioridad = Prioridad;
                dbc.SaveChanges();
                idtipoticket = dbc.TipoTicket.Where(c => c.nombre == "Solicitud").Single().id;
                idprioridadticket = dbc.PrioridadTicket.Where(c => c.id == Prioridad).Single().id;
                var contacto = dbc.Contactos.Where(c => c.dni == Solicitante.nombre);
                if (contacto.Count() > 0)
                {
                    cliente = contacto.First().id_contacto;
                }
                else
                {
                    Contactos nuevo = new Contactos
                    {
                        dni = Solicitante.nombre,
                        nombre = Solicitante.displayName,
                        id_dependencia = this.Dependencia.idDependencia,
                        email = Solicitante.email
                    };
                    dbc.Contactos.Add(nuevo);
                    dbc.SaveChanges();
                    cliente = nuevo.id_contacto;
                }
                if (this is PedidoRegla)
                {
                    var tip = dbc.Tipificaciones.Where(c => c.nombre.Equals("Pedido de Reglas")).SingleOrDefault();
                    if (tip != null)
                    {
                        idTipificacion = tip.id_tipificacion;
                    }
                }
                if (this is PedidoAcceso)
                {
                    var tip = dbc.Tipificaciones.Where(c => c.nombre.Equals("Pedido Acceso Remoto")).SingleOrDefault();
                    if (tip != null)
                    {
                        idTipificacion = tip.id_tipificacion;
                    }
                }
            }
            string pathImagen = string.Empty;
            string pathFile = string.Empty;
            string mime = string.Empty;
            result = gen.generacionTicketsPedidos("nuevo", idTipificacion, idtipoticket, idprioridadticket, Dependencia.idDependencia, idUsuario, observaciones, referentes, cliente, pathImagen, pathFile, mime);
            using (var dbc = new IncidenciasEntities())
            {
                var id = dbc.Incidencias.Where(c => c.numero == result.Detail).Single().id_incidencias;
                var pedido = dbc.PedidosSeguridad.Where(c => c.id == Id).Single();
                pedido.id_ticket = id;
                dbc.SaveChanges();
            }
            return result;
        }
        public string Archivo { get; set; }
        public TicketVista TicketAsociado { get; set; }
    }
}