﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class EstadosPedVista
    {
        public string Estado { get; set; }
        public string FechaDesde {
            get {
                return fdesde.ToString("dd/MM/yyyy HH:mm:ss");
            } set { } }
        public string FechaHasta {
            get
            {
                return fhasta != null ? fhasta.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
            }
            set { }
        }
        public string Tecnico { get; set; }
        public string Observaciones { get; set; }
        public DateTime fdesde { get; set; }
        public DateTime ? fhasta { get; set; }
    }
}