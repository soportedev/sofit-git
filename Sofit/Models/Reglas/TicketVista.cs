﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public class TicketVista:IComparable
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Estado { get; set; }
        public DateTime Fecha { get; set; }
        public string FechaStr
        {
            get
            {
                return Fecha.ToShortDateString();
            }
            set
            {

            }
        }

        public int CompareTo(object obj)
        {
            TicketVista other = obj as TicketVista;
            return other.Fecha.CompareTo(this.Fecha);
        }
    }
}