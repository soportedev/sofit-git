﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.Reglas
{
    public abstract class EstadoPedido
    {
        public string EmailSeguridad {
            get
            {
                return "Seguridad.Informatica@cba.gov.ar";
            }
        }
        public string EmailOperaciones
        {
            get
            {
                return "operaciones.noc@cba.gov.ar";
            }
        }
        public int Id { get; set; }
        public Pedidos Pedido { get; set; }
        public string Nombre { get; set; }
        public abstract Responses rechazar(string nombreUsuario,string obs);
        public abstract Responses aprobar(string nombreUsuario,string obs);
        public abstract Responses paraRealizar(string nombreUsuario,string obs);
        public abstract Responses realizado(string nombreUsuario,string obs);
        public abstract Responses revisar(string nombreUsuario,string obs);
        public abstract Responses cancelar(string nombreUsuario,string obs);
        public abstract Responses Autorizar(string nombreUsuario, string obs);
        public abstract bool requiereAutorizacion();
        public abstract Responses paraAprobar(string nombreUsuario, string obs);
    }
}