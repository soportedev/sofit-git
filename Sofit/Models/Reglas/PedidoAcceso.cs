﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.Models.Reglas;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class PedidoAcceso : Pedidos
    {
        public string HostOrigen { get; set; }
        public string IpOrigen { get; set; }
        public string TipoOrigen { get; set; }
        public string Detalle { get; set; }
        public string TipoAcceso { get; set; }
        public UsuarioVista Usuario { get; set; }
    }
}