﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoRechazado : EstadoPedido
    {
        public EstadoRechazado(bool look)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var est = dbc.EstadosPedidos.Where(c => c.nombre == "Rechazado").Single();
                base.Id = est.id;
                base.Nombre = est.nombre;
            }
        }
        public EstadoRechazado(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }

        public override Responses realizado(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }

        public override Responses rechazar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }

        public override bool requiereAutorizacion()
        {
            return false;
        }

        public override Responses revisar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException("El pedido está rechazado");
        }
    }
}