﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Helpers;
using soporte.Models.Statics;

namespace soporte.Models.Reglas
{
    public class EstadoParaVerificar : EstadoPedido
    {        
        public EstadoParaVerificar(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            Responses result = new Responses();

            Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Aprobado");
            result = Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
            if (result.Info == "ok")
            {
                result = Pedido.generarTicket();
            }
            return result;
        }

        public override Responses Autorizar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses cancelar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraAprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraRealizar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses realizado(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses rechazar(string nombreUsuario,string obs)
        {
            Responses result = new Responses();
            this.Pedido.EstadoActual = EstadoReglaFactory.getEstadoBiz("Rechazado");
            result = this.Pedido.registrarBaseCambioEstado(nombreUsuario, obs);
            if (result.Info == "ok")
            {
                result.Detail = "El pedido fue rechazado";

                //notificamos
                string email1 = this.Pedido.Autorizante.email;
                string email2 = this.Pedido.Solicitante.email;
                EmailSender sender = new EmailSender();
                string subject = "Sofit - Servicio de Administración de Acceso a Servicios Informáticos";
                string body = "El pedido nro. " + this.Pedido.Id + " fue rechazado el día " + DateTime.Now.ToShortDateString() +
                    " " + DateTime.Now.ToShortTimeString() + " hrs. por " + nombreUsuario + ". Revise en los movimientos las observaciones." +
                          ".\n Dirección General de IT.";
                
                sender.sendEmail(new string[] { email1, subject, body });
                sender.sendEmail(new string[] { email2, subject, body });


            }
            return result;
        }

        public override bool requiereAutorizacion()
        {
            return true;
        }

        public override Responses revisar(string nombreUsuario,string obs)
        {
            throw new NotImplementedException();
        }
    }
}