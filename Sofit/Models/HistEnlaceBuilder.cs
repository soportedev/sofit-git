﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class HistEnlaceBuilder
    {
        private HistoricoEnlace historico;
        public void Construir(string nroOblea, bool movAreas)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var eq = from u in dbc.EnlaceComunicaciones
                         where u.UnidadConf.nombre == nroOblea
                         select u;
                if (eq != null & eq.Count() > 0)
                {
                    EnlaceComunicaciones primero = eq.First();
                    historico = new HistoricoEnlace
                    {
                        idEnlace = primero.id_enlace,
                        Nombre = primero.UnidadConf.nombre                        
                    };
                    var historicoEnlace = from u in dbc.EstadoXEnlace
                                           where u.id_enlace == primero.id_enlace & u.Incidencias.fecha_fin.HasValue
                                           group u by u.id_incidencia into g
                                           orderby g.Key
                                           select g;
                    if (historicoEnlace.Count() > 0)
                    {
                        foreach (var incid in historicoEnlace)
                        {
                            var firstMov = incid.First();
                            HistoricoIncidencia incidencia = new HistoricoIncidencia
                            {
                                Numero = firstMov.Incidencias.numero,
                                FechaInicio = firstMov.fecha_inicio,
                                FechaFin=firstMov.fecha_fin.Value,
                                Dependencia = firstMov.Incidencias.Dependencia != null ? firstMov.Incidencias.Dependencia.descripcion : "N/A",
                                Jurisdiccion = firstMov.Incidencias.Dependencia != null ? firstMov.Incidencias.Dependencia.Jurisdiccion.descripcion : firstMov.Incidencias.Jurisdiccion.descripcion 
                            };
                            historico.Incidentes.Add(incidencia);
                            //Debug.WriteLine("Key: {0}", movimiento.Key);
                            //foreach (var mov in movimiento)
                            //{
                            //    Debug.WriteLine("\t{0}, {1}", mov.id_incidencia, mov.fecha_inicio);
                            //}
                        }
                    }
                    //else
                    //{
                    //    var resoluciones = from u in dbc.Resoluciones
                    //                       where u.id_equipo == primero.id_enlace
                    //                       select u;
                    //    foreach (var res in resoluciones)
                    //    {
                    //        HistoricoIncidencia incidencia = new HistoricoIncidencia
                    //        {
                    //            Numero = res.Incidencias.numero,
                    //            FechaInicio = res.Incidencias.fecha_inicio,
                    //            FechaFin = res.Incidencias.fecha_fin.Value,
                    //            Dependencia = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.descripcion : "N/A",
                    //            Jurisdiccion = res.Incidencias.Dependencia != null ? res.Incidencias.Dependencia.Jurisdiccion.descripcion : res.Incidencias.Jurisdiccion.descripcion
                    //        };
                    //        if (!historico.Incidentes.Contains(incidencia))
                    //            historico.Incidentes.Add(incidencia);
                    //    }
                    //}

                }
            }

        }
        internal HistoricoEnlace getResult()
        {
            return historico;
        }
    }
}