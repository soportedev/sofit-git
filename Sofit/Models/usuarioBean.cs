﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Script.Serialization;
using soporte.Models.Statics;
using soporte.Areas.Soporte.Models;
using soporte.Areas.Soporte.Models.Statics;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;

namespace soporte.Models
{
    public class usuarioBean:IComparable
    {

        public string Avatar { get; set; }
        public DireccionVista Direccion { get; set; }
        public List<int> idsPerfiles = new List<int>();
        public string PathAvatar
        {
            get
            {
                return "/Content/img/Photo/" + Avatar;
            }
            set
            {
            }
        }
        public bool EsInterno { get; set; }
        public string NombreCompleto { get; set; }
        public int IdUsuario { get; set; }
        public int idDireccion { get; set; }
        public int Acceso { get; set; }
        public string Firma { get; set; }
        public string email { get; set; }
        public string HostLogin { get; set; }
        public HttpSessionStateBase Session { get; set; }
        public List<RolUsuario> Roles { get; set; }
        public Perfil[] Perfiles
        {
            get
            {
                return perfiles.ToArray();
            }
            set
            {
                perfiles = value.ToList();
            }
        }
        [ScriptIgnore]
        public string Nombre { get; set; }

        public int Personalizacion { get; set; }
        [ScriptIgnore]
        public string Password { get; set; }
        [ScriptIgnore]
        public bool Activo { get; set; }
        public string Estado
        {
            get
            {
                if (Activo) return "Habilitado";
                else return "Inhabilitado";
            }
        }

        public bool TieneMensaje { get; set; }

        public bool NecesitaReload { get; set; }

        public Mensaje mensaje { get; set; }

        public List<VentanaUsuario> ventanas;
        private List<Perfil> perfiles;
        public usuarioBean()
        {
            perfiles = new List<Perfil>();
            ventanas = new List<VentanaUsuario>();
            NecesitaReload = false;
        }
        public Responses AgregarPerfil(Perfil perfil)
        {
            Responses result = new Responses();
            if (!this.perfiles.Contains(perfil))
            {
                this.perfiles.Add(perfil);
                saveDB();
                result.Info = "ok";
                result.Detail = perfil.nombre;
            }
            else
            {
                result.Info = "Error";
                result.Detail = "El Perfil ya está agregado";
            }
            return result;
        }
        internal Responses QuitarPerfil(Perfil perfil)
        {
            Responses result = new Responses();
            if (this.perfiles.Contains(perfil)&&this.perfiles.Count>1)
            {
                this.perfiles.Remove(perfil);
                saveDB();
                result.Info = "ok";
                result.Detail ="";
            }
            else
            {
                result.Info = "Error";
                result.Detail = "No puede quedar sin perfil un usuario.";
            }
            return result;
        }
        public void addPerfil(Perfil perfil)
        {
            this.perfiles.Add(perfil);
        }
        public List<Perfil> getPerfiles()
        {
            return this.perfiles;
        }
        public Perfil[] getPerfilesConFormat()
        {
            return this.perfiles.ToArray();
        }
        [ScriptIgnore]
        public usuarioHandler handler { get; set; }              

        internal Responses updateVentana(VentanaUsuario otherWindow)
        {
            Responses result = new Responses();
            foreach (VentanaUsuario ventana in ventanas)
            {
                if (ventana.id == otherWindow.id)
                {
                    if (ventana.height != otherWindow.height | ventana.width != otherWindow.width | ventana.visible != otherWindow.visible)
                    {
                        this.NecesitaReload = true;
                        ventana.height = otherWindow.height;
                        ventana.width = otherWindow.width;
                        ventana.visible = otherWindow.visible;
                        handler.saveVentanaDB(ventana, this);
                        result.Info = "ok";
                        break;
                    }
                }
            }
            return result;
        }
        internal void AddVentana(VentanaUsuario ventana)
        {
            ventanas.Add(ventana);
        }

        internal void recreateWindows()
        {
            var values = Enum.GetValues(typeof(Constants.NombreVentana));
            foreach (Constants.NombreVentana val in values)
            {
                bool flag = false;
                foreach (VentanaUsuario ventana in ventanas)
                {
                    if (val.ToString() == ventana.id)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    handler.crearVentana(val.ToString(),this);
                    this.NecesitaReload = true;
                }
            }            
        }
        internal Responses setTheme(int tema)
        {
            this.Personalizacion = tema;
            return handler.saveDB(this);
        }
        internal Responses saveDB()
        {
            return this.handler.saveDB(this);
        }       
        internal Constantes.AccesosPagina getAccess(string page)
        {
            List<Constantes.AccesosPagina> listaAcceso = new List<Constantes.AccesosPagina>();
            Constantes.AccesosPagina acceso=Constantes.AccesosPagina.Full;
            foreach (Perfil p in perfiles)
            {
                listaAcceso.Add(p.getAcceso(page));
            }
            if (listaAcceso.Contains(Constantes.AccesosPagina.Full)) acceso= Constantes.AccesosPagina.Full;
            else
            {
                if (listaAcceso.Contains(Constantes.AccesosPagina.ReadOnly)) acceso = Constantes.AccesosPagina.ReadOnly;
                else acceso = Constantes.AccesosPagina.NoAccess;
            }
            return acceso;
        }

        public string getHomePage()
        {
            string home = string.Empty;
            List<string> homePage = new List<string>();
            if (EsInterno)
            {               
                foreach (Perfil p in perfiles)
                {
                    string hp = p.getHomePage();
                    homePage.Add(hp);
                }
                home = homePage.ElementAt(0);
            }
            return home;
        }

        internal string getFirma()
        {
            throw new NotImplementedException();
        }

        internal Responses updateFirma()
        {
            return this.handler.updateFirma(this);
        }

        public int CompareTo(object obj)
        {
            usuarioBean otro = (usuarioBean)obj;
            return this.NombreCompleto.CompareTo(otro.NombreCompleto);
        }
       
        public override bool Equals(object o)
        {
            usuarioBean other = o as usuarioBean;
            if (other.IdUsuario == this.IdUsuario) return true;
            return false;
        }
        public override int GetHashCode()
        {
            return this.IdUsuario.GetHashCode();
        }
    }
}
