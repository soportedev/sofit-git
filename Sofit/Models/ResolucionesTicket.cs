﻿using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sofit.Models;

namespace soporte.Models
{
    public class ResolucionesTicket:ResolucionesVista
    {
        Reporte reporte;
        public override string getResolucionesParametros(HistoricosParametros par)
        {
            String result = string.Empty;
            try
            {
                switch (par.TipoReport)
                {
                    case "anual":
                        reporte = new ReporteAnual();
                        result = reporte.ObtenerReporte(par);
                        break;
                    case "mensual":
                        reporte = new ReporteMensual();
                        result = reporte.ObtenerReporte(par);
                        break;
                    case "listado":
                        reporte = new ReporteListado();
                        result = reporte.ObtenerReporte(par);
                        //result = listado(par);
                        break;
                    case "creados":
                        reporte = new ReporteCreados();
                        result = reporte.ObtenerReporte(par);
                        break;
                    case "creadosycerradostotales": reporte = new ReportesCreadosYCerrados();
                        result = reporte.ObtenerReporte(par);
                        break;
                    case "mensualjurisdiccion":reporte = new ReporteMensualXJur();
                        result = reporte.ObtenerReporte(par);
                        break;
                    case "mensualTotal":reporte = new ReporteMensualTotal();
                        result = reporte.ObtenerReporte(par);
                        break;
                }                
            }            
            catch(Exception e)
            {
                result = e.Message;
            }
            return result;
        }        
        
        #region creados y cerrados diarios
            private string CreadosYCerrados(HistoricosParametros par)
            {
                if (par.DetalleSubdir)
                {
                    return toHtmlTotalesCerrados(lookupTicketCerrados(par));
                }
                else
                {
                    return toHtmlTotalesCerradosGeneral(lookupTicketCerradosTotales(par));
                }
            }
            //con detalle de subdireccion
            private string toHtmlTotalesCerrados(IEnumerable<TicketsCreados> resGroup)
            {
                StringBuilder sb = new StringBuilder();
                if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
                else
                {
                    sb.Append("<table class='info'><tr class='ui-state-active'><th>Subdirección Generadora</th>" +
                        "<th>Tipo de Ticket</th><th>Prioridad</th><th>Fecha</th><th>Creados</th><th>Cerrados en término</th>" +
                        "<th>Fuera de término</th><th>Fuera del Márgen</th><th>Sin Cerrar</th></tr>");
                    foreach (TicketsCreados j in resGroup)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + j.SubDirGeneradora + "</td>");
                        sb.Append("<td>" + j.TipoTicket + "</td>");
                        sb.Append("<td>" + j.PrioridadTicket + "</td>");
                        sb.Append("<td>" + j.FechaCreacion + "</td>");
                        sb.Append("<td>" + j.Cantidad + "</td>");
                        sb.Append("<td>" + j.DentroDelTiempo + "<span> (" + j.DentroDelTiempoPorc + " %)</span></td>");
                        sb.Append("<td>" + j.DentroDelMargen + "<span> (" + j.DentroDelMargenPorc + " %)</span></td>");
                        sb.Append("<td>" + j.FueraMargen + "<span> (" + j.FueraDelMargenPorc + " %)</span></td>");
                        sb.Append("<td>" + j.SinCerrar + "<span> (" + j.SinCerrarPorc + " %)</span></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                }
                return sb.ToString();
            }
            private List<TicketsCreados> lookupTicketCerrados(HistoricosParametros par)
            {
                List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

                using (var dbc = new IncidenciasEntities())
                {

                    List<TicketsQuery> tickets =
                               (from u in dbc.Incidencias
                                where DateTime.Compare(u.fecha_inicio, par.FechaHasta) < 0 && DateTime.Compare(u.fecha_inicio, par.FechaDesde) > 0 &&
                                (par.idArea == 0 || u.direccion_generacion == par.idArea) &&
                                (par.idPrioridadTicket==0 | u.id_prioridad == par.idPrioridadTicket) &&
                                (par.idTipoTicket ==0| u.id_tipo == par.idTipoTicket)&&
                                (par.idJurisdiccion==0 | u.Jurisdiccion.id_jurisdiccion == par.idJurisdiccion | u.Dependencia.Jurisdiccion.id_jurisdiccion == par.idJurisdiccion)
                                select new TicketsQuery
                                {
                                    DireccionGeneracion = u.Direcciones.nombre,
                                    TipoTicket = u.TipoTicket.nombre,
                                    Numero = u.numero,
                                    FechaGeneracion = u.fecha_inicio,
                                    PrioridadTicket = u.PrioridadTicket.nombre,
                                    HorasResolucion = u.Tipificaciones.Servicios.tiempo_res.Value,
                                    FechaCierre = u.fecha_fin
                                }).ToList();
                    List<TicketsQuery> ticketsFiltrados = tickets.Where(c => c.FechaResolucion <= par.FechaHasta).ToList();
                    ticketsCreados = (from u in ticketsFiltrados
                                      group u by new
                                      {
                                          u.DireccionGeneracion,
                                          u.FechaDia,
                                          u.FechaAnio,
                                          u.FechaMes,
                                          u.PrioridadTicket,
                                          u.TipoTicket
                                      } into g
                                      //orderby g.Count() descending
                                      select new TicketsCreados
                                      {
                                          SubDirGeneradora = g.Key.DireccionGeneracion,
                                          PrioridadTicket = g.Key.PrioridadTicket,
                                          TipoTicket = g.Key.TipoTicket,
                                          FechaCreacion = new DateTime(g.Key.FechaAnio, g.Key.FechaMes, g.Key.FechaDia),
                                          Cantidad = g.Count()
                                      }).ToList();
                }
                ticketsCreados.Sort();
                return ticketsCreados;
            }
            //-->
            //sin detalle de subdireccion
            private string toHtmlTotalesCerradosGeneral(IEnumerable<TicketsCreados> resGroup)
            {
                StringBuilder sb = new StringBuilder();
                if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
                else
                {
                    sb.Append("<table class='info'><tr class='ui-state-active'>" +
                        "<th>Tipo de Ticket</th><th>Prioridad</th><th>Fecha</th><th>Creados</th><th>Cerrados en término</th>" +
                        "<th>Fuera de término</th><th>Sin Cerrar</th></tr>");
                    foreach (TicketsCreados j in resGroup)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + j.TipoTicket + "</td>");
                        sb.Append("<td>" + j.PrioridadTicket + "</td>");
                        sb.Append("<td>" + j.Cantidad + "</td>");
                        sb.Append("<td>" + j.DentroDelTiempo + "<span>" + j.DentroDelTiempo / j.Cantidad * 100 + " %</span></td>");
                        sb.Append("<td>" + j.DentroDelMargen + "<span>" + j.DentroDelMargen / j.Cantidad * 100 + " %</span></td>");
                        sb.Append("<td>" + j.FueraMargen + "<span>" + j.FueraMargen / j.Cantidad * 100 + " %</span></td>");
                        sb.Append("<td>" + j.SinCerrar + "<span>" + j.SinCerrar / j.Cantidad * 100 + " %</span></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                }
                return sb.ToString();
            }
            private List<TicketsCreados> lookupTicketCerradosTotales(HistoricosParametros par)
            {
                List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

                using (var dbc = new IncidenciasEntities())
                {
                    List<TicketsQuery> tickets =
                               (from u in dbc.Incidencias
                                where DateTime.Compare(u.fecha_inicio, par.FechaHasta) < 0 &&
                                DateTime.Compare(u.fecha_inicio, par.FechaDesde) > 0 &&
                                (par.idPrioridadTicket ==0| u.id_prioridad == par.idPrioridadTicket) &&
                                (par.idTipoTicket ==0| u.id_tipo == par.idTipoTicket) &&
                                (par.idArea == 0 || u.direccion_generacion == par.idArea) &&
                                (par.idJurisdiccion==0 | u.Jurisdiccion.id_jurisdiccion == par.idJurisdiccion | u.Dependencia.Jurisdiccion.id_jurisdiccion == par.idJurisdiccion)
                                select new TicketsQuery
                                {
                                    DireccionGeneracion = u.Direcciones.nombre,
                                    TipoTicket = u.TipoTicket.nombre,
                                    Numero = u.numero,
                                    FechaGeneracion = u.fecha_inicio,
                                    PrioridadTicket = u.PrioridadTicket.nombre,
                                    HorasResolucion = u.Tipificaciones.Servicios.tiempo_res.Value,
                                    FechaCierre = u.fecha_fin
                                }).ToList();
                    List<TicketsQuery> ticketsFiltrados = tickets.Where(c => c.FechaResolucion <= par.FechaHasta).ToList();
                    ticketsCreados = (from u in ticketsFiltrados
                                      group u by new
                                      {
                                          u.FechaDia,
                                          u.FechaAnio,
                                          u.FechaMes,
                                          u.PrioridadTicket,
                                          u.TipoTicket
                                      } into g
                                      //orderby g.Count() descending
                                      select new TicketsCreados
                                      {
                                          PrioridadTicket = g.Key.PrioridadTicket,
                                          TipoTicket = g.Key.TipoTicket,
                                          Cantidad = g.Count(),
                                          DentroDelTiempo = ticketsFiltrados.Where(c => c.DentroDelTiempo & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                          DentroDelMargen = ticketsFiltrados.Where(c => c.DentroDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                          FueraMargen = ticketsFiltrados.Where(c => c.FueraDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                          SinCerrar = ticketsFiltrados.Where(c => c.SinCerrar & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count()
                                      }).ToList();
                }
                ticketsCreados.Sort();
                return ticketsCreados;
            }    
            //-->
            
        #endregion
              
        public override string getResolucionesTotalesTecnico(HistoricosParametros par)
        {
            throw new NotImplementedException();
        }
        public override string getResolucionesTotalesJurisdiccion(HistoricosParametros par)
        {
            throw new NotImplementedException();
        }
        public override string getResolucionesTotalesTrabajo(HistoricosParametros par)
        {
            throw new NotImplementedException();
        }
    }
}