﻿using System;
using soporte.Models.Statics;
using soporte.Models.Interface;
using Sofit.DataAccess;
using System.Linq;

namespace soporte.Models
{
    public class EstadoIncidenciaDerivado:IEstadoIncidencia{       
        
        public override Responses derivar(string area, int tecnico,int idUsuario)
        {
            throw new NotImplementedException();
        }        

        public string cambiarEstado(IEstadoIncidencia state)
        {
            throw new NotImplementedException();
        }       
       
        public override Responses solucionar(int idUsuario)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    int idEstadoCerrado = (from u in dbc.Estado_incidente where u.descripcion == "Cerrado" select u.id_estado).Single();
                    int idAreaTeleco = (from u in dbc.area where u.descripcion == "Telecomunicaciones" select u.id_area).Single();
                    int idTecnicoTele = (from u in dbc.tecnico where u.descripcion == "Telecomunicaciones" select u.id_tecnico).Single();
                    int idDireccion = (from u in dbc.Direcciones where u.nombre == "Telecomunicaciones" select u.id_direccion).FirstOrDefault();
                    var inc = from u in dbc.Incidencias
                              where u.id_incidencias == this.Incidencia.Id
                              select u;
                    if (inc.Count() == 0)
                    {
                        result.Detail = "No se encontró el incidente.";
                    }
                    else
                    {
                        Incidencias incid = inc.Single();
                        incid.fecha_fin = DateTime.Now;

                        IncidenciasActivas ica = (from ic in dbc.IncidenciasActivas
                                                  where ic.id_incidencia == incid.id_incidencias
                                                  select ic).Single();
                        dbc.IncidenciasActivas.Remove(ica);

                        AreaXIncidencia axi = (from ax in dbc.AreaXIncidencia
                                               where ax.id_incidencia == incid.id_incidencias & !ax.fecha_fin.HasValue
                                               select ax).Single();
                        axi.fecha_fin = DateTime.Now;

                        EstadoXIncidencia exi = (from ex in dbc.EstadoXIncidencia
                                                 where ex.id_incidencia == incid.id_incidencias & !ex.fecha_fin.HasValue
                                                 select ex).Single();
                        exi.fecha_fin = DateTime.Now;
                        
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }

        public override Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo,DateTime fechaActualizar, string imagen)
        {
            return this.Incidencia.AreaActual.cargarSolucion(observacion, tecnico, turno, idUsuario, tResolucionUsuarios, idEquipo, fechaActualizar,imagen);
        }
    }
}
