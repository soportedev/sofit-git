﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using Sofit.DataAccess;
using System.Text;

namespace soporte.Models
{
    public class ReporteCreados : Reporte
    {
        public override string ObtenerReporte(HistoricosParametros param)
        {
            return Creados(param);
        }
        #region creados
        private string Creados(HistoricosParametros par)
        {
            if (par.DetalleSubdir)
            {
                return toHtmlTotales(lookupRes(par));
            }
            else
            {
                return toHtmlTotalesGeneral(lookupResTotales(par));
            }
        }
        //con detalle subdireccion
        private List<TicketsCreados> lookupRes(HistoricosParametros par)
        {
            List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

            using (var dbc = new IncidenciasEntities())
            {

                List<TicketsQuery> tickets = (dbc.sp_listado_tickets(new DateTime(par.anioReporte, par.mesReporte, 1, 0, 0, 0),
                    new DateTime(par.anioReporte, par.mesReporte, DateTime.DaysInMonth(par.anioReporte, par.mesReporte), 23, 59, 59), par.idArea, par.idPrioridadTicket,
                            par.idTipoTicket, par.idServicio, par.idJurisdiccion).Select(c =>
                       new TicketsQuery
                       {
                           DireccionGeneracion = c.Direccion,
                           TipoTicket = c.TipoTicket,
                           Numero = c.Numero,
                           IdTicket = c.IdTicket.Value,
                           FechaGeneracion = c.FechaGeneracion.Value,
                           HoursOffline = c.TiempoSuspendido.Value,
                           HoursWeekend = c.TiempoWeekend.Value,
                           PrioridadTicket = c.PrioridadTicket,
                           HorasResolucion = c.HorasResolucion.Value,
                           HorasMargen = c.HorasMargen.Value,
                           FechaCierre = c.FechaCierrre
                       })).ToList();
                ticketsCreados = (from u in tickets
                                  group u by new
                                  {
                                      u.DireccionGeneracion,
                                      u.FechaDia,
                                      u.FechaAnio,
                                      u.FechaMes,
                                      u.PrioridadTicket,
                                      u.TipoTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      SubDirGeneradora = g.Key.DireccionGeneracion,
                                      PrioridadTicket = g.Key.PrioridadTicket,
                                      TipoTicket = g.Key.TipoTicket,
                                      FechaCreacion = new DateTime(g.Key.FechaAnio, g.Key.FechaMes, g.Key.FechaDia),
                                      Cantidad = g.Count()
                                  }).ToList();
            }
            HttpContext.Current.Session["tableSource"] = DataTableConverter.ToDataTable(ticketsCreados);
            ticketsCreados.Sort();
            return ticketsCreados;
        }
        private string toHtmlTotales(IEnumerable<TicketsCreados> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Subdirección Generadora</th><th>Tipo de Ticket</th><th>Prioridad</th><th>Fecha</th><th>Total</th></tr>");
                foreach (TicketsCreados j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.SubDirGeneradora + "</td>");
                    sb.Append("<td>" + j.TipoTicket + "</td>");
                    sb.Append("<td>" + j.PrioridadTicket + "</td>");
                    sb.Append("<td>" + j.FechaStr + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        //-->
        //sin detalle subdireccion
        private string toHtmlTotalesGeneral(IEnumerable<TicketsCreados> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Tipo de Ticket</th><th>Prioridad</th><th>Fecha</th><th>Total</th></tr>");
                foreach (TicketsCreados j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.TipoTicket + "</td>");
                    sb.Append("<td>" + j.PrioridadTicket + "</td>");
                    sb.Append("<td>" + j.FechaStr + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        private List<TicketsCreados> lookupResTotales(HistoricosParametros par)
        {
            List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

            using (var dbc = new IncidenciasEntities())
            {

                List<TicketsQuery> tickets = (dbc.sp_listado_tickets(new DateTime(par.anioReporte, par.mesReporte, 1, 0, 0, 0),
                    new DateTime(par.anioReporte, par.mesReporte, DateTime.DaysInMonth(par.anioReporte, par.mesReporte), 23, 59, 59), par.idArea, par.idPrioridadTicket,
                            par.idTipoTicket, par.idServicio, par.idJurisdiccion).Select(c =>
                       new TicketsQuery
                       {
                           DireccionGeneracion = c.Direccion,
                           TipoTicket = c.TipoTicket,
                           Numero = c.Numero,
                           IdTicket = c.IdTicket.Value,
                           FechaGeneracion = c.FechaGeneracion.Value,
                           HoursOffline = c.TiempoSuspendido.Value,
                           HoursWeekend = c.TiempoWeekend.Value,
                           PrioridadTicket = c.PrioridadTicket,
                           HorasResolucion = c.HorasResolucion.Value,
                           HorasMargen = c.HorasMargen.Value,
                           FechaCierre = c.FechaCierrre
                       })).ToList();
                ticketsCreados = (from u in tickets
                                  group u by new
                                  {
                                      u.FechaDia,
                                      u.FechaAnio,
                                      u.FechaMes,
                                      u.PrioridadTicket,
                                      u.TipoTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      PrioridadTicket = g.Key.PrioridadTicket,
                                      TipoTicket = g.Key.TipoTicket,
                                      FechaCreacion = new DateTime(g.Key.FechaAnio, g.Key.FechaMes, g.Key.FechaDia),
                                      Cantidad = g.Count()
                                  }).ToList();
            }
            HttpContext.Current.Session["tableSource"] = DataTableConverter.ToDataTable(ticketsCreados);
            ticketsCreados.Sort();
            return ticketsCreados;
        }
        //-->
        #endregion
    }
}