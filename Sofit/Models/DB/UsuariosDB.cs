﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using soporte.Models.Statics;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Interface;

namespace soporte.Models.DB
{
    public class UsuariosDB
    {      
        
        internal List<usuarioBean> getTodos(IDirecciones direccion)
        {
            List<usuarioBean> usuarios = new List<usuarioBean>();
            List<Usuarios> usuariosBase = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {                    
                    if (direccion != null)
                    {
                        usuariosBase = (from us in dbc.Usuarios
                                 where us.id_direccion == direccion.IdDireccion
                                 select us).ToList();
                    }
                    //else usuariosBase = (from us in dbc.Usuarios                              
                    //          select us).ToList();
                    foreach (Usuarios us in usuariosBase)
                    {
                        usuarioBean usuario = new usuarioBean();
                        usuario.Acceso = us.acceso;
                        usuario.Activo = us.activo.Value;
                        usuario.Firma = us.firma;
                        usuario.IdUsuario = us.id_usuario;
                        usuario.Nombre = us.nombre;
                        usuario.NombreCompleto = us.nombre_completo;                        
                        usuario.Personalizacion = us.personalizacion;

                        foreach (Perfiles p in us.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }                       
                        
                        usuarios.Add(usuario);
                    }
                    usuarios.Sort();
                }
            }
            catch (Exception)
            {
                usuarios = null;
            }
            return usuarios;
        }      
        internal List<usuarioBean>getTodos()
        {
            List<usuarioBean> usuarios = new List<usuarioBean>();
            //List<Usuarios> usuariosBase = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    
                        //usuariosBase = (from us in dbc.Usuarios                                        
                        //                select us).ToList();
                    usuarios = dbc.Usuarios.Select(c => new usuarioBean
                    {
                        Acceso = c.acceso,
                        Activo = c.activo.HasValue?c.activo.Value:false,
                        EsInterno = c.es_interno.HasValue?c.es_interno.Value:false,
                        IdUsuario = c.id_usuario,
                        Nombre = c.nombre,
                        NombreCompleto = c.nombre_completo,
                        idsPerfiles=c.Perfiles.Select(d=>d.id_perfil).ToList()
                    }).ToList();                    
                    
                    
                }
                foreach(usuarioBean u in usuarios)
                {
                    foreach(int id in u.idsPerfiles)
                    {
                        u.addPerfil(PerfilFactory.getInstancia().crear(id));
                    }
                }
                usuarios.Sort();
            }
            catch (Exception e)
            {
                var deb = e;
                usuarios = null;
            }
            return usuarios;
        }
        internal usuarioBean getUsuarioXNombre(string DispName, string nombre, string email)
        {
            usuarioBean usuario = new usuarioBean();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.nombre_completo == DispName
                             select us).SingleOrDefault();
                    if (r.nombre != nombre) r.nombre = nombre;
                    if (r.email != null) r.email = email;
                    usuario.Acceso = r.acceso;
                    usuario.Activo = r.activo.Value;
                    usuario.Firma = r.firma;
                    usuario.IdUsuario = r.id_usuario;
                    usuario.Nombre = r.nombre;
                    usuario.NombreCompleto = r.nombre_completo;                    
                    usuario.Personalizacion = r.personalizacion;
                    usuario.idDireccion = r.Direcciones.id_direccion;
                    usuario.email = r.email;
                    foreach (Perfiles p in r.Perfiles)
                    {
                        usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                    }
                    foreach (Ventana v in r.Ventana)
                    {
                        VentanaUsuario ventana = new VentanaUsuario();
                        ventana.id = v.id_ventana;
                        ventana.height = v.height;
                        ventana.width = v.width;
                        ventana.visible = v.visible;
                        usuario.AddVentana(ventana);
                    }
                    dbc.SaveChanges();
                }
            }
            catch (Exception)
            {
                usuario = null;
            }
            return usuario;
        }

        internal usuarioBean getUsuarioXId(int idUsuario)
        {
            usuarioBean usuario = new usuarioBean();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.id_usuario == idUsuario
                             select us).SingleOrDefault();

                    usuario.Acceso = r.acceso;
                    usuario.Activo = r.activo.Value;
                    usuario.Firma = r.firma;
                    usuario.IdUsuario = r.id_usuario;
                    usuario.Nombre = r.nombre;
                    usuario.NombreCompleto = r.nombre_completo;                    
                    usuario.Personalizacion = r.personalizacion;
                    foreach (Perfiles p in r.Perfiles)
                    {
                        usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                    }
                    foreach (Ventana v in r.Ventana)
                    {
                        VentanaUsuario ventana = new VentanaUsuario();
                        ventana.id = v.id_ventana;
                        ventana.height = v.height;
                        ventana.width = v.width;
                        ventana.visible = v.visible;
                        usuario.AddVentana(ventana);
                    }
                }
            }
            catch (Exception)
            {
                usuario = null;
            }
            return usuario;
        }

        internal List<usuarioBean> getUsuariosTele()
        {
            List<usuarioBean> usuarios = new List<usuarioBean>();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.Direcciones.nombre == "Telecomunicaciones"
                             select us).ToList();

                    foreach (Usuarios us in r)
                    {
                        usuarioBean usuario = new usuarioBean();
                        usuario.Acceso = us.acceso;
                        usuario.Activo = us.activo.Value;
                        usuario.Firma = us.firma;
                        usuario.IdUsuario = us.id_usuario;
                        usuario.Nombre = us.nombre;
                        usuario.NombreCompleto = us.nombre_completo;                        
                        usuario.Personalizacion = us.personalizacion;

                        foreach (Perfiles p in us.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }

                        usuarios.Add(usuario);
                    }                    
                }
            }
            catch (Exception)
            {
                usuarios = null;
            }
            return usuarios;
        }

        internal usuarioBean getUsuariosXLogin(string login)
        {
            usuarioBean usuario = new usuarioBean();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.nombre == login
                             select us).Single();
                    if(r!=null)
                    {                        
                        usuario.Acceso = r.acceso;
                        usuario.Activo = r.activo.Value;
                        usuario.Firma = r.firma;
                        usuario.IdUsuario = r.id_usuario;
                        usuario.Nombre = r.nombre;
                        usuario.NombreCompleto = r.nombre_completo;                        
                        usuario.Personalizacion = r.personalizacion;

                        foreach (Perfiles p in r.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }
                    }
                    else
                    {
                        usuario = null;
                    }
                }
            }
            catch (Exception)
            {
                usuario = null;
            }
            return usuario;
        }      

        internal Responses setPasswordFor(int userid, string pass)
        {
            Responses result = new Responses();
            using (var db = new IncidenciasEntities())
            {                
                Usuarios u = (from us in db.Usuarios
                                 where us.id_usuario == userid
                                 select us).Single();                
                db.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }

        internal Responses toggleUsuario(int p)
        {
            Responses result = new Responses();
            using (var db = new IncidenciasEntities())
            {
                Usuarios u = (from us in db.Usuarios
                              where us.id_usuario == p
                              select us).Single();
                u.activo = !u.activo;
                tecnico tec = (from t in db.tecnico
                             where t.id_usuario == p
                             select t).SingleOrDefault();
                
                if (tec == null)
                {
                    if (u.id_direccion != null)
                    {
                        area area = db.area.Where(c => c.id_direccion == u.id_direccion).First();
                        tec = new tecnico
                        {
                            activo = u.activo,
                            descripcion = u.nombre_completo,
                            id_area = area.id_area,
                            id_usuario = u.id_usuario
                        };
                        db.tecnico.Add(tec);
                    }                    
                }
                else tec.activo = u.activo;
                db.SaveChanges();
                result.Info = "ok";
                if (u.activo.Value) result.Detail = "Habilitado";
                else result.Detail = "Inhabilitado";
            }
            return result;
        }

        internal List<usuarioBean> getUsuariosMesa()
        {
            List<usuarioBean> usuarios = new List<usuarioBean>();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.Direcciones.nombre == "Mesa de Ayuda"
                             select us).ToList();

                    foreach (Usuarios us in r)
                    {
                        usuarioBean usuario = new usuarioBean();
                        usuario.Acceso = us.acceso;
                        usuario.Activo = us.activo.Value;
                        usuario.Firma = us.firma;
                        usuario.IdUsuario = us.id_usuario;
                        usuario.Nombre = us.nombre;
                        usuario.NombreCompleto = us.nombre_completo;                        
                        usuario.Personalizacion = us.personalizacion;

                        foreach (Perfiles p in us.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }

                        usuarios.Add(usuario);
                    }
                }
            }
            catch (Exception)
            {
                usuarios = null;
            }
            return usuarios;
        }

        internal List<usuarioBean> getUsuariosOperaciones()
        {
            List<usuarioBean> usuarios = new List<usuarioBean>();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var r = (from us in dbc.Usuarios
                             where us.Direcciones.nombre == "Operaciones"
                             select us).ToList();

                    foreach (Usuarios us in r)
                    {
                        usuarioBean usuario = new usuarioBean();
                        usuario.Acceso = us.acceso;
                        usuario.Activo = us.activo.Value;
                        usuario.Firma = us.firma;
                        usuario.IdUsuario = us.id_usuario;
                        usuario.Nombre = us.nombre;
                        usuario.NombreCompleto = us.nombre_completo;                        
                        usuario.Personalizacion = us.personalizacion;

                        foreach (Perfiles p in us.Perfiles)
                        {
                            usuario.addPerfil(PerfilFactory.getInstancia().crear(p.id_perfil));
                        }

                        usuarios.Add(usuario);
                    }
                }
            }
            catch (Exception)
            {
                usuarios = null;
            }
            return usuarios;
        }
        internal bool validarPassword(int id, string passActual)
        {
            throw new NotImplementedException();
        }
        
    }
}
