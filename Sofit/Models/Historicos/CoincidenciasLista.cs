﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Historicos
{
    public class CoincidenciasLista
    {
        private string desc = string.Empty;
        public int IdTicket { get; set; }
        public string Texto { get; set; }
        public string Descripcion {
            get
            {
                string result = string.Empty;
                int totalL = desc.Length;
                int starting = desc.ToLower().IndexOf(Texto.ToLower());
                int crindex = desc.IndexOf('\r', starting);
                int maxLength = totalL - 1;
                if (crindex != -1)
                {
                    if (crindex < maxLength) maxLength = crindex;
                }
                int length = starting + 40;                
                int finallength = 0;
                if (length <= maxLength) finallength = 40;
                else finallength = maxLength-starting;
                return desc.Substring(starting,finallength);
            }
            set
            {
                desc = value;
            }
        }
    }
}