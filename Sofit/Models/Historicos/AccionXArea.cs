﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace soporte.Models
{
    public class AccionXArea : IComparable
    {
        // Cambios de estado
        public int TipoAccion { get; set; }
        public string TecnicoNombre { get; set; }
        public string Estado { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        //public Nullable<System.DateTime> FechaFin { get; set; }
        public string FechaIString
        {
            get => FechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
            set { }
        }
        public string FechaFString
        {
            get => FechaFin.ToString("dd/MM/yyyy HH:mm:ss");
            set { }
        }

        // Resoluciones
        [ScriptIgnore]
        public DateTime Fecha { get; set; }
        private string equipo;
        public int IdUsuario { get; set; }
        public string FechaStr
        {
            get
            {
                return Fecha.ToString();
            }
            set
            {

            }
        }
        public string Tecnico { get; set; }
        public string Turno { get; set; }
        public string Observaciones { get; set; }
        public string Equipo
        {
            get
            {
                if (equipo == null) equipo = "N/A";
                return equipo;
            }
            set
            {
                equipo = value;
            }
        }
        public int? IdEquipo { get; set; }
        public string TipoResolucion { get; set; }
        public int idResolucion { get; set; }
        public int idTipoResolucion { get; set; }
        public int idTecnico { get; set; }
        public string PathImagen { get; set; }

        // Compare
        public int CompareTo(object obj)
        {
            AccionXArea other = (AccionXArea)obj;
            if (FechaInicio > other.FechaInicio)
                return 1;
            else if (FechaInicio < other.FechaInicio)
                return -1;
            else return 0;
        }
    }
}