﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class IncidentesActivosBuilder
    {
        private HistoricoIncidencia historico;

        internal void Construir(string nroInc)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var incid = (from u in dbc.Incidencias
                             where u.numero == nroInc 
                             select u).SingleOrDefault();
                if (incid != null)
                {
                    historico = new HistoricoIncidencia
                    {
                        TipoTicket = incid.TipoTicket.nombre,
                        PrioridadTicket = incid.PrioridadTicket.nombre,
                        TiempoServicio = incid.Tipificaciones.Servicios.tiempo_res.Value,
                        TiempoMargen = incid.Tipificaciones.Servicios.tiempo_margen.Value,
                        Servicio = incid.Tipificaciones.Servicios.nombre,
                        Numero = incid.numero,
                        IdIncidencia = incid.id_incidencias,
                        Dependencia = incid.Dependencia != null ? incid.Dependencia.descripcion : "N/A",
                        Jurisdiccion = incid.Dependencia != null ? incid.Dependencia.Jurisdiccion.descripcion : incid.Jurisdiccion.descripcion,
                        Tipificacion = incid.Tipificaciones.nombre,
                        Descripcion = incid.descripcion,
                        FechaInicio = incid.fecha_inicio,
                        Cliente = incid.Contactos != null ? incid.Contactos.nombre : "N/A",
                        EstadoTicket = incid.fecha_fin != null ? "Cerrado" : dbc.IncidenciasActivas.Where(c => c.id_incidencia == incid.id_incidencias).Single().Estado_incidente.descripcion
                    };
                    historico.Areas = new List<MovAreaIncidencia>();
                    var mov = (from u in dbc.AreaXIncidencia
                               where u.id_incidencia == historico.IdIncidencia
                               select u).ToList();
                    foreach (var v in mov)
                    {
                        DateTime fechaHastaParsed = (v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
                        if (v.fecha_fin != null)
                        {
                            bool sinTime = fechaHastaParsed.TimeOfDay.TotalSeconds == 0;
                            if (sinTime)
                            {
                                fechaHastaParsed = fechaHastaParsed.AddHours(23).AddMinutes(59).AddSeconds(59);
                            }
                        }
                        List<ResolucionesIncidencias> resoluciones = (from s in dbc.Resoluciones
                                                                      where s.id_incidencia == historico.IdIncidencia && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
                                                                      //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                      select new ResolucionesIncidencias
                                                                      {
                                                                          Fecha = s.fecha,
                                                                          Equipo = s.UnidadConf.nombre,
                                                                          Observaciones = s.observaciones,
                                                                          Tecnico = s.tecnico.descripcion,
                                                                          TipoResolucion = s.tipo_resolucion.descripcion,
                                                                          Turno = s.turno.descripcion
                                                                      }).ToList();
                        resoluciones.Sort();
                        MovAreaIncidencia mar = new MovAreaIncidencia
                        {
                            Area = AreaIncidenciaFactory.getArea(v.id_area),
                            FechaDesde = v.fecha_inicio,
                            FechaHasta = v.fecha_fin,
                            UsuarioQEnvio = v.Usuarios.nombre_completo,
                            Resoluciones = resoluciones,
                            Direccion = v.area.Direcciones.nombre
                        };
                        mar.Resoluciones.Sort();
                        historico.Areas.Add(mar);
                    };
                    var movH = (from u in dbc.AreaXIncidenciaHistorico
                               where u.id_incidencia == historico.IdIncidencia
                               select u).ToList();
                    foreach (var v in movH)
                    {
                        DateTime fechaHastaParsed = (v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
                        if (v.fecha_fin != null)
                        {
                            bool sinTime = fechaHastaParsed.TimeOfDay.TotalSeconds == 0;
                            if (sinTime)
                            {
                                fechaHastaParsed = fechaHastaParsed.AddHours(23).AddMinutes(59).AddSeconds(59);
                            }
                        }
                        List<ResolucionesIncidencias> resoluciones = (from s in dbc.Resoluciones
                                                                      where s.id_incidencia == historico.IdIncidencia && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
                                                                      //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                      select new ResolucionesIncidencias
                                                                      {
                                                                          Fecha = s.fecha,
                                                                          Equipo = s.UnidadConf.nombre,
                                                                          Observaciones = s.observaciones,
                                                                          Tecnico = s.tecnico.descripcion,
                                                                          TipoResolucion = s.tipo_resolucion.descripcion,
                                                                          Turno = s.turno.descripcion
                                                                      }).ToList();
                        resoluciones.Sort();
                        MovAreaIncidencia mar = new MovAreaIncidencia
                        {
                            Area = AreaIncidenciaFactory.getArea(v.id_area),
                            FechaDesde = v.fecha_inicio,
                            FechaHasta = v.fecha_fin,
                            UsuarioQEnvio = v.Usuarios.nombre_completo,
                            Resoluciones = resoluciones,
                            Direccion = v.area.Direcciones.nombre
                        };
                        mar.Resoluciones.Sort();
                        historico.Areas.Add(mar);
                    };                   
                    
                }
            }
        }
        internal HistoricoIncidencia getResult()
        {
            List<MovAreaIncidencia> newList = new List<MovAreaIncidencia>();
            if (historico != null)
            {
                MovAreaIncidencia previo = null;
                historico.Areas.Sort();
                foreach (MovAreaIncidencia a in historico.Areas)
                {
                    if (previo == null)
                    {
                        previo = a;
                        newList.Add(previo);
                    }
                    else
                    {
                        if (previo.Direccion == a.Direccion)
                        {
                            previo.FechaHasta = a.FechaHasta;
                            previo.Resoluciones.AddRange(a.Resoluciones);

                        }
                        else
                        {
                            previo = a;
                            newList.Add(previo);
                        }
                    }             
                }
                historico.Areas = newList;
            }
            return historico;
        }
    }
}