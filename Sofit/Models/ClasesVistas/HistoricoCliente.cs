﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class HistoricoCliente
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Te { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public string Dependencia { get; set; }
        public string Jurisdiccion { get; set; }
        private List<HistoricoIncidencia> incInternal = new List<HistoricoIncidencia>();
        public List<HistoricoIncidencia> Incidentes
        {
            get
            {
                incInternal.Sort();
                return incInternal;
            }
            set { incInternal = value; }
        }
    }
}