﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class LineaBaseGralVista
    {
        public int idLineaBase { get; set; }
        public List<ElementoLineaBase> Elementos = new List<ElementoLineaBase>();
        public DateTime Fecha
        {
            get;
            set;
        }

        public String FechaStr
        {
            get
            {
                return Fecha.ToShortDateString();
            }
            set
            {
            }
        }
        public string CreadoPor { get; set; }
        public string Nombre { get; set; }
        
    }
}