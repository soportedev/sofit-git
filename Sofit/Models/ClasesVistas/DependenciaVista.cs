﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class DependenciaVista
    {
        public DependenciaVista()
        {
            Jurisdiccion = new JurisdiccionVista();
        }
        public int idDependencia { get; set; }
        public string Nombre { get; set; }
        public JurisdiccionVista Jurisdiccion { get; set; }
    }
}