﻿using soporte.Areas.Soporte.Models.Displays;
using soporte.Models.Displays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class InfoTickets
    {        
        public int idDependencia { get; set; }
        public string Firma { get; set; }
        public string NroTicket { get; set; }
        public int IdTicket { get; set; }
        public List<ResToDisplay> resoluciones { get; set; }
        public List<ReferentesVista> referentes { get; set; }
    }
}