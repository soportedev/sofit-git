﻿using Sofit.DataAccess;
using soporte.Areas.Soporte.Controllers;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas
{
    public class HistoricoEstados
    {
        public string TecnicoNombre { get; set; }
        public string Estado { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string FechaIString
        {
            get => FechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
            set { }
        }
        public string FechaFString
        {
            get => FechaFin.ToString("dd/MM/yyyy HH:mm:ss");
            set { }
        }
    }
}