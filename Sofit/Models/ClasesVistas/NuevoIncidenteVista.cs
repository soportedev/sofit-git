﻿using Sofit.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class NuevoIncidenteVista
    {
        public List<JurisdiccionVista> Jurisdicciones { get; set; }
        public List<TipificacVista> Tipificaciones { get; set; }
        public List<TipifEnServicio> TipificacionesEnServicio { get; set; }
        public List<TipoTicketVista> TipoDeTicket { get; set; }
        public List<PrioridadesTicketVista> Prioridades { get; set; }
    }
}