﻿using soporte.Areas.Soporte.Models;
using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class EquipoSoporteVista
    {
        private string ubicacion;
        public int IdEquipo { get; set; }
        public int IdIncidente { get; set; }
        public string Oblea { get; set; }
        public string NroIncidente { get; set; }
        public DateTime FechaDesde { get; set; }
        public string Dependencia { get; set; }
        public string Tecnico { get; set; }
        public string EstadoIcono { get; set; }
        public string Prioridad { get; set; }
        public string Ubicacion {
            get
            {
                if (ubicacion == null) return "Sin Ubicar";
                else return ubicacion;
            }
            set
            {
                ubicacion = value;
            }
        }
        public string Observacion { get; set; }
        public string Notificacion { get; set; }
        public DateTime FechaNotificacion { get; set; }
        public bool ParaNotificar { get; set; }
        public bool TieneNotificacion { get; set; }
        private int idarea;
        private int idestado;
        public int idEstado
        {
            get
            {
                return this.idestado;
            }
            set
            {
                this.idestado = value;
                Estado = EstadoEquipoFactory.getEstadoPorId(value);
            }
        }
        public int idArea
        {
            get
            {
                return this.idarea;
            }
            set
            {
                this.idarea = value;
                Area = AreaEquipoFactory.getAreaXId(value);
            }
        }
        public AreaEquipo Area { get; set; }
        public EstadoEquipo Estado { get; set; }
    }
}