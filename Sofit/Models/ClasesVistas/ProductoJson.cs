﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ProductoJson
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int idDependencia { get; set; }
        public int stock { get; set; }
        public int idUbicacion { get; set; }
        public string nombreUbicacion { get; set; }
        public string nombreRubro { get; set; }
        public string codigo { get; set; }
        public int idRubro { get; set; }
        public string pathImagen { get; set; }

    }
}