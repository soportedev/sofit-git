﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class EquipoComplete
    {
        public int idEquipo { get; set; }
        public int? idHijo { get; set; }
        public string Oblea { get; set; }
        public string NroSerie { get; set; }
        public IEnumerable<IpVista> Ip { get; set; }
        public string NombreComun { get; set; }
        public string Ip4 { get; set; }
    }
}