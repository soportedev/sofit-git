﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class CambiosPrioridadVista
    {
        public int Id { get; set; }
        public string EstadoPrevio { get; set; }
        public string EstadoNuevo { get; set; }
        public string Usuario { get; set; }
        public string FechaStr {
            get
            {
                return Fecha.ToShortDateString();
            } set { } }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
    }
}