﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TipoResolucion
    {
        public int idTipoResolucion { get; set; }
        public string Nombre { get; set; }
        public override bool Equals(object obj)
        {
            TipoResolucion otro = obj as TipoResolucion;
            return Nombre.Equals(otro.Nombre);
        }
        public override int GetHashCode()
        {
            return Nombre.GetHashCode();
        }
    }
}