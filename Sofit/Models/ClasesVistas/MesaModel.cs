﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class MesaModel
    {        
        public List<IncTeleVista> IncidentesIngresados { get; set; }
        public List<IncTeleVista> IncidentesSuspendidos { get; set; }
        public List<IncTeleVista> IncidentesTotales { get; set; } 
        public List<Area> Areas { get; set; }
        public List<Tecnico> Tecnicos { get; set; }        
        public List<TipoResolucion> TiposResolucion { get; set; }
        public List<TipoResolucion> TiposResolucion2 { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<TipificacVista> Tipificaciones { get; set; }
        public List<Jurisdicciones> Jurisdicciones { get; set; }
        public List<DireccionVista> SubDirecciones { get; set; }
        public List<TipoEquipoVista> TiposEquipo { get; set; }
    }
}