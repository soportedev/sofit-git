﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class EstadoIncidenciaVista
    {
        public int IdEstado { get; set; }
        public string Nombre { get; set; }
    }
}