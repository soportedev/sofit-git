﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class RubrosVisualizer
    {
        public int idRubro { get; set; }
        public string Nombre { get; set; }
        public string NombrePadre { get; set; }
        public int? idPadre { get; set; }
    }
}