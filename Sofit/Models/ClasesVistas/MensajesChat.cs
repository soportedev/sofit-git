﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas
{
    public class MensajesChat:IComparable
    {
        public bool HayMas { get; set; }
        public int Id { get; set; }
        public string Fecha {
            get
            {
                return fechahora.Date.ToShortDateString();
            }
            private set { }
        }
        public string Hora
        {
            get
            {
                return fechahora.ToString("HH:mm:ss");
            }
            private set { }
        }
        public int Id_From { get; set; }
        public string NombreFrom { get; set; }
        public string NombreTo { get; set; }
        public int Id_To { get; set; }
        public string Mensaje { get; set; }
        public string Avatar { get; set; }
        public bool Leido { get; set; }
        public DateTime fechahora = new DateTime();

        public int CompareTo(object obj)
        {
            MensajesChat otro = (MensajesChat)obj;
            if (this.fechahora < otro.fechahora) return -1;
            if (this.fechahora > otro.fechahora) return 1;
            return 0;
        }
    }
}