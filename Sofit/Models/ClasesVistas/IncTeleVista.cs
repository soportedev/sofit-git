﻿using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class IncTeleVista
    {
        public int IdIncidente { get; set; }
        public string Numero { get; set; }
        private DateTime fecha;
        private int idarea;
        private bool tieneNotificacion;
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string Notificacion { get; set; }
        public bool TieneNotificacion
        {
            get { return tieneNotificacion; }
            set
            {
                if (value) Notificacion = "Notificado";
                else Notificacion = "Sin Notificar";
                tieneNotificacion = value;
            }
        }
        public int idArea
        {
            get 
            {
                return idarea;
            }
            set
            {
                idarea = value;
                Area = AreaIncidenciaFactory.getArea(value);
                Direccion = Area.NombreDireccion;
            }
        }
        public DateTime FechaDesde 
        {
            get
            {
                return fecha;
            }
            set
            {
                fecha = value;
            }
        }
        public string Tipificacion { get; set; }
        public string Cliente { get; set; }
        public string Dependencia { get; set; }
        public string Técnico { get; set; }
        public string EstadoIcono { get; set; }
        public string Observacion { get; set; }        
        public string Descripcion { get; set; }
        public bool Imagen { get; set; }
        public string PathImage { get; set; }
        public bool Archivo { get; set; }
        public bool FueSuspendido { get; set; }
        public bool EstuvoEsperando { get; set; }
        public string Subdireccion { get; set; }
        public IAreaIncidencia Area { get; set; }
        public string Servicio { get; set; }
        public TimeSpan TiempServicio { get; set; }
        public string Prioridad { get; set; }
        public int sTolerance { get; set; }
        public TimeSpan TiempoSuspendido { get; set; }
        public TimeSpan TiempoEsperando { get; set; }
        public void calcularEstadoServicio(int tiempocritico)
        {
            if (Numero == "CBA3524476")
            {
                string variable = "";
            }
            DateTime now = DateTime.Now;
            TimeSpan tiempoAviso = new TimeSpan(TiempServicio.Ticks * sTolerance / 100);            
            TimeSpan tiempoExcedidoCritico = new TimeSpan();
            TimeSpan tiempoExcedidoAviso = new TimeSpan();
            TimeSpan tiempoSuscalc = TimeSpan.Zero;
            TimeSpan tiempoEspera = TimeSpan.Zero;
            if (FueSuspendido)
            {                
                using(var dbc=new IncidenciasEntities())
                {
                    var estadossusp = from u in dbc.EstadoXIncidencia
                                      where u.id_incidencia == IdIncidente & u.Estado_incidente.descripcion == "Suspendido"
                                      select u;
                    foreach(var e in estadossusp)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;                        
                        tiempoSuscalc=tiempoSuscalc.Add(BusinessDays.GetBusinessTime(fechainicio,fechafin));
                    }
                }
            }
            if (EstuvoEsperando)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var estadoespera = from u in dbc.EstadoXIncidencia
                                      where u.id_incidencia == IdIncidente & u.Estado_incidente.descripcion == "Esperando Retiro Equipo"
                                      select u;
                    foreach (var e in estadoespera)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;
                        tiempoEspera = tiempoEspera.Add(BusinessDays.GetBusinessTime(fechainicio, fechafin));
                    }
                }
            }
            this.TiempoSuspendido = tiempoSuscalc;
            this.TiempoEsperando = tiempoEspera;
            //tiempo transcurrido
            TimeSpan diff = BusinessDays.GetBusinessTime(this.FechaDesde,now);
            //DateTime fdesde = new DateTime(2017, 8, 4,10,0,0);
            //DateTime fhasta = new DateTime(2017, 8, 7,13,0,0);
            //TimeSpan timeElapsed = BusinessDays.GetBusinessTime(fdesde, fhasta);
            
            if (Prioridad == "Crítico")
            {
                TimeSpan tiempoCritico = new TimeSpan(tiempocritico, 0, 0);
                tiempoAviso = new TimeSpan(tiempoCritico.Ticks * sTolerance / 100);
                tiempoExcedidoCritico = diff - tiempoCritico + TiempoSuspendido + TiempoEsperando;
                tiempoExcedidoAviso = diff - tiempoAviso + TiempoSuspendido + TiempoEsperando;
                this.Observacion = "Este es un Incidente crítico. Han pasado " + diff.Days + " días y " + diff.Hours + " horas desde su generación" +
                    (FueSuspendido ? ", estuvo suspendido " + TiempoSuspendido.Days + " días y " + TiempoSuspendido.Hours + " horas " : "") +
                    (EstuvoEsperando ? ", y estuvo esperando a ser retirado " + TiempoEsperando.Days + " días y " + TiempoEsperando.Hours + " horas " : "");
                if (tiempoExcedidoAviso > TimeSpan.Zero)
                {
                    if (tiempoExcedidoCritico > TimeSpan.Zero)
                    {
                        EstadoIcono = "nook";
                    }
                    else
                    {
                        EstadoIcono = "nok";
                    }
                }
                else
                {
                    EstadoIcono = "ok";
                }
            }
            else
            {
                tiempoAviso = new TimeSpan(TiempServicio.Ticks * sTolerance / 100);                
                tiempoExcedidoCritico = TiempServicio - diff + TiempoSuspendido + TiempoEsperando;
                tiempoExcedidoAviso = tiempoAviso - diff + TiempoSuspendido + TiempoEsperando;

                if (tiempoExcedidoAviso < TimeSpan.Zero)
                {
                    if (tiempoExcedidoCritico < TimeSpan.Zero)
                    {
                        EstadoIcono = "nook";
                        this.Observacion = "El tiempo de resolución indicado en el Servicio " + this.Servicio +
                            " se ha excedido en " + tiempoExcedidoCritico.Duration().Days + " días y " + tiempoExcedidoCritico.Duration().Hours + " horas.";
                    }
                    else
                    {
                        EstadoIcono = "nok";
                        this.Observacion = "En " + tiempoExcedidoCritico.Duration().Days + " días y " + tiempoExcedidoCritico.Duration().Hours +
                            " horas se va a cumplir el plazo especificado" +
                            " en el Servicio " + this.Servicio;
                    }
                }
                else
                {
                    EstadoIcono = "ok";
                    this.Observacion = "Han pasado " + diff.Days + " días";
                }
                this.Observacion += (FueSuspendido ? ", estuvo suspendido " + TiempoSuspendido.Days + " días y " + TiempoSuspendido.Hours + " horas " : "") +
                    (EstuvoEsperando ? ", y estuvo esperando a ser retirado " + TiempoEsperando.Days + " días y " + TiempoEsperando.Hours + " horas " : "");
            }
        }

        internal void calcularEstadoServicioYSuspensión(int tiempocritico, int tiempoSuspension)
        {
            if (Numero == "SOL1703505")
            {
                string variable = "";
            }
            DateTime now = DateTime.Now;
            TimeSpan tiempoAviso = new TimeSpan(TiempServicio.Ticks * sTolerance / 100);
            TimeSpan tiempoMaxSuspension = new TimeSpan(tiempoSuspension, 0, 0);
            TimeSpan tiempoExcedidoCritico = new TimeSpan();
            TimeSpan tiempoExcedidoAviso = new TimeSpan();
            TimeSpan tiempoSuscalc = TimeSpan.Zero;
            TimeSpan tiempoEspera = TimeSpan.Zero;
            if (FueSuspendido)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var estadossusp = from u in dbc.EstadoXIncidencia
                                      where u.id_incidencia == IdIncidente & u.Estado_incidente.descripcion == "Suspendido"
                                      select u;
                    foreach (var e in estadossusp)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;
                        tiempoSuscalc = tiempoSuscalc.Add(fechafin.Subtract(fechainicio));
                    }
                }
            }
            if (EstuvoEsperando)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var estadoespera = from u in dbc.EstadoXIncidencia
                                       where u.id_incidencia == IdIncidente & u.Estado_incidente.descripcion == "Esperando Retiro Equipo"
                                       select u;
                    foreach (var e in estadoespera)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;
                        tiempoEspera = tiempoEspera.Add(fechafin.Subtract(fechainicio));
                    }
                }
            }
            this.TiempoSuspendido = tiempoSuscalc;
            this.TiempoEsperando = tiempoEspera;
            //tiempo transcurrido
            TimeSpan diff = BusinessDays.GetBusinessTime(this.FechaDesde, now);


            if (Prioridad == "Crítico")
            {
                TimeSpan tiempoCritico = new TimeSpan(tiempocritico, 0, 0);
                tiempoAviso = new TimeSpan(tiempoCritico.Ticks * sTolerance / 100);
                tiempoExcedidoCritico = tiempoCritico - diff + TiempoSuspendido + TiempoEsperando;
                tiempoExcedidoAviso = tiempoAviso - diff + TiempoSuspendido + TiempoEsperando;
                this.Observacion = "Este es un Incidente crítico. Han pasado " + diff.Days + " días y " + diff.Hours + " horas desde su generación" +
                    (FueSuspendido ? ", estuvo suspendido " + TiempoSuspendido.Days + " días y " + TiempoSuspendido.Hours + " horas " : "") +
                    (EstuvoEsperando ? ", y estuvo esperando a ser retirado " + TiempoEsperando.Days + " días y " + TiempoEsperando.Hours + " horas " : "");
                if (tiempoExcedidoAviso < TimeSpan.Zero)
                {
                    if (tiempoExcedidoCritico < TimeSpan.Zero)
                    {
                        EstadoIcono = "nook";
                    }
                    else
                    {
                        EstadoIcono = "nok";
                    }
                }
                else
                {
                    EstadoIcono = "ok";
                }
            }
            else
            {
                tiempoAviso = new TimeSpan(TiempServicio.Ticks * sTolerance / 100);
                tiempoExcedidoCritico =TiempServicio - diff + TiempoSuspendido + TiempoEsperando;
                tiempoExcedidoAviso = tiempoAviso - diff + TiempoSuspendido + TiempoEsperando;

                if (tiempoExcedidoAviso < TimeSpan.Zero)
                {
                    if (tiempoExcedidoCritico < TimeSpan.Zero)
                    {
                        EstadoIcono = "nook";
                        this.Observacion = "El tiempo de resolución indicado en el Servicio " + this.Servicio +
                            " se ha excedido en " + tiempoExcedidoCritico.Duration().Days + " días y " + tiempoExcedidoCritico.Duration().Hours + " horas.";
                    }
                    else
                    {
                        EstadoIcono = "nok";
                        this.Observacion = "En " + tiempoExcedidoAviso.Duration().Days + " días y " + tiempoExcedidoAviso.Duration().Hours +
                            " horas se va a cumplir el plazo especificado" +
                            " en el Servicio " + this.Servicio;
                    }
                }
                else
                {
                    EstadoIcono = "ok";
                    this.Observacion = "Han pasado " + diff.Days + " días";
                }
                this.Observacion += (FueSuspendido ? ", estuvo suspendido " + TiempoSuspendido.Days + " días y " + TiempoSuspendido.Hours + " horas " : "") +
                    (EstuvoEsperando ? ", y estuvo esperando a ser retirado " + TiempoEsperando.Days + " días y " + TiempoEsperando.Hours + " horas " : "");
            }
            if (Estado == "Suspendido")
            {
                TimeSpan overSuspension = TiempoSuspendido - tiempoMaxSuspension;                
                if (TiempoSuspendido > tiempoMaxSuspension)
                {
                    EstadoIcono = "nook";
                    this.Observacion += "Se ha excedido el período suspendido en " + overSuspension.Days + " días y " +
                        overSuspension.Hours + " horas.";
                }
            }
        }
    }
}