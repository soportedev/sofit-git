﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ResClonadoVista
    {
        private string observaciones;
        public string NroIncidente { get; set; }
        public DateTime Fecha { get; set; }
        public string Equipo { get; set; }

        public string TipoResolucion { get; set; }
        public string Técnico { get; set; }
        public string Jurisdiccion { get; set; }
        public string Observaciones
        {
            get
            {
                if (observaciones == "") return "N/A";
                return observaciones;
            }
            set
            {
                observaciones = value;
            }
        }
        public string Direccion { get; set; }
        public string ImagenUsada { get; set; }
    }
}