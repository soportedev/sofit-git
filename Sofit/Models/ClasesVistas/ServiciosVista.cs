﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ServiciosVista
    {
        private int horas;
        private int dias;
        private int horasTotales;
        private int horasMargen;
        private int diasMargen;
        private int horasMargenTotales;
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Vigente { get; set; }
        public int Dias
        {
            get
            {
                return dias;
            }
            set
            {
                dias = value;
            }
        }
        public int DiasMargen
        {
            get
            {
                return diasMargen;
            }
            set
            {
                diasMargen = value;
            }
        }
        public int TiempoHoras {
            get
            {
                return horasTotales;
            }
            set
            {
                horasTotales = value;
                dias = TimeSpan.FromHours(value).Days;
                horas = (TimeSpan.FromHours(value) - TimeSpan.FromDays(dias)).Hours;
            }
        }
        public int TiempoMargen
        {
            get
            {
                return horasMargenTotales;
            }
            set
            {
                horasMargenTotales = value;
                diasMargen = TimeSpan.FromHours(value).Days;
                horasMargen = (TimeSpan.FromHours(value) - TimeSpan.FromDays(diasMargen)).Hours;
            }
        }
        public int Horas {
            get
            {
                return horas;
            }
            set
            {

            }
        }
        public int HorasMargen
        {
            get
            {
                return horasMargen;
            }
            set
            {

            }
        }
    }
}