﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class LineaBaseVista
    {
        private string bData;
        public DateTime Fecha
        {
            get;
            set;
        }

        public String FechaStr {
            get
            {
                return Fecha.ToShortDateString();
            }
            set
            {                
            }
        }
        public string CreadoPor { get; set; }
        public string Data {
            get
            {
                return bData;
            }
            set
            {
                bData = value;
            }
        }
    }
}