﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class DatosUbicacion
    {
        private DateTime fecha;        
        public string SubDireccion { get; set; }
        public string Area { get; set; }
        public string Estado { get; set; }
        public string Numero { get; set; }
        public string Tipo { get; set; }
        public string FechaStr { get; set; }
        public string Desc { get; set; }
        public DateTime Fecha
        {
            get 
            { 
                return this.fecha;
            }
            set
            {
                fecha = value;
                FechaStr = value.ToShortDateString();
            }
        }
        public string Dependencia { get; set; }
        public string Jurisdiccion { get; set; }
    }
}