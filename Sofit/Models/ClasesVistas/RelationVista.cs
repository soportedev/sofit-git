﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class RelationVista
    {
        public string TipoEquipo { get; set; }
        public string Oblea { get; set; }
        public string Fecha { get; set; }
        public string Observaciones { get; set; }
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            RelationVista otro = obj as RelationVista;
            return Oblea.Equals(otro.Oblea);
        }
        public override string ToString()
        {
            return this.Oblea;
        }
    }
}