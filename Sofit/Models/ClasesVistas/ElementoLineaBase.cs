﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ElementoLineaBase:IComparable
    {
        public string Nombre { get; set; }
        public int id { get; set; }
        public int orden { get; set; }

        public int CompareTo(object obj)
        {
            ElementoLineaBase other = obj as ElementoLineaBase;
            if (this.orden < other.orden) return -1;
            if (this.orden > other.orden) return -1;
            return 0;
        }
    }
}