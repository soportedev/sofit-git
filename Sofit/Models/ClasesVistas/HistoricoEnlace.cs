﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class HistoricoEnlace
    {
        public int idEnlace { get; set; }
        public string Nombre { get; set; }        
        public string Tipo { get; set; }
        
        private List<HistoricoIncidencia> incInternal = new List<HistoricoIncidencia>();
        public List<HistoricoIncidencia> Incidentes
        {
            get
            {
                incInternal.Sort();
                return incInternal;
            }
            set { incInternal = value; }
        }
    }
}