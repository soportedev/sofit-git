﻿using soporte.Areas.Soporte.Models.Displays;
using Sofit.DataAccess;
using soporte.Models.Displays;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class IncidenteImpresion
    {
        public int id { get; set; }
        public string Numero { get; set; }
        public string Generador { get; set; }
        public DateTime Fecha { get; set; }
        public string FechaStr 
        {
            get
            {
                return Fecha.ToShortDateString();
            }
            set
            {

            }
        }
        public string FechaStrPrevista
        {
            get
            {
                DateTime endDate;
                switch (PrioridadTicket)
                {
                    case "Crítico": endDate = Fecha.AddDays(1);
                        break;
                    case "Urgente": endDate = Fecha.AddDays(2);
                        break;
                    default:
                        endDate = Fecha + new TimeSpan(TiempoServicio, 0, 0) + new TimeSpan(TiempoMargen, 0, 0);

                        break;
                }
                using (var dbc = new IncidenciasEntities())
                {
                    var estadossusp = from u in dbc.EstadoXIncidencia
                                      where u.id_incidencia == id & (u.Estado_incidente.descripcion == "Suspendido" | u.Estado_incidente.descripcion == "Esperando Retiro Equipo")
                                      select u;
                    foreach (var e in estadossusp)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;
                        endDate = endDate.Add(BusinessDays.GetBusinessTime(fechainicio, fechafin));
                    }
                }
                for (DateTime index = Fecha; index < endDate; index = index.AddDays(1))
                {
                    if (index.DayOfWeek == DayOfWeek.Sunday | index.DayOfWeek == DayOfWeek.Saturday)
                    {
                        endDate = endDate.Add(new TimeSpan(1, 0, 0, 0));
                    }
                }
                return endDate.ToShortDateString();
            }
            set { }
        }
        public int TiempoServicio { get; set; }
        public int TiempoMargen { get; set; }
        public string Cliente { get; set; }
        public List<ReferentesVista> Notificados { get; set; }
        public string Jurisdiccion { get; set; }
        public string Dependencia { get; set; }
        public string Tipificacion { get; set; }
        public string Descripcion { get; set; }
        public string TipoTicket { get; set; }
        public string PrioridadTicket { get; set; }
        public string Servicio { get; set; }
        public string imgPath { get; set; }
        public string filePath { get; set; }
        public List<ResToDisplay> resoluciones { get; set; }
    }
}