﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas
{
    public class SwitchListadoExcel:ElementoListadoExcel
    {
        public string Estado { get; set; }
        public string IP { get; set; }

        public string Relaciones
        {
            get
            {
                return string.Join(",", RelacionesLista);
            }
        }       
        public List<RelationVista> RelacionesLista { get; set; }
        public string NombreComun { get; set; }
    }
}