﻿using Sofit.DataAccess;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TicketsQuery
    {
        public int idDireccion { get; set; }
        public int TotalCount { get; set; }
        public int idPrioridad { get; set; }
        public int idTipo { get; set; }
        public int idServicio { get; set; }
        public string Servicio { get; set; }
        public int idJurisdiccion { get; set; }
        public string Jurisdiccion { get; set; }
        private DateTime backFecha;
        private DateTime? backFechaCierre;
        private int hoursoffline;
        private int hoursweekend;
        public int IdTicket { get; set; }
        public string Numero { get; set; }
        public string Tecnico { get; set; }
        public string DireccionGeneracion { get; set; }
        public string TipoTicket { get; set; }
        public string PrioridadTicket { get; set; }
        public int HorasResolucion
        {
            get
            {
                if (PrioridadTicket == "Crítico")
                {
                    return 24;
                }
                if(PrioridadTicket=="Urgente")
                {
                    return 48;
                }
                return horasresolucion;
            }
            set
            {
                horasresolucion = value;
            }
        }
        private int horasresolucion;
        public int HorasMargen {
            get
            {
                if(PrioridadTicket=="Crítico")
                {
                    return 24;
                }
                if(PrioridadTicket=="Urgente")
                {
                    return 48;
                }
                return horasmargen;
            }
            set
            {
                horasmargen = value;
            }
        }
        private int horasmargen;
        public TimeSpan TiempoOffline { get; set; }
        public TimeSpan TiempoWeekend { get; set; }
        public int HoursOffline
        {
            get { return hoursoffline; }
            set
            {
                hoursoffline = value;
                TiempoOffline = new TimeSpan(hoursoffline, 0, 0);
            }
        }
        public int HoursWeekend
        {
            get { return hoursweekend; }
            set
            {
                hoursweekend = value;
                TiempoWeekend = new TimeSpan(hoursweekend, 0, 0);
            }
        }
        public DateTime? FechaCierre 
        {
            get
            {
                return backFechaCierre;
            }
            set 
            {
                backFechaCierre = value;
            }
        }
        public string FechaCierreStr
        {
            get
            {
                if (backFechaCierre.HasValue)
                {
                    return backFechaCierre.ToString();
                }
                else return "Sin Cerrar";
            }
            set
            { }
        }
        public DateTime FechaGeneracion 
        {
            get
            {
                return backFecha;
            }
            set
            {
                backFecha = value;
            }
        }
        public int FechaDia
        {
            get
            {
                return backFecha.Day;
            }
            set { }
        }
        public int FechaMes 
        {
            get
            {
                return backFecha.Month;
            }
            set { }
        }
        public int FechaAnio {
            get
            {
                return backFecha.Year;
            }
            set { }
        }      
        public DateTime FechaResolucion {
            get
            {
                if (Numero == "SOL1826105")
                {
                    Debug.Write("Found");
                }
                DateTime fechaFinal;
                if (PrioridadTicket == "Crítico")
                {
                    fechaFinal = FechaGeneracion.AddDays(1).Add(TiempoOffline);
                }
                else
                {
                    if (PrioridadTicket == "Urgente")
                    {
                        fechaFinal = FechaGeneracion.AddDays(2).Add(TiempoOffline);
                    }
                    else
                    {
                        fechaFinal = FechaGeneracion.AddHours(HorasResolucion).Add(TiempoOffline);
                    }
                }
                //fechaFinal = FechaGeneracion.AddHours(HorasResolucion).Add(TiempoOffline);
                DateTime fechaHastaWeekends = fechaFinal.Add(-TiempoOffline);
                for (DateTime index = FechaGeneracion; index < fechaHastaWeekends; index = index.AddDays(1))
                {
                    if (index.DayOfWeek == DayOfWeek.Sunday | index.DayOfWeek == DayOfWeek.Saturday)
                    {
                        fechaFinal = fechaFinal.Add(new TimeSpan(1, 0, 0, 0));
                    }
                }
                return fechaFinal;
            }
            set { }
        }
        public DateTime FechaResolucionMargen {
            get
            {
                DateTime fechaFinal;
                if (PrioridadTicket == "Crítico")
                {
                    fechaFinal = FechaResolucion.AddDays(1);
                }
                else
                {
                    if (PrioridadTicket == "Urgente")
                    {
                        fechaFinal = FechaResolucion.AddDays(2);
                    }
                    else
                    {
                        fechaFinal = FechaResolucion.AddHours(HorasMargen);
                    }
                }
                for (DateTime index = FechaResolucion; index < fechaFinal; index = index.AddDays(1))
                {
                    if (index.DayOfWeek == DayOfWeek.Sunday | index.DayOfWeek == DayOfWeek.Saturday)
                    {
                        fechaFinal = fechaFinal.Add(new TimeSpan(1, 0, 0, 0));
                    }
                }
                return fechaFinal;
            }
            set
            {

            }
        }
        public string ResultadoTiempo 
        {
            get
            {
                if (Numero == "INC1907385")
                {
                    string dummy = "Encontrado";
                    Debug.Write("Encontrado");
                }
                DateTime fechaFinal;//fecha cierre ticket
                TimeSpan duracionTicketHoras = new TimeSpan(0, 0, 0);
                TimeSpan duracionOff = new TimeSpan(HoursOffline, 0, 0);
                TimeSpan duracionweekend = new TimeSpan(HoursWeekend, 0, 0);
                if (!FechaCierre.HasValue)
                {
                    fechaFinal = DateTime.Now;
                }
                else
                {
                    fechaFinal = FechaCierre.Value;
                }
                    duracionTicketHoras = fechaFinal - FechaGeneracion;
                    duracionTicketHoras = duracionTicketHoras.Subtract(duracionOff).Subtract(duracionweekend);
                    if (duracionTicketHoras.TotalHours < HorasResolucion)
                    {
                        return "Si";
                    }
                    else
                    {
                        if (duracionTicketHoras.TotalHours > HorasResolucion & duracionTicketHoras.TotalHours < (HorasResolucion + HorasMargen))
                        {
                            return "Margen";
                        }
                        else
                        {
                            return "Fuera";
                        }
                    }             
                
            }            
        }
        public bool DentroDelTiempo
        {
            get
            {
                if (Numero == "SOL1708892")
                {
                    //string dummy = "";
                }
                return ResultadoTiempo == "Si";
            }
            
        }
        public bool DentroDelMargen 
        {
            get
            {
                if (Numero == "SOL1708892")
                {
                    //string dummy = "";
                }
                return ResultadoTiempo == "Margen";                
            }            
        }
        public bool FueraDelMargen {
            get
            {
                return ResultadoTiempo == "Fuera";
            }        
        }
        public bool SinCerrar
        {
            get
            {
                return !FechaCierre.HasValue;
            }
        }
    }
}