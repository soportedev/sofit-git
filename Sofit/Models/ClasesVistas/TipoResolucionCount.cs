﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TipoResolucionCount
    {
        public string TipoResolucion { get; set; }
        public int Cantidad { get; set; }

    }
}