﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class HistoricoEquipo
    {
        public int idEquipo { get; set; }
        public string Oblea { get; set; }
        public string PasswordAdmin { get; set; }
        public string PasswordBios { get; set; }
        public string Tipo { get; set; }
        private List<HistoricoIncidencia> incInternal = new List<HistoricoIncidencia>();
        public List<HistoricoIncidencia> Incidentes
        {
            get
            {
                incInternal.Sort();
                return incInternal;
            }
            set { incInternal = value; }
        }
    }
}