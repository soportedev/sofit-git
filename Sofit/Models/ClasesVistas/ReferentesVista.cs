﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Models.Displays
{
    public class ReferentesVista
    {
        public ReferentesVista()
        {
            location = new List<LocationVista>();
        }
        public int id { get; set; }
        public string nombre { get; set; }
        public string displayname { get; set; }
        public string email { get; set; }
        public string te { get; set; }
        public string Direccion { get; set; }
        public string dni { get; set; }
        public string Dominio { get; set; }
        public List<LocationVista> location { get; set; }
        public List<JurisdiccionVista> Jurisdicciones { get; set; }
        public List<DependenciaVista> Dependencias { get; set; }
        public override bool Equals(object obj)
        {
            ReferentesVista other = obj as ReferentesVista;
            return this.id == other.id;
        }
        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }
    }
}
