﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ModelHistoricos
    {
        public List<Tecnico> Tecnicos { get; set; }
        public List<Area> Areas { get; set; }
        public List<DireccionVista> Direcciones { get; set; }
        public List<TipoResolucion> ResolucionesClonado { get; set; }
        public List<TipoResolucion> ResolucionesLaboratorio { get; set; }
        public List<TipoResolucion> ResolucionesAtUsuarios { get; set; }
        public List<TipoResolucion> ResolucionesLogística { get; set; }
        public List<TipoResolucion> Resoluciones { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<Jurisdicciones> Jurisdicciones { get; set; }

        public List<TipoTicketVista> TiposTicket { get; set; }

        public List<PrioridadesTicketVista> PrioridadesTicket { get; set; }
        public List<ServiciosVista> Servicios { get; set; }
        public DireccionVista DireccionActual { get; set; }
    }
}