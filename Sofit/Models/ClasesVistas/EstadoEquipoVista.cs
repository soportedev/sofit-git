﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class EstadoEquipoVista
    {
        public int idEstado { get; set; }
        public string Nombre { get; set; }
        public int idDireccion { get; set; }
        public string NombreDireccion { get; set; }
    }
}