﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas
{
    public class ElementoListadoExcel
    {
        public string Nombre { get; set; }
        public string Jurisdiccion { get; set; }
        public string TipoEquipo { get; set; }
        public string Observaciones { get; set; }
    }
}