﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class JurisdiccionesCount
    {
        public string Jurisdiccion { get; set; }
        public int Cantidad { get; set; }
    }
}