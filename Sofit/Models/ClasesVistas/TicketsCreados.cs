﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TicketsCreados:IComparable
    {
        public string Servicio { get; set; }
        private int dentrodeltiempo { get; set; }
        private int dentrodelmargen { get; set; }
        private int fueradelmargen { get; set; }
        private int sincerrar { get; set; }
        public int Cantidad { get; set; }
        public int CantidadEspecifica { get; set; }
        public int CantidadJurisdiccion { get; set; }
        public string Mes { get; set; }
        public string Anio { get; set; }
        public string PorcObtenido
        {
            get
            {
                double p = (double)DentroDelTiempo / CantidadEspecifica * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string Jurisdiccion { get; set; }
        public string Objetivo {
            get
            {
                switch (PrioridadTicket)
                {
                    case "Normal": return "70%";                        
                    case "Urgente": return "80%";
                    case "Crítico": return "90%";
                    default: return "N/A";
                }
            }
            set { }
        }
        public int DentroDelTiempo {
            get
            {
                return dentrodeltiempo;
            }
            set { dentrodeltiempo = value; }
        }
        public int DentroDelMargen {
            get
            {
                return dentrodelmargen;
            }
            set { dentrodelmargen = value; }
        }
        public int FueraMargen {
            get
            {
                return fueradelmargen;
            }
            set { fueradelmargen = value; }
        }
        public int SinCerrar {
            get 
            {
                return sincerrar;
            }
            set {
                sincerrar = value;
            }
        }
        public string DentroDelTiempoPorc {
            get
            {
                double p = (double)DentroDelTiempo / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string PorcentajeEspecifico 
        {
            get
            {
                double p = (double) CantidadEspecifica / CantidadJurisdiccion * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string PorcentajeSobreTotal
        {
            get
            {
                double p = (double)CantidadEspecifica / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string DentroDelMargenPorc {
            get
            {
                double p = (double)DentroDelMargen / Cantidad * 100;
                
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string FueraDelMargenPorc {
            get
            {
                double p = (double)FueraMargen / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string SinCerrarPorc {
            get
            {
                double p = (double)SinCerrar / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }        
        public string TipoTicket { get; set; }
        public string PrioridadTicket { get; set; }
        public string SubDirGeneradora { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string FechaStr {
            get
            {
                return FechaCreacion.ToShortDateString();
            }
            set { }
        }


        public int CompareTo(object obj)
        {
            TicketsCreados other = (TicketsCreados)obj;
            if (FechaCreacion > other.FechaCreacion)
                return 1;
            else if (FechaCreacion < other.FechaCreacion)
                return -1;
            else {
                if (Cantidad > other.Cantidad) return -1;
                else
                {
                    if (Cantidad < other.Cantidad) return 1;
                    else return 0;
                }
            };
        }
    }
}