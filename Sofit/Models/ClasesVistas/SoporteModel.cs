﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class SoporteModel
    {
        public List<Ubicacion> Ubicaciones { get; set; }
        public List<IncTeleVista> IncidentesIngresados { get; set; }
        public List<IncTeleVista> IncidentesAtUsuarios { get; set; }
        public List<IncTeleVista> IncidentesLogistica { get; set; }
        public List<EquipoSoporteVista> EquiposClonado { get; set; }
        public List<EquipoSoporteVista> EquiposLaboratorio { get; set; }
        public List<EquipoSoporteVista> EquiposSuspendidos { get; set; }
        public List<EquipoSoporteVista> EquiposParaEntregar { get; set; }
        public List<EquipoSoporteVista>EquiposAvisoGarantia  { get; set; }
        public List<EquipoSoporteVista> EquiposEnGarantia { get; set; }
        public List<Area> Areas { get; set; }
        public List<Tecnico> Tecnicos { get; set; }
        public List<Tecnico> TecnicoAtUsuarios { get; set; }
        public List<Tecnico> TecnicoLogistica { get; set; }
        public List<TipoResolucion> TiposResolucionClonado { get; set; }
        public List<TipoResolucion> TiposResolucionLaboratorio { get; set; }
        public List<TipoResolucion> TiposResolucionAtUs { get; set; }
        public List<TipoResolucion> TiposResolucionLogistica { get; set; }
        public List<TipoResolucion> TiposResolucionDepósito { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<TipificacVista> Tipificaciones { get; set; }
        public List<Jurisdicciones> Jurisdicciones { get; set; }
        public List<DireccionVista> SubDirecciones { get; set; }
        public List<ImagenesClonadoVista> Imagenes { get; set; }
        public List<PrioridadesTicketVista> PrioridadTickets { get; set; }
    }
}