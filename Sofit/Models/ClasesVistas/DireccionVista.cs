﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class DireccionVista
    {
        public int idDireccion { get; set; }
        public string Nombre { get; set; }
    }
}