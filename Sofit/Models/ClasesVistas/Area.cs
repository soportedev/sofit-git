﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class Area
    {
        public int idArea { get; set; }
        public int idDireccion { get; set; }
        public string Nombre { get; set; }
        public string NombreDirecion { get; set; }
    }
}