﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public abstract class ResolucionesVista
    {
        public abstract string getResolucionesParametros(HistoricosParametros par);
        public abstract string getResolucionesTotalesTecnico(HistoricosParametros par);
        public abstract string getResolucionesTotalesJurisdiccion(HistoricosParametros par);
        public abstract string getResolucionesTotalesTrabajo(HistoricosParametros par);
        public virtual string toHtmlTotalesJurisdiccion(IEnumerable<JurisdiccionesCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Jurisdicción</th><th>Total</th></tr>");
                foreach (JurisdiccionesCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.Jurisdiccion + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        public virtual string toHtmlTotalesTecnico(IEnumerable<TecnicosCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Técnico</th><th>Tipo de Trabajo</th><th>Total</th></tr>");
                foreach (TecnicosCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.Nombre + "</td>");
                    sb.Append("<td>" + j.TipoTrabajo + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        public virtual string toHtmlTotalesTrabajo(IEnumerable<TipoResolucionCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Tipo de Trabajo</th><th>Total</th></tr>");
                foreach (TipoResolucionCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.TipoResolucion + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        public virtual string toHtmlTable(List<ResolucionesIncidentesVista> resoluciones)
        {

            StringBuilder sb = new StringBuilder();
            if (resoluciones.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Fecha</th><th>Incidente</th><th>Oblea</th><th>Tipo Resolución</th>");
                sb.Append("<th>Técnico</th><th>Jurisdicción</th><th>Notas</th></tr>");
                foreach (ResolucionesIncidentesVista r in resoluciones)
                {
                    sb.Append("<tr>");

                    DateTime sDate = r.Fecha;
                    sb.Append("<td>" + sDate.ToShortDateString() + "</td>");
                    sb.Append("<td class='verIncidente'>" + r.NroIncidente + "</td>");
                    sb.Append("<td>" + r.Equipo + "</td>");
                    sb.Append("<td>" + r.TipoResolucion + "</td>");
                    sb.Append("<td>" + r.Técnico + "</td>");
                    sb.Append("<td>" + r.Jurisdiccion + "</td>");
                    sb.Append("<td><a href='JavaScript:void(0)' data-res='" + r.Observaciones + "' class='verInfo'>Ver</a></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();

        }
        public virtual string ErrorCantidadExcesiva()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("La cantidad de registros es muy grande para poder mostrarlos. Introduzca un rango de fechas que reduzca la cantidad.!!");
            
            return sb.ToString();
        }
    }
}