﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class IpVista
    {
        public int Id { get; set; }
        public string IpV4 { get; set; }
        public string IpV6 { get; set; }
        public string Dns { get; set; }
        public string Wins { get; set; }
        public string Mascara { get; set; }
        public string Gateway { get; set; }
        public string Mac { get; set; }
        public string IpLoopback { get; set; }
        public string IpWan { get; set; }
        public string IpGestion { get; set; }
    }
}