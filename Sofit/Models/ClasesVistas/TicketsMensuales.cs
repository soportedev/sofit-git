﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TicketsMensuales
    {
        private string tipot, prioridadt, serviciot, jurisdicciont;
        public TicketsMensuales(){
            tipot="Sin Especificar el tipo";
            prioridadt="Sin Especificar prioridad";
            serviciot = "Sin Especificar el servicio";
            jurisdicciont="Sin Especificar la jurisdicción";
        }
        private int dentrodeltiempo { get; set; }
        private int dentrodelmargen { get; set; }
        private int fueradelmargen { get; set; }
        private int sincerrar { get; set; }
        public int Cantidad { get; set; }
        public int TotalGenerados { get; set; }        
        public int TotalesFiltrados { get; set; }
        public string Jurisd { get; set; }
        public int DentroDelTiempo
        {
            get
            {
                return dentrodeltiempo;
            }
            set { dentrodeltiempo = value; }
        }
        public int DentroDelMargen
        {
            get
            {
                return dentrodelmargen;
            }
            set { dentrodelmargen = value; }
        }
        public int FueraMargen
        {
            get
            {
                return fueradelmargen;
            }
            set { fueradelmargen = value; }
        }
        public int SinCerrar
        {
            get
            {
                return sincerrar;
            }
            set
            {
                sincerrar = value;
            }
        }
        public string DentroDelTiempoPorc
        {
            get
            {
                double p = (double)DentroDelTiempo / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string DentroDelMargenPorc
        {
            get
            {
                double p = (double)DentroDelMargen / Cantidad * 100;

                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string FueraDelMargenPorc
        {
            get
            {
                double p = (double)FueraMargen / Cantidad * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string PorcSobreTotal
        {
            get
            {
                double p = (double)TotalesFiltrados / TotalGenerados  * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string PorcObtenido
        {
            get
            {
                double p = (double) DentroDelTiempo / TotalesFiltrados * 100;
                return Math.Round(p, 2).ToString();
            }
            set { }
        }
        public string TipoTicket { 
            get 
            {
                return tipot;
            }
            set
            {
                tipot = value;
            }
        }
        public string PrioridadTicket {
            get
            {
                return prioridadt;
            }
            set
            {
                prioridadt = value;
            }
        }
        public string Mes { get; set; }
        public string Anio { get; set; }
        public string Jurisdiccion 
        {
            get
            {
                return jurisdicciont;
            }
            set
            {
                jurisdicciont = value;
            }
        }
        public string Servicio {
            get
            {
                return serviciot;
            }
            set
            {
                serviciot = value;
            }
        }
    }
}