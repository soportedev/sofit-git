﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class UbicacionIncidente
    {
        public string Area { get; set; }
        public string Estado { get; set; }
        public string NroIncidente { get; set; }
        public string Tipo { get; set; }
    }
}