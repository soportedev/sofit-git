﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Models.Displays
{
    public class LocationVista
    {
        public int id { get; set; }
        public string tipo { get; set; }
        public string nombre { get; set; }
    }
}
