﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ResolucionesIncidentesVista:IComparable
    {
        private string equipo;
        private string uc;
        private string observaciones;
        public DateTime Fecha { get; set; }
        public string FechaStr
        {
            get
            {
                return Fecha.ToShortDateString();
            }
            set { }
        }
        public string Equipo
        {
            get
            {
                if (equipo == null) return "N/A";
                else return equipo;
            }
            set
            {
                equipo = value;
            }
        }
        public string Uc
        {
            get
            {
                if (uc == null) return "N/A";
                else return uc;
            }
            set
            {
                uc = value;
            }
        }
        public string TipoResolucion { get; set; }
        public string Técnico { get; set; }
        public string Observaciones
        {
            get
            {
                if (observaciones == "") return "N/A";
                return observaciones;
            }
            set
            {
                observaciones = value;
            }
        }
        public string Direccion { get; set; }
        public string NroIncidente { get; set; }
        public string Jurisdiccion { get; set; }
        public string PathImagen { get; set; }
        public int CompareTo(object obj)
        {
            ResolucionesIncidentesVista other = obj as ResolucionesIncidentesVista;
            return other.Fecha.CompareTo(this.Fecha);
        }
        public string ImagenClonadoUsada { get; set; }
    }
}