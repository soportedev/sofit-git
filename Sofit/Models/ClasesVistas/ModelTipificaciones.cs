﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ModelTipificaciones
    {
        public List<TipificacVista> Tipificaciones { get; set; }
        public List<ServiciosVista> Servicios { get; set; }
    }
}