﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TipificacVista:IComparable
    {
        public int? idPadre { get; set; }
        public string Padre { get; set; }
        public int idTipificacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int idDireccion { get; set; }
        public List<TipificacVista> Hijos { get; set; }
        public string Servicio { get; set; }
        public int idServicio { get; set; }
        public bool Agregado { get; set; }
        public TipificacVista()
        {
            this.Hijos = new List<TipificacVista>();
        }
        public TipificacVista(Tipificaciones tb)
        {
            this.idTipificacion = tb.id_tipificacion;
            this.Nombre = tb.nombre;
            this.Descripcion = tb.descripcion;
            this.idDireccion = tb.id_direccion;
            this.idPadre = tb.id_padre;
            this.Servicio = tb.Servicios!=null?tb.Servicios.nombre:"N/A";
            this.Hijos = new List<TipificacVista>();
        }
        public bool tieneHijos
        {
            get
            {               
                return Hijos.Count > 0;
            }
            set { }
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            TipificacVista otro = obj as TipificacVista;
            if ((System.Object)obj == null)
            {
                return false;
            }
            return idTipificacion == otro.idTipificacion;
        }
        public override int GetHashCode()
        {
            return this.idTipificacion.GetHashCode();
        }
        public override string ToString()
        {
            return this.Nombre;
        }

        public int CompareTo(object obj)
        {
            TipificacVista otro = obj as TipificacVista;
            return this.idTipificacion.CompareTo(otro.idTipificacion);
        }
    }
}