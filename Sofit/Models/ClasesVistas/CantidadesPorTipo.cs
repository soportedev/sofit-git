﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class CantidadesPorTipo
    {
        public int idTipo { get; set; }
        public string TipoElem { get; set; }
        public int Cantidad { get; set; }
        public string Imagen { get; set; }
    }
}