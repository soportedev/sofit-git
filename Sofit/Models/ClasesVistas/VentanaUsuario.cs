﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class VentanaUsuario
    {
        public int width { get; set; }
        public int height { get; set; }
        public string id { get; set; }
        public bool visible { get; set; }
    }
}