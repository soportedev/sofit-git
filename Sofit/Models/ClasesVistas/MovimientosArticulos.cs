﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class MovimientosArticulos:IComparable
    {
        private string observ;
        public DateTime Fecha { get; set; }
        public string FechaStr { get {
            return Fecha.ToString();
        }
            set { }
        }
        public string Tipo { get; set; }
        public int Cantidad { get; set; }
        public string Observaciones { get {
            if (observ != null) return observ;
            else return "N/A";
        }
            set {
                this.observ = value;
            }
        }
        public string Usuario { get; set; }

        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            MovimientosArticulos other = (MovimientosArticulos)obj;
            if (Fecha > other.Fecha)
                return 1;
            else if (Fecha < other.Fecha)
                return -1;
            else return 0;
        }
        #endregion
    }
}