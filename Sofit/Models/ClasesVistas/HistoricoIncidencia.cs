﻿using Sofit.DataAccess;
using soporte.Areas.Soporte.Controllers;
using soporte.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class HistoricoIncidencia:IComparable
    {
        public List<CambiosPrioridadVista> CambiosPrioridad { get; set; }
        public List<NotifDisplay> Notificaciones { get; set; }
        public DateTime fechaprevista { get; set; }
        public string EstadoTicket { get; set; }
        public string FechaStrPrevista {
            get
            {
                DateTime endDate;               
                switch (PrioridadTicket)
                {
                    case "Crítico": endDate = FechaInicio.AddDays(1);
                        break;
                    case "Urgente": endDate = FechaInicio.AddDays(2);
                        break;
                    default:
                        endDate = FechaInicio + new TimeSpan(TiempoServicio, 0, 0) + new TimeSpan(TiempoMargen, 0, 0);
                        
                        break;
                }
                using (var dbc = new IncidenciasEntities())
                {
                    var estadossusp = from u in dbc.EstadoXIncidencia
                                      where u.id_incidencia == IdIncidencia & (u.Estado_incidente.descripcion == "Suspendido" | u.Estado_incidente.descripcion == "Esperando Retiro Equipo")
                                      select u;
                    foreach (var e in estadossusp)
                    {
                        DateTime fechainicio = e.fecha_inicio;
                        DateTime fechafin = e.fecha_fin.HasValue ? e.fecha_fin.Value : DateTime.Now;
                        endDate = endDate.Add(BusinessDays.GetBusinessTime(fechainicio, fechafin));
                    }
                }
                for (DateTime index = FechaInicio; index < endDate; index = index.AddDays(1))
                {
                    if (index.DayOfWeek == DayOfWeek.Sunday | index.DayOfWeek == DayOfWeek.Saturday)
                    {
                        endDate = endDate.Add(new TimeSpan(1, 0, 0, 0));
                    }
                }
                return endDate.ToShortDateString();
            }
            set { }
        }
        public int TiempoServicio { get; set; }
        public int TiempoMargen { get; set; }
        public string TipoTicket { get; set; }
        public string PrioridadTicket { get; set; }
        public int IdIncidencia { get; set; }
        public string Numero { get; set; }
        public string Tipificacion { get; set; }
        public string Jurisdiccion { get; set; }
        public string Dependencia { get; set; }
        public List<MovAreaIncidencia> Areas;
        public DateTime FechaInicio { get; set; }
        public DateTime ? FechaFin { get; set; }
        public string Cliente { get; set; }
        public string Descripcion { get; set; }
        public string pathImg { get; set; }
        public string pathFile { get; set; }
        public string Servicio { get; set; }
        public string FechaInicioString
        {
            get
            {
                return FechaInicio.ToShortDateString();
            }
            set { }
        }
        public string FechaFinString
        {
            get
            {
                if (FechaFin != null) return FechaFin.Value.ToShortDateString();
                else return "N/A";
            }
            set { }
        }
        public int CompareTo(object obj)
        {
            HistoricoIncidencia other = obj as HistoricoIncidencia;
            if (this.FechaInicio < other.FechaInicio) return 1;
            if (this.FechaInicio > other.FechaInicio) return -1;
            return 0;            
        }
        public override bool Equals(object obj)
        {
            HistoricoIncidencia other=obj as HistoricoIncidencia;
            return this.Numero.Equals(other.Numero);
        }
        public override int GetHashCode()
        {
            return FechaInicio.GetHashCode();
        }
    }
}