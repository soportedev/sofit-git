﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TecnicosCount
    {
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public string TipoTrabajo { get; set; }
    }
}