﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TicketsMensualJurisdiccion
    {
        public int CantidadGenerada { get; set; }
        public string Mes { get; set; }
        public string Año { get; set; }
        public string Jurisdiccion { get; set; }
        public List<TicketsCreados> Prioridad { get; set; }
        public List<TicketsCreados> Servicio { get; set; }
        public List<TicketsCreados> Tipo { get; set; }
    }
}