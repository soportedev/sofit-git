﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class RolUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public override bool Equals(object obj)
        {
            RolUsuario otro = obj as RolUsuario;
            return Nombre.Equals(otro.Nombre);
        }
    }
}