﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class JurisdiccionVista
    {
        public int idJurisdiccion { get; set; }
        public string Nombre { get; set; }
    }
}