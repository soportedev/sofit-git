﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas.cmdb
{
    public class GCambio
    {
        public int Id { get; set; }
        public DateTime FechaDt { get; set; }
        public string Fecha { get; set; }
    }
}