﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.ClasesVistas.cmdb
{
    public class EquipoEnRelacion
    {
        public int Id { get; set; }
        public string Oblea { get; set; }
        public string TipoEquipo { get; set; }
    }
}