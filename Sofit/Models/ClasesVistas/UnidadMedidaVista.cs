﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class UnidadMedidaVista
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}