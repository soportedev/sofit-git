﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class HistoricosParametros
    {
        public int anioReporte { get; set; }
        public int mesReporte { get; set; }
        public int anioDesde { get
            {
                return FechaDesde.Year;
            }
            set { } }
        public int anioHasta { get
            {
                return FechaHasta.Year;
            }
            set { } }
        public int mesDesde { get { return FechaDesde.Month; } set { } }
        public int mesHasta { get { return FechaHasta.Month; } set { } }
        public int diaDesde { get { return FechaDesde.Day; } set { } }
        public int diaHasta { get { return FechaHasta.Day; } set { } }
        public int idArea { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string oblea { get; set; }
        public string nroIncidente { get; set; }
        public int idJurisdiccion { get; set; }
        public int idDependencia { get; set; }
        public int idTecnico { get; set; }
        public int idServicio { get; set; }
        public int idTipoResolucion { get; set; }
        public int idTipoTicket { get; set; }
        public int idPrioridadTicket { get; set; }
        public bool xTecnico { get; set; }
        public bool xJurisdiccion { get; set; }
        public bool xTrabajo { get; set; }
        public bool xDireccion { get; set; }
        public bool xJurisdMensual { get; set; }
        public bool xMensualTotal { get; set; }
        public bool Anual { get; set; }
        public string TipoReport { get; set; }
        public bool DetalleSubdir { get; set; }
        public bool OfT { get; set; }
    }
}