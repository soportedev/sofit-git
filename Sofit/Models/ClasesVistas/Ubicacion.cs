﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class Ubicacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public Ubicacion()
        {
            this.Id = -1;
            this.Nombre = "Sin Ubicar";
            this.Descripcion = "N/A";
        }
    }
}