﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class TipifEnServicio
    {
        public string Servicio { get; set; }
        public List<TipificacVista>Tipificaciones { get; set; }
    }
}