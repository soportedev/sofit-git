﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;
using System.Globalization;
using soporte.Controllers;
using Sofit.DataAccess;

namespace soporte.Models
{
    public class ReporteMensual : Reporte
    {
        public override string ObtenerReporte(HistoricosParametros param)
        {
            string result = string.Empty;
            if (param.idJurisdiccion == 0)
            {
                result = Mensual(param);
            }
            else
            {
                result = MensualJur(param);
            }
            return result;
        }

        private string MensualJur(HistoricosParametros par)
        {
            List<TicketsQuery> tickets, ticketsFiltrados = null;
            List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

            using (var dbc = new IncidenciasEntities())
            {
                tickets = (dbc.sp_listado_tickets(new DateTime(par.anioReporte, par.mesReporte, 1, 0, 0, 0),
                    new DateTime(par.anioReporte, par.mesReporte, DateTime.DaysInMonth(par.anioReporte, par.mesReporte), 23, 59, 59),
                    0, 0, 0, 0, 0).
                    Select(c =>
                           new TicketsQuery
                           {
                               DireccionGeneracion = c.Direccion,
                               TipoTicket = c.TipoTicket,
                               Numero = c.Numero,
                               IdTicket = c.IdTicket.Value,
                               FechaGeneracion = c.FechaGeneracion.Value,
                               HoursOffline = c.TiempoSuspendido.Value,
                               HoursWeekend = c.TiempoWeekend.Value,
                               PrioridadTicket = c.PrioridadTicket,
                               Jurisdiccion = c.Jurisdiccion,
                               HorasResolucion = c.HorasResolucion.Value,
                               HorasMargen = c.HorasMargen.Value,
                               FechaCierre = c.FechaCierrre,
                               idJurisdiccion = c.IdJurisdiccion.Value,
                               idPrioridad = c.IdPrioridad.Value,
                               idTipo = c.IdTipo.Value,
                               idServicio = c.IdServicio.Value
                           })).ToList();
            }
            ticketsFiltrados = tickets.Where(c => c.idJurisdiccion == par.idJurisdiccion & (par.idTipoTicket == 0 | c.idTipo == par.idTipoTicket)).ToList();
            if (ticketsFiltrados.Count > 0)
            {
                ticketsCreados = (from u in ticketsFiltrados
                                  where (par.idServicio == 0 | par.idServicio == u.idServicio)
                                  group u by new
                                  {
                                      u.PrioridadTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      Jurisdiccion = ticketsFiltrados.First().Jurisdiccion,
                                      Mes = new DateTime(par.anioReporte, par.mesReporte, 1).ToString("MMMM", new CultureInfo("es-AR")),
                                      Anio = new DateTime(par.anioReporte, par.mesReporte, 1).ToString("yyyy", new CultureInfo("es-AR")),
                                      PrioridadTicket = g.Key.PrioridadTicket,
                                      Cantidad = tickets.Count(),
                                      CantidadJurisdiccion = ticketsFiltrados.Count(),
                                      CantidadEspecifica = g.Count(),
                                      DentroDelTiempo = ticketsFiltrados.Where(c => c.DentroDelTiempo & c.PrioridadTicket == g.Key.PrioridadTicket & (par.idServicio == 0 | par.idServicio == c.idServicio)).Count(),
                                      DentroDelMargen = ticketsFiltrados.Where(c => c.DentroDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & (par.idServicio == 0 | par.idServicio == c.idServicio)).Count(),
                                      FueraMargen = ticketsFiltrados.Where(c => c.FueraDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & (par.idServicio == 0 | par.idServicio == c.idServicio)).Count(),
                                      SinCerrar = ticketsFiltrados.Where(c => c.SinCerrar & c.PrioridadTicket == g.Key.PrioridadTicket & (par.idServicio == 0 | par.idServicio == c.idServicio)).Count()
                                  }).ToList();
            }
            return HtmlFormater.toHtmlInformeMensualXJur(ticketsCreados);
        }

        private string Mensual(HistoricosParametros par)
        {
            List<TicketsQuery> tickets = null;
            using (var dbc = new IncidenciasEntities())
            {
                tickets = (dbc.sp_listado_tickets(new DateTime(par.anioReporte, par.mesReporte, 1, 0, 0, 0),
                    new DateTime(par.anioReporte, par.mesReporte, DateTime.DaysInMonth(par.anioReporte, par.mesReporte), 23, 59, 59),
                    0, 0, 0, 0, 0).
                    Select(c =>
                           new TicketsQuery
                           {
                               DireccionGeneracion = c.Direccion,
                               TipoTicket = c.TipoTicket,
                               Numero = c.Numero,
                               IdTicket = c.IdTicket.Value,
                               FechaGeneracion = c.FechaGeneracion.Value,
                               HoursOffline = c.TiempoSuspendido.Value,
                               HoursWeekend = c.TiempoWeekend.Value,
                               PrioridadTicket = c.PrioridadTicket,
                               Jurisdiccion = c.Jurisdiccion,
                               HorasResolucion = c.HorasResolucion.Value,
                               HorasMargen = c.HorasMargen.Value,
                               FechaCierre = c.FechaCierrre,
                               idJurisdiccion = c.IdJurisdiccion.Value,
                               idPrioridad = c.IdPrioridad.Value,
                               idTipo = c.IdTipo.Value,
                               idServicio = c.IdServicio.Value
                           })).ToList();
            }
            List<TicketsQuery> ticketsFiltrados = tickets.Where(c => (par.idServicio == 0 | c.idServicio == par.idServicio)
                & (par.idPrioridadTicket == 0 | c.idPrioridad == par.idPrioridadTicket)
                & (par.idTipoTicket == 0 | c.idTipo == par.idTipoTicket)
                & (par.idJurisdiccion == 0 | c.idJurisdiccion == par.idJurisdiccion)).ToList();

            TicketsMensuales tm = null;
            if (ticketsFiltrados.Count > 0)
            {
                tm = new TicketsMensuales
                {
                    Mes = new DateTime(par.anioReporte, par.mesReporte, 1).ToString("MMMM", new CultureInfo("es-AR")),
                    Anio = new DateTime(par.anioReporte, par.mesReporte, 1).ToString("yyyy", new CultureInfo("es-AR")),
                    Jurisd = par.idJurisdiccion != 0 ? ticketsFiltrados.First().Jurisdiccion : "Sin Especificar Jurisdicción",
                    PrioridadTicket = par.idPrioridadTicket != 0 ? ticketsFiltrados.First().PrioridadTicket : "Sin Especificar Prioridad",
                    TotalGenerados = tickets.Count,
                    TotalesFiltrados = ticketsFiltrados.Count,
                    DentroDelTiempo = ticketsFiltrados.Where(c => c.DentroDelTiempo & c.FechaCierre.HasValue).Count(),
                    DentroDelMargen = ticketsFiltrados.Where(c => c.DentroDelMargen & c.FechaCierre.HasValue).Count(),
                    FueraMargen = ticketsFiltrados.Where(c => c.FueraDelMargen).Count(),
                    SinCerrar = ticketsFiltrados.Where(c => c.SinCerrar).Count()

                };
            }
            return HtmlFormater.toHtmlInformeMensual(tm);
        }
    }
}