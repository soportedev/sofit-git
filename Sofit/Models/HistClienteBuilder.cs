﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class HistClienteBuilder
    {
        HistoricoCliente historico = null;
        public HistoricoCliente Construir(string cliente)
        {           
            using (var dbc = new IncidenciasEntities())
            {
                HistoricoCliente hist = new HistoricoCliente();
                var clientes = dbc.Contactos.Where(c => c.nombre == cliente);
                if (clientes.Count() == 1)
                {                
                    var cl = clientes.Single();
                    hist.Dependencia = cl.Dependencia.descripcion;
                    hist.Direccion = cl.direccion;
                    hist.Dni = cl.dni;
                    hist.Email = cl.email;
                    hist.Jurisdiccion = cl.Dependencia.Jurisdiccion.descripcion;
                    hist.Nombre = cl.nombre;
                    hist.Te = cl.te;
                    hist.Id = cl.id_contacto;
                    List<HistoricoIncidencia> items = (from u in dbc.Incidencias
                                                       where u.id_contacto == hist.Id                                                       
                                                       select new HistoricoIncidencia
                                                       {
                                                           Numero = u.numero,
                                                           FechaInicio = u.fecha_inicio,
                                                           FechaFin = u.fecha_fin.HasValue ? u.fecha_fin.Value : (DateTime?)null,
                                                           Descripcion=u.descripcion,
                                                           Dependencia = u.Dependencia != null ? u.Dependencia.descripcion : "N/A",
                                                           Jurisdiccion = u.Dependencia != null ? u.Dependencia.Jurisdiccion.descripcion : u.Jurisdiccion.descripcion
                                                       }).ToList();                   
                    hist.Incidentes = items;
                    historico = hist;
                }//if encuentra 1 cliente
            }
            return historico;
        }
    }
}