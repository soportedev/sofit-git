﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public abstract class Reporte
    {
        public abstract string ObtenerReporte(HistoricosParametros param);
    }
}