﻿using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace soporte.Models
{
    public class IncidenciasManager
    {
        private static int lastid = -1;
        public static int LastId
        {
            get 
            {
                return lastid;
            }
            set 
            { 
                lastid = value; 
            }
        }
        private static Regex incidenteForma = new Regex(@"(\w{3})?(\d{2})(\d{5})");//reg digitos incidente
        public Responses crearIncidencia(string numeroIncidente, int tipificacion, int idDependencia, int idUsuario, string observaciones, IDirecciones generador, int idTipoTicket, int idPrioridad, string pathImagen, string pathFile, string mime, int cliente,bool esNvoNro)
        {            
            Responses result = new Responses();            
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    int idTecnico = 0;
                    int idEstadoIniciado = (from u in dbc.Estado_incidente where u.descripcion == "Ingresado" select u.id_estado).Single();
                    int idAreaGenera = generador.getAreaEntrada().IdArea;
                    
                    try
                    {
                        var usuario = dbc.Usuarios.Where(c => c.id_usuario == idUsuario).Single();
                        if (usuario.es_interno.Value)
                        {
                            var idTec = (dbc.tecnico.Where(c => c.id_usuario == idUsuario).Single()).id_tecnico;
                            idTecnico = idTec;
                        }
                        else idTecnico = generador.getIdTecnicoGenerico();
                    }
                    catch (Exception)
                    {
                        idTecnico = generador.getIdTecnicoGenerico();
                    }
                    //int idTecnico = generador.getIdTecnicoGenerico();
                    int idDireccion = generador.IdDireccion;
                    var inc = from u in dbc.Incidencias
                              where u.numero == numeroIncidente
                              select u;
                    if (inc.Count() > 0)
                    {
                        Incidencias encontrada = inc.Single();
                        string estado = (encontrada.fecha_fin.HasValue ? "Cerrado" : "Activo");
                        result.Detail = "Ya existe un incidente con ese número. El estado es: " + estado;
                        if (estado.Equals("Activo"))
                        {
                            string areaActual = encontrada.AreaXIncidencia.Where(s => s.id_incidencia == encontrada.id_incidencias & !s.fecha_fin.HasValue).Single().area.descripcion;
                            string direccionActual = encontrada.AreaXIncidencia.Where(s => s.id_incidencia == encontrada.id_incidencias & !s.fecha_fin.HasValue).Single().area.Direcciones.nombre;
                            result.Detail += ". El área actual es :" + areaActual + ", de la SubDirección: " + direccionActual;
                        }
                    }
                    else
                    {
                        Incidencias ici = new Incidencias
                        {
                            numero = numeroIncidente,
                            id_dependencia = idDependencia,
                            fecha_inicio = System.DateTime.Now,
                            id_tipificacion = tipificacion,
                            descripcion = observaciones,
                            direccion_generacion = idDireccion,
                            id_prioridad = idPrioridad,
                            id_tipo = idTipoTicket,
                            id_contacto=cliente
                        };
                        if (!(pathImagen==string.Empty))
                        {
                            ImagenIncidencia ima = new ImagenIncidencia
                            {                                
                                path = pathImagen
                            };
                            ici.ImagenIncidencia.Add(ima);
                        }
                        if(!(pathFile==string.Empty))
                        {
                            AdjuntoIncidencia adj = new AdjuntoIncidencia
                            {
                                path = pathFile,
                                mime=mime
                            };
                            ici.AdjuntoIncidencia.Add(adj);
                        }
                        dbc.Incidencias.Add(ici);                        
                        IncidenciasActivas ica = new IncidenciasActivas
                        {
                            id_incidencia = ici.id_incidencias,
                            id_area = idAreaGenera,
                            id_estado = idEstadoIniciado,
                            fecha_inicio = ici.fecha_inicio,
                            id_tecnico = idTecnico
                        };
                        AreaXIncidencia axi = new AreaXIncidencia
                        {
                            id_area = idAreaGenera,
                            id_incidencia = ici.id_incidencias,
                            fecha_inicio = DateTime.Now,
                            id_usuario = idUsuario
                        };
                        EstadoXIncidencia exi = new EstadoXIncidencia
                        {
                            id_estado = idEstadoIniciado,
                            id_incidencia = ici.id_incidencias,
                            fecha_inicio = DateTime.Now,
                            id_usuario = idUsuario
                        };                        
                        dbc.IncidenciasActivas.Add(ica);
                        dbc.AreaXIncidencia.Add(axi);
                        dbc.EstadoXIncidencia.Add(exi);
                        dbc.SaveChanges();
                        LastId = ici.id_incidencias; 
                        result.Info = "ok";                        
                        result.Detail = ici.numero;
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        result.Detail += "- Property: \"{0}\", Error: \"{1}\"" +
                            ve.PropertyName + ve.ErrorMessage;
                    }
                }                
            }
            catch (Exception e)
            {
                
                result.Detail = e.Message;                
            }
            return result;
        }
        internal Responses reanimarIncidencia(string nroInc, int idUsuario, IDirecciones direccion)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var incidencia = (from u in dbc.Incidencias
                                  where u.numero == nroInc
                                  select u).SingleOrDefault();

                if (incidencia != null)
                {
                    if (incidencia.fecha_fin == null)
                    {
                        result.Detail = "La Incidencia no fue terminada.";
                    }
                    else
                    {
                        //verifico tiempo de cerrado
                        DateTime fechaCerrado = incidencia.fecha_fin.Value;
                        TimeSpan diferencia = BusinessDays.GetBusinessTime(fechaCerrado, DateTime.Now);
                        if (diferencia.TotalDays < 2)
                        {//puedo cerrar
                            incidencia.fecha_fin = null;
                            int idEstadoIniciado = (from u in dbc.Estado_incidente where u.descripcion == "Ingresado" select u.id_estado).Single();
                            int idArea = direccion.getAreaEntrada().IdArea;
                            int idTecnico = direccion.getIdTecnicoGenerico();
                            IncidenciasActivas ica = new IncidenciasActivas
                            {
                                id_incidencia = incidencia.id_incidencias,
                                id_area = idArea,
                                id_estado = idEstadoIniciado,
                                fecha_inicio = incidencia.fecha_inicio,
                                id_tecnico = idTecnico
                            };
                            AreaXIncidencia axi = new AreaXIncidencia
                            {
                                id_area = idArea,
                                id_incidencia = incidencia.id_incidencias,
                                fecha_inicio = DateTime.Now,
                                id_usuario = idUsuario
                            };
                            EstadoXIncidencia exi = new EstadoXIncidencia
                            {
                                id_estado = idEstadoIniciado,
                                id_incidencia = incidencia.id_incidencias,
                                fecha_inicio = DateTime.Now,
                                id_usuario = idUsuario
                            };
                            dbc.IncidenciasActivas.Add(ica);
                            dbc.AreaXIncidencia.Add(axi);
                            dbc.EstadoXIncidencia.Add(exi);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        else
                        {
                            result.Detail = "Han pasado más de 2 días desde que fue cerrado el ticket y no se puede Revivir.";
                        }
                    }
                }
                else
                {
                    result.Detail = "No se ha encontrado el Incidente.";
                }

            }
            return result;
        }

        internal Responses crearIncidenciaPedidos(string nroTicket, int idTipif, int idDep, int idUsuario, string obs, IDirecciones generador, int idTipoTicket, int idPrioridad, string pathImagen, string pathFile, string mime, int idCliente, bool nvoNro)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    int idTecnico = 0;
                    int idEstadoIniciado = (from u in dbc.Estado_incidente where u.descripcion == "Derivado" select u.id_estado).Single();
                    int idAreaGenera = dbc.area.Where(c => c.descripcion == "Seguridad").Single().id_area;

                    try
                    {
                        var usuario = dbc.Usuarios.Where(c => c.id_usuario == idUsuario).Single();
                        if (usuario.es_interno.Value)
                        {
                            var idTec = (dbc.tecnico.Where(c => c.id_usuario == idUsuario).Single()).id_tecnico;
                            idTecnico = idTec;
                        }
                        else idTecnico = generador.getIdTecnicoGenerico();
                    }
                    catch (Exception)
                    {
                        idTecnico = generador.getIdTecnicoGenerico();
                    }
                    //int idTecnico = generador.getIdTecnicoGenerico();
                    int idDireccion = generador.IdDireccion;
                    var inc = from u in dbc.Incidencias
                              where u.numero == nroTicket
                              select u;
                    if (inc.Count() > 0)
                    {
                        Incidencias encontrada = inc.Single();
                        string estado = (encontrada.fecha_fin.HasValue ? "Cerrado" : "Activo");
                        result.Detail = "Ya existe un incidente con ese número. El estado es: " + estado;
                        if (estado.Equals("Activo"))
                        {
                            string areaActual = encontrada.AreaXIncidencia.Where(s => s.id_incidencia == encontrada.id_incidencias & !s.fecha_fin.HasValue).Single().area.descripcion;
                            string direccionActual = encontrada.AreaXIncidencia.Where(s => s.id_incidencia == encontrada.id_incidencias & !s.fecha_fin.HasValue).Single().area.Direcciones.nombre;
                            result.Detail += ". El área actual es :" + areaActual + ", de la SubDirección: " + direccionActual;
                        }
                    }
                    else
                    {
                        Incidencias ici = new Incidencias
                        {
                            numero = nroTicket,
                            id_dependencia = idDep,
                            fecha_inicio = System.DateTime.Now,
                            id_tipificacion = idTipif,
                            descripcion = obs,
                            direccion_generacion = idDireccion,
                            id_prioridad = idPrioridad,
                            id_tipo = idTipoTicket,
                            id_contacto = idCliente
                        };
                        if (!(pathImagen == string.Empty))
                        {
                            ImagenIncidencia ima = new ImagenIncidencia
                            {
                                path = pathImagen
                            };
                            ici.ImagenIncidencia.Add(ima);
                        }
                        if (!(pathFile == string.Empty))
                        {
                            AdjuntoIncidencia adj = new AdjuntoIncidencia
                            {
                                path = pathFile,
                                mime = mime
                            };
                            ici.AdjuntoIncidencia.Add(adj);
                        }
                        dbc.Incidencias.Add(ici);
                        IncidenciasActivas ica = new IncidenciasActivas
                        {
                            id_incidencia = ici.id_incidencias,
                            id_area = idAreaGenera,
                            id_estado = idEstadoIniciado,
                            fecha_inicio = ici.fecha_inicio,
                            id_tecnico = idTecnico
                        };
                        AreaXIncidencia axi = new AreaXIncidencia
                        {
                            id_area = idAreaGenera,
                            id_incidencia = ici.id_incidencias,
                            fecha_inicio = DateTime.Now,
                            id_usuario = idUsuario
                        };
                        EstadoXIncidencia exi = new EstadoXIncidencia
                        {
                            id_estado = idEstadoIniciado,
                            id_incidencia = ici.id_incidencias,
                            fecha_inicio = DateTime.Now,
                            id_usuario = idUsuario
                        };
                        dbc.IncidenciasActivas.Add(ica);
                        dbc.AreaXIncidencia.Add(axi);
                        dbc.EstadoXIncidencia.Add(exi);
                        dbc.SaveChanges();
                        LastId = ici.id_incidencias;
                        result.Info = "ok";
                        result.Detail = ici.numero;
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        result.Detail += "- Property: \"{0}\", Error: \"{1}\"" +
                            ve.PropertyName + ve.ErrorMessage;
                    }
                }
            }
            catch (Exception e)
            {

                result.Detail = e.Message;
            }
            return result;
        }

        internal Responses eliminarIncidente(int idInc)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                try
                {
                    IncidenciasActivas iac = (from u in dbc.IncidenciasActivas
                                              where u.id_incidencia == idInc
                                              select u).FirstOrDefault();
                    if (iac.Estado_incidente.descripcion == "Ingresado")
                    {
                        var notis = dbc.NotificacionXIncidente.Where(c => c.id_incidencia == idInc);
                        List<string> emails = new List<string>();
                        string nroTicket = iac.Incidencias.numero;                        
                        foreach (NotificacionXIncidente n in notis)
                        {
                            emails.Add(n.email);
                            dbc.NotificacionXIncidente.Remove(n);
                        }
                        dbc.IncidenciasActivas.Remove(iac);
                        var ax = dbc.AreaXIncidencia.Where(c => c.id_incidencia == idInc);
                        foreach (var a in ax)
                        {
                            dbc.AreaXIncidencia.Remove(a);
                        }
                        var exi = from v in dbc.EstadoXIncidencia
                                  where v.id_incidencia == idInc
                                  select v;
                        foreach (var e in exi)
                        {
                            dbc.EstadoXIncidencia.Remove(e);
                        }
                        Incidencias inc = (from i in dbc.Incidencias
                                           where i.id_incidencias == idInc
                                           select i).FirstOrDefault();                        
                        inc.ImagenIncidencia.Clear();
                        inc.AdjuntoIncidencia.Clear();
                        dbc.Incidencias.Remove(inc);
                        dbc.SaveChanges();
                        result.Info = "ok";
                        if (result.Info == "ok")
                        {
                            if (emails.Count() > 0)
                            {
                                string subject = "Ticket Cancelado";
                                string body = "Fue Cancelado el Ticket con la identificación <b>" + nroTicket;
                                   
                                EmailSender sender = new EmailSender();
                                foreach (string n in emails)
                                {
                                    sender.sendEmail(new string[] { n, subject, body });
                                }

                            }
                        }                        
                    }
                    else
                    {
                        result.Detail = "El Ticket no se puede eliminar. Contacte al administrador.";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = "No se puede eliminar el Ticket. Contacte al administrador.";
                }
            }
            return result;
        }
        internal Responses cerrarIncidencia(int idUsuario,Incidencia incidencia)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {                    
                    var inc = from u in dbc.Incidencias
                              where u.id_incidencias == incidencia.Id
                              select u;
                    if (inc.Count() == 0)
                    {
                        result.Detail = "No se encontró el incidente.";
                    }
                    else
                    {
                        string nombreUsuario = dbc.Usuarios.Where(c => c.id_usuario == idUsuario).Single().nombre_completo;
                        Incidencias incid = inc.Single();
                        incid.fecha_fin = DateTime.Now;
                        incid.id_usuario_cierra = idUsuario;
                        IncidenciasActivas ica = (from ic in dbc.IncidenciasActivas
                                                  where ic.id_incidencia == incid.id_incidencias
                                                  select ic).Single();
                        dbc.IncidenciasActivas.Remove(ica);

                        AreaXIncidencia axi = (from ax in dbc.AreaXIncidencia
                                               where ax.id_incidencia == incid.id_incidencias & !ax.fecha_fin.HasValue
                                               select ax).Single();
                        axi.fecha_fin = DateTime.Now;

                        EstadoXIncidencia exi = (from ex in dbc.EstadoXIncidencia
                                                 where ex.id_incidencia == incid.id_incidencias & !ex.fecha_fin.HasValue
                                                 select ex).Single();
                        exi.fecha_fin = DateTime.Now;

                        var notis = incid.NotificacionXIncidente;
                        if (notis.Count() > 0)
                        {
                            string subject = "Ticket Solucionado";
                            string body = "Fue Solucionado el Ticket con la identificación <b>" + incid.numero + 
                                "</b> en el día "+DateTime.Now.ToShortDateString() +" por <b>" + nombreUsuario;
                            EmailSender sender = new EmailSender();
                            foreach (NotificacionXIncidente n in notis)
                            {
                                sender.sendEmail(new string[] { n.email, subject, body });                                
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }
        internal static Responses getNextNro()
        {
            Responses result = new Responses();
            Regex regañoultDos = new Regex(@"\d{2}$");//reg digitos año            
            Match match2anio = regañoultDos.Match(DateTime.Now.Year.ToString()); //ultimos digitos año actual                    
            string nvoUltimo = string.Empty;//string nro final

            using (var dbc = new IncidenciasEntities())
            {
                UltimoNroIncidencia ultimo = (from u in dbc.UltimoNroIncidencia
                                              select u).First();
                if (ultimo == null) ultimo = new UltimoNroIncidencia
                {
                    numero = "1600000"
                };
                Match ticketMatch = incidenteForma.Match(ultimo.numero);  //digitos incidente
                int secuencia = Int32.Parse(ticketMatch.Groups[3].Value); //secuencia

                int añoUltimoTicket = Int32.Parse(ticketMatch.Groups[2].Value);
                int añoActual = Int32.Parse(match2anio.Value);
                if (añoUltimoTicket != añoActual)
                {
                    secuencia = 1;
                }
                else secuencia++;
                nvoUltimo = añoActual.ToString("D2") + secuencia.ToString("D5");//Nro. final                    
                //actualizo el ultimo nro
                ultimo.numero = nvoUltimo;
                dbc.SaveChanges();
                result.Info = "ok";
                result.Detail = nvoUltimo;
            }
            return result;
        }
        
    }
}