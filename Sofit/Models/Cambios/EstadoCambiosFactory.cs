﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public class EstadoCambiosFactory
    {
        private static List<EstadoCambioVista> estados = null;
        static EstadoCambiosFactory()
        {
            using (var db = new IncidenciasEntities())
            {
                estados = (from u in db.EstadoGC
                           select new EstadoCambioVista
                           {
                               Id = u.id,
                               Nombre = u.nombre
                           }).ToList();
            }
        }
        public static EstadoCambioVista getEstadoVista(string nombre)
        {
            return estados.Where(c => c.Nombre == nombre).Single();
        }
        public static EstadoCambioVista getEstadoVista(int id)
        {
            return estados.Where(c => c.Id == id).Single();
        }
        public static EstadoCambio getEstadoBiz(string nombre)
        {
            EstadoCambioVista ev = estados.Where(c => c.Nombre == nombre).SingleOrDefault();
            if (ev == null) return null;
            else return getEpn(ev);
        }
        public static EstadoCambio getEstadoBiz(int id)
        {
            EstadoCambioVista ev = estados.Where(c => c.Id == id).SingleOrDefault();
            if (ev == null) return null;
            else return getEpn(ev);
        }

        private static EstadoCambio getEpn(EstadoCambioVista epv)
        {
            EstadoCambio estado = null;
            switch (epv.Nombre)
            {
                case "Solicitud de Cambio":
                    estado = new EstadoSolicitudCambio(epv.Nombre, epv.Id);
                    break;
                case "Aprobado":
                    estado = new EstadoAprobado(epv.Nombre, epv.Id);
                    break;                
                case "Rechazado":
                    estado = new EstadoRechazado(epv.Nombre, epv.Id);
                    break;
                case "Pendiente comite":
                    estado = new EstadoPendienteAprobacion(epv.Nombre, epv.Id);
                    break;
                case "Implementado":
                    estado = new EstadoImplementado(epv.Nombre, epv.Id);
                    break;
            }
            return estado;
        }
    }
}