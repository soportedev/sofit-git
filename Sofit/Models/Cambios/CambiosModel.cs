﻿using soporte.Models.Displays;
using soporte.Models.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public class CambiosModel
    {
        public CambiosModel()
        {
            this.Autorizantes = new List<UsuarioVista>();
            this.MisCambios = new List<CambiosVista>();
        }
        public int PaginaActualCambios { get; set; }
        public int CantidadPaginasCambios { get; set; }
        public int CantidadPorPagina { get; set; }
        public string FiltroAplicado { get; set; }
        
        public List<CambiosVista> MisCambios { get; set; }
        public List<UsuarioVista> Autorizantes { get; set; }       
        public List<EstadoCambioVista> Estados { get; set; }
        public List<PrioridadCambio> Prioridad { get; set; }
        public List<TipoCambio> Tipo { get; set; }
        public List<string> dummy { get; set; }
    }
}