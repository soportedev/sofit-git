﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Statics;

namespace Sofit.Models.Cambios
{
    public class EstadoSolicitudCambio : EstadoCambio
    {
        public EstadoSolicitudCambio (string nombre, int id){
            base.Id = id;
            base.Nombre = nombre;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses implementar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraComite(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses rechazar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }
    }
}