﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Statics;

namespace Sofit.Models.Cambios
{
    public class EstadoImplementado : EstadoCambio
    {
        public EstadoImplementado(string nombre, int id)
        {
            base.Nombre = nombre;
            base.Id = id;
        }
        public override Responses aprobar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses implementar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses paraComite(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }

        public override Responses rechazar(string nombreUsuario, string obs)
        {
            throw new NotImplementedException();
        }
    }
}