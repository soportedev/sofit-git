﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sofit.DataAccess;
using soporte.Models.Statics;

namespace Sofit.Models.Cambios
{
    public class PedidosCambiosManager
    {
        internal Responses NuevoPedidoCambio(CambiosVista pedido)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {                    
                    
                    GestionCambios nuevoPedido = new GestionCambios
                    {
                        fecha_creacion = DateTime.Now,
                        asunto = pedido.Asunto,
                        descripcion = pedido.Descripcion,
                        id_solicitante = pedido.Solicitante.id,
                        id_autorizante = pedido.Autorizante.id,
                        id_estado = pedido.EstadoNegocio.Id,
                        id_tipo = pedido.Tipo.Id,
                        id_prioridad=pedido.Prioridad.Id,
                        fecha_inicio_estimada=pedido.fechaInicioEstimada,
                        duracion_estimada=pedido.duracionEstimada,
                        razon=pedido.Razon,
                        resultados=pedido.Resultados,
                        riesgos=pedido.Riesgos,
                        recursos=pedido.Recursos,
                        servicios_afectados=pedido.ServiciosAfectados,
                        vuelta_atras=pedido.VueltaAtras,
                        conclusion=pedido.Conclusion                                             
                    };
                    foreach (var u in pedido.ElemConf)
                    {
                        UnidadConf uc = null;
                        if (u.Nombre != null) uc = dbc.UnidadConf.Where(c => c.nombre == u.Nombre).Single();
                        else uc = dbc.Servidor.Where(c => c.nombreComun == u.NombreComun).Single().UnidadConf;
                        nuevoPedido.EcxGc.Add(new EcxGc { UnidadConf = uc });
                    }                    
                    if (pedido.Archivos.Count>0)
                    {
                        nuevoPedido.DocXGC.Add(new DocXGC { path_doc = pedido.Archivos[0] });
                    }

                    dbc.GestionCambios.Add(nuevoPedido);                   
                    dbc.SaveChanges();                    
                    result.Info = "ok";
                    result.Detail = nuevoPedido.id.ToString();
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }

        internal Responses modificacion(CambiosVista nuevoPedido)
        {
            Responses result = new Responses();
            result = verificarDiferencias(nuevoPedido);
            return result;
        }

        private Responses verificarDiferencias(CambiosVista nuevoPedido)
        {
            Responses result = new Responses();
            List<string> mods = new List<string>();
            bool flag = false;
            //sb.Append(DateTime.Now + "- Modificacion realizada por: " + nuevoPedido.UsuarioActual.displayName);
            using (var dbc = new IncidenciasEntities())
            {                
                var pedidoActual = dbc.GestionCambios.Where(c => c.id == nuevoPedido.idCambio).Single();
                if (pedidoActual.fecha_inicio_estimada != nuevoPedido.fechaInicioEstimada) {
                    if (pedidoActual.fecha_inicio_estimada.Date != nuevoPedido.fechaInicioEstimada.Date) {
                        flag = true;
                        mods.Add("Se Actualizó la fecha de inicio estimada por: " + nuevoPedido.fechaInicioEstimada.Date.ToShortDateString());                        
                    }
                    int compare = TimeSpan.Compare(pedidoActual.fecha_inicio_estimada.TimeOfDay, nuevoPedido.fechaInicioEstimada.TimeOfDay);
                    if (compare != 0)
                    {
                        flag = true;
                        mods.Add("Se actualizó la hora de Inicio estimada por: " + nuevoPedido.fechaInicioEstimada.ToString("HH:mm"));
                    }
                    pedidoActual.fecha_inicio_estimada = nuevoPedido.fechaInicioEstimada;
                }
                if (pedidoActual.duracion_estimada != nuevoPedido.duracionEstimada)
                {
                    flag = true;
                    mods.Add("Se Actualizó la duración prevista por: " + nuevoPedido.duracionEstimada);
                    pedidoActual.duracion_estimada = nuevoPedido.duracionEstimada;
                }
                if (nuevoPedido.Notas.Length>10)
                {
                    flag = true;
                    mods.Add("Agregado Nota: " + nuevoPedido.Notas);                    
                }                              
                if (pedidoActual.id_prioridad!=nuevoPedido.Prioridad.Id)
                {
                    flag = true;
                    pedidoActual.id_prioridad = nuevoPedido.Prioridad.Id;
                    mods.Add("Modificado la prioridad a: " + dbc.PrioridadGC.Where(c=>c.id==nuevoPedido.Prioridad.Id).Single().nombre);
                    
                }
                if(pedidoActual.descripcion!=nuevoPedido.Descripcion)
                {
                    flag = true;
                    mods.Add("Modificado la descripción");
                    pedidoActual.descripcion = nuevoPedido.Descripcion;
                }
                if (pedidoActual.id_tipo != nuevoPedido.Tipo.Id)
                {
                    flag = true;
                    mods.Add("Modificado el tipo a: " + dbc.TipoPedido.Where(c => c.id == nuevoPedido.Tipo.Id).Single().nombre);
                    pedidoActual.id_tipo = nuevoPedido.Tipo.Id;
                }
                if (pedidoActual.razon != nuevoPedido.Razon)
                {
                    flag = true;
                    mods.Add("Modificado el campo razón");
                    pedidoActual.razon = nuevoPedido.Razon;
                }
                if (pedidoActual.recursos != nuevoPedido.Recursos)
                {
                    flag = true;
                    mods.Add("Modificado el campo recursos.");
                    pedidoActual.recursos = nuevoPedido.Recursos;
                }
                if (pedidoActual.resultados != nuevoPedido.Resultados)
                {
                    flag = true;
                    mods.Add("Modificado el campo resultados");
                    pedidoActual.resultados = nuevoPedido.Resultados;
                }
                if (pedidoActual.riesgos != nuevoPedido.Riesgos)
                {
                    flag = true;
                    mods.Add("Modificado el campo Riesgos");
                    pedidoActual.riesgos = nuevoPedido.Riesgos;
                }
                if(pedidoActual.servicios_afectados!=nuevoPedido.ServiciosAfectados)
                {
                    flag = true;
                    mods.Add("Modificado el campo Servicios");
                    pedidoActual.servicios_afectados = nuevoPedido.ServiciosAfectados;
                }
                if (pedidoActual.vuelta_atras != nuevoPedido.VueltaAtras)
                {
                    flag = true;
                    mods.Add("Modificado el campo vuelta atrás");
                    pedidoActual.vuelta_atras = nuevoPedido.VueltaAtras;
                }
                if (pedidoActual.conclusion != nuevoPedido.Conclusion)
                {
                    flag = true;
                    mods.Add("Modificado el campo conclusión");
                    pedidoActual.conclusion = nuevoPedido.Conclusion;
                }
                if (nuevoPedido.Archivos.Count > 0)
                {
                    flag = true;
                    mods.Add("Se agregó el archivo: " + nuevoPedido.Archivos[0]);
                    pedidoActual.DocXGC.Add(new DocXGC { path_doc = nuevoPedido.Archivos[0] });
                }
                if(flag)
                {
                    try
                    {
                        HistoricoGC nuevo = new HistoricoGC
                        {
                            id_cambio = nuevoPedido.idCambio,
                            fecha = DateTime.Now,
                            id_usuario = nuevoPedido.UsuarioActual.id                            
                        };
                        foreach (var v in mods)
                        {
                            nuevo.Mods.Add(new Mods
                            {
                                observaciones = v
                            });                            
                        }
                        dbc.HistoricoGC.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }catch(Exception e)
                    {
                        result.Detail = e.Message;
                    }
                }else
                {
                    result.Detail = "Sin cambios registrados";
                }
            }
            return result;
        }
    }
}