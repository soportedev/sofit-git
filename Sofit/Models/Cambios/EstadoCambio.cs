﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public abstract class EstadoCambio
    {
        public int Id { get; set; }        
        public string Nombre { get; set; }
        public abstract Responses rechazar(string nombreUsuario, string obs);
        public abstract Responses aprobar(string nombreUsuario, string obs);
        public abstract Responses paraComite(string nombreUsuario, string obs);
        public abstract Responses implementar(string nombreUsuario, string obs);               
    }
}