﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public class GestionCambiosVista
    {
        public int Nro { get; set; }
        public DateTime Fecha { get; set; }
        public string FechaStr {
            get
            {
                return Fecha.ToShortDateString();
            }
        }
    }
}