﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public class Log:IComparable
    {
        public DateTime fechadt { get; set; }
        public string Usuario { get; set; }
        public string Fecha {
            get
            {
                return fechadt.ToString("dd/MM/yyyy HH:mm");
            }
        }
        public List<string> Modificacion { get; set; }

        public int CompareTo(object obj)
        {
            Log otro = obj as Log;
            return fechadt.CompareTo(otro.fechadt);
        }
    }
}