﻿using soporte.Models.ClasesVistas;
using soporte.Models.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Models.Cambios
{
    public class CambiosVista:IComparable
    {
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaInicioEstimada { get; set; }
        public DateTime fechaInicioReal { get; set; }
        public DateTime fechaRealizacion { get; set; }
        public string FechaOnly {
            get
            {
                return fechaInicioEstimada.ToString("dd/MM/yyyy");
            }
        }
        public string HoraOnly {
            get
            {
                return fechaInicioEstimada.ToString("HH:mm");
            }
        }
        public string MiRol { get; set; }
        public string Notas { get; set; }
        public string[] Acciones
        {
            get
            {
                string[] Acciones = null;
                switch (this.EstadoActual.Nombre)
                {
                    case "Solicitud de Cambio":
                        if (MiRol == "Autorizante" | MiRol=="Director Cambios")
                        {
                            Acciones = new string[] { "Aprobación comité", "Cancelar" ,"Modificar"};
                        }
                        if (MiRol == "Solicitante")
                        {
                            Acciones = new string[] { "Cancelar", "Modificar" };
                        }
                        return Acciones;
                    case "Pendiente aprobación comité":
                        if (MiRol == "Director Cambios")
                        {
                            Acciones = new string[] { "Aprobar", "Rechazar", "Modificar" };
                        }
                        return Acciones;
                    case "Aprobado":
                        if (MiRol == "Autorizante" | MiRol == "Director Cambios" | MiRol == "Solicitante")
                        {
                            Acciones = new string[] { "Implementado", "Rechazar", "Modificar" };
                        }
                        return Acciones;
                    case "Implementado":
                        if (MiRol == "Director Cambios")
                        {
                            Acciones = new string[] { "Cerrar" };
                        }                        
                        return Acciones;
                    case "ActualizadoCmdb":
                        if(MiRol=="Director Cambios")
                        {
                            Acciones= new string[] { "Cerrar" };
                        }
                        return Acciones;
                    default: return null;
                }
            }
            set { }
        }
       
        public int idCambio { get; set; }
        public List<EcCambiosVista> ElemConf { get; set; }
        public string FCreacion {
            get
            {
                return fechaCreacion.ToShortDateString();
            }
            set { } }
        public string FInicioEstimada {
            get
            {
                return fechaInicioEstimada.ToShortDateString();
            }
            set { } }
        public string HoraInicioEstimada {
            get
            {
                return fechaInicioEstimada.ToString("HH:mm");
            }
            set { } }
        public string FInicioReal {
            get
            {
                return fechaInicioReal.ToShortDateString();
            }
            set { } }
        public string FRealizacion {
            get
            {
                return fechaRealizacion.ToShortDateString();
            }
            set { } }
        public UsuarioVista UsuarioActual { get; set; }
        public TipoCambio Tipo { get; set; }
        public int duracionEstimada { get; set; }
        public string Asunto { get; set; }
        public string Descripcion { get; set; }
        public string Razon { get; set; }
        public string Resultados { get; set; }
        public string Riesgos { get; set; }
        public string Recursos { get; set; }
        public string ServiciosAfectados { get; set; }
        public string VueltaAtras { get; set; }
        public string Conclusion { get; set; }
        public EstadoCambioVista EstadoActual { get; set; }
        public EstadoCambio EstadoNegocio { get; set; }
        public UsuarioVista Solicitante { get; set; }
        public UsuarioVista Autorizante { get; set; }        
        public PrioridadCambio Prioridad { get; set; }                             
        public List<string> Archivos { get; set; }
        public List<Log> Logs { get; set; }
        public int CompareTo(object obj)
        {
            CambiosVista otro = obj as CambiosVista;
            return this.fechaCreacion.CompareTo(otro.fechaCreacion);
        }
    }
}