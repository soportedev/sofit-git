﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;
using soporte.Controllers;
using Sofit.DataAccess;
using System.Data.Entity.Infrastructure;

namespace soporte.Models
{
    public class ReportesCreadosYCerrados : Reporte
    {
        public override string ObtenerReporte(HistoricosParametros param)
        {
            return CreadosYCerradosTotales(param);
        }
        #region creados y cerrados totales
        private string CreadosYCerradosTotales(HistoricosParametros par)
        {
            if (par.DetalleSubdir)
            {
                return HtmlFormater.toHtmlTotalesCerradosTotales(lookupTicketCerradosTotalesTotales(par));
            }
            else
            {
                return HtmlFormater.toHtmlTotalesCerradosGeneralTotales(lookupTicketCerradosTotalesGeneralTotales(par));
            }
        }
        //sin detalle de subdireccion
        private List<TicketsCreados> lookupTicketCerradosTotalesGeneralTotales(HistoricosParametros par)
        {
            List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();
            List<TicketsQuery> tickets = new List<TicketsQuery>();
            using (var dbc = new IncidenciasEntities())
            {
                ((IObjectContextAdapter)dbc).ObjectContext.CommandTimeout = 40;
                tickets = (dbc.sp_listado_tickets(new DateTime(par.anioDesde, par.mesDesde, par.diaDesde, 0, 0, 0),
                    new DateTime(par.anioHasta, par.mesHasta, par.diaHasta, 23, 59, 59), par.idArea, par.idPrioridadTicket,
                        par.idTipoTicket, par.idServicio, par.idJurisdiccion).Select(c =>
                   new TicketsQuery
                   {
                       DireccionGeneracion = c.Direccion,
                       TipoTicket = c.TipoTicket,
                       Numero = c.Numero,
                       IdTicket = c.IdTicket.Value,
                       FechaGeneracion = c.FechaGeneracion.Value,
                       HoursOffline = c.TiempoSuspendido.Value,
                       HoursWeekend = c.TiempoWeekend.Value,
                       PrioridadTicket = c.PrioridadTicket,
                       HorasResolucion = c.HorasResolucion.Value,
                       HorasMargen = c.HorasMargen.Value,
                       FechaCierre = c.FechaCierrre
                   })).ToList();

                //List<TicketsQuery> ticketsFiltrados = tickets.Where(c => c.FechaResolucion <= par.FechaHasta).ToList();
                ticketsCreados = (from u in tickets
                                  group u by new
                                  {
                                      u.PrioridadTicket,
                                      u.TipoTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      PrioridadTicket = g.Key.PrioridadTicket,
                                      TipoTicket = g.Key.TipoTicket,
                                      Cantidad = g.Count(),
                                      DentroDelTiempo = tickets.Where(c => c.DentroDelTiempo & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      DentroDelMargen = tickets.Where(c => c.DentroDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      FueraMargen = tickets.Where(c => c.FueraDelMargen & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      SinCerrar = tickets.Where(c => c.SinCerrar & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count()
                                  }).ToList();
            }
            //HttpContext.Current.Session["tableSource"] = DataTableConverter.ToDataTable(ticketsCreados);
            return ticketsCreados;
        }
        //-->

        //con detalle de subdireccion
        private List<TicketsCreados> lookupTicketCerradosTotalesTotales(HistoricosParametros par)
        {
            List<TicketsCreados> ticketsCreados = new List<TicketsCreados>();

            using (var dbc = new IncidenciasEntities())
            {
                List<TicketsQuery> tickets = (dbc.sp_listado_tickets(par.FechaDesde, par.FechaHasta, par.idArea, par.idPrioridadTicket,
                        par.idTipoTicket, par.idServicio, par.idJurisdiccion).Select(c =>
                   new TicketsQuery
                   {
                       DireccionGeneracion = c.Direccion,
                       TipoTicket = c.TipoTicket,
                       Numero = c.Numero,
                       IdTicket = c.IdTicket.Value,
                       FechaGeneracion = c.FechaGeneracion.Value,
                       HoursOffline = c.TiempoSuspendido.Value,
                       HoursWeekend = c.TiempoWeekend.Value,
                       PrioridadTicket = c.PrioridadTicket,
                       HorasResolucion = c.HorasResolucion.Value,
                       HorasMargen = c.HorasMargen.Value,
                       FechaCierre = c.FechaCierrre
                   })).ToList();
                List<TicketsQuery> ticketsFiltrados = tickets.Where(c => c.FechaResolucion <= par.FechaHasta).ToList();
                ticketsCreados = (from u in ticketsFiltrados
                                  group u by new
                                  {
                                      u.DireccionGeneracion,
                                      u.PrioridadTicket,
                                      u.TipoTicket
                                  } into g
                                  //orderby g.Count() descending
                                  select new TicketsCreados
                                  {
                                      SubDirGeneradora = g.Key.DireccionGeneracion,
                                      PrioridadTicket = g.Key.PrioridadTicket,
                                      TipoTicket = g.Key.TipoTicket,
                                      Cantidad = g.Count(),
                                      DentroDelTiempo = ticketsFiltrados.Where(c => c.DentroDelTiempo & c.DireccionGeneracion == g.Key.DireccionGeneracion & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      DentroDelMargen = ticketsFiltrados.Where(c => c.DentroDelMargen & c.DireccionGeneracion == g.Key.DireccionGeneracion & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      FueraMargen = ticketsFiltrados.Where(c => c.FueraDelMargen & c.DireccionGeneracion == g.Key.DireccionGeneracion & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count(),
                                      SinCerrar = ticketsFiltrados.Where(c => c.SinCerrar & c.DireccionGeneracion == g.Key.DireccionGeneracion & c.PrioridadTicket == g.Key.PrioridadTicket & c.TipoTicket == g.Key.TipoTicket).Count()
                                  }).ToList();
            }
            //HttpContext.Current.Session["tableSource"] = DataTableConverter.ToDataTable(ticketsCreados);
            return ticketsCreados;
        }
        //-->
        #endregion
    }
}