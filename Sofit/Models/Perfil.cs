﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public abstract class Perfil
    {
        public int id { get; set; }
        public string nombre{ get; set; }
        public string nombreBase { get; set; }
        public Direccion direccion { get; set; }
        public int relevancia { get; set; }
        public abstract Constantes.AccesosPagina getAcceso(string page);
        public abstract string getHomePage();

        public override bool Equals(object obj)
        {
            Perfil o = (Perfil)obj;
            return nombre.Equals(o.nombre);
        }
        public override int GetHashCode()
        {
            return nombre.GetHashCode();
        }
    }
}