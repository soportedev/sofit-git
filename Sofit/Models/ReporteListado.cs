﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;
using soporte.Controllers;
using Sofit.DataAccess;

namespace soporte.Models
{
    public class ReporteListado : Reporte
    {
        public override string ObtenerReporte(HistoricosParametros param)
        {
            TimeSpan diasdiferencia = param.FechaHasta - param.FechaDesde;
            if (diasdiferencia.TotalDays<=31 ) return listado(param);
            else return "El período máximo para solicitar un listado es de un (1)mes";
        }
        #region listado
        private string listado(HistoricosParametros par)
        {
            List<TicketsQuery> tickets = null;
            using (var dbc = new IncidenciasEntities())
            {
                tickets = (dbc.sp_listado_tickets(new DateTime(par.anioDesde, par.mesDesde, par.diaDesde, 0, 0, 0),
                    new DateTime(par.anioHasta, par.mesHasta, par.diaHasta, 23, 59, 59), par.idArea, par.idPrioridadTicket,
                                par.idTipoTicket, par.idServicio, par.idJurisdiccion).Select(c =>
                           new TicketsQuery
                           {
                               DireccionGeneracion = c.Direccion,
                               TipoTicket = c.TipoTicket,
                               Numero = c.Numero,
                               IdTicket = c.IdTicket.Value,
                               FechaGeneracion = c.FechaGeneracion.Value,
                               HoursOffline = c.TiempoSuspendido.Value,
                               HoursWeekend = c.TiempoWeekend.Value,
                               PrioridadTicket = c.PrioridadTicket,
                               HorasResolucion = c.HorasResolucion.Value,
                               HorasMargen = c.HorasMargen.Value,
                               FechaCierre = c.FechaCierrre
                           })).ToList();

            }
            if (par.OfT)
            {
                List<TicketsQuery> ticketsFiltrados = tickets.Where(c => !c.DentroDelTiempo & !c.DentroDelMargen).ToList();
                tickets = ticketsFiltrados;
            }
            //HttpContext.Current.Session["tableSource"] = DataTableConverter.ToDataTable(tickets);            
            return HtmlFormater.toHtmlListado(tickets);
        }
        #endregion
    }
}