﻿using soporte.Areas.Operaciones.Models;
using soporte.Areas.Soporte.Models;
using soporte.Areas.Soporte.Models.Factory;
using Sofit.DataAccess;
using soporte.Models.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class IncidenciaBuilder
    {
        private Incidencia incidencia;
        public IncidenciaBuilder()
        {
            
        }    
        /// <summary>
        /// Creacion de tickets
        /// </summary>
        /// <param name="idIncidencia">id del ticket</param>
        /// <param name="movimientoAreas">true para construir el movimiento en areas</param>
        /// <param name="agregarEquipos">para agregar equipos</param>
        /// <param name="movimientoEquipos">agregar movimientoequipos</param>
        internal void Construir(int idIncidencia,bool movimientoAreas,bool agregarEquipos,bool movimientoEquipos)
        {            
            using (var dbc = new IncidenciasEntities())
            {
                var incid = (from u in dbc.IncidenciasActivas
                             where u.Incidencias.id_incidencias == idIncidencia
                             select u).Single();
                if (incid != null)
                {
                    incidencia = new Incidencia
                    {
                        Numero = incid.Incidencias.numero,
                        Id=incid.id_incidencia,
                        AreaActual=AreaIncidenciaFactory.getArea(incid.area.id_area),
                        EstadoActual=EstadoIncidenciaFactory.getEstado(incid.id_estado),
                        idTecnicoActual=incid.id_tecnico
                    };                    
                }
                if (movimientoAreas)//movimiento tickets
                {
                    incidencia.AreasActuantes = new List<MovAreaIncidencia>();
                    var mov = (from u in dbc.AreaXIncidencia
                               where u.id_incidencia == idIncidencia
                               select u).ToList();
                    foreach(var v in mov){
                        DateTime fechaHastaParsed=(v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
                        List<ResolucionesIncidencias> resoluciones = (from s in dbc.Resoluciones
                                                                      where s.id_incidencia == idIncidencia && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
                                                                      //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                      select new ResolucionesIncidencias
                                                                      {
                                                                          idResolucion = s.id_resolucion,
                                                                          IdEquipo = s.id_uc,
                                                                          Fecha = s.fecha,
                                                                          Equipo = s.UnidadConf.nombre,
                                                                          Observaciones = s.observaciones,
                                                                          Tecnico = s.tecnico.descripcion,
                                                                          idTecnico = s.tecnico.id_tecnico,
                                                                          TipoResolucion = s.tipo_resolucion.descripcion,
                                                                          Turno = s.turno.descripcion,
                                                                          idTipoResolucion = s.id_tipo_resolucion.Value,
                                                                          IdUsuario=s.usuario,
                                                                          PathImagen = s.path_imagen
                                                                      }).ToList();
                        MovAreaIncidencia mar = new MovAreaIncidencia
                        {
                            Area = AreaIncidenciaFactory.getArea(v.id_area),
                            FechaDesde = v.fecha_inicio,
                            FechaHasta = v.fecha_fin,
                            Resoluciones = resoluciones
                        };
                        incidencia.AreasActuantes.Add(mar);
                    }
                }
                //agregar equipos si corresponde
                if (agregarEquipos)
                {
                    if (dbc.EquiposXIncidencia.Where(c=>c.id_incidencia==incidencia.Id).Any())
                    {
                        incidencia.EquiposDecorator = new EquiposEnIncidenciaSoporte();
                        incidencia.EquiposDecorator.ticket = incidencia;
                        var equipos = (from u in dbc.EquiposXIncidencia
                                      where u.id_incidencia == incidencia.Id
                                      select new EquipoEnIncidente
                                      {
                                          nroOblea = u.UnidadConf.nombre,
                                          idEquipo = u.UnidadConf.id_uc,
                                          Serie = u.UnidadConf.nro_serie,
                                          idTecnico=u.id_tecnico,
                                          observaciones = u.UnidadConf.observaciones,
                                          fecha = u.UnidadConf.fecha,
                                          idEstado = u.id_estado,
                                          idTipo = u.UnidadConf.id_tipo,
                                          idArea=u.id_area
                                      }).ToList();
                        foreach (var e in equipos)
                        {
                            e.incidencia = incidencia;
                        }
                        incidencia.EquiposDecorator.equiposSoporte = equipos;
                        if (movimientoEquipos)//movimientoequipos
                        {
                            foreach (EquipoEnIncidente e in equipos)
                            {
                                var mov = (from u in dbc.AreaXEquipoN
                                           where u.id_incidencia == incidencia.Id & u.id_equipo == e.idEquipo
                                           select u).ToList();
                                if (mov.Count > 0)
                                {
                                    e.AreasActuantes = new List<MovAreaEquipo>();
                                    foreach (var v in mov)
                                    {
                                        DateTime fechaHastaParsed = (v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
                                        List<ResolucionesEquipo> resoluciones = (from s in dbc.Resoluciones
                                                                                 where s.id_incidencia == incidencia.Id && s.id_uc == e.idEquipo && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
                                                                                 //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                                                 select new ResolucionesEquipo
                                                                                 {
                                                                                     Fecha = s.fecha,
                                                                                     Observaciones = s.observaciones,
                                                                                     Tecnico = s.tecnico.descripcion,
                                                                                     TipoResolucion = s.tipo_resolucion.descripcion,
                                                                                     idResolucion = s.id_resolucion,
                                                                                     idTipoResolucion = s.tipo_resolucion.id_tipo_resolucion,
                                                                                     Turno = s.turno.descripcion,
                                                                                     IdUsuario=s.usuario
                                                                                 }).ToList();
                                        MovAreaEquipo mar = new MovAreaEquipo
                                        {
                                            Area = AreaEquipoFactory.getAreaXId(v.id_area),
                                            FechaDesde = v.fecha_inicio,
                                            FechaHasta = v.fecha_fin,
                                            Resoluciones = resoluciones
                                        };
                                        mar.Resoluciones.Sort();
                                        e.AreasActuantes.Add(mar);
                                    }
                                }
                                if (e.AreasActuantes != null) e.AreasActuantes.Sort();
                            }//for
                        }
                    }//si tiene equipos soporte
                    if (dbc.EnlaceXIncidencia.Where(c => c.id_incidencia == incidencia.Id).Any())
                    {
                        incidencia.EquiposDecorator = new EnlacesEnIncidencia();
                        incidencia.EquiposDecorator.ticket = incidencia;
                        List<EnlaceOperaciones> enlaces = new List<EnlaceOperaciones>();
                        var en = from u in dbc.EnlaceXIncidencia
                                 where u.id_incidencia == incidencia.Id
                                 select u;
                        foreach (EnlaceXIncidencia exi in en)
                        {
                             EnlaceOperaciones enlace = new EnlaceOperaciones();
                             if (exi.EnlaceComunicaciones.AnchoBanda != null)
                             {
                                 enlace.anchoBanda = exi.EnlaceComunicaciones.AnchoBanda.nombre;
                             }
                             if (exi.EnlaceComunicaciones.ContactosOperaciones != null)
                             {
                                 enlace.Contacto = new Contacto
                                 {
                                        id = exi.EnlaceComunicaciones.ContactosOperaciones.id_contacto,
                                        direccion = exi.EnlaceComunicaciones.ContactosOperaciones.direccion,
                                        nombre = exi.EnlaceComunicaciones.ContactosOperaciones.nombre,
                                        te = exi.EnlaceComunicaciones.ContactosOperaciones.te
                                 };
                                }
                                enlace.Descripcion = exi.EnlaceComunicaciones.descripcion;
                                enlace.Estado = EstadoEquipoFactory.getEstadoPorId(exi.id_estado);
                                enlace.IdEnlace = exi.EnlaceComunicaciones.id_enlace;
                                enlace.IdEquipo = exi.EnlaceComunicaciones.UnidadConf.id_uc;
                                enlace.Jurisdicciones = new soporte.Models.ClasesVistas.Jurisdicciones
                                {
                                    idJurisdiccion = exi.EnlaceComunicaciones.UnidadConf.Jurisdiccion.id_jurisdiccion,
                                    Nombre = exi.EnlaceComunicaciones.UnidadConf.Jurisdiccion.descripcion
                                };
                                enlace.lan = exi.EnlaceComunicaciones.lan;
                                enlace.loopback = exi.EnlaceComunicaciones.loopback;
                                enlace.Nombre = exi.EnlaceComunicaciones.UnidadConf.nombre;
                                enlace.nroRef = exi.EnlaceComunicaciones.nro_ref;
                                enlace.Proveedor = exi.EnlaceComunicaciones.ProveedoresEnlace.nombre;
                                enlace.TipoEnlace = exi.EnlaceComunicaciones.TipoEnlace.nombre;
                                enlace.wan = exi.EnlaceComunicaciones.wan;
                                enlaces.Add(enlace);
                            }
                        incidencia.EquiposDecorator.enlaces = enlaces;                    
                        
                    }//enlaces
                    if (incidencia.EquiposDecorator == null) incidencia.EquiposDecorator = new EquipoEnIncidenciaNull();
                }
            }
        }       
        internal Incidencia getResult()
        {
            if (incidencia.AreasActuantes != null) incidencia.AreasActuantes.Sort();
            return incidencia;
        }

        //internal void Construir(string nroIncidente, bool movimientoAreas)
        //{
        //    using (var dbc = new IncidenciasEntities())
        //    {
        //        var incid = (from u in dbc.IncidenciasActivas
        //                     where u.Incidencias.numero==nroIncidente
        //                     select u).Single();
        //        if (incid != null)
        //        {
        //            incidencia = new Incidencia
        //            {
        //                Numero = incid.Incidencias.numero,
        //                Id = incid.id_incidencia,
        //                AreaActual = AreaIncidenciaFactory.getArea(incid.area.id_area),
        //                EstadoActual = EstadoIncidenciaFactory.getEstado(incid.id_estado)
        //            };
        //            if (incidencia.AreaActual.NombreDireccion == "Soporte Técnico")
        //            {
        //                incidencia.EquiposDecorator = new EquiposEnIncidenciaSoporte(incidencia);
        //            }
        //            else
        //            {
        //                incidencia.EquiposDecorator = new EquipoEnIncidenciaNull(incidencia);
        //            }
        //        }
        //        if (movimientoAreas)
        //        {
        //            incidencia.AreasActuantes = new List<MovAreaIncidencia>();
        //            var mov = (from u in dbc.AreaXIncidencia
        //                       where u.id_incidencia == incidencia.Id
        //                       select u).ToList();
        //            foreach (var v in mov)
        //            {
        //                DateTime fechaHastaParsed = (v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
        //                List<ResolucionesIncidencias> resoluciones = (from s in dbc.Resoluciones
        //                                                              where s.id_incidencia == incidencia.Id && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
        //                                                              //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
        //                                                              select new ResolucionesIncidencias
        //                                                              {
        //                                                                  Fecha = s.fecha,
        //                                                                  Equipo = s.UnidadConf.nombre,
        //                                                                  Observaciones = s.observaciones,
        //                                                                  Tecnico = s.tecnico.descripcion,
        //                                                                  TipoResolucion = s.tipo_resolucion.descripcion,
        //                                                                  Turno = s.turno.descripcion
        //                                                              }).ToList();
        //                MovAreaIncidencia mar = new MovAreaIncidencia
        //                {
        //                    Area = AreaIncidenciaFactory.getArea(v.id_area),
        //                    FechaDesde = v.fecha_inicio,
        //                    FechaHasta = v.fecha_fin,
        //                    Resoluciones = resoluciones
        //                };
        //                incidencia.AreasActuantes.Add(mar);
        //            }
        //        }
        //    }
        //}//metodo
    }
}