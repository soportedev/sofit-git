﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Models
{
    public class DireccionHistoricoModel
    {        
        public List<DireccionVista> Direcciones { get; set; }        
        public List<TipoTicketVista> TiposTicket { get; set; }
        public List<PrioridadesTicketVista> PrioridadesTicket { get; set; }
        public List<ServiciosVista> Servicios { get; set; }
        public List<JurisdiccionVista> Jurisdicciones { get; set; }
    }
}