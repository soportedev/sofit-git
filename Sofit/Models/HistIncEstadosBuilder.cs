﻿using soporte.Areas.Soporte.Models;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Helpers;
using soporte.Areas.Soporte.Controllers;
using Sofit.Models.ClasesVistas;

namespace Sofit.Models
{
    public class HistIncEstadosBuilder
    {
        List<HistoricoEstados> estadosHistorial = new List<HistoricoEstados>();

        internal void Construir(string nroInc)
        {
            using (var dbc = new IncidenciasEntities())
            {
                var incidencia = (from u in dbc.Incidencias
                                 where u.numero == nroInc
                                 select u).Single();
                if (incidencia != null)
                {
                    estadosHistorial = (from v in dbc.EstadoXIncidencia
                                        where v.id_incidencia == incidencia.id_incidencias
                                        orderby v.fecha_inicio ascending
                                        select new HistoricoEstados
                                        {
                                            //TecnicoNombre = (from t in dbc.tecnico
                                            //                 where t.id_tecnico == v.id_usuario
                                            //                 select t.descripcion).FirstOrDefault(),
                                            TecnicoNombre = v.Usuarios.nombre_completo,
                                            Estado = v.Estado_incidente.descripcion,
                                            FechaInicio = v.fecha_inicio,
                                            FechaFin = v.fecha_fin ?? DateTime.MinValue
                                        }).ToList();
                }
            }
        }

        internal List<HistoricoEstados> getResult()
        {
            if (estadosHistorial != null)
            {
                return estadosHistorial;
            }
            else
            {
                return null;
            }
        }
    }
}
