﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Direccion.Models
{
    public class TiemposDireccion
    {
        public int tCriticoIncidente { get; set; }
        public int tMaxSuspension { get; set; }
        public TiemposDireccion()
        {
            using(var dbc=new IncidenciasEntities())
            {
                tCriticoIncidente = dbc.TiemposGral.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                tMaxSuspension = dbc.TiemposGral.Where(c => c.nombre == "MaxSuspension").Single().duracion;
            }
        }
    }
}