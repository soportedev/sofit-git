﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Direccion.Models
{
    public class DireccionModel
    {        
        public List<IncTeleVista> IncidentesTotales { get; set; }
        public List<TipoResolucion> Resoluciones { get; set; }
        public List<Tecnico> Tecnicos { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<DireccionVista> Direcciones { get; set; }
    }
}