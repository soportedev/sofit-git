﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Direccion.Models
{
    public class PerfilDireccion:Perfil
    {
        public PerfilDireccion()
        {
            this.nombre = "Direccion";
            this.nombreBase = "Dirección";
        }

        public override Constantes.AccesosPagina getAcceso(string page)
        {
            return Constantes.AccesosPagina.Full;
        }

        public override string getHomePage()
        {
            return "Direccion/Incidentes";
        }        
    }
}