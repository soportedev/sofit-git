﻿using Sofit.DataAccess;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Direccion.Controllers
{
    [SessionActionFilter]
    public class HistoricosController : Controller
    {
        //
        // GET: /Direccion/Historicos/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Direccion/Historicos");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            ModelHistoricos model = new ModelHistoricos();           
            List<DireccionVista> direcciones = new List<DireccionVista>();
            List<TipoTicketVista> tiposTicket = new List<TipoTicketVista>();
            List<PrioridadesTicketVista> prioridadesTicket = new List<PrioridadesTicketVista>();
            List<ServiciosVista> servicios = new List<ServiciosVista>();
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            using (var dbc = new IncidenciasEntities())
            {
                servicios = (from u in dbc.Servicios
                             select new ServiciosVista
                             {
                                 Id = u.id_servicio,
                                 Nombre = u.nombre
                             }).ToList();
                tiposTicket = (from u in dbc.TipoTicket
                               select new TipoTicketVista
                               {
                                   Id = u.id,
                                   Nombre = u.nombre
                               }).ToList();
                prioridadesTicket = (from u in dbc.PrioridadTicket
                                     select new PrioridadesTicketVista
                                     {
                                         Id = u.id,
                                         Nombre = u.nombre
                                     }).ToList();                
                direcciones = (from u in dbc.Direcciones
                               select new DireccionVista
                               {
                                   idDireccion = u.id_direccion,
                                   Nombre = u.nombre
                               }).ToList();
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                tecnicos = dbc.tecnico.Where(c => c.activo.Value == true).Select(c => new Tecnico { idTecnico = c.id_tecnico, Nombre = c.descripcion }).ToList();
            }
            model.Servicios = servicios;            
            model.Direcciones = direcciones;            
            model.TiposTicket = tiposTicket;
            model.PrioridadesTicket = prioridadesTicket;
            model.Jurisdicciones = jurisdicciones;
            model.Tecnicos = tecnicos;
            return View(model);
        }

    }
}
