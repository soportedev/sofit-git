﻿using Sofit.DataAccess;
using soporte.Areas.Direccion.Models;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Direccion.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        //
        // GET: /Direccion/Incidentes/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Direccion/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            DireccionModel model = new DireccionModel();
            TiemposDireccion tiempos = new TiemposDireccion();
            List<IncTeleVista> incTotales = new List<IncTeleVista>();
            List<TipoResolucion> resoluciones = new List<TipoResolucion>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Turno> turnos = new List<Turno>();
            List<DireccionVista> direcciones = new List<DireccionVista>();
            using (var dbc = new IncidenciasEntities())
            {
                incTotales = (from d in dbc.selectIncidenciasActivas(0)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value
                              }).ToList();              
                resoluciones = (from us in dbc.tipo_resolucion
                                 where us.area.descripcion == "Mesa de Ayuda" & us.descripcion == "Seguimiento"
                                 orderby us.descripcion
                                 select new TipoResolucion
                                 {
                                     idTipoResolucion = us.id_tipo_resolucion,
                                     Nombre = us.descripcion
                                 }).ToList();
                tecnicos = (from us in dbc.tecnico
                            where !us.descripcion.StartsWith("Admin") & us.activo.Value & us.area.Direcciones.nombre=="Gerencia"
                            select new Tecnico
                            {
                             idTecnico=us.id_tecnico,
                             Nombre=us.descripcion
                            }).ToList();
                turnos = (from us in dbc.turno
                          select new Turno
                          {
                              idTurno = us.id_turno,
                              Nombre = us.descripcion
                          }).ToList();
                direcciones = (from us in dbc.Direcciones
                               where !(us.nombre=="Gerencia")
                               select new DireccionVista
                               {
                                   idDireccion = us.id_direccion,
                                   Nombre = us.nombre
                               }).ToList();
            }

            foreach (IncTeleVista t in incTotales)
            {
                t.calcularEstadoServicioYSuspensión(tiempos.tCriticoIncidente,tiempos.tMaxSuspension);
            }
            model.IncidentesTotales = incTotales;
            model.Resoluciones = resoluciones;
            model.Tecnicos = tecnicos;
            model.Turnos = turnos;
            model.Direcciones = direcciones;
            return View(model);            
        }
        public JsonResult cargaTrabajo()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            //string oblea = nvc.Get("oblea");
            string nroIncidente = nvc.Get("nroIncidente");
            int tResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int tecnico = Int32.Parse(nvc.Get("idTecnico"));
            int turno = Int32.Parse(nvc.Get("idTurno"));
            int idIncidente = Int32.Parse(nvc.Get("idIncidente"));            
            string observacion = nvc.Get("observaciones");            
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente,false,false,false);
            Incidencia inc = builder.getResult();
            string nombreImagen = string.Empty;
            bool nosobrescribir = false;
            inc.cargarSolucion(usuario.IdUsuario, null, tResolucion, tecnico, turno, observacion, DateTime.Now, nombreImagen, nosobrescribir);
                    
            result.Info = "ok";           
            return Json(result);
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.numero.StartsWith(prefixText)
                         select u.Incidencias.numero).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var tecnico = (from u in dbc.tecnico
                                   where u.descripcion == Tecnico
                                   select u).SingleOrDefault();
                    foreach (string strId in idIncidencia)
                    {
                        int idIncidente = Int32.Parse(strId);
                        var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                        incActiva.id_tecnico = tecnico.id_tecnico;
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult buscarEqOInc(string texto)
        {
            DatosUbicacion resp = new DatosUbicacion();

            using (var dbc = new IncidenciasEntities())
            {
                resp = (from u in dbc.IncidenciasActivas
                        where u.Incidencias.numero == texto
                        select new DatosUbicacion
                        {
                            Area = u.area.Direcciones.nombre,
                            Estado = u.Estado_incidente.descripcion,
                            Numero = u.Incidencias.numero,
                            Tipo = "Incidente"
                        }).FirstOrDefault();

            }
            if (resp == null) resp = new DatosUbicacion();
            return Json(resp);
        }
        public ActionResult getExcel(string win)
        {
            switch (win)
            {
                case "TotalesDireccion":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var incTotales = (from d in dbc.selectIncidenciasActivas(0)
                                      select new IncTeleVista
                                      {
                                          IdIncidente=d.id,
                                          Numero = d.numero,
                                          FechaDesde = d.fechaI,
                                          Tipificacion = d.tipificacion,
                                          Dependencia = d.dependencia,
                                          Técnico = d.tecnico,
                                          Descripcion = d.observaciones,                
                                          idArea=d.idarea,
                                          Servicio = d.nombreServicio,
                                          TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                          sTolerance = d.tolerance,
                                          Prioridad = d.prioridad,
                                          Cliente = d.Cliente,
                                          Estado = d.Estado,                                          
                                          TieneNotificacion = d.noti.Value,
                                          FueSuspendido = d.fuesuspendido.Value
                                      }).ToList();
                        foreach (IncTeleVista t in incTotales)
                        {
                            t.calcularEstadoServicioYSuspensión(24,480);
                        }
                        DataTable table = DataTableConverter.ToDataTable(incTotales);
                        Session["tableSource"] = table;

                    }
                    break;                
            }
            return RedirectToRoute("Excel");
        }
    }
}
