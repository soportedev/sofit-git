﻿$(function () {
    $("#Trabajos2").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '109px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);            
        },
        buttons: {
            Aceptar: {
                id: 'bTrabajos',
                text: 'Aceptar',
                click: function () {
                    var datos = $(this).data('datos');
                    var context = $(this);
                    var tresolucion = false;
                    var idTipoResolucion = $('select.tipoSolucion option:selected', context).val();
                    tresolucion = idTipoResolucion == 0;
                    if (tresolucion) {
                        $('#Trabajos2').find('.message').html('Seleccione el tipo de Resolución.');
                        $('.error, #Trabajos2').show();
                        return;
                    }
                    var obs = $('.tSolucion', context);
                    if ($.trim(obs.val()).length < 10) {
                        $('#Trabajos2').find('.message').html('Complete el campo de Observaciones');
                        $('.error, #Trabajos2').show();
                        return;
                    }
                    datos.idTecnico = $('.dTecnicos option:selected', context).val();
                    datos.idTurno = $('.dTurnos', context).val();
                    datos.observaciones = $('.tSolucion', context).val();
                    datos.idTipoResolucion = idTipoResolucion;
                    $('#Trabajos2').appendSerialToObject(datos);
                    $.ajax({
                        url: 'Incidentes/cargaTrabajo',
                        data: datos,
                        type: 'POST',
                        datatype: 'json',
                        beforeSend: function () {
                            ajaxReady = false;
                        },
                        success: function (data) {
                            ajaxReady = true;
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                $("#Trabajos2").dialog('close').find('#observaciones').val('');
                                mostrarExito('Se ha cargado la solucion');
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        }
                    });
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $('li.trabajos2').on('click', function () {
        var nro = [];
        var trs = {};
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("Seleccione UN elemento");
        }
        else {
            var $trabajosDialog = $("#Trabajos2");
            clearTrabajosPanel($trabajosDialog);
            var datosArray = nro[0].find('td').slice(1, 7);
            var conforms = true;
            var oblea, idIncidente, nroIncidente, idEquipo;
            idIncidente = datosArray[0].innerHTML;
            var datos = {};
            datos.idIncidente = idIncidente;
            datos.nroIncidente = $(datosArray[1]).find('span').text();
            datos.idEquipo = idEquipo;
            datos.oblea = oblea;
            datos.nombreGrilla = nombreGrilla;
            $trabajosDialog.data('datos', datos).dialog('open');
        }
    });
    $('select.sDireccion').on('change', function () {
        var idDir = $('option:selected', this).val();
        var selectTecs = $(this).parents('ul.fly').find('select.sTecnico');
        if (idDir == 0) {
            mostrarError('Seleccione la Subdirección!!');
            var option = $('<option>').attr('value', 0).text('--Seleccione--');
            selectTecs.html(option);
            return;
        }
        $.post('/Service/TecnicosXDir', {'idDir':idDir}, function (data) {
            selectTecs.html('');
            $.each(data, function (i, v) {
                var option = $('<option>').attr('value', v.Id).text(v.Nombre);
                selectTecs.append(option);
            });
        });
    });
})
function ubicarIncidente(data) {
    doScroll(ventanas[0], data.Numero);  
}
function searchAutocomplete(request, response) {
    var texto = request.term;
    texto = texto.toUpperCase();
    var uri = 'GetIncidentesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR EQUIPOS O INCIDENTES*/
function buscarEquiposInc(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    resString = resString.toUpperCase();

    if (!regularEquipo.test(resString) && !regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }
    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/buscarEqOInc",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("El equipo/incidente no está activo");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {                        
                        default: doScroll(ventanas[0], data.Numero);
                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
/**Search Autocomplete Cliente**/
function searchAutocompleteCliente(request, response) {
    var texto = request.term;
    var uri = 'GetClientesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/**TRAER INCIDENTES A LA VIDA**/
function incidenteToLife() {
    
}
function notificar() {

}
function derivar() {
}
function cargarTrabajos() {
    
}
function derivarEq() {
}
function cerrarIncidente() {
    
}
function tomarEquipo() {

}
function terminarEquipo() {

}
function asignarEquipo() {


}
function actualizarPaneles(tabla) {
    $.ajax({
        url: "Incidentes",
        type: 'GET',
        beforeSend: function () {
            ajaxReady = false;
        },
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var results = new Array();
            results[0] = $(data).find('#gITotales');
            switch (tabla) {
                case "todos":
                    ventanas[0].update(results[0]);
                    break;
                default:;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarFinSesion();
        },
        complete: function () {
            ajaxReady = true;
            $('p.ajaxGif').hide();
        }
    });
}
function notificarIncidente() { }