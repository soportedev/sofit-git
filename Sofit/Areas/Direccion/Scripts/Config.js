﻿$(function () {   
    $('#tabset').on('click', '.password', function () {
        var tr = $(this).parents('tr');
        var nro = tr.find('td:eq(0)').text();
        var nombre = tr.find('td:eq(1)').text();
        var titulo = $("#PasswordChanger").siblings('.ui-dialog-titlebar').find('span');
        titulo.html('Cambio de Password para: ' + nombre);
        $("#PasswordChanger").dialog('open').data('datos', { 'nro': nro, 'nombre': nombre });
    });    
});//   READY
function bindsOnLoad(event, ui) {
    $('button').button();

    switch (ui.panel[0].id) {
        case 'ui-tabs-1':
            var personal = $('#idTemaPers').val();
            setSelectByVal($('#themeSwitcher'), personal);
            $('#cambioPassword').button({ disabled: true });
            $('#themeSwitcher').trigger('change');
            break;
        case 'ui-tabs-3':
            setUpDialogs();
            break;

    }//fin switch
}//find binds--------------------------------- >
function actualizarTiempos(e) {
    e.preventDefault();
    var tAvisoClon = $('#tAvisoClon').val();
    var tEmerClon = $('#tEmergClon').val();
    var tAvisoLab = $('#tAvisoLab').val();
    var tEmerLab = $('#tEmergLab').val();
    var tAvisoEntrega = $('#tAvisoDel').val();
    var tEmerEntrega = $('#tEmergDel').val();
    var tAvisoSus = $('#tAvisoSus').val();
    var tEmerSus = $('#tEmergSus').val();
    var tAvisoAvisoGar = $('#tAvisoAvisoGar').val();
    var tAvisoEnGar = $('#tAvisoEnGar').val();
    var tEmerAvisoGar = $('#tEmergAvisoGar').val();
    var tAvisoEnEmer = $('#tAvisoEnGar').val();
    var tEmerEnEmer = $('#tEmerEnGar').val();
    if (regExTest(regTime, tAvisoClon) && regExTest(regTime, tEmerClon) && regExTest(regTime, tAvisoLab)
    && regExTest(regTime, tAvisoSus) && regExTest(regTime, tEmerSus) &&
    regExTest(regTime, tEmerLab) && regExTest(regTime, tAvisoEntrega) && regExTest(regTime, tEmerEntrega)
    && regExTest(regTime, tAvisoAvisoGar) && regExTest(regTime, tAvisoEnEmer) && regExTest(regTime, tEmerEnEmer)
    ) {
        var datos = {};
        datos.tAClon = tAvisoClon;
        datos.tALab = tAvisoLab;
        datos.tADel = tAvisoEntrega;
        datos.tASus = tAvisoSus;
        datos.tAAGar = tAvisoAvisoGar;
        datos.tAEGar = tAvisoEnGar;
        datos.tEClon = tEmerClon;
        datos.tELab = tEmerLab;
        datos.tEDel = tEmerEntrega;
        datos.tESus = tEmerSus;
        datos.tEAGar = tEmerAvisoGar;
        datos.tEEGar = tEmerEnEmer;
        $.ajax({
            url: "Config/setTiempos",
            type: 'POST',
            data: datos,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Tiempos Actualizados');
                }
                else {
                    mostrarError(data.Detail);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError)
            }
        });
    }
    else {
        mostrarError("Verifique los datos");
    }
}
function getTiempos() {
    $.ajax({
        url: "../negocio/handlers/getTiempos.ashx",
        datatype: 'json',
        beforeSend: function () {

        },
        success: function (data) {
            if (data.data == 'out') {
                mostrarFinSesion();
                return;
            }
            else {
                $('#tAvisoClon').val(data.tAClon);
                $('#tEmergClon').val(data.tEClon);
                $('#tAvisoLab').val(data.tALab);
                $('#tEmergLab').val(data.tELab);
                $('#tAvisoLog').val(data.tALog);
                $('#tEmerLog').val(data.tELog);
                $('#tAvisoSus').val(data.tASus);
                $('#tEmergSus').val(data.tESus);
                $('#tAvisoDel').val(data.tADel);
                $('#tEmergDel').val(data.tEDel);
                $('#tAvisoIAt').val(data.tAIncAt);
                $('#tEmerIAt').val(data.tEIncAt);
                $('#tAvisoAvisoGar').val(data.tAAGar);
                $('#tEmergAvisoGar').val(data.tEAGar);
                $('#tAvisoEnGar').val(data.tAEGar);
                $('#tEmerEnGar').val(data.tEEGar);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        },
        complete: function () {
            $('.ajaxGif').css({ 'display': 'none' });
        }
    });
}
function deleteEquipo() { }
function bindDatosPers() { }
