﻿using System.Web.Mvc;

namespace soporte.Areas.Direccion
{
    public class DireccionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Direccion";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "lineabasedireccion",
                url: "Direccion/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiodir",
                url: "Direccion/Cambios/{startIndex}",
                defaults: new { controller = "Cambios", action = "Index", startIndex=0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8dir",
                url: "Direccion/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Direccion_default",
                "Direccion/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
