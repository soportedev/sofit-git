﻿// Inicio
var Nice = false; // Objeto para niceScroll
$(function () {
	// NiceScroll
	window._selfNice = $('.scrolling'); // Contenedor para niceScroll
	initNiceScroll(_selfNice); // Ejecuta niceSroll al inicio

	_selfNice.mouseenter(function () { // Activa niceScroll cuando el mouse entra en el contenedor
		initNiceScroll(_selfNice);
	}).mouseleave(function () { // Desactiva niceScroll cuando el mouse sale del contenedor
		Nice = false;
	});

	$('#accordion').accordion({
		//active: false,
		collapsible: true,
		heightStyle: 'content'
	});
	$('.ui-button').button();

	$('.accordion-content').on('click', '.agregar-accion', function (e) {
		var data = {};
		//data.idHallazgo = e.currentTarget.dataset.idhallazgo; // usando el id en el boton
		data.idHallazgo = $(this).parents('.accordion-content').attr('id');
		AgregarAccion(data);
	});
});

function initNiceScroll(_selfNice) // Crea el niceScroll
{
	Nice = _selfNice.niceScroll({
		background: "#7CD569",
		cursorcolor: "#24890D",
		cursorwidth: "10px",
		cursorborder: "0",
		cursorborderradius: "2px",
		horizrailenabled: false,
		enabletranslate3d: true,
		railoffset: { top: 0, left: 10 },
		//autohidemode: false
	}).resize();
}

function AgregarAccion(data)
{
	// Vars
	var idHallazgo = '#'+data.idHallazgo;
	var accionesCount = 0;

	// Cuenta acciones
	$('[data-secciontipo="accion"]', idHallazgo).each(function (i) {
		//console.log($(this), i);
		accionesCount++;
	});

	//console.log(accionesCount);

	$.ajax({
		url: '/Gs/Shared/Accion',
		dataType: 'html',
		async: false,
		success: function (html) {
			var $html = $(html);
			$html[0].id = data.idHallazgo + 'a' + accionesCount;
			$(".acciones-extra").append($html);
		},
		error: function (err1, err2, err3) {
			
		}
	});
}

function AutoComplete(objectID, url)
{
	this.objectID = objectID;
	this.url = url;
	this.combo = {};
}

AutoComplete.prototype.ajax = function ()
{
	$.ajax({
		url: this.url,
	})
}