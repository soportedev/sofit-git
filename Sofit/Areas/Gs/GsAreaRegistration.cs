﻿using System.Web.Mvc;

namespace Sofit.Areas.Gs.Controllers
{
    public class GsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Gs";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Gs_default",
                "Gs/{controller}/{action}/{id}",
                new { controller = "Gs/Hallazgos", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                name: "AccionGS",
                url: "Gs/{controller}/Accion",
                defaults: new { controller = "Shared", action = "Accion" },
                namespaces: new[] { "Sofit.Areas.Gs.Controllers" }
            );
        }
    }
}