﻿using Sofit.DataAccess;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Mesa.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        //
        // GET: /Mesa/Incidentes/
        
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Mesa/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            MesaModel model = new MesaModel();
            List<IncTeleVista> incTotales = new List<IncTeleVista>();
            List<IncTeleVista> incIngresados = new List<IncTeleVista>();
            List<IncTeleVista> incSuspendidos = new List<IncTeleVista>();            
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<DireccionVista> subdir = new List<DireccionVista>();
            List<TipoResolucion> resoluciones = new List<TipoResolucion>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Area> areas = new List<Area>();
            List<TipoResolucion> resoluciones2 = new List<TipoResolucion>();            
            List<Turno> turnos = new List<Turno>();
            List<TipoEquipoVista> tipoEquipo = new List<TipoEquipoVista>();
            IDirecciones mesaDir = DireccionesFactory.getDireccion("Mesa de Ayuda");
            soporte.Areas.Mesa.Models.TiemposMesa tiempos = new soporte.Areas.Mesa.Models.TiemposMesa();
            using (var dbc = new IncidenciasEntities())
            {
                incTotales = (from d in dbc.selectIncidenciasActivas(mesaDir.IdDireccion)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value
                              }).ToList();                
                resoluciones = (from us in dbc.tipo_resolucion
                                where us.area.descripcion == "Mesa de Ayuda"
                                orderby us.descripcion
                                select new TipoResolucion
                                {
                                    idTipoResolucion = us.id_tipo_resolucion,
                                    Nombre = us.descripcion
                                }).ToList();
                resoluciones2 = (from us in dbc.tipo_resolucion
                                where us.area.descripcion == "Mesa de Ayuda" & us.descripcion=="Seguimiento"
                                orderby us.descripcion
                                select new TipoResolucion
                                {
                                    idTipoResolucion = us.id_tipo_resolucion,
                                    Nombre = us.descripcion
                                }).ToList();
                incIngresados = incTotales.Where(c => c.Estado!="Suspendido").ToList();
                incSuspendidos = incTotales.Where(c => c.Estado == "Suspendido").ToList();

                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Mesa de Ayuda"
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Mesa de Ayuda" & u.activo.Value & u.id_usuario.HasValue
                            orderby u.descripcion
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
                turnos = (from u in dbc.turno
                          select new Turno
                          {
                              idTurno = u.id_turno,
                              Nombre = u.descripcion
                          }).ToList();               
                subdir = (from u in dbc.Direcciones
                          select new DireccionVista
                          {
                              idDireccion = u.id_direccion,
                              Nombre = u.nombre
                          }).ToList();
                tipoEquipo = (from u in dbc.Tipo_equipo
                              select new TipoEquipoVista
                              {
                                  Id = u.id_tipo,
                                  Nombre = u.descripcion
                              }).ToList();
            }
            foreach (IncTeleVista t in incIngresados)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
                
            }
            //foreach(IncTeleVista t in incTotales)
            //{
            //    t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            //}
            foreach(IncTeleVista t in incSuspendidos)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            model.TiposResolucion = resoluciones;
            model.TiposResolucion2 = resoluciones2;
            //model.IncidentesTotales = incTotales;
            model.IncidentesIngresados = incIngresados;
            model.IncidentesSuspendidos = incSuspendidos;
            model.Turnos = turnos;
            model.SubDirecciones = subdir;
            model.Areas = areas.Where(u => u.Nombre != "Telecomunicaciones").ToList();
            model.Tecnicos = tecnicos;
            model.Jurisdicciones = jurisdicciones;
            model.TiposEquipo = tipoEquipo;
            return View(model);
        }
        public JsonResult GetClientesActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.Contactos.nombre.StartsWith(prefixText)|u.Incidencias.Contactos.dni.StartsWith(prefixText)
                         select u.Incidencias.numero).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult incidenteXCliente(string texto)
        {
            List<DatosUbicacion> resp = new List<DatosUbicacion>();
            Regex pattern = new Regex(@"(?<dni>^\d+)");
            Match match = pattern.Match(texto);
            if (match.Success)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.Contactos.dni.StartsWith(texto)
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente",
                                Fecha = u.Incidencias.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Jurisdiccion = u.Incidencias.Jurisdiccion.descripcion
                            }).Take(5).ToList();
                }      
            }
            else
            {
                using (var dbc = new IncidenciasEntities())
                {
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.Contactos.nombre.StartsWith(texto)
                            select new DatosUbicacion
                            {
                                Area = u.area.Direcciones.nombre,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente"
                            }).Take(5).ToList();
                }      
            }
                  
            return Json(resp);
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.numero.StartsWith(prefixText)
                         select u.Incidencias.numero).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult buscarEqOInc(string texto)
        {
            DatosUbicacion resp = new DatosUbicacion();

            using (var dbc = new IncidenciasEntities())
            {                
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.numero == texto
                            select new DatosUbicacion
                            {
                                Area = u.area.Direcciones.nombre,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente"
                            }).FirstOrDefault();      
                   
            }
            if (resp == null) resp = new DatosUbicacion();
            return Json(resp);
        }
        public JsonResult cargaTrabajo(HttpPostedFileBase fileupload)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            //string oblea = nvc.Get("oblea");
            string nroIncidente = nvc.Get("nroIncidente");
            int tResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int tecnico = Int32.Parse(nvc.Get("idTecnico"));
            int turno = Int32.Parse(nvc.Get("idTurno"));
            int idIncidente = Int32.Parse(nvc.Get("idIncidente"));
            //int idEquipo = 0;
            //Int32.TryParse(nvc.Get("idEquipo"), out idEquipo);
            string observacion = nvc.Get("observaciones");
            string nombreImagen = string.Empty;
            //sobrescribir ultima
            bool sobrescribir = false;
            sobrescribir = Boolean.Parse(nvc.Get("sobrescribir"));
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente, sobrescribir, false, false);
            //            
            Incidencia inc = builder.getResult();
            bool hayImagen = fileupload != null;
            string nombreFile = string.Empty;            
            #region foto
            if (hayImagen)
            {
                string pathImagen = string.Empty;
                string nombreArchivo = fileupload.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string ran = new Random().Next(10000).ToString("0000");
                nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getResolucionCustom(nombreFile);
                    var stream = fileupload.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion            
            //buscar equipos
            bool flag = true;
            List<int> idEquipos = new List<int>();
            bool esParaEquipo = Boolean.Parse(nvc.Get("esParaEquipo"));
            if (esParaEquipo)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("idEquipo")) idEquipos.Add(Int32.Parse(nvc.GetValues(key)[0]));
                }
                if (idEquipos.Count == 0)
                {
                    result.Info = "error";
                    result.Detail = "No se cargaron las obleas del equipo.";
                    flag = false;
                }
                if (flag)
                {
                    foreach (int id in idEquipos)
                    {
                        inc.cargarSolucion(usuario.IdUsuario, id, tResolucion, tecnico, turno, observacion, DateTime.Now, nombreFile, sobrescribir);
                    }
                    result.Info = "ok";
                }
            }
            else
            {
                result = inc.cargarSolucion(usuario.IdUsuario, null, tResolucion, tecnico, turno, observacion, DateTime.Now, nombreFile, sobrescribir);
            }
            return Json(result);
        }
        public JsonResult eliminarIncidente(int idInc)
        {
            Responses result = new Responses();
            IncidenciasManager mgr = new IncidenciasManager();
            result = mgr.eliminarIncidente(idInc);
            return Json(result);
        }
        public JsonResult solucionarIncidente()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, true,true,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.cerrarIncidencia(idUsuario);

            return Json(result);
        }
        public JsonResult incidentesToLife(string nroInc)
        {
            IncidenciasManager im = new IncidenciasManager();
            IDirecciones soporte = DireccionesFactory.getDireccion("Mesa de Ayuda");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            return Json(im.reanimarIncidencia(nroInc, idUsuario, soporte));
        }
        public JsonResult suspenderIncidentes(int idInc)
        {
            Responses result = new Responses();
            int idUsuarioActual = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.suspenderIncidencia(idUsuarioActual);
            return Json(result);
        
        }
        public JsonResult activarIncidentes()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, false,true,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.reactivarTicket(id_inc, idUsuario);
            return Json(result);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var tecnico = (from u in dbc.tecnico
                                   where u.descripcion == Tecnico
                                   select u).SingleOrDefault();
                    foreach (string strId in idIncidencia)
                    {
                        int idIncidente = Int32.Parse(strId);
                        var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                        incActiva.id_tecnico = tecnico.id_tecnico;
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult derivarADireccion(int idDir, int idInc)
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false, false);
            Incidencia inc = builder.getResult();
            result = inc.derivarIncidenciaADireccion(idDir, idUsuario);
            return Json(result);
        }
        
    }
}
