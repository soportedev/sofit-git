﻿using Sofit.DataAccess;
using soporte.Areas.Infraestructura.Models.ClasesVista;
using soporte.Areas.Operaciones.Models.ClasesVista;
using soporte.Areas.Soporte.Models.Displays;
using soporte.Areas.Telecomunicaciones.Models.ClasesVistas;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Mesa.Controllers
{
    [SessionActionFilter]
    public class InventarioController : Controller
    {
        //
        // GET: /Mesa/Inventario/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Mesa/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            else
            {
                return View();
            }            
        }
        public JsonResult EquipoXOblea(string prefixText)
        {
            Responses result = new Responses();
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre
                         }).Take(10).ToList();
            }
            return Json(items);            
        }
        public JsonResult GetEquipo(string oblea)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        //soporte
                        case "Pc": equipo = new PcInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Impresora": equipo = new ImpresoraInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Monitor": equipo = new MonitorInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Varios": equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Grupo Electrogeno": equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "UPS": equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Rack": equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //operaciones
                        case "Enlace": equipo = new EnlaceInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //infraestructura
                        case "Servidor Físico": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Workstation Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Storage": equipo = new StorageInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster": equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        //telecomunicaciones
                        default: equipo = new EquipoTelecomunicaciones();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);    
        }
    }
}
