﻿using Sofit.DataAccess;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Mesa.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {
        //
        // GET: /Mesa/Config/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Mesa/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            else
            {
                return View();
            }
        }
        public ActionResult Usuarios()
        {
            usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
            Constantes.AccesosPagina acceso = usuario.getAccess("Mesa/Usuarios");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesMesa();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getUsuariosMesa();
                return PartialView("Usuarios",model);
            }
        }        
       
        public JsonResult quitarServicio(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var serv = dbc.Servicios.Where(c => c.id_servicio == id).Single();
                    dbc.Servicios.Remove(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = "El servicio está siendo utilizado";
            }            
            return Json(result);
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas, int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Mesa de Ayuda");
            using (var dbc = new IncidenciasEntities())
            {
                var encontrado = dbc.Servicios.Where(c => c.nombre == nombre);
                if (encontrado.Count() > 0)
                {
                    result.Detail = "Ya existe un Servicio con ese nombre, en la Subdireccion " + encontrado.First().Direcciones.nombre;
                }
                else
                {
                    var serv = new Servicios
                    {
                        nombre = nombre,
                        tiempo_res = (int)totales.TotalHours,
                        tiempo_margen=(int)totalesMargen.TotalHours,
                        id_direccion = direccion.IdDireccion,
                        tolerancia=80,
                        vigente = true
                    };
                    dbc.Servicios.Add(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            return Json(result);
        }
        public ActionResult Tipificaciones()
        {
            usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
            Constantes.AccesosPagina acceso = usuario.getAccess("Mesa/Tipificaciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre=="Mesa de Ayuda"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Mesa de Ayuda");
                model.Tipificaciones = arbol.getResult();
                return View(model);
            }
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t = (from u in dbc.Tipificaciones
                             where u.id_tipificacion == id
                             select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre, int idServicio)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Mesa de Ayuda" select u).Single();
                    var encontrada = dbc.Tipificaciones.Where(c => c.nombre == nombre & c.id_padre == null & idPadre == null).FirstOrDefault();
                    if (encontrada != null)
                    {
                        result.Detail = "Ya existe esa Tipificación Padre en la subdirección: " + encontrada.Direcciones.nombre;
                    }
                    else
                    {
                        Tipificaciones nueva = new Tipificaciones()
                        {
                            nombre = nombre,
                            descripcion = desc,
                            id_padre = idPadre,
                            id_servicio = idServicio == 0 ? (int?)null : idServicio,
                            id_direccion = direccionSoporte.id_direccion
                        };
                        dbc.Tipificaciones.Add(nueva);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoUsuario(string login, string nombre,string p1, int perfil)
        {
            IDirecciones mesa = DireccionesFactory.getDireccion("Mesa de Ayuda");
            Responses result = new Responses();            
            //usuarioHandler user = new usuarioHandler();
            //result = user.nvoUsuario(login, nombre, p1, perfil, mesa);
            return Json(result);            
        }
    }
}
