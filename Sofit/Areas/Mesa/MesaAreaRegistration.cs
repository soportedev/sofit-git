﻿using System.Web.Mvc;

namespace soporte.Areas.Mesa
{
    public class MesaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Mesa";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "lineabasemesa",
                url: "Mesa/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiomesa",
                url: "Mesa/Cambios/{startIndex}",
                constraints: new { startIndex = @"\d+" },
                defaults: new { controller = "Cambios", action = "Index",startIndex=0 }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8mesa",
                url: "Mesa/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Mesa_default",
                "Mesa/{controller}/{action}/{id}",
                new { controller="Mesa/Incidentes", action = "Index", id = UrlParameter.Optional },
                new string[] { "soporte.Areas.Mesa.Controllers" }
            );
        }
    }
}
