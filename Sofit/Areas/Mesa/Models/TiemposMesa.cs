﻿using Sofit.DataAccess;
using System.Linq;

namespace soporte.Areas.Mesa.Models
{
    public class TiemposMesa
    {
        public int tCriticoIncidente { get; set; }
        public TiemposMesa()
        {
            using(var dbc=new IncidenciasEntities())
            {
                tCriticoIncidente = dbc.TiemposGral.Where(c => c.nombre == "IncidenteCritico").Single().duracion;                
            }
        }
    }
}