﻿using Sofit.DataAccess;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Linq;

namespace soporte.Areas.Mesa.Models
{
    public class AreaMesa:IAreaIncidencia
    {
        public AreaMesa(int idArea, string nombre)
        {
            base.IdArea = idArea;
            base.NombreArea = nombre;
        }
        public override Responses derivar(int idarea, int tecnico, int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override Responses solucionar(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo,DateTime actualizarFecha, string imagen)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    Resoluciones res = new Resoluciones
                    {
                        id_area = this.IdArea,
                        fecha = DateTime.Now,
                        id_uc = idEquipo,
                        id_tecnico = tecnico,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_tipo_resolucion = tResolucionUsuarios,
                        id_incidencia = this.Incidencia.Id,
                        path_imagen = (imagen != string.Empty) ? imagen : null
                    };
                    dbc.Resoluciones.Add(res);                                       
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e) { result.Detail = e.Message; }
            return result;
        }
        public override int getIdTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                idTecnico = (from u in dbc.tecnico
                             where u.descripcion == "Mesa de Ayuda"
                             select u.id_tecnico).First();
            }
            return idTecnico;
        }
    }
}