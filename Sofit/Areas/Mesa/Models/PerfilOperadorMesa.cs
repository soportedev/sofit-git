﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Mesa.Models
{
    public class PerfilOperadorMesa:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.ReadOnly;
            string[] res = page.Split('/');
            if (res[0].Equals("Mesa"))
            {
                switch(res[1])
                {
                    case "Incidentes":  acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Historicos": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Inventario": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Config": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "NuevoTicket": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Tipificaciones": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Usuarios": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Jurisdicciones": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    default: acceso = Constantes.AccesosPagina.Full;
                        break;
                }
            }            
            else
            {
                if (res[1].Equals("Inventario")) acceso = Constantes.AccesosPagina.ReadOnly;
                else acceso = Constantes.AccesosPagina.NoAccess;
            }
            return acceso;
        }

        public override string getHomePage()
        {
            return "Mesa/Incidentes";
        }
    }
}