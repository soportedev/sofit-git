﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Mesa.Models
{
    public class PerfilAdministradorMesa:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.ReadOnly;
            string[] res = page.Split('/');
            if (res[0].Equals("Mesa")|res[0].Equals("Config")) acceso = Constantes.AccesosPagina.Full;
            else
            {
                if (res[1].Equals("Inventario")) acceso = Constantes.AccesosPagina.ReadOnly;
                else acceso = Constantes.AccesosPagina.NoAccess;
            }
            return acceso;
        }

        public override string getHomePage()
        {
            return "Mesa/Incidentes";
        }
    }
}