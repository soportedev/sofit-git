﻿var index;
var $tabs;
$(function () {
    $('button').button();
    $('#searchButton').on('click', function () {
        var oblea = $('#tBuscar').val();
        GetEquipo(oblea);
    });
    $('#tBuscar, #Oblea').keyup(function (e) {
        var key = e.which;
        var val = $(this).val();
        if (key == 8 & val.length == 0) {
            return false;
        }
        $(this).val(this.value.toUpperCase())
    }).autocomplete({
        minLength: "2",
        source: searchAutocomplete,
        select: function (e, ui) {
            $('#tBuscar').val(ui.item.label);
            GetEquipo(ui.item.label);
            return false;
        },
        focus: function (e, ui) {
            $('#tBuscar').val(ui.item.label);
            return false;
        }
    });
    ClearForm();
});
function searchAutocomplete(request, response) {    
    var uri = 'Inventario/EquipoXOblea';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val.idEquipo;
                    obj.label = val.Oblea;                    
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
// GetEquipo
window.GetEquipo = function (oblea) {    
    $.ajax({
        data: { 'oblea': oblea },
        url: 'Inventario/GetEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();

            // create the notification
            var message, type;
            if (response['Info'] === 'No inicializado') {
                message = '<p>' +/*response['Info']+'<br>'+*/response['Detail'] + '</p>';
                type = 'error';
                mostrarError(message, type);
                $('#tBuscar').focus();
            } else {
                showingState();
                CompletarInfo(response);
            }
        },
        error: function (response, textStatus) {
            // Hide Loading
            NProgress.done();
            mostrarError(response.Detail, 'error');

        }
    });
}
function showingState() {
    ClearForm();
    window.State = 'showing';    
    OcultarRelaciones();
}// GetEquipo

function CompletarInfo(response)
{
    $('#idEquipo').val(response['idEquipo'])
    $('#Oblea').val(response['Oblea'])
    if (response.NroSerie !== null) {
        $('#NroSerie').val(response['NroSerie'])
    }
    $('#idTipo').val(response['TipoEquipo']['Nombre']).attr('data-id', response['TipoEquipo']['id'])
    if (response.Marca != null & response.Marca != undefined) {
        $('#Marca').val(response['Marca']['Nombre']).attr('data-id', response['Marca']['idMarca'])
    }
    $('#Modelo').val(response.Modelo);
    if (response.idModelo) {
        $('#Modelo').attr('data-id', response.idModelo);
    }
    if (response.Jurisdiccion != null & response.Jurisdiccion != undefined) {
        $('#Jurisdiccion').val(response['Jurisdiccion']['Nombre']).attr('data-id', response['Jurisdiccion']['idJurisdiccion'])
    }
    if (response.Dependencia != null & response.Dependencia != undefined) {
        $('#Ubicacion').val(response['Dependencia']['Nombre']).attr('data-id', response['Dependencia']['idDependencia'])
    }
    if (response['Estado'] !== null) {
        $('#Estado').val(response['Estado']['Nombre']).attr('data-id', response['Estado']['idEstado'])
    }
    $('#Observaciones').val(response['Observaciones'])
    
}
function ClearForm() {    
    // Clean all inputs/textarea
    $('input, textarea', '#content-box').each(function () {
        $(this).val('');
        $(this).attr('readonly', 'readonly');
        $(this).removeClass('error');
    });
    
}
