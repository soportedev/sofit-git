﻿$(function () {         
    $("#Trabajos").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        //position: ['center', 109],
        title: "Cargar Trabajo",
        width: '600px',
        resizeStop: function (ev, ui) {
            var originalSize = ui.originalSize;
            var size = ui.size;
            var textArea = $(ev.target).find('.tSolucion');
            var heightDif = originalSize.height - size.height;
            var widthDif = originalSize.width - size.width;
            textArea.height(textArea.height() - heightDif);
            //textArea.width(textArea.width() - widthDif);
        },
        open: function () {
            var thisdialog = $(this);
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('input[type=text]', '.obleaEquipo').val(datos.oblea);
            $('label.idInc', '.obleaEquipo').text(datos.idEquipo);
        },
        buttons: {
            Aceptar: {
                class: 'aceptardialog',
                id: 'bTrabajos',
                text: 'Aceptar',
                click: function () {
                    var $thisButton = $(this).dialog('widget').find('button.aceptardialog');
                    var fData = new FormData();
                    var datos = $(this).data('datos');
                    var tresolucion = false;
                    var idTipoResolucion= $('#dTipoResolucion option:selected').val();
                    tresolucion = idTipoResolucion == 0;
                    if (tresolucion) {
                        $('#Trabajos').find('.message').html('Seleccione el tipo de Resolución.');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    var obs = $('#observaciones');
                    if ($.trim(obs.val()).length < 10) {
                        $('#Trabajos').find('.message').html('Complete el campo de Observaciones');
                        $('.error, #Trabajos').show();
                        return;
                    }                    
                    datos.idTecnico = $('#idTecnico').val();
                    datos.idTurno = $('#idTurno').val();
                    datos.observaciones = $('#observaciones').val();
                    datos.idTipoResolucion = idTipoResolucion;                    
                    //fdata
                    var inpFile = $('#upFileRes');
                    var img = inpFile.get(0).files[0];                    
                    fData.append('fileupload', img);
                    fData.append('idTecnico', datos.idTecnico);
                    fData.append('idTurno', datos.idTurno);
                    fData.append('observaciones', datos.observaciones);
                    fData.append('idTipoResolucion', datos.idTipoResolucion);
                    fData.append('idIncidente', datos.idIncidente);
                    if ($('#dImagen').is(':visible')) fData.append('idImagen', $('#dImagen').val());
                    if ($('#esParaEquipo').is(':visible')) {
                        var valorCheck = $('#esParaEquipo').is(':checked');
                        fData.append('esParaEquipo', valorCheck);
                        if ($('#esParaEquipo').is(':checked')) {
                            var els = $('#Trabajos').find('.idEquipo');
                            var sufix = 0;
                            $.each(els, function () {
                                fData.append('idEquipo.' + sufix, $(this).html());
                                sufix++;
                            });
                        }
                    }
                    fData.append('sobrescribir', $('#sobrescribir').is(':checked'));
                    $.ajax({
                        url: 'Incidentes/cargaTrabajo',
                        data: fData,
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            NProgress.start();
                            $($thisButton).button('disable');
                        },
                        success: function (data) {
                            ajaxReady = true;
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                $("#Trabajos").dialog('close').find('#observaciones').val('');                                
                                mostrarExito('Se ha cargado la solucion');
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            NProgress.done();
                            $($thisButton).button('enable').removeClass("ui-state-focus ui-state-hover");

                        }
                    });                    
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $("#Trabajos2").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '109px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('input[type=text]', '.obleaEquipo').val(datos.oblea);
            $('label.idInc', '.obleaEquipo').text(datos.idEquipo);
        },
        buttons: {
            Aceptar: {
                id: 'bTrabajos',
                text: 'Aceptar',
                click: function () {
                    var datos = $(this).data('datos');
                    var context = $(this);
                    var tresolucion = false;
                    var idTipoResolucion = $('select.tipoSolucion option:selected', context).val();
                    tresolucion = idTipoResolucion == 0;
                    if (tresolucion) {
                        $('#Trabajos2').find('.message').html('Seleccione el tipo de Resolución.');
                        $('.error, #Trabajos2').show();
                        return;
                    }
                    var obs = $('.tSolucion', context);
                    if ($.trim(obs.val()).length < 10) {
                        $('#Trabajos2').find('.message').html('Complete el campo de Observaciones');
                        $('.error, #Trabajos2').show();
                        return;
                    }
                    datos.idTecnico = $('.dTecnicos option:selected', context).val();
                    datos.idTurno = $('.dTurnos', context).val();
                    datos.observaciones = $('.tSolucion', context).val();
                    datos.idTipoResolucion = idTipoResolucion;
                    datos.sobrescribir = false;
                    datos.esParaEquipo = false;
                    $('#Trabajos2').appendSerialToObject(datos);
                    $.ajax({
                        url: 'Incidentes/cargaTrabajo',
                        data: datos,
                        type: 'POST',
                        datatype: 'json',
                        beforeSend: function () {
                            ajaxReady = false;
                        },
                        success: function (data) {
                            ajaxReady = true;
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                $("#Trabajos2").dialog('close').find('#observaciones').val('');
                                mostrarExito('Se ha cargado la solucion');
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        }
                    });
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $('li.trabajo2').on('click', function () {
        var nro = [];
        var trs = {};
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("Seleccione UN elemento");
        }
        else {
            var $trabajosDialog = $("#Trabajos2");
            clearTrabajosPanel($trabajosDialog);
            var datosArray = nro[0].find('td').slice(1, 7);
            var conforms = true;
            var oblea, idIncidente, nroIncidente, idEquipo;            
            idIncidente = datosArray[0].innerHTML;
            var datos = {};
            datos.idIncidente = idIncidente;
            datos.nroIncidente = $(datosArray[1]).find('span').text();
            datos.idEquipo = idEquipo;
            datos.oblea = oblea;
            datos.nombreGrilla = nombreGrilla;
            $trabajosDialog.data('datos', datos).dialog('open');
        }
    });
});//ready
function ubicarIncidente(data) {
    var direccion = data.SubDireccion;
    if (direccion == "Mesa de Ayuda") {
        var area = data.Area;
        switch (direccion) {
            case "Mesa de Ayuda":
                if (data.Estado == "Ingresado" | data.Estado == "Derivado") doScroll(ventanas[0], data.Numero);
                if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                break;
            
        }
    } else {
        mostrarInfo("El ticket se encuentra en: " + direccion);
    }
}
function derivar() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var context = $(this).parents('ul.fly');
    var origen = grilla.attr('id');
    var idArea = $('.sArea', context).val();
    var textArea = $('.sArea option:selected', context).text();
    var idTecnico = $('.sTecnico', context).val();
    var fData = new FormData();

    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Incidente");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(3)').text();
        fData.append("idAreaDestino", idArea);
        fData.append("idTecnico", idTecnico);
        fData.append("idIncidente", id_inc);
        if (window.ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarIncidente",
                data: fData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    window.ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el Incidente a ' + textArea);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    window.ajaxReady = true;
                    if (exito) {
                        actualizarPaneles('incidentes')
                    }
                }
            });
        }
    }
}
/**FUNCION OCULTAR INFO**/
function ocultar() {
    $('#tOblea').val("").removeClass().addClass('input');
    mostrarSub();
}
/**TRAER INCIDENTES A LA VIDA**/
function incidenteToLife() {
    var exito = false;
    if (validarNoVacio($('#tIncToLive'))) {
        var nroInc = $('#tIncToLive').val();
        $.ajax({
            url: "Incidentes/incidentesToLife",
            type: 'POST',
            data: 'nroInc=' + nroInc,
            beforeSend: ajaxReady = false,
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    exito = true;
                    mostrarExito('El Incidente ' + nroInc + ' fue reactivado');
                    $('#tIncToLive').val('');
                }
                else mostrarError(data.Detail);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                if (exito) actualizarPaneles('ingresados');
            }
        });
    }
}
/**Search Autocomplete**/
function searchAutocomplete(request, response) {
    var texto = request.term;
    texto = texto.toUpperCase();
    var uri = 'GetIncidentesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR EQUIPOS O INCIDENTES*/
function buscarEquiposInc(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    resString = resString.toUpperCase();

    if (!regularEquipo.test(resString) && !regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }
    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/buscarEqOInc",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("El equipo/incidente no está activo");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Mesa de Ayuda":
                            if (data.Estado == "Ingresado" | data.Estado == "Derivado") doScroll(ventanas[0], data.Numero);
                            else {
                                if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                                else { }
                            }
                            break;
                        default: mostrarInfo("El ticket se encuentra en " + data.Area)  

                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
/**Search Autocomplete Cliente**/
function searchAutocompleteCliente(request, response) {
    var texto = request.term;    
    var uri = 'GetClientesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR Cliente*/
function buscarCliente(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    
    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/incidenteXCliente",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("No hay Incidentes para ese cliente");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Mesa de Ayuda": if (data.Estado == "Ingresado") doScroll(ventanas[0], data.Numero);
                        else {
                            if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                            else { }
                        }
                            break;
                        default: ;
                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
/**FUNCION FINALIZAR EQUIPO**/
function terminarEquipo() {
    
}
/**FUNCION TOMAR EQUIPO**/
function tomarEquipo() {
   
}
//FUNCION ASIGNAR TECNICO A EQUIPO    
function asignarEquipo() {

   
}
//helper send data
function setTecnicoAEquipo(tecnico, idEq) {
    
}
/**FUNCION SOLUCIONAR Y CERRAR INCIDENTE**/
function cerrarIncidente() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN incidente");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);
        var data = ["idInc=", "nroInc=", "fecha=", "tipif=", "jurisd=", "a=", "area=", "estado="];
        var value;
        $(trsel).each(function (i) {
            value = $(this).text();
            data[i] += value;
        });
        var datos = data.join('&');
        if (ajaxReady) {
            var divdialog = $('<div>').append("Se va a cerrar el incidente " + $(trsel[1]).text() + ". Está Seguro?").dialog({
                width: '250px',
                show: 'slide',
                //position: 'center',
                open: function () {
                    $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                },
                buttons: {
                    Aceptar: function () {
                        $.ajax({
                            url: "Incidentes/solucionarIncidente",
                            data: datos,
                            type: 'POST',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarInfo('Se ha Cerrado el Incidente');
                                    exito = true;
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                $(divdialog).dialog('destroy');
                                if (exito) {
                                    actualizarPaneles('ingresados');
                                }
                            }
                        });               //fin ajax
                    },
                    Cancelar: function () { $(this).dialog('destroy') }
                }

            });


        } //fin ajaxready
    } //fin else
} //fin funcion
/**FUNCION QUITAR EQUIPOS AL INCIDENTE**/
function quitarEquipos(event) {
   
}
/**FUNCION AGREGAR EQUIPOS AL INCIDENTE**/
function addEquipos(event) {
    
}
function notificar() {

}
function cargarTrabajos() {
    var nro = [];
    var trs = {};
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Seleccione UN elemento");
    }
    else {
        var $trabajosDialog = $("#Trabajos");
        clearTrabajosPanel($trabajosDialog);
        var datosArray = nro[0].find('td').slice(1, 7);
        var conforms = true;
        var oblea, idIncidente, nroIncidente, idEquipo;        
        switch (nombreGrilla) {            

        }        
        idIncidente = datosArray[0].innerHTML;
        var datos = {};
        datos.idIncidente = idIncidente;
        datos.nroIncidente = $(datosArray[1]).find('span').text();
        datos.idEquipo = idEquipo;
        datos.oblea = oblea;
        datos.nombreGrilla = nombreGrilla;
        $trabajosDialog.data('datos', datos).dialog('open');
    }
}
function derivarEq() {
}
function actualizarPaneles(tabla) {
    if (ajaxReady) {
        $.ajax({
            url: "Incidentes",
            type: 'GET',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                var results = new Array();
                results[0] = $(data).find('#grillaBandeja');
                results[1] = $(data).find('#grillaSuspendidos');                
                switch (tabla) {                    
                    case "suspendidos": ventanas[1].update(results[1]);
                        break;
                    case "ingresados": ventanas[0].update(results[0]);
                        break;                    
                    case "todos":
                        ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);                        
                        break;
                    default:;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarFinSesion();
            },
            complete: function () {
                ajaxReady = true;
                $('p.ajaxGif').hide();
            }
        });//fin ajax
    }
}
function notificarIncidente() { }