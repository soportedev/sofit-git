﻿using soporte.Controllers;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using soporte.Areas.Operaciones.Models;
using System.Data;
using System.Reflection;
using System.Text;
using System.Collections.Specialized;
using soporte.Models.Statics;
using soporte.Models;
using System.Globalization;
using Sofit.DataAccess;

namespace soporte.Areas.Operaciones.Controllers
{
    [SessionActionFilter]
    public class HistoricosController : Controller
    {
        //
        // GET: /Operaciones/Historicos/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Historicos");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            else
            {
                ModelHistoricos model = new ModelHistoricos();
                List<Area> areas = new List<Area>();
                List<Tecnico> tecnicos = new List<Tecnico>();
                List<TipoResolucion> tipoResoluciones = new List<TipoResolucion>();
                List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
                List<Turno> turnos = new List<Turno>();
                List<TipoTicketVista> tiposTicket = new List<TipoTicketVista>();
                List<DireccionVista> direcciones = new List<DireccionVista>();
                List<PrioridadesTicketVista> prioridadesTicket = new List<PrioridadesTicketVista>();
                List<ServiciosVista> servicios = new List<ServiciosVista>();
                using (var dbc = new IncidenciasEntities())
                {
                    servicios = (from u in dbc.Servicios
                                 select new ServiciosVista
                                 {
                                     Id = u.id_servicio,
                                     Nombre = u.nombre
                                 }).ToList();
                    tiposTicket = (from u in dbc.TipoTicket
                                   select new TipoTicketVista
                                   {
                                       Id = u.id,
                                       Nombre = u.nombre
                                   }).ToList();
                    prioridadesTicket = (from u in dbc.PrioridadTicket
                                         select new PrioridadesTicketVista
                                         {
                                             Id = u.id,
                                             Nombre = u.nombre
                                         }).ToList();
                    direcciones = (from u in dbc.Direcciones
                                   select new DireccionVista
                                   {
                                       idDireccion = u.id_direccion,
                                       Nombre = u.nombre
                                   }).ToList();
                    jurisdicciones = (from u in dbc.Jurisdiccion
                                      select new Jurisdicciones
                                      {
                                          idJurisdiccion = u.id_jurisdiccion,
                                          Nombre = u.descripcion
                                      }).ToList();
                    areas = (from u in dbc.area
                             where u.Direcciones.nombre == "Operaciones"
                             select new Area
                             {
                                 idArea = u.id_area,
                                 Nombre = u.descripcion
                             }).ToList();
                    tecnicos = (from u in dbc.tecnico
                                where u.area.Direcciones.nombre == "Operaciones" & u.activo.Value
                                select new Tecnico
                                {
                                    idTecnico = u.id_tecnico,
                                    Nombre = u.descripcion
                                }).ToList();
                    turnos = (from u in dbc.turno
                              select new Turno
                              {
                                  idTurno = u.id_turno,
                                  Nombre = u.descripcion
                              }).ToList();
                    tipoResoluciones = (from u in dbc.tipo_resolucion
                                        where u.area.Direcciones.nombre == "Operaciones"
                                        orderby u.descripcion
                                        select new TipoResolucion
                                        {
                                            idTipoResolucion = u.id_tipo_resolucion,
                                            Nombre = u.descripcion
                                        }).ToList();
                }
                List<TipoResolucion> TiposTrabajo = new List<TipoResolucion>();
                foreach (var v in tipoResoluciones)
                {
                    TextInfo txtInfo = new CultureInfo("es-ar", false).TextInfo;
                    string lower = v.Nombre.ToLower();
                    string palabra = txtInfo.ToTitleCase(lower);
                    TipoResolucion nuevo = new TipoResolucion
                    {
                        idTipoResolucion = v.idTipoResolucion,
                        Nombre = palabra
                    };
                    if (!TiposTrabajo.Contains(nuevo)) TiposTrabajo.Add(nuevo);
                }
                model.Servicios = servicios;
                model.Areas = areas;
                model.Tecnicos = tecnicos;
                model.Resoluciones = TiposTrabajo;
                model.Turnos = turnos;
                model.Jurisdicciones = jurisdicciones;
                model.Direcciones = direcciones;
                model.TiposTicket = tiposTicket;
                model.PrioridadesTicket = prioridadesTicket;
                return View(model);
            }
        }
        public JsonResult GetIncHistoricoPorNro(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.AreaXIncidencia
                         where u.area.Direcciones.nombre == "Operaciones" & u.Incidencias.numero.StartsWith(prefixText)
                         group u by u.Incidencias.numero into g
                         select g.Key).Take(7).ToList();
            }
            return Json(items);
        }
        public JsonResult GetEnlaceHistoricoPorNro(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Resoluciones
                         where u.area.Direcciones.nombre == "Operaciones" && u.UnidadConf.nombre.StartsWith(prefixText)
                         && u.Incidencias.fecha_fin.HasValue
                         select u.UnidadConf.nombre).Distinct().Take(7).ToList();
            }
            return Json(items);
        }
        public JsonResult getHistoricoIncidente(string text)
        {
            HistIncBuilder incBuilder = new HistIncBuilder();
            incBuilder.Construir(text);
            return Json(incBuilder.getResult());
        }
        public JsonResult getHistoricoEquipo(string text)
        {
            HistEnlaceBuilder builder = new HistEnlaceBuilder();
            builder.Construir(text, true);
            HistoricoEnlace historicoEquipo = builder.getResult();

            return Json(historicoEquipo);
        }
        public JsonResult getDatos()
        {
            HistoricosParametros param = new HistoricosParametros();
            param.FechaDesde = new DateTime(DateTime.Now.Year, 1, 1);
            param.FechaHasta = DateTime.Now;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();

            //obtengo datos
            if (nvc.Count > 0)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("xtecn")) param.idTecnico = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("xjuri")) param.idJurisdiccion = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("xtrab")) param.idTipoResolucion = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("rangofechadesde")) param.FechaDesde = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (key.StartsWith("rangofechahasta")) param.FechaHasta = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59);
                    if (key.StartsWith("oblea")) param.oblea = nvc.GetValues(key)[0];
                    if (key.StartsWith("incide")) param.nroIncidente = nvc.GetValues(key)[0];
                    if (key.StartsWith("total"))
                    {
                        string valor = nvc.GetValues(key)[0];
                        switch (valor)
                        {
                            case "xt": param.xTecnico = true;
                                break;
                            case "xj": param.xJurisdiccion = true;
                                break;
                            case "xtr": param.xTrabajo = true;
                                break;
                        }
                    }

                }
                ResolucionesVista controladorResoluciones = new ResolucionesOperaciones();
                //consulta con parametros
                if (!param.xTrabajo && !param.xJurisdiccion && !param.xTecnico)
                {
                    result.Detail = controladorResoluciones.getResolucionesParametros(param);
                    result.Info = "ok";
                    //guardo en session
                    //saveSource(resoluciones);
                }
                else
                {
                    //son totales
                    if (param.xJurisdiccion)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesJurisdiccion(param);
                        result.Info = "ok";
                    }
                    else if (param.xTecnico)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesTecnico(param);
                        result.Info = "ok";
                    }
                    else if (param.xTrabajo)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesTrabajo(param);
                        result.Info = "ok";
                    }
                }
            }
            return Json(result);
        }        
        private string ToHtmlTableTotalTecnico(IEnumerable<Sofit.DataAccess.Resoluciones> d)
        {
            StringBuilder sb = new StringBuilder();
            if (d.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Técnico</th><th>Resolución</th><th>Total</th></tr>");
                foreach (Resoluciones r in d)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + r.tecnico.descripcion + "</td>");
                    sb.Append("<td>" + r.tipo_resolucion.descripcion + "</td>");
                    sb.Append("<td>" + "?" + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        private string toHtmlTableTotalTrabajo(IEnumerable<Resoluciones> d)
        {
            StringBuilder sb = new StringBuilder();
            if (d.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Resolución</th><th>Total</th></tr>");
                foreach (Resoluciones r in d)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + r.tipo_resolucion.descripcion + "</td>");
                    sb.Append("<td>" + "?" + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        private string ToHtmlTableGral(IEnumerable<Resoluciones> d)
        {
            StringBuilder sb = new StringBuilder();
            string clas = string.Empty;

            if (d.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr  class='ui-state-active'><th>Fecha</th><th>Oblea</th><th>Incidente</th><th>Resolución</th>");
                sb.Append("<th>Técnico</th><th>Ubicación</th><th>Jurisdicción</th><th>Notas</th></tr>");
                foreach (Resoluciones r in d)
                {
                    sb.Append("<tr>");
                    DateTime sDate = r.fecha;
                    sb.Append("<td>" + sDate.ToShortDateString() + "</td>");
                    sb.Append("<td>" + (r.UnidadConf == null ? "N/A" : r.UnidadConf.nombre) + "</td>");
                    sb.Append("<td>" + r.Incidencias.numero + "</td>");
                    sb.Append("<td>" + r.tipo_resolucion.descripcion + "</td>");
                    sb.Append("<td>" + r.tecnico.descripcion + "</td>");
                    sb.Append("<td>" + (r.Incidencias.Dependencia != null ? r.Incidencias.Dependencia.descripcion : "N/A") + "</td>");
                    sb.Append("<td>" + (r.Incidencias.Dependencia != null ? r.Incidencias.Dependencia.Jurisdiccion.descripcion : r.Incidencias.Jurisdiccion.descripcion) + "</td>");
                    string observaciones = r.observaciones;
                    if (observaciones == "") clas = "verInfoDisabled";
                    else clas = "verInfo";
                    sb.Append("<td><a href='JavaScript:void(0)' data-res='" + observaciones + "' class='" + clas + "'>Ver</a></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        private void saveSource<T>(IEnumerable<T> d)
        {
            var tb = new DataTable();

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                tb.Columns.Add(prop.Name, prop.PropertyType);
            }

            foreach (var item in d)
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }
            Session["tableSource"] = tb;
        }

    }
}
