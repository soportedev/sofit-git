﻿using Sofit.DataAccess;
using soporte.Areas.Operaciones.Models;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Operaciones.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
        //
        // GET: /Operaciones/Config/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            else
            {
                return View();
            }
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas,int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Operaciones");
            using (var dbc = new IncidenciasEntities())
            {
                var serv = new Servicios
                {
                    nombre = nombre,
                    tiempo_res = totales.Hours,
                    id_direccion = direccion.IdDireccion,
                    tiempo_margen=totalesMargen.Hours,
                    tolerancia = 80,
                    vigente = true
                };
                dbc.Servicios.Add(serv);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public ActionResult Tipificaciones()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Tipificaciones");            
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre == "Operaciones"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Operaciones");
                model.Tipificaciones = arbol.getResult();
                return View(model);
            }
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Operaciones" select u).Single();
                    Tipificaciones nueva = new Tipificaciones()
                    {
                        nombre = nombre,
                        descripcion = desc,
                        id_padre = idPadre,
                        id_direccion = direccionSoporte.id_direccion
                    };
                    dbc.Tipificaciones.Add(nueva);

                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t = (from u in dbc.Tipificaciones
                             where u.id_tipificacion == id
                             select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public ActionResult Usuarios()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Usuarios");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesOperaciones();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getUsuariosOperaciones();
                return PartialView("Usuarios",model);
            }                
        }
        public JsonResult NuevoUsuario(string login, string nombre, string p1, string perfil)
        {
            Responses result = new Responses();
            //usuarioHandler user = new usuarioHandler();
            //int idPerfil = Int32.Parse(perfil);
            //IDirecciones operaciones = DireccionesFactory.getDireccion("Operaciones");
            //result = user.nvoUsuario(login, nombre, p1, idPerfil,operaciones);
            return Json(result);
        }
        public ActionResult Tiempos()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Tiempos");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                TiemposOperaciones tiempos = new TiemposOperaciones();
                return PartialView(tiempos);
            }
        }
        public JsonResult setTiempos(int tAIng,int tAvisoAdm, int tAvisoClaro,int tAvisoEpec,int tAvisoSes,int tAvisoTelecom,int tEIng,int TEmergAdm, int tEmergClaro, int tEmergEpec,int tEmergSes,int tEmergTelecom)
        {
            Responses result = new Responses();
            TiemposOperaciones tiempos = new TiemposOperaciones();
            result = tiempos.setTiempos(tAIng,tEIng,tAvisoAdm,TEmergAdm,tAvisoClaro,tEmergClaro,tAvisoEpec,tEmergEpec,tAvisoTelecom,tEmergTelecom,tAvisoSes,tEmergSes);
            return Json(result);
        }
        public JsonResult borrarEquipo(string nombre)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var equipo = dbc.UnidadConf.Where(c => c.nombre == nombre).SingleOrDefault();
                    if (equipo == null)
                    {
                        result.Detail = "Equipo no encontrado";
                    }
                    else
                    {
                        var enlace = (from u in dbc.EnlaceComunicaciones
                                      where u.UnidadConf.nombre.Equals(nombre)
                                      select u).SingleOrDefault();
                        var varios = (dbc.Varios.Where(c => c.UnidadConf.nombre == nombre)).SingleOrDefault();
                        if (enlace != null) dbc.EnlaceComunicaciones.Remove(enlace);
                        if(varios != null) dbc.Varios.Remove(varios);
                        dbc.UnidadConf.Remove(equipo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }           
                }                
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
    
    }
}
