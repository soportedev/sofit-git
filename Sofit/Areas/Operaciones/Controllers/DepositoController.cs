﻿using soporte.Controllers;
using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Operaciones.Controllers
{
    [SessionActionFilter]
    public class DepositoController : Controller
    {
        //
        // GET: /Operaciones/Deposito/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Deposito");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            else
            {
                return View();
            }
        }

    }
}
