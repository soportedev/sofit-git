﻿using Sofit.DataAccess;
using soporte.Areas.Operaciones.Models.ClasesVista;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Operaciones.Controllers
{
    [SessionActionFilter]
    public class InventarioController : Controller
    {
        //
        // GET: /Operaciones/Enlaces/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Operaciones/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            if (acceso == Constantes.AccesosPagina.ReadOnly)
            {
                ViewBag.acceso = "readonly";
            }
            if (acceso == Constantes.AccesosPagina.Full)
            {
                ViewBag.acceso = "full";
            }   
            return View();
        }
        public JsonResult TiposEnlace()
        {
            List<TiposEnlaceVista> result = new List<TiposEnlaceVista>();
            using (var dbc = new IncidenciasEntities())
            {
                result = (from u in dbc.TipoEnlace
                          select new TiposEnlaceVista
                          {
                              Id = u.id_tipo_enlace,
                              Nombre = u.nombre
                          }).ToList();
            }
            return Json(result);
        }
        public JsonResult TiposEquipo()
        {
            List<TiposEquipoVista> result = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                result = (from u in dbc.Tipo_equipo
                          where u.Direcciones.nombre=="Operaciones"
                          select new TiposEquipoVista
                          {
                              id = u.id_tipo,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(result);
        }
        public JsonResult AnchosBanda()
        {
            List<AnchoBandaVista> result = new List<AnchoBandaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                result = (from u in dbc.AnchoBanda                          
                          select new AnchoBandaVista
                          {
                              Id = u.id_ab,
                              Nombre = u.nombre
                          }).ToList();
                result.Sort();
               
            }
            return Json(result);
        }
        public JsonResult Proveedores()
        {
            List<ProveedoresVista> result = new List<ProveedoresVista>();
            using (var dbc = new IncidenciasEntities())
            {
                result = (from u in dbc.ProveedoresEnlace
                          select new ProveedoresVista
                          {
                              Id = u.id_proveedor,
                              Nombre = u.nombre
                          }).ToList();

            }
            return Json(result);
        }        
        public JsonResult NuevoEc()
        {
            usuarioBean usuarioActual=(usuarioBean)Session["usuario"];
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            try
            {
                string Oblea = nvc.Get("nombre").Trim();                
                int idTipo = Int32.Parse(nvc.Get("idTipo"));                
                string version = nvc.Get("version");
                int idDependencia = Int16.Parse(nvc.Get("Ubicacion"));
                int idJurisdiccion = Int16.Parse(nvc.Get("Jurisdiccion"));                
                string Observaciones = nvc.Get("observacion");
                int bw, idProveedor, idTipoEnlace = 0;
                Int32.TryParse(nvc.Get("bw"), out bw);
                Int32.TryParse(nvc.Get("idProveedor"), out idProveedor);
                Int32.TryParse(nvc.Get("tipoEnlace"), out idTipoEnlace);
                string descripcion = nvc.Get("descripcion");
                string lan = nvc.Get("lan");
                string wan = nvc.Get("wan");
                string loopb = nvc.Get("loopb");
                string nContacto = nvc.Get("nContacto");
                string direccion = nvc.Get("direccion");
                string te = nvc.Get("Te");
                string nroRef = nvc.Get("nroReferencia");
                using (var dbc = new IncidenciasEntities())
                {
                    var checking = dbc.UnidadConf.Where(c => c.nombre == Oblea);
                    if (checking.Count() == 0)
                    {
                        string tipo = dbc.Tipo_equipo.Where(c => c.id_tipo == idTipo).Single().descripcion;
                        if (tipo == "Enlace")
                        {
                            EnlaceComunicaciones enlace = new EnlaceComunicaciones
                            {
                                bw = bw != 0 ? bw : (int?)null,
                                descripcion = descripcion,
                                id_estado = (from u in dbc.Estado_UC where u.descripcion == "Productivo" select u.id_estado).First(),
                                UnidadConf = new UnidadConf
                                {
                                    id_jurisdiccion = idJurisdiccion,
                                    id_tipo = idTipo,
                                    fecha = DateTime.Now,
                                    id_usuario = usuarioActual.IdUsuario,
                                    nombre = Oblea,
                                    observaciones = Observaciones
                                },
                                id_proveedor = idProveedor,
                                id_tipo_enlace = idTipoEnlace != 0 ? idTipoEnlace : (int?)null,
                                lan = lan,
                                loopback = loopb,
                                nro_ref = nroRef,
                                id_dependencia=idDependencia,
                                wan = wan
                            };
                            if (nContacto != "" | te != "" | direccion != "")
                            {
                                ContactosOperaciones found = null;
                                if(nContacto!=null){
                                    found = dbc.ContactosOperaciones.Where(c => c.nombre == nContacto).SingleOrDefault();
                                }
                                if (found == null)
                                {                             
                                    found = new ContactosOperaciones
                                    {
                                        direccion = direccion,
                                        nombre = nContacto,
                                        te = te
                                    };
                                }
                                enlace.ContactosOperaciones = found;
                            }
                            dbc.EnlaceComunicaciones.Add(enlace);
                        }
                        else//no es enlace
                        {
                            Varios v = new Varios
                            {
                                modelo = version,
                                id_dependencia=idDependencia,
                                id_estado=dbc.Estado_UC.Where(c=>c.descripcion=="Productivo"&c.Direcciones.nombre=="Operaciones").Single().id_estado,
                                UnidadConf = new UnidadConf
                                {
                                    nombre = Oblea,
                                    id_tipo = idTipo,
                                    id_usuario = usuarioActual.IdUsuario,
                                    id_jurisdiccion = idJurisdiccion,
                                    fecha = DateTime.Now,
                                    observaciones = Observaciones
                                }
                            };
                            dbc.Varios.Add(v);
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else result.Detail = "Ya existe un elemento con ese nombre.";
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        result.Detail = validationError.ErrorMessage + "\n";
                        //Trace.TraceInformation("Property: {0} Error: {1}",
                        //                        validationError.PropertyName,
                        //                        validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);           
        }
        public JsonResult UpdateEc()
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = (usuarioBean)Session["usuario"];
            NameValueCollection nvc = Request.Form;
            string Oblea = nvc.Get("nombre").Trim();
            int idTipo = Int32.Parse(nvc.Get("idTipo"));
            string version = nvc.Get("version");
            int idDependencia = 0;
            Int32.TryParse(nvc.Get("Ubicacion"),out idDependencia);
            int idJurisdiccion = Int16.Parse(nvc.Get("Jurisdiccion"));
            int idEstado = Int16.Parse(nvc.Get("idEstado"));
            string Observaciones = nvc.Get("observacion");
            int bw, idProveedor, idTipoEnlace = 0;
            Int32.TryParse(nvc.Get("bw"), out bw);
            Int32.TryParse(nvc.Get("idProveedor"), out idProveedor);
            Int32.TryParse(nvc.Get("tipoEnlace"), out idTipoEnlace);
            string descripcion = nvc.Get("descripcion");
            string lan = nvc.Get("lan");
            string wan = nvc.Get("wan");
            string loopb = nvc.Get("loopb");
            string nContacto = nvc.Get("nContacto");
            string direccion = nvc.Get("direccion");
            string te = nvc.Get("Te");
            string nroRef = nvc.Get("nroReferencia");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var uc = dbc.UnidadConf.Where(c => c.nombre == Oblea).FirstOrDefault();
                    if (uc != null)
                    {
                        string tipo = uc.Tipo_equipo.descripcion;
                        if (tipo == "Enlace")
                        {
                            var enlace = dbc.EnlaceComunicaciones.Where(c => c.UnidadConf.nombre == Oblea).First();
                            if (enlace != null)
                            {
                                enlace.bw = bw != 0 ? bw : (int?)null;
                                enlace.descripcion = descripcion;
                                enlace.id_estado = idEstado;
                                enlace.UnidadConf.id_jurisdiccion = idJurisdiccion;
                                enlace.id_tipo_enlace = idTipoEnlace != 0 ? idTipoEnlace : (int?)null;
                                enlace.UnidadConf.observaciones = Observaciones;
                                enlace.id_estado = idEstado;
                                enlace.id_dependencia = idDependencia;
                                enlace.lan = lan;
                                enlace.loopback = loopb;
                                enlace.wan = wan;
                                enlace.nro_ref = nroRef;
                                enlace.id_proveedor = idProveedor;
                                if (nContacto != null | te != null | direccion != null)
                                {
                                    ContactosOperaciones found = null;
                                    if (nContacto != null)
                                    {
                                        found =enlace.ContactosOperaciones;
                                    }
                                    if (found == null)
                                    {
                                        found = new ContactosOperaciones
                                        {
                                            direccion = direccion,
                                            nombre = nContacto,
                                            te = te
                                        };
                                        enlace.ContactosOperaciones = found;
                                    }
                                    else
                                    {
                                        found.direccion = direccion;
                                        found.nombre = nContacto;
                                        found.te = te;
                                    }                                    
                                }
                                dbc.SaveChanges();
                                result.Info = "ok";
                            }
                            else result.Detail = "El equipo no se encuentra";
                        }//tipo enlace
                        if(tipo=="Software de Base"|tipo=="Aplicación de Usuarios")
                        {
                            var tiponuevo = dbc.Tipo_equipo.Where(c => c.id_tipo == idTipo).Single();
                            if (tiponuevo.descripcion != "Enlace") uc.id_tipo = idTipo;
                            var softB = uc.Varios.SingleOrDefault();
                            softB.modelo = version;
                            softB.UnidadConf.observaciones = Observaciones;
                            softB.UnidadConf.id_jurisdiccion = idJurisdiccion;
                            softB.id_dependencia = idDependencia;
                            softB.id_estado = idEstado;
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == uc.id_uc & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult EstadosEnlace()
        {
            List<EstadoEquipoVista> estados = new List<EstadoEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                estados = (from u in dbc.Estado_UC                        
                           select new EstadoEquipoVista
                           {
                               idEstado = u.id_estado,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(estados);
        }
        public JsonResult SaveBaseLine(int id)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Enlace": equipo = new EnlaceInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        default: equipo = new AplicacionInventario();
                            equipo.construir(eqBase.id_uc);
                            break;                        
                    }
                }
                if (equipo != null)
                {
                    result = equipo.SaveLineaBase();
                }
                return Json(result);
            }
        }
        public JsonResult EquipoXNombre(string prefixText)
        {
            List<EnlaceVista> items = new List<EnlaceVista>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText)
                         select new EnlaceVista
                         {
                             Id = u.id_uc,
                             Nombre = u.nombre                            
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EnlaceXProveedor(string prefixText, string proveedor)
        {
            List<EnlaceVista> items = new List<EnlaceVista>();
            string wildcard = string.Empty;
            if (proveedor.Equals("adm")) wildcard = "ADM";
            
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EnlaceComunicaciones
                         where (wildcard == "ADM" && u.ProveedoresEnlace.nombre == "ADM" && u.UnidadConf.nombre.StartsWith(prefixText))
                         | (wildcard==string.Empty&&u.ProveedoresEnlace.nombre != "ADM" && u.UnidadConf.nombre.StartsWith(prefixText))
                         select new EnlaceVista
                         {
                             Id = u.UnidadConf.id_uc,
                             Nombre = u.UnidadConf.nombre,
                             IP = u.lan
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EnlaceXIp(string prefixText)
        {
            List<EnlaceVista> items = new List<EnlaceVista>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EnlaceComunicaciones
                         where u.lan.StartsWith(prefixText)
                         select new EnlaceVista
                         {
                             Id = u.UnidadConf.id_uc,
                             Nombre = u.UnidadConf.nombre,
                             IP=u.lan
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EnlaceXReferencia(string prefixText)
        {
            List<EnlaceVista> items = new List<EnlaceVista>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EnlaceComunicaciones
                         where u.nro_ref.StartsWith(prefixText.ToUpper())
                         select new EnlaceVista
                         {
                             Id = u.UnidadConf.id_uc,
                             Nombre = u.UnidadConf.nombre,
                             Referencia = u.nro_ref
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EnlaceXDescripcion(string prefixText)
        {
            List<EnlaceVista> items = new List<EnlaceVista>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EnlaceComunicaciones
                         where u.descripcion.StartsWith(prefixText)
                         select new EnlaceVista
                         {
                             Id = u.UnidadConf.id_uc,
                             Nombre = u.UnidadConf.nombre,
                             Descripcion = u.descripcion
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult Arbol()
        {
            List<ArbolVista> items = new List<ArbolVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var proveedores = from u in dbc.ProveedoresEnlace
                                  select u;
                foreach (ProveedoresEnlace p in proveedores)
                {
                    ArbolVista a = new ArbolVista();
                    a.Proveedor = p.nombre;
                    a.hijos = (from v in dbc.EnlaceComunicaciones
                               where v.id_proveedor == p.id_proveedor                               
                               select new EnlaceVista
                               {
                                   Descripcion = v.descripcion,
                                   Nombre = v.UnidadConf.nombre
                               }).ToList();
                    a.hijos.Sort();
                    items.Add(a);
                }                               
            }
            return Json(items);
        }        
    }
}
