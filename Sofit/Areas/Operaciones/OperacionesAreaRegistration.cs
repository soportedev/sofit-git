﻿using System.Web.Mvc;

namespace soporte.Areas.Operaciones
{
    public class OperacionesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Operaciones";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "lineabaseOperaciones",
                url: "Operaciones/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiooperaciones",
                url: "Operaciones/Cambios/{startIndex}",
                defaults: new { controller = "Cambios", action = "Index", startIndex=0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8oper",
                url: "Operaciones/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Operaciones_default",
                "Operaciones/{controller}/{action}/{id}",
                new { controller="Operaciones/Incidentes", action = "Index", id = UrlParameter.Optional },
                new string[] { "soporte.Areas.Operaciones.Controllers" }
            );
        }
    }
}
