﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class EnlaceIncidentesVista
    {
        public int IdEquipo { get; set; }
        public int IdEnlace { get; set; }
        public int IdIncidente { get; set; }
        public string Enlace { get; set; }
        public string NroIncidente { get; set; }
        public DateTime FechaDesde { get; set; }
        public string Dependencia { get; set; }
        public string Tecnico { get; set; }
        public string EstadoIcono { get; set; }        
        public string Observacion { get; set; }
        public string Tipificacion { get; set; }
        public string DescIncidente { get; set; }
        public string Proveedor { get; set; }
        public Jurisdicciones Jurisdicciones { get; set; }
        public string Nombre { get; set; }
    }
}