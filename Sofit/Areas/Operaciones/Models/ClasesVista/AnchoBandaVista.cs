﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class AnchoBandaVista:IComparable
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public int CompareTo(object obj)
        {
            AnchoBandaVista other = obj as AnchoBandaVista;
            int num1=Int32.Parse(Regex.Match(this.Nombre,@"\d+").Value);
            int num2 = Int32.Parse(Regex.Match(other.Nombre, @"\d+").Value);
            return num1.CompareTo(num2);
        }
    }
}