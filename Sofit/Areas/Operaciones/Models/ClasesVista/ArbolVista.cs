﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class ArbolVista
    {        
        public string Proveedor { get; set; }
        public List<EnlaceVista> hijos { get; set; }
    }
}