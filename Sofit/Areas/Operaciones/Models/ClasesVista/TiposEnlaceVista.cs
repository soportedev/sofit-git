﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class TiposEnlaceVista
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}