﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class EnlaceVista:IComparable
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string IP { get; set; }
        public string Referencia { get; set; }

        public int CompareTo(object obj)
        {
            EnlaceVista other = obj as EnlaceVista;
            bool intParse = true;
            string[] prefix1 = this.Descripcion.Split(new Char[]{'.','-','_'});
            string[] prefix2 = other.Descripcion.Split(new Char[] { '.', '-','_' });
            int intPref=0;
            int intPref2=0;
            try
            {
                Int32.TryParse(prefix1[0],out intPref);
                Int32.TryParse(prefix2[0],out intPref2);
            }
            catch (Exception)
            {
                intParse = false;
            }
            if (intParse)
            {
                try
                {
                    if (intPref == intPref2)
                    {
                        Int32.TryParse(prefix1[1], out intPref);
                        Int32.TryParse(prefix2[1], out intPref2);
                    }
                }
                catch (Exception)
                {
                    intParse = false;
                }                
            }
            if (intParse) return intPref.CompareTo(intPref2);
            else  return this.Descripcion.CompareTo(other.Descripcion);
        }
    }
}