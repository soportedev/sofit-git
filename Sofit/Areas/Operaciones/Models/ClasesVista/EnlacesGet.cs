﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class EnlacesGet
    {
        public int IdEstado { get; set; }        
        public string Te { get; set; }
        public int bw { get; set; }
        public string descripcion { get; set; }
        public string direccion { get; set; }
        public int idProveedor { get; set; }
        public int jurisdiccion { get; set; }
        public string lan { get; set; }
        public string loopb { get; set; }
        public string nContacto { get; set; }
        public string nombre { get; set; }
        public string nroReferencia { get; set; }
        public string observaciones { get; set; }
        public int tipo { get; set; }
        public string url { get; set; }
        public string wan { get; set; }
    }
}