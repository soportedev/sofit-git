﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class EnlaceInventario : IEquipoInventario
    {
        public AnchoBandaVista AnchoBanda { get; set; }
        public ProveedoresVista Proveedor { get; set; }        
        public EstadoEquipoVista Estado { get; set; }
        public TiposEnlaceVista TipoEnlace { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public string Te { get; set; }       
        public string descripcion { get; set; }
        public string direccion { get; set; }        
        public string lan { get; set; }
        public string loopb { get; set; }
        public string nContacto { get; set; }
        public string nombre { get; set; }
        public string nroReferencia { get; set; }
        public string observaciones { get; set; }             
        public string wan { get; set; }

        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Nombre:").Append(Oblea).AppendLine();
            sb.Append("TipoElemento:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Jurisdicción:").Append(this.Jurisdiccion.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            sb.Append("Proveedor:").Append(this.Proveedor!=null?this.Proveedor.Nombre:"N/A").AppendLine();
            sb.Append("Descripción:").Append(this.descripcion).AppendLine();
            sb.Append("Nro.Referencia:").Append(this.nroReferencia).AppendLine();
            sb.Append("Ancho de Banda:").Append(this.AnchoBanda!=null?this.AnchoBanda.Nombre:"N/A").AppendLine();
            sb.Append("Wan:").Append(this.wan).AppendLine();
            sb.Append("Loopback:").Append(this.loopb).AppendLine();
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
        public override void construir(int id)
        {
            base.construir(id);
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var enlace = dbc.EnlaceComunicaciones.Where(c => c.UnidadConf.id_uc==id).SingleOrDefault();
                    if (enlace != null)
                    {                        
                        if (enlace.AnchoBanda != null)
                        {
                            this.AnchoBanda = new AnchoBandaVista
                            {
                                Id = enlace.AnchoBanda.id_ab,
                                Nombre = enlace.AnchoBanda.nombre
                            };
                        }
                        this.Proveedor = new ProveedoresVista
                        {
                            Id = enlace.ProveedoresEnlace.id_proveedor,
                            Nombre = enlace.ProveedoresEnlace.nombre
                        };
                        this.Estado = new EstadoEquipoVista
                        {
                            idEstado = enlace.Estado_UC.id_estado,
                            Nombre = enlace.Estado_UC.descripcion
                        };
                        if (enlace.TipoEnlace != null)
                        {
                            this.TipoEnlace = new TiposEnlaceVista
                            {
                                Id = enlace.TipoEnlace.id_tipo_enlace,
                                Nombre = enlace.TipoEnlace.nombre
                            };
                        }
                        this.nroReferencia = enlace.nro_ref;
                        this.wan = enlace.wan;
                        this.loopb = enlace.loopback;
                        this.lan = enlace.lan;
                        this.descripcion = enlace.descripcion;
                        if (enlace.ContactosOperaciones != null)
                        {
                            this.direccion = enlace.ContactosOperaciones.direccion;
                            this.nContacto = enlace.ContactosOperaciones.nombre;
                            this.Te = enlace.ContactosOperaciones.te;
                        }
                        this.Jurisdiccion = new soporte.Models.ClasesVistas.JurisdiccionVista
                        {
                            idJurisdiccion = enlace.UnidadConf.Jurisdiccion.id_jurisdiccion,
                            Nombre = enlace.UnidadConf.Jurisdiccion.descripcion
                        };
                        this.Dependencia = new DependenciaVista
                        {
                            idDependencia = enlace.Dependencia.id_dependencia,
                            Nombre = enlace.Dependencia.descripcion
                        };
                        this.nombre = enlace.UnidadConf.nombre;
                        this.observaciones = enlace.UnidadConf.observaciones;
                    }
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}