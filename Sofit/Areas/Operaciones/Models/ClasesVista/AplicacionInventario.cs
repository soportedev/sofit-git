﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Operaciones.Models.ClasesVista
{
    public class AplicacionInventario:IEquipoInventario
    {
        public string Version { get; set; }
        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var varios = dbc.Varios.Where(c => c.id_uc == id).Single();
                this.Version = varios.modelo;
            }
        }
        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Nombre:").Append(Oblea).AppendLine();
            sb.Append("TipoElemento:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Varios:").Append(this.Version).AppendLine();
            sb.Append("Jurisdicción:").Append(this.Jurisdiccion.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}