﻿using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Linq;

namespace soporte.Areas.Operaciones.Models
{
    public class EnlacesEnIncidencia:IEquiposEnIncidencia
    {       
                      
        public EnlacesEnIncidencia()
        {            
        }
        private void LoadEnlaces()
        {
            
        }
        public override soporte.Models.Statics.Responses agregarEquipoAIncidencia(string oble, int id_area, int idTecnico)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses derivarEquipo(int idEq, Soporte.Models.Interface.AreaEquipo area, string observaciones, int idUsuario, int idTecnico)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses buscarEqPendientes(string texto)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses quitarEquipoAIncidencia(string oblea)
        {
            Responses result = new Responses();
            EnlaceOperaciones en = enlaces.Where(c => c.Nombre == oblea).Single();
            enlaces.Remove(en);
            result.Info = "ok";
            return result;
        }

        public override soporte.Models.Statics.Responses cargarSolucionEquipo(int idUsuario, int idEquipo, int tipoResolucion, int tecnico, int turno, int? imagen, string nombref, string observacion, bool update)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses terminarEquipo(int id_eq, int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override Soporte.Models.EquipoEnIncidente getEquipoXOblea(string oblea)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses notificarEquipo(int idEquipo, string[] referentes, string firma, string cuerpo, string subject)
        {
            throw new NotImplementedException();
        }

        public override Soporte.Models.EquipoEnIncidente getEquipoXId(int idEquipo)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cambioEstadoEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cambioAreaEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        internal override bool tieneEquiposActivos()
        {
            return enlaces.Count > 0;
        }

        internal override string getEquiposSinTerminar()
        {
            return enlaces[0].Nombre;
        }

        internal override Responses puedeReactivarTicket()
        {
            return new Responses { Info = "ok" };
        }
    }
}