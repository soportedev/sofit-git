﻿using Sofit.DataAccess;
using soporte.Models.Statics;
using System.Linq;

namespace soporte.Areas.Operaciones.Models
{
    public class TiemposOperaciones
    {
        public int tAIngresados { get; set; }
        public int tEIngresados { get; set; }
        public int tAPendiente { get; set; }
        public int tEPendiente { get; set; }
        public int tAAdm { get; set; }
        public int tEAdm { get; set; }
        public int tAClaro { get; set; }
        public int tEClaro { get; set; }
        public int tAEpec { get; set; }
        public int tEEpec { get; set; }
        public int tATelecom { get; set; }
        public int tETelecom { get; set; }
        public int tASes { get; set; }
        public int tESes { get; set; }
        public int tCriticoIncidente { get; set; }

        public TiemposOperaciones()
        {
        using (var dbc = new IncidenciasEntities())
            {
                TOperaciones t = (from v in dbc.TOperaciones
                                  select v).FirstOrDefault();
                var ts = from to in dbc.TiemposGral
                         select to;
                if (t != null)
                {
                    this.tAAdm = t.tAAdm;
                    this.tAClaro = t.tAClaro;
                    this.tAEpec=t.tAEpec;
                    this.tAIngresados = t.tAIngresados;
                    this.tAPendiente = t.tAIngresados;
                    this.tEPendiente = t.tEIngresados;
                    this.tASes = t.tASes;
                    this.tATelecom = t.tATelecom;
                    this.tEAdm = t.tEAdm;
                    this.tEClaro = t.tEClaro;
                    this.tEEpec = t.tEEpec;
                    this.tEIngresados = t.tEIngresados;
                    this.tESes = t.tESes;
                    this.tETelecom = t.tETelecom;
                    tCriticoIncidente = ts.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                }
            }
        }
        internal soporte.Models.Statics.Responses setTiempos(int tAIng, int tEIng, int tAAdm, int tEAdm, int tAClaro, int tEClaro, int tAEpec, int tEEpec, int tATelecom, int tETelecom, int tASes, int tESes)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                TOperaciones t = (from v in dbc.TOperaciones
                                  select v).FirstOrDefault();
                if (t != null)
                {
                    t.tAAdm = tAAdm;
                    t.tAClaro = tAClaro;
                    t.tAEpec = tAEpec;
                    t.tAIngresados = tAIngresados;
                    t.tASes = tASes;
                    t.tATelecom = tATelecom;
                    t.tEAdm = tEAdm;
                    t.tEClaro = tEClaro;
                    t.tEEpec = tEEpec;
                    t.tEIngresados = tEIngresados;
                    t.tESes = tESes;
                    t.tETelecom = tETelecom;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            return result;
        }
    }
}