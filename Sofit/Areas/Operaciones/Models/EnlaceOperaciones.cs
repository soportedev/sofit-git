﻿using soporte.Areas.Soporte.Models;
using soporte.Models.ClasesVistas;
using System;

namespace soporte.Areas.Operaciones.Models
{
    public class EnlaceOperaciones
    {         
        public int IdEquipo { get; set; }
        public int IdEnlace { get; set; }        
        public DateTime Fecha { get; set; }      
        public string Proveedor { get; set; }
        public Jurisdicciones Jurisdicciones { get; set; }
        public string Nombre { get; set; }
        public string TipoEnlace { get; set; }
        public string Descripcion { get; set; }        
        public string nroRef { get; set; }
        public string lan { get; set; }
        public string anchoBanda { get; set; }
        public string wan { get; set; }
        public string loopback { get; set; }
        public EstadoEquipo Estado { get; set; }
        public Contacto Contacto { get; set; }
    }
}