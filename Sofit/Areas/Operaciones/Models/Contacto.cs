﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models
{
    public class Contacto
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string te { get; set; }
    }
}