﻿using soporte.Areas.Soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models
{
    public class EstadoEnlaceEnReparacion:EstadoEquipo
    {
        public EstadoEnlaceEnReparacion(int idEstado, string nombre)
        {
            base.idEstado = idEstado;
            base.nombreEstado = nombre;
        }
        public override soporte.Models.Statics.Responses cargarSolucion(int idUsuario, string fecha, int idTipoResolucion, int tecnico, int turno, int? imagen, string observacion)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses agregarAIncidente(Soporte.Models.Interface.AreaEquipo area)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses SolucionarEquipo()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses pasarArea(Soporte.Models.Interface.AreaEquipo area)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses suspender()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses paraGarantia()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses enGarantia()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses updateDb()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses notificarMail()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses setUbicacion(int idArmario)
        {
            throw new NotImplementedException();
        }
    }
}