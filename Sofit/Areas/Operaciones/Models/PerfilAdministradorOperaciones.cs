﻿using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models
{
    public class PerfilAdministradorOperaciones:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            string[] url = page.Split('/');
            if(url[0].Equals("Operaciones"))return soporte.Models.Statics.Constantes.AccesosPagina.Full;
            else return soporte.Models.Statics.Constantes.AccesosPagina.NoAccess;
        }

        public override string getHomePage()
        {
            return "Operaciones/Incidentes";
        }
    }
}