﻿using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Operaciones.Models
{
    public class PerfilOperadorOperaciones:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            switch (page)
            {
                case "Operaciones/Inventario":return soporte.Models.Statics.Constantes.AccesosPagina.Full;                    
                case "Operaciones/Historicos": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Operaciones/Incidentes": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Operaciones/NuevoTicket": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Operaciones/Config": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                default: return soporte.Models.Statics.Constantes.AccesosPagina.NoAccess;                    
            }
        }

        public override string getHomePage()
        {
            return "Operaciones/Incidentes";
        }
    }
}