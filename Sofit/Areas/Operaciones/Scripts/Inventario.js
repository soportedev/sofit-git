﻿$(function () {
    window.modal = document.getElementById('myModal');
    var btn = $("span#works_btn");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0]; // Only for first close class
    // Get the <send> element that closes the modal
    var send = document.getElementById("SendForm")[0]; // Only for first id	
    // When the user clicks the button, open the modal
    btn.each(function () {
        $(this).on("click", function () {
            modal.style.display = "block";
        });
    });
    $('#EquipoNuevo').click(function () {        
        clearForm();
        nuevoState();                   
    });
    $('#AceptarCambios').click(function () {
        // Ventana de confirmacion
        $('.classMarca').addClass('hide');
        $('.classEnviar').removeClass('hide');
        $('span#works_btn').trigger(jQuery.Event("click"));
    });
    $('#extraInfo').on('click , change', showingData);
    $('#CancelarCambios').on('click', initialState);
    $('#searchButton').on('click', function () {
        var name = $('#tBuscar').val();
        getEquipo(name);
    });
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
    // When the user clicks on <span> (x), close the modal
    /*send.onclick = function() {
		modal.style.display = "none";
	}*/

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    //Tipo Equipo    
    $.ajax({
        url: 'Inventario/TiposEquipo',
        type: 'post',
        dataType: 'json',        
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                mapping = new Object();
                mapping.value = raw[i].id;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            // Function
            $('#idTipo').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {                    
                    $(this).attr('data-id', ui.item.value);
                    var texto = ui.item.label;
                    switch (texto) {
                        case "Enlace": $('div.divEnlace').show();
                            break;
                        default: $('div.divEnlace').hide();
                            break;
                    }
                    $(this).val(texto);
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable == 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If inpit is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            });
        }
    });
    //Tipos Enlace
    $.ajax({
        url: 'Inventario/TiposEnlace',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            //NProgress.start();
        },
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].Id;
            }
            // Function
            $('#tTipoE').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable == 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If inpit is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //Anchos de Banda
    $.ajax({
        url: 'Inventario/AnchosBanda',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].Id;
            }
            $('#tBw').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                if (disable == 'readonly') {
                    e.preventDefault();
                }
                else {
                    $(this).val('');
                    $(this).autocomplete('search');
                }
            })
        }
    });
    //-->
    //Proveedores
    $.ajax({
        url: 'Inventario/Proveedores',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].Id;
            }
            $('#idProveedor').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                if (disable == 'readonly') {
                    e.preventDefault();
                }
                else {
                    $(this).val('');
                    $(this).autocomplete('search');
                }
            })
        }
    });
    //-->
    //Jurisdicciones
    $.ajax({
        url: '/Service/Jurisdicciones',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {                
                mapping = new Object();
                mapping.value = raw[i].idJurisdiccion;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            $('#Jurisdiccion').autocomplete({
                source: source,
                minLength: 0,
                focus: function () {
                    $(this).attr('data-id', "");
                },
                select: function (event, ui) {                    
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    getUbicacion();
                    event.preventDefault();
                },
                close: function () {
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                if (disable == 'readonly') {
                    e.preventDefault();
                }
                else {
                    $(this).val('');
                    $(this).autocomplete('search');
                }
            })
        }
    });
    //--> 
    // Ubicacion
    window.getUbicacion = function () {
        $('#Ubicacion').val('');
        var idJur = $("#Jurisdiccion").attr("data-Id");
        // Send data
        // Ajax
        $.ajax({
            data: { idJur: $("#Jurisdiccion").attr("data-Id") },
            url: '/Service/Dependencias',
            type: 'post',
            dataType: 'json',
            async: false,
            beforeSend: function () {
                // Show Loading
                //NProgress.start();
            },
            success: function (response) {
                // Values
                var raw = response;
                var source = [];
                var mapping = {};
                for (var i = 0; i < raw.length; ++i) {
                    source.push(raw[i].Nombre);
                    mapping[raw[i].Nombre] = raw[i].idDependencia;
                }
                // Function
                $('#Ubicacion').autocomplete({
                    source: source,
                    minLength: 0,
                    select: function (event, ui) {
                        var dataId = mapping[ui.item.value]
                        $(this).attr('data-id', dataId);
                    },
                    close: function () {
                        // Finish selection and make a blur
                        $(this).blur();
                    }
                }).trigger('search');
            },
            // On error
            error: function (response, textStatus) {
                // NOTIFICAION
            }
        });
        // Ajax
        // Send data
    };
    $('#Ubicacion').on('click', function () {
        var check = $(this).data('ui-autocomplete') != undefined;
        var disable = $(this).attr('readonly');
        // Check input status
        if (disable == 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {            
                getUbicacion();
                $(this).val('');
                $(this).autocomplete('search');            
        }
    });
    //-->
    //Estados
    $.ajax({
        url: 'Inventario/EstadosEnlace',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idEstado;
            }
            $('#idEstado').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                if (disable == 'readonly') {
                    e.preventDefault();
                }
                else {
                    $(this).val('');
                    $(this).autocomplete('search');
                }
            })
        }
    });
    //-->    
    //Searching
    $('#tNombre,#tBuscar').keyup(function (e) {
        //if ((e.which > 36 & e.which<41)|e.which==13) return;
            //$(this).val($(this).val().toUpperCase())
    });
    $('#tBuscar').autocomplete({
        minLength: "2",
        source: (
        function (request, response) {
            var uri = 'Inventario/EquipoXNombre';            
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: uri,
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    else {
                        var suggestions = [];
                        $.each(data, function (i, val) {
                            var obj = {};
                            obj.value = val.Id;
                            obj.Descripcion = val.Descripcion;
                            obj.Ip = val.IP;
                            obj.label = val.Nombre;
                            obj.Referencia = val.Referencia;                            
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    }
                }
            });
        }),
        select: function (e, ui) {
            e.preventDefault();
            $('#tBuscar').val(ui.item.label);
            getEquipo(ui.item.Nombre);
            return false;
        },
        focus: function (e, ui) {
            e.preventDefault();
            $('#tBuscar').val(ui.item.label);
            return false;
        }
    });
    //-->
    //Incidentes Activos
    $('div.iconContainer').on({
        click: function () {
            var divenlaces = $(this).parent();
            var datarelated = divenlaces.siblings('div');
            var divIcon = $(this).find('div.ui-icon');
            var h2 = $(this).siblings('h2');
            h2.removeClass('enlaceRotated normal');
            divIcon.removeClass('ui-icon-circle-triangle-e ui-icon-circle-triangle-w')
            var w = divenlaces.width();
            if (w == 300) {//achico
                datarelated.css('left', '40px');
                divenlaces.css('width', '35px');
                h2.addClass('enlaceRotated');
                divIcon.addClass('ui-icon-circle-triangle-e')
            } else {//agrando
                datarelated.css('left', '305px');
                divenlaces.css('width', '300px');
                divIcon.addClass('ui-icon-circle-triangle-w')
                h2.addClass('normal');
            }
        },
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
        }
    });
    //-->
    $('#editData').click(function () {
        editState();
    });
    $('#SendForm').click(function (e) {
        e.preventDefault();       
        EnviarDatos();
    });
    //Arbol dinamic
    $('#menu').on({
        click: function (event) {
            event.preventDefault();
            if (this == event.target) {
                $(this).siblings('ul').toggle();
                var icon = $(this).siblings('div.ui-icon');
                icon.removeClass('ui-icon-folder-collapsed ui-icon-folder-open');
                if ($(this).siblings('ul').length == 0) icon.addClass('ui-icon-folder-collapsed');
                else { ($(this).siblings('ul').is(':hidden')) ? icon.addClass('ui-icon-folder-collapsed') : icon.addClass('ui-icon-folder-open') }
            }
            return false;
        }
    }, 'li>a').css('cursor','pointer');
    $('#menu').on('click', 'a.itemDescripcion', function (e) {
        $('a.itemDescripcion', '#menu').removeClass('ui-state-highlight');
        $(this).addClass('ui-state-highlight');
        var name = $(this).siblings('span.hide').html();
        getEquipo(name);
    });
    //-->    
    clearForm();
    getArbol();
});//READY
//Getting Enlace
function getEquipo(nombre) {
    if(!nombre)
    var nombre = $('#tBuscar').val();
    $.ajax({
        url: '/Service/GetEquipoInventario',
        data: { 'oblea': nombre },
        type:'Post',
        dataType: 'json',
        beforeSend: function () {
            clearForm();
            NProgress.start();
        },
        success: function (data) {
            NProgress.done();
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            CompletarInfo(data);
            if (data.TipoEquipo.Nombre == 'Enlace') {
                viewState();
                $('div.divEnlace').show();
                CompletarEnlace(data);
            } else {
                if (data.TipoEquipo.Nombre == 'Software de Base' | data.TipoEquipo.Nombre == 'Aplicación de Usuarios') {
                    viewState();
                    $('div.divEnlace').hide();
                } else viewOnly();
            }
        }
    });
}
//-->
//Arbol getting
function getArbol() {    
    $.ajax({
        url: 'Inventario/Arbol',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            var data = response;
            var ul = $('#menu');
            ul.html('');
            $.each(data, function (i, v) {
                var icon = $('<div>');
                var b = $('<a>').html(v.Proveedor);
                $(icon).addClass('ui-icon left');
                var li = $('<li>').append(icon).append(b);
                ul.append(li);
                $.each(v.hijos, function (j, m) {
                    var a = $('<a>').addClass('itemDescripcion');
                    var iconb = $('<div>').addClass('ui-icon ui-icon-document left');                    
                    var inputH = $('<span>').addClass('hide');
                    inputH.html(m.Nombre);
                    a.html(m.Descripcion);
                    var li2 = $('<li>').append(iconb).append(a).append(inputH);
                    var newUl = $('<ul>');
                    newUl.append(li2);
                    li.append(newUl);
                })
            });
            $('#menu>li>a').trigger('click');
        }
    });
}
function CompletarEnlace(response)
{
    if (response.AnchoBanda != null) $('#tBw').val(response.AnchoBanda.Nombre);
    if (response.Proveedor != null) $('#idProveedor').val(response.Proveedor.Nombre).attr('data-id',response.Proveedor.Id);
    if (response.TipoEnlace != null) $('#tTipoE').val(response.TipoEnlace.Nombre).attr('data-id', response.TipoEnlace.Id);
    $('#tDesc').val(response.descripcion);
    $('#idReferencia').val(response.nroReferencia);
    if (response.Tipo != null) $('#tTipoE').val(response.Tipo.Nombre);
    $('#tAtelefonos').val(response.Te);
    $('#tDireccion').val(response.direccion);
    $('#tNContacto').val(response.nContacto);
    $('#tWan').val(response.wan);
    $('#tLoop').val(response.loopb);
    $('#tLan').val(response.lan);
}
function CompletarInfo(response) {
    $('#idEquipo').val(response['idEquipo'])
    $('#Oblea').val(response['Oblea'])
    if (response.NroSerie !== null) {
        $('#NroSerie').val(response['NroSerie'])
    }
    $('#idTipo').val(response['TipoEquipo']['Nombre']).attr('data-id', response['TipoEquipo']['id'])
    if (response.Marca != null & response.Marca != undefined) {
        $('#Marca').val(response['Marca']['Nombre']).attr('data-id', response['Marca']['idMarca'])
    }
    $('#Modelo').val(response.Modelo);
    if (response.idModelo) {
        $('#Modelo').attr('data-id', response.idModelo);
    }
    if (response.Jurisdiccion != null & response.Jurisdiccion != undefined) {
        $('#Jurisdiccion').val(response['Jurisdiccion']['Nombre']).attr('data-id', response['Jurisdiccion']['idJurisdiccion'])
    }
    if (response.Dependencia != null & response.Dependencia != undefined) {
        $('#Ubicacion').val(response['Dependencia']['Nombre']).attr('data-id', response['Dependencia']['idDependencia'])
    }
    if (response['Estado'] !== null) {
        $('#Estado').val(response['Estado']['Nombre']).attr('data-id', response['Estado']['idEstado'])
    }
    $('#Observaciones').val(response['Observaciones'])

}
function clearForm() {
    $('input, textarea','#content-box').each(function () {
        $(this).val('');
        $(this).removeClass('error');
    });
    $('input[type="checkbox"]').each(function () {
        $(this).prop('checked', false).trigger('change');
    });
    OcultarRelaciones();
}

function enableControls() {
    $('input:text', '#content-box').each(function () {
        $(this).removeAttr('readonly');
    });
    $('textarea').each(function () {
        $(this).removeAttr('readonly');
    });
    $('input[type="checkbox"]').each(function () {
        $(this).removeClass('readonly')
    });
}
function showingData() {
    var inp = $(this);
    if (inp.hasClass('readonly')) return false;
    var div = $(inp).siblings('div.networkInfo');
    var checked = $(inp).attr('checked');
    if (checked == 'checked') {        
        div.show();
    } else {
        div.hide();
    }
}
// Actualizar Datos
function EnviarDatos() {
    var data = {};
    // Check if the page is Altas or Update
    if (window.Altas) {        
        data.url = 'Inventario/NuevoEc'
    } else {
        // URL for update
        data.url = 'Inventario/UpdateEc';
        data.idEstado = $('#Estado').attr('data-id');
    }
    //-->        
    // Hide modal
    window.modal.style.display = "none";
    // Validations
    var tipo=$('#idTipo').val();
    if (tipo) {
        var resTipo = 'ok';
        $('#idTipo').removeClass('error');
    }
    else {
        $('#idTipo').addClass('error');
    }
    var oblea = $('#Oblea').val();
    var resProveedor, resOblea, resJurisdiccion, resUbicacion,resDescripcion;
    switch (tipo) {
        case "Enlace":
            if (regEquipoOperaciones.test(oblea)) {
                var resOblea = 'ok';
                $('#Oblea').removeClass('error');
            } else {
                $('#Oblea').addClass('error');
            }
            var descripcion = $('#tDesc').val();
            if (/\w{5,}/.test(descripcion)) {
                var resDescripcion = 'ok';
                $('#tDesc').removeClass('error');
            } else {
                $('#tDesc').addClass('error');
            }
            var $prove = $('#idProveedor');
            if ($prove.val()) {
                resProveedor = 'ok';
                $prove.removeClass('error');
            } else $prove.addClass('error');
            break;
        default:
            if (/\w{3,}/.test(oblea)) {
                var resOblea = 'ok';
                $('#Oblea').removeClass('error');
            } else {
                $('#Oblea').addClass('error');
            }
            resProveedor = 'ok';
            break;
    }   
    if ($('#Jurisdiccion').val()) {
        resJurisdiccion = 'ok';
        $('#Jurisdiccion').removeClass('error');
    }
    else {
        $('#Jurisdiccion').addClass('error');
    }
    if ($('#Ubicacion').val()) {
        resUbicacion = 'ok';
        $('#Ubicacion').removeClass('error');
    }
    else {
        $('#Ubicacion').addClass('error');
    }    
    var $descripcion = $('#tDesc');    
    var $tBw = $('#tBw');   
    
    
    //-->
    if ((resOblea && resJurisdiccion && resTipo && resUbicacion && resProveedor && ((tipo=='Enlace' & resDescripcion == 'ok')|tipo!='Enlace')) ){
        // Get data
        data.nombre = oblea;
        data.idTipo = $('#idTipo').attr('data-id');
        data.observacion = $('#Observaciones').val();        
        data.idProveedor = $('#idProveedor').attr('data-id');
        data.jurisdiccion = $('#Jurisdiccion').attr('data-id');
        data.ubicacion = $('#Ubicacion').attr('data-id');
        data.nroReferencia = $('#idReferencia').val();
        data.lan = $('#tLan').val();
        if ($tBw.val()) {
            data.bw = $('#tBw').attr('data-id');
        }
        data.wan = $('#tWan').val();
        data.loopb = $('#tLoop').val();
        data.descripcion = $('#tDesc').val();
        data.version = $('#Modelo').val();
        data.nContacto = $('#tNContacto').val();
        data.direccion = $('#tDireccion').val();
        data.Te = $('#tAtelefonos').val();
        data.tipoEnlace = $('#tTipoE').attr('data-id');
        //-->      
        // Send data
        // Ajax
        $.ajax({
            data: data,
            url: data.url,
            type: 'post',
            dataType: 'json',            
            success: function (response) {
                if (response.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                // create the notification
                var message, type;
                if (response['Info'] == 'ok') {
                    var message;
                    if(window.Altas) {
                        if (tipo == "Enlace") {
                            message = "Enlace agregado!!!";
                        } else {
                            message = "Software agregado!!!";
                        }
                    } else {
                        if (tipo == "Enlace") {
                            message = "Enlace actualizado!!!";
                        } else {
                            message = "Software actualizado!!!";
                        }
                    }
                    mostrarExito("Se actualizaron los datos del enlace");
                    initialState();
                    getArbol();
                } else {
                    mostrarError(response.Detail);
                }               
            },
            error: function (response, textStatus) {
                mostrarError(textStatus);
            }
        });
        // Ajax
        // Send data
        //-->
    } else {        
        mostrarError("Complete los campos obligratorios o<br>revise que los datos ingresados sean correctos.");
    }
}
// Actualizar Datos
function obtenerDependencias(idJur) {    
    $.ajax({
        data: { 'idJur': idJur },
        url: '/Service/Dependencias',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            var mapping = {};
            source.push(mapping);
            for (var i = 0; i < raw.length; ++i) {
                mapping = new Object();
                mapping.value = raw[i].idDependencia;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            $('#Ubicacion').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).trigger('search');

        }
    });
}

function viewOnly() {
    window.State = 'showing';
    disableControls();
    OcultarRelaciones();
    $('div.right nav a').fadeOut(0);
    $('#EquipoNuevo').fadeIn(0);
}
function initialState() {
    window.State = 'initial';
    clearForm();    
    $('#AceptarCambios,#CancelarCambios').fadeOut(0);
    $('#EquipoNuevo').fadeIn(0);
    $('#LineaBase').hide();
}
function viewState() {
    window.State = 'showing';
    disableControls();
    OcultarRelaciones();
    $('#AceptarCambios,#CancelarCambios').fadeOut(0);
    $('#EquipoNuevo, li.estadoEnlace,#editData').fadeIn(0);
    $('#LineaBase').show();
}
function editState() {
    window.Altas = false;
    window.State = "edit";
    OcultarRelaciones();
    enableControls();
    $('#AceptarCambios,#CancelarCambios,li.estadoEnlace').fadeIn(0);
    $('#EquipoNuevo').fadeOut(0);
    $('#editData').fadeOut(0);
}
function nuevoState() {
    window.Altas = true;
    window.State = "nuevo";
    $('div.divEnlace').hide();
    OcultarRelaciones();
    enableControls();
    $('#Estado').attr('readonly', 'readonly');
    $('#AceptarCambios,#CancelarCambios').fadeIn(0);
    $('#EquipoNuevo,#editData,li.estadoEnlace').fadeOut(0);
}
function disableControls() {
    $('input:text', '#content-box').each(function () {
        $(this).attr('readonly', 'readonly');
    });
    $('textarea').each(function () {
        $(this).attr('readonly', 'readonly');
    });
    $('input[type="checkbox"]').each(function () {
        $(this).addClass('readonly')
    });
}