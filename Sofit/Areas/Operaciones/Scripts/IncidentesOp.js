﻿$(function () {
    //Trabajos BandejaEntrada
    $('#Trabajos').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        //position: ['center', 150],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            clearTrabajosPanel(thisdialog);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            var ul = $(thisdialog).children('.ulForm');
            var ch = ul.children();
            switch (datos.nombreGrilla) {
                case 'grillaBandeja':
                    ul.children('li.entrada').show();
                    ul.children('li:not(.entrada)').hide();
                    break;
                case 'grillaOperaciones':
                    ul.children('li.oper').show();
                    ul.children('li:not(.oper)').hide();
                    ul.children('li.ticketContainer').show("fast", function () {
                        $("#esParaEquipo").attr("checked", false).trigger('change')
                    });
                    break;
                case 'grillaCorreo':
                    ul.children('li.correo').show();
                    ul.children('li:not(.correo)').hide();
                    ul.children('li.ticketContainer').show("fast", function () {
                        $("#esParaEquipo").attr("checked", false).trigger('change')
                    });
                    break;
            }
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('#idTipoResolucion').val(0);
            $('.error', this).hide();
            setSelectByVal($('#idTipoTrabajo'), 0);

        },
        buttons: {
            Aceptar: {
                class: 'aceptardialog',
                text:'Aceptar',
                click: function () {
                    var $thisButton = $(this).dialog('widget').find('button.aceptardialog');                    
                    $('.error', this).hide();
                    var fData = new FormData();
                    var datos = $(this).data('datos');
                    var thisdialog = $(this);
                    hayEnlace = false;
                    var idTecnico = $('#idTecnico').val();
                    var idTurno = $('#idTurno').val();
                    var observaciones = $('#observaciones').val();
                    var inpFile = $('#upFileRes');
                    var img = inpFile.get(0).files[0];
                    fData.append('imagen', img);
                    fData.append('idTecnico', idTecnico);
                    fData.append('idTurno', idTurno);
                    fData.append('observaciones', observaciones);
                    fData.append('idIncidente', datos.idIncidente);
                    if ($('#esParaEquipo').is(':visible')) {
                        var valorCheck = $('#esParaEquipo').is(':checked');
                        fData.append('esParaEquipo', valorCheck);
                        if ($('#esParaEquipo').is(':checked')) {
                            var els = $(thisdialog).find('div.obleaCont .idEquipo');
                            var sufix = 0;
                            $.each(els, function () {
                                fData.append('idEquipo.' + sufix, $(this).html());
                                sufix++;
                            });
                        }
                    }
                    fData.append('sobrescribir', $('#sobrescribir').is(':checked'));
                    var selTrabajo = $('li.res:visible select', thisdialog);
                    var ttrabajo = selTrabajo.val() == 0;
                    if (ttrabajo) {
                        $('#Trabajos').find('.message').html('Seleccione el tipo de Trabajo.');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    fData.append('tipoResolucion', selTrabajo.val());
                    if ($('li.enlaceContainer', thisdialog).is(':visible')) {
                        hayEnlace = true;
                        var nombre = regEquipoOperaciones.test($('#tNombreEnlace').val());
                        if (!nombre) {
                            $(thisdialog).find('.message').html('Seleccione el Nombre del Enlace.');
                            $('.error', thisdialog).show();
                            return;
                        }
                        var tfalla = $('#tipoFalla option:selected').val() == 0;
                        if (tfalla) {
                            $(thisdialog).find('.message').html('Seleccione el tipo de Falla.');
                            $('.error', thisdialog).show();
                            return;
                        }
                        var date = regDateTime.test($('#datetimeinput').val());
                        if (!date) {
                            $(thisdialog).find('.message').html('Verifique fecha y hora.');
                            $('.error', thisdialog).show();
                            return;
                        }
                        fData.append('tipoFalla', $('#tipoFalla option:selected').val());
                        fData.append('datetimeinput', $('#datetimeinput').val());
                        fData.append('idEquipo', $('#tNombreEnlace').siblings('span.idEquipo').html());
                        datos.url = "Incidentes/cargaFalla";
                    } else {
                        datos.url = "Incidentes/cargarSolucion";
                    }
                    var obs = $('#observaciones');
                    if ($.trim(obs.val()).length < 10) {
                        $(thisdialog).find('.message').html('Complete el campo de Observaciones (al menos 10 caracteres)');
                        $('.error', thisdialog).show();
                        return;
                    }

                    $.ajax({
                        url: datos.url,
                        data: fData,
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            NProgress.start();
                            $($thisButton).button('disable');
                        },
                        success: function (data) {
                            if (data != null) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarExito('Trabajo cargado!');
                                    var esteDialog = $('#Trabajos');
                                    esteDialog.dialog('close');                                
                                    actualizarPaneles('todos');
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            NProgress.done();
                            $($thisButton).button('enable').removeClass("ui-state-focus ui-state-hover");
                        }
                    });

                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        },
        resizeStop: function (ev, ui) {
            var originalSize = ui.originalSize;
            var size = ui.size;
            var textArea = $(ev.target).find('.tSolucion');
            var heightDif = originalSize.height - size.height;
            var widthDif = originalSize.width - size.width;
            textArea.height(textArea.height() - heightDif);
            //textArea.width(textArea.width() - widthDif);
        }
    });//-->TRABAJOS Ticket   
    /***********
    *TRABAJOS Enlaces
    ************/
    $('#TrabajosEnlace').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: false,
        //position: ['center', 150],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            clearTrabajosPanel(thisdialog);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '109px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('input[type=text]', '.nombreEnlace').val(datos.nroEnlace);
            $('.error', this).hide();
        },
        buttons: {
            Aceptar: function () {
                var thisdialog = $(this);
                $('.error', this).hide();
                var flagOk=false;
                var datos = $(this).data('datos');
                var ttrabajo = $('.tipoSolucion option:selected', thisdialog).val() == 0;
                var sttrabajo = $('.tipoSolucion option:selected', thisdialog).text();
                if (ttrabajo) {
                    $(thisdialog).find('.message').html('Seleccione el tipo de Solución.');
                    $('.error', thisdialog).show();
                    return;
                }              
                var date = regDateTime.test($('#datetimeI').val());
                if (!date&sttrabajo=='Fin Falla') {
                    $(thisdialog).find('.message').html('Verifique fecha y hora.');
                    $('.error', thisdialog).show();
                    return;
                }
                var obs = $('.observaciones',thisdialog);
                if ($.trim(obs.val()).length < 10) {
                    $(thisdialog).find('.message').html('Complete el campo de Observaciones (al menos 10 caracteres)');
                    $('.error', thisdialog).show();
                    return;
                }
                var nData = {
                    nroIncidente: datos.nroIncidente, idIncidente: datos.idIncidente, nombreGrilla: datos.nombreGrilla,
                    idEquipo: datos.idEquipo
                };
                $(thisdialog).appendSerialToObject(nData);
                if (sttrabajo == 'Fin Falla') {
                    var divdialog = $('<div>').append("El tipo de solución ingresada va a cerrar el incidente " +
                        datos.nroIncidente + " Con la fecha" + $('#datetimeI').val() + ". Está Seguro?").dialog({
                            width: '250px',
                            show: 'slide',
                            //position: 'center',
                            open: function () {
                                $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                            },
                            buttons: {
                                Aceptar: function () {
                                    enviarTrabajo(nData);
                                    $(this).dialog('destroy');
                                }
                                ,
                                Cancelar: function () {
                                    $(this).dialog('destroy');
                                    return;
                                }
                            }
                        });//dialog
                } else enviarTrabajo(nData);
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });//TRABAJOS Enlace
    
    //Enviar trabajo
    function enviarTrabajo(nData) {
        $.ajax({
            url: "Incidentes/cargaTrabajoEnlace",
            data: nData,
            type: 'POST',
            datatype: 'json',
            success: function (data) {
                if (data != null) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Trabajo cargado!');
                        $('#TrabajosEnlace').dialog('close');
                        actualizarPaneles('todos');
                    }
                    else {
                        mostrarError(data.Detail);
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                $(".ajaxGif").css({ 'display': 'none' });
            }
        });
    }
    //Timepicker
    $('#datetimeinput,#datetimeI').datetimepicker({
        currentText: "Ahora",
        closeText: "Listo",
        timeText: "Tiempo",
        hourText: "Horas",
        minuteText:"Minutos"
        ,dateFormat:"dd/mm/yy"     
    });
    //
    
    $('#idTipoTrabajo').on('change', function () {
        $('#tObleaSolucion').val('');
        var val = $(this).val();
        switch (val) {            
            case '3':
                $('li.ticketContainer, li.resentrada').show("fast", function () {
                    $("#esParaEquipo").attr("checked", false).trigger('change')
                });
                $('li.enlaceContainer').hide();                
                break;
            default:
                $('li.enlaceContainer').show();
                $('li.ticketContainer, li.resentrada').hide();
                break;
        }        
    });   
    $("#tNombreEnlace").autocomplete({
        minLength: "2",
        source: (
        function (request, response) {            
            var tipoTrabajo = $('#idTipoTrabajo').val();
            if (tipoTrabajo == '0') {
                mostrarError('Seleccione el Tipo de Trabajo');
                return;
            }
            if (tipoTrabajo == '2') var proveedor = 'adm';
            else proveedor = 'otro';
            var term = { prefixText: request.term ,proveedor:proveedor};
            $.ajax({
                type: "POST",
                url: "Inventario/EnlaceXProveedor",
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    else {
                        var suggestions = [];
                        $.each(data, function (i, val) {
                            var obj = {};
                            obj.value = val.Id;
                            obj.label = val.Nombre;
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    }
                }
            });
        }),
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        focus: function (e, ui) { $(this).val(ui.item.label); return false;},
        select: function (e, ui) {            
            var spanId = $('<span class="hide idEquipo">').html(ui.item.value);            
            $('#bTrabajos').button("enable");
            $(this).val(ui.item.label);
            $(this).focus();
            $(this).parent().find('span.idEquipo').remove().end().append(spanId);
            return false;
        }
    });    
    $('li.derivarSelector').on('change', function () {
        var chec = $(this).find('input[type="checkbox"]');
        if (chec.is(':checked')) {
            showCombo();
        } else hideCombo();
    });    
});//READY
function ubicarIncidente(data) {
    var direccion = data.SubDireccion;
    if (direccion == "Operaciones") {
        var area = data.Area;
        switch (area) {
            case "Entrada Operaciones": doScroll(ventanas[0], data.Numero);
                break;
            default: doScroll(ventanas[1], data.Numero);
                break;
        }
    } else {
        mostrarInfo("Se encuentra en otra Subdirección!!!");
    }
}
function searchAutocomplete(request, response) {
    var texto = request.term;    
    texto = texto.toUpperCase();
   var uri = 'GetIncidentesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSession();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/****************
*OBTENER UBICACION
*****************/
function buscarEquiposInc(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    resString = resString.toUpperCase();
    if (!regEquipoOperaciones.test(resString) && !regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }    
    $.ajax({
        url: "Incidentes/UbicacionIncidente",
        data: datos,
        dataType: 'json',
        type: 'POST',
        beforeSend: function () {
            ajaxReady = false;
        },
        success: function (data) {
            if (data == null) {
                mostrarInfo("El Ticket no está activo");
                return;
            }
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }            
            if (data.SubDireccion == 'Operaciones') {        
                if (data.Estado == "Suspendido") {
                    doScroll(ventanas[3], data.Numero);
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Operaciones": doScroll(ventanas[1], data.Numero);
                            break;
                        case "Entrada Operaciones": doScroll(ventanas[0], data.Numero);
                            break;
                        case "Correo Electrónico": doScroll(ventanas[2], data.Numero);
                            break;
                    }
                }
            } else {
                mostrarInfo("El Ticket se encuentra en: " + data.SubDireccion);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            ajaxReady = true;
        }
    });                         //fin ajax
}
/**************
*CARGA TRABAJOS
***************/
function cargarTrabajos() {
    var t = $(this);
    var nro = [];
    var trs = {};
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Seleccione UN elemento");
    }
    else {
        var datos = nro[0].find('td').slice(1, 7);
        var idIncidente = datos[0].innerHTML;
        var nroIncidente = $(datos[1]).find('span').text();
        var nroEnlace = datos[2].innerHTML;
        var idEquipo = datos[3].innerHTML;
        var data = {};
        data.idIncidente = idIncidente;
        data.nroIncidente = nroIncidente;
        data.nombreGrilla = nombreGrilla;
        data.nroEnlace = nroEnlace;
        data.idEquipo = idEquipo;
        if (nombreGrilla == 'grillaEnlaces') {
            $('#TrabajosEnlace').data('datos', data).dialog('open');
        } else {
            $('#Trabajos').data('datos', data).dialog('open');
        }        
    }
}
function cerrarIncidente() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN incidente");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);

        if (ajaxReady) {
            var divdialog = $('<div>').append("Se va a cerrar el incidente " + $(trsel[1]).text() + ". Está Seguro?").dialog({
                width: '250px',
                show: 'slide',
                //position: 'center',
                open: function () {
                    $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                },
                buttons: {
                    Aceptar: function () {
                        $.ajax({
                            url: "Incidentes/cerrarIncidente",
                            data: { idIncidente: $(trsel[0]).text() },
                            type: 'POST',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarInfo('Se ha Cerrado el Incidente');
                                    exito = true;
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                $(divdialog).dialog('destroy');
                                if (exito) {
                                    actualizarPaneles('todos');
                                }
                            }
                        });               //fin ajax
                    },
                    Cancelar: function () { $(this).dialog('destroy') }
                }

            });


        } //fin ajaxready
    }
}
/*************************
*FUNCION ACTUALIZAR PANELES
*************************/
function actualizarPaneles(tabla) {
    if (window.ajaxReady) {
        $.ajax({
            url: "Incidentes",
            type: 'GET',
            beforeSend: function () {
                window.ajaxReady = false;
            },
            success: function (data) {
                var ventanas = window.ventanas;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                var results = new Array();
                results[0] = $(data).find('#grillaBandeja');
                results[1] = $(data).find('#grillaOperaciones');
                results[2] = $(data).find('#grillaCorreo');                
                results[3] = $(data).find('#grillaSuspendidos');
                results[4] = $(data).find('#grillaEnlaces');
                switch (tabla) {
                    case "todos":
                        ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        ventanas[3].update(results[3]);
                        ventanas[4].update(results[4]);
                        break;                    
                    default: ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        ventanas[3].update(results[3]);
                        ventanas[4].update(results[4]);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);

            },
            complete: function () {
                window.ajaxReady = true;
                $('p.ajaxGif').hide();
            }
        });//fin ajax
    }
}
function notificar(){}
function derivar() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var context = $(this).parents('ul.fly');
    var origen = grilla.attr('id');
    var idArea = $('.sArea', context).val();
    var textArea = $('.sArea option:selected', context).text();
    var idTecnico = $('.sTecnico', context).val();
    var fData = new FormData();

    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Incidente");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(3)').text();
        fData.append("idAreaDestino", idArea);
        fData.append("idTecnico", idTecnico);
        fData.append("idIncidente", id_inc);
        if (window.ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarIncidente",
                data: fData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    window.ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el Incidente a ' + textArea);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    window.ajaxReady = true;
                    if (exito) {
                        actualizarPaneles("todos");
                        $('p.ajaxGif').hide();
                    }
                }
            });
        }
    }
}
function derivarGar() {

}
function derivarEq() {

}
function solucionTotal() {
    var exito = false;
    var nro = [];
    var User = $('#user').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        alert("debe seleccionar al menos UN incidente");
    }
    else {
        var id_inc = [];
        for (i = 0; i < nro.length; i++) {
            id_inc.push($.trim(nro[i].find('td:nth-child(2)').text()));
        }
    }
}

function terminarEquipo() {
    
}
function asignarEquipo() {
    var exito = false;
    var nro = [];
    var User = $(this).parent().siblings().find('select.sTecnico option:selected').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));            
        }
        asignarTecnicoEnlace(id_enlace, User);
    }    
}
function tomarEquipo() {
    var exito = false;
    var nro = [];
    var User = $('#user').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));
        }
    }
    asignarTecnicoEnlace(id_enlace,User);
}
function asignarTecnicoEnlace(id_enlace,Tecnico)
{
    var flag = true;
    for (var i = 0; i < id_enlace.length; i++) {
        $.ajax({
            url: "Incidentes/asignarEnlace",
            data: "id_enlace=" + id_enlace[i] + "&tecnico=" + Tecnico,
            datatype: 'json',
            async:false,
            type: 'POST',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    flag = false;
                    return;
                }               
                if (data.Info != 'ok') {
                    mostrarError(data.Detail);
                    flag = false;
                    return;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
                flag = false;
                return;
            },
            ajaxError: function(xhr,ajaxOptions, thrownError)
            {
                mostrarError(thrownError);
                flag = false;
                return;
            }

        });              //fin ajax 
    }
    if (flag) {
        mostrarExito('Enlace Asignado a: ' + Tecnico);
        actualizarPaneles('todos');
    }
}
/************************
 TRAER INCIDENTES A LA VIDA
 ************************/
function incidenteToLife() {
    var exito = false;
    if (validarNoVacio($('#tIncToLive'))) {
        var nroInc = $('#tIncToLive').val();
        $.ajax({
            url: "Incidentes/incidentesToLife",
            type: 'POST',
            data: 'nroInc=' + nroInc,
            beforeSend: ajaxReady = false,
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    exito = true;
                    mostrarExito('El Incidente ' + nroInc + ' fue reactivado');
                    $('#tIncToLive').val('');
                }
                else mostrarError(data.Detail);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                if (exito) actualizarPaneles('usuarios');
            }
        });
    }
}
/**Search Autocomplete Cliente**/
function searchAutocompleteCliente(request, response) {
    var texto = request.term;
    var uri = 'GetClientesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR Cliente*/
function buscarCliente(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];

    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/incidenteXCliente",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("No hay Incidentes para ese cliente");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Mesa de Ayuda": if (data.Estado == "Ingresado") doScroll(ventanas[0], data.Numero);
                        else {
                            if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                            else { }
                        }
                            break;
                        default: doScroll(ventanas[2], data.Numero);
                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}

