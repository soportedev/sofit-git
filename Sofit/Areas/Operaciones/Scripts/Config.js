﻿function actualizarTiempos(e) {
    e.preventDefault();
    var flag = true;
    var boxes = new Array();
    boxes.push($('#tAIng'));
    boxes.push($('#tEIng'));
    boxes.push($('#tAvisoAdm'));
    boxes.push($('#tEmergAdm'));
    boxes.push($('#tAvisoClaro'));
    boxes.push($('#tEmergClaro'));
    boxes.push($('#tAvisoEpec'));
    boxes.push($('#tEmergEpec'));
    boxes.push($('#tAvisoTelecom'));
    boxes.push($('#tEmergTelecom'));
    boxes.push($('#tAvisoSes'));
    boxes.push($('#tEmergSes'));    
    for (var i = 0; i < boxes.length; i++) {
        boxes[i].removeClass('denied');
        if (!regExTest(regTime, boxes[i].val())) {
            boxes[i].addClass('denied');
            flag = false;
            break;
        }
    }    
    if (flag) {
        var datos = $('#ui-tabs-2 ul').serializeAnything();
        $.ajax({
            url: "Config/setTiempos",
            type: 'POST',
            data: datos,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Tiempos Actualizados');
                }
                else {
                    $('.message', '#tiempos').html(data.Detail);
                    $('.divError', '#tiempos').show('fast');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                $('.ajaxGif').css({ 'display': 'none' });
            }
        });
    }
    else {
        mostrarError('Verifique los datos y vuelva a intentar');
    }
}
function deleteEquipo(e) {
    e.preventDefault();
    var oblea = $.trim($('#tOblea').val());
    var este = $('#removeEquipo');
    $('.divError', este).hide();

    $.ajax({
        url: "Config/borrarEquipo",
        type: 'POST',
        data: 'nombre=' + oblea,
        beforeSend: function () {
            $('.error').hide('fast');
        },
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            if (data.Info == 'ok') {
                mostrarExito('Elemento Borrado');
            }
            else {
                mostrarError(data.Detail);                
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        },
        complete: function () {
            $('.ajaxGif').css({ 'display': 'none' });
        }
    });
}    
function bindsOnLoad(event, ui) {

}
$('#nvoUsuario').live('click', function (e) {
    e.preventDefault();
    $('#nvoUsuarioDialog').dialog('open');
});