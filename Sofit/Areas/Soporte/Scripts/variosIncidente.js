﻿/***************************
**TRABAJOS HANDLER DEL EVENTO
****************************/
function cargarTrabajos() {
    var nro = [];
    var trs = {};
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Seleccione UN elemento", 'error');
    }
    else {
        var $trabajosDialog = $("#Trabajos");
        clearTrabajosPanel($trabajosDialog);
        var datos = nro[0].find('td').slice(1, 7);
        var conforms = true;
        var oblea, idIncidente, nroIncidente, idEquipo;
        $('#lOblea').removeClass('equipoIncompleto');
        switch (nombreGrilla) {            
            case "divEnClonado": $trabajosDialog.find('li:not(.clon)').addClass('hide');
                oblea = $(datos[2]).text();
                idEquipo = datos[1].innerHTML;
                nroIncidente = datos[3].innerHTML;
                $.ajax({
                    datatype: 'json',
                    data: { 'oblea': oblea },
                    type: 'post',
                    async:false,
                    url: 'Inventario/verificarCompleto',
                    success: function (data) {
                        if (data.Info != "ok")
                        {
                            $('#lOblea').addClass('equipoIncompleto');
                        }
                    }
                });
                break;
            case "divEnLaboratorio": $trabajosDialog.find('li:not(.lab)').addClass('hide');
                oblea = $(datos[2]).text();
                idEquipo = datos[1].innerHTML;
                nroIncidente = datos[3].innerHTML;
                $.ajax({
                    datatype: 'json',
                    data: { 'oblea': oblea },
                    type: 'post',
                    async:false,
                    url: 'Inventario/verificarCompleto',
                    success: function (data) {
                        if (data.Info != "ok") {
                            $('#lOblea').addClass('equipoIncompleto');
                        }
                    }
                });
                break;
            case "divEntregar": $trabajosDialog.find('li:not(.entregar)').addClass('hide');
                oblea = $(datos[2]).text();
                idEquipo = datos[1].innerHTML;
                nroIncidente = datos[3].innerHTML;
                break;
            case "divEnAtUsuarios": $trabajosDialog.find('li:not(.incidentes)').addClass('hide');
                nroIncidente = $(datos[1]).find('span').text();
                break;
            case "divLogistica": $trabajosDialog.find('li:not(.ingresados)').addClass('hide');
                nroIncidente = $(datos[1]).find('span').text();
            case "divIngresados": $trabajosDialog.find('li:not(.incidentes)').addClass('hide');
                nroIncidente = $(datos[1]).find('span').text();
                break;

        }
        if ($('#lOblea').hasClass('equipoIncompleto')) {           
            var path='Inventario?nro='
            var myWindow = $("<a style='font:italic 12px sans-serif;' href='JavaScript:void()'>Completarlo...</a>");
            myWindow.click(function (event) {
                window.open(path + oblea, '', 'height=600,width=1000,menubar=no,toolbar=no,scrollbars=yes,status=no');
                window.focus();
            });
            $('#Trabajos').find('.message').html('El equipo está incompleto en el inventario...').append(myWindow);
            $('.error', '#Trabajos').show();
        }
        idIncidente = datos[0].innerHTML;
        var datos = {};
        datos.idIncidente = idIncidente;
        datos.nroIncidente = nroIncidente;
        datos.idEquipo = idEquipo;
        datos.oblea = oblea;
        datos.nombreGrilla = nombreGrilla;
        $trabajosDialog.data('datos', datos).dialog('open');
    }
}
/********************
*Notificaciones
*********************/
function notificar() {
    var nro = [];
    var datos;
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var origen = grilla.attr('id');
    var area = $.trim($(this).text());
    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN equipo", 'error');
    }
    else {
        var id_eq = nro[0].find('td:nth-child(3)').text();
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(5)').text();
        var nroOblea = nro[0].find('a', 'td:nth-child(4)').text();
        var qstr = {};
        var str="";
        qstr.idEq = id_eq;
        qstr.idInc = id_inc;
        $.ajax({
            url: "Incidentes/buscarDatosEquipo",
            data: qstr,
            async:false,
            type: 'POST',
            success: function (data) {
                datos = data;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    str = "No se encuentran resoluciones";
                }       
                data.idIncidente = id_inc;
                data.nroIncidente = nroInc;                
            }
        });        
        $('#Notificacion').data('datos', datos).dialog('open');
    }
}
function mostrarNotificaciones(e) {
    var datos = [];
    var row = $(e.target).parents('tr').find('td');
    var idEquipo = row.eq(2).text();
    var idIncidente = row.eq(1).text();
    datos.push(idEquipo);
    datos.push(idIncidente);
    $.ajax({
        data: 'nros=' + datos,
        url: 'Incidentes/Notificaciones',
        type: 'POST',
        success: function (res) {
            var dialog = $('<div style="white-space: pre-wrap;">');
            var result = JsonToUl2(res);
            dialog.append(result).dialog({
                width: '500px',
                show: 'slide',
                maxHeight: 350,
                position: { my: "right middle", at: "right bottom", of: e.target }

            }).click(function () { $(this).dialog('destroy') });
            $(".ui-dialog-titlebar").hide();
        },
        error: function (xhr, error, thrown) {
            mostrarError(error, 'error');
        }
    });
}
//-->fin notificaciones
/**FUNCION ACTUALIZAR PANELES**/
function actualizarPaneles(tabla) {
    if (ajaxReady) {
        $.ajax({
            url: "Incidentes",            
            type: 'GET',
            beforeSend: function() {
                ajaxReady = false;
            },
            success: function(data) {                
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                var results = new Array();
                results[0] = $(data).find('#gIIngresados');
                results[1] = $(data).find('#gIAtUsuarios');;
                results[2] = $(data).find('#gIAtLogistica');
                results[3] = $(data).find('#gEClonado');
                results[4] = $(data).find('#gELaboratorio');
                results[5] = $(data).find('#gIEEntregar');
                results[6] = $(data).find('#gESuspendidos');
                results[7] = $(data).find('#gEPGarantia');
                results[8] = $(data).find('#gEEnGarantia');
                switch (tabla) {
                    case "suspendidos": ventanas[6].update(results[6]);
                        ventanas[2].update(results[2]);
                        break;
                    case "ingresados": ventanas[0].update(results[0]);
                        break;
                    case "usuarios": ventanas[1].update(results[1]);
                        break;
                    case "logistica": ventanas[2].update(results[2]);
                        break;
                    case "incidentes":ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        break;
                    case "equipos":
                        ventanas[3].update(results[3]);
                        ventanas[4].update(results[4]);
                        ventanas[5].update(results[5]);
                        ventanas[6].update(results[6]);
                        break;
                    case "clonado": ventanas[3].update(results[3]);
                        break;
                    case "laboratorio": ventanas[4].update(results[4]);
                        break;
                    case "entregar": ventanas[5].update(results[5]);
                        break;                    
                    case "clonadoEntregar": ventanas[3].update(results[3]);
                        ventanas[5].update(results[5]);
                        break;
                    case "laboratorioEntregar": ventanas[4].update(results[4]);
                        ventanas[5].update(results[5]);
                        break;
                    case "usuaIngresados": ventanas[1].update(results[1]);
                        ventanas[0].update(results[0]);
                        break;
                    case "logIngresados": ventanas[0].update(results[0]);
                        ventanas[2].update(results[2]);
                        break;
                    case "logyUsuarios": ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        break;
                    case "suspendidosALab": ventanas[6].update(results[6]);
                        ventanas[4].update(results[4]);
                        ventanas[2].update(results[2])
                        break;
                    case "suspendidosALog": ventanas[6].update(results[6]);
                        ventanas[5].update(results[5]);
                        break;
                    case "suspendidosAClon": ventanas[6].update(results[6]);
                        ventanas[3].update(results[3]);
                        ventanas[2].update(results[2])
                        break;
                    case "aGarantiaLaboratorio": ventanas[7].update(results[7]);
                        ventanas[4].update(results[4]);
                        break;
                    case "aGarantiaLog": ventanas[7].update(results[7]);
                        ventanas[5].update(results[5]);
                        break;
                    case "aGarantiaClonado": ventanas[7].update(results[7]);
                        ventanas[3].update(results[3]);
                        break;
                    case "enGarantiaLaboratorio": ventanas[4].update(results[4]);
                        ventanas[8].update(results[8]);
                        break;
                    case "enGarantiaLog": ventanas[5].update(results[5]);
                        ventanas[8].update(results[8]);
                        break;
                    case "enGarantiaClonado": ventanas[3].update(results[3]);
                        ventanas[8].update(results[8]);
                        break;
                    case "entregarAGarantia": ventanas[5].update(results[5]);
                        ventanas[7].update(results[7]);
                        break;
                    case "entregarEnGarantia": ventanas[5].update(results[5]);
                        ventanas[8].update(results[8]);
                        break;
                    case "entregarLogistica": ventanas[2].update(results[2]);
                        ventanas[5].update(results[5]);
                        break;
                    case "aGarantiaEnGarantia": ventanas[7].update(results[7]);
                        ventanas[8].update(results[8]);
                        break;
                    case "todos":
                        ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        ventanas[3].update(results[3]);
                        ventanas[4].update(results[4]);
                        ventanas[5].update(results[5]);
                        ventanas[6].update(results[6]);
                        ventanas[7].update(results[7]);
                        ventanas[8].update(results[8]);
                        break;
                    default: ;
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                mostrarFinSesion();
            },
            complete: function() {
                ajaxReady = true;                
                $('p.ajaxGif').hide();
            }
        });//fin ajax
    }
}
function mostrarDatosEquipos(e) {
    var tr = $(e.target).closest('tr');
    var idEquipo = $(tr).find('td:eq(2)').html();
    $.ajax({
        data: 'idEquipo=' + idEquipo,
        url: 'Incidentes/getHistoriaEquipo',
        type: 'POST',

        success: function(res) {
            var result = JsonToUl(res);
            $('<div>').append(result).dialog(
                {
                    close: function () { $(this).dialog('destroy')},
                    width: 'auto',
                    show: 'slide',
                    position: { my: "left bottom", at: "right bottom", of: e.target }

                }).dblclick(function() { $(this).dialog('destroy') });
            //$(".ui-dialog-titlebar").hide();
        },
        error: function(xhr, error, thrown) {
            mostrarError(error, 'error');
        }
    });    
}
function JsonToUl2(res) {
    var result = "<ul class='quickView2'>";
    if (res.length == 0) { return "Sin resultados" };
    
    var prefix = '';
    $.each(res, function (i, item) {
        result += "<li><label class='ui-widget-header'>E-mails: </label><label>";
        $.each(item.mailsAdd, function (j, k) {
            result += prefix + k ;
            prefix = ', ';
        });
        result += "</label></li>";
        result += "<li><label class='ui-widget-header'>Fecha: </label><label>" + item.fecha + "</label></li>";
        result += "<li><label class='ui-widget-header'>Mensaje: </label><label>" + item.mensaje + "</label></li>";
        result += "<li><label class='ui-widget-header'>Enviado por: </label><label>" + item.enviadoPor + "</label></li>";
        result +="<li>-------------</li>"
    });
    result += "</ul></li></ul>";
    return result;
}
function JsonToUl(res) {
    var result = "<ul class='quickView'>";
    result += "<li><label>Oblea: </label><label>" + res.Oblea + "</label></li>";
    if (res.Tipo == "Pc") {
        result += "<li><label>Clave Admin.: </label><label>" + res.PasswordAdmin + "</label></li>";
        result += "<li><label>Clave Bios: </label><label>" + res.PasswordBios + "</label></li>";
        result += "<li><label><a href='JavaScript:void(0)'class='verInventario'data-o='"+res.Oblea+"'>Ver Inventario</a></label></li>";
    }
    result += "<li><label>";
    result += "<li><label>Tickets</label></li><li><ul class='subQuickView'>";
    $.each(res.Incidentes, function(i, item) {
    result += "<li><label>Nro.Ticket: </label><label>" + "<a href='JavaScript:void(0)'class='verIncidente'>" + item.Numero + "</a>" + "</label>" +
        "<label> - Fecha: </label><label>" + item.FechaInicioString + "</label></li>";
    })
    result += "</ul></li></ul>";
    return result;
}    
function Dimension(id, alto, ancho) {
    this.id = id;
    this.alto = alto;
    this.ancho = ancho;
}
/**FUNCION DERIVAR EQUIPOS**/
function derivarEq(event) {
    var nro = [];
    var ul = $(this).parents('ul');
    var area = $(ul).find('.sArea').val();
    var areaText = $(ul).find('.sArea option:selected').text();
    var tecnico = $(ul).find('.sTecnico').val();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var origen = grilla.attr('id');    
    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN equipo", 'error');
    }
    else {
        var id_eq = nro[0].find('td:nth-child(3)').text();
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(5)').text();
        var datos = "id_Eq=" + id_eq + "&area=" + area + "&id_inc=" + id_inc + "&idTec=" + tecnico;
        if (ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarEquipos",
                data: datos,
                type: 'POST',
                beforeSend: function () {
                    ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el equipo a ' + areaText);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail, 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError, 'error');
                },
                complete: function () {
                    ajaxReady = true;
                    if (exito) {
                        if (origen == 'divEnClonado') {
                            if (areaText == 'Logística') actualizarPaneles('clonadoEntregar');
                            if (areaText == 'Laboratorio') actualizarPaneles('equipos');
                        }
                        if (origen == 'divSuspendidos') {
                            if (areaText == 'Logística') actualizarPaneles('suspendidosALog');
                            if (areaText == 'Laboratorio') actualizarPaneles('suspendidosALab');
                            if (areaText == 'Clonado') actualizarPaneles('suspendidosAClon');
                        }
                        if (origen == 'divEnLaboratorio') {
                            if (areaText == 'Logística') actualizarPaneles('laboratorioEntregar');
                            if (areaText == 'Clonado') actualizarPaneles('equipos');
                        }
                        if (origen == 'divEntregar') {
                            if (areaText == 'Laboratorio') actualizarPaneles('laboratorioEntregar');
                            if (areaText == 'Clonado') actualizarPaneles('clonadoEntregar');
                        }
                        if (origen == 'divParaGarantia') {
                            if (areaText == 'Laboratorio') actualizarPaneles('aGarantiaLaboratorio');
                            if (areaText == 'Clonado') actualizarPaneles('aGarantiaClonado');
                            if (areaText == 'Logística') actualizarPaneles('aGarantiaLog');
                        }
                        if (origen == 'divEnGarantia') {
                            if (areaText == 'Laboratorio') actualizarPaneles('enGarantiaLaboratorio');
                            if (areaText == 'Clonado') actualizarPaneles('enGarantiaClonado');
                            if (areaText == 'Logística') actualizarPaneles('enGarantiaLog');
                        }
                        $('p.ajaxGif').hide();
                    }
                }
            });
        }
    }
}
/**FUNCION DERIVAR INCIDENTES**/
function derivar() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var context = $(this).parents('ul.fly');
    var origen = grilla.attr('id');
    var idArea = $('.sArea', context).val();
    var textArea = $('.sArea option:selected', context).text();
    var idTecnico = $('.sTecnico', context).val();
    var fData = new FormData();

    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Incidente", 'error');
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(3)').text();
        fData.append("idAreaDestino", idArea);
        fData.append("idTecnico", idTecnico);
        fData.append("idIncidente", id_inc);
        if (window.ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarIncidente",
                data: fData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    window.ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el Incidente a ' + textArea);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail, 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError, 'error');
                },
                complete: function () {
                    window.ajaxReady = true;
                    if (exito) {
                        actualizarPaneles('incidentes')
                    }
                }
            });
        }
    }
}
    /**FUNCION OCULTAR INFO**/
function ocultar() {
        $('#tOblea').val("").removeClass().addClass('input');
        mostrarSub();
    }
    /**TRAER INCIDENTES A LA VIDA**/
function incidenteToLife() {
        var exito = false;
        if (validarNoVacio($('#tIncToLive'))) {
            var nroInc = $('#tIncToLive').val();
            $.ajax({
                url: "Incidentes/incidentesToLife",
                type: 'POST',
                data: 'nroInc=' + nroInc,
                beforeSend: ajaxReady = false,
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == 'ok') {
                        exito = true;
                        mostrarExito('El Incidente ' + nroInc + ' fue reactivado');
                        $('#tIncToLive').val('');
                    }
                    else mostrarError(data.Detail, 'error');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                },
                complete: function () {
                    ajaxReady = true;
                    if (exito) actualizarPaneles('ingresados');
                }
            });
        }
    }
    /**Search Autocomplete**/
function searchAutocomplete(request, response) {
        var texto = request.term;
        var comienzoEquipo = /^WS\-*\d{1,5}|^NB\-*\d{1,5}|^PR\-*\d{1,5}|^MN\-*\d{1,5}|^VS\-*\d{1,5}|^SV\-*\d{1,5}|^KS\d{1,5}/;
        texto = texto.toUpperCase();
        var buscoEquipo = false;
        var buscoIncidente = false;

        if (comienzoEquipo.test(texto)) {
            buscoEquipo = true;
        }
        if (comienzoIncidente.test(texto)) {
            buscoIncidente = true;
        }
        var uri;
        if (buscoEquipo) uri = 'GetEquiposActivos';
        else uri = 'GetIncidentesActivos';
        var term = '{ prefixText: "' + request.term + '" }';
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "Incidentes/" + uri,
            data: term,
            dataType: "json",
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                else {
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val;
                        obj.label = val;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            }
        });
    }
    /*FUNCION BUSCAR EQUIPOS O INCIDENTES*/
function buscarEquiposInc(datos) {
        var strObj = new String(datos);
        var arr = strObj.split('=');
        var resString = arr[1];
        resString = resString.toUpperCase();

        if (!regularEquipo.test(resString) && !regularIncidente.test(resString)) {
            mostrarError("El texto íngresado no parece válido", 'error');
            return;
        }
        if (ajaxReady) {
            $.ajax({
                url: "Incidentes/buscarEqOInc",
                data: datos,
                dataType: 'json',
                type: 'POST',
                beforeSend: function () {
                    ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Area == null) {
                        mostrarInfo("El equipo/incidente no está activo");
                        $('p.ajaxGif').hide();
                    }
                    else {
                        ubicarIncidente(data);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError, 'error');
                },
                complete: function () {
                    ajaxReady = true;
                }
            });                          //fin ajax
        } //fin ajaxready
}
function ubicarIncidente(data) {
    var direccion = data.SubDireccion;
    if (direccion == "Soporte Técnico") {
        var area = data.Area;
        if (data.Estado == "Suspendido") {
            if (data.Tipo == "Equipo") {
                doScroll(ventanas[6], data.Numero);
            }else doScroll(ventanas[2], data.Numero);
        } else {
            switch (area) {
                case "Logística": if (data.Tipo == "Incidente") {
                    if (data.Estado == "Para Cerrar") {
                        doScroll(ventanas[0], data.Numero);
                    }else doScroll(ventanas[2], data.Numero);
                }
                else {
                    if (data.estado == "Para Garantia") doScroll(ventanas[7], data.Numero);
                    else doScroll(ventanas[5], data.Numero);
                }
                    break;
                case "Depósito": doScroll(ventanas[6], data.Numero);
                    break;
                case "At.Usuarios": doScroll(ventanas[1], data.Numero);
                    break;
                case "Clonado": doScroll(ventanas[3], data.Numero);
                    break;
                case "Laboratorio": doScroll(ventanas[4], data.Numero);
                    break;
                case "Garantia": doScroll(ventanas[8], data.Numero);
                    break;
                case "Soporte": doScroll(ventanas[0], data.Numero);
                    break;
            }
        }
    } else {
        mostrarInfo("El Ticket se encuentra en " + direccion);
    }
}
    /**FUNCION FINALIZAR EQUIPO**/
function terminarEquipo() {
        var nro = [];
        var exito = false;
        $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("Debe seleccionar UN Equipo", 'error');
        }
        else {
            var datoss = nro[0].find('td').slice(1, 9);
            var trsel = $.makeArray(datoss);
            var id_eq = $(trsel[1]).text();
            var id_inc = $(trsel[0]).text();
            var nroInc = $(trsel[3]).text();
            var datos = "idEq=" + id_eq + "&idInc=" + id_inc + "&nroInc=" + nroInc;
            if (ajaxReady) {
                var divdialog = $('<div>').append("Se va a solucionar el equipo " + $(trsel[2]).text() + ". Está Seguro?").dialog({
                    width: '250px',
                    show: 'slide',
                    //position: 'center',
                    open: function () {
                        $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                    },
                    buttons: {
                        Aceptar: function () {
                            NProgress.start();
                            $.ajax({
                                url: "Incidentes/terminarEquipo",
                                data: datos,
                                type: 'POST',
                                beforeSend: function () {
                                    ajaxReady = false;
                                },
                                success: function (data) {
                                    if (data.Info == "Login") {
                                        mostrarFinSesion();
                                        return;
                                    }
                                    if (data.Info == "ok") {
                                        mostrarExito('Equipo solucionado');
                                        exito = true;
                                    }
                                    else {
                                        mostrarError(data.Detail, 'error');
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    mostrarError(thrownError, 'error');
                                },
                                complete: function () {
                                    NProgress.done();
                                    $(divdialog).dialog('destroy')
                                    ajaxReady = true;
                                    if (exito) actualizarPaneles('todos');
                                }
                            });               //fin ajax
                        },
                        Cancelar: function () { $(divdialog).dialog('destroy') }
                    }

                });
                $.ajax({
                    
                });     //fin ajax
            } //fin ajaxready
        } //fin else 
    }
    /**FUNCION TOMAR EQUIPO**/
function tomarEquipo() {
        var exito = false;
        var nro = [];
        var User = $('#user').text();
        var id_eq = [];
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0) {
            mostrarError("debe seleccionar al menos UN equipo", 'error');
            return;
        }
        else {         
            for (i = 0; i < nro.length; i++) {                
                id_eq.push($.trim(nro[i].find('td:nth-child(3)').text()));
            }
            setTecnicoAEquipo(User, id_eq);
        }
    }   
    //FUNCION ASIGNAR TECNICO A EQUIPO    
function asignarEquipo() {

        var nro = [];
        var exito = false;
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        var tec = $(this).parent().prev().find('option:selected').text();
        $(grilla).find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0) {
            mostrarError("Debe seleccionar al menos UN Equipo", 'error');
        }
        else {            
            var id_eq = [];
            for (i = 0; i < nro.length; i++) {                
                id_eq.push($.trim(nro[i].find('td:nth-child(3)').text()));
            }
            setTecnicoAEquipo(tec, id_eq);
        }        
    }
//helper send data
function setTecnicoAEquipo(tecnico, idEq) {
        $.ajax({
            url: "Incidentes/setTecnicoAEquipo",
            data: "id_Eq=" + idEq + "&tecnico=" + tecnico,
            datatype: 'json',
            type: 'POST',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == "ok") {
                    mostrarExito('Equipo Asignado a: ' + tecnico);
                    actualizarPaneles('equipos');
                }
                else {
                    mostrarError(data.Detail, 'error');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError, 'error');
            },
            complete: function () {
                $('p.ajaxGif').hide();
            }
        });              //fin ajax
    }
    /**FUNCION SOLUCIONAR Y CERRAR INCIDENTE**/
function cerrarIncidente() {
        var nro = [];
        var exito = false;
        var url;
        var grilla = $(this).parents('.botones').siblings('.grilla');
        if (grilla.hasClass('divBandeja')) {
            url = 'CierreTotal';
        }
        else url = 'solucionarIncidente';
        $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("debe seleccionar UN incidente", 'error');
        }
        else {
            var datoss = nro[0].find('td').slice(1, 9);
            var trsel = $.makeArray(datoss);
            var data = ["idInc=", "nroInc=", "fecha=", "tipif=", "jurisd=", "a=", "area=", "estado="];
            var value;
            $(trsel).each(function (i) {
                value = $(this).text();
                data[i] += value;
            });
            var datos = data.join('&');
            if (ajaxReady) {
                var divdialog = $('<div>').append("Se va a cerrar el incidente " + $(trsel[1]).text() + ". Está Seguro?").dialog({
                    width: '250px',
                    show: 'slide',
                    //position: 'center',
                    open: function () {
                        $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                    },
                    buttons: {
                        Aceptar: function () {
                            var button = $(this);
                            $.ajax({
                                url: "Incidentes/" + url,
                                data: datos,
                                type: 'POST',
                                beforeSend: function () {
                                    button.attr('disabled', 'disabled');
                                },
                                success: function (data) {
                                    if (data.Info == "Login") {
                                        mostrarFinSesion();
                                        return;
                                    }
                                    if (data.Info == "ok") {
                                        mostrarInfo('Se ha Cerrado el Incidente');
                                        exito = true;
                                    }
                                    else {
                                        mostrarError(data.Detail, 'error');
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    mostrarError(thrownError, 'error');
                                },
                                complete: function () {
                                    ajaxReady = true;
                                    $(divdialog).dialog('destroy');
                                    if (exito) {
                                        actualizarPaneles('todos');
                                    }
                                }
                            });               //fin ajax
                        },
                        Cancelar: function () { $(this).dialog('destroy') }
                    }
                });
            } //fin ajaxready
        } //fin else
    } //fin funcion
    /**FUNCION QUITAR EQUIPOS AL INCIDENTE**/
function quitarEquipos(event) {
        var oblea = $.trim($("#tQuitarOblea").val());
        oblea = oblea.toUpperCase();
        if (!regularEquipo.test(oblea)) {
            mostrarError("Ingrese una oblea válida", 'error');
            return;
        }
        var cantInc = $("#gIAtLogistica :checked").length;
        if (cantInc != 1) {
            alert("Seleccione un único incidente");
            return;
        }
        var inc = $("#gIAtLogistica :checked").closest("tr").find("td").eq(1).html();
        var nroInc = $("#gIAtLogistica :checked").closest("tr").find("td").eq(2).find('span').html();
        var exito = false;
        if (ajaxReady) {

            var datos = "oblea=" + oblea + "&incidente=" + inc + "&nroIncidente=" + nroInc;
            $.ajax({
                url: "Incidentes/quitarEquiposaInc",
                data: datos,
                type: 'POST',
                beforeSend: function () { ajaxReady = false },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito("El equipo " + oblea + " se quitó exitosamente del incidente " + nroInc);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail, 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(xhr.status + '/' + thrownError, 'error');
                },
                complete: function () {
                    ajaxReady = true;
                    if (exito) {
                        actualizarPaneles('equipos');
                        $('p.ajaxGif').hide();
                    }
                }
            });
        } //fin ajax ready
    }
    /**FUNCION AGREGAR EQUIPOS AL INCIDENTE**/
function addEquipos(event) {
        var toblea = $("#tOblea").val();
        var cantInc = $("#gIAtLogistica :checked").length;
        if (cantInc != 1) {
            mostrarError("Seleccione UN incidente", 'error');
            return;
        }
        var inc = $("#gIAtLogistica :checked").closest("tr").find("td").eq(1).html();
        if (ajaxReady) {
            var tecnico = $("#tecnicoAddEquipo").val();
            var area = $("#dAreaAddEquipo").val();
            if (area == 0) {
                mostrarError("Seleccione el área", 'error');
                return;
            }
            var exito = false;
            var datos = "incidente=" + inc + "&idArea=" + area + "&idTecnico=" + tecnico + "&oblea=" + toblea;
            $.ajax({
                url: "Incidentes/agregarEquiposaInc",
                data: datos,
                type: 'POST',
                datatype: 'json',
                beforeSend: function () { ajaxReady = false },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito("El equipo se agregó exitosamente");
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail, 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(xhr.status + '/' + thrownError, 'error');
                },
                complete: function () {
                    ajaxReady = true;
                    if (exito) {
                        actualizarPaneles('equipos');
                        $('p.ajaxGif').hide();
                    }
                }
            });
            toblea.className = "approved";
        } //fin ajax ready     
}
function notificarIncidente() { }
    

                            