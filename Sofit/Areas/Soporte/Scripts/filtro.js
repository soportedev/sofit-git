﻿var filterCount = 0;

$(function() {

    $(':button').button();
    $('#excel').button({ icons: { primary: "ui-icon-print", secondary: "ui-icon-disk"} }).
position({
    my: 'left bottom',
    at: 'right bottom',
    of: '#content',
    offset: "10 -50"
});
    $('#addFilterButton').click(function() {
        var filterItem = $('<div>')
            .addClass('filterItem')
            .appendTo('#filterPane')
            .data('suffix', '.' + (filterCount++));
        $('div.template.filterChooser')
              .children().clone().appendTo(filterItem)
              .trigger('adjustName');

    });

    $('select.filterChooser').live('change', function() {
        var filterType = $(':selected', this).attr('data-filter-type');
        var filterItem = $(this).closest('.filterItem');
        $('.qualifier', filterItem).remove();
        $('div.template.' + filterType)
              .children().clone().addClass('qualifier')
              .appendTo(filterItem)
              .trigger('adjustName');
        $('option[value=""]', this).remove();
        $('.date').removeClass('hasDatepicker').datepicker({ dateFormat: "dd/mm/yy" });

    });

    $('button.filterRemover').live('click', function() {
        $(this).closest('div.filterItem').remove();
    });

    $('.filterItem [name]').live('adjustName', function() {
        var suffix = $(this).closest('.filterItem').data('suffix');
        if (/(\w)+\.(\d)+$/.test($(this).attr('name'))) return;
        $(this).attr('name', $(this).attr('name') + suffix);
    });

    $('#applyFilterButton').click(function() {

        var n = $('#filterPane *').serialize();

        $.ajax({
            beforeSend: function() {
                bgModal($("#resultsPane"));
            },
            url: 'estadistica.aspx',
            data: n,
            type: 'POST',
            success: function(data) {
                var grilla = $(data).find('#gEstadistica');
                if (grilla.length > 0) {
                    $("#resultsPane").html(grilla);
                    $("#gEstadistica").styleTable();
                }
                else $("#resultsPane").html("<p>No se encontraron resultados</p>");
            },
            complete: function() {

                closeModal();
            },
            error: function(data, thrown) {

                mostrarError(data.responseText);
            }

        });
    });


    $('#addFilterButton').click();

});


function bgModal(container) {
    var contenidoHTML = '<img src="../img/ajax-loader.gif" alt="Espere">';
    // fondo transparente
    // creamos un div nuevo, con dos atributos
    var bgdiv = $('<div>').attr({
        'class': 'bgtransparent',
        id: 'bgtransparent'
    });

    // agregamos nuevo div a la pagina
    $(container).append(bgdiv);

    // obtenemos ancho y alto de la ventana del container

    var wscr = container.width();
    var hscr = container.height();
    wscr += parseInt(container.css("padding-left"), 10) + parseInt(container.css("padding-right"), 10); //Total Padding Width
    wscr += parseInt(container.css("margin-left"), 10) + parseInt(container.css("margin-right"), 10); //Total Margin Width
    wscr += parseInt(container.css("borderLeftWidth"), 10) + parseInt(container.css("borderRightWidth"), 10); //Total Border Width

    hscr += parseInt(container.css("padding-top"), 10) + parseInt(container.css("padding-bottom"), 10); //Total Padding Width
    hscr += parseInt(container.css("margin-top"), 10) + parseInt(container.css("margin-bottom"), 10); //Total Margin Width
    //    hscr += parseInt(theDiv.css("borderLeftWidth"), 10) + parseInt(theDiv.css("borderRightWidth"), 10); //Total Border Width    

    //establecemos las dimensiones del fondo
    $('#bgtransparent').css("width", wscr);
    $('#bgtransparent').css("height", hscr);


    // ventana modal
    // creamos otro div para la ventana modal y dos atributos
    var moddiv = $('<div>').attr({
        'class': 'bgmodal',
        id: 'bgmodal'
    });

    // agregamos div a la pagina
    $('#bgtransparent').append(moddiv);

}
function closeModal() {
    // removemos divs creados
    $('#bgmodal').remove();
    $('#bgtransparent').remove();
}