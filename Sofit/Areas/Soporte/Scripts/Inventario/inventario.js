﻿// JavaScript Document
window.pathServer = 'Inventario/';
window.RestoreWS;
// Vars
window.DiscoExtra = '';
window.RamExtra = '';
window.VideoExtra = '';
window.RedExtra = '';
window.OpticoExtra = '';
window.SoftwareExtra = '';
window.AltaModeloMonitor = false;
window.EdicionModeloMonitor = false;
$(function () {
    Modal();    
    RestoreWS = $('#RestoreHardware').clone();
    $('#searchButton').on('click', function () {
        var oblea = $('#tBuscar').val();
        GetEquipo(oblea);
    });
    var nroOb = getParameterByName('nro');
    if (nroOb != "") {
        $('#tBuscar').val(nroOb);
        $('#searchButton').trigger('click');
        var varWSNB = /^WS.*$|^NB.*$/
        var varMN = /^MN.*$/
        var varPR = /^PR.*$/
        var varVS = /^VS.*$|^KS.*$|^TBL.*$|^AAC.*$|^GE.*$|^RCK.*$|^UPS.*$/        		
        if (varWSNB.test(nroOb)) {
            // Show WS content
            mostrarDetailsPc();
        }
        else if(varMN.test(nroOb)){
            // Show MN content				
            mostrarDetailsMn();
        }
        else if(varPR.test(nroOb)){
            // Show PR content
            mostrarDetailsPr();
        }
        else if (varVS.test(nroOb)) {
            // Show VS content
            mostrarDetailsVs();
        }
    }
	// buscar
    $(document).keydown(function (e) {
		e.preventDefault
        if (e.which === 114) {
            $('#tBuscar').focus();
			return false;
        }
	});
	// Slide Menu
    var overflowautoWidth = $('.overflow-auto').width();
	$('.slideout-menu-toggle').on('click', function(event){
    	event.preventDefault();
    	// create menu variables
    	var slideoutMenu = $('.slideout-menu');
    	var slideoutMenuWidth = $('.slideout-menu').width();
      
    	var overflowauto = $(".overflow-auto");
    	
    	// toggle open class
    	slideoutMenu.toggleClass("open");
    	
    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	left: "0px"
	    	});
	    	overflowauto.animate({
		    	width: overflowautoWidth-slideoutMenuWidth,
		    	marginLeft: slideoutMenuWidth
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	left: -slideoutMenuWidth
	    	}, 250);
	    	overflowauto.animate({
		    	width: overflowautoWidth,
		    	marginLeft: '0px'
	    	}, 250);
    	}
    });
    // COPIAR EQUIPO
    $('#CopiarEquipo').keypress(function (e) {
        if (e.which == 13) {
            RestoreOriginalCode()
            var ObleaSerie = $(this).val();
            // Ajax
            // Send data
            $.ajax({
                data: { Oblea: ObleaSerie },
                url: pathServer + 'GetEquipoSoporte',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Show Loading
                    NProgress.done();
                    if (response['Info'] === 'No inicializado') {
                        mostrarError(response['Detail'])
                    } else {
                        CompletarWS(response)
                        $('#CopiarEquipo').val('')
                        mostrarExito('Copiado correctamente')
                    }
                },
                error: function (response) {
                    // Show Loading
                    NProgress.done();
                    mostrarError(response['Detail'])
                }
            });
            // Ajax
            //-->
        }
    })
    // COPIAR EQUIPO
    //-->
    //reset errors
    $('input').each(function () {
        $(this).on('focus', function () {
            $(this).removeClass('error');
        })
    });	
	//-->
	$('#CancelarCambios').on('click', function () {
	    initialState();
	});
	// TRANFORM Uppercase
	$('#tBuscar, #Oblea').keyup(function (e) {
	    var key = e.which;
	    var val = $(this).val();
	    if (key == 8 & val.length == 0) {
	        return false;
	    }
	    $(this).val(this.value.toUpperCase())
	});
	// Show specific content
	$('#tBuscar, #Oblea').keyup(function(e){
		if (e.which == 13) {
			$('#searchButton').trigger('click');
		}
	    var varWSNB = /^WS.*$|^NB.*$/
	    var varMN = /^MN.*$/
	    var varPR = /^PR.*$/
        var varVS = /^VS.*$|^KS.*$|^TBL.*$|^AAC.*$|^GE.*$|^RCK.*$|^UPS.*$/
	    var Oblea = $(e.currentTarget).val();
	    var DefineURL = /^WS.*$|^NB.*$|^PR.*$|^MN.*$|^VS.*$|^KS.*$/;		
	    if (varWSNB.test(Oblea)) {
	        // Show WS content
	        mostrarDetailsPc();
	    }
	    else if(varMN.test(Oblea)){
	        // Show MN content				
	        mostrarDetailsMn();
	    }
	    else if(varPR.test(Oblea)){
	        // Show PR content
	        mostrarDetailsPr();
	    }
	    else if (varVS.test(Oblea)) {
	        // Show VS content
	        mostrarDetailsVs();
	    } else {
	        // Hide other contents
	        hideDetails();
	    }	    
	    $('#body').trigger('CreateNiceScroll')// Create scroll
	});
	$("#tBuscar").autocomplete({
	    minLength: "2",
	    source: searchAutocomplete,
	    select: function (e, ui) {
	        $('#tBuscar').val(ui.item.oblea);	        
	        GetEquipo(ui.item.oblea);
	        return false;
	    },
	    focus: function (e, ui) {	        
	        $('#tBuscar').val(ui.item.label);	        
	        return false;
	    }
	});
    //--->
    $('#editData').click(function () {
        editState();
    });    
	// TIPO autocomlete
    $.ajax({
        url: pathServer+'TiposEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                if (raw[i].Nombre === 'Pc' || raw[i].Nombre === 'Impresora' || raw[i].Nombre === 'Monitor' || raw[i].Nombre === 'Varios'
                    | raw[i].Nombre === 'UPS' | raw[i].Nombre === 'Rack' | raw[i].Nombre === 'Grupo Electrogeno' | raw[i].Nombre === 'Aire Acondicionado' | raw[i].Nombre === 'Tablero') {
					source.push(raw[i].Nombre);
					mapping[raw[i].Nombre] = raw[i].id;
				}	
            }
			
			var identificador = {}
			for(var i = 0; i < response.length; ++i) {
			  identificador[response[i].Nombre] =
			  response[i].id
			}
			
			window.marcaTipo = {
				"motherboard":identificador['Placa Madre'],
				"hd":identificador['Disco Rígido'],
				"ram":identificador['RAM'],
				"optico":identificador['Disco Óptico'],
				"red":identificador['Placa Red'],
				"video":identificador['Placa Video'],
				"micro":identificador['Microprocesador'],
				"pc":identificador['Pc'],
				"impresora":identificador['Impresora'],
				"monitor":identificador['Monitor'],
				"varios":identificador['Varios']
			};
            // Function
            $('#idTipo').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    // $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });    
	var state;
	$('#fullscreenBtn').on('click', function() {
		state = !state; 
		if (state) {
			// do this (1st click, 3rd click, etc)
			$( '#fullscreen' ).addClass( "fullscreen" );
		} else {
			// do that
			$( '#fullscreen' ).removeClass( "fullscreen" );
		}
	});
	// Fullscreen function
	//-->	
	function AgregarNuevaMarca(add, thisElem){
		if (add === 'Agregar') {
			$('.classEnviar').each(function () {
				$(this).addClass('hide');
			})
			$('.classMarca').each(function () {
				$(this).removeClass('hide');
			})
			$('span#works_btn').trigger(jQuery.Event("click"));
			thisElem.val('');
		}
	}
	
	// MARCA
	$('#Marca, #ModMArcaImpresora, #ModMArcaMonitor').click(function (e) {
	    var disable = $(this).attr('readonly');
	    // Check input status
	    if (disable === 'readonly') {
	        // If input is readonly, diable functions
	        e.preventDefault();
	    }
	    else {
	        // If input is writable
	        // Clean val
	        $(this).val('');

	        // Start function
	        var This = $(this)
	        /*if ( $('#idTipo').attr('data-id') != '' ){
				var $TipoEqupo = $('#idTipo').attr('data-id')
			}else{
				var $TipoEqupo = '2'
			}*/

	        var target = $(e.target)
	        if (target.is('#Marca')) {
	            var $TipoEqupo = $('#idTipo').attr('data-id');
	            if ($TipoEqupo == '') {
	                mostrarError("Seleccione el tipo de equipo!!!");
	                return;
	            }
	        } else if (target.is('#ModMArcaImpresora')) {
	            var $TipoEqupo = '2'
	        } else if (target.is('#ModMArcaMonitor')) {
	            var $TipoEqupo = '3'
	        }	        
	        $.ajax({
	            data: { tipo: $TipoEqupo },
	            url: pathServer + 'MarcasPorTipo',
	            type: 'post',
	            dataType: 'json',
	            beforeSend: function () {
	                // Show Loading
	                NProgress.start();
	            },
	            success: function (response) {
	                // Show Loading
	                NProgress.done();
	                // Values
	                var raw = response;
	                var source = [];
	                var mapping = {};
	                // Push val to eneable Add
	                source.push('Agregar');
	                for (var i = 0; i < raw.length; ++i) {
	                    source.push(raw[i].Nombre);
	                    mapping[raw[i].Nombre] = raw[i].idMarca;
	                }
	                // Function
	                This.autocomplete({
	                    source: source,
	                    minLength: 0,
	                    select: function (event, ui) {
							if (ui.item.label == "Agregar") {
								AgregarNuevaMarca("Agregar", "")
							}
	                        var dataId = mapping[ui.item.value]
	                        $(this).attr('data-id', dataId);
	                        if ($TipoEqupo == '2') {
	                            var pos = { my: "right top", at: "right bottom", }
	                            bindAutocompleteModelo(pos, target);
	                        }
	                        if ($TipoEqupo == '3') {
	                            var pos = { my: "right top", at: "right bottom", }
	                            bindAutocompleteModeloM(pos, target);
	                        }
	                    },
	                    close: function () {
	                        // Finish selection and make a blur
	                        // $(this).blur();
	                    }
	                }).autocomplete('search');
	            },
	            // On error
	            error: function (response, textStatus, tres) {
	                // create the notification
	                mostrarError(tres);
	            }
	        });// Ajax
	    }
	});
    // Marca
	//-->	
    // Modelo
	$('#Modelo').click(function () {
	    var target=$('#Marca');
	    var atribute = $(this).attr('readonly');
	    if (atribute == 'readonly') {
	        return false;
	    }
	    var $TipoEqupo = $('#idTipo').attr('data-id')
	    if ($TipoEqupo == '2') {// Comprueba tipo de equipo
	        var pos = { my: "right top", at: "right bottom", }
	        bindAutocompleteModelo(pos, target);
	    }
	    if ($TipoEqupo == '3') {// Comprueba tipo de equipo
	        var pos = { my: "right top", at: "right bottom", }
	        bindAutocompleteModeloM(pos, target);
	    }

	    $(this).val('').autocomplete('search')// Ejecuta la busqueda
	})
    // Modelo
    //-->	
	// Ubicacion
    window.getUbicacion = function () {
        //$('#Ubicacion').val('');
        // Send data
        // Ajax
        $.ajax({
            data: { idJur: $("#Jurisdiccion").attr("data-Id") },
            url: pathServer+'Dependencias',
            type: 'post',
            dataType: 'json',
            async:false,
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Show Loading
                NProgress.done();
                // Values
                var raw = response;
                var source = [];
                var mapping = {};
                for (var i = 0; i < raw.length; ++i) {
                    source.push(raw[i].Nombre);
                    mapping[raw[i].Nombre] = raw[i].idDependencia;
                }
                // Function
                $('#Ubicacion').autocomplete({
                    source: source,
                    minLength: 0,
                    select: function (event, ui) {
                        var dataId = mapping[ui.item.value]
                        $(this).attr('data-id', dataId);
                    },
                    close: function () {
                        // Finish selection and make a blur
                         $(this).blur();
                    }
                }).trigger('search')
            },
            // On error
            error: function (response, textStatus) {
                // NOTIFICAION
            }
        });
        // Ajax
        // Send data
    };
    //-->
	
	$('#Ubicacion').click(function (e) {
        var disable = $(this).attr('readonly');
        // Check input status
        if (disable == 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {

            getUbicacion();
            $(this).val('');
            $(this).autocomplete('search');

        }
	})

    // Jurisdiccion
    // Send data
    // Ajax
    $.ajax({
        url: pathServer+'Jurisdicciones',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
			// Show Loading
			NProgress.done();
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idJurisdiccion;
            }
            // Function
            $('#Jurisdiccion').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                    getUbicacion();
                    //event.preventDefault();
                },
                close: function () {
                    // Finish selection and make a blur
                    // $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
            //-->
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //estados
    $.ajax({
        url: pathServer+'EstadosEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
			// Show Loading
			NProgress.done();
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idEstado;
            }
            // Function
            $('#Estado').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    // $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus, tres) {
            mostrarMensajeError(tres, 'error');
        }
    });	
	//-->
	
	// Claves
	$('#GenClaveAdmin').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			var text = "";
			var possible = "0123456789";

			for( var i=0; i < 10; i++ ){
				text += possible.charAt(Math.floor(Math.random() * possible.length))
			}

			$("#ClaveAdmin").val(text)
		}
	})
	$('#GenClaveBIOS').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			var text = "";
			var possible = "0123456789";

			for( var i=0; i < 6; i++ ){
				text += possible.charAt(Math.floor(Math.random() * possible.length))
			}

			$("#ClaveBIOS").val(text)
		}
	})
	// Claves
	//-->	
	// Sistema Operativo
	$.ajax({
        url: pathServer+'Os',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].Id;
            }
            // Function
            $('#OS').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    // $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    // Ajax
    // Send data
	// Sistema Operativo
	//-->
	
	// Hardware
	// Select1
    window.Select1 = function (trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add) {
		$.ajax({
			data:  data,
			url:   pathServer+url,
			type:  'post',
			dataType: 'json',
			beforeSend: function () {
				// Show Loading
				NProgress.start();
			},
			success:  function(response) {
				// Show Loading
				NProgress.done();
				// Values
				var raw = response;
				var source = [];
				var mapping = {};
				if(Add === 'Yes'){ // If available to add
					source.push('Agregar');// Push val to eneable Add
				}
				// Making autocomplete sources
				for (var i = 0; i < raw.length; ++i) {
					if( RemROTO === 'Y' && raw[i][labelName] === 'ROTO'){
						continue;
					}
					if( UnidadMedida ){ // If function need to use Unidad de Medida
						var UM = raw[i][labelName]+' '+raw[i][UnidadMedida];
						var item = {label: UM, value: raw[i][valueName]}; // Create item for source
						source.push(item); // Create source for autocomplete
						//mapping[UM] = raw[i][valueName]; // Create a map betwen dblabel and dbid
					}else{ // If not
						var item = {label: raw[i][labelName], value: raw[i][valueName]}; // Create item for source
						source.push(item); // Create source for autocomplete
						//mapping[raw[i][labelName]] = raw[i][valueName]; // Create a map betwen dblabel and dbid
					}
				}
				
				RemROTO = ''// Clear RemROTO value to restore complete function
				
				// Function autocomplete
				trigger.autocomplete({
				    source: source,
				    minLength: 0,
				    position: { my: "right bottom", at: "right top", },
				    select: function (event, ui) {
				        if (ui.item.label == "Agregar") {
				            AgregarNuevaMarca("Agregar", "")
				        }
				        window.TempToAdd = '' // Create variable for store the objet to add hard
						event.preventDefault();
				        var dataId = ui.item.value // Create variable for store dbid for the selected hard
						$(this).val(ui.item.label); // Assign label into the input form
				        $(this).attr('data-id', dataId); // Assign dbid into the input form
				        if (ReadyToAdd === 'Yes') { // Check if this is the last step before add the hard
				            $.each(raw, function (e, value) { // Making the object to store hard info
				                if (value[valueName] === dataId) { // Checking if selected hard is the same that hard included in ajax response
				                    TempToAdd = raw[e] // Finally add the hard info to a object
				                }
				            })
				        }

				    },
				    close: function () {
				        // Finish selection and make a blur
				        //$(this).blur();
				    }
				}).val('').autocomplete('search')
				$('#body').trigger('CreateNiceScroll')
			},
            // On error
            error: function (response, textStatus) {
                // NOTIFICAION
                // Show Loading
                NProgress.done();
            }
		})
	}
	
	// Hide Automplete when page scroll
	$('.overflow-auto').scroll(function(){
		$(this).focus();
	})
	// Hide Automplete when page scroll
	//-->
	
	// Hardware Calls
	$('.classWS').on('click', '#marcaMother', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: marcaTipo['motherboard'] }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#modeloMother', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MotherXMarca'
			var data = { idMarca: $("#marcaMother").attr("data-Id") }
			var labelName = 'Modelo'
			var valueName = 'Id'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#AddMother', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				$('#MarcaMotherCell').html(TempToAdd.Marca)
				$('#ModeloMotherCell').html(TempToAdd.Modelo)
				$('#ChipsetMotherCell').html(TempToAdd.Chipset)
				$('#SocketMotherCell').html(TempToAdd.Socket)
				$('#USBMotherCell').html(TempToAdd.CantUsb)
				if(TempToAdd.PciX == true){
					$('#PCI-E').attr('checked', 'checked')
				}else if(TempToAdd.Agp == true){
					$('#AGP').attr('checked', 'checked')
				}
				$('#RAMMotherCell').html(TempToAdd.cantRam)
				$('#SATAMotherCell').html(TempToAdd.cantSata)
				$('#Motherboard').val(TempToAdd.Id)
				TempToAdd = ''
				$('#marcaMother').val('')
				$('#modeloMother').val('')
			}else{
			    mostrarError('Seleccione todos los campos.');
			}
		}
	})
	
	$('.classWS').on('click', '#marcaMicro', function (e) {

		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: marcaTipo['micro'] }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#modeloMicro', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'cpuxmarca'
			var data = { idMarca: $("#marcaMicro").attr("data-Id") }
			var labelName = 'Modelo'
			var valueName = 'Id'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#AddMicro', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				$('#MarcaMicroCell').html(TempToAdd.Marca.Nombre)
				$('#ModeloMicroCell').html(TempToAdd.Modelo)
				$('#FrecMicroCell').html(TempToAdd.Frecuencia)
				$('#CacheMicroCell').html(TempToAdd.Cache)
				$('#SocketMicroCell').html(TempToAdd.Socket.Nombre)
				$('#CPU').val(TempToAdd.Id)
				TempToAdd = ''
				$('#marcaMicro').val('')
				$('#modeloMicro').val('')
			}else{
			    mostrarError('Seleccione todos los campos');
			}
		}
	})
	
	$('.classWS').on('click', '#interfazDisco', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'InterfazDisco'
			var data = { tipo: marcaTipo['micro'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#marcaDisco', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcaDiscoXInterfaz'
			var data = { idInterfaz: $("#interfazDisco").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#capacidadDisco', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'DiscoXMarcaInterfaz'
			var data = { idInterfaz: $("#interfazDisco").attr("data-Id"), IdMarca: $("#marcaDisco").attr("data-Id") }
			var labelName = 'Capacidad'
			var valueName = 'Id'
			var UnidadMedida = 'UM'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida)
		}
	})
	var iDisco = 1
	$('.classWS').on('click', '#AddDisco', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				var missmatch = [];
				$('input', '#DiscoRow').each(function() {
					var ID = $(this).val();
					if ( ID == TempToAdd.Id ) {
						missmatch.push(ID);
					}
				});
				if (missmatch.length > 0) {
					var notification = new NotificationFx({
						wrapper: document.body,
						message: 'Estedispositivo ya se encuentra agregado.<br> Seleccione otro.',
						layout: 'growl',
						effect: 'genie',
						type: 'error', // sussess, notice, warning or error
						ttl: 8000,
						onClose: function () {
							bttn.disabled = false;
						}
					});

					// show the notification
					notification.show();
				} else {
					if( DiscoExtra ){
						iDisco ++
						$('#DiscoRow').append('<div class="row row-td ui-widget-content" data-Remove="DiscoExtraR'+iDisco+'">'+
														'<div class="cell" id="MarcaDiscoCell'+iDisco+'"></div>'+
														'<div class="cell" id="InterfazDiscoCell'+iDisco+'"></div>'+
														'<div class="cell" id="CapacidadDiscoCell'+iDisco+'"></div>'+
														'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
														'<input id="HDD'+iDisco+'" class="idHDD" type="hidden" value="" />'+
													'</div>')
						$('#MarcaDiscoCell'+iDisco).html(TempToAdd.Marca)
						$('#InterfazDiscoCell'+iDisco).html(TempToAdd.Interfaz)
						$('#CapacidadDiscoCell'+iDisco).html(TempToAdd.Capacidad+' '+TempToAdd.UM)
						$('#HDD'+iDisco).val(TempToAdd.Id)
						$('#DiscoForm').append('<div class="row row-td ui-widget-content" data-Remove="DiscoExtraR'+iDisco+'"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
						TempToAdd = ''
					}else{
						$('#MarcaDiscoCell').html(TempToAdd.Marca)
						$('#InterfazDiscoCell').html(TempToAdd.Interfaz)
						$('#CapacidadDiscoCell').html(TempToAdd.Capacidad+' '+TempToAdd.UM)
						$('#HDD').val(TempToAdd.Id)
						DiscoExtra = 'OK'
						TempToAdd = ''
					}
					$('#interfazDisco').val('')
					$('#marcaDisco').val('')
					$('#capacidadDisco').val('')
				}
			}else{
				var notification = new NotificationFx({
					wrapper: document.body,
					message: 'Seleccione todos los campos',
					layout: 'growl',
					effect: 'genie',
					type: 'error', // sussess, notice, warning or error
					ttl: 8000,
					onClose: function () {
						bttn.disabled = false;
					}
				});

				// show the notification
				notification.show();
			}
		}
	})
	
	$('.classWS').on('click', '#ramTipo', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'TipoMemoria'
			var data = { tipo: marcaTipo['ram'] }
			var labelName = 'Tipo'
			var valueName = 'idTipo'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#ramMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcaMemoriaXTipo'
			var data = { idTipo: $("#ramTipo").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#ramCapacidad', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MemoriaXTipoYMarca'
			var data = { idTipo: $("#ramTipo").attr("data-Id"), idMarca: $("#ramMarca").attr("data-Id") }
			var labelName = 'Capacidad'
			var valueName = 'Id'
			var UnidadMedida = 'UM'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida)
		}
	})
	var iRam = 1
	$('.classWS').on('click', '#AddRam', function(){
		if( TempToAdd !== '' ){
			var missmatch = [];
			$('input', '#RamRow').each(function() {
				var ID = $(this).val();
				if ( ID == TempToAdd.Id ) {
					missmatch.push(ID);
				}
			});
			if (missmatch.length > 0) {
				var notification = new NotificationFx({
					wrapper: document.body,
					message: 'Estedispositivo ya se encuentra agregado.<br> Seleccione otro.',
					layout: 'growl',
					effect: 'genie',
					type: 'error', // sussess, notice, warning or error
					ttl: 8000,
					onClose: function () {
						bttn.disabled = false;
					}
				});

				// show the notification
				notification.show();
			} else {
				if( RamExtra ){
					iRam ++
					$('#RamRow').append('<div class="row row-td ui-widget-content" data-Remove="RamExtraR'+iRam+'">'+
													'<div class="cell" id="MarcaRamCell'+iRam+'"></div>'+
													'<div class="cell" id="TipoRamCell'+iRam+'"></div>'+
													'<div class="cell" id="CapacidadRamCell'+iRam+'"></div>'+
													'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
													'<input id="RAM'+iRam+'" class="idRAM" type="hidden" value="" />'+
												'</div>')
					$('#MarcaRamCell'+iRam).html(TempToAdd.Marca)
					$('#TipoRamCell'+iRam).html(TempToAdd.Tipo)
					$('#CapacidadRamCell'+iRam).html(TempToAdd.Capacidad+' '+TempToAdd.UM)
					$('#RAM'+iRam).val(TempToAdd.Id)
					$('#RamForm').append('<div class="row row-td ui-widget-content" data-Remove="RamExtraR'+iRam+'"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
					TempToAdd = ''
				}else{
					$('#MarcaRamCell').html(TempToAdd.Marca)
					$('#TipoRamCell').html(TempToAdd.Tipo)
					$('#CapacidadRamCell').html(TempToAdd.Capacidad+' '+TempToAdd.UM)
					$('#RAM').val(TempToAdd.Id)
					RamExtra = 'OK'
					TempToAdd = ''
				}
				$('#ramTipo').val('')
				$('#ramMarca').val('')
				$('#ramCapacidad').val('')
			}
		}else{
			var notification = new NotificationFx({
				wrapper: document.body,
				message: 'Seleccione todos los campos',
				layout: 'growl',
				effect: 'genie',
				type: 'error', // sussess, notice, warning or error
				ttl: 8000,
				onClose: function () {
					bttn.disabled = false;
				}
			});

			// show the notification
			notification.show();
		}
	})
	
	$('.classWS').on('click', '#videoInterfaz', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'InterfazVideo'
			var data = { tipo: marcaTipo['video'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#videoMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcaVideoXInterfaz'
			var data = { idInterfaz: $("#videoInterfaz").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#videoModelo', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'VideoXInterfazMarca'
			var data = { idInterfaz: $("#videoInterfaz").attr("data-Id"), idMarca: $("#videoMarca").attr("data-Id") }
			var labelName = 'Modelo'
			var valueName = 'Id'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	var iVideo = 1
	$('.classWS').on('click', '#AddVideo', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				var missmatch = [];
				$('input', '#VideoRow').each(function() {
					var ID = $(this).val();
					if ( ID == TempToAdd.Id ) {
						missmatch.push(ID);
					}
				});
				if (missmatch.length > 0) {
					var notification = new NotificationFx({
						wrapper: document.body,
						message: 'Estedispositivo ya se encuentra agregado.<br> Seleccione otro.',
						layout: 'growl',
						effect: 'genie',
						type: 'error', // sussess, notice, warning or error
						ttl: 8000,
						onClose: function () {
							bttn.disabled = false;
						}
					});

					// show the notification
					notification.show();
				} else {
					if( VideoExtra ){
						iVideo ++
						$('#VideoRow').append('<div class="row row-td ui-widget-content" data-Remove="VideoExtraR'+iVideo+'">'+
														'<div class="cell" id="MarcaVideoCell'+iVideo+'"></div>'+
														'<div class="cell" id="ModeloVideoCell'+iVideo+'"></div>'+
														'<div class="cell" id="InterfazVideoCell'+iVideo+'"></div>'+
														'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
											  			'<input id="VIDEO'+iVideo+'" class="idVIDEO" type="hidden" value="" />'+
													'</div>')
						$('#MarcaVideoCell'+iVideo).html(TempToAdd.Marca)
						$('#ModeloVideoCell'+iVideo).html(TempToAdd.Modelo)
						$('#InterfazVideoCell'+iVideo).html(TempToAdd.Interfaz)
						$('#VIDEO'+iVideo).val(TempToAdd.Id)
						$('#VideoForm').append('<div class="row row-td ui-widget-content" data-Remove="VideoExtraR'+iVideo+'"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
						TempToAdd = ''
					}else{
						$('#MarcaVideoCell').html(TempToAdd.Marca)
						$('#ModeloVideoCell').html(TempToAdd.Modelo)
						$('#InterfazVideoCell').html(TempToAdd.Interfaz)
						$('#VIDEO').val(TempToAdd.Id)
						VideoExtra = 'OK'
						TempToAdd = ''
					}
					$('#videoInterfaz').val('')
					$('#videoMarca').val('')
					$('#videoModelo').val('')
				}
			}else{
				var notification = new NotificationFx({
					wrapper: document.body,
					message: 'Seleccione todos los campos',
					layout: 'growl',
					effect: 'genie',
					type: 'error', // sussess, notice, warning or error
					ttl: 8000,
					onClose: function () {
						bttn.disabled = false;
					}
				});

				// show the notification
				notification.show();
			}
		}
	})
	
	var source = [ {'label':'Wireless'}, {'label':'Ethernet'} ]
	//$('.classWS').on('click', 
	$('.classWS').on('click', '#redTecnologia', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Clean val
			$(this).val('');
			// Trigger Search values on fucus
			$(this).autocomplete({
				source: source,
				minLength: 0,
				position: { my : "right bottom", at: "right top", },
				select: function (event, ui) {
					var dataId = ui.item.label
					$(this).attr('data-id', dataId);
				},
				close: function () {
					// Finish selection and make a blur
					// $(this).blur();
				}
			})
			$(this).autocomplete('search');
			// Trigger KeyDown on focus
			//$(this).trigger(jQuery.Event("keydown"));
		}
		$('#body').trigger('CreateNiceScroll')
	})
	$('.classWS').on('click', '#redMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'RedXTecnologia'
			var data = { tecnologia: $("#redTecnologia").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#redModelo', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'RedXTecnologiaMarca'
			var data = { tecnologia: $("#redTecnologia").attr("data-Id"), idMarca: $("#redMarca").attr("data-Id") }
			var labelName = 'Modelo'
			var valueName = 'Id'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	var iRed = 1
	$('.classWS').on('click', '#AddRed', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				var missmatch = [];
				$('input', '#RedRow').each(function() {
					var ID = $(this).val();
					if ( ID == TempToAdd.Id ) {
						missmatch.push(ID);
					}
				});
				if (missmatch.length > 0) {
					var notification = new NotificationFx({
						wrapper: document.body,
						message: 'Estedispositivo ya se encuentra agregado.<br> Seleccione otro.',
						layout: 'growl',
						effect: 'genie',
						type: 'error', // sussess, notice, warning or error
						ttl: 8000,
						onClose: function () {
							bttn.disabled = false;
						}
					});

					// show the notification
					notification.show();
				} else {
					if( RedExtra ){
						iRed ++
						$('#RedRow').append('<div class="row row-td ui-widget-content" data-Remove="RedExtraR'+iRed+'">'+
														'<div class="cell" id="MarcaRedCell'+iRed+'"></div>'+
														'<div class="cell" id="ModeloRedCell'+iRed+'"></div>'+
														'<div class="cell" id="TecnologiaRedCell'+iRed+'"></div>'+
														'<div class="cell" id="VelocidadRedCell'+iRed+'"></div>'+
														'<input id="RED'+iRed+'" class="idRED" type="hidden" value="" />'+
														'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/Inventario/images/rem.png" width="15"></div>' +
													'</div>')
						$('#MarcaRedCell'+iRed).html(TempToAdd.Marca)
						$('#ModeloRedCell'+iRed).html(TempToAdd.Modelo)
						$('#TecnologiaRedCell'+iRed).html(TempToAdd.Tecnologia)
						$('#VelocidadRedCell'+iRed).html(TempToAdd.Velocidad)
						$('#RED'+iRed).val(TempToAdd.Id)
						$('#RedForm').append('<div class="row row-td ui-widget-content" data-Remove="RedExtraR'+iRed+'"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
						TempToAdd = ''
					}else{
						$('#MarcaRedCell').html(TempToAdd.Marca)
						$('#ModeloRedCell').html(TempToAdd.Modelo)
						$('#TecnologiaRedCell').html(TempToAdd.Tecnologia)
						$('#VelocidadRedCell').html(TempToAdd.Velocidad)
						$('#RED').val(TempToAdd.Id)
						RedExtra = 'OK'
						TempToAdd = ''
					}
					$('#redTecnologia').val('')
					$('#redMarca').val('')
					$('#redModelo').val('')
				}
			}else{
				var notification = new NotificationFx({
					wrapper: document.body,
					message: 'Seleccione todos los campos',
					layout: 'growl',
					effect: 'genie',
					type: 'error', // sussess, notice, warning or error
					ttl: 8000,
					onClose: function () {
						bttn.disabled = false;
					}
				});

				// show the notification
				notification.show();
			}
		}
	})
	
	$('.classWS').on('click', '#tipoOptico', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'TipoOptico'
			var data = { tipo: marcaTipo['optico'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#marcaOptico', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'OpticoXTipo'
			var data = { idTipo: $("#tipoOptico").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	$('.classWS').on('click', '#interfazOptico', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'OpticoXTipoMarca'
			var data = { idTipo: $("#tipoOptico").attr("data-Id"), idMarca: $("#marcaOptico").attr("data-Id") }
			var labelName = 'Interf'
			var valueName = 'idInterfaz'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	var iOptico = 1
	$('.classWS').on('click', '#AddOptico', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				var missmatch = [];
				$('input', '#OpticoRow').each(function() {
					var ID = $(this).val();
					if ( ID == TempToAdd.Id ) {
						missmatch.push(ID);
					}
				});
				if (missmatch.length > 0) {
					var notification = new NotificationFx({
						wrapper: document.body,
						message: 'Estedispositivo ya se encuentra agregado.<br> Seleccione otro.',
						layout: 'growl',
						effect: 'genie',
						type: 'error', // sussess, notice, warning or error
						ttl: 8000,
						onClose: function () {
							bttn.disabled = false;
						}
					});

					// show the notification
					notification.show();
				} else {
					if( OpticoExtra ){
						iOptico ++
						$('#OpticoRow').append('<div class="row row-td ui-widget-content" data-Remove="OpticoExtraR'+iOptico+'">'+
														'<div class="cell" id="MarcaOpticoCell'+iOptico+'"></div>'+
														'<div class="cell" id="TipoOpticoCell'+iOptico+'"></div>'+
														'<div class="cell" id="InterfazOpticoCell'+iOptico+'"></div>'+
														'<input id="LOPTICO'+iOptico+'" class="idOPTICO" type="hidden" value="" />'+
														'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
													'</div>')
						$('#MarcaOpticoCell'+iOptico).html(TempToAdd.Marca.Nombre)
						$('#TipoOpticoCell'+iOptico).html(TempToAdd.Tipo.Nombre)
						$('#InterfazOpticoCell'+iOptico).html(TempToAdd.Interf)
						$('#LOPTICO'+iOptico).val(TempToAdd.Id)
						$('#OpticoForm').append('<div class="row row-td ui-widget-content" data-Remove="OpticoExtraR'+iOptico+'"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
						TempToAdd = ''
					}else{
						$('#MarcaOpticoCell').html(TempToAdd.Marca.Nombre)
						$('#TipoOpticoCell').html(TempToAdd.Tipo.Nombre)
						$('#InterfazOpticoCell').html(TempToAdd.Interf)
						$('#LOPTICO').val(TempToAdd.Id)
						OpticoExtra = 'OK'
						TempToAdd = ''
					}
					$('#tipoOptico').val('')
					$('#marcaOptico').val('')
					$('#interfazOptico').val('')
				}
			}else{
				var notification = new NotificationFx({
					wrapper: document.body,
					message: 'Seleccione todos los campos',
					layout: 'growl',
					effect: 'genie',
					type: 'error', // sussess, notice, warning or error
					ttl: 8000,
					onClose: function () {
						bttn.disabled = false;
					}
				});

				// show the notification
				notification.show();
			}
		}
	})	
	$('.classWS').on('click', '#Software', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'Software'
			var data = {  }
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd)
		}
	})
	var iSoftware = 1
	$('.classWS').on('click', '#AddSoftware', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			if( TempToAdd !== '' ){
				var missmatch = [];
				$('input', '#SoftwareRow').each(function() {
					var ID = $(this).val();
					if ( ID == TempToAdd.Id ) {
						missmatch.push(ID);
					}
				});
				if (missmatch.length > 0) {
					var notification = new NotificationFx({
						wrapper: document.body,
						message: 'Este software ya se encuentra agregado.<br> Seleccione otro.',
						layout: 'growl',
						effect: 'genie',
						type: 'error', // sussess, notice, warning or error
						ttl: 8000,
						onClose: function () {
							bttn.disabled = false;
						}
					});

					// show the notification
					notification.show();
				} else {
					if( SoftwareExtra ){
						iSoftware ++
						$('#SoftwareRow').append('<div class="row row-td ui-widget-content" data-Remove="SoftwareExtra'+iSoftware+'">'+
													'<input id="SOFTWARE'+iSoftware+'" class="idSOFTWARE" type="hidden" value="" />'+
													'<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
													'<div class="cell" id="SoftwareCell'+iSoftware+'"></div>'+
												'</div>')
						$('#SoftwareCell'+iSoftware).html(TempToAdd.Nombre)
						$('#SOFTWARE'+iSoftware).val(TempToAdd.Id)
						TempToAdd = ''
					}else{
						$('#SoftwareCell').html(TempToAdd.Nombre)
						$('#SOFTWARE').val(TempToAdd.Id)
						SoftwareExtra = 'OK'
						TempToAdd = ''
					}
					$('#Software').val('')
				}
			}
		}
	})	
	// Remover Hardware
	$('.classWS').on('click', '.remove', function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			EdicionDeshabilitadaNotif();
		}
		else {
			var RowRemove = $(this).parents('.row-td').attr('data-Remove')
			switch(RowRemove){
				case 'FirstMother':
					$('div[data-Remove='+RowRemove+']').remove()
					$('#MotherRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstMother">'+
							'<input type="hidden" id="Motherboard">'+
							'<div class="cell" id="MarcaMotherCell"></div>'+
							'<div class="cell" id="ModeloMotherCell"></div>'+
							'<div class="cell" id="ChipsetMotherCell"></div>'+
							'<div class="cell" id="SocketMotherCell"></div>'+
							'<div class="cell" id="USBMotherCell"></div>'+
							'<div class="cell" id="PCIEMotherCell">'+
								'<ul>'+
									'<li>'+
										'<input type="radio" id="PCI-E" name="PCIAGP">'+
										'<div class="smallchek"><div class="inside"></div></div>'+
									'</li>'+
								'</ul>'+
							'</div>'+
							'<div class="cell" id="AGPMotherCell">'+
								'<ul>'+
									'<li>'+
										'<input type="radio" id="AGP" name="PCIAGP">'+
										'<div class="smallchek"><div class="inside"></div></div>'+
									'</li>'+
								'</ul>'+
							'</div>'+
							'<div class="cell" id="RAMMotherCell"></div>'+
							'<div class="cell" id="SATAMotherCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstMicro':
					$('div[data-Remove='+RowRemove+']').remove()
					$('#MicroRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstMicro">'+
							'<input type="hidden" id="CPU">'+
							'<div class="cell" id="MarcaMicroCell"></div>'+
							'<div class="cell" id="ModeloMicroCell"></div>'+
							'<div class="cell" id="FrecMicroCell"></div>'+
							'<div class="cell" id="CacheMicroCell"></div>'+
							'<div class="cell" id="SocketMicroCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstDisco':
					$('div[data-Remove='+RowRemove+']').remove()
					DiscoExtra = ''
					$('#DiscoRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstDisco">'+
							'<input type="hidden" class="idHDD" id="HDD">'+
							'<div class="cell" id="MarcaDiscoCell"></div>'+
							'<div class="cell" id="InterfazDiscoCell"></div>'+
							'<div class="cell" id="CapacidadDiscoCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstRam':
					$('div[data-Remove='+RowRemove+']').remove()
					RamExtra = ''
					$('#RamRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstRam">'+
							'<input type="hidden" class="idRAM" id="RAM">'+
							'<div class="cell" id="MarcaRamCell"></div>'+
							'<div class="cell" id="TipoRamCell"></div>'+
							'<div class="cell" id="CapacidadRamCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstVideo':
					$('div[data-Remove='+RowRemove+']').remove()
					VideoExtra = ''
					$('#VideoRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstVideo">'+
							'<input type="hidden" class="idVIDEO" id="VIDEO">'+
							'<div class="cell" id="MarcaVideoCell"></div>'+
							'<div class="cell" id="ModeloVideoCell"></div>'+
							'<div class="cell" id="InterfazVideoCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstRed':
					$('div[data-Remove='+RowRemove+']').remove()
					RedExtra = ''
					$('#RedRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstRed">'+
							'<input type="hidden" class="idRED" id="RED">'+
							'<div class="cell" id="MarcaRedCell"></div>'+
							'<div class="cell" id="ModeloRedCell"></div>'+
							'<div class="cell" id="TecnologiaRedCell"></div>'+
							'<div class="cell" id="VelocidadRedCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstOptico':
					$('div[data-Remove='+RowRemove+']').remove()
					OpticoExtra = ''
					$('#OpticoRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstOptico">'+
							'<input type="hidden" class="idOPTICO" id="LOPTICO">'+
							'<div class="cell" id="MarcaOpticoCell"></div>'+
							'<div class="cell" id="TipoOpticoCell"></div>'+
							'<div class="cell" id="InterfazOpticoCell"></div>'+
							'<div class="cell img-center"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'</div>')
					break;
				case 'FirstSoft':
					$('div[data-Remove='+RowRemove+']').remove()
					SoftwareExtra = ''
					$('#SoftwareRow').append('<div class="row row-td ui-widget-content" data-Remove="FirstSoft">'+
						'<div class="cell img-center" scope="col"><img class="button remove" id="RemSoftware" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
						'<div class="cell" id="SoftwareCell"></div>'+
						'<input type="hidden" class="idSOFTWARE" id="SOFTWARE">'+
						'</div>')
					break;
				default:
					$('div[data-Remove='+RowRemove+']').remove()
			}
		}
	})
	// Remover Hardware
	//-->
	// Hardware Calls
	//-->	
    // Monitores
	$('#MNTecnologia').click(function () {
	    var disable = $(this).attr('readonly');
	    // Check input status
	    if (disable === 'readonly') {
	        // If input is readonly, diable functions
	        e.preventDefault();
	    }
	    else {
	        var MNTecnologia = $(this)
	        // Ajax
	        $.ajax({
	            url: pathServer + 'TecnologiaMonitores',
	            type: 'post',
	            dataType: 'json',
	            beforeSend: function () {
	                // Show Loading
	                NProgress.start();
	            },
	            success: function (response) {
	                // Hide Loading
	                NProgress.done();

	                // Values
	                var raw = response;
	                var source = [];
	                var mapping = {};
	                for (var i = 0; i < raw.length; ++i) {
	                    source.push(raw[i].Nombre);
	                    mapping[raw[i].Nombre] = raw[i].Id;
	                }
	                MNTecnologia.autocomplete({
	                    source: source,
	                    minLength: 0,
	                    select: function (event, ui) {
	                        var dataId = mapping[ui.item.value]
	                        $(this).attr('data-id', dataId);
	                    },
	                    close: function () {
	                        // Finish selection and make a blur
	                        // $(this).blur();
	                    }
	                }).val('').autocomplete('search');
	            },
	            error: function (response) {
	                // Hide Loading
	                NProgress.done();
	            }
	        })
	        // Ajax
	    }
	})
	$('#MNRelAs').click(function (e) {
	    var disable = $(this).attr('readonly');
	    // Check input status
	    if (disable === 'readonly') {
	        // If input is readonly, diable functions
	        e.preventDefault();
	    }
	    else {
	        var MNTecnologia = $(this)
	        // Ajax
	        $.ajax({
	            url: pathServer + 'RelacionAspecto',
	            type: 'post',
	            dataType: 'json',
	            beforeSend: function () {
	                // Show Loading
	                NProgress.start();
	            },
	            success: function (response) {
	                // Hide Loading
	                NProgress.done();

	                // Values
	                var raw = response;
	                var source = [];
	                var mapping = {};
	                for (var i = 0; i < raw.length; ++i) {
	                    source.push(raw[i].Nombre);
	                    mapping[raw[i].Nombre] = raw[i].Id;
	                }
	                MNTecnologia.autocomplete({
	                    source: source,
	                    minLength: 0,
	                    select: function (event, ui) {
	                        var dataId = mapping[ui.item.value]
	                        $(this).attr('data-id', dataId);
	                    },
	                    close: function () {
	                        // Finish selection and make a blur
	                        // $(this).blur();
	                    }
	                }).val('').autocomplete('search');
	            },
	            error: function (response) {
	                // Hide Loading
	                NProgress.done();
	            }
	        })
	        // Ajax
	    }
	})
    $('#AltaMonitorBttn, #ModMonitorBttn').click(function (e) {
        initialState();
		if (!$('#fullscreen').hasClass('fullscreen'))
			{
				$('#fullscreenBtn').trigger(jQuery.Event("click"))
			}
	    if (e.target.id == 'AltaMonitorBttn') {
	        window.AltaModeloMonitor = true;
	    } else {
	        window.EdicionModeloMonitor = true;
	    }	    	    
	    var inputName
	    var NoEdtiable = /^MNTecnologia$|^MNTamaño$|^MNRelAs$|^ModMArcaMonitor|^ModModeloMonitor/;
	    $('#MNselector input:text , #MNselector textarea').each(function () {
	        inputName = $(this).attr('name');
	        if (NoEdtiable.test(inputName)) {
	            $(this).removeAttr('readonly');
				$(this).val('');
	        }
	    });
		if ( $(this).attr('id') == 'AltaMonitorBttn' )
			{
				window.EditarModMonitor = 0;
			}
		else if ( $(this).attr('id') == 'ModMonitorBttn' )
			{
				window.EditarModMonitor = 1;
			} 
	})
	$('#ModModeloMonitor').click(function () {
	    if ($(this).attr('readonly') === 'readonly' | window.AltaModeloMonitor) {
	        e.preventDefault()
	    } else {
	        $(this).val('').autocomplete('search')
	    }
	})
	// Monitores
	//-->	
	// Impresoras
	$('#PRTipo').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			var PRTipo = $(this)
			// Ajax
			$.ajax({
				url:   pathServer+'TipoImpresora',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].Id;
					}
					PRTipo.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							$(this).attr('data-id', dataId);
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					}).val('').autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
			// Ajax
		}
	})
	$('#PRTecnologia').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			var PRTecnologia = $(this)
			// Ajax
			$.ajax({
				url:   pathServer+'TecnologiaImpresora',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].Id;
					}
					PRTecnologia.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							$(this).attr('data-id', dataId);
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					}).val('').autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
			// Ajax
		}
	})
	$('#PRRendimiento').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			var PRRendimiento = $(this)
			// Ajax
			$.ajax({
				url:   pathServer+'RendimientoImpresora',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].Id;
					}
					PRRendimiento.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							$(this).attr('data-id', dataId);
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					}).val('').autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
			// Ajax
		}
	})
	$('#PRConectividad').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			var PRConectividad = $(this)
			// Ajax
			$.ajax({
				url:   pathServer+'ConectividadImpresora',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].id;
					}
					PRConectividad.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							var Texto = ui.item.value
							
							var missmatch = [];
							$('#PRConectividades span').each(function() {
								var ID = $(this).attr('data-id');
								if ( ID == dataId ) {
									missmatch.push(ID);
								}
							});
							if (missmatch.length > 0) {
							    mostrarError('Esta opcion ya se encuentra agregada.<br> Seleccione otra.')
							} else {
								$('#PRConectividades .tags').append('<span id="conectividad" class="tag ui-widget-content" data-id="'+dataId+'">'+Texto+'</span>')
							}
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					}).val('').autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
		}
	})
	$('#PRInsumosTipo').click(function(){
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			var PRInsumosTipo = $(this)
			// Ajax
			$.ajax({
				url:   pathServer+'TipoInsumo',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].Id;
					}
					PRInsumosTipo.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							$(this).attr('data-id', dataId);
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					})/*.val('')*/.autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
			// Ajax
		}
	})
	$('#PRInsumosCod').click(function(){
		var disable = $(this).attr('readonly');
		var editInsumo = $(this).attr('data-editable');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, disable functions
			e.preventDefault();
		}
		else if(editInsumo == 'Y') {
			// If edit insumo is eneabled, disable functions
			e.preventDefault();
		}
		else {
			var PRInsumosCod = $(this)
			// Ajax
			$.ajax({
				data: {idTipo: $('#PRInsumosTipo').attr('data-id') },
				url:   pathServer+'InsumoXTipo',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Hide Loading
					NProgress.done();

					// Values
					var raw = response;
					var source = [];
					var mapping = {};
					for (var i = 0; i < raw.length; ++i) {
						source.push(raw[i].Nombre);
						mapping[raw[i].Nombre] = raw[i].Id;
					}
					PRInsumosCod.autocomplete({
						source: source,
						minLength: 0,
						select: function (event, ui) {
							var dataId = mapping[ui.item.value]
							var Texto = $('#PRInsumosTipo').val()+' /-/ '+ui.item.value							
							var missmatch = [];
							$('#PRInsumos span').each(function() {
								var ID = $(this).attr('data-id');
								if ( ID == dataId ) {
									missmatch.push(ID);
								}
							});
							if (missmatch.length > 0) {
							    mostrarError('Esta opcion ya se encuentra agregada.<br> Seleccione otra.', 'error')
							} else {
								$('#PRInsumos .tags').append('<span id="insumo" class="tag ui-widget-content" data-id="'+dataId+'">'+Texto+' <i class="fa fa-pencil" aria-hidden="true"></i></span>')
							}
							$('#PRInsumosCod').val('');
							return false;
						},
						close: function () {
							// Finish selection and make a blur
							// $(this).blur();
						}
					}).val('').autocomplete('search');
				},
				error:  function(response) {
					// Hide Loading
					NProgress.done();
				}
			})
			// Ajax
		}
	})
	$('fieldset').on('dblclick', '.tag', function(e){
		if ( $(this).parent().parent().attr('readonly') === 'readonly'){
			e.preventDefault()
		}else{
			 if (!$(e.target).hasClass('taghover')) {
				e.preventDefault()
				$(this).remove()
			}
		}
	})
    $('#AltaImpresoraBttn, #ModImpresoraBttn').click(function () {
        initialState();
		if (!$('#fullscreen').hasClass('fullscreen'))
			{
				$('#fullscreenBtn').trigger(jQuery.Event("click"))
			}
		$('#ModMArcaImpresora').removeAttr('readonly')
		$('#PRConectividad, #PRInsumosTipo, #PRInsumosCod').removeClass('hide')
		var inputName
		var NoEdtiable = /^PRTipo$|^PRTecnologia$|^PRRendimiento$|^PRConectividad$|^PRInsumosTipo$|^PRInsumosCod$|^ModMArcaImpresora$|^ModModeloImpresora$/;
		$('#PRselector input:text').each(function () {
			inputName = $(this).attr('name');
			if(NoEdtiable.test(inputName)){
				$(this).removeAttr('readonly');
				$(this).val('');
			}
		});
		$('#PRConectividades, #PRInsumos').removeAttr('readonly');		
		if ( $(this).attr('id') == 'AltaImpresoraBttn' )
			{
				window.EditarModImpresora = 0;
			}
		else if ( $(this).attr('id') == 'ModImpresoraBttn' )
			{
				window.EditarModImpresora = 1;
			}
	})
	$('#ModMArcaImpresora').click(function(e){
		if ( $(this).attr('readonly') === 'readonly'){
			e.preventDefault()
		}
	})
	$('#ModModeloImpresora').click(function(){
		if ( $(this).attr('readonly') === 'readonly'){
			e.preventDefault()
		}else{
			$(this).val('').autocomplete('search')
		}
	})
	// Impresoras
	//-->	
	$('#TipoElementoConf').click(function(){
		var TipoElementoConf = $(this)
		// Ajax
		$.ajax({
			url:   pathServer+'TiposEquipoGral',
			type:  'post',
			dataType: 'json',
			beforeSend: function () {
				// Show Loading
				NProgress.start();
			},
			success:  function(response) {
				// Hide Loading
				NProgress.done();

				// Values
				var raw = response;
				var source = [];
				var mapping = {};
				for (var i = 0; i < raw.length; ++i) {
					source.push(raw[i].Nombre);
					mapping[raw[i].Nombre] = raw[i].id;
				}
				TipoElementoConf.autocomplete({
					source: source,
					minLength: 0,
					select: function (event, ui) {
						var dataId = mapping[ui.item.value]
						$(this).attr('data-id', dataId);
					},
					close: function () {
						// Finish selection and make a blur
						// $(this).blur();
					}
				}).val('').autocomplete('search');
			},
			error:  function(response) {
				// Hide Loading
				NProgress.done();
			}
		})
		// Ajax
	})    
    // Ajax
    // Send data    
    // Deshabilita edicion de campos
    $('#AceptarCambios').click(function () {
        // Ventana de confirmacion
        $('.classMarca').addClass('hide');
        $('.classEnviar').removeClass('hide');
        $('span#works_btn').trigger(jQuery.Event("click"));
    });	
	// Altas
	$('#EquipoNuevo').click(function(){
		window.Altas = 'Y';		
		//mostrarDetails();		
		RestoreOriginalCode()// Clean all inventory info and restores original code	
		newState();
	})
	//-->	
	// Print
	$('#ImprOblea').click(function(e) {
		e.preventDefault()
		var oblea = $('#Oblea').val();
		if (regularEquipo.test(oblea)) window.open('/Print?id=' + oblea + '&area=Soporte Técnico', 'Imprimir')
		else mostrarErrorFx('Seleccione un equipo Primero', 'error');
	});
	// Print
	//-->	
	// Replace jQuery UI Autcomplete scroll
	$('#body').on( "CreateNiceScroll", function( event, myName ) {
	    $('.overflow-auto, .ui-autocomplete').niceScroll(
            {
                cursorcolor: "#24890D",
                zindex: "999",
                cursorborder: "0",
                horizrailenabled: "false",
                bouncescroll: "true"
            });
	})
	//$(document)..overflow-auto, 
	// Replace jQuery UI Autcomplete scroll
    //-->
	initialState();
});//

function mostrarDetailsPc() {
    if (!$('#WSselector').is(':visible')) {    
        hideDetails()
        $('.classClaves,#WSselector,#fullscreen,#fullscreenBtn').fadeIn(800)
    }
}
function mostrarDetailsMn() {
    if (!$('#MNselector').is(':visible')) {
        hideDetails()
        $('#MNselector,#fullscreen,#fullscreenBtn').fadeIn(800)
    }
}
function mostrarDetailsPr() {
    if (!$('#PRselector').is(':visible')) {
        hideDetails()
        $('#PRselector,#fullscreen,#fullscreenBtn').fadeIn(800)
    }
}
function mostrarDetailsVs() {    
    hideDetails()
}
function hideDetails() {
    $('#fullscreenBtn').fadeOut(250)
    $('#fullscreen').fadeOut(800);
    $('#WSselector').fadeOut(250)
    $('#MNselector').fadeOut(250)
    $('#PRselector').fadeOut(250)
}
function initialState() {
    window.State = 'initial';
    $('#CancelarCambios').hide();
    $('#ImprOblea').hide();
    $('#editData').hide();
    $('#AceptarCambios').hide();
    $('#EquipoNuevo').show();
    $('#LineaBase').hide();
    // Clean all inputs/textarea
    $('input, textarea','#content-box').each(function () {
        $(this).val('');
        $(this).attr('readonly', 'readonly');
        $(this).attr('data-id', '');
        $(this).removeClass('error');
    });
    $('#content-box .button').each(function () {
        $(this).attr('readonly', 'readonly');
    });
    $('#PRConectividades .tags, #PRInsumos .tags').html('');
    $('#PRInsumosTipo, #PRInsumosCod, #PRConectividad').addClass('hide')
    RestoreOriginalCode();
    $('#Estado').removeAttr('disabled').attr('placeholder', 'Campo Obligatorio').attr('readonly', 'readonly');
    OcultarRelaciones();
}
function editState() {
    window.Altas = undefined;
    window.State = 'edit';
    $('#AceptarCambios').show();
    $('#CancelarCambios').show();
    $('#editData').hide();
    $('#ImprOblea').hide();
    $('#EquipoNuevo').hide();    
    //$('#editData').hide();
	var inputName
	var NoEdtiable = /^Oblea$|^PRTipo$|^PRTecnologia$|^PRRendimiento$|^PRConectividad$|^PRInsumosTipo$|^PRInsumosCod$|^MNTecnologia$|^MNTamaño$|^MNRelAs$|^ModMArcaImpresora$|^ModModeloImpresora$/;
	$('input:text').each(function () {
	    if (typeof Altas === 'undefined') {
	        inputName = $(this).attr('name');
	        if (!NoEdtiable.test(inputName)) {
	            $(this).removeAttr('readonly');
	        }
	    } else {
	        inputName = $(this).attr('name');
	        if (!NoEdtiable.test(inputName)) {
	            $(this).removeAttr('readonly');

	            // NO COPIAR //
	            $('#Oblea').removeAttr('readonly');
	            // NO COPIAR //
	        }
	    }
	});
    $('textarea').each(function () {
        $(this).removeAttr('readonly');
    });
    $('#content-box .button').each(function () {
        $(this).removeAttr('readonly');
    });
    $('input:radio').each(function () {
        $(this).removeAttr('disabled');
    });
    $('#AltaComponentesBttn').removeAttr('readonly');
    $('#AgregarRelacion').removeAttr('readonly');
    OcultarRelaciones();
}
function newState() {
    window.Altas = 'Y';
    window.State = 'nuevo';
	var inputName
	var NoEdtiable = /^ClaveBIOS|^ClaveAdmin$|^PRTipo$|^PRTecnologia$|^PRRendimiento$|^PRConectividad$|^PRInsumosTipo$|^PRInsumosCod$|^MNTecnologia$|^MNTamaño$|^MNRelAs$|^ModMArcaImpresora$|^ModModeloImpresora$/;
    $('input, textarea', '#content-box').each(function () {
		inputName = $(this).attr('name');
		if(!NoEdtiable.test(inputName)){
			$(this).val('');
			$(this).removeAttr('readonly', 'readonly');
			$(this).attr('data-id', '');
			$(this).removeClass('error');
		}
    });
    $('#content-box .button').each(function () {
        $(this).removeAttr('readonly');
    });
    $('#editData').hide();
    $('#EquipoNuevo').hide();
    $('#CancelarCambios').show();
    $('#LineaBase').hide();
    $('#AceptarCambios').show();
    $('#ImprOblea').hide();
    $('#Estado').attr('disabled', 'disabled').removeAttr('placeholder');
    OcultarRelaciones();
}
function showingState() {
    window.State = 'showing';
    $('#ImprOblea').show();
    $('#editData').show();
    $('#AceptarCambios').hide();
    $('#LineaBase').show();
    OcultarRelaciones();
}
function EdicionDeshabilitadaNotif() {
    mostrarError('Edición deshabilitada', 'error');
}
function bindAutocompleteModelo(pos, target) {
    //Modelos Impresora
    var elem = $('#Modelo, #ModModeloImpresora')

    var idMarca
    if (target.is('#Marca')) {
        idMarca = $('#Marca').attr('data-id');
    } else if (target.is('#ModMArcaImpresora')) {
        idMarca = $('#ModMArcaImpresora').attr('data-id');
    }

    var position
    if (pos == null) {
        position = { my: "right top", at: "right bottom", }
    } else {
        position = pos
    }
    // Ajax
    $.ajax({
        url: pathServer + 'ModelosImpresora',
        type: 'post',
        data: { 'marca': idMarca },
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();

            // Values                
            var source = [];
            for (var i = 0; i < response.length; ++i) {
                var mapping = {};
                mapping.label = response[i].Nombre;
                mapping.value = response[i].id;
                source.push(mapping);
            }
            elem.autocomplete({
                source: source,
                minLength: 0,
                position: position,
                select: function (event, ui) {
                    var dataId = ui.item.value;
                    $(this).attr('data-id', dataId);
                    $(this).val(ui.item.label);
                    getDataImpresora(ui.item.value);
                    return false;
                }
            })
        },
        error: function (response) {
            // Hide Loading
            NProgress.done();
        }
    })
    // Ajax
}
function bindAutocompleteModeloM(pos, target) {
    //Modelos Monitor
    var elem = $('#Modelo, #ModModeloMonitor')

    var idMarca
    if (target.is('#Marca')) {
        idMarca = $('#Marca').attr('data-id');
    } else if (target.is('#ModMArcaMonitor')) {
        idMarca = $('#ModMArcaMonitor').attr('data-id');
    }   
    var position
    if (pos == null) {
        position = { my: "right top", at: "right bottom", }
    } else {
        position = pos
    }
    // Ajax
    $.ajax({
        url: pathServer + 'ModelosMonitor',
        type: 'post',
        data: { 'marca': idMarca },
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();

            // Values
            var source = [];
            for (var i = 0; i < response.length; ++i) {
                var mapping = {};
                mapping.label = response[i].Nombre;
                mapping.value = response[i].id;
                source.push(mapping);
            }
            elem.autocomplete({
                source: source,
                minLength: 0,
                position: position,
                select: function (event, ui) {
                    var dataId = ui.item.value;
                    $(this).attr('data-id', dataId);
                    $(this).val(ui.item.label);

                    for (var i = 0; i < response.length; ++i) {
                        if (response[i].id == ui.item.value) {
                            var selected = response[i]
                        }

                    }
                    CompletarMN(selected);
                    return false;
                }
            })
        },
        error: function (response) {
            // Hide Loading
            NProgress.done();
        }
    })
    // Ajax
}
// Set original code
function RestoreOriginalCode() {
    if (typeof RestoreWS === 'undefined' || RestoreWS === null) {
    } else {
        $("#RestoreHardware").replaceWith(RestoreWS.clone())
    }

}
// GetEquipo
window.GetEquipo = function (oblea) {
	initialState()
    RestoreOriginalCode();
    $.ajax({
        data: { 'oblea': oblea },
        url: 'Inventario/GetEquipoSoporte',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();

            // create the notification
            var message, type;
            if (response['Info'] === 'No inicializado') {

                message = '<p>' +/*response['Info']+'<br>'+*/response['Detail'] + '</p>';
                type = 'error';
                mostrarError(message, type);
                $('#tBuscar').focus()

            } else {               
                showingState();                
                CompletarBasicInfo(response)
                // Hardware Info
                var TipoOblea = $('#Oblea').val().substring(0, 2)
                if (TipoOblea == 'WS' || TipoOblea == 'NB') {// PC o Notebook
                    CompletarWS(response)
                    mostrarDetailsPc();
                } else if (TipoOblea == 'MN') { // Monitores
                    CompletarMN(response);
                    mostrarDetailsMn();
                } else if (TipoOblea == 'PR') { // Impresoras
                    CompletarPR(response);
                    mostrarDetailsPr();
                }
            }
        },
        error: function (response, textStatus) {
            // Hide Loading
            NProgress.done();
            mostrarError(response.Detail, 'error');

        }
    });
}
// GetEquipo
//-->
function getDataImpresora(idModelo){   
    $.ajax({
        url: pathServer + 'GetDatosModelo',
        type: 'post',
        data: { 'idModelo': idModelo },
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();            // Values                
            
            CompletarPREdit(response);
        },
        error: function (response) {
            // Hide Loading
            NProgress.done();
        }
    })
}
//CompletarBasicInfo
function CompletarBasicInfo (response) {
    $('#idEquipo').val(response['idEquipo'])
    $('#Oblea').val(response['Oblea'])
    if (response.NroSerie !== null) {
        $('#NroSerie').val(response['NroSerie'])
    }
    $('#idTipo').val(response['TipoEquipo']['Nombre']).attr('data-id', response['TipoEquipo']['id'])
    if (response.Marca !== null) {
        $('#Marca').val(response['Marca']['Nombre']).attr('data-id', response['Marca']['idMarca'])
    }
    $('#Modelo').val(response.Modelo);
    if (response.idModelo) {
        $('#Modelo').attr('data-id', response.idModelo);
    }
    if (response['Jurisdiccion'] !== null) {
        $('#Jurisdiccion').val(response['Jurisdiccion']['Nombre']).attr('data-id', response['Jurisdiccion']['idJurisdiccion'])
    }
    if (response['Dependencia'] !== null) {
        $('#Ubicacion').val(response['Dependencia']['Nombre']).attr('data-id', response['Dependencia']['idDependencia'])
    }
    if (response['Estado'] !== null) {
        $('#Estado').val(response['Estado']['Nombre']).attr('data-id', response['Estado']['idEstado'])
    }
    $('#Observaciones').val(response['Observaciones'])

    var TipoOblea = $('#Oblea').val().substring(0, 2)
    if (TipoOblea == 'WS' || TipoOblea == 'NB') {// PC o Notebook
        $('#ClaveAdmin').val(response.ClaveAdmin)
        $('#ClaveBIOS').val(response.ClaveBios)
        if (response.Os != null) {
            $('#OS').val(response.Os.Nombre).attr('data-id', response.Os.Id)
        }
    }

    // Autocomplete para modelos
    if ($('#idTipo').attr('data-id') === '2') {
        var pos = { my: "right top", at: "right bottom", }, target = $('#Marca');
        bindAutocompleteModelo(pos, target)
    } else if ($('#idTipo').attr('data-id') === '3') {
        var pos = { my: "right top", at: "right bottom", }, target = $('#Marca');
        bindAutocompleteModeloM(pos, target)
    }
    // Autocomplete para modelos
}
//CompletarBasicInfo
//Completar WS
function CompletarWS(response) {
    // Placa Madre
    if (response.MB !== null) {
        $('#Motherboard').html(response.MB.Id)
        $('#MarcaMotherCell').html(response.MB.Marca)
        $('#ModeloMotherCell').html(response.MB.Modelo)
        $('#ChipsetMotherCell').html(response.MB.Chipset)
        $('#SocketMotherCell').html(response.MB.Socket)
        $('#USBMotherCell').html(response.MB.CantUsb)
        if (response.MB.PciX == true) {
            $('#PCI-E').attr('checked', 'checked')
        } else if (response.Agp == true) {
            $('#AGP').attr('checked', 'checked')
        }
        $('#RAMMotherCell').html(response.MB.cantRam)
        $('#SATAMotherCell').html(response.MB.cantSata)
        $('#Motherboard').val(response.MB.Id)
    }

    // CPU
    if (response.Cpu !== null) {
        $('#CPU').html(response.Cpu.Marca.Id)
        $('#MarcaMicroCell').html(response.Cpu.Marca.Nombre)
        $('#ModeloMicroCell').html(response.Cpu.Modelo)
        $('#FrecMicroCell').html(response.Cpu.Frecuencia)
        $('#CacheMicroCell').html(response.Cpu.Cache)
        $('#SocketMicroCell').html(response.Cpu.Socket.Nombre)
        $('#CPU').val(response.Cpu.Id)
    }
    
    // Discos
    if (response.Hd != null) {
        for (i = 0; i < response.Hd.length; i++) {
            if (i === 0) {
                $('#MarcaDiscoCell').html(response.Hd[i].Marca)
                $('#InterfazDiscoCell').html(response.Hd[i].Interfaz)
                $('#CapacidadDiscoCell').html(response.Hd[i].Capacidad + ' ' + response.Hd[i].UM)
                $('#HDD').val(response.Hd[i].Id)
                DiscoExtra = 'OK'
            } else {
                $('#DiscoRow').append('<div class="row row-td ui-widget-content" data-Remove="DiscoExtraR' + i + '">' +
                    '<div class="cell" id="MarcaDiscoCell' + i + '"></div>' +
                    '<div class="cell" id="InterfazDiscoCell' + i + '"></div>' +
                    '<div class="cell" id="CapacidadDiscoCell' + i + '"></div>' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<input id="HDD' + i + '" class="idHDD" type="hidden" value="" />' +
                '</div>')
                $('#MarcaDiscoCell' + i).html(response.Hd[i].Marca)
                $('#InterfazDiscoCell' + i).html(response.Hd[i].Interfaz)
                $('#CapacidadDiscoCell' + i).html(response.Hd[i].Capacidad + ' ' + response.Hd[i].UM)
                $('#HDD' + i).val(response.Hd[i].Id)
                $('#DiscoForm').append('<div class="row row-td ui-widget-content" data-Remove="DiscoExtraR' + i + '"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
            }
        }
    }
    // Memorias RAM
    if (response.Ram != null) {
        for (i = 0; i < response.Ram.length; i++) {
            if (i === 0) {
                $('#MarcaRamCell').html(response.Ram[i].Marca)
                $('#TipoRamCell').html(response.Ram[i].Tipo)
                $('#CapacidadRamCell').html(response.Ram[i].Capacidad + ' ' + response.Ram[i].UM)
                $('#RAM').val(response.Ram[i].Id)
                RamExtra = 'OK'
            } else {
                $('#RamRow').append('<div class="row row-td ui-widget-content" data-Remove="RamExtraR' + i + '">' +
                    '<div class="cell" id="MarcaRamCell' + i + '"></div>' +
                    '<div class="cell" id="TipoRamCell' + i + '"></div>' +
                    '<div class="cell" id="CapacidadRamCell' + i + '"></div>' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<input id="RAM' + i + '" class="idRAM" type="hidden" value="" />' +
                '</div>')
                $('#MarcaRamCell' + i).html(response.Ram[i].Marca)
                $('#TipoRamCell' + i).html(response.Ram[i].Tipo)
                $('#CapacidadRamCell' + i).html(response.Ram[i].Capacidad + ' ' + response.Ram[i].UM)
                $('#RAM' + i).val(response.Ram[i].Id)
                $('#RamForm').append('<div class="row row-td ui-widget-content" data-Remove="RamExtraR' + i + '"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
            }
        }
    }
    // Placas de video
    if (response.Video != null) {
        for (i = 0; i < response.Video.length; i++) {
            if (i === 0) {
                $('#VIDEO').val(response.Video[i].Id)
                $('#MarcaVideoCell').html(response.Video[i].Marca)
                $('#ModeloVideoCell').html(response.Video[i].Modelo)
                $('#InterfazVideoCell').html(response.Video[i].Interfaz)
                VideoExtra = 'OK'
            } else {
                $('#VideoRow').append('<div class="row row-td ui-widget-content" data-Remove="VideoExtraR' + i + '">' +
                    '<div class="cell" id="MarcaVideoCell' + i + '"></div>' +
                    '<div class="cell" id="ModeloVideoCell' + i + '"></div>' +
                    '<div class="cell" id="InterfazVideoCell' + i + '"></div>' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<input id="VIDEO' + i + '" class="idVIDEO" type="hidden" value="" />' +
                '</div>')
                $('#MarcaVideoCell' + i).html(response.Video[i].Marca)
                $('#ModeloVideoCell' + i).html(response.Video[i].Modelo)
                $('#InterfazVideoCell' + i).html(response.Video[i].Interfaz)
                $('#VIDEO' + i).val(response.Video[i].Id)
                $('#VideoForm').append('<div class="row row-td ui-widget-content" data-Remove="VideoExtraR' + i + '"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
            }
        }
    }
    // Placas de red
    if (response.PlacaRed != null) {
        for (i = 0; i < response.PlacaRed.length; i++) {
            if (i === 0) {
                $('#RED').val(response.PlacaRed[i].Id)
                $('#MarcaRedCell').html(response.PlacaRed[i].Marca)
                $('#ModeloRedCell').html(response.PlacaRed[i].Modelo)
                $('#TecnologiaRedCell').html(response.PlacaRed[i].Tecnologia)
                $('#VelocidadRedCell').html(response.PlacaRed[i].Velocidad)
                RedExtra = 'OK'
            } else {
                $('#RedRow').append('<div class="row row-td ui-widget-content" data-Remove="RedExtraR' + i + '">' +
                    '<div class="cell" id="MarcaRedCell' + i + '"></div>' +
                    '<div class="cell" id="ModeloRedCell' + i + '"></div>' +
                    '<div class="cell" id="TecnologiaRedCell' + i + '"></div>' +
                    '<div class="cell" id="VelocidadRedCell' + i + '"></div>' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<input id="RED' + i + '" class="idRED" type="hidden" value="" />' +
                '</div>')
                $('#MarcaRedCell' + i).html(response.PlacaRed[i].Marca)
                $('#ModeloRedCell' + i).html(response.PlacaRed[i].Modelo)
                $('#TecnologiaRedCell' + i).html(response.PlacaRed[i].Tecnologia)
                $('#VelocidadRedCell' + i).html(response.PlacaRed[i].Velocidad)
                $('#RED' + i).val(response.PlacaRed[i].Id)
                $('#RedForm').append('<div class="row row-td ui-widget-content" data-Remove="RedExtraR' + i + '"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
            }
        }
    }
    // Lectores Opticos
    if (response.DiscoOptico != null) {
        for (i = 0; i < response.DiscoOptico.length; i++) {
            if (i === 0) {
                $('#LOPTICO').val(response.DiscoOptico[i].Id)
                $('#MarcaOpticoCell').html(response.DiscoOptico[i].Marca.Nombre)
                $('#TipoOpticoCell').html(response.DiscoOptico[i].Tipo.Nombre)
                $('#InterfazOpticoCell').html(response.DiscoOptico[i].Interf)
                OpticoExtra = 'OK'
            } else {
                $('#OpticoRow').append('<div class="row row-td ui-widget-content" data-Remove="OpticoExtraR' + i + '">' +
                    '<div class="cell" id="MarcaOpticoCell' + i + '"></div>' +
                    '<div class="cell" id="TipoOpticoCell' + i + '"></div>' +
                    '<div class="cell" id="InterfazOpticoCell' + i + '"></div>' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<input id="LOPTICO' + i + '" class="idOPTICO" type="hidden" value="" />' +
                '</div>')
                $('#MarcaOpticoCell' + i).html(response.DiscoOptico[i].Marca.Nombre)
                $('#TipoOpticoCell' + i).html(response.DiscoOptico[i].Tipo.Nombre)
                $('#InterfazOpticoCell' + i).html(response.DiscoOptico[i].Interf)
                $('#LOPTICO' + i).val(response.DiscoOptico[i].Id)
                $('#OpticoForm').append('<div class="row row-td ui-widget-content" data-Remove="OpticoExtraR' + i + '"><div class="cell rowClear"></div><div class="cell"> </div><div class="cell"> </div><div class="cell"> </div>')
            }
        }
    }
    // Lerctor de Tarjetas
    if (response.LectorTarjeta == true) {
        $('#TarjetaSi').attr('checked', 'checked')
    } else {
        $('#TarjetaNo').attr('checked', 'checked')
    }

    // Mouse
    if (response.PuertoMouse == 'USB') {
        $('#MouseUSB').attr('checked', 'checked')
    } else {
        $('#MousePS2').attr('checked', 'checked')
    }

    // Mouse
    if (response.PuertoTeclado == 'USB') {
        $('#TecladoUSB').attr('checked', 'checked')
    } else {
        $('#TecladoPS2').attr('checked', 'checked')
    }
    
    // Software
    if (response.SoftAdicional != null) {
        for (i = 0; i < response.SoftAdicional.length; i++) {
            if (i === 0) {
                $('#SOFTWARE').val(response.SoftAdicional[i].Id)
                $('#SoftwareCell').html(response.SoftAdicional[i].Nombre)
                SoftwareExtra = 'OK'
            } else {
                $('#SoftwareRow').append('<div class="row row-td ui-widget-content" data-Remove="SoftwareExtra' + i + '">' +
                    '<input id="SOFTWARE' + i + '" class="idSOFTWARE" type="hidden" value="" />' +
                    '<div class="cell img-center" scope="col"><img class="button remove" src="' + soportePath + 'Content/img/rem.png" width="15"></div>' +
                    '<div class="cell" id="SoftwareCell' + i + '"></div>' +
                '</div>')
                $('#SoftwareCell' + i).html(response.SoftAdicional[i].Nombre)
                $('#SOFTWARE' + i).val(response.SoftAdicional[i].Id)
            }
        }
    }
    //-->software
}
//CompletarWS
//CompletarMN
function CompletarMN (response) {
    $('#MNTecnologia').val(response.Tecnologia.Nombre)
    $('#MNTecnologia').attr('data-id', response.Tecnologia.Id)
    $('#MNTamaño').val(response.Tamanio)
    $('#MNRelAs').val(response.RelAspecto.Nombre)
    $('#MNRelAs').attr('data-id', response.RelAspecto.Id)
}
//CompletarMN
//CompletarPR
function CompletarPR(response) {
	$('#PRTipo').val(response.Tipo)
	$('#PRTecnologia').val(response.Tecnologia)
	$('#PRConectividades .tags').html('')
	for(i=0;i<response.Conectividad.length;i++){
		$('#PRConectividades .tags').append('<span id="conectividad'+i+'" class="tag ui-widget-content" data-id="'+response.Conectividad[i].id+'">'+response.Conectividad[i].Nombre+'</span>')
	}
	$('#PRRendimiento').val(response.Rendimiento.Nombre).attr('data-id', response.Rendimiento.Id)
	$('#PRInsumos .tags').html('')
	for(i=0;i<response.Insumos.length;i++){
		$('#PRInsumos .tags').append('<span id="insumo'+i+'" class="tag ui-widget-content" data-id="'+response.Insumos[i].Id+'">'+response.Insumos[i].Tipo+' /-/ '+response.Insumos[i].Nombre+'</span>')
	}
}
//CompletarPR
//EditarModeloImpresrora
function CompletarPREdit(response) {
	$('#PRTipo').val(response.Tipo.Nombre)
	$('#PRTipo').attr('data-id', response.Tipo.Id)
	$('#PRTecnologia').val(response.Tecnologia.Nombre)
	$('#PRTecnologia').attr('data-id', response.Tecnologia.Id)
	$('#PRConectividades .tags').html('')
	for(i=0;i<response.Conectividad.length;i++){
		$('#PRConectividades .tags').append('<span id="conectividad'+i+'" class="tag ui-widget-content" data-id="'+response.Conectividad[i].id+'">'+response.Conectividad[i].Nombre+'</span>')
	}
	$('#PRRendimiento').val(response.Rendimiento.Nombre).attr('data-id', response.Rendimiento.Id)
	$('#PRInsumos .tags').html('')
	for(i=0;i<response.Insumos.length;i++){
		$('#PRInsumos .tags').append('<span id="insumo'+i+'" class="tag ui-widget-content" data-id="'+response.Insumos[i].Id+'">'+response.Insumos[i].Tipo+' /-/ '+response.Insumos[i].Nombre+' <i class="fa fa-pencil" aria-hidden="true"></i></span>')
	}
}
//-->
//MODAL
function Modal(){
	// Get the modal
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var btn = $("span#works_btn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0]; // Only for first close class


	// Get the <send> element that closes the modal
	var send = document.getElementById("SendForm"); // Only for first id	
	send.onclick = function () {
	    modal.style.display = "none";
	    AceptarCambios();
	}

	// When the user clicks the button, open the modal
	btn.each(function(){
		$(this).on("click", function(){
			modal.style.display = "block";
		});
	});

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// When the user clicks on <span> (x), close the modal
	/*send.onclick = function() {
		modal.style.display = "none";
	}*/

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
}
function searchAutocomplete(request, response) {

    var texto = request.term;
    var comienzoEquipo = /^WS|^NB|^PR|^MN|^VS|^SV|^GE|^UPS|^RCK|^AAC|^TBL/;
    texto = texto.toUpperCase();
    var buscoOblea = false;   
    if (comienzoEquipo.test(texto)) {
        buscoOblea = true;
    }  
    var uri;
    if (buscoOblea) uri = 'Inventario/EquipoXOblea';
    else uri = 'Inventario/EquipoXSerie';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val.idEquipo;
                    if (buscoOblea) obj.label = val.Oblea;
                    else obj.label = val.NroSerie;
                    obj.oblea = val.Oblea;
                    obj.serie = val.NroSerie;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
