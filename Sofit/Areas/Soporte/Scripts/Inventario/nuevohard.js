$(function(){
	$('#AltaComponentes').dialog({
		create: function(e, ui) {
			// get the whole widget (.ui-dialog) with .dialog('widget')
			// find the title bar element
			$(this).dialog('widget').removeClass('ui-corner-all').find('.ui-dialog-titlebar, .ui-button').removeClass('ui-corner-all')
			// alter the css classes
			$(this).parent().css('z-index', '1000').css('overflow', 'initial')
		},
		open: function(event, ui) {
			$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
		},
        width: 400,
        height: 380,
		resizable: false,
		autoOpen: false,
		buttons: {
			Cerrar: function() {
				$('#AltaComponentes input').val('')
				$('#AltaComponentes fieldset').hide(0)
				$(this).dialog('close')
			} //end function for Ok button
		}//end buttons
	});
	window.RemROTO = ''// Define RemROTO global
	
})

$(function(){
	// Show notification message
	function HWNotificacion(response){
		// create the notification
		var message, type;
		if(response['Info'] === 'ok'){
			message = '<p>Se actualizaron los datos del equipo</p>';
			type = 'sussess';
		}else{
			message = '<p>'+/*response['Info']+'<br>'+*/response['Detail']+'</p>';
			type = 'error';
		}
		var notification = new NotificationFx({
			wrapper: document.body,
			message: message,
			layout: 'growl',
			effect: 'genie',
			type: type, // sussess, notice, warning or error
			ttl: 8000,
			onClose: function () {
				bttn.disabled = false;
			}
		});

		// show the notification
		notification.show();
	}
	
	var source = [ 
        {  label : 'Placa Madre', value : '17' },
        {  label : 'Microprocesador', value : '23' },
        {  label : 'Disco', value : '18' },
        {  label : 'RAM', value : '19' },
        {  label : 'Video', value : '22' },
        {  label : 'Red', value : '21' },
        {  label : 'Lector Optico', value : '20' },
        {  label : 'Software', value : '8' }
	]
	$('#NvHrdTipo').autocomplete({
		source: source,
		minLength: 0,
		select: function (event, ui) {
			event.preventDefault()
			var dataId = ui.item.value
			$(this).attr('data-id', dataId);
			$(this).val(ui.item.label);
			switch(dataId){
				case '17':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevaMother').fadeIn(300)
					break;
				case '23':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevoMicro').fadeIn(300)
					break;
				case '18':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevoDisco').fadeIn(300)
					break;
				case '19':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevaRam').fadeIn(300)
					break;
				case '22':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevoVideo').fadeIn(300)
					break;
				case '21':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevaRed').fadeIn(300)
					break;
				case '20':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevoOptico').fadeIn(300)
					break;
				case '8':
					$('#AltaComponentes fieldset').fadeOut(0)
					$('#NuevoSoft').fadeIn(300)
					break;
			}
		},
		close: function () {
			// Finish selection and make a blur
			$(this).blur();
		}
	}).click(function (e) {
		$(this).val('');
		// Trigger Search values on fucus
		$(this).autocomplete('search');
		// Trigger KeyDown on focus
		//$(this).trigger(jQuery.Event("keydown"));
	})
	// Open Nuevo Compomente dialog
	$('li').on('click', '#AltaComponentesBttn', function (e) {
	    $('#AltaComponentes').dialog('open')
	})
	// Open Nuevo Compomente dialog
	//-->

	// Hardware Calls
	// Mother
	$('#AltaComponentes').on('click', '#NuevaMotherMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			RemROTO = 'Y'
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: marcaTipo['motherboard'] }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#NuevaMotherChipset', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			window.Puerto90 = 'Y';
			var trigger = $(this)
			var url = 'Chipset'
			var data
			var labelName = 'Nombre'
			var valueName = 'Id'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevaMotherSocket', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'Socket'
			var data
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevaMother', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewMother = new Object()
				NewMother.Marca = $('#NuevaMotherMarca').attr('data-id')
				NewMother.Modelo = $('#NuevaMotherModelo').val()
				NewMother.Chipset = $('#NuevaMotherChipset').attr('data-id')
				NewMother.Socket = $('#NuevaMotherSocket').attr('data-id')
				NewMother.CantUsb = $('#NuevaMotherUSB').val()
				NewMother.cantIde = '0'
				NewMother.cantSata = $('#NuevaMotherSata').val()
				NewMother.cantRam = $('#NuevaMotherRAM').val()
				if ($('#NuevaMotherPCI').is(':checked')){
					NewMother.PciX = true
					NewMother.Agp = false
				}else{
					NewMother.PciX = false
					NewMother.Agp = true
				}
			// Sending object
			$.ajax({
				data:  NewMother,
				url:   pathServer+'NuevoMother',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})

	// Micro
	$('#AltaComponentes').on('click', '#NuevoMicroMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			RemROTO = 'Y'
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: marcaTipo['micro'] }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevoMicroSocket', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			window.Puerto90 = 'Y'
			var trigger = $(this)
			var url = 'Socket'
			var data
			var labelName = 'Nombre'
			var valueName = 'Id'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevoMicro', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewMicro = new Object()
				NewMicro.idmarca = $('#NuevoMicroMarca').attr('data-id')
				NewMicro.idsocket = $('#NuevoMicroSocket').attr('data-id')
				NewMicro.modelo = $('#NuevoMicroModelo').val()
				NewMicro.frecuencia = $('#NuevoMicroVelocidad').val()
				NewMicro.cache = $('#NuevoMicroCache').val()
			// Sending object
			$.ajax({
				data:  NewMicro,
				url:   pathServer+'NuevoMicro',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})

	// Disco
	$('#AltaComponentes').on('click', '#NuevoDiscoInterfaz', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			RemROTO = 'Y'
			var trigger = $(this)
			var url = 'InterfazDisco'
			var data = { tipo: marcaTipo['micro'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevoDiscoMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: $("#NvHrdTipo").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevoDisco', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewDisco = new Object()
				NewDisco.id_marca = $('#NuevoDiscoMarca').attr('data-id')
				NewDisco.id_interfaz = $('#NuevoDiscoInterfaz').attr('data-id')
				NewDisco.capacidad = $('#NuevoDiscoCapacidad').val()+' '+$('#NuevoDiscoUM').val()
			// Sending object
			$.ajax({
				data:  NewDisco,
				url:   pathServer+'NuevoDisco',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})

	// RAM
	$('#AltaComponentes').on('click', '#NuevaRamTipo', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			RemROTO = 'Y'
			var trigger = $(this)
			var url = 'TipoMemoria'
			var data = { tipo: marcaTipo['ram'] }
			var labelName = 'Tipo'
			var valueName = 'idTipo'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevaRamMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If inpit is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: $("#NvHrdTipo").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevaRam', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewRam = new Object()
				NewRam.id_marca = $('#NuevaRamMarca').attr('data-id')
				NewRam.id_tipo = $('#NuevaRamTipo').attr('data-id')
				NewRam.capacidad = $('#NuevaRamCapacidad').val()
				NewRam.UM = $('#NuevaRamUM').val()
			// Sending object
			$.ajax({
				data:  NewRam,
				url:   pathServer+'NuevoRAM',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})

	// Video
	$('#AltaComponentes').on('click', '#NuevoVideoInterfaz', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'InterfazVideo'
			var data = { tipo: marcaTipo['video'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevoVideoMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcaVideoXInterfaz'
			var data = { idInterfaz: $("#NuevoVideoInterfaz").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevoVideo', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewVideo = new Object()
				NewVideo.idmarca = $('#NuevoVideoMarca').attr('data-id')
				NewVideo.idinterfaz = $('#NuevoVideoInterfaz').attr('data-id')
				NewVideo.modelo = $('#NuevoVideoModelo').val()
			// Sending object
			$.ajax({
				data:  NewVideo,
				url:   pathServer+'NuevoVideo',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})

	// Red
	var	source = [ {'label':'Wireless'}, {'label':'Ethernet'} ]
	$('#AltaComponentes').on('click', '#NuevaRedTecnologia', function (e) {
			var disable = $(this).attr('readonly');
			// Check input status
			if (disable === 'readonly') {
				// If input is readonly, diable functions
				e.preventDefault();
			}
			else {
				// If input is writable
				// Clean val
				$(this).val('');
				// Trigger Search values on fucus
				$(this).autocomplete({
					source: source,
					minLength: 0,
					position: { my : "right bottom", at: "right top", },
					select: function (event, ui) {
						var dataId = ui.item.label
						$(this).attr('data-id', dataId);
					},
					close: function () {
						// Finish selection and make a blur
						$(this).blur();
					}
				})
				$(this).autocomplete('search');
				// Trigger KeyDown on focus
				//$(this).trigger(jQuery.Event("keydown"));
			}
	})
	$('#AltaComponentes').on('click', '#NuevaRedMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'MarcasPorTipo'
			var data = { tipo: $("#NvHrdTipo").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevaRed', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewRed = new Object()
				NewRed.tecnologia = $('#NuevaRedTecnologia').attr('data-id')
				NewRed.idmarca = $('#NuevaRedMarca').attr('data-id')
				NewRed.modelo = $('#NuevaRedModelo').val()
				NewRed.velocidad = $('#NuevaRedVelocidad').val()
			// Sending object
			$.ajax({
				data:  NewRed,
				url:   pathServer+'NuevoRed',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})
	
	// Optico
	$('#AltaComponentes').on('click', '#NuevoOpticoTipo', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'TipoOptico'
			var data = { tipo: marcaTipo['optico'] }
			var labelName = 'Nombre'
			var valueName = 'Id'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#NuevoOpticoMarca', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'OpticoXTipo'
			var data = { idTipo: $("#NuevoOpticoTipo").attr("data-Id") }
			var labelName = 'Nombre'
			var valueName = 'idMarca'
			var ReadyToAdd = 'No'
			var UnidadMedida
			var Add = 'Yes'
			Select1(trigger, url, data, labelName, valueName, ReadyToAdd, UnidadMedida, Add)
		}
	})
	$('#AltaComponentes').on('click', '#NuevoOpticoInterfaz', function (e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// If input is writable
			// Call autocomplete
			var trigger = $(this)
			var url = 'OpticoXTipoMarca'
			var data = { idTipo: $("#NuevoOpticoTipo").attr("data-Id"), idMarca: $("#NuevoOpticoMarca").attr("data-Id") }
			var labelName = 'Interf'
			var valueName = 'idInterfaz'
			Select1(trigger, url, data, labelName, valueName)
		}
	})
	$('#AltaComponentes').on('click', '#AddNuevoOptico', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewOptico = new Object()
				NewOptico.idtipo = $('#NuevoOpticoTipo').attr('data-id')
				NewOptico.idmarca = $('#NuevoOpticoMarca').attr('data-id')
				NewOptico.idinterfaz = $('#NuevoOpticoInterfaz').attr('data-id')
			// Sending object
			$.ajax({
				data:  NewOptico,
				url:   pathServer+'NuevoOptico',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})
	
	// Software
	$('#AltaComponentes').on('click', '#AddNuevoSoft', function(e) {
		var disable = $(this).attr('readonly');
		// Check input status
		if (disable === 'readonly') {
			// If input is readonly, diable functions
			e.preventDefault();
		}
		else {
			// Create object to send
			var NewSoft = new Object()
				NewSoft.nombre = $('#NuevoSoftAlta').val()
			// Sending object
			$.ajax({
				data:  NewSoft,
				url:   pathServer+'NuevoSoft',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
					// Show Loading
					NProgress.start();
				},
				success:  function(response) {
					// Show Loading
					NProgress.done();
					HWNotificacion(response);
				},
				error: function(response, textStatus){
					// Show Loading
					NProgress.done();
					HWNotificacion(textStatus);
				}
			})
		}
	})
	// Calls
	//-->
})