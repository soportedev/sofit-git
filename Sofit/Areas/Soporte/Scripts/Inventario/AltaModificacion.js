$(function(){  
    $('#SendMarca').click(function (e) {
        e.preventDefault();
        var NvHrdTipo = $('#NvHrdTipo').val()
        if( NvHrdTipo !== '' ){
            var TipoDisp = $('#NvHrdTipo').attr('data-Id')	
        }
        if( TipoDisp ){
            var idTipoDis = $('#NvHrdTipo').attr('data-Id')
        }
        else{
            var idTipoDis = $('#idTipo').attr('data-Id')
        }
        var vMarca = $('#AddMarca').val()
		
        // Remove error class
        $('#AddMarca').removeClass('error');

        // Check if a valid value
        if (vMarca != '' && vMarca.length > 1) {
            // Ajax
            $.ajax({
                data: { nombre: vMarca, idTipoDis: idTipoDis },
                url: pathServer+'NuevaMarcaSoporte',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Hide Loading
                    NProgress.done();
                    // Recall Marcas
                    FunctionMarca();
					
                    // create the notification
                    var message, type;
                    if(response['Info'] === 'ok'){
                        message = '<p>Se agregó la nueva marca</p>';
                        type = 'sussess';
                    }else{
                        message = '<p>'+/*response['Info']+'<br>'+*/response['Detail']+'</p>';
                        type = 'error';
                    }
                    var notification = new NotificationFx({
                        wrapper: document.body,
                        message: message,
                        layout: 'growl',
                        effect: 'genie',
                        type: type, // sussess, notice, warning or error
                        ttl: 8000,
                        onClose: function () {
                            bttn.disabled = false;
                        }
                    });

                    // show the notification
                    notification.show();
                },
                error: function (response, textStatus) {
                    // Hide Loading
                    NProgress.done();

                    // create the notification
                    var notification = new NotificationFx({
                        wrapper: document.body,
                        message: textStatus,
                        layout: 'growl',
                        effect: 'genie',
                        type: 'error', // sussess, notice, warning or error 
                        ttl: 3000,
                        onClose: function () {
                            bttn.disabled = false;
                        }
                    });

                    // show the notification
                    notification.show();

                }
            });
            // Ajax

            // Get the modal
            var modal = document.getElementById('myModal');
            // Hide modal
            modal.style.display = "none";
        } else {
            // Add error class
            $('#AddMarca').addClass('error');
            // create the notification
            var notification = new NotificationFx({
                wrapper: document.body,
                message: '<p>Ingrese un valor correcto</p>',
                layout: 'growl',
                effect: 'genie',
                type: 'error', // sussess, notice, warning or error 
                ttl: 15000,
                onClose: function () {
                    bttn.disabled = false;
                }
            });

            // show the notification
            notification.show();
        }
        // Check if a valid value
        //-->
    })
    // Impresoras
    $('#AceptarModImpresora').click(function(){
        var ImpresoraModelo = new Object();
        if( $('#ModMArcaImpresora').attr('data-id') !== '' )
            {
                ImpresoraModelo.idMarca = $('#ModMArcaImpresora').attr('data-id')
            }
        else if( $('#Marca').val() !== '' )
            {
                ImpresoraModelo.idMarca = $('#Marca').attr('data-id')
            }
        else
            {
                mostrarError('Seleccione una marca!!!');
                return;
            }
        if ($('#ModModeloImpresora').val() !== '')
            {
                ImpresoraModelo.Nombre = $('#ModModeloImpresora').val();
            }
        else
            {
                mostrarError('Ingrese el nombre del modelo!!!');
                return;
            }
        ImpresoraModelo.Tipo = {}
        if ($('#PRTipo').attr('data-id') !== '')
            {
                ImpresoraModelo.Tipo.Id = $('#PRTipo').attr('data-id')
            }
        else
            {
                mostrarError('Seleccione un tipo!!!');
                return;
            }
        ImpresoraModelo.Tecnologia = {}
        if ($('#PRTecnologia').attr('data-id') !== '')
            {
                ImpresoraModelo.Tecnologia.Id = $('#PRTecnologia').attr('data-id')
            }
        else
            {
                mostrarError('Seleccione una tecnología');
                return;
            }
        ImpresoraModelo.Rendimiento = {};
        if ($('#PRRendimiento').attr('data-id') !== '')
            {
                ImpresoraModelo.Rendimiento.Id = $('#PRRendimiento').attr('data-id');   
            }
        else
            {
                mostrarError('Seleccione un rendiento!!!');
                return;
            }
        ImpresoraModelo.Conectividad = new Array();
        $('#PRConectividades .tag').each(function () {
            var conec = {};
            conec.id = $(this).attr('data-id');
            ImpresoraModelo.Conectividad.push(conec);
        });
        if (ImpresoraModelo.Conectividad.length <= 0)
            {
                mostrarError('Seleccione al menos una conectividad!!!');
                return;
            }
        ImpresoraModelo.Insumos = new Array();
        $('#PRInsumos .tag').each(function(){			
            var ins = {};
            ins.id=$(this).attr('data-id');
            ImpresoraModelo.Insumos.push(ins);
        })
        if (ImpresoraModelo.Insumos.length <= 0)
            {
                mostrarError('Seleccione al menos un insumo!!!');
                return;
            }
        if (EditarModImpresora  == 1)
            {
                if( $('#ModModeloImpresora').attr('data-id') !== '' )
                    {
                        ImpresoraModelo.id = $('#ModModeloImpresora').attr('data-id')
                    }
                else if( $('#Modelo').val() !== '' )
                    {
                        ImpresoraModelo.id = $('#Modelo').attr('data-id')
                    }
                var url = 'Inventario/UpdateModelo';
            }
        else
            {
                var url = 'Inventario/NuevoModelo';
            }
        $.ajax({
            data: JSON.stringify(ImpresoraModelo),
            url: url,//compeltar url
            contentType: 'application/json; charset=utf-8',
            datatype:'json',
            type: 'post',               
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Hide Loading
                NProgress.done();

                // create the notification
                var message, type;
                if (response['Info'] === 'ok') {
                    if (window.Altas != undefined) {
                        message = 'Se agregó el nuevo equipo';
                    } else {
                        message = '<p>Se actualizaron los datos del equipo</p>';
                    }
                    type = 'success';
                    mostrarExito(message);
                    // Send data // Return db data  
                    //initialState();
                    // Ocultar menu y fullscreen
                    $('#PRselector .slideout-menu .slideout-menu-toggle').trigger(jQuery.Event("click"))
                    if ($('#fullscreen').hasClass('fullscreen'))
                        {
                            $('#fullscreenBtn').trigger(jQuery.Event("click"))
                        }
                } else {
                    message = '<p>' +/*response['Info']+'<br>'+*/response['Detail'] + '</p>';
                    type = 'error';
                    // Show Acept buttom
                    $('#editData').trigger('click');
                    mostrarError(message);
                }
                initialState();
            },
            error: function (response, textStatus) {
                // Hide Loading
                NProgress.done();
                mostrarError(response.Detail);
            }
        });
    })
    // Mod / Nuevo Insumo
    $('#PRInsumosCod').keydown(function (e) {
        e.preventDefault
        if (e.which === 13) {
            var NuevoInsumo = {};
            NuevoInsumo.Nombre = $('#PRInsumosCod').val();

            if ($(this).attr('data-editable') == 'Y')
                {
                    var url = 'Inventario/EditInsumo';
                    NuevoInsumo.id = $(this).attr('data-id');
                }
            else
                {
                    var url = 'Inventario/NuevoInsumo';
                    NuevoInsumo.idTipo = $('#PRInsumosTipo').attr('data-id');
                }
            $.ajax({
                data: JSON.stringify(NuevoInsumo),
                url: url,
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',
                type: 'post',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Hide Loading
                    NProgress.done();
                    
                    // create the notification
                    var message, type;
                    if (response['Info'] === 'ok') {
                        message = 'Insumo actualizado';
                        mostrarExito(message);
                        // Send data // Return db data  
                        // initialState();
                    } else {
                        message = ' ' +/*response['Info']+' '+*/response['Detail'] + ' ';                    
                        mostrarError(message);
                    }
                    $('#PRInsumosCod').attr('data-editable', '').val(''); // Elimina el estado de edicion
                },
                error: function (response, textStatus) {
                    // Hide Loading
                    NProgress.done();
                    mostrarError(response.Detail);
                }
            });
        }
    });
    // Editar insumo
    $('#PRInsumos').on('click', 'i', function(){
        var InsumoID = $(this).parent().attr('data-id'); // Guarda el ID

        var str = $(this).parent().text();
        var res = str.split(' /-/ ');

        $('#PRInsumosCod').attr('data-editable', 'Y'); // Deshabilita autocomplete
        $('#PRInsumosCod').val(res[1]).attr('data-id', InsumoID); // Prepara el campo para la edición
    })
    //Impresoras
    // Monitores
    $('#AceptarModMonitor').click(function(){
        var MonitorModelo = {};
        if ($('#ModModeloMonitor').val() !== '')
            {
                MonitorModelo.Nombre = $('#ModModeloMonitor').val();
            }
        else
            {
                mostrarError('Escriba el nombre del modelo!!!');
                return;
            }
        if ($('#ModMArcaMonitor').attr('data-id') !== '')
            {
                MonitorModelo.idMarca = $('#ModMArcaMonitor').attr('data-id');
            }
        else
            {
                mostrarError('Seleccione una marca!!!');
                return;
            }
        MonitorModelo.Tecnologia = {};
        if ($('#MNTecnologia').attr('data-id') !== '')
            {
                MonitorModelo.Tecnologia.Id = $('#MNTecnologia').attr('data-id');
            }
        else
            {
                mostrarError('Seleccione una tecnología!!!');
                return;
            }
        if ($('#MNTamaño').val() !== '')
            {
                MonitorModelo.Tamanio = $('#MNTamaño').val();
            }
        else
            {
                mostrarError('Ingrese un tamaño!!!');
                return;
            }
        MonitorModelo.RelAspecto = {};
        if ($('#MNRelAs').attr('data-id') !== '')
            {
                MonitorModelo.RelAspecto.Id = $('#MNRelAs').attr('data-id');
            }
        else
            {
                mostrarError('Seleccione una Rel. Aspecto');
                return;
            }
        if (EditarModMonitor  == 1)
            {
                var url = 'Inventario/UpdateModeloMonitor';
                if ($('#ModModeloMonitor').attr('data-id') !== '') {
                    MonitorModelo.id = $('#ModModeloMonitor').attr('data-id');
                } else {
                    mostrarError('Seleccione un modelo!!!');
                    return;
                }
            }
        else
            {
                var url = 'Inventario/NuevoModeloMonitor';
            }
        $.ajax({
            data: JSON.stringify(MonitorModelo),
            url: url,//compeltar url
            contentType: 'application/json; charset=utf-8',
            datatype: 'json',
            type: 'post',
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Hide Loading
                NProgress.done();
				
                // create the notification
                var message, type;
                if (response['Info'] === 'ok') {
                    if (window.Altas != undefined) {
                        message = 'Se agregó el nuevo modelo';
                    } else {
                        message = 'Se actualizaron los datos del modelo';
                    }                    
                    mostrarExito(message);
                    // Send data // Return db data  
                    // initialState();
                    // Ocultar menu y fullscreen
                    $('#MNselector .slideout-menu .slideout-menu-toggle').trigger(jQuery.Event("click"))
                    if ($('#fullscreen').hasClass('fullscreen'))
                        {
                            $('#fullscreenBtn').trigger(jQuery.Event("click"))
                        }
                } else {
                    message = ' ' +/*response['Info']+' '+*/response['Detail'] + ' ';                    
                    mostrarError(message);
                }
                initialState();
            },
            error: function (response, textStatus) {
                // Hide Loading
                NProgress.done();
                mostrarError(response.Detail);
            }
        });
    })
    // Monitores
});
function AceptarCambios() {
    var varWSNB = /^WS.*$|^NB.*$/
    var varMN = /^MN.*$/
    var varPR = /^PR.*$/
    var varVS = /^VS.*$|^KS.*$/
    // Validations
    if ($('#Oblea').val()) {
        var Val = $('#Oblea').val();
        if (regularEquipo.test(Val)) {
            var resOblea = 'ok';
            $('#Oblea').removeClass('error');
        } else {
            $('#Oblea').addClass('error');
        }
    } else {
        $('#Oblea').addClass('error');
    }

    if ($('#idTipo').val()) {
        var resTipo = 'ok';
        $('#idTipo').removeClass('error');
    }
    else {
        $('#idTipo').addClass('error');
    }

    if ($('#Marca').val()) {
        var resMarca = 'ok';
        $('#Marca').removeClass('error');
    }
    else {
        $('#Marca').addClass('error');
    }

    if ($('#Jurisdiccion').val()) {
        var resJurisdiccion = 'ok';
        $('#Jurisdiccion').removeClass('error');
    }
    else {
        $('#Jurisdiccion').addClass('error');
    }

    if ($('#Ubicacion').val()) {
        var resUbicacion = 'ok';
        $('#Ubicacion').removeClass('error');
    }
    else {
        $('#Ubicacion').addClass('error');
    }        
    // Validations
    //-->

    if ((resOblea && resTipo && resMarca && resJurisdiccion && resUbicacion ) === 'ok') {
        
        var Equipo = new Object()
        // equipo
        var idEquipo=$('#idEquipo').val();
        if ( idEquipo!= '') {
            Equipo.idEquipo = $('#idEquipo').val()
        }
        Equipo.Dependencia = {};
        Equipo.Jurisdiccion = {};
        Equipo.TipoEquipo = {};
        Equipo.Estado = {};
        Equipo.Estado.idEstado = $('#Estado').attr('data-id');
        Equipo.Oblea = $('#Oblea').val()
        Equipo.NroSerie = $('#NroSerie').val()
        Equipo.Jurisdiccion = {};
        Equipo.Jurisdiccion.id = $('#Jurisdiccion').attr('data-Id');
        Equipo.TipoEquipo = {};
        Equipo.TipoEquipo.id = $('#idTipo').attr('data-Id');
        Equipo.Observaciones = $('#Observaciones').val();
        Equipo.Jurisdiccion.idJurisdiccion = parseInt($('#Jurisdiccion').attr('data-Id'));
        Equipo.Dependencia.idDependencia = $('#Ubicacion').attr('data-Id');
        Equipo.Observaciones = $('#Observaciones').val();
        Equipo.Marca = {};
        Equipo.Marca.idMarca = $('#Marca').attr('data-Id');
        var tipoEquipo = $('#idTipo').val();
        switch (tipoEquipo) {
            case 'Pc':
                if (window.State == 'nuevo') {
                    // If is Altas
                    // Determines the URL to send data
                    // URL for new item
                    Equipo.Url = pathServer + 'NuevoPc';
                } else {
                    // URL for update
                    Equipo.Url = pathServer + 'UpdatePc';
                }                
                Equipo.Os = {};                
                Equipo.Modelo = $('#Modelo').val();               
                Equipo.ClaveBios = $('#ClaveBIOS').val()
                Equipo.ClaveAdmin = $('#ClaveAdmin').val()
                var idOs=$('#OS').attr('data-Id');
                if (idOs != 0) {
                    Equipo.Os.id = idOs;
                }
                var motherVar = $('#Motherboard').val();// Motherboar id
                if (motherVar != 0) {
                    Equipo.MB = {};
                    Equipo.MB.id = motherVar;
                }
                var cpuVar=$('#CPU').val();// CPU id
                if (cpuVar != 0) {
                    Equipo.Cpu = {};
                    Equipo.Cpu.Id = cpuVar;
                }
                var HDD = new Array();// Get HDDs
                $('#DiscoRow input').each(function (i) {                    
                    if ($(this).is('.idHDD')) {
                        var hddO = {};
                        var id = $(this).val();
                        if (id != 0) {
                            hddO.id = id;
                            HDD.push(hddO);
                        }
                    }                    
                })
                Equipo.Hd = HDD// Push array into obj
                var RAM = new Array();// Get RAMs
                $('#RamRow input').each(function (i) {                    
                    
                    if ($(this).is('.idRAM') ) {
                        var id = $(this).val();
                        if (id != 0) {
                            var ramo = {};
                            ramo.id = id;
                            RAM.push(ramo);
                        }
                    }
                })
                Equipo.Ram = RAM// Push array into obj

                var VIDEO = new Array();// Get VIDEOs
                $('#VideoRow input').each(function (i) {                   
                    if ($(this).is('.idVIDEO')) {
                        var id = $(this).val();
                        if (id != 0) {
                            var videoO = {};
                            videoO.Id = id;
                            VIDEO.push(videoO);
                        }
                    }
                })
                Equipo.Video = VIDEO// Push array into obj

                var RED = new Array();// Get REDs
                $('#RedRow input').each(function (i) {                    
                    if ($(this).is('.idRED')) {                        
                        var id = $(this).val();
                        if (id != 0) {
                            var red0 = {};
                            red0.Id = id;
                            RED.push(red0);
                        }
                    }
                })
                Equipo.PlacaRed= RED// Push array into obj

                var OPTICO = new Array();// Get OPTICOs
                $('#OpticoRow input').each(function (i) {                    
                    if ($(this).is('.idOPTICO')) {
                        var id = $(this).val();
                        var opticoo = {};
                        if (id != 0) {
                            opticoo.Id = id;
                        }
                        // else {
                        //     opticoo.Id = '';
                        // }
                        OPTICO.push(opticoo);
                    }
                })
                Equipo.DiscoOptico = OPTICO// Push array into obj

                var SOFTWAREE = new Array();// Get SOFTWAREs
                //$('#SoftwareRow input').each(function (i) {
                //    var id = $(this).val()
                //    var software = {};
                //    if ($(this).is('.idSOFTWARE')) {
                //        software.Id = id;
                //        SOFTWAREE.push(software);
                //    }
                //})
                Equipo.SoftAdicional = {}// Push array into obj

                if ($('#TarjetaSi').is(':checked')) {
                    Equipo.LectorTarjeta = true
                } else {
                    Equipo.LectorTarjeta = false
                }

                if ($('#MouseUSB').is(':checked')) {
                    Equipo.PuertoMouse = 'USB'
                } else {
                    Equipo.PuertoMouse = false
                }

                if ($('#TecladoUSB').is(':checked')) {
                    Equipo.PuertoTeclado = 'USB'
                } else {
                    Equipo.PuertoTeclado = false
                }
                break;
            case 'Impresora':
                if (window.State == 'nuevo') {
                    // If is Altas
                    // Determines the URL to send data
                    // URL for new item
                    Equipo.Url = pathServer + 'NuevoImpresora';
                } else {
                    // URL for update
                    Equipo.Url = pathServer + 'UpdateImpresora';
                }
                Equipo.Modelo = $('#Modelo').attr('data-id');
                break;
            case 'Monitor':
                if (window.State == 'nuevo') {
                    // If is Altas
                    // Determines the URL to send data
                    // URL for new item
                    Equipo.Url = pathServer + 'NuevoMonitor';
                } else {
                    // URL for update
                    Equipo.Url = pathServer + 'UpdateMonitor';
                    Equipo.idModelo = $('#Modelo').attr('data-id');
                }
                Equipo.Modelo = $('#Modelo').attr('data-id');                
                break;
            default:
                if (window.State == 'nuevo') {
                    // If is Altas
                    // Determines the URL to send data
                    // URL for new item
                    Equipo.Url = pathServer + 'NuevoVarios';
                } else {
                    // URL for update
                    Equipo.Url = pathServer + 'UpdateVarios';
                }
                Equipo.Modelo = $('#Modelo').val();
                
                break;
        }   // Send data
        // Ajax
        $.ajax({
            data: JSON.stringify(Equipo),
            url: Equipo.Url,
            contentType:'application/json',
            type: 'post',               
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Hide Loading
                NProgress.done();

                // create the notification
                var message, type;
                if (response['Info'] === 'ok') {
                    if (window.State == 'nuevo') {
                        message = 'Se agregó el nuevo equipo';
                    } else {
                        message = '<p>Se actualizaron los datos del equipo</p>';
                    }
                    type = 'success';
                    mostrarExito(message);
                    // Send data // Return db data  
                    initialState();
                } else {
                    message = '<p>' +/*response['Info']+'<br>'+*/response['Detail'] + '</p>';                    
                    mostrarError(message);
                }                    
            },
            error: function (response, textStatus) {
                // Hide Loading
                NProgress.done();
                mostrarError(response.Detail);
            }
        });
        // Ajax		    
    } else {
        // Show Acept buttom
        //$('#editData').trigger('click');
        var message = '<p>Complete los campos obligratorios o<br>revise que los datos ingresados sean correctos.</p>';
        mostrarError(message);
    }
} 