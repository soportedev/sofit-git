﻿$(function () {   
    $('#esParaEquipo').click(function () {
        var checked = $(this).is(':checked');
        var oblea = $(this).siblings('.obleaCont');
        if (checked) {
            oblea.show();
        } else {
            $('tbody', '#equiposSolucion').html('');
            $('#tObleaSolucion').val('');
            $('tr:not(:first)', oblea).remove();
            $(oblea).hide();
        }
    });    
    $('.grilla').on('click', '.anchorEquipo', function (e) {
        mostrarDatosEquipos(e);
    });
    $('.grilla').on('click', '.notificado', function (e) {
        mostrarNotificaciones(e);
    });
    $('body').on('click', '.verIncidente', function () {
        var nroInc = $(this).html();
        var myWindow = window.open('Historicos?nroInc=' + nroInc, '', 'height=600,width=800,menubar=no,toolbar=no,scrollbars=yes,status=no');
        myWindow.focus();
    });
    $('body').on('click', '.verInventario', function () {
        var nroOblea = $(this).attr('data-o');
        var myWindow = window.open('Inventario?nro=' + nroOblea, '', 'width=800,menubar=no,toolbar=no,scrollbars=yes,status=no,location=0');
        myWindow.focus();
    });    
    /***************
    *TRABAJOS DIALOG
    ****************/
    $('#dTipoResolucionClonado').on('change', function () {
        var sel = $('option:selected', this).text();
        var liImagen = $(this).parents('li').siblings('li:has(#dImagen)');
        if (sel.toLowerCase() == 'clonado') { liImagen.removeClass('hide') }
        else { liImagen.addClass('hide') }
    });
    $("#Trabajos").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        //position: ['center', 109],
        title: "Cargar Trabajo",
        width: '600px',
        resizeStop: function (ev, ui) {
            var originalSize = ui.originalSize;
            var size = ui.size;
            var textArea = $(ev.target).find('.tSolucion');
            var heightDif = originalSize.height - size.height;
            var widthDif = originalSize.width - size.width;
            textArea.height(textArea.height() - heightDif);
            //textArea.width(textArea.width() - widthDif);
        },
        open: function () {
            var thisdialog = $(this);
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            var theparent = $(thisdialog).parent();
            theparent.css('position', 'fixed');
            theparent.css('top', '0px');
            theparent.css('z-index', '99');            
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('input[type=text]', '.obleaEquipo').val(datos.oblea);
            $('label.idInc', '.obleaEquipo').text(datos.idEquipo);            
        },
        buttons: {
            Aceptar: {
                id: 'bTrabajos',
                text: 'Aceptar',
                click: function () {
                    var fData = new FormData();
                    var datos = $(this).data('datos');
                    var tresolucion = false;
                    var idTipoResolucion;
                    switch (datos.nombreGrilla) {
                        case "divEnClonado": idTipoResolucion = $('#dTipoResolucionClonado option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            fData.append('idEquipo', datos.idEquipo);
                            break;
                        case "divEnLaboratorio": idTipoResolucion = $('#dTipoResolucionLaboratorio option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            fData.append('idEquipo', datos.idEquipo);
                            break;
                        case "divEntregar": idTipoResolucion = $('#dTipoResolucionLogistica option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            fData.append('idEquipo', datos.idEquipo);
                            break;
                        case "divEnAtUsuarios": idTipoResolucion = $('#dTipoResolucionAtUsuarios option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            break;
                        case "divIngresados": idTipoResolucion = $('#dTipoResolucionAtUsuarios option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            break;
                        case "divLogistica": idTipoResolucion = $('#dTipoResolucionAtUsuarios option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            break;
                    }
                    if (tresolucion) {
                        $('#Trabajos').find('.message').html('Seleccione el tipo de Resolución.');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    var obs = $('#observaciones');
                    if ($.trim(obs.val()).length < 10) {
                        $('#Trabajos').find('.message').html('Complete el campo de Observaciones');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    if ($('#lOblea').hasClass('equipoIncompleto')) {
                        $('#Trabajos').find('.message').html('El equipo está incompleto...')
                        return;
                    }
                    datos.idTecnico = $('#idTecnico').val();
                    datos.idTurno = $('#idTurno').val();
                    datos.observaciones = $('#observaciones').val();
                    datos.idTipoResolucion = idTipoResolucion;                    
                    var inpFile = $('#upFileRes');
                    var img = inpFile.get(0).files[0];
                    //set url y actualizaciones
                    switch (datos.nombreGrilla) {
                        case "divEnClonado": var esSuspendido = $('#dTipoResolucionClonado option:selected').text();
                            if (esSuspendido == 'SUSPENDIDO') {
                                datos.actualizacion = 'suspendidosAClon';
                            } else datos.actualizacion = 'none';
                            datos.url = "Incidentes/cargarSolucionesEquipo";
                            break;
                        case "divEnLaboratorio": var esSuspendido = $('#dTipoResolucionLaboratorio option:selected').text();
                            if (esSuspendido == 'SUSPENDIDO') {
                                datos.actualizacion = 'suspendidosALab';
                            } else datos.actualizacion = 'none';
                            datos.url = "Incidentes/cargarSolucionesEquipo";
                            break;
                        case "divEntregar": var tResolucion = $('#dTipoResolucionLogistica option:selected').text();
                            if (tResolucion == 'Aviso a Garantía') {
                                datos.actualizacion = 'entregarAGarantia';
                            } else {
                                if (tResolucion == 'En Garantía') {
                                    datos.actualizacion = 'entregarEnGarantia';
                                } else {
                                    datos.actualizacion = 'none';
                                }
                            }
                            datos.url = "Incidentes/cargarSolucionesEquipo";
                            break;                        
                        default: datos.url = "Incidentes/cargarSoluciones";
                            datos.actualizacion = "none";
                            break;
                    }                   
                    fData.append('fileupload', img);
                    fData.append('idTecnico', datos.idTecnico);
                    fData.append('idTurno', datos.idTurno);
                    fData.append('observaciones', datos.observaciones);
                    fData.append('idTipoResolucion', datos.idTipoResolucion);                    
                    fData.append('idIncidente', datos.idIncidente);
                    if ($('#dImagen').is(':visible')) fData.append('idImagen', $('#dImagen').val());
                    if ($('#esParaEquipo').is(':visible')) {
                        var valorCheck = $('#esParaEquipo').is(':checked');
                        fData.append('esParaEquipo', valorCheck);
                        if ($('#esParaEquipo').is(':checked')) {
                            var els = $('#Trabajos').find('.idEquipo');
                            var sufix = 0;
                            $.each(els, function () {
                                fData.append('idEquipo.' + sufix, $(this).html());
                                sufix++;
                            });
                        }
                    } else {
                        fData.append('esParaEquipo', false);
                    }
                    fData.append('sobrescribir', $('#sobrescribir').is(':checked'));                    
                    if (ajaxReady) {
                        $.ajax({
                            url: datos.url,
                            data: fData,
                            type: 'POST',
                            processData: false,
                            contentType: false,                            
                            beforeSend: function () {
                                NProgress.start();
                                ajaxReady = false;
                            },
                            success: function (data) {
                                ajaxReady = true;
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    $("#Trabajos").dialog('close').find('#observaciones').val('');
                                    if (datos.actualizacion != 'none') actualizarPaneles(datos.actualizacion);
                                    mostrarExito('Se ha cargado la solucion');
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                NProgress.done();
                            }
                        });
                    }
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    
    /************************
    *TRABAJOS ALT DIALOG
    *************************/
    $("#TrabajosAlt").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);            
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('input[type=text]', '.obleaEquipo').val(datos.oblea);
            $('label.idInc', '.obleaEquipo').text(datos.idEquipo);            
        },
        buttons: {
            Aceptar: {                
                text: 'Aceptar',
                click: function () {
                    var datos = $(this).data('datos');
                    var tresolucion = false;
                    var idTipoResolucion;
                    switch (datos.nombreGrilla) {
                        case "divSuspendidos": idTipoResolucion = $('#dTipoResolucionSuspendido option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            break;
                        case "divParaGarantia": idTipoResolucion = $('#dTipoResolucionParaGarantia option:selected').val();
                            tresolucion = idTipoResolucion == 0;
                            var desResolucion = $('#dTipoResolucionParaGarantia option:selected').text();
                            if (desResolucion == 'En Garantía') {
                                datos.actualizacion = 'aGarantiaEnGarantia';
                            }                            
                            break;
                        case "divEnGarantia": idTipoResolucion = $('#dTipoResolucionEnGarantia option:selected').val();
                            tresolucion = idTipoResolucion == 0;                            
                    }                    
                    tresolucion = idTipoResolucion == 0;                   
                    if (tresolucion) {
                        $('#TrabajosAlt').find('.message').html('Seleccione el tipo de Resolución.');
                        $('.error, #TrabajosAlt').show();
                        return;
                    }
                    var obs = $('.observaciones','#TrabajosAlt');
                    if ($.trim(obs.val()).length < 10) {
                        $('#TrabajosAlt').find('.message').html('Complete el campo de Observaciones');
                        $('.error, #TrabajosAlt').show();
                        return;
                    }                    
                    datos.observaciones = $(obs).val();
                    datos.idTecnico = $('.dTecnicos','#TrabajosAlt').val();
                    datos.idTurno = $('.dTurnos','#TrabajosAlt').val();
                    datos.idTipoResolucion = idTipoResolucion;                    
                    if (ajaxReady) {
                        $.ajax({
                            url: 'Incidentes/cargarSolucionesEquipo',
                            data: datos,
                            type: 'POST',
                            datatype: 'json',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                ajaxReady = true;
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    $("#TrabajosAlt").dialog('close');
                                    if (datos.actualizacion != 'none') actualizarPaneles(datos.actualizacion);
                                    mostrarExito('Se ha cargado la solucion');
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                $(".ajaxGif").css({ 'display': 'none' });
                            }
                        });
                    }
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    /*************************
    *TRABAJOS ALT HANDLER
    **************************/
    $('.trabajosAlt', '#DivEqParaGar,#DivEqEnGar,#DivEqSus').on('click', function () {
        var nro = [];
        var trs = {};
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 || nro.length > 1) {
            mostrarError("Seleccione UN elemento");
        }
        else {
            var $trabajosDialog = $("#TrabajosAlt");
            clearTrabajosPanel($trabajosDialog);
            var datos = nro[0].find('td').slice(1, 7);
            var conforms = true;
            var oblea, idIncidente, nroIncidente, idEquipo;
            switch (nombreGrilla) {
                case "divParaGarantia": $trabajosDialog.find('li:not(.paraGar)').addClass('hide');                    
                    break;
                case "divEnGarantia": $trabajosDialog.find('li:not(.enGar)').addClass('hide');                   
                    break;
                case "divSuspendidos": $trabajosDialog.find('li:not(.susp)').addClass('hide');
                    break;
            }
            oblea = $(datos[2]).text();
            idEquipo = datos[1].innerHTML;
            nroIncidente = datos[3].innerText;
            idIncidente = datos[0].innerHTML;
            var datos = {};
            datos.idIncidente = idIncidente;
            datos.nroIncidente = nroIncidente;
            datos.idEquipo = idEquipo;
            datos.oblea = oblea;
            datos.nombreGrilla = nombreGrilla;
            $trabajosDialog.data('datos', datos).dialog('open');
            $('#TrabajosGarantia').dialog('open');
        }
    });
    //-------------------------------
    //Ubicacion
    //--------------------------------
    $('#setUbicacion').on('click', function () {
        var nro = [];
        var trs = {};
        var ids = [];
        var obleas = [];
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0 ) {
            mostrarError("Seleccione algún equipo");
            return;
        }
        else {
            for (var i = 0; i < nro.length; i++) {
                var datos = nro[i].find('td').slice(1, 7);
                ids.push($(datos[1]).text());
                obleas.push($(datos[2]).text());
            }
            var datos = nro[0].find('td').slice(1, 7);
            trs.tipo = 'equipo';
            trs.tabla = nombreGrilla;
            trs.idEquipo = ids;
            trs.idIncidente = $(datos[0]).text();
            trs.oblea = obleas;
            trs.nroIncidente = $(datos[3]).text();
            trs.tecnico = $(datos[4]).text();
            trs.dependencia = $(datos[6]).text();
        }
        $("#Ubicacion").data('datos', trs).dialog('open').find('.error').hide();
    });
    $("#Ubicacion").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 109],
        title: "Ubicar equipos",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '109px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('textarea','.obleas').val(datos.oblea);            
        },
        buttons: {
            Aceptar: function () {
                var datos = $(this).data('datos');
                var ids = datos.idEquipo;
                var idA = $('.armarios option:selected',this).val();
                if (idA == 0) { mostrarError('Seleccione un armario.'); return; }               
                datos.idArmario = idA;
                if (ajaxReady) {
                    $.ajax({
                        url: "Incidentes/EstablecerUbicacion",
                        data: 'idEquipo='+datos.idEquipo+'&idArmario='+idA,
                        type: 'POST',
                        datatype:'json',
                        beforeSend: function () {
                            ajaxReady = false;
                        },
                        success: function (data) {
                            ajaxReady = true;
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                actualizarPaneles(datos.actualizacion="suspendidos");
                                $("#Ubicacion").dialog('close').find('#observaciones').val('');
                                mostrarExito('Se ha establecido la ubicacion');
                            }
                            else {
                                $("#Ubicacion").dialog('close');
                                mostrarError(data.Detail);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                           mostrarError(thrownError);
                        },
                        complete: function () {
                            ajaxReady = true;
                            $(".ajaxGif").css({ 'display': 'none' });
                        }
                    });
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    /************
    *Notificaciones
    ************/
    $('#referentesContainer').on('click','span.trash', function () {
        $(this).parents('.mailAdd').remove();
    });
    $('#inputDestinatarios').autocomplete({
        minLength: "2",
        delay: 800,
        source: (function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '/Service/getAddBook',
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    var suggestions = [];
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.email;
                        obj.label = val.Nombre;
                        suggestions.push(obj);
                    })
                    response(suggestions);
                }
            });
        }),
        select: function (e, ui) {
            addBlock(ui);
            $(this).val("");
            return false;
        }        
    }).on('keyup', function (e) {
        if (e.keyCode == 13) {
            var ev = {};
            ev.item = new Array();
            ev.item.label = $(this).val();
            ev.item.value = $(this).val();
            addBlock(ev);
            $(this).val("");
        }
    });
    function addBlock(valor) {
        var divDest = $('#referentesContainer');
        var inputDest = $('input[type="text"]', divDest);
        var spanId = $('<span>').attr('class', 'hide');
        var spanNombre = $('<span>').attr('class', 'nomJur');
        var spanTrash = $('<span>').attr('class', 'icon trash');
        var div = $('<div>').attr('class', 'mailAdd');
        spanId.html(valor.item.value);
        spanNombre.html(valor.item.label);
        div.append(spanId).append(spanNombre).append(spanTrash);
        div.insertBefore(inputDest);
        var widthspnom = spanNombre.width();
        var widthsptr = spanTrash.width();
        var total = widthspnom + widthsptr+1;
        div.css('width', total++);
    }
    $("#Notificacion").dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: false,
        draggable: false,
        //position: ['center', 105],
        title: "Enviar Aviso",
        width: 800,
        open: function () {
            var thisdialog = $(this);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '101');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '100');
            var datos = $(this).data('datos');
            var strData = 'Estimados:\nPara el equipo: ' + datos.oblea + ' , Marca: ' + datos.Marca + ', Modelo: ' + datos.Modelo +
                ', N.Serie: ' + datos.NroSerie + ', con Incidente: ' + datos.nroIncidente + ', se encontraron las siguientes observaciones:\n';
            if (datos.resoluciones) {
                $.each(datos.resoluciones, function (i, v) {
                    strData += '-' + v.Observ + "\n Diagnosticado por: " + v.Tecnico + '\n';
                });

                strData += 'Saludos.\n';
                strData +=(datos.Firma == null) ? '' : datos.Firma;
                $('#asuntoNotificacion').val('Atención-Pedido de Repuestos para ' + datos.oblea + ' del incidente ' + datos.nroIncidente);

                $('#cuerpoMail').val(strData);
                var hei = $('#cuerpoMail')[0].scrollHeight;
                $('#cuerpoMail').css('height', hei);
            }
            $('#Notificacion').find('.error').hide();
            var divDest = $('#referentesContainer');
            $('.mailAdd', divDest).remove();
            var inputDest = $('input[type="text"]', divDest);
            if (datos.referentes) {
                $.each(datos.referentes, function (i, v) {
                    var spanId = $('<span>').attr('class', 'hide');
                    var spanNombre = $('<span>').attr('class', 'nomJur');
                    var spanTrash = $('<span>').attr('class', 'icon trash');
                    var div = $('<div>').attr('class', 'mailAdd');
                    spanId.html(v.id);
                    spanNombre.html(v.nombre);
                    div.append(spanId).append(spanNombre).append(spanTrash);
                    div.insertBefore(inputDest);
                    var widthspnom = spanNombre.width();
                    var widthsptr = spanTrash.width();
                    var total = widthspnom + widthsptr + 1;
                    div.css('width', total);
                });
            }
            //chars
            var spanCounter = $(thisdialog).find('p.charCounter');
            var txtarea = $(thisdialog).find('.tSolucion');
            var str = txtarea[0].value;
            var taLength = 0;
            var lns = str.match(/[^\r]\n/g);
            if (lns) {
                var onlystr = str.length - lns.length;
                taLength = onlystr + lns.length * 2;
            }
            else
                taLength = str.length;
            if (taLength > 3999) {
                spanCounter.addClass('error2').html('Chars:' + taLength + '. El mail se envía completo, pero en la BD se registran los primeros 4000 chars');
            } else spanCounter.removeClass('error2').html('Chars:' + taLength);
        },
        buttons: {
            Aceptar: function () {
                var panel = $('#referentesContainer');
                var datos = $(this).data('datos');
                datos.subject = $('#asuntoNotificacion').val();
                datos.body = '';
                var body = $('#cuerpoMail').val();
                var destinatarios = $('div.mailAdd>span.hide', '#referentesContainer');
                var jsonDest = [];
                if (destinatarios.length == 0) {
                    mostrarError("No hay referentes para enviar el mail!");
                    return;
                }
                destinatarios.each(function (i, v) {
                    jsonDest.push($(v).html());
                });
                datos.referentes = 'ids=' + jsonDest;
                //var lines = body.replace(/\n/, ',');
                datos.body = body;
                $.ajax({
                    url: "Incidentes/notificarEquipos",
                    data: datos,
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        mostrarInfo("Se esta enviando la Notificación");
                    },
                    success: function (data) {
                        switch (data.Info) {
                            case "Login": mostrarFinSesion();
                                return;
                            case "ok": mostrarExito('Notificacion Exitosa');
                                $("#Notificacion").dialog('close');
                                actualizarPaneles("suspendidos");
                                break;
                            case "algunos errores": mostrarExito("Hubo errores enviando la notificación a: " + data.Detail +
                            ", pero los otros se enviaron bien. Verifique la dirección de email e intente manualmente.");
                                $("#Notificacion").dialog('close');
                                actualizarPaneles("suspendidos")
                                break;
                            default: mostrarError(data.Detail)
                                break;
                        }                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        mostrarError(thrownError);
                    },
                    complete: function () {
                        $("#infoInfo").remove();
                    }
                });
            }
            ,
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    //-->Fin notificaciones   
    /*****************
    VER LOS EQUIPOS
    ******************/
    $(".window").on("click",".verEquipo", function(event) {
        if (ajaxReady) {
            var nroInc = $(this).closest("tr").find("td").eq(2).find('span').html();
            var datos = "incidente=" + nroInc;
            $.ajax({
                url: "Incidentes/getEquiposDeIncidente",
                data: datos,
                type: 'POST',
                beforeSend: function() {
                    ajaxReady = false;
                },
                success: function (data) {
                    var html = data;
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.length == 0) {
                        html = "No se encontraron equipos";
                    }
                    else {
                        html = "<table><tr><th>Oblea</th><th>Area</th><th>Estado</th><th>Técnico</th></tr>";
                        $.each(data, function (i,v) {                            
                            html += "<tr>";
                            html += "<td>" + v.Oblea + "</td>";
                            html += "<td>" + v.Area + "</td>";
                            html += "<td>" + v.Estado + "</td>";
                            html += "<td>" + v.Tecnico + "</td>";
                            html += "</tr>";
                        });
                        html += "</table>";
                    }
                    var div = $('<div class="info">').append(html).dialog({
                        show: 'slide',
                        position: { my: "right top", at: "right bottom", of: event.target },
                        width: 'auto'
                    }).click(function () { $(this).dialog('destroy') });
                    $(".ui-dialog-titlebar", $(div).parent()).hide();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                   mostrarError(thrownError);
                },
                complete: function() {
                    ajaxReady = true;
                }
            }); /*fin ajax*/
        } //fin ajaxready
    });
    /***************
    SOLUCION EQUIPOS
    ****************/
    $(".window").on("click",".verSolucion", function(event) {
        var idInc = $.trim($(this).closest("tr").find("td").eq(1).html());
        var idEq = $.trim($(this).closest("tr").find("td").eq(2).html());
        var datos = "idIncidente=" + idInc + "&idEquipo=" + idEq;   

        $.ajax({
            url: "Incidentes/buscarResolucionesEquipo",
            data: datos,
            type: 'POST',
            beforeSend: function() {
                ajaxReady = false;
            },
            success: function(data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }                
                var tabla = $('<table>');
                if (data.length == 0) {
                    tabla = "<p>No se encontraron soluciones.</p>";
                } else {
                    var thead = $('<thead>');
                    var tbody = $('<tbody>');
                    var tr = $('<tr>');
                    var thF = $('<th>');
                    var thTS = $('<th>');
                    var thOb = $('<th>');
                    var thT = $('<th>');
                    var thFile = $('<th>');
                    thF.html('Fecha');
                    thTS.html('Tipo Sol.');
                    thOb.html('Observ.');
                    thT.html('Técnico');
                    thFile.html('Archivo');
                    tr.append(thF);
                    tr.append(thTS);
                    tr.append(thOb);
                    tr.append(thT);
                    tr.append(thFile);
                    thead.append(tr);
                    tabla.append(thead).append(tbody);
                    $.each(data, function (i, v) {
                        var trX = $('<tr>');
                        var tdF = $('<td>');
                        var tdTS = $('<td>');
                        var tdOb = $('<td>');
                        var tdT = $('<td>');
                        tdF.html(v.FechaString);
                        tdTS.html(v.TipoRes);
                        tdOb.html(v.Observ);
                        tdT.html(v.Tecnico);
                        trX.append(tdF).append(tdTS).append(tdOb).append(tdT);
                        var tdFile = $('<td>');
                        if (v.Archivo != "") {
                            tdFile.append('<a target="_blank" rel="noopener noreferrer" href="/Content/img/Resoluciones/' + v.Archivo + '"><i class="fa fa-file cursor" data-path="' + v.Archivo + '"></a>')
                        } else {
                            tdFile.append('<i class="fa fa-eye-slash">');
                        }
                        trX.append(tdFile);
                        tbody.append(trX)
                    });
                }
                var div = $('<div class="info"><span class="fa fa-close topright cursor"></span>').append(tabla).dialog({
                    show: 'slide',
                    position: { my: "right top", at: "right bottom", of: event.target },
                    width: 'auto'
                })
                $(".ui-dialog-titlebar",$(div).parent()).hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
               mostrarError(thrownError);
            },
            complete: function() {
                ajaxReady = true;
            }
        }); /*fin ajax*/
    });
    
    //Busqueda autocomplete equipo valido en agregar equipos
    $("#tOblea").click(function(event, ui) { $('#bNvoEquipo').button('disable') }).autocomplete({
        minLength: "2",
        source: (
        function(request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Service/EquipoXOblea",
                data: term,
                dataType: "json",
                success: function(data) {                    
                    if (data.Info == 'Login') mostrarFinSesion();
                    else {
                        var suggestions = [];
                        $.each(data, function(i, val) {
                            var obj = {};
                            obj.value = val.idEquipo;
                            obj.label = val.Oblea;
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    }
                }
            });
        }),
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        focus: function (e, ui) { $(this).val(ui.item.label); return false; },
        select: function(e, ui) {
            $('#bNvoEquipo').button("enable").focus();
            $('#dAreaAddEquipo').val(0).data('idEquipo', ui.item.value);
            $('#tecnicoAddEquipo').val(0);
            $(e.target).val(ui.item.label);
            return false;
        }
    });
    //para buscar el ultimo tecnico
    $('#dAreaAddEquipo').on('change', function () {
        var valu = $('option:selected', this).val();
        if (valu != 0) {
            var idEquipo = $(this).data('idEquipo');
            $.ajax({
                type: "POST",
                url: "Incidentes/UltimoTecnico",
                data: { idEquipo: idEquipo,idArea:valu },
                dataType: "json",
                success: function (data) {
                    if (data.Info == 'Login') mostrarFinSesion();
                    if (data.Info != 'No') {
                        var divdialog = $('<div>').append("El último técnico que vio este equipo es: "+data.Info+
                            ", el "+data.Detail+". Quiere asignarlo a ese técnico?").dialog({
                            width: '250px',
                            show: 'slide',
                            //position: 'center',
                            open: function () {
                                $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                            },
                            buttons: {
                                Aceptar: function () {
                                    setDropText(data.Info, $('#tecnicoAddEquipo'))
                                    $(divdialog).dialog('destroy');
                                },
                                Cancelar: function () {
                                    setDrop(0, $('#tecnicoAddEquipo'));
                                    $(this).dialog('destroy');
                                }
                            }

                        });
                    } else setDrop(0, $('#tecnicoAddEquipo'));
                }
            });
        }
    });
    $("#tObleaSolucion").autocomplete({
        minLength: "2",
        source: (
        function (request, response) {
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Inventario/EquipoXOblea",
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    else {
                        var suggestions = [];
                        $.each(data, function (i, val) {
                            var obj = {};
                            obj.value = val.idEquipo;
                            obj.label = val.Oblea;
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    }
                }                
            });
        }),
        open: function () { $('.ui-autocomplete').css('font-size', '.8em') },
        focus: function (e, ui) { $(this).val(ui.item.label); return false; },
        select: function (e, ui) {
            var spanId = $('<span class="hide idEquipo">').html(ui.item.value);            
            $(this).val('');
            $(this).focus();            
            var tdId = $('<td class="idEquipo hide">').html(ui.item.value);
            var td = $('<td class="oblea">').html(ui.item.label);
            var tr = $('<tr>').append(td).append(tdId).append('<td><div class="icon drop"></td>');
            $('tbody', '#equiposSolucion').append(tr);
            $('#bTrabajos').button("enable");
            return false;
        }
    });    
    //BUSCAR TECNICOS POR AREA
    $("#dAreas,#dAreas1,#dAreas2").bind('change keypress', function(event) {
        $(this).parent().next().find('select').load(
        soportePath + 'Models/handlers/buscarTecnicosXArea.ashx',
        { idArea: $(event.target).val() },
        function() { $('[value=""]', event.target).remove(); }
        );
    });
    $(".remito").click(function (event) {
        var nro = [];
        var trs = {};
        var ids = [];
        var obleas = [];
        var grilla = $(this).parents('.botones').siblings('.grilla');
        var nombreGrilla = grilla.attr('id');
        grilla.find('tr').has('td :checked').each(function (i) {
            var val = $(this);
            nro.push(val);
        });
        if (nro.length == 0) {
            mostrarError("Seleccione algún equipo. Hasta un máximo de 20");
            return;
        }
        else if (nro.length > 20) {
            mostrarError("Selecciones menos de 20 equipos")
        }
        else {
            for (var i = 0; i < nro.length; i++) {
                var datos = nro[i].find('td').slice(1, 7);
                ids.push($(datos[1]).text());
            }
        }
        var urlvalues = '';
        for (var i = 0; i < ids.length; i++) {
            if (i === 0) {
                urlvalues = 'id=' + ids[i];
            }
            else {
                urlvalues += '&id=' + ids[i];
            }
        }
        //window.open('/Remito/Index' + urlvalues, '_blank', 'height=600,width=930,menubar=no,toolbar=no,scrollbars=yes,status=no');
        window.open('/Remito/Index?' + urlvalues, '_blank');
    });
                      
});                           
                            /*****************
                            FIN DOCUMENT READY
                            *****************/
