window.onload = initPage;
var welcomePaneShowing = true;
var selectedTab;
var obleaValid;
var nombreValid=false;
var nArchivoValid = false;
var fechaValid = false;
var obsComplete = false;

function initPage() {
    $('#HyperLink1').button();
    $('#register').click('registrarTrabajo');
    var tabs =
    document.getElementById("tabs").getElementsByTagName("a");
    for (var i = 0; i < tabs.length; i++) {
        var currentTab = tabs[i];
        currentTab.onmouseover = showHint;
        currentTab.onmouseout = hideHint;
        currentTab.onclick = showTab;
    }    
}
function showHint() {
  if (!welcomePaneShowing) {
    return;
  }
  switch (this.title) {
    case "Trabajos":
      var hintText = "Registrar un trabajo";
      break;
    case "Imagenes":
      var hintText = "Ver las imagenes disponibles";
      break;        
  }  
  var contentPane = document.getElementById("content");
  contentPane.innerHTML = "<h3>" + hintText + "</h3>";
}
function hideHint() {
  if (welcomePaneShowing) {
    var contentPane = document.getElementById("content");
    contentPane.innerHTML = 
      "<h3>Seleccione una solapa</h3>";
  }
}
function showTab() {
  selectedTab = this.title;
  if (selectedTab == "") {
    welcomePaneShowing = true;
    document.getElementById("content").innerHTML = 
      "<h3>Seleccione una solapa</h3>";
  } else {
    welcomePaneShowing = false;
  }

  var tabs = document.getElementById("tabs").getElementsByTagName("a");
  for (var i=0; i<tabs.length; i++) { 
    var currentTab = tabs[i];
    if (currentTab.title == selectedTab) {
      currentTab.className = 'active';
    } else {
      currentTab.className = 'inactive';
    }
  }
  var url;
  if (selectedTab == "Estadistica") {      
      url = selectedTab + ".aspx";      
  }
  else url = selectedTab + ".aspx";
  var request = createRequest();
  if (request == null) {
    alert("Unable to create request");
    return;
  }
  request.onreadystatechange = showSchedule;
  request.open("GET", url, true);
  request.send(null);
}

function showSchedule() {
  if (request.readyState == 4) {
    if (request.status == 200) {
        document.getElementById("content").innerHTML = request.responseText;
        initTrabajos();
     }
  }
}
function initTrabajos() {

    if (selectedTab == "Trabajos") {
        $(":button").button();
        document.getElementById("register").disabled = true;
        document.getElementById("tOblea").onchange = verificarOblea;
        document.getElementById("register").onclick = registrarTrabajo;
        document.getElementById("tOblea").focus();
        var fecha = new Date();
        var hours = fecha.getHours();
        var turno = hours >= 14 ? 'Tarde' : 'Ma�ana';
        var tecnico = $("#user").html();       
        $("#dTecnico option:selected").removeAttr("selected");
        var prueba = $("#dTecnico option:contains(tecnico)");
        $("#dTecnico option").each(function() {
            if ($(this).text() == tecnico) $(this).attr('selected', 'selected');
        });
        $("#dTurno option").each(function() {
            if ($(this).text() == turno) $(this).attr('selected', 'selected');
        });           
    }
    if (selectedTab == "Imagenes") {
        $(":button").button();
        document.getElementById("hAgregar").disabled = true;
        document.getElementById("hFiltrar").disabled = true;
        document.getElementById("tNombre").onblur = validarNombre;
        document.getElementById("tNArchivo").onblur = validarArchivo;
        document.getElementById("tFecha").onblur=validarFecha;
        document.getElementById("cEstandar").onclick = toogleObserv;
        document.getElementById("tObservaciones").onkeypress = toogleObserv;
        document.getElementById("tObservaciones").onblur = toogleObserv;
    }
    if (selectedTab == "Estadistica") {
        cargarFiltros();            
    }
}

function filtrar() {
    var group = document.getElementsByName("radioGroup");
    for (var i = 0; i < group.length; i++) {
        var current = group[i];
        var checked = current.checked;
        var value = current.value;
        if (checked) {
            switch (value) {
                case ("B"): filtrarFecha();
                    break;
                case ("A"): filtrarTecnico();
                    break;
                case ("C"): filtrarJurisd();
                    break;
            }
        }        
    }
}
function filtrarFecha() {
    var fDesde = document.getElementById("fecha_desde").value;
    var fHasta = document.getElementById("fecha_hasta").value;
    url = selectedTab + ".aspx?param=fecha&fDesde="+fDesde+"&fHasta="+fHasta;
    var request = createRequest();
    if (request == null) {
        alert("Unable to create request");
        return;
    }
    request.onreadystatechange = showSchedule;
    request.open("GET", url, true);
    request.send(null);
}
function filtrarTecnico() {
    url = selectedTab + ".aspx?param=tecnico";
    var request = createRequest();
    if (request == null) {
        alert("Unable to create request");
        return;
    }
    request.onreadystatechange = showSchedule;
    request.open("GET", url, true);
    request.send(null);
}
function filtrarJurisd() {
    url = selectedTab + ".aspx?param=jurisdiccion";
    var request = createRequest();
    if (request == null) {
        alert("Unable to create request");
        return;
    }
    request.onreadystatechange = showSchedule;
    request.open("GET", url, true);
    request.send(null);
}
function toogleFecha() {
    var group = document.getElementsByName("radioGroup");
    for (var i = 0; i < group.length; i++) {
        var current = group[i];
        if (current.value == "B" && current.checked) {
            document.getElementById("fecha_desde").style.visibility = "visible";
            document.getElementById("fecha_hasta").style.visibility = "visible";
        }
        else {
            if (current.value != "B"&&current.checked) {
                document.getElementById("fecha_desde").style.visibility = "hidden";
                document.getElementById("fecha_hasta").style.visibility = "hidden";
            }
        }
    }
}
function toogleObserv() {

    var check = document.getElementById("cEstandar");
    if (check.checked) {
        document.getElementById("tObservaciones").disabled = true;
        obsComplete = true;
    }
    else {
        document.getElementById("tObservaciones").disabled = false;
        if (validarNoVacio(document.getElementById("tObservaciones"))) obsComplete = true;
        else obsComplete = false;

    }
    checkFormStatus2();
}
function validarNombre() {
    var inputField = document.getElementById("tNombre");
    if (!validarNoVacio(inputField)) {
        inputField.className = "error2";
        nombreValid = false;
    }
    else {
        inputField.className = "input2";
        nombreValid = true;
    }
    checkFormStatus2();
}
function validarArchivo() {
    var inputField = document.getElementById("tNArchivo");
    if (!validarNoVacio(inputField)) {
        inputField.className = "error2";
        nArchivoValid = false;
    }
    else {
        inputField.className = "input2";
        nArchivoValid = true;
    }
    checkFormStatus2();    
}
function validarFecha() {
    var inputField = document.getElementById("tFecha");
    var helpText = document.getElementById("tFecha_help");
    if (!validarNoVacio(inputField)) {
        inputField.className = "error2";
        fechaValid = false;
    }
    if (!regularExpression(/^\d{2}\/\d{2}\/\d{4}$/, helpText, "El formato es 'dd/mm/aaaa'")) {
        inputField.className = "error3";
        fechaValid = false;
    }
    else {
        inputField.className = "input2";
        fechaValid = true;
    }
    checkFormStatus2();

}
function regularExpression(regex, helpText, helpMessage) {
    var inputField = document.getElementById("tFecha");
        if (!regex.test(inputField.value)) {

            if (helpText != null)
                helpText.innerHTML = helpMessage;
            return false;
        }
        else {
            if (helpText != null)
                helpText.innerHTML = "";
            return true;
        }

    }
function agregarImagen() {
    var nombre = document.getElementById("tNombre").value;
    var archivo = document.getElementById("tNArchivo").value;
    var observaciones = document.getElementById("tObservaciones").value;
    var fecha = document.getElementById("tFecha").value;
    var indexOs = document.getElementById("dOs").selectedIndex;
    var os = document.getElementById("dOs").options[indexOs].value;
    var indexChipset = document.getElementById("dChipset").selectedIndex;
    var chipset = document.getElementById("dChipset").options[indexChipset].value;
    var indexDominio = document.getElementById("dDominio").selectedIndex;
    var dominio = document.getElementById("dDominio").options[indexDominio].value;
    var estandar;
    if (document.getElementById("cEstandar").checked) estandar = 1;
    else estandar = -1;
    request = createRequest();
    if (request == null)
        alert("Unable to create request");
    else {
        var requestData = "nombre=" + nombre + "&archivo=" + archivo + "&observaciones=" +
        observaciones + "&fecha=" + fecha + "&os=" + os + "&chipset=" +
        chipset + "&dominio=" + dominio + "&estandar=" + estandar;

        var url = "addImagen.ashx";
        
        request.onreadystatechange = updateGrid;
        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.send(requestData);        
    }
}
function updateGrid() {
    if (request.readyState == 4) {
        if (request.status == 200) {
            var resp = request.responseText;
            if (resp == "ok") {
                selectedTab = "Imagenes";
                showTab2();
            }
            else document.getElementById("content").innerHTML = "<h3>Hubo un error</h3>";
        }
  }

}
function showTab2() {
  var tabs = document.getElementById("tabs").getElementsByTagName("a");
  for (var i=0; i<tabs.length; i++) { 
    var currentTab = tabs[i];
    if (currentTab.title == selectedTab) {
      currentTab.className = 'active';
    } else {
      currentTab.className = 'inactive';
    }
  }

  var request = createRequest();
  if (request == null) {
    alert("Unable to create request");
    return;
  }
  request.onreadystatechange = showGrid;
  request.open("GET", selectedTab + ".aspx", true);
  request.send(null);
}
function showGrid() {
    if (request.readyState == 4) {
        if (request.status == 200) {
            
            document.getElementById("content").innerHTML = request.responseText;
            
        }
    }
}    

function buttonOver() {
  this.className = "active";
}
function buttonOut() {
  this.className = "";
}
function registrarTrabajo() {
    var datos = $('li').serializeAnything();
    $.ajax({
        url: "../negocio/handlers/cargarResolucionClonado.ashx",
        data: datos,
        type: 'POST',
        success: function(data) {
            switch (data) {
                case "out": mostrarFinSesion();
                    break;
                case "ok": mostrarExito('Se ha cargado la Resolucion');
                    $("#tOblea").val("").removeClass('approved').addClass('input');
                    $("#tSolucion").val("");
                    obleaValid = false;
                    checkFormStatus();
                    break;
                default: mostrarError(data);
                    break;
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        },
        complete: function() {
        $('.ajaxGif').css({ 'display': 'none' });
        }
    });
}
function verificarOblea() {
    document.getElementById("tOblea").className = "thinking";
    obleaRequest = createRequest();
    if (obleaRequest == null)
        alert("Unable to create request");
    else {
        var theOblea = document.getElementById("tOblea").value;
        var oblea = escape(theOblea);

        var url = "../negocio/ajax.ashx?oblea=" + oblea;
        obleaRequest.onreadystatechange = showObleaStatus;
        obleaRequest.open("GET", url, true);
        obleaRequest.send(null);
    }
}
function showObleaStatus() {
    if (obleaRequest.readyState == 4) {
        if (obleaRequest.status == 200) {
            if (obleaRequest.responseText == "ok") {
                document.getElementById("tOblea").className = "approved";
                document.getElementById("hiddenTip").className = "hidden";
                document.getElementById("hiddenTip2").className = "hidden";
                obleaValid = true;
            } else {
                if (obleaRequest.responseText == "inc") {
                    document.getElementById("tOblea").className = "denied";
                    document.getElementById("hiddenTip2").className = "tip";
                    document.getElementById("hiddenTip2").innerHTML = "El equipo esta incompleto...<a href='abm1.aspx?eq=" + 
                    document.getElementById("tOblea").value + "'>Completarlo...</a>";
                    document.getElementById("hiddenTip").className = "hidden";
//                    document.getElementById("tOblea").focus();
//                    document.getElementById("tOblea").select();
                    obleaValid = false;
                } else {
                    document.getElementById("tOblea").className = "denied";
                    document.getElementById("hiddenTip").className = "tip";
                    document.getElementById("hiddenTip2").className = "hidden";
                    document.getElementById("tOblea").focus();
                    document.getElementById("tOblea").select();
                    obleaValid = false;
                }
            }                                   
            checkFormStatus();
        }
    }
}
function checkFormStatus() {

    if (obleaValid) {
        document.getElementById("register").disabled = false;
    }
    else {
        document.getElementById("register").disabled = true;
    }
}

function checkFormStatus2() {
    if (nombreValid && nArchivoValid && fechaValid && obsComplete) document.getElementById("hAgregar").disabled = false;
    else document.getElementById("hAgregar").disabled = true;

}
function validarNoVacio(inputField) {
    if (inputField.value.length == 0) {

        return false;
    }
    else {

        return true;

    }
}
function desImagen() {
    var dropResolucion = document.getElementById("dTipoResolucion");
    var indice = dropResolucion.selectedIndex;
    var textoEscogido = dropResolucion.options[indice].text;
    if (textoEscogido != "CLONADO")
        document.getElementById("dImagen").disabled = true;
    else document.getElementById("dImagen").disabled = false; 


}