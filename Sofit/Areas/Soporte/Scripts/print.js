﻿//<reference path="jquery-1.4.1-vsdoc.js"/>
var regular = /^WS\d{5}$|^WS-\d{5}-RHC$|^NB\d{5}$|^NB-\d{5}-RHC$|^PR\d{5}$|^MN\d{5}$|^VS\d{5}$|^SV\d{5}$|^FR\d{5}$|^D\d{3}NT\d{2}$/;
var filterCount = 0;
$(function () {
    $('#nvaOblea').live('click', agregarOblea).button();
    $('.sticker').live('adjustName', function () {
        var suffix = $(this).data('suffix');
        var oblea = $(this).data('nro');
        $(this).find('.texto').html(oblea);
        $(this).find('.code').html('*'+oblea+'*');
        if (/(\w)+\.(\d)+$/.test($(this).attr('id'))) return;
        $(this).attr('id', $(this).attr('id') + suffix);
    });
    $('.texto').html(getParameterByName('id'));
    $('.code').html('*'+getParameterByName('id')+'*');
    $('#divNvaOblea').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        position: "center",
        show: 'slide',
        close: function () { $('#nroOblea').val(''); $('.error').hide(); },
        title: "Ingrese el Número de Oblea",
        buttons: {
            Aceptar: function () {
                var nroOblea = $.trim($('#nroOblea').val());
                $('.error').hide();
                if (!regular.test(nroOblea)) {
                    $('.error').show('fast').find('.message').html('El texto ingresado no parece una oblea válida.');
                }
                else {
                    $(this).dialog('close');
                    var nvoDiv = $('#sticker').clone().data({ 'suffix': '.' + filterCount++, 'nro': nroOblea });
                    $('.button-close', nvoDiv).removeAttr('disabled');
                    nvoDiv.appendTo('#stickerContainer').trigger('adjustName');
                    checkQuantity();
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });
    $('.button-close').live('click', function (e) {
        e.preventDefault();
        $(this).parents('div.sticker').remove();
        filterCount--;
        checkQuantity();
    });
});

function agregarOblea(e) {
    e.preventDefault();
    $('#divNvaOblea').dialog('open');
}
function checkQuantity() {
    if (filterCount > 12) $('#nvaOblea').button('disable', 'disable');
    else $('#nvaOblea').button({ 'disabled': false });
}