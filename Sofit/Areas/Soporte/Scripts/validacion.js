﻿function ponerWS() {

    if(document.getElementById("tOblea").value.length==0)
    {
        document.getElementById("tOblea").value = "WS";        
    }
}   

function checkValidacion(){

    if (validarNoVacio(document.getElementById("tModelo"), tModelo_help) &&
          validarIsNum(document.getElementById("tUsb"), tUsb_help) &&
          validarIsNum(document.getElementById("tIde"),tIde_help)&&          
          validarIsNum(document.getElementById("tPci"),tPci_help)&&          
          validarIsNum(document.getElementById("tSata"),tSata_help)&&
          validarIsNum(document.getElementById("tMemoria"),tMemoria_help)
       )
         {
             
             document.getElementById("aspAceptar").click();
    }
    else alert("Verifique los errores");


}
function imprimir(){
    window.print();
}

function aceptarTodo() {
    
    if (validarNombreEquipo(document.getElementById("tOblea"), tOblea_help) &&
    validarRadio(document.getElementsByName("serie"), document.getElementById("serie_help")) &&
    validarRadio(document.getElementsByName("modelo"), document.getElementById("modelo_help")) &&    
    validarTextoYRadio(document.getElementById("tSerie"),tserie_help,document.getElementsByName("serie"))&&
    validarTextoYRadio(document.getElementById("tModeloPc"), tModeloPc_help, document.getElementsByName("modelo")) &&
    checkDrop(document.getElementById("dDependencia"), document.getElementById("dDependencia_help")) &&

    validarRadio(document.getElementsByName("licencia"), document.getElementById("licenciaOs_help")) &&
    validarRadio(document.getElementsByName("licenciaOffice"), document.getElementById("licenciaOff_help")) &&
    validarTextoYRadio(document.getElementById("tLicenciaOs"), licenciaOs_help, document.getElementsByName("licencia")) &&
    validarTextoYRadio(document.getElementById("tLicenciaOff"), licenciaOff_help, document.getElementsByName("licenciaOffice")) &&
    confirm("Se van a actualizar los datos. OK?"))
        document.getElementById("aspAceptarTodo").click();         
    else alert("Verificar los errores");
}

function validarTextoYRadio(textBox,help_text,radioB){
    if (getCheckedValue(radioB) == "no" || getCheckedValue(radioB) == "mno" || getCheckedValue(radioB) == "lno"
    || getCheckedValue(radioB)=="lono") {

        if (help_text != null) {
            help_text.innerHTML = "";

        }
        return true;
    }
    else {

        if ((getCheckedValue(radioB) == "si" || getCheckedValue(radioB) == "msi" 
        || getCheckedValue(radioB) == "lsi"|| getCheckedValue(radioB)=="losi") 
        && validarNoVacio(textBox, help_text)) {

            if (help_text != null) {
                help_text.innerHTML = "";

            }
            return true;
        }
    }
    return false;    
}


function validarRadio(radioB, helpText) {

    var c = false;
    for (i = 0; i < radioB.length; i++) {
        if (radioB[i].checked) {
            c = true;
        }
    }


    if (c == false && helpText != null) helpText.innerHTML = "Debe seleccionar un valor";
    else {

        if (helpText != null) {
            helpText.innerHTML = "";

        }
        return c;

    }
}

    function getCheckedValue(radioObj) {
        if (!radioObj)
            return "";
        var radioLength = radioObj.length;
        if (radioLength == undefined)
            if (radioObj.checked)
            return radioObj.value;
        else
            return "";
        for (var i = 0; i < radioLength; i++) {
            if (radioObj[i].checked) {
                return radioObj[i].value;
            }
        }
        return "";
    }

    function updateUbicacion() {

        document.getElementById("buscarDependencia").click();
    }



    function validarNuevaDep() {

        if (validarNoVacio(document.getElementById("tADependencia"), tADependencia_help))
            document.getElementById("aspNuevaDep").click();
        else alert("Verificar los errores");
    }

    function verificarWs(inputStr) {

        var inp = inputStr.substring(0, 2);
        if (inp == "WS") return true;

    }
    function validarNuevaRed() {
        if (validarNoVacio(document.getElementById("tModeloRed"), tModeloRed_help) &&
        validarNoVacio(document.getElementById("tVelocidadRed"), tVelocidadRed_help))
            document.getElementById("aspNuevaRed").click();
        else alert("Verificar errores");
    }

    function validarNuevoSoft() {
        if (validarNoVacio(document.getElementById("tDescSoft"), tDescSoft_help))
            document.getElementById("aspNuevoSoft").click();
        else alert("Verificar errores");
    }

    function validarNuevoCpu() {

        if (validarNoVacio(document.getElementById("tModeloCpu"), tModeloCpu_help) &&
    validarNoVacio(document.getElementById("tVelocidadCpu"), tVelocidadCpu_help) &&
    validarNoVacio(document.getElementById("tCache"), tCache_help)) {
            document.getElementById("aspNuevoCpu").click();
        }
        else alert("Vefifique los errores");
    }
    function nuevoOptico() {
        document.getElementById("aspNuevoOptico").click();
    }

    function validarAltaDisco() {
        if (validarNoVacio(document.getElementById("tACapacidad"), tACapacidad_help))
            document.getElementById("aspNuevoDisco").click();
        else alert("Verifique los errores");

    }

    function validarAltaVideo() {
        if (validarNoVacio(document.getElementById("tAModeloVideo"), tAModeloVideo_help))
            document.getElementById("aspNuevoVideo").click();
        else alert("Verifique los errores");

    }

    function validarAltaRam() {

        if (validarNoVacio(document.getElementById("tACapacidadRam"), tACapacidad_help) &&
    validarIsNum(document.getElementById("tACapacidadRam"), tACapacidad_help) &&
    validarNoVacio(document.getElementById("tAUM"), tAUM_help))
            document.getElementById("aspNuevoRam").click();
        else
            alert("Verifique los errores");
    }


    function validarNoVacio(inputField, helpText) {
        if (inputField.value.length == 0) {
            inputField.value = "Ingrese un valor";
            inputField.className = "help";
            
            return false;
        }
        else {
            if (helpText != null) 
                helpText.innerHTML = "";
                return true;
            
        }

    }
    

    function validarIsNum(inputField, helpText) {
        if (isNaN(parseInt(inputField.value))) {
            if (helpText != null) helpText.innerHTML = "Debe ingresar un número";
            return false;
        }
        else {
            if (helpText != null) {
                helpText.innerHTML = "";
                return true;
            }
        }
    }

    function validarNombreEquipo(inputField, helpText) {

        if (!validarNoVacio(inputField, helpText))
            return false;


        return regularExpression(/^WS\d{5}$|^WS-\d{5}-RHC$|^NB\d{5}$/, inputField, helpText,
        "El formato no es correcto");
    }

    function regularExpression(regex, inputStr, helpText, helpMessage) {
        if (!regex.test(inputStr.value)) {

            if (helpText != null)
                helpText.innerHTML = helpMessage;
            return false;
        }
        else {
            if (helpText != null)
                helpText.innerHTML = "";
            return true;
        }

    }
    function resetStyle(inputField) {

        inputField.disabled = false;
        inputField.className = "";
        inputField.value = "";
        
    }
    function blockedInput(inputField) {

        inputField.className = "blocked";
        inputField.value = "";
        inputField.disabled = true;
    }
    
    function checkDrop(drop,drop_help) {

        var indice = drop.selectedIndex;
        if (indice == -1) {
            if (drop_help != null) drop_help.innerHTML = "Seleccione un valor";
            
            return false;
        }
        else {
            if (drop_help != null) drop_help.innerHTML = "";
            return true;
        }
    }

    function updateModeloMother() {
        document.getElementById("buscarMother").click();
    }
    function updateModeloCpu() {
        document.getElementById("buscarCpu").click();
    }
    function updateMarcaHd() {
        document.getElementById("buscarHdMarca").click();
    }
    function buscarCapHd() {
        document.getElementById("buscarHdCapacidad").click();
    }
    function updateMarcaRam() {
        document.getElementById("buscarMarcaRam").click();
    }
    function updateCapRam() {
        document.getElementById("buscarCapacidadRam").click();
    }
    function updateMarcaVideo() {
        document.getElementById("buscarMarcaVideo").click();
    }
    function updateModeloVideo() {
        document.getElementById("buscarModeloVideo").click();
    }
    function updateMarcaRed() {
        document.getElementById("buscarMarcaRed").click();
    }
    function updateModeloRed() {
        document.getElementById("buscarModeloRed").click();
    }
    function updateMarcaOptico() {
        document.getElementById("buscarMarcaOptico").click();
    }
    function updateInterfazOptico() {
        document.getElementById("buscarModeloOptico").click();
    }
    function updateCapacidadRam() {
        document.getElementById("buscarCapacidadRam").click();
    }