﻿$(function () {
    $('#bQuery').on('click', function () {
        var NewMother = new Object();
        NewMother.id_marca = 240;
        NewMother.id_interfaz = 3;
        NewMother.Capacidad = '240 GB';
        //NewMother.Socket = 1;
        //NewMother.CantUsb = 1;
        //NewMother.cantIde = 0;
        //NewMother.cantSata = 1;
        //NewMother.cantRam = 1;
        //NewMother.PciX = false;
        //NewMother.Agp = true;
        $.ajax({
            url: "Inventario/EquipoXTipo",
            data: {'prefixText':'W','idTipo':1} ,
            datatype:'json',
            type: 'POST',
            beforeSend: function () {
            },
            success: function (data) {
                alert(data.Info)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            },
            complete: function () {
                $(".ajaxGif").css({ 'display': 'none' });
            }
        }); //fin ajax
    });  
    $('.locRefContainer', '#Referentes').on('click', '.row .trash', function () {
        var div = $(this).parents('div.row');
        div.remove();
    });
    $('.refContainer', '#Referentes').on('click', '.row .trash', function () {
        var div = $(this).parents('div.row');
        var spanId = $(div).find('span:first').html();
        $.ajax({
            url: '/Config/quitarReferente',
            data: { 'idRef': spanId },
            type: 'post',
            success: function (d) {
                if (d.Info == 'ok') {
                    div.remove()
                }
                else mostrarError(d.Detail);
            },
            error: function (d, e, f) {
                mostrarError(f);
            }
        });
        ;
    });
    $('#sJurisd').trigger('change');
    $('#tabset').on('click', '#jurisdicciones .trashJur', function () {
        var tr = $(this).parents('tr');
        var id = $(tr).find('td:hidden').html();
        $.ajax({
            url: 'Config/quitarJurisdiccion',
            data: { 'id': id },
            type: 'post',
            success: function (data) {
                if (data.Info == "Login") { mostrarFinSesion(); }
                if (data.Info == 'ok') {
                    tr.remove();
                    reloadCurrentTab();
                }
                else mostrarError(data.Detail);
            },
            error: function () {
                mostrarError("Error quitando Jurisdicción.");
            }
        });
    });
    $('#tabset').on('click', '#jurisdicciones .renameJur', function () {
        var tr = $(this).parents('tr');
        var id = $(tr).find('td:hidden').html();
        $('#rJurisdiccion').data('id', id).dialog('open');
    });
    $('#tabset').on('click', '#dependencias .renameDep', function () {
        var tr = $(this).parents('tr');
        var id = $(tr).find('td:hidden').html();
        $('#rDependencia').data('id', id).dialog('open');
    });
    $('#tabset').on('click', '#dependencias .trashDep', function () {
        var tr = $(this).parents('tr');
        var id = $(tr).find('td:hidden').html();
        $.ajax({
            url: 'Config/quitarDependencia',
            data: { 'id': id },
            type: 'post',
            success: function (data) {
                if (data.Info == "Login") { mostrarFinSesion(); }
                if (data.Info == 'ok') {
                    tr.remove();
                    reloadCurrentTab();
                }
                else mostrarError(data.Detail);
            },
            error: function () {
                mostrarError("Error quitando Dependencia.");
            }
        });
    });
    $('#tabset').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
            $(this).find('td>div').eq(1).addClass('renameJur');
            $(this).find('td>div').eq(0).addClass('trashJur');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover icon aviso');
            $(this).find('td>div').eq(1).removeClass('renameJur');
            $(this).find('td>div').eq(0).removeClass('trashJur');
        },
        click: function () {
            $('.jurisdicciones tr').each(function () {
                $(this).removeClass('ui-state-highlight');
            });
            $(this).addClass('ui-state-highlight');
            var id = $(this).find('td:eq(0)').text();
            var panel = $(this).parents('div.ui-tabs-panel');
            buscarDependencias(id, panel);
        }
    },
            '#jurisdicciones tr');
    $('#tabset').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
            $(this).find('td>div').eq(1).addClass('renameDep');
            $(this).find('td>div').eq(0).addClass('trashDep');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
            $(this).find('td>div').eq(1).removeClass('renameDep');
            $(this).find('td>div').eq(0).removeClass('trashDep');
        },
        click: function () {
            $('.dependencias tr').each(function () {
                $(this).removeClass('ui-state-highlight');
            });
            $(this).addClass('ui-state-highlight');
        }
    },
            '#dependencias tr');    
    //VER INVENTARIO
    $('#tabset').on('click', '.verInventario', function () {
        var nroOblea = $(this).attr('data-o');
        var myWindow = window.open('Inventario?nro=' + nroOblea, '', 'height=800,width=670,menubar=no,toolbar=no,scrollbars=yes,status=no');
        myWindow.focus();
    });
    //DIALOG NVA JURISD
    $('#tabset').on('click', '#nvaJurisd', function (e) {
        e.preventDefault();
        $('#Jurisdicciones').dialog('open');
    });
    //DIALOG NVA DEP
    $('#tabset').on('click', '#nvaDependencia', function (e) {
        e.preventDefault();
        var checkin = $('tr.ui-state-highlight td:eq(0)', '.jurisdicciones');
        if (checkin.length == 0) {
            mostrarError("Seleccione primero una Jurisdicción");
            return;
        }
        $('#Dependencia').dialog('open');
    });
    $('#tabset').on('click', '.password', function () {
        var tr = $(this).parents('tr');
        var nro = tr.find('td:eq(0)').text();
        var nombre = tr.find('td:eq(1)').text();
        var titulo = $("#PasswordChanger").siblings('.ui-dialog-titlebar').find('span');
        titulo.html('Cambio de Password para: ' + nombre);
        $("#PasswordChanger").dialog('open').data('datos', { 'nro': nro, 'nombre': nombre });
    });

    $('#tabset').on('click', '#verEquipoXDep', function (e) {
        e.preventDefault();
        var checkin = $('tr.ui-state-highlight td:eq(0)', '.dependencias');
        if (checkin.length == 0) {
            mostrarError("Seleccione primero la Dependencia");
            return;
        }
        listadoEquiposXDependencia(checkin.text());
    });
});//   READY
function bindsOnLoad(event,ui) {
    $('button').button();

    switch (ui.panel[0].id) {
        case 'ui-tabs-1':
            var personal = $('#idTemaPers').val();
            setSelectByVal($('#themeSwitcher'), personal);
            $('#cambioPassword').button({ disabled: true });
            $('#themeSwitcher').trigger('change');
            break;
        case 'ui-tabs-3':
            setUpDialogs();            
            break;

    }//fin switch
}//find binds--------------------------------- >
function deleteEquipo(e) {
    e.preventDefault();
    var oblea = $.trim($('#tOblea').val());
    var este = $('#removeEquipo');
    if (regularEquipo.test(oblea.toUpperCase())) {
        $.ajax({
            url: 'Config/borrarEquipo',
            type: 'POST',
            data: 'oblea=' + oblea,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Equipo Borrado');
                }
                else {
                    mostrarError(data.Detail)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            },
            complete: function () {
                $('.ajaxGif').css({ 'display': 'none' });
            }
        });
    }
    else {
        mostrarError("El formato de la oblea no es válido");
    }
}
function actualizarTiempos(e) {
    e.preventDefault();
    var tAvisoClon = $('#tAvisoClon').val();
    var tEmerClon = $('#tEmergClon').val();
    var tAvisoLab = $('#tAvisoLab').val();
    var tEmerLab = $('#tEmergLab').val();
    var tAvisoEntrega = $('#tAvisoDel').val();
    var tEmerEntrega = $('#tEmergDel').val();
    var tAvisoSus = $('#tAvisoSus').val();
    var tEmerSus = $('#tEmergSus').val();   
    var tAvisoAvisoGar = $('#tAvisoAvisoGar').val();
    var tAvisoEnGar = $('#tAvisoEnGar').val();
    var tEmerAvisoGar = $('#tEmergAvisoGar').val();
    var tAvisoEnEmer = $('#tAvisoEnGar').val();
    var tEmerEnEmer = $('#tEmerEnGar').val();
    if (regExTest(regTime, tAvisoClon) && regExTest(regTime, tEmerClon) && regExTest(regTime, tAvisoLab)
    && regExTest(regTime, tAvisoSus) && regExTest(regTime, tEmerSus) &&
    regExTest(regTime, tEmerLab) && regExTest(regTime, tAvisoEntrega) && regExTest(regTime, tEmerEntrega)
    && regExTest(regTime, tAvisoAvisoGar) && regExTest(regTime, tAvisoEnEmer)&& regExTest(regTime, tEmerEnEmer)
    ) {
        var datos = {};
        datos.tAClon = tAvisoClon;
        datos.tALab = tAvisoLab;
        datos.tADel = tAvisoEntrega;
        datos.tASus = tAvisoSus;
        datos.tAAGar = tAvisoAvisoGar;
        datos.tAEGar = tAvisoEnGar;
        datos.tEClon = tEmerClon;
        datos.tELab = tEmerLab;
        datos.tEDel = tEmerEntrega;
        datos.tESus = tEmerSus;
        datos.tEAGar = tEmerAvisoGar;
        datos.tEEGar = tEmerEnEmer;
        $.ajax({
            url: "Config/setTiempos",
            type: 'POST',
            data: datos,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Tiempos Actualizados');
                }
                else {
                    mostrarError(data.Detail);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError)
            }
        });
    }
    else {
        mostrarError("Verifique los datos");
    }
}
function getTiempos() {
    $.ajax({
        url: "../negocio/handlers/getTiempos.ashx",
        datatype: 'json',
        beforeSend: function () {

        },
        success: function (data) {
            if (data.data == 'out') {
                mostrarFinSesion();
                return;
            }
            else {
                $('#tAvisoClon').val(data.tAClon);
                $('#tEmergClon').val(data.tEClon);
                $('#tAvisoLab').val(data.tALab);
                $('#tEmergLab').val(data.tELab);
                $('#tAvisoLog').val(data.tALog);
                $('#tEmerLog').val(data.tELog);
                $('#tAvisoSus').val(data.tASus);
                $('#tEmergSus').val(data.tESus);
                $('#tAvisoDel').val(data.tADel);
                $('#tEmergDel').val(data.tEDel);
                $('#tAvisoIAt').val(data.tAIncAt);
                $('#tEmerIAt').val(data.tEIncAt);
                $('#tAvisoAvisoGar').val(data.tAAGar);
                $('#tEmergAvisoGar').val(data.tEAGar);
                $('#tAvisoEnGar').val(data.tAEGar);
                $('#tEmerEnGar').val(data.tEEGar);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        },
        complete: function () {
            $('.ajaxGif').css({ 'display': 'none' });
        }
    });
}
