// Custom Date function to display a date in DD/MM/YYYY format
Date.prototype.shortFormat = function() {
return this.getDate() + "/" + (this.getMonth() + 1) + "/" + this.getFullYear();
}

String.prototype.startsWith = function(str) { return (this.indexOf(str) === 0); } 

