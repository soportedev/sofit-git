﻿// Global array of blog entries
var blog = new Array();

// Global Ajax request
var ajaxReq = new AjaxRequest();

//Constructor 
function Blog(body, date, author, image) {
    this.body = body;
    this.date = date;
    this.author = author;
    this.image = image;
}

//funcion de instancia to String
Blog.prototype.toString = function() {
    return "[" + this.date.shortFormat() + "]" + this.body ;
};

//funcion de instancia to Html
Blog.prototype.toHTML = function(highlight) {
    var blogHTML = "";
    blogHTML += highlight ? "<p style='background-color:#AAA'>" : "<p>";

    if (this.image) {
        blogHTML += "<strong>" + this.date.shortFormat() + "</strong><br /><table><tr><td><img src='" +
            this.image + "'/></td><td style='vertical-align:top'>" + this.body + "</td></tr></table><em>" +
            "por "+this.author + "</em></p>";
    }
    else {
        blogHTML += "<strong>" + this.date.shortFormat() + "</strong><br />" + this.body +
            "<br /><em>" + "por " + this.author + "</em></p>";
    }
    return blogHTML;
};

//funcion de instancia buscar texto
Blog.prototype.containsText = function(text) {
    return (this.body.toLowerCase().indexOf(text.toLowerCase()) != -1);
};

//funcion de clase ordenador
Blog.blogSorter = function(blog1, blog2) {
    return blog2.date - blog1.date;
};



// Load the blog from an XML doc on the server using Ajax
function loadBlog() {
    initForm();
    document.getElementById("blogTexto").innerHTML = "<img src='../img/imgwait.gif' alt='Loading...' />";
    ajaxReq.send("GET", "../blog.xml", handleRequest);
}

// Handle the Ajax request
function handleRequest() {
    if (ajaxReq.getReadyState() == 4 && ajaxReq.getStatus() == 200) {

        
        // Store the XML response data
        var xmlData = ajaxReq.getResponseXML().getElementsByTagName("blog")[0];

        // Set the blog-wide signature
        Blog.prototype.signature = "by " + getText(xmlData.getElementsByTagName("author")[0]);

        // Create the array of Blog entry objects
        var entries = xmlData.getElementsByTagName("entry");
        for (var i = 0; i < entries.length; i++) {
            // Create the blog entry
            blog.push(new Blog(getText(entries[i].getElementsByTagName("body")[0]),
              new Date(getText(entries[i].getElementsByTagName("date")[0])),
              getText(entries[i].getElementsByTagName("author")[0]),
              getText(entries[i].getElementsByTagName("image")[0])));
        }

        // Enable the blog buttons
        document.getElementById("search").disabled = false;
        document.getElementById("showall").disabled = false;
//        document.getElementById("viewrandom").disabled = false;

        // Show the blog
        showBlog(5);
    }
}

// Show the list of blog entries
function showBlog(numEntries) {
    // First sort the blog
blog.sort(Blog.blogSorter);

    // Adjust the number of entries to show the full blog, if necessary
    if (!numEntries)
        numEntries = blog.length;

    // Show the blog entries
    var i = 0, blogListHTML = "";
    while (i < blog.length && i < numEntries) {
        blogListHTML += blog[i].toHTML(i % 2 == 0);
        i++;
    }

    // Set the blog HTML code on the page
   
    $("#blogTexto").html(blogListHTML);
}

// Search the list of blog entries for a piece of text
function searchBlog() {
    var searchText = document.getElementById("searchtext").value;
    for (var i = 0; i < blog.length; i++) {
        // See if the blog entry contains the search text
        if (blog[i].containsText(searchText)) {
            alert(blog[i]);
            break;
        }
    }

    // If the search text wasn't found, display a message
    if (i == blog.length)
        alert("Lo sentimos, no hay un blog con el texto indicado.");
}

// Display a randomly chosen blog entry
function randomBlog() {
    // Pick a random number between 0 and blog.length - 1
    var i = Math.floor(Math.random() * blog.length);
    alert(blog[i]);
}

// Get the text content of an HTML element
function getText(elem) {
    var text = "";
    if (elem) {
        if (elem.childNodes) {
            for (var i = 0; i < elem.childNodes.length; i++) {
                var child = elem.childNodes[i];
                if (child.nodeValue)
                    text += child.nodeValue;
                else {
                    if (child.childNodes[0])
                        if (child.childNodes[0].nodeValue)
                        text += child.childNodes[0].nodeValue;
                }
            }
        }
    }
    return text;
}


// Add a new blog entry to an XML doc on the server using Ajax
function addBlogEntry() {
    // Disable the Add button and set the status to busy
    document.getElementById("add").disabled = true;
    document.getElementById("status").innerHTML = "Adding...";

    // Send the new blog entry data as an Ajax request
    ajaxReq.send("POST", "../negocio/blogHandler.aspx", handleAddRequest, "application/x-www-form-urlencoded; charset=UTF-8",
          "date=" + document.getElementById("date").value + "&body=" + document.getElementById("body").value );
}

// Handle the Ajax request
function handleAddRequest() {
    if (ajaxReq.getReadyState() == 4 && ajaxReq.getStatus() == 200) {
        // Enable the Add button and clear the status
        document.getElementById("add").disabled = false;
        document.getElementById("status").innerHTML = "";
        var respuesta = ajaxReq.getResponseText();
        if (respuesta.indexOf("ok") === 0)
        // Confirm the addition of the blog entry
            alert("El nuevo blog fue agregado con éxito.");
        else alert("Hubo un problema con el sistema");

        $find('nuevo_ModalPopupExtender').hide();
        blog = new Array();
        loadBlog();
    }
}

// Initialize the blog entry form
function initForm() {
    document.getElementById("date").value = (new Date()).shortFormat();
    //document.getElementById("body").focus();
}