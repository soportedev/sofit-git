﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

namespace soporte.etiqueta
{
    public partial class BarCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the Requested code to be created.
            string Code;
            try
            {
                Code = Request["id"].ToString();
            }
            catch (Exception)
            {
                Code = "WS-12345-RHC";
            }
            // Multiply the lenght of the code by 40 (just to have enough width)
            int ancho = 350;
            int alto = 90;
            // Create a bitmap object of the width that we calculated and height of 100
            Bitmap oBitmap = new Bitmap(ancho, alto, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            //oBitmap.SetResolution(203, 203);
            // then create a Graphic object for the bitmap we just created.
            Graphics oGraphics = Graphics.FromImage(oBitmap);
            oGraphics.Clear(Color.White);
            oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            oGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            // Now create a Font object for the Barcode Font
            // (in this case the IDAutomationHC39M) of 18 point size
            Font oFont = new Font("IDAutomationHC39M", 10);

            // Let's create the Point and Brushes for the barcode
            PointF oPoint = new PointF(0, 0);
            SolidBrush oBrushWrite = new SolidBrush(Color.Black);
            SolidBrush oBrush = new SolidBrush(Color.White);

            // Now lets create the actual barcode image
            // with a rectangle filled with white color
            oGraphics.FillRectangle(oBrush, 0, 0, ancho, alto);
            // Set format of string.
    StringFormat drawFormat = new StringFormat();
    //drawFormat.LineAlignment = StringAlignment.Center;
    //drawFormat.Alignment = StringAlignment.Center;
    //drawFormat.FormatFlags = StringFormatFlags.NoWrap;



            // We have to put prefix and sufix of an asterisk (*),
            // in order to be a valid barcode
    oGraphics.DrawString("*" + Code + "*", oFont, oBrushWrite, 2, 2);

            // Then we send the Graphics with the actual barcode
            Response.ContentType = "image/jpeg";
            oBitmap.Save(Response.OutputStream, ImageFormat.Jpeg);

        }
    }
}
