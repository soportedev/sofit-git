﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Areas.Soporte.Controllers
{
    public class NotifDisplay
    {        
        public string fecha { get; set; }
        public string mensaje { get; set; }
        public List<string> mailsAdd { get; set; }
        public string enviadoPor { get; set; }
    }
}
