﻿using soporte.Areas.Soporte.Models.Displays;
using soporte.Areas.Telecomunicaciones.Models.ClasesVistas;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sofit.Areas.Soporte.Models.Displays;

namespace soporte.Areas.Soporte.Controllers
{
    [SessionActionFilter]
    [AllowCrossSiteJson]
    //[SessionActionFilter]
    public class InventarioController : Controller
    {
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            if (acceso == Constantes.AccesosPagina.ReadOnly)
            {
                ViewBag.acceso = "readonly";
            }
            if (acceso == Constantes.AccesosPagina.Full)
            {
                ViewBag.acceso = "full";
            }
            return View();
        }
        public ActionResult MiniView()
        {
            return PartialView();
        }
        public ActionResult Popup()
        {
            return View();
        }
        #region Pc
        #region nuevos
        public JsonResult NuevoMother(MBVista mother)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Motherboard existe = dbc.Motherboard.Where(c => c.modelo == mother.Modelo).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe el modelo";
                    }
                    else
                    {
                        Motherboard nuevo = new Motherboard
                        {
                            nro_usb = mother.CantUsb,
                            pci_express = mother.PciX ? 1 : 0,
                            agp = mother.Agp ? 1 : 0,
                            nro_slot_mem = mother.cantRam,
                            modelo = mother.Modelo,
                            nro_sata = mother.cantSata,
                            id_marca = Int32.Parse(mother.Marca),
                            id_chipset = Int32.Parse(mother.Chipset),
                            id_socket = Int32.Parse(mother.Socket),
                            nro_pci = 0
                        };
                        dbc.Motherboard.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoDisco(HdVista disco)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    HD existe = dbc.HD.Where(c => c.id_interfaz == disco.Id_Interfaz & c.id_marca == disco.Id_Marca & c.capacidad == (disco.Capacidad + disco.UM)).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe el Disco";
                    }
                    else
                    {
                        HD nuevo = new HD
                        {
                            id_interfaz = disco.Id_Interfaz,
                            id_marca = disco.Id_Marca,
                            capacidad = (disco.Capacidad + disco.UM)
                        };
                        dbc.HD.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoRam(RamVista ram)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Ram nuevo = new Ram
                    {
                        id_tipo_ram = ram.id_tipo,
                        id_marca = ram.id_marca,
                        capacidad = ram.Capacidad,
                        um = ram.UM
                    };
                    dbc.Ram.Add(nuevo);
                    dbc.SaveChanges();
                    result.Info = "ok";                    
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoVideo(VideoVista video)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Video existe = dbc.Video.Where(c => c.id_interfaz == video.idInterfaz & c.id_marca == video.idMarca & c.modelo == video.Modelo).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe la placa de video";
                    }
                    else
                    {
                        Video nuevo = new Video
                        {
                            id_interfaz = video.idInterfaz,
                            id_marca = video.idMarca,
                            modelo = video.Modelo
                        };
                        dbc.Video.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoRed(NicVista nic)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Placa_red existe = dbc.Placa_red.Where(c => c.id_marca == nic.idMarca & c.velocidad == nic.Velocidad & c.tecnologia == nic.Tecnologia & c.modelo == nic.Modelo).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe la placa";
                    }
                    else
                    {
                        Placa_red nuevo = new Placa_red
                        {
                            velocidad = nic.Velocidad,
                            tecnologia = nic.Tecnologia,
                            modelo = nic.Modelo,
                            id_marca = nic.idMarca
                        };
                        dbc.Placa_red.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevooptico(DiscoOpticoVista optico)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Optico existe = dbc.Optico.Where(c => c.id_interfaz == optico.idInterfaz & c.id_marca == optico.idMarca & c.id_tipo_disco == optico.idTipo).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe la unidad optica";
                    }
                    else
                    {
                        Optico nuevo = new Optico
                        {
                            id_interfaz = optico.idInterfaz,
                            id_tipo_disco = optico.idTipo,
                            id_marca = optico.idMarca
                        };
                        dbc.Optico.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoMicro(CpuVista cpu)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Micro existe = dbc.Micro.Where(c => c.modelo == cpu.Modelo).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe el CPU";
                    }
                    else
                    {
                        Micro nuevo = new Micro
                        {
                            modelo = cpu.Modelo,
                            velocidad = cpu.Frecuencia,
                            cache = cpu.Cache,
                            id_marca = cpu.idMarca,
                            id_socket = cpu.idSocket
                        };
                        dbc.Micro.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult nuevoSoft(SoftVista soft)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Software existe = dbc.Software.Where(c => c.descripcion == soft.Nombre).SingleOrDefault();
                    if (existe != null)
                    {
                        result.Info = "error";
                        result.Detail = "Ya existe el Soft";
                    }
                    else
                    {
                        Software nuevo = new Software
                        {
                            descripcion = soft.Nombre
                        };
                        dbc.Software.Add(nuevo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }

        #endregion nuevos
        public JsonResult DropMother(int mother)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                Motherboard buscado = dbc.Motherboard.Where(c => c.id_mother == mother).First();
                dbc.Motherboard.Remove(buscado);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public JsonResult EditPc(PcInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.UnidadConf where u.nombre == e.Oblea select u).SingleOrDefault();

                if (encontrado == null)
                {
                    Pc pc = dbc.Pc.Where(c => c.id_uc == e.idEquipo).Single();
                    pc.id_dependencia = e.Dependencia.idDependencia;
                    pc.id_estado = dbc.Estado_UC.Where(c => c.id_estado == e.Estado.idEstado).Single().id_estado;
                    pc.id_marca = e.Marca.idMarca;
                    pc.id_uc = e.idEquipo;
                    pc.clave_adm = e.ClaveAdmin;
                    pc.clave_bios = e.ClaveBios;
                    pc.f_mouse = e.PuertoMouse;
                    pc.f_teclado = e.PuertoTeclado;
                    pc.id_Micro = e.Cpu.Id;
                    pc.id_mother = e.MB.Id;
                    pc.id_os = e.Os.Id;
                    pc.l_tarjeta = e.LectorTarjeta ? 1 : 0;
                    pc.modelo = e.Modelo;
                    dbc.SaveChanges();
                    result.Info = "ok";
                    
                }
                else
                {
                    result.Detail = "Equipo No Encontrado";
                }
            }
            return Json(result);
        }

        public JsonResult UpdatePc(PcInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Pc encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.Pc where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                    if (encontrado != null)
                    {

                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        //estado check primero si esta activo
                        IQueryable<EquiposXIncidencia> res = dbc.EquiposXIncidencia.Where(c => c.UnidadConf.id_uc == e.idEquipo);
                        if (res.Count() == 0)
                        {
                            encontrado.id_estado = e.Estado.idEstado;
                        }
                        encontrado.UnidadConf.id_jurisdiccion = e.Jurisdiccion.idJurisdiccion;
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        encontrado.id_marca = e.Marca.idMarca;
                        encontrado.clave_adm = e.ClaveAdmin;
                        encontrado.clave_bios = e.ClaveBios;
                        encontrado.f_mouse = e.PuertoMouse;
                        encontrado.f_teclado = e.PuertoTeclado;
                        if (e.Cpu != null) { encontrado.id_Micro = e.Cpu.Id; }
                        if (e.MB != null) { encontrado.id_mother = e.MB.Id; }
                        if (e.Os != null) { encontrado.id_os = e.Os.Id; }
                        encontrado.l_tarjeta = e.LectorTarjeta ? 1 : 0;
                        encontrado.modelo = e.Modelo;

                        //devices
                        if (e.DiscoOptico != null)
                        {
                            encontrado.Optico.Clear();
                            foreach (var v in e.DiscoOptico)
                            {
                                var found = dbc.Optico.Where(c => c.id_optico == v.Id).Single();
                                encontrado.Optico.Add(found);
                            }
                        }
                        else
                        {
                            encontrado.Optico.Clear();
                        }
                        if (e.Hd != null)
                        {
                            encontrado.HD.Clear();
                            foreach (var v in e.Hd)
                            {
                                var found = dbc.HD.Where(c => c.id_disco == v.Id).Single();
                                encontrado.HD.Add(found);
                            }
                        }
                        else
                        {
                            encontrado.HD.Clear();
                        }
                        if (e.PlacaRed != null)
                        {
                            encontrado.Placa_red.Clear();
                            foreach (var v in e.PlacaRed)
                            {
                                var found = dbc.Placa_red.Where(c => c.id_placa == v.Id).Single();
                                encontrado.Placa_red.Add(found);
                            }
                        }
                        else
                        {
                            encontrado.Placa_red.Clear();
                        }
                        if (e.Ram != null)
                        {
                            encontrado.Ram.Clear();
                            foreach (var v in e.Ram)
                            {
                                var found = dbc.Ram.Where(c => c.id_ram == v.Id).Single();
                                encontrado.Ram.Add(found);
                            }
                        }
                        else
                        {
                            encontrado.Ram.Clear();
                        }
                        if (e.Video != null)
                        {
                            encontrado.Video.Clear();
                            foreach (var v in e.Video)
                            {
                                var found = dbc.Video.Where(c => c.id_video == v.Id).Single();
                                encontrado.Video.Add(found);
                            }
                        }
                        else
                        {
                            encontrado.Video.Clear();
                        }
                        //if(e.SoftAdicional.Count>0)
                        //{
                        //    encontrado.Software.Clear();
                        //    foreach(var v in e.SoftAdicional)
                        //    {
                        //        var found = dbc.Software.Where(c => c.id_soft == v.Id).Single();
                        //        encontrado.Software.Add(found);
                        //    }
                        //}
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";                        
                    }
                    else
                    {
                        result.Detail = "Pc No encontrada";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;

            }
            return Json(result);
        }
        public JsonResult NuevoPc(PcInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf where (u.nombre == e.Oblea | 
                                      (e.NroSerie.Length>0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion=="Pc")) select u).FirstOrDefault();

                    if (encontrado == null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        UnidadConf uc = new UnidadConf
                            {
                                nombre = e.Oblea,
                                id_jurisdiccion = e.Jurisdiccion.idJurisdiccion,
                                fecha = DateTime.Now,
                                id_tipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Pc").Single().id_tipo,
                                observaciones = e.Observaciones,
                                nro_serie = e.NroSerie,
                                id_usuario = actual.IdUsuario
                            };
                        dbc.UnidadConf.Add(uc);
                        Pc pc = new Pc
                        {
                            id_dependencia = e.Dependencia.idDependencia,
                            id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Activo").Single().id_estado,
                            id_marca = e.Marca.idMarca,
                            id_uc = uc.id_uc,
                            clave_adm = e.ClaveAdmin,
                            clave_bios = e.ClaveBios,
                            f_mouse = e.PuertoMouse,
                            f_teclado = e.PuertoTeclado,
                            l_tarjeta = e.LectorTarjeta ? 1 : 0,
                            modelo = e.Modelo

                        };
                        if (e.Cpu != null)
                        {
                            pc.id_Micro = e.Cpu.Id;
                        }
                        if (e.MB != null)
                        {
                            pc.id_mother = e.MB.Id;
                        }
                        if (e.Os != null)
                        {
                            pc.id_os = e.Os.Id;
                        }
                        dbc.Pc.Add(pc);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {
                        result.Detail = "El equipo con esa Oblea/Nro. de serie ya existe";
                        return Json(result);
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }

            return Json(result);
        }

        public JsonResult NuevoEquipoSlim()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            string oblea = nvc.Get("oblea");
            int idTipo = Int32.Parse(nvc.Get("idTipo"));
            int idMarca = Int32.Parse(nvc.Get("idMarca"));
            int idJur = Int32.Parse(nvc.Get("idJur"));
            int idDep = Int32.Parse(nvc.Get("idDep"));
            using (var dbc = new IncidenciasEntities())
            {
                var checking = dbc.UnidadConf.Where(c => c.nombre == oblea).SingleOrDefault();
                if (checking != null)
                {
                    result.Info = "Error";
                    result.Detail = "Ya existe un equipo con esa oblea";
                }
                else
                {
                    try
                    {
                        var equipo = new UnidadConf
                        {
                            nombre = oblea,
                            id_jurisdiccion = idJur,
                            id_tipo = idTipo,
                            id_usuario = usuarioActual.IdUsuario,
                            fecha = DateTime.Now
                        };
                        string tipoEquipo = dbc.Tipo_equipo.Where(c => c.id_tipo == idTipo).Single().descripcion;
                        int idEstado = dbc.Estado_UC.Where(c => c.descripcion == "Activo").Single().id_estado;
                        switch (tipoEquipo)
                        {
                            case "Pc": dbc.Pc.Add(new Pc
                            {
                                id_uc = equipo.id_uc,
                                id_dependencia = idDep,
                                id_marca = idMarca,
                                id_estado = idEstado
                            });
                                break;
                            case "Impresora": dbc.Impresoras.Add(new Impresoras
                            {
                                id_uc = equipo.id_uc,
                                id_dependencia = idDep,
                                id_estado = idEstado
                            });
                                break;
                            case "Monitor": dbc.Monitores.Add(new Monitores
                            {
                                id_uc = equipo.id_uc,
                                id_dependencia = idDep,
                                id_estado = idEstado
                            });
                                break;
                        }
                        dbc.UnidadConf.Add(equipo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    catch (Exception e)
                    {
                        result.Detail = e.Message;
                    }
                }
            }
            return Json(result);
        }
        public JsonResult verificarCompleto(string oblea)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var equipo = from u in dbc.UnidadConf
                             where u.nombre == oblea
                             select u;
                if (equipo != null)
                {
                    result.Info = "ok";
                    string tipo = equipo.Single().Tipo_equipo.descripcion;
                    switch (tipo)
                    {
                        case "Pc": Pc pc = equipo.Single().Pc.Where(c => c.id_uc == equipo.Single().id_uc).Single();
                            if (pc.Micro == null | pc.Motherboard == null)
                            {
                                result.Info = "inc";
                            }
                            break;
                        case "Impresora": Impresoras pr = equipo.Single().Impresoras.Where(c => c.id_uc == equipo.Single().id_uc).Single();
                            if (pr.Modelo_Impresora == null)
                            {
                                result.Info = "inc";
                            }
                            break;
                        case "Monitor": Monitores mn = equipo.Single().Monitores.Where(c => c.id_uc == equipo.Single().id_uc).Single();
                            if (mn.Modelo_Monitor == null)
                            {
                                result.Info = "inc";
                            }
                            break;
                    }
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "No se encuentra el equipo";
                }
            }
            return Json(result);
        }
        #region get
        public JsonResult EquipoXOblea(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXSerie(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nro_serie.StartsWith(prefixText)
                         orderby u.nro_serie.Length
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXTipo(string prefixText, int idTipo)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nombre.StartsWith(prefixText) & u.id_tipo == idTipo
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();

            }
            return Json(items);
        }
        public JsonResult GetEquipoSoporte(string oblea)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Pc": equipo = new PcInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Impresora": equipo = new ImpresoraInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Monitor": equipo = new MonitorInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        default: equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);
        }
        public JsonResult Jurisdicciones()
        {
            List<JurisdiccionVista> jurisdicciones = new List<JurisdiccionVista>();
            using (var dbc = new IncidenciasEntities())
            {
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new JurisdiccionVista
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
            }
            return Json(jurisdicciones);
        }
        public JsonResult Dependencias(int idJur)
        {
            List<DependenciaVista> dependencias = new List<DependenciaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                dependencias = (from u in dbc.Dependencia
                                where u.id_jurisdiccion == idJur
                                orderby u.descripcion
                                select new DependenciaVista
                                {
                                    idDependencia = u.id_dependencia,
                                    Nombre = u.descripcion
                                }).ToList();
            }
            return Json(dependencias);
        }
        public JsonResult EstadosEquipo()
        {
            List<EstadoEquipoVista> estados = new List<EstadoEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                estados = (from u in dbc.Estado_UC
                           where u.Direcciones.nombre=="Soporte Técnico"
                           orderby u.descripcion
                           select new EstadoEquipoVista
                           {
                               idEstado = u.id_estado,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(estados);
        }
        public JsonResult MarcasSoporte()
        {
            List<MarcaVista> marcas = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                marcas = (from u in dbc.Marca
                          where u.Direcciones.nombre == "Soporte Técnico"
                          orderby u.descripcion
                          select new MarcaVista
                          {
                              idMarca = u.id_marca,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(marcas);
        }
        public JsonResult MarcasPorTipo(int tipo)
        {
            List<MarcaVista> marcas = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                marcas = (from u in dbc.Marca
                          where u.Direcciones.nombre == "Soporte Técnico" & u.dispositivo == tipo
                          orderby u.descripcion
                          select new MarcaVista
                          {
                              idMarca = u.id_marca,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(marcas);
        }
        public JsonResult NuevaMarcaSoporte(string nombre, int idTipoDis)
        {
            Responses result = new Responses();
            if (nombre.Length == 0)
            {
                result.Detail = "El campo no puede estar vacío";
            }
            else
            {
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var found = (from u in dbc.Marca
                                     where u.descripcion == nombre && u.Direcciones.nombre == "Soporte Técnico" && u.dispositivo == idTipoDis
                                     select u).SingleOrDefault();
                        if (found != null)
                        {
                            result.Detail = "La marca ya existe";
                            return Json(result);
                        }
                        else
                        {
                            Marca m = new Marca
                            {
                                descripcion = nombre,
                                dispositivo = idTipoDis,
                                id_direccion = (from u in dbc.Direcciones where u.nombre == "Soporte Técnico" select u.id_direccion).SingleOrDefault()
                            };
                            dbc.Marca.Add(m);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult TiposEquipo()
        {
            List<TiposEquipoVista> tipos = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipos = (from u in dbc.Tipo_equipo
                         where u.Direcciones.nombre == "Soporte Técnico"
                         //& (u.descripcion=="Pc"|u.descripcion=="Impresora"|u.descripcion=="Monitor"|u.descripcion=="Varios")
                         orderby u.descripcion
                         select new TiposEquipoVista
                         {
                             id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(tipos);
        }
        public JsonResult TiposEquipoGral()
        {
            List<TiposEquipoVista> tipos = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipos = (from u in dbc.Tipo_equipo
                         where u.Direcciones.nombre == "Soporte Técnico"
                         select new TiposEquipoVista
                         {
                             id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(tipos);
        }
        public JsonResult MotherXMarca(int idMarca)
        {
            List<MBVista> mb = new List<MBVista>();
            using (var dbc = new IncidenciasEntities())
            {
                mb = (from u in dbc.Motherboard
                      where u.id_marca == idMarca
                      orderby u.modelo
                      select new MBVista
                      {
                          Agp = u.agp.Value == 1,
                          cantRam = u.nro_slot_mem.Value,
                          cantSata = u.nro_sata.Value,
                          CantUsb = u.nro_usb.Value,
                          Chipset = u.Chipset.descripcion,
                          Id = u.id_mother,
                          Marca = u.Marca.descripcion,
                          Modelo = u.modelo,
                          PciX = u.pci_express.Value == 1,
                          Socket = u.Socket.descripcion
                      }).ToList();
            }
            return Json(mb);
        }
        public JsonResult CpuXMarca(int idMarca)
        {
            List<CpuVista> cpu = new List<CpuVista>();
            using (var dbc = new IncidenciasEntities())
            {
                cpu = (from u in dbc.Micro
                       where u.id_marca == idMarca
                       orderby u.modelo
                       select new CpuVista
                       {
                           Id = u.id_Micro,
                           Socket = new SocketVista { Id = u.Socket.id_socket, Nombre = u.Socket.descripcion },
                           Cache = u.cache,
                           Frecuencia = u.velocidad,
                           Marca = new MarcaVista { idMarca = u.Marca.id_marca, Nombre = u.Marca.descripcion },
                           Modelo = u.modelo
                       }).ToList();
            }
            return Json(cpu);
        }
        public JsonResult DiscoXMarcaInterfaz(int idMarca, int idInterfaz)
        {
            List<HdVista> disco = new List<HdVista>();
            using (var dbc = new IncidenciasEntities())
            {
                disco = (from u in dbc.HD
                         where u.id_marca == idMarca & u.id_interfaz == idInterfaz
                         orderby u.capacidad
                         select new HdVista
                         {
                             Capacidad = u.capacidad,
                             Id = u.id_disco,
                             Marca = u.Marca.descripcion,
                             Interfaz = u.Interfaz_disco.descripcion
                         }).ToList();
            }
            return Json(disco);
        }
        public JsonResult InterfazDisco()
        {
            List<IfDiscosVista> ifz = new List<IfDiscosVista>();
            using (var dbc = new IncidenciasEntities())
            {
                ifz = (from u in dbc.Interfaz_disco
                       orderby u.descripcion
                       select new IfDiscosVista
                       {
                           Id = u.id_interfaz,
                           Nombre = u.descripcion
                       }).ToList();
            }
            return Json(ifz);
        }
        public JsonResult MarcaDiscoXInterfaz(int idInterfaz)
        {
            List<MarcaVista> disco = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var f = from u in dbc.HD
                        where u.id_interfaz == idInterfaz
                        orderby u.Marca.descripcion
                        select u;
                var t = f.GroupBy(c => c.id_marca).Select(g => g.FirstOrDefault()).OrderBy(d => d.Marca.descripcion);
                foreach (var h in t)
                {
                    disco.Add(new MarcaVista
                    {
                        idMarca = h.Marca.id_marca,
                        Nombre = h.Marca.descripcion
                    });
                }
            }
            return Json(disco);
        }
        public JsonResult TipoMemoria()
        {
            List<TipoRamVista> tipo = new List<TipoRamVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipo = (from u in dbc.Tipo_ram
                        orderby u.descripcion
                        select new TipoRamVista
                        {
                            idTipo = u.id_tipo_ram,
                            Tipo = u.descripcion
                        }).ToList();
            }
            return Json(tipo);
        }
        public JsonResult MarcaMemoriaXTipo(int idTipo)
        {
            List<MarcaVista> mem = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var m = from u in dbc.Ram
                        where u.id_tipo_ram == idTipo
                        orderby u.Marca.descripcion
                        select u;
                var t = m.GroupBy(c => c.id_marca).Select(g => g.FirstOrDefault());
                foreach (var h in t)
                {
                    mem.Add(new MarcaVista
                    {
                        idMarca = h.Marca.id_marca,
                        Nombre = h.Marca.descripcion
                    });
                }
            }
            return Json(mem);
        }
        public JsonResult MemoriaXTipoYMarca(int idTipo, int idMarca)
        {
            List<RamVista> mem = new List<RamVista>();
            using (var dbc = new IncidenciasEntities())
            {
                mem = (from u in dbc.Ram
                       where u.id_tipo_ram == idTipo & u.id_marca == idMarca
                       orderby u.capacidad
                       select new RamVista
                       {
                           Id = u.id_ram,
                           Marca = u.Marca.descripcion,
                           Capacidad = u.capacidad,
                           UM = u.um,
                           Tipo = u.Tipo_ram.descripcion
                       }).ToList();
            }
            return Json(mem);
        }
        public JsonResult InterfazVideo()
        {
            List<IfVideoVista> ifvideo = new List<IfVideoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                ifvideo = (from u in dbc.Interfaz_placa
                           orderby u.descripcion
                           select new IfVideoVista
                           {
                               Id = u.id_interfaz,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(ifvideo);
        }
        public JsonResult MarcaVideoXInterfaz(int idInterfaz)
        {
            List<MarcaVista> video = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var v = from u in dbc.Video
                        where u.id_interfaz == idInterfaz
                        orderby u.modelo
                        select u;
                var t = v.GroupBy(c => c.id_marca).Select(g => g.FirstOrDefault()).OrderBy(d => d.Marca.descripcion);
                foreach (var h in t)
                {
                    video.Add(new MarcaVista
                    {
                        idMarca = h.Marca.id_marca,
                        Nombre = h.Marca.descripcion
                    });
                }
            }
            return Json(video);
        }
        public JsonResult VideoXInterfazMarca(int idInterfaz, int idMarca)
        {
            List<VideoVista> video = new List<VideoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                video = (from u in dbc.Video
                         where u.id_interfaz == idInterfaz & u.id_marca == idMarca
                         orderby u.modelo
                         select new VideoVista
                         {
                             Id = u.id_video,
                             Interfaz = u.Interfaz_placa.descripcion,
                             Marca = u.Marca.descripcion,
                             Modelo = u.modelo
                         }).ToList();
            }
            return Json(video);
        }
        public JsonResult RedXTecnologia(string tecnologia)
        {
            List<MarcaVista> red = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var te = from u in dbc.Placa_red
                         where u.tecnologia == tecnologia
                         orderby u.modelo
                         select u;
                var t = te.GroupBy(c => c.id_marca).Select(g => g.FirstOrDefault()).OrderBy(d => d.Marca.descripcion);
                foreach (var h in t)
                {
                    red.Add(new MarcaVista
                    {
                        idMarca = h.Marca.id_marca,
                        Nombre = h.Marca.descripcion
                    });
                }
            }
            return Json(red);
        }
        public JsonResult RedXTecnologiaMarca(string tecnologia, int idMarca)
        {
            List<NicVista> tec = new List<NicVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tec = (from u in dbc.Placa_red
                       where u.tecnologia == tecnologia & u.id_marca == idMarca
                       orderby u.modelo
                       select new NicVista
                       {
                           Id = u.id_placa,
                           Marca = u.Marca.descripcion,
                           Modelo = u.modelo,
                           Tecnologia = u.tecnologia,
                           Velocidad = u.velocidad
                       }).ToList();
            }
            return Json(tec);
        }
        public JsonResult TipoOptico()
        {
            List<TipoDiscoOpticoVista> topt = new List<TipoDiscoOpticoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                topt = (from u in dbc.Tipo_disco
                        orderby u.descripcion
                        select new TipoDiscoOpticoVista
                        {
                            Id = u.id_tipo_disco,
                            Nombre = u.descripcion
                        }).ToList();
            }
            return Json(topt);
        }
        public JsonResult OpticoXTipo(int idTipo)
        {
            List<MarcaVista> opticoM = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var optico = from u in dbc.Optico
                             where u.id_tipo_disco == idTipo
                             select u;
                var t = optico.GroupBy(c => c.id_marca).Select(g => g.FirstOrDefault()).OrderBy(d => d.Marca.descripcion);
                foreach (var h in t)
                {
                    opticoM.Add(new MarcaVista
                    {
                        idMarca = h.Marca.id_marca,
                        Nombre = h.Marca.descripcion
                    });
                }
            }
            return Json(opticoM);
        }
        public JsonResult OpticoXTipoMarca(int idTipo, int idMarca)
        {
            List<DiscoOpticoVista> optico = new List<DiscoOpticoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                optico = (from u in dbc.Optico
                          where u.id_tipo_disco == idTipo & u.id_marca == idMarca
                          orderby u.Interfaz_disco.descripcion
                          select new DiscoOpticoVista
                          {
                              Id = u.id_optico,
                              Interf = u.Interfaz_disco.descripcion,
                              idInterfaz = u.Interfaz_disco.id_interfaz,
                              Interfaz = new IfDiscosVista
                              {
                                  Nombre = u.Interfaz_disco.descripcion
                              },
                              Marca = new MarcaVista { Nombre = u.Marca.descripcion },
                              Tipo = new TipoDiscoOpticoVista { Nombre = u.Tipo_disco.descripcion }

                          }).ToList();
            }
            return Json(optico);
        }
        public JsonResult Software()
        {
            List<SoftVista> sof = new List<SoftVista>();
            using (var dbc = new IncidenciasEntities())
            {
                sof = (from u in dbc.Software
                       orderby u.descripcion
                       select new SoftVista
                       {
                           Id = u.id_soft,
                           Nombre = u.descripcion
                       }).ToList();
            }
            return Json(sof);
        }
        public JsonResult Os()
        {
            List<OsVista> os = new List<OsVista>();
            using (var dbc = new IncidenciasEntities())
            {
                os = (from u in dbc.Os
                      where u.Tipo_equipo.descripcion == "Pc"
                      orderby u.descripcion
                      select new OsVista
                      {
                          Id = u.id_os,
                          Nombre = u.descripcion
                      }).ToList();
            }
            return Json(os);
        }
        public JsonResult Socket()
        {
            List<SocketVista> socket = new List<SocketVista>();
            using (var dbc = new IncidenciasEntities())
            {
                socket = (from u in dbc.Socket
                          orderby u.descripcion
                          select new SocketVista
                          {
                              Id = u.id_socket,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(socket);
        }
        public JsonResult RelacionAspecto()
        {
            List<RelAspecto> socket = new List<RelAspecto>();
            using (var dbc = new IncidenciasEntities())
            {
                socket = (from u in dbc.Relacion_aspecto
                          orderby u.descripcion
                          select new RelAspecto
                          {
                              Id = u.id_relAspecto,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(socket);
        }
        public JsonResult Chipset()
        {
            List<ChipsetVista> chipset = new List<ChipsetVista>();
            using (var dbc = new IncidenciasEntities())
            {
                chipset = (from u in dbc.Chipset
                           orderby u.descripcion
                           select new ChipsetVista
                           {
                               Id = u.id_chipset,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(chipset);
        }
        #endregion
        #endregion
        #region Impresoras
        public JsonResult conectividadImpresora()
        {
            List<ConectividadPr> items = new List<ConectividadPr>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Conectividad_impresora

                         select new ConectividadPr
                         {
                             id = u.id_conectividad,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult rendimientoImpresora()
        {
            List<RendimientoImpresora> items = new List<RendimientoImpresora>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Rendimiento_impresora

                         select new RendimientoImpresora
                         {
                             Id = u.id_rendimiento,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult tecnologiaImpresora()
        {
            List<TecnologiaPr> items = new List<TecnologiaPr>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Tecnologia_Impresoras

                         select new TecnologiaPr
                         {
                             Id = u.id_tecnologia,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        #region insumos
        public JsonResult tipoInsumo()
        {
            List<TipoInsumo> items = new List<TipoInsumo>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Tipo_insumo

                         select new TipoInsumo
                         {
                             Id = u.id_tipo_insumo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult insumoXTipo(int idTipo)
        {
            List<InsumoPr> items = new List<InsumoPr>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Insumos
                         where u.id_tipo_insumo == idTipo
                         select new InsumoPr
                         {
                             Id = u.id_insumo,
                             Nombre = u.descripcion,
                             idTipo = u.id_tipo_insumo.Value,
                             Tipo = u.Tipo_insumo.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult NuevoInsumo(int idTipo, string nombre)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var check = dbc.Insumos.Any(c => c.descripcion == nombre & c.id_tipo_insumo == idTipo);
                if (check)
                {
                    result.Detail = "Ya existe un insumo cargado!!";
                    return Json(result);
                }
                Insumos nuevo = new Insumos
                {
                    descripcion = nombre,
                    id_tipo_insumo = idTipo
                };
                dbc.Insumos.Add(nuevo);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public JsonResult EditInsumo(int id, string nombre)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var found = dbc.Insumos.Where(c => c.id_insumo == id).SingleOrDefault();
                if (found!=null)
                {                    
                    found.descripcion = nombre;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }                
                else
                {
                    result.Detail = "No se encontró el Insumo";
                }
            }
            return Json(result);
        }
        #endregion

        public JsonResult tipoImpresora()
        {
            List<TipoPr> items = new List<TipoPr>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Tipo_impresora
                         select new TipoPr
                         {
                             Id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult ModelosImpresora(int marca)
        {
            Responses result = new Responses();
            List<ModeloImpresora> modelos = new List<ModeloImpresora>();
            using (var dbc = new IncidenciasEntities())
            {
                modelos = (from u in dbc.Modelo_Impresora
                           where u.id_marca == marca
                           orderby u.descripcion
                           select new ModeloImpresora
                           {
                               id = u.id_modelo,
                               Nombre = u.descripcion,
                               Rendimiento = new RendimientoImpresora { Nombre = u.Rendimiento_impresora.descripcion, Id = u.Rendimiento_impresora.id_rendimiento },
                               Tecnologia = new TecnologiaPr { Id = u.Tecnologia_Impresoras.id_tecnologia, Nombre = u.Tecnologia_Impresoras.descripcion },
                               Tipo = new TipoPr { Id = u.Tipo_impresora.id_tipo, Nombre = u.Tipo_impresora.descripcion }
                           }).ToList();

                return Json(modelos);
            }
        }
        //modelo impresora
        public JsonResult UpdateModelo(ModeloImpresora m)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Modelo_Impresora existente = dbc.Modelo_Impresora.Where(c => c.id_modelo == m.id).Single();
                    
                    existente.descripcion = m.Nombre;
                    existente.id_marca = m.idMarca;
                    existente.id_rendimiento = m.Rendimiento.Id;
                    existente.id_tipo = m.Tipo.Id;
                    existente.id_tecnologia = m.Tecnologia.Id;
                    existente.Conectividad_impresora.Clear();
                    foreach (ConectividadPr c in m.Conectividad)
                    {
                        existente.Conectividad_impresora.Add(dbc.Conectividad_impresora.Where(d => d.id_conectividad == c.id).Single());
                    }
                    existente.Insumos.Clear();
                    foreach (InsumoPr i in m.Insumos)
                    {
                        existente.Insumos.Add(dbc.Insumos.Where(f => f.id_insumo == i.Id).Single());
                    }                    
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoModelo(ModeloImpresora m)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    if (dbc.Modelo_Impresora.Any(c => c.descripcion == m.Nombre))
                    {
                        result.Detail = "Ya existe un modelo con ese nombre";
                        return Json(result);
                    }
                    Modelo_Impresora mod = new Modelo_Impresora
                    {
                        descripcion = m.Nombre,
                        id_tecnologia = m.Tecnologia.Id,
                        id_rendimiento = m.Rendimiento.Id,
                        id_tipo = m.Tipo.Id,
                        id_marca = m.idMarca
                    };
                    foreach (ConectividadPr c in m.Conectividad)
                    {
                        mod.Conectividad_impresora.Add(dbc.Conectividad_impresora.Where(d => d.id_conectividad == c.id).Single());
                    }
                    foreach (InsumoPr i in m.Insumos)
                    {
                        mod.Insumos.Add(dbc.Insumos.Where(f => f.id_insumo == i.Id).Single());
                    }
                    dbc.Modelo_Impresora.Add(mod);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        //fin modelo impresora
        //modelo monitor
        public JsonResult UpdateModeloMonitor(ModeloMonitor m)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    Modelo_Monitor existente = dbc.Modelo_Monitor.Where(c => c.id_modelo == m.id).Single();

                    existente.descripcion = m.Nombre;
                    existente.id_marca = m.idMarca;
                    existente.id_relAspecto = m.RelAspecto.Id;
                    existente.id_tecnologia = m.Tecnologia.Id;
                    existente.tamaño = m.Tamanio;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoModeloMonitor(ModeloMonitor m)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    if (dbc.Modelo_Monitor.Any(c => c.descripcion == m.Nombre))
                    {
                        result.Detail = "Ya existe un modelo con ese nombre";
                        return Json(result);
                    }
                    Modelo_Monitor mod = new Modelo_Monitor
                    {
                        descripcion = m.Nombre,
                        id_tecnologia = m.Tecnologia.Id,
                        id_relAspecto=m.RelAspecto.Id,     
                        tamaño=m.Tamanio,
                        id_marca = m.idMarca
                    };                    
                    dbc.Modelo_Monitor.Add(mod);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        //fin modelo monitor
        public JsonResult GetDatosModelo(int idModelo)
        {
            ModeloImpresora pr = null;
            using (var dbc = new IncidenciasEntities())
            {
                pr = (from u in dbc.Modelo_Impresora
                      where u.id_modelo == idModelo
                      select new ModeloImpresora
                      {
                          id = u.id_modelo,
                          Nombre = u.descripcion,
                          Rendimiento = new RendimientoImpresora { Id = u.Rendimiento_impresora.id_rendimiento, Nombre = u.Rendimiento_impresora.descripcion },
                          Tecnologia = new TecnologiaPr
                          {
                              Id = u.Tecnologia_Impresoras.id_tecnologia,
                              Nombre = u.Tecnologia_Impresoras.descripcion
                          },
                          Tipo = new TipoPr { Id = u.Tipo_impresora.id_tipo, Nombre = u.Tipo_impresora.descripcion },
                      }).First();


                pr.Conectividad = (from u in dbc.Conectividad_impresora
                                   where u.Modelo_Impresora.Any(z => z.id_modelo == pr.id)
                                   select new ConectividadPr
                                   {
                                       id = u.id_conectividad,
                                       Nombre = u.descripcion
                                   }).ToList();
                pr.Insumos = (from u in dbc.Insumos
                              where u.Modelo_Impresora.Any(z => z.id_modelo == pr.id)
                              select new InsumoPr
                              {
                                  Nombre = u.descripcion,
                                  Id = u.id_insumo,
                                  idTipo = u.Tipo_insumo.id_tipo_insumo,
                                  Tipo = u.Tipo_insumo.descripcion
                              }).ToList();

            }//using
            return Json(pr);
        }
        public JsonResult NuevoImpresora(ImpresoraInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.UnidadConf where (u.nombre == e.Oblea |
                                  (e.NroSerie.Length>0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Impresora"))
                              select u).FirstOrDefault();

                if (encontrado == null)
                {
                    usuarioBean actual = Session["usuario"] as usuarioBean;
                    UnidadConf uc = new UnidadConf
                    {
                        nombre = e.Oblea,
                        id_jurisdiccion = e.Jurisdiccion.idJurisdiccion,
                        fecha = DateTime.Now,
                        id_tipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Impresora").Single().id_tipo,
                        observaciones = e.Observaciones,
                        nro_serie = e.NroSerie,                        
                        id_usuario = actual.IdUsuario
                    };
                    dbc.UnidadConf.Add(uc);
                    Impresoras pr = new Impresoras
                    {
                        id_uc = uc.id_uc,
                        id_dependencia = e.Dependencia.idDependencia,
                        id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Activo").Single().id_estado,
                        id_modelo = e.Modelo==null?(int?)null:Int32.Parse(e.Modelo)
                    };                    
                    dbc.Impresoras.Add(pr);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "El equipo con esa Oblea/Nro. de Serie ya existe";
                    return Json(result);
                }
            }

            return Json(result);
        }
        public JsonResult UpdateImpresora(ImpresoraInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Impresoras encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.Impresoras where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                    if (encontrado != null)
                    {

                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        //estado check primero si esta activo
                        IQueryable<EquiposXIncidencia> res = dbc.EquiposXIncidencia.Where(c => c.UnidadConf.id_uc == e.idEquipo);
                        if (res.Count() == 0)
                        {
                            encontrado.id_estado = e.Estado.idEstado;
                        }
                        encontrado.UnidadConf.id_jurisdiccion = e.Jurisdiccion.idJurisdiccion;
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        if (e.Modelo != null)
                        {
                            encontrado.id_modelo = Int32.Parse(e.Modelo);
                        }
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {
                        result.Detail = "Equipo No Encontrado!!";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        #endregion
        #region Monitores
        public JsonResult NuevoMonitor(MonitorInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.UnidadConf where (u.nombre == e.Oblea |
                                 (e.NroSerie.Length > 0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Monitor"))
                              select u).FirstOrDefault();

                if (encontrado == null)
                {
                    usuarioBean actual = Session["usuario"] as usuarioBean;
                    UnidadConf uc = new UnidadConf
                    {
                        nombre = e.Oblea,
                        id_jurisdiccion = e.Jurisdiccion.idJurisdiccion,
                        fecha = DateTime.Now,
                        id_tipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Monitor").Single().id_tipo,
                        observaciones = e.Observaciones,
                        nro_serie = e.NroSerie,
                        id_usuario = actual.IdUsuario
                    };
                    Monitores mn = new Monitores
                    {
                        id_uc = uc.id_uc,
                        id_dependencia = e.Dependencia.idDependencia,
                        id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Activo").Single().id_estado,
                        id_modelo = e.Modelo == null ? (int?)null : Int32.Parse(e.Modelo)
                    };
                    dbc.UnidadConf.Add(uc);
                    dbc.Monitores.Add(mn);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "El equipo con esa Oblea/Nro. de serie ya existe";
                    return Json(result);
                }
            }

            return Json(result);
        }
        public JsonResult UpdateMonitor(ImpresoraInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Monitores encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.Monitores where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                if (encontrado != null)
                {

                    encontrado.id_dependencia = e.Dependencia.idDependencia;
                    //estado check primero si esta activo
                    IQueryable<EquiposXIncidencia> res = dbc.EquiposXIncidencia.Where(c => c.UnidadConf.id_uc == e.idEquipo);
                    if (res.Count() == 0)
                    {
                        encontrado.id_estado = e.Estado.idEstado;
                    }
                    encontrado.UnidadConf.id_jurisdiccion = e.Jurisdiccion.idJurisdiccion;
                    encontrado.UnidadConf.nro_serie = e.NroSerie;
                    if (e.Modelo != null)
                    {
                        encontrado.id_modelo = Int32.Parse(e.Modelo);
                    }
                    encontrado.UnidadConf.observaciones = e.Observaciones;
                    //actualización gestión de cambios
                    var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                    if (ecx.Count() > 0)
                    {
                        var exfir = ecx.First();
                        if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                        {
                            ecx.First().fecha_actualizacion = DateTime.Now;
                        }
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "El equipo con esa Oblea ya existe";
                    return Json(result);
                }
            }
            return Json(result);
        }
        public JsonResult tecnologiaMonitores()
        {
            List<TecnologiaMn> items = new List<TecnologiaMn>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Tecnologia_monitores

                         select new TecnologiaMn
                         {
                             Id = u.id_tecnologia,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(items);
        }
        public JsonResult ModelosMonitor(int marca)
        {
            Responses result = new Responses();
            List<ModeloMonitor> modelos = new List<ModeloMonitor>();
            using (var dbc = new IncidenciasEntities())
            {
                modelos = (from u in dbc.Modelo_Monitor
                           where u.id_marca == marca
                           orderby u.descripcion
                           select new ModeloMonitor
                           {
                               id = u.id_modelo,
                               Nombre = u.descripcion,
                               RelAspecto = new RelAspecto { Nombre = u.Relacion_aspecto.descripcion, Id = u.Relacion_aspecto.id_relAspecto },
                               Tamanio = u.tamaño,
                               Tecnologia = new TecnologiaMn { Nombre = u.Tecnologia_monitores.descripcion, Id = u.Tecnologia_monitores.id_tecnologia }
                           }).ToList();

                return Json(modelos);
            }
        }

        #endregion
        #region Varios
        public JsonResult NuevoVarios(VariosInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.UnidadConf where (u.nombre == e.Oblea |
                                  (e.NroSerie.Length>0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Varios"))
                              select u).FirstOrDefault();

                if (encontrado == null)
                {
                    usuarioBean actual = Session["usuario"] as usuarioBean;
                    UnidadConf uc = new UnidadConf
                    {
                        nombre = e.Oblea,
                        id_jurisdiccion = e.Jurisdiccion.idJurisdiccion,
                        fecha = DateTime.Now,
                        id_tipo = e.TipoEquipo.id,
                        observaciones = e.Observaciones,
                        nro_serie = e.NroSerie,
                        id_usuario = actual.IdUsuario
                    };
                    Varios pr = new Varios
                    {
                        id_uc = uc.id_uc,
                        id_dependencia = e.Dependencia.idDependencia,
                        id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Activo").Single().id_estado,
                        id_marca = e.Marca.idMarca,
                        modelo = e.Modelo
                    };
                    dbc.UnidadConf.Add(uc);
                    dbc.Varios.Add(pr);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "El equipo con esa Oblea/Nro. de serie ya existe";
                    return Json(result);
                }
            }

            return Json(result);
        }
        public JsonResult UpdateVarios(VariosInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Varios encontrado = null;
            using (var dbc = new IncidenciasEntities())
            {
                encontrado = (from u in dbc.Varios where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                if (encontrado != null)
                {

                    encontrado.id_dependencia = e.Dependencia.idDependencia;
                    //estado check primero si esta activo
                    IQueryable<EquiposXIncidencia> res = dbc.EquiposXIncidencia.Where(c => c.UnidadConf.id_uc == e.idEquipo);
                    if (res.Count() == 0)
                    {
                        encontrado.id_estado = e.Estado.idEstado;
                    }
                    encontrado.UnidadConf.id_jurisdiccion = e.Jurisdiccion.idJurisdiccion;
                    encontrado.UnidadConf.nro_serie = e.NroSerie;
                    encontrado.modelo = e.Modelo;
                    encontrado.UnidadConf.observaciones = e.Observaciones;
                    encontrado.id_dependencia = e.Dependencia.idDependencia;
                    encontrado.id_marca = e.Marca.idMarca;
                    //actualización gestión de cambios
                    var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                    if (ecx.Count() > 0)
                    {
                        var exfir = ecx.First();
                        if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                        {
                            ecx.First().fecha_actualizacion = DateTime.Now;
                        }
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "El equipo con esa Oblea ya existe";
                    return Json(result);
                }
            }
            return Json(result);
        }
        #endregion
        public JsonResult BaseLine(int id)
        {
            List<LineaBaseVista> lineaB = new List<LineaBaseVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var basel = from u in dbc.BaseLines
                            where u.master_id == id
                            orderby u.fecha descending
                            select u;
                foreach (var b in basel)
                {
                    lineaB.Add(new LineaBaseVista
                    {
                        CreadoPor = b.Usuarios.nombre_completo,
                        Data = b.data,
                        Fecha = b.fecha
                    });
                }
            }
            return Json(lineaB);
        }
        public JsonResult SaveBaseLine(int id)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Pc": equipo = new PcInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Impresora": equipo = new ImpresoraInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Monitor": equipo = new MonitorInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Rack":equipo = new RackInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        default: equipo = new VariosInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                if (equipo != null)
                {
                    result = equipo.SaveLineaBase();
                }
                return Json(result);
            }
        }
    }
}
