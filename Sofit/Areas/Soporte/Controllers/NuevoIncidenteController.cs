﻿using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Soporte.Controllers
{
    [SessionActionFilter]
    public class NuevoIncidenteController : Controller
    {        
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/NuevoTicket");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }           
            NuevoIncidenteVista model = new NuevoIncidenteVista();
            BuilderTipifTree arbol = new BuilderTipifTree();
            arbol.construir("Soporte Técnico");
            using (var dbc = new IncidenciasEntities())
            {
                model.Jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new JurisdiccionVista
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                model.TipoDeTicket = (from u in dbc.TipoTicket
                                      select new TipoTicketVista
                                      {
                                          Id = u.id,
                                          Nombre = u.nombre
                                      }).ToList();
                model.Prioridades = (from u in dbc.PrioridadTicket
                                     select new PrioridadesTicketVista
                                     {
                                         Id = u.id,
                                         Nombre = u.nombre
                                     }).ToList();
            }
            model.Tipificaciones = arbol.getResult();
            return View("NuevoIncidente",model);
        }        
        public JsonResult nuevoIncidente(HttpPostedFileBase photo,HttpPostedFileBase archivo)
        {
            usuarioBean usuarioActual = (usuarioBean)Session["usuario"];
            bool nvoNro = false;
            Responses result = new Responses { Info="ok"};
            #region getData
            NameValueCollection nvc = Request.Form;
            string nroIncidente = nvc.Get("numero");
            string idTipificacion = nvc.Get("idTipificacion");
            int idTipoTicket = Int32.Parse(nvc.Get("tipoTicket"));
            int idPrioridad = Int32.Parse(nvc.Get("prioridadTicket"));
            string idDep = nvc.Get("idDependencia");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            string observaciones = nvc.Get("observaciones");
            string referentesString = nvc.Get("referentes");
            string nombreCliente=string.Empty;
            List<String> referentes = new List<string>();
            if (referentesString != null)
            {
                referentes=referentesString.Split(',').ToList();
            }
            if (!referentes.Contains(usuarioActual.email))
            {
                referentes.Add(usuarioActual.email);
            }
            int cliente = Int32.Parse(nvc.Get("cliente"));
            #endregion
            string pathImagen = string.Empty;
            string pathFile = string.Empty;
            string mime = string.Empty;
            #region numeroNuevo
            if (nroIncidente == "nuevo")
            {
                lock (result = IncidenciasManager.getNextNro())
                {                    
                    nvoNro = true;
                }
                using(var dbc=new IncidenciasEntities()){
                    nombreCliente=(from u in dbc.Contactos where u.id_contacto==cliente select u.nombre).Single();
                    var prefix=(from u in dbc.TipoTicket
                               where u.id==idTipoTicket
                               select u.prefijoActual).Single();
                    nroIncidente=prefix+result.Detail;
                }
            }
            #endregion
            #region archivo
            if(archivo!=null)
            {
                string nombreArchivo = archivo.FileName;
                mime = archivo.ContentType;
                string[] splitN = nombreArchivo.Split('.');
                string nombreFinal = nroIncidente + '_' + nombreArchivo;
                try
                {
                    pathFile = PathImage.getCustomFileDirectory(nombreFinal);
                    var stream = archivo.InputStream;
                    using (var fileStream = System.IO.File.Create(pathFile))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion
            #region foto
            if (photo != null)
            {
                string nombreArchivo = photo.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string nombreFinal = nroIncidente + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getCustomImagenDefault(nombreFinal);
                    var stream = photo.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion
            #region creacion incidencia
            if (result.Info == "ok")
            {                
                IncidenciasManager manager = new IncidenciasManager();
                IDirecciones soporte = DireccionesFactory.getDireccion("Soporte Técnico");
                result = manager.crearIncidencia(nroIncidente, Int16.Parse(idTipificacion), Int16.Parse(idDep), idUsuario,observaciones, soporte,idTipoTicket,idPrioridad,pathImagen,pathFile,mime,cliente,nvoNro);
                if (result.Info == "ok") nroIncidente = result.Detail;
            }
            #endregion
            #region mails a referentes
            if (referentesString != null & result.Info=="ok")
            {
                string nombreTipificacion = string.Empty;
                int idtip=Int16.Parse(idTipificacion);
                using(var dbc=new IncidenciasEntities())
                {
                    nombreTipificacion = (from t in dbc.Tipificaciones
                                          where t.id_tipificacion == idtip
                                          select t.nombre).Single();
                }
                bool huboError = false;
                StringBuilder sb = new StringBuilder();
                string subject = "Ticket Generado con el número "+nroIncidente;
                string body = "Fue Generado un Ticket con la identificación <b>" + nroIncidente + "</b> a pedido de <b>" + nombreCliente +
                    ",</b> el día " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " hrs. " +
                    ", tipificado como " + nombreTipificacion + ", y se encuentra pendiente de resolución.";
                    
                EmailSender sender = new EmailSender();
                foreach (string referente in referentes)
                {
                    Referentes referenteBuscado;
                    string direccionMail = string.Empty;
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(referente);
                    if (match.Success)
                    {
                        direccionMail = referente;
                    }
                    else
                    {
                        try
                        {
                            int idRef = 0;
                            Int32.TryParse(referente, out idRef);
                            using (var dbc = new IncidenciasEntities())
                            {
                                referenteBuscado = (from t in dbc.Referentes
                                                    where t.id == idRef
                                                    select t).Single();
                            }
                            direccionMail = referenteBuscado.email;
                        }
                        catch (Exception e)
                        {
                            huboError = true;
                            sb.Append(referente).Append(". ");
                            continue;
                        }
                    }
                    //envio         
                    result = sender.sendEmail(new string[] { direccionMail, subject, body });
                    if (result.Info == "ok")
                    {
                        using (var dbc = new IncidenciasEntities())
                        {
                            var noti = new NotificacionXIncidente
                            {
                                email = direccionMail,
                                id_incidencia = IncidenciasManager.LastId
                            };
                            dbc.NotificacionXIncidente.Add(noti);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }//contexto dbc
                    }//envio exitoso
                    else
                    {                        //error envío
                        huboError = true;
                        sb.Append(referente).Append(". ");
                    }
                }//iteracion referentes
                
                if (huboError)
                {
                    result.Info = "con errores";
                    result.Detail = sb.ToString();
                }
                
            }//envio mails
            #endregion mails
            if (result.Info == "ok") result.Detail = nroIncidente;
            return Json(result);
        }
    }
}
