﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using soporte.Controllers;
using soporte.Areas.Soporte.Models;
using soporte.Models.Statics;
using soporte.Models;
using soporte.Areas.Soporte.Models.Displays;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.Displays;
using System.Diagnostics;

namespace soporte.Areas.Soporte.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        public ActionResult Index()
        {            
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }       
            SoporteModel model = new SoporteModel();
            List<IncTeleVista> incTotales = new List<IncTeleVista>();
            List<IncTeleVista> incIngresados = new List<IncTeleVista>();
            List<IncTeleVista> incAtUsuarios = new List<IncTeleVista>();
            List<IncTeleVista> incLogistica = new List<IncTeleVista>();
            List<EquipoSoporteVista> equiposTotales = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposClonado = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposLaboratorio = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposSuspendidos = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposEntregar = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposAvisoGarantia = new List<EquipoSoporteVista>();
            List<EquipoSoporteVista> equiposEnGarantia = new List<EquipoSoporteVista>();            
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<DireccionVista> subdir = new List<DireccionVista>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Tecnico> tecnicoAtUsuarios = new List<Tecnico>();
            List<Tecnico> tecnicoLogistica = new List<Tecnico>();
            List<Area> areas = new List<Area>();
            List<TipoResolucion> tResClon = new List<TipoResolucion>();
            List<TipoResolucion> tResLab = new List<TipoResolucion>();
            List<TipoResolucion> tResAt = new List<TipoResolucion>();
            List<TipoResolucion> tResLog = new List<TipoResolucion>();
            List<TipoResolucion> tResDep = new List<TipoResolucion>();
            List<ImagenesClonadoVista> imag = new List<ImagenesClonadoVista>();
            List<Turno> turnos = new List<Turno>();
            List<Ubicacion> ubicaciones = new List<Ubicacion>();
            Tiempos tiempos = new Tiempos();
            IDirecciones soporteDir = DireccionesFactory.getDireccion("Soporte Técnico");
            using (var dbc = new IncidenciasEntities())
            {

                //Debug.Write("antes de queries: " + DateTime.Now + "\n");
                equiposTotales = (from d in dbc.selectEquiposActivosSoporte(soporteDir.IdDireccion)
                                  select new EquipoSoporteVista
                                  {
                                      IdIncidente = d.id_incidencias,
                                      idArea = d.id_area,
                                      Oblea = d.nombre,
                                      FechaDesde = d.fecha_inicio,
                                      idEstado = d.id_estado,
                                      IdEquipo = d.id_uc,
                                      NroIncidente=d.numero,
                                      Notificacion = "",
                                      Dependencia=d.Jurisdiccion,
                                      Tecnico = d.descripcion,
                                      TieneNotificacion=d.noti.Value,
                                      Ubicacion=d.ubicacion,
                                      Prioridad=d.Prioridad
                                  }).ToList();
                
                incTotales = (from d in dbc.selectIncidenciasActivas(soporteDir.IdDireccion)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value,
                                  EstuvoEsperando=d.esperando.Value
                              }).ToList();               
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Soporte Técnico"
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Soporte Técnico" & u.activo.Value & u.id_usuario.HasValue
                            orderby u.descripcion
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
                tecnicoAtUsuarios = (from u in dbc.tecnico
                                     where u.area.Direcciones.nombre == "Soporte Técnico" & u.descripcion == "At.Usuarios"
                                     select new Tecnico
                                     {
                                         idTecnico = u.id_tecnico,
                                         Nombre = u.descripcion
                                     }).ToList();
                tecnicoLogistica = (from u in dbc.tecnico
                                    where u.area.Direcciones.nombre == "Soporte Técnico" & u.descripcion == "Logística"
                                    select new Tecnico
                                    {
                                        idTecnico = u.id_tecnico,
                                        Nombre = u.descripcion
                                    }).ToList();
                turnos = (from u in dbc.turno
                          select new Turno
                          {
                              idTurno = u.id_turno,
                              Nombre = u.descripcion
                          }).ToList();
                tResAt = (from u in dbc.tipo_resolucion
                          where u.area.descripcion == "At.Usuarios"
                          orderby u.descripcion
                          select new TipoResolucion
                          {
                              idTipoResolucion = u.id_tipo_resolucion,
                              Nombre = u.descripcion
                          }).ToList();
                tResClon = (from u in dbc.tipo_resolucion
                            where u.area.descripcion == "Clonado"
                            orderby u.descripcion
                            select new TipoResolucion
                            {
                                idTipoResolucion = u.id_tipo_resolucion,
                                Nombre = u.descripcion
                            }).ToList();
                tResLab = (from u in dbc.tipo_resolucion
                           where u.area.descripcion == "Laboratorio"
                           orderby u.descripcion
                           select new TipoResolucion
                           {
                               idTipoResolucion = u.id_tipo_resolucion,
                               Nombre = u.descripcion
                           }).ToList();
                tResLog = (from u in dbc.tipo_resolucion
                           where u.area.descripcion == "Logística"
                           orderby u.descripcion
                           select new TipoResolucion
                           {
                               idTipoResolucion = u.id_tipo_resolucion,
                               Nombre = u.descripcion
                           }).ToList();
                tResDep = (from u in dbc.tipo_resolucion
                           where u.area.descripcion == "Depósito"
                           orderby u.descripcion
                           select new TipoResolucion
                           {
                               idTipoResolucion = u.id_tipo_resolucion,
                               Nombre = u.descripcion
                           }).ToList();
                subdir = (from u in dbc.Direcciones
                          select new DireccionVista
                          {
                              idDireccion = u.id_direccion,
                              Nombre = u.nombre
                          }).ToList();
                imag = (from u in dbc.Imagenes
                        select new ImagenesClonadoVista
                        {
                            idImagen = u.id_imagen,
                            Nombre = u.nombre
                        }).ToList();
                ubicaciones = (from u in dbc.UbicacionProducto
                               select new Ubicacion
                               {
                                   Id = u.id_ubicacion,
                                   Descripcion = u.descripcion,
                                   Nombre = u.nombre
                               }).ToList();
                model.PrioridadTickets = dbc.PrioridadTicket.Select(c => new PrioridadesTicketVista { Id = c.id, Nombre = c.nombre }).ToList();
            }

                incIngresados = incTotales.Where(c => (c.Area.NombreArea.Equals("Soporte") & c.Estado != "Suspendido")).ToList();
                incAtUsuarios = incTotales.Where(c => c.Area.NombreArea.Equals("At.Usuarios") & c.Estado != "Suspendido").ToList();
                incLogistica = incTotales.Where(c => (c.Area.NombreArea.Equals("Logística") | c.Estado == "Suspendido")).ToList();               
                equiposClonado = equiposTotales.Where(c => c.Area.nombreArea.Equals("Clonado")).ToList();
                equiposLaboratorio = equiposTotales.Where(c => c.Area.nombreArea.Equals("Laboratorio")).ToList();
                equiposEntregar = equiposTotales.Where(c => c.Estado.nombreEstado.Equals("Para Entregar")).ToList();
                equiposSuspendidos = equiposTotales.Where(c => c.Estado.nombreEstado.Equals("Suspendido") | c.Estado.nombreEstado.Equals("Para Notificar") | c.Estado.nombreEstado.Equals("Para Ubicar")).ToList();
                equiposAvisoGarantia = equiposTotales.Where(c => c.Estado.nombreEstado.Equals("Para Garantia")).ToList();
                equiposEnGarantia = equiposTotales.Where(c => c.Estado.nombreEstado.Equals("En Garantia")).ToList();

                //Debug.Write("antes de iteraciones incidentes: " + DateTime.Now + "\n");
            
            foreach (IncTeleVista t in incIngresados)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);                
            }
            foreach (IncTeleVista t in incAtUsuarios)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incLogistica)
            {
                t.calcularEstadoServicioYSuspensión(tiempos.tCriticoIncidente,tiempos.tMaxSuspension);
            }
            foreach (EquipoSoporteVista e in equiposClonado)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
               
                if (diff.Days >= tiempos.tEClon) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tAClon) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                if (e.TieneNotificacion)
                {
                    e.Notificacion = "Notificado";
                }
                else
                {
                    e.Notificacion = "Sin Notificar";
                }    
            }
            foreach (EquipoSoporteVista e in equiposLaboratorio)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
                if (diff.Days >= tiempos.tELab) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tALab) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                if (e.TieneNotificacion)
                {
                    e.Notificacion = "Notificado";
                }
                else
                {
                    e.Notificacion = "Sin Notificar";
                }    
            }
           //Debug.Write("Despues de iteraciones incidentes: " + DateTime.Now+"\n");
            #region iteracion equipos suspendidos
            foreach (EquipoSoporteVista e in equiposSuspendidos)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
                if (diff.Days >= tiempos.tESus) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tASus) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                if (e.TieneNotificacion)
                {
                    if (e.Estado.nombreEstado == "Para Notificar") e.Notificacion = "ReNotificar";
                    else e.Notificacion = "Notificado";
                }
                else
                {
                    e.Notificacion = "Sin Notificar";
                }          
                
            }
            #endregion
            #region iteracion equipos entregar
            foreach (EquipoSoporteVista e in equiposEntregar)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
                if (diff.Days >= tiempos.tEDel) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tADel) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                using (var dbc = new IncidenciasEntities())
                {
                    var notis = from u in dbc.NotificacionSoporte
                                where u.id_incidencia == e.IdIncidente & u.id_equipo == e.IdEquipo
                                select u;
                    if (notis.Count() > 0)
                    {
                        e.Notificacion = "Notificado";
                    }
                    else
                    {
                        e.Notificacion = "Sin Notif.";
                    }
                }
            }
            #endregion
            #region Iteracion equipos aviso garantia
            foreach (EquipoSoporteVista e in equiposAvisoGarantia)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
                if (diff.Days >= tiempos.tEAGar) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tAAGar) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                using (var dbc = new IncidenciasEntities())
                {
                    var notis = from u in dbc.NotificacionSoporte
                                where u.id_incidencia == e.IdIncidente & u.id_equipo == e.IdEquipo
                                select u;
                    if (notis.Count() > 0)
                    {
                        e.Notificacion = "Notificado";
                    }
                    else
                    {
                        e.Notificacion = "Sin Notif.";
                    }
                }
            }
            #endregion
            #region Iteración de Equipos En Garantia
            foreach (EquipoSoporteVista e in equiposEnGarantia)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = BusinessDays.GetBusinessTime(e.FechaDesde, now);
                if (diff.Days >= tiempos.tEEGar) e.EstadoIcono = "nook";
                else
                {
                    if (diff.Days >= tiempos.tAEGar) e.EstadoIcono = "nok";
                    else e.EstadoIcono = "ok";
                }
                e.Observacion = diff.Days.ToString() + " días han pasado sin solución";
                using (var dbc = new IncidenciasEntities())
                {
                    var notis = from u in dbc.NotificacionSoporte
                                where u.id_incidencia == e.IdIncidente & u.id_equipo == e.IdEquipo
                                select u;
                    if (notis.Count() > 0)
                    {
                        e.Notificacion = "Notificado";
                    }
                    else
                    {
                        e.Notificacion = "Sin Notif.";
                    }
                }
            }
            #endregion
            //Debug.Write("Despues de iteraciones: " + DateTime.Now);
            model.IncidentesIngresados = incIngresados;
            model.IncidentesAtUsuarios = incAtUsuarios;
            model.IncidentesLogistica = incLogistica;
            model.EquiposClonado = equiposClonado;
            model.EquiposLaboratorio = equiposLaboratorio;
            model.EquiposParaEntregar = equiposEntregar;
            model.EquiposSuspendidos = equiposSuspendidos;
            model.EquiposAvisoGarantia = equiposAvisoGarantia;
            model.EquiposEnGarantia = equiposEnGarantia;
            model.TiposResolucionClonado = tResClon;
            model.TiposResolucionLaboratorio = tResLab;
            model.TiposResolucionAtUs = tResAt;
            model.TiposResolucionLogistica = tResLog;
            model.TiposResolucionDepósito = tResDep;
            model.Turnos = turnos;
            model.SubDirecciones = subdir;
            model.Areas = areas.Where(u => u.Nombre != "Telecomunicaciones").ToList();
            model.Tecnicos = tecnicos;
            model.TecnicoAtUsuarios = tecnicoAtUsuarios;
            model.TecnicoLogistica = tecnicoLogistica;
            model.Jurisdicciones = jurisdicciones;
            model.Imagenes = imag;
            model.Ubicaciones = ubicaciones;
            //Debug.Write("Antes del fin: " + DateTime.Now + "\n");
            return View(model);
        }
        public JsonResult pathImagen(int id)
        {
            string pathDirectorio = "/Content/img/Photo/";
            string pathImagen=string.Empty;
            using (var dbc = new IncidenciasEntities())
            {                
                var imageninc = dbc.Incidencias.Where(c=>c.id_incidencias==id).First().ImagenIncidencia;
                pathImagen = imageninc.First().path;
            }
            string nombreArchivo = pathImagen.Split('\\').Last();
            pathDirectorio += nombreArchivo;
            return Json(new Responses { 
                 Info="ok",
                 Detail=pathDirectorio
            });
        }
        
        public JsonResult EstablecerUbicacion(string idEquipo,int idArmario)
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            string[] idsEquipo = idEquipo.Split(',');
            foreach (string idEq in idsEquipo)
            {
                int idE = Int32.Parse(idEq);
                EquipoEnIncidente eq = new EquipoEnIncidente();
                eq.idEquipo = idE;
                
                result = eq.Ubicar(idArmario);
                if (result.Info != "ok")
                {
                    result.Detail += "Error ubicando " + eq.nroOblea + " ";
                    break;
                }
            }           
            return Json(result);
        }
        public JsonResult derivarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idAreaDestino = Int32.Parse(nvc.Get("idAreaDestino"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idAreaDestino);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, true, false, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.derivar(areaDestino.IdArea, idTecnico == 0 ? areaDestino.getIdTecnicoGenerico() : idTecnico, idUsuario);
            return Json(result);
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.numero.StartsWith(prefixText)&u.area.Direcciones.nombre=="Soporte Técnico"
                         select u.Incidencias.numero).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult getEquiposDeIncidente(string incidente)
        {
            List<EquipoInfo> equipos = new List<EquipoInfo>();
            using (var dbc = new IncidenciasEntities())
            {
                equipos = (from u in dbc.EquiposXIncidencia
                           where u.Incidencias.numero == incidente
                           select new EquipoInfo
                           {
                               Oblea = u.UnidadConf.nombre,
                               Area = u.area.descripcion,
                               Estado = u.Estado_UC.descripcion,
                               Tecnico = u.tecnico.descripcion
                           }).ToList();               
            }
            return Json(equipos);
        }
        public JsonResult GetEquiposActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquiposXIncidencia
                         where u.UnidadConf.nombre.StartsWith(prefixText) & u.area.Direcciones.nombre == "Soporte Técnico"
                         select u.UnidadConf.nombre).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult buscarEqOInc(string texto)
        {
            DatosUbicacion resp = new DatosUbicacion();

            using (var dbc = new IncidenciasEntities())
            {
                if (texto.ToLower().StartsWith("cba") | texto.ToLower().StartsWith("ln") | 
                    texto.ToLower().StartsWith("sol") | texto.ToLower().StartsWith("inc") | 
                    texto.ToLower().StartsWith("prb"))
                {

                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.numero == texto// & u.area.Direcciones.nombre == "Soporte Técnico"
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente",
                                Fecha = u.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Desc = u.Incidencias.descripcion

                            }).FirstOrDefault();

                }
                else
                {
                    resp = (from u in dbc.EquiposXIncidencia
                            where u.UnidadConf.nombre == texto
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_UC.descripcion,
                                Numero = u.UnidadConf.nombre,
                                Fecha = u.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Desc = u.Incidencias.descripcion,
                                Tipo = "Equipo"
                            }).FirstOrDefault();                  
                }
            }
            if (resp == null) resp = new DatosUbicacion();
            return Json(resp);
        }        
        
        public JsonResult derivarEquipos(HttpPostedFileBase file)
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();

            if (nvc.Count > 1)
            {
                usuarioBean user = Session["usuario"] as usuarioBean;
                int id_eq = Int32.Parse(nvc.GetValues("id_Eq")[0]);
                int id_inc = Int32.Parse(nvc.GetValues("id_inc")[0]);
                int idTec = Int32.Parse(nvc.Get("idTec"));
                int area = Int32.Parse(nvc.Get("area"));
                string nroIncidente = nvc.Get("nroInc");
                string observaciones = nvc.Get("obs");
                AreaEquipo a=AreaEquipoFactory.getAreaXId(area);
                if (idTec == 0)
                {
                    idTec = a.getTecnicoGenerico();
                }
                IncidenciaBuilder builder = new IncidenciaBuilder();
                builder.Construir(id_inc, false, true, true);
                Incidencia incidente = builder.getResult();
                result = incidente.derivarEquipo(id_eq, a, observaciones, user.IdUsuario, idTec);
            }

            return Json(result);
        }
        public JsonResult derivarADireccion(int idDir, int idInc)
        {
            Responses result = new Responses();            
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false, false);
            Incidencia inc = builder.getResult();
            result = inc.derivarIncidenciaADireccion(idDir, idUsuario);
            return Json(result);
        }
        public JsonResult eliminarIncidente(int idInc)
        {
            Responses result = new Responses();
            IncidenciasManager mgr = new IncidenciasManager();
            result=mgr.eliminarIncidente(idInc);
            return Json(result);
        }
        public JsonResult terminarEquipo()
        {
            NameValueCollection nvc = Request.Form;

            string nroInc = nvc.GetValues("nroInc")[0];
            int id_eq = Int32.Parse(nvc.GetValues("idEq")[0]);
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;            
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc,true,true,true);
            Incidencia incidente = builder.getResult();
            Responses result = incidente.terminarEquipo(id_eq,idUsuario);
            return Json(result);
        }
        public JsonResult Notificaciones(string nros)
        {
            Responses result = new Responses();
            List<NotificacionSoporte> notis = null;
            string[] res = nros.Split(',');
            int idEquipo = Int32.Parse(res[0]);
            int idIncidente = Int32.Parse(res[1]);
            List<NotifDisplay> notificaciones = new List<NotifDisplay>();
            using (var dbc = new IncidenciasEntities())
            {
                notis = (from s in dbc.NotificacionSoporte
                         where s.id_equipo == idEquipo & s.id_incidencia == idIncidente
                         select s).ToList();
                foreach (NotificacionSoporte n in notis)
                {
                    List<string> mails = (from m in n.NotMailRefSoporte
                                          where m.id_notificacion == n.id
                                          select m.direccion_mail).ToList();

                    NotifDisplay no = new NotifDisplay()
                    {
                        mailsAdd = mails,
                        fecha = n.fecha.ToShortDateString(),
                        mensaje = n.mensaje,
                        enviadoPor = n.Usuarios.nombre_completo
                    };

                    notificaciones.Add(no);
                }
            }
            return Json(notificaciones);            
        }
        public JsonResult incidentesToLife(string nroInc)
        {
            IncidenciasManager im = new IncidenciasManager();
            IDirecciones soporte = DireccionesFactory.getDireccion("Soporte Técnico");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            return Json(im.reanimarIncidencia(nroInc, idUsuario, soporte));
        }
        public JsonResult cierreTotal()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            string nroInc = nvc.GetValues("nroInc")[0];
            string fechaInicio = nvc.GetValues("fecha")[0];
            string tipif = nvc.GetValues("tipif")[0];
            string jurisd = nvc.GetValues("jurisd")[0];
            string area = nvc.GetValues("area")[0];
            string estado = nvc.GetValues("estado")[0];
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, true, true, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.cerrarIncidencia(idUsuario);
            return Json(result);
        }
        public JsonResult solucionarIncidente()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            string nroInc = nvc.GetValues("nroInc")[0];
            string fechaInicio = nvc.GetValues("fecha")[0];
            string tipif = nvc.GetValues("tipif")[0];
            string jurisd = nvc.GetValues("jurisd")[0];
            string area = nvc.GetValues("area")[0];
            string estado = nvc.GetValues("estado")[0];
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, true,true,false);
            Incidencia incidencia = builder.getResult();
            if (incidencia.EstadoActual is EstadoIncidenciaSuspendido)
            {
                result.Info = "no";
                result.Detail = "El Ticket se encuentra suspendido.";
            }
            else
            {
                result = verificarResoluciones(incidencia);
                if (result.Info == "ok")
                {
                    if (incidencia.EquiposDecorator.tieneEquiposActivos())
                    {
                        result.Info = "no";
                        result.Detail = "El Ticket tiene Elementos Activos: " + incidencia.EquiposDecorator.getEquiposSinTerminar();
                    }
                    else
                    {
                        result = verificarPedidosVigentes(incidencia.Id);
                        if (result.Info == "ok")
                        {
                            int idTecnico = 0;
                            using (var dbc = new IncidenciasEntities())
                            {
                                idTecnico = dbc.tecnico.Where(c => c.id_usuario == idUsuario).Single().id_tecnico;
                            }
                            IEstadoIncidencia nuevoE = incidencia.EstadoActual = EstadoIncidenciaFactory.getEstado("Para Cerrar");
                            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea("Soporte");
                            incidencia.cambioArea(areaDestino, areaDestino.getIdTecnicoGenerico(), idUsuario);
                            incidencia.cambioEstado(nuevoE, idTecnico, idUsuario);
                        }
                    }
                }
            }
            return Json(result);
        }
        private Responses verificarPedidosVigentes(int Id)
        {
            Responses result = new Responses();
            bool flagCheck = true;
            using (var dbc = new IncidenciasEntities())
            {
                var pV = dbc.PedidosSeguridad.Where(c => c.id_ticket == Id);
                foreach (var p in pV)
                {
                    if (p.EstadosPedidos.nombre != "Rechazado" & p.EstadosPedidos.nombre != "Realizado") flagCheck = false;
                }
            }
            if (flagCheck) result.Info = "ok";
            else result.Detail = "Este ticket tiene pedidos de acceso a servicios informáticos que aún no han sido resueltos";
            return result;
        }
        public Responses verificarResoluciones(Incidencia inc)
        {
            Responses result = new Responses();
            inc.AreasActuantes.Sort();
            MovAreaIncidencia ultimaArea = inc.AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "La Incidencia no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    result.Info = "ok";
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar la resolución del Área: " + ultimaArea.Area.NombreArea;
                }
            }
            return result;
        }
        public JsonResult buscarResolucionesEquipo(int idEquipo, int idIncidente)
        {
            List<ResToDisplay> resToDisplay = new List<ResToDisplay>();
            using (var dbc = new IncidenciasEntities())
            {
                resToDisplay = (from u in dbc.Resoluciones
                                where u.id_uc == idEquipo & u.id_incidencia == idIncidente
                                select new ResToDisplay
                                {
                                    Fecha = u.fecha,
                                    Observ = u.observaciones,
                                    Tecnico = u.tecnico.descripcion,
                                    TipoRes = u.tipo_resolucion.descripcion,
                                    Archivo = u.path_imagen != null ? u.path_imagen : ""
                                }).ToList();
            }
            resToDisplay.Sort();
            return Json(resToDisplay);
        }
        public JsonResult buscarDatosEquipo(int idEq, int idInc)
        {
            InfoNotificacion info = new InfoNotificacion();
            using (var dbc = new IncidenciasEntities())
            {
                var equipo = (from u in dbc.UnidadConf
                              where u.id_uc == idEq
                              select u).First();
                info.idEquipo = equipo.id_uc;
                info.NroSerie = equipo.nro_serie;
                info.oblea = equipo.nombre;
                
                switch (equipo.Tipo_equipo.descripcion)
                {
                    case "Impresora": Impresoras estaImp=equipo.Impresoras.Where(c => c.id_uc == equipo.id_uc).First();
                        if (estaImp.Modelo_Impresora != null)
                        {
                            info.Modelo = estaImp.Modelo_Impresora.descripcion;
                            info.Marca = estaImp.Modelo_Impresora.Marca.descripcion;                            
                        }
                        info.idDependencia = estaImp.id_dependencia;
                        break;
                    case "Monitor": Monitores esteMon = equipo.Monitores.Where(c => c.id_uc == equipo.id_uc).First();
                        if (esteMon.Modelo_Monitor != null)
                        {
                            info.Modelo = esteMon.Modelo_Monitor.descripcion;
                            info.Marca = esteMon.Modelo_Monitor.Marca.descripcion;
                        }
                        info.idDependencia = esteMon.id_dependencia;
                        break;
                    case "Pc": Pc estaPc = equipo.Pc.Where(c => c.id_uc == equipo.id_uc).First();
                        info.Modelo = estaPc.modelo != null ? estaPc.modelo : "N/A";
                        info.idDependencia = estaPc.id_dependencia;
                        info.Marca = estaPc.Marca != null ? estaPc.Marca.descripcion : "N/A";
                        break;
                    case "Varios": Varios esteVarios = equipo.Varios.Where(c => c.id_uc == equipo.id_uc).First();
                        info.idDependencia = esteVarios.id_dependencia.Value;
                        break;
                }
                info.resoluciones = (from u in dbc.Resoluciones
                                   where u.id_uc == idEq && u.id_incidencia == idInc
                                   select new ResToDisplay
                                   {
                                       Fecha = u.fecha,
                                       Observ = u.observaciones,
                                       Tecnico = u.tecnico.descripcion,
                                       TipoRes = u.tipo_resolucion.descripcion
                                   }).ToList();
                info.referentes = (from r in dbc.Referentes.Include("Dependencia").Include("Jurisdiccion")
                                   where r.Dependencia.Any(s => s.id_dependencia == info.idDependencia) |
                                   r.Jurisdiccion.Any(o => o.Dependencia.Any(p => p.id_dependencia == info.idDependencia))
                                   select new ReferentesVista
                                   {
                                       id = r.id,
                                       nombre = r.nombre,
                                       email = r.email,
                                       te = r.te
                                   }).ToList();
                info.Firma = (Session["usuario"] as usuarioBean).Firma;
            }          
            
            return Json(info);        
        }
        public JsonResult getHistoriaEquipo(int idEquipo)
        {
            HistEquipoBuilder builder = new HistEquipoBuilder();
            builder.ConstruirHistoricoCorto(idEquipo);
            HistoricoEquipo historico = builder.getResult();
            return Json(historico);     
        }              
        public JsonResult notificarEquipos()
        {            
            usuarioBean user = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idEquipo=Int32.Parse(nvc.Get("idEquipo"));
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            string nroIncidente = nvc.Get("nroIncidente");
            string referentes = nvc.Get("referentes");
            referentes = referentes.Substring(referentes.IndexOf('=')+1);
            string [] parsed=referentes.Split(',');            
            string firma = user.Firma;
            string cuerpo = nvc.Get("body");
            string subject = nvc.Get("subject");
            //--
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, false, true, false);
            Incidencia incidencia = builder.getResult();
            //--
            result = incidencia.notificacionEquipo(idEquipo, parsed, firma, cuerpo, subject);          
            return Json(result);
        }
        public JsonResult cargarSolucionesEquipo(HttpPostedFileBase fileupload)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            string oblea = nvc.Get("oblea");
            string nroIncidente = nvc.Get("nroIncidente");
            int tResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int tecnico = Int32.Parse(nvc.Get("idTecnico"));
            int turno = Int32.Parse(nvc.Get("idTurno"));
            int idIncidente = Int32.Parse(nvc.Get("idIncidente"));
            int idEquipo = Int32.Parse(nvc.Get("idEquipo"));
            string observacion = nvc.Get("observaciones");
            //Get the Data
            //string fecha = DateTime.Now.ToShortDateString();
            //string tabla = nvc.Get("nombreGrilla");
            bool sobrescribir = false;
            Boolean.TryParse(nvc.Get("sobrescribir"),out sobrescribir);
            int? imagen = null;
            try
            {
                imagen = Int32.Parse(nvc.Get("idImagen"));
            }
            catch (Exception) { };

            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente, false, true, true);
            Incidencia inc = builder.getResult();
            string nombreFile = string.Empty;
            
            int largo = observacion.Length;
            bool hayFile = fileupload != null;
            #region foto
            if (hayFile)
            {
                string pathImagen = string.Empty;
                string nombreArchivo = fileupload.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string ran = new Random().Next(10000).ToString("0000");
                nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getResolucionCustom(nombreFile);
                    var stream = fileupload.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion
            result = inc.cargarSolucionEquipo(usuario.IdUsuario, idEquipo, tResolucion, tecnico, turno, imagen,nombreFile, observacion,sobrescribir);
            return Json(result);
        }
        public JsonResult cargarSoluciones(HttpPostedFileBase fileupload)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            //Get the Data
            //string fecha = DateTime.Now.ToShortDateString();            
            //string tabla = nvc.Get("nombreGrilla");
            //string oblea = nvc.Get("oblea");            
            //string nroIncidente = nvc.Get("nroIncidente");
            int tResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int tecnico = Int32.Parse(nvc.Get("idTecnico"));
            int turno = Int32.Parse(nvc.Get("idTurno"));
            int idIncidente = Int32.Parse(nvc.Get("idIncidente"));
            int idEquipo = 0;
            Int32.TryParse(nvc.Get("idEquipo"), out idEquipo);            
            string observacion = nvc.Get("observaciones");
            //sobrescribir ultima
            bool sobrescribir = false;
            sobrescribir = Boolean.Parse(nvc.Get("sobrescribir"));
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente, sobrescribir, false, false);
            //            
            Incidencia inc = builder.getResult();
            bool hayFile = fileupload != null;
            string nombreFile = string.Empty;            
            
            int largo = observacion.Length;
            
            #region foto
            if (hayFile)
            {
                  string pathImagen = string.Empty;
                  string nombreArchivo = fileupload.FileName;
                  string[] splitN = nombreArchivo.Split('.');
                  string ran = new Random().Next(10000).ToString("0000");
                  nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                  try
                  {
                       pathImagen = PathImage.getResolucionCustom(nombreFile);
                       var stream = fileupload.InputStream;
                       using (var fileStream = System.IO.File.Create(pathImagen))
                       {
                           stream.CopyTo(fileStream);
                       }
                       result.Info = "ok";
                  }
                  catch (Exception e)
                  {
                       result.Detail = e.Message;
                  }            
            }
            #endregion
                      
            //buscar equipos
            bool flag = true;
            List<int> idEquipos = new List<int>();
            bool esParaEquipo = Boolean.Parse(nvc.Get("esParaEquipo"));
            if (esParaEquipo)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("idEquipo")) idEquipos.Add(Int32.Parse(nvc.GetValues(key)[0]));
                }
                if (idEquipos.Count == 0)
                {
                    result.Detail = "No se cargaron las obleas del equipo.";
                    flag = false;
                }
                if (flag)
                {
                    foreach (int id in idEquipos)
                    {
                        result=inc.cargarSolucion(usuario.IdUsuario, id, tResolucion, tecnico, turno, observacion, DateTime.Now,nombreFile,sobrescribir);
                    }                    
                }
            }
                //no hay equipos
            else
            {
                result = inc.cargarSolucion(usuario.IdUsuario, null, tResolucion, tecnico, turno, observacion, DateTime.Now,nombreFile,sobrescribir);
            }           
            return Json(result);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var tecnico = (from u in dbc.tecnico
                                   where u.descripcion == Tecnico
                                   select u).SingleOrDefault();
                    foreach (string strId in idIncidencia)
                    {
                        int idIncidente = Int32.Parse(strId);
                        var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                        incActiva.id_tecnico = tecnico.id_tecnico;
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult setTecnicoAEquipo(string id_Eq, string tecnico)
        {
            Responses result = new Responses();
            string[] idES = id_Eq.Split(',');
            using(var dbc=new IncidenciasEntities())
            {
                int idTecnico = dbc.tecnico.Where(c => c.descripcion == tecnico).First().id_tecnico;
                foreach (string idEq in idES)
                {
                    int idEquipoParsed=Int32.Parse(idEq);
                    EquiposXIncidencia exi = (from u in dbc.EquiposXIncidencia
                                              where u.id_equipo == idEquipoParsed
                                              select u).Single();
                    exi.id_tecnico = idTecnico;                    
                }
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public JsonResult quitarEquiposAInc()
        {
            Responses result;
            string equipo = Request.Form["oblea"];
            int incidente = Int32.Parse(Request.Form["incidente"]);
            string nroIncidente = Request.Form["nroIncidente"];
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(incidente, false, true, false);
            Incidencia incidencia = builder.getResult();      
            result = incidencia.quitarEquipoaIncidente(equipo);
            return Json(result);
        }
        public JsonResult agregarEquiposaInc()
        {
            Responses result = new Responses();
            string oblea = Request.Form["oblea"];
            int idIncidente = Int32.Parse(Request.Form["incidente"]);
            int id_area = Int32.Parse(Request.Form["idArea"]);
            int id_tecnico = Int32.Parse(Request.Form["idTecnico"]);
            if (id_tecnico == 0)
            {
                id_tecnico = AreaEquipoFactory.getAreaXId(id_area).getTecnicoGenerico();
            }
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente, false,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.agregarEquipoaIncidente(oblea, id_area,id_tecnico);
            return Json(result);
        }
        public JsonResult UltimoTecnico(int idEquipo, int idArea)
        {            
            Responses result = new Responses { Info = "No" };
            using (var dbc = new IncidenciasEntities())
            {
                var res = (from u in dbc.Resoluciones
                           where u.id_uc == idEquipo & u.id_area == idArea
                           orderby u.fecha descending
                           select u).FirstOrDefault();
                if (res != null)
                {
                    result.Info = res.tecnico.descripcion;
                    result.Detail = res.fecha.ToShortDateString();
                }
                else
                {
                    var hist = (from u in dbc.ResolucionesHistorico
                                where u.id_uc == idEquipo & u.id_area == idArea
                                orderby u.fecha descending
                                select u).FirstOrDefault();
                    if (hist != null)
                    {
                        result.Info = hist.tecnico.descripcion;
                        result.Detail = hist.fecha.ToShortDateString();
                    }
                }                
            }
            return Json(result);
        }
        public ActionResult getExcel(string win)
        {
            switch (win)
            {
                case "DivIncAt":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var incIngresados = (from us in dbc.IncidenciasActivas
                                             where us.area.descripcion == "At.Usuarios"
                                             select new IncTeleVista
                                             {                                                 
                                                 Numero = us.Incidencias.numero,
                                                 FechaDesde = us.fecha_inicio,
                                                 Tipificacion = us.Incidencias.Tipificaciones.nombre,
                                                 Dependencia = us.Incidencias.Dependencia.descripcion,
                                                 Técnico = us.tecnico.descripcion
                                             });
                        DataTable table = DataTableConverter.ToDataTable(incIngresados);
                        Session["tableSource"] = table;

                    }
                    break;
                case "DivIncLog": using (var dbc = new IncidenciasEntities())
                    {
                        var incLog = (from us in dbc.IncidenciasActivas
                                      where us.area.descripcion == "Logística"
                                      select new IncTeleVista
                                      {                                          
                                          Numero = us.Incidencias.numero,
                                          FechaDesde = us.fecha_inicio,
                                          Tipificacion = us.Incidencias.Tipificaciones.nombre,
                                          Dependencia = us.Incidencias.Dependencia.descripcion,
                                          Técnico = us.tecnico.descripcion
                                      });
                        DataTable table = DataTableConverter.ToDataTable(incLog);
                        Session["tableSource"] = table;

                    }
                    break;
                case "DivEqClon":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var equiposClonado = (from us in dbc.EquiposXIncidencia
                                              where us.area.descripcion == "Clonado"
                                              select new EquipoSoporteVista
                                              {                                                  
                                                  Dependencia = us.UnidadConf.nombre,
                                                  FechaDesde = us.fecha_inicio,
                                                  NroIncidente = us.Incidencias.numero,
                                                  Oblea = us.UnidadConf.nombre,
                                                  Tecnico = us.tecnico.descripcion
                                              });
                        DataTable table = DataTableConverter.ToDataTable(equiposClonado);
                        Session["tableSource"] = table;
                    }
                    break;
                case "DivEqLab":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var equiposLaboratorio = (from us in dbc.EquiposXIncidencia
                                                  where us.area.descripcion == "Laboratorio"
                                                  select new EquipoSoporteVista
                                                  {
                                                      Dependencia = us.UnidadConf.nombre,
                                                      FechaDesde = us.fecha_inicio,
                                                      NroIncidente = us.Incidencias.numero,
                                                      Oblea = us.UnidadConf.nombre,
                                                      Tecnico = us.tecnico.descripcion

                                                  });
                        DataTable table = DataTableConverter.ToDataTable(equiposLaboratorio);
                        Session["tableSource"] = table;
                    }
                    break;
                case "DivEqDel":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var equiposEntregar = (from us in dbc.EquiposXIncidencia
                                           where us.Estado_UC.descripcion == "Para Entregar"
                                           select new EquipoSoporteVista
                                           {                                               
                                               Dependencia = us.UnidadConf.nombre,
                                               FechaDesde = us.fecha_inicio,
                                               NroIncidente = us.Incidencias.numero,
                                               Oblea = us.UnidadConf.nombre,
                                               Tecnico = us.tecnico.descripcion

                                           });
                        DataTable table = DataTableConverter.ToDataTable(equiposEntregar);
                        Session["tableSource"] = table;
                    }
                    break;
                case "DivEqSus":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var equiposSuspendidos = (from us in dbc.EquiposXIncidencia
                                                  where us.Estado_UC.descripcion == "Suspendido"
                                                  select new EquipoSoporteVista
                                               {
                                                   Dependencia = us.UnidadConf.nombre,
                                                   FechaDesde = us.fecha_inicio,
                                                   NroIncidente = us.Incidencias.numero,
                                                   Oblea = us.UnidadConf.nombre,
                                                   Tecnico = us.tecnico.descripcion,
                                                   Ubicacion = (from ub in dbc.UbicacionProducto
                                                                where ub.UnidadConf.Any(c => c.id_uc== us.UnidadConf.id_uc)
                                                                select ub).FirstOrDefault().nombre
                                               });
                        DataTable table = DataTableConverter.ToDataTable(equiposSuspendidos);
                        Session["tableSource"] = table;
                    }
                    break;
                case "DviEqParaGar":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var equiposParaGar = (from us in dbc.EquiposXIncidencia
                                              where us.Estado_UC.descripcion == "Para Garantia"
                                              select new EquipoSoporteVista
                                              {
                                                  Dependencia = us.UnidadConf.nombre,
                                                  FechaDesde = us.fecha_inicio,
                                                  NroIncidente = us.Incidencias.numero,
                                                  Oblea = us.UnidadConf.nombre,
                                                  Tecnico = us.tecnico.descripcion,
                                                  Ubicacion = (from ub in dbc.UbicacionProducto
                                                               where ub.UnidadConf.Any(c => c.id_uc == us.UnidadConf.id_uc)
                                                               select ub).FirstOrDefault().nombre
                                              });
                        DataTable table = DataTableConverter.ToDataTable(equiposParaGar);
                        Session["tableSource"] = table;
                    }
                    break;
                case "DivEqEnGar": 
                    using(var dbc=new IncidenciasEntities())
                    {
                        var equiposEnGarantia = (from us in dbc.EquiposXIncidencia
                                             where us.Estado_UC.descripcion == "En Garantia"
                                             select new EquipoSoporteVista
                                             {
                                                 IdEquipo = us.id_equipo,
                                                 IdIncidente = us.id_incidencia,
                                                 Dependencia = us.UnidadConf.Jurisdiccion.descripcion,
                                                 FechaDesde = us.fecha_inicio,
                                                 NroIncidente = us.Incidencias.numero,
                                                 Oblea = us.UnidadConf.nombre,
                                                 Tecnico = us.tecnico.descripcion,
                                                 Ubicacion = "Sin Ubicar"

                                             });
                        DataTable table = DataTableConverter.ToDataTable(equiposEnGarantia);
                        Session["tableSource"] = table;
                    }
                    break;
            }
            return RedirectToRoute("Excel");
        }
        public JsonResult suspenderIncidentes(int idInc)
        {
            Responses result = new Responses();
            int idUsuarioActual = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.suspenderIncidencia(idUsuarioActual);
            return Json(result);
        }
        public JsonResult activarIncidentes()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, true, true, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.reactivarTicket(id_inc, idUsuario);
            return Json(result);
        }
        public JsonResult remito(int idInc, int idEq)
        {
            RemitoVista remito = null;
            using (var dbc = new IncidenciasEntities())
            {
                var equipo = (from u in dbc.EquiposXIncidencia
                              where u.id_equipo == idEq & u.id_incidencia == idInc
                              select u).SingleOrDefault();
                if (equipo != null)
                {
                    string tipoEquipo = equipo.UnidadConf.Tipo_equipo.descripcion;
                    
                    switch (tipoEquipo)
                    {
                        case "Pc": remito = (from u in dbc.Pc
                                             where u.id_uc == equipo.id_equipo
                                             select new RemitoVista
                                             {
                                                 Marca = u.Marca.descripcion,
                                                 Modelo = u.modelo,
                                                 NroSerie = u.UnidadConf.nro_serie,
                                                 NroTicket = equipo.Incidencias.numero,
                                                 Oblea = u.UnidadConf.nombre,
                                                 TipoDispositivo = tipoEquipo,
                                                 UsuarioActual = (HttpContext.Session["usuario"] as usuarioBean).NombreCompleto
                                             }).SingleOrDefault();
                            break;
                        case "Impresora": remito = (from u in dbc.Impresoras
                                                    where u.id_uc == equipo.id_equipo
                                                    select new RemitoVista
                                                    {
                                                        Marca = u.Modelo_Impresora.Marca.descripcion,
                                                        Modelo = u.Modelo_Impresora.descripcion,
                                                        NroSerie = u.UnidadConf.nro_serie,
                                                        NroTicket = equipo.Incidencias.numero,
                                                        Oblea = u.UnidadConf.nombre,
                                                        TipoDispositivo = tipoEquipo,
                                                        UsuarioActual = (HttpContext.Session["usuario"] as usuarioBean).NombreCompleto
                                                    }).SingleOrDefault();
                            break;
                        case "Monitor": remito = (from u in dbc.Monitores
                                                  where u.id_uc == equipo.id_equipo
                                                  select new RemitoVista
                                                  {
                                                      Marca = u.Modelo_Monitor.Marca.descripcion,
                                                      Modelo = u.Modelo_Monitor.descripcion,
                                                      NroSerie = u.UnidadConf.nro_serie,
                                                      NroTicket = equipo.Incidencias.numero,
                                                      Oblea = u.UnidadConf.nombre,
                                                      TipoDispositivo = tipoEquipo,
                                                      UsuarioActual = (HttpContext.Session["usuario"] as usuarioBean).NombreCompleto
                                                  }).SingleOrDefault();
                            break;
                        case "Varios": remito = (from u in dbc.Varios
                                                 where u.id_uc == equipo.id_equipo
                                                 select new RemitoVista
                                                 {
                                                     Marca = u.Marca.descripcion,
                                                     Modelo = u.modelo,
                                                     NroSerie = u.UnidadConf.nro_serie,
                                                     NroTicket = equipo.Incidencias.numero,
                                                     Oblea = u.UnidadConf.nombre,
                                                     TipoDispositivo = tipoEquipo,
                                                     UsuarioActual = (HttpContext.Session["usuario"] as usuarioBean).NombreCompleto
                                                 }).SingleOrDefault();
                            break;
                    }
                }
                                      
            }
            return Json(remito);
        }
    }
}
