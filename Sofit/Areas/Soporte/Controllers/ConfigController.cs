﻿using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Linq;
using System;
using soporte.Areas.Soporte.Models;
using soporte.Controllers;
using soporte.Models.Statics;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Helpers;
using soporte.Models.ClasesVistas;
using System.Data.Entity.Infrastructure;
using soporte.Models.Interface;
using soporte.Models.Factory;
using soporte.Models.Displays;
using System.Text;
using System.Data.Entity;

namespace soporte.Areas.Soporte.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {       
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];        
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }            
            return View();
        }
        public ActionResult Tipificaciones()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Tipificaciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using(var dbc=new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre=="Soporte Técnico"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Soporte Técnico");                
                model.Tipificaciones = arbol.getResult();                
                return View(model);
            }
        }
        public ActionResult Clientes()
        {
            return View();
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre,int idServicio)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Soporte Técnico" select u).Single();
                    Tipificaciones nueva = new Tipificaciones()
                    {
                        nombre = nombre,
                        descripcion = desc,
                        id_padre = idPadre,
                        id_servicio = idServicio,
                        id_direccion = direccionSoporte.id_direccion
                    };
                    dbc.Tipificaciones.Add(nueva);

                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas,int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras=TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Soporte Técnico");
            using (var dbc = new IncidenciasEntities())
            {
                var serv = new Servicios
                {
                    nombre = nombre,
                    tiempo_res = totales.Hours,
                    tiempo_margen = totalesMargen.Hours,
                    id_direccion = direccion.IdDireccion,
                    tolerancia = 80,
                    vigente = true
                };
                dbc.Servicios.Add(serv);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }        
        public JsonResult quitarServicio(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var serv = dbc.Servicios.Where(c => c.id_servicio == id).Single();
                    dbc.Servicios.Remove(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = "El servicio está siendo utilizado";
            }
            return Json(result);
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t=(from u in dbc.Tipificaciones where u.id_tipificacion==id
                          select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public ActionResult Jurisdicciones()
        {
            Responses result = new Responses();
            List<JurisdiccionVista> jurisdicciones = null;
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Jurisdicciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return View("DenyPermission");
            }
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    jurisdicciones = dbc.Jurisdiccion.Select(z => new JurisdiccionVista
                    {
                        idJurisdiccion = z.id_jurisdiccion,
                        Nombre = z.descripcion
                    }).OrderBy(c=>c.Nombre).ToList();
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            ViewBag.Jurisdicciones = jurisdicciones;
            return PartialView();
        }
        #region Referentes
        public JsonResult AgregarReferente(ReferentesVista data)
        {
            Responses result = new Responses();
            Referentes referentebuscado=null;

            try
            {
                using (var db = new IncidenciasEntities())
                {
                    referentebuscado = db.Referentes.Where(c => c.nombre == data.nombre).SingleOrDefault(); ;

                    if (referentebuscado != null)
                    {
                        referentebuscado.email = data.email;
                        referentebuscado.te = data.te;
                        data.Jurisdicciones = (from u in referentebuscado.Jurisdiccion
                                               select new JurisdiccionVista
                                               {
                                                   idJurisdiccion = u.id_jurisdiccion,
                                                   Nombre = u.descripcion
                                               }).ToList();
                        data.Dependencias = (from u in referentebuscado.Dependencia
                                             select new DependenciaVista
                                             {
                                                 Nombre = u.descripcion
                                             }).ToList();
                        if (data.Jurisdicciones.Count == 0 & data.Dependencias.Count == 0)
                        {
                            result = agregarLocacionesAlReferente(data, referentebuscado,db);
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            foreach (JurisdiccionVista j in data.Jurisdicciones)
                            {
                                sb.Append("Jurisd.: ").Append(j.Nombre).Append(Environment.NewLine);
                            }
                            foreach (DependenciaVista d in data.Dependencias)
                            {
                                sb.Append("Depend.: ").Append(d.Nombre).Append(Environment.NewLine);
                            }
                            result.Detail = "Ya existe un referente con ese nombre en: " + sb.ToString();
                        }
                    }
                    else
                    {
                        Referentes nuevo = new Referentes
                        {
                            nombre = data.nombre,
                            email = data.email,
                            te = data.te
                        };
                        db.Referentes.Add(nuevo);
                        result = agregarLocacionesAlReferente(data, nuevo,db);
                    }                    
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }        
            return Json(result);
        }
        private Responses agregarLocacionesAlReferente(ReferentesVista data, Referentes r, IncidenciasEntities dbc)
        {
            Responses result = new Responses();

            List<int> idJur = new List<int>();
            foreach (LocationVista l in data.location)
            {
                if (l.tipo.Equals("jurisdiccion"))
                {
                    idJur.Add(l.id);
                }
            }
            foreach (LocationVista l in data.location)
            {
                if (l.tipo.Equals("jurisdiccion"))
                {
                    Jurisdiccion j = dbc.Jurisdiccion.Where(s => s.id_jurisdiccion == l.id).Single();
                    r.Jurisdiccion.Add(j);
                }
                if (l.tipo.Equals("dependencia"))
                {
                    Sofit.DataAccess.Dependencia d = dbc.Dependencia.Where(s => s.id_dependencia == l.id).Single();
                    if (!idJur.Contains(d.id_jurisdiccion.Value))
                    {
                        r.Dependencia.Add(d);
                    }
                }
            }
            dbc.SaveChanges();
            result.Info = "ok";
            return result;
        }
        public JsonResult updateReferente(ReferentesVista data)
        {
            Responses result = new Responses();
            try
            {
                using (var db = new IncidenciasEntities())
                {
                    Referentes r = db.Referentes.Include("Jurisdiccion").Include("Dependencia").Where(o => o.nombre == data.nombre).Single();
                    r.nombre = data.nombre;
                    r.email = data.email;
                    r.te = data.te;
                    List<int> idJur = new List<int>();
                    r.Jurisdiccion.Clear();
                    r.Dependencia.Clear();
                    foreach (LocationVista l in data.location)
                    {
                        if (l.tipo.Equals("jurisdiccion"))
                        {                            
                            idJur.Add(l.id);
                        }                        
                    }
                    foreach (LocationVista l in data.location)
                    {
                        
                        if (l.tipo.Equals("jurisdiccion"))
                        {
                            Jurisdiccion j = db.Jurisdiccion.Where(s => s.id_jurisdiccion == l.id).Single();
                            r.Jurisdiccion.Add(j);
                        }
                        if (l.tipo.Equals("dependencia"))
                        {
                            Sofit.DataAccess.Dependencia d = db.Dependencia.Where(s => s.id_dependencia == l.id).Single();
                            if (!idJur.Contains(d.id_jurisdiccion.Value))
                            {
                                r.Dependencia.Add(d);
                            }
                        }
                    }
                    db.Entry(r).State = EntityState.Modified;
                    db.SaveChanges();
                    result.Info = "ok";
                }                   
                
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        
        public JsonResult quitarReferente(int idRef,int idJur)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var referente = dbc.Referentes.Include("Jurisdiccion").Include("Dependencia").Where(q => q.id == idRef).Single();
                    Jurisdiccion juri = referente.Jurisdiccion.Where(c => c.id_jurisdiccion == idJur).SingleOrDefault();
                    var depe = referente.Dependencia.Where(c => c.Jurisdiccion.id_jurisdiccion == idJur);
                    referente.Jurisdiccion.Remove(juri);
                    depe.ToList().ForEach(r => referente.Dependencia.Remove(r));
                                    
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        #endregion
        
        public JsonResult DependenciasXJurisdiccionTabla(int idJur, string formato)
        {
            Responses result = new Responses();
            string html = "";
            List<DependenciaVista>dependencias=null;
            using(var dbc=new IncidenciasEntities())
            {
                dependencias = dbc.Dependencia.Where(c => c.id_jurisdiccion == idJur).Select(x => new DependenciaVista
                {
                    idDependencia = x.id_dependencia,
                    Nombre = x.descripcion
                }).OrderBy(c=>c.Nombre).ToList();
            }
            html += "<thead class='fixedHeader'><tr class='ui-state-default'><th style='display:none;'>id</th><th>Dependencias</th></tr></thead>" +
                    "<tbody class='scrollContent'>";
            foreach (DependenciaVista d in dependencias)
            {
                html += "<tr><td style='display:none;'>" + d.idDependencia + "</td>";
                html += "<td>" + d.Nombre + "<div class='right icon'></div><div class='right icon'></div></td></tr>";
            }
            html += "</tbody>";
            result.Info = "ok";
            result.Detail = html;
            return Json(result);
        }
        public JsonResult renameDependencia(int id, string nombre)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    dbc.Dependencia.Where(c => c.id_dependencia == id).Single().descripcion = nombre;
                    dbc.SaveChanges();
                }
            }catch(Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult renameJurisdiccion(int id, string nombre)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    dbc.Jurisdiccion.Where(c => c.id_jurisdiccion == id).Single().descripcion = nombre;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoUsuario(string login, string nombre, string p1, int perfil)
        {
            Responses result = new Responses();
            //usuarioHandler user = new usuarioHandler();
            //IDirecciones soporte = DireccionesFactory.getDireccion("Soporte Técnico");
            //result = user.nvoUsuario(login, nombre, p1, perfil, soporte);
            return Json(result);
        }
        public ActionResult Usuarios()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Usuarios");
            IDirecciones soporteDir = DireccionesFactory.getDireccion("Soporte Técnico");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            if(usuario.Perfiles.Contains(new PerfilAdministradorGral() { nombre = "Administrador Gral." }))
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getAll();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getTodos();
                return PartialView("Usuarios", model);
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesSoporte();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getTodosDeDireccion(soporteDir);
                return PartialView("Usuarios",model); 
            }                
        }  
            
        public ActionResult Tiempos() {
            Constantes.AccesosPagina acceso = usuario.getAccess("Soporte/Tiempos");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            Tiempos model = new Tiempos();            
            return PartialView(model);            
        }
        public JsonResult setTiempos(int tAClon, int tALab,
            int tADel, int tASus,
            int tAAGar, int tAEGar,
            int tEClon, int tELab, 
            int tEDel, int tESus, 
            int tEAGar, int tEEGar)
        {
            Responses result = new Responses();
            Tiempos tiempos = new Tiempos();
            tiempos.tAClon = tAClon;
            tiempos.tALab = tALab;
            tiempos.tADel = tADel;
            tiempos.tASus = tASus;
            tiempos.tAAGar = tAAGar;
            tiempos.tAEGar = tAEGar;
            
            tiempos.tEClon = tEClon;
            tiempos.tELab = tELab;
            tiempos.tEDel = tEDel;           
            tiempos.tESus = tESus;
            tiempos.tEAGar = tEAGar;
            tiempos.tEEGar = tEEGar;
            result = tiempos.save();
            return Json(result);
        }
        

        public ActionResult Fun()
        {
            return PartialView();
        }
        public JsonResult borrarEquipo(string oblea)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    bool flag = true;
                    var pr = dbc.Impresoras.Where(c => c.UnidadConf.nombre == oblea).SingleOrDefault();
                    if (pr != null)
                    {
                        var uc = pr.UnidadConf;
                        dbc.Impresoras.Remove(pr);
                        dbc.UnidadConf.Remove(uc);
                        flag = false;
                    }
                    var ws = dbc.Pc.Where(c => c.UnidadConf.nombre == oblea).SingleOrDefault();
                    if (ws != null)
                    {
                        var uc = ws.UnidadConf;
                        dbc.Pc.Remove(ws);
                        dbc.UnidadConf.Remove(uc);
                        flag = false;
                    }
                    var mn = dbc.Monitores.Where(c => c.UnidadConf.nombre == oblea).SingleOrDefault();
                    if (mn != null)
                    {
                        var uc = mn.UnidadConf;
                        dbc.Monitores.Remove(mn);
                        dbc.UnidadConf.Remove(uc);
                        flag = false;
                    }
                    if (flag)
                    {
                        result.Detail = "Equipo no encontrado! Si es del tipo Varios contacte al administrador!";
                    }
                    else
                    {
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = "El equipo no se puede borrar porque está asociado con algunos trabajos!!";
            }
            return Json(result);
        }
        //
        // GET: /Config/Create

        public JsonResult quitarJurisdiccion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var dep = dbc.Jurisdiccion.Where(c => c.id_jurisdiccion == id).Single();
                    dbc.Jurisdiccion.Remove(dep);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult quitarDependencia(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var dep = dbc.Dependencia.Where(c => c.id_dependencia == id).Single();
                    dbc.Dependencia.Remove(dep);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult nvaJurisdiccion(string nombre)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var dep = new Jurisdiccion
                    {
                        descripcion = nombre
                    };
                    dbc.Jurisdiccion.Add(dep);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult nvaDependencia(string nombre, int idJur)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var dep = new Sofit.DataAccess.Dependencia
                    {
                        id_jurisdiccion=idJur,
                        descripcion = nombre
                    };
                    dbc.Dependencia.Add(dep);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }       
    }
}
