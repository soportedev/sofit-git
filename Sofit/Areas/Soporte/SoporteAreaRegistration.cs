﻿using System.Web.Mvc;

namespace soporte.Areas.Soporte.Models
{
    public class SoporteAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Soporte";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {            
            //context.Routes.MapPageRoute("historicos", "Soporte/Historicos", "~/Areas/Soporte/Views/Historicos.aspx");
            //context.Routes.MapPageRoute("inventario", "Soporte/Inventario", "~/Areas/Soporte/Views/Default.aspx");
            //////context.Routes.MapPageRoute("soporte_por_default", "Soporte", "~/Areas/Soporte/Views/incidentes.aspx");
            //context.Routes.MapPageRoute("abm1", "Soporte/Abmws", "~/Areas/Soporte/Views/abm1.aspx");
            //context.Routes.MapPageRoute("abm2", "Soporte/Abmpr", "~/Areas/Soporte/Views/abm2.aspx");
            //context.Routes.MapPageRoute("abm3", "Soporte/Abmmn", "~/Areas/Soporte/Views/abm3.aspx");
            //context.Routes.MapPageRoute("abm4", "Soporte/Abmvr", "~/Areas/Soporte/Views/abm4.aspx");
            context.MapRoute(
                name: "lineabasesoporte",
                url: "Soporte/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiosoporte",
                url: "Soporte/Cambios/{startIndex}",
                defaults: new { controller = "Cambios", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8oporte",
                url: "Soporte/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );            
            context.MapRoute(
                "Soporte_default",
                "Soporte/{controller}/{action}/{id}",
                new { controller = "Soporte/Incidentes", action = "Index", id = UrlParameter.Optional },
                new[] { "soporte.Areas.Soporte.Controllers" }
            );           
        }
    }
}
