﻿using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Linq;
using soporte.Models;

namespace soporte.Areas.Soporte.Models
{
    public class EquiposEnIncidenciaSoporte:IEquiposEnIncidencia
    {       
        public EquiposEnIncidenciaSoporte()
        {
                   
        }
        
        public override soporte.Models.Statics.Responses quitarEquipoAIncidencia(string oblea)
        {
            Responses result = new Responses();
            EquipoEnIncidente equipoEncontrado = null;
            foreach (EquipoEnIncidente e in equiposSoporte)
            {               
                if (e.nroOblea.Equals(oblea))
                {
                    equipoEncontrado = e;
                    break;
                }
            }
            if (equipoEncontrado != null)
            {
                using (var dbc = new IncidenciasEntities())
                {
                    EquiposXIncidencia exi = dbc.EquiposXIncidencia.Where(c => c.UnidadConf.nombre == oblea).FirstOrDefault();
                    int idticket=exi.id_incidencia;
                    int idequipo=exi.id_equipo;
                    AreaXEquipoN axe = dbc.AreaXEquipoN.Where(c => c.id_equipo==idequipo & c.id_incidencia == idticket).FirstOrDefault();
                    EstadoXEquipoN exe = dbc.EstadoXEquipoN.Where(c => c.id_equipo == idequipo & c.id_incidencia == idticket).FirstOrDefault();
                    UnidadConf equipo = dbc.UnidadConf.Where(c => c.nombre == oblea).FirstOrDefault();
                    //equipo.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "En Locación").FirstOrDefault().id_estado;
                    dbc.EquiposXIncidencia.Remove(exi);
                    dbc.AreaXEquipoN.Remove(axe);
                    dbc.EstadoXEquipoN.Remove(exe);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            else
            {
                result.Detail = "No se ha encontrado ese equipo.";
            }
            return result;
        }
        public override soporte.Models.Statics.Responses agregarEquipoAIncidencia(string oblea, int id_area,int idtecnico)
        {
            Responses result = new Responses();            
            using (var dbc = new IncidenciasEntities())
            {
                var equ = (from u in dbc.EquiposXIncidencia
                         where u.UnidadConf.nombre==oblea
                         select u).FirstOrDefault();
                if (equ != null)
                {
                    result.Detail = "El equipo está asociado al incidente " + equ.Incidencias.numero
                        + ". Debe resolver el Incidente primero o contacte al administrador.";
                }
                else
                {
                    var equipo = (from u in dbc.UnidadConf
                          where u.nombre == oblea
                          select u).First();
                    
                    try
                    {
                        EquiposXIncidencia exi = new EquiposXIncidencia
                        {
                            id_incidencia = ticket.Id,
                            id_area = id_area,
                            id_tecnico = idtecnico,
                            fecha_inicio = DateTime.Now,
                            id_estado = EstadoEquipoFactory.getEstadoPorNombre("En Reparación", "Soporte Técnico").idEstado,
                            id_equipo = equipo.id_uc
                        };
                        //poner estado equipo en reparacion
                        //equipo.id_estado = exi.id_estado;
                        AreaXEquipoN axe = new AreaXEquipoN
                        {
                            id_area = id_area,
                            id_equipo = equipo.id_uc,
                            id_tecnico = idtecnico,
                            id_incidencia = ticket.Id,
                            fecha_inicio = DateTime.Now
                        };
                        EstadoXEquipoN exe = new EstadoXEquipoN
                        {
                            id_equipo = equipo.id_uc,
                            id_estado = exi.id_estado,
                            id_incidencia = ticket.Id,
                            id_tecnico = idtecnico,
                            fecha_inicio = DateTime.Now
                        };
                        dbc.EquiposXIncidencia.Add(exi);
                        dbc.AreaXEquipoN.Add(axe);
                        dbc.EstadoXEquipoN.Add(exe);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    catch (Exception e)
                    {
                        result.Detail = e.Message;
                    }
                }
            }           
            return result;
        }

        public override soporte.Models.Statics.Responses derivarEquipo(int idEq, AreaEquipo area, string observaciones, int idUsuario, int idTecnico)
        {
            Responses result = new Responses();
            EquipoEnIncidente eq = equiposSoporte.Where(s => s.idEquipo == idEq).FirstOrDefault();
            result = eq.Derivar(area, observaciones, idUsuario,idTecnico);
            if (result.Info == "ok")
            {
                //verifico si tengo que reactivar o poner en standby
                bool flagTodosEntregar = true;
                bool flagTodosReparacion = true;
                foreach (EquipoEnIncidente equi in equiposSoporte)
                {
                    EstadoEquipo estado=equi.getEstadoActual();
                    if (!(estado is EstadoEnLocacion | estado is EstadoEntregar | estado is EstadoTerminado))
                    {
                        flagTodosEntregar = false;
                    }
                    if (estado.GetType()!=typeof(EstadoEnReparacion))
                    {
                        flagTodosReparacion = false;
                    }
                }
                if (flagTodosEntregar)
                {
                    IEstadoIncidencia nuevoEstado=EstadoIncidenciaFactory.getEstado("Esperando Retiro Equipo");
                    ticket.cambioEstado(nuevoEstado, idTecnico, idUsuario);
                }
                if(flagTodosReparacion & ticket.EstadoActual.GetType() != typeof(EstadoIncidenciaDerivado))
                {
                    IEstadoIncidencia nuevoEstado = EstadoIncidenciaFactory.getEstado("Derivado");
                    ticket.cambioEstado(nuevoEstado, idTecnico, idUsuario);
                }
            }
            return result;
        }

        public override soporte.Models.Statics.Responses buscarEqPendientes(string texto)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cargarSolucionEquipo(int idUsuario, int idEquipo, int tipoResolucion, int tecnico, int turno, int? imagen,string archivo, string observacion, bool update)
        {
            Responses result = new Responses();
            foreach (EquipoEnIncidente equipo in equiposSoporte)
            {
                if (equipo.idEquipo == idEquipo)
                {
                    result = equipo.cargarSolucion(idUsuario, tipoResolucion, tecnico, turno, imagen, archivo, observacion, update);
                }
            }
            return result;
        }

        public override soporte.Models.Statics.Responses terminarEquipo(int id_eq, int idUsuario)
        {
            Responses result = new Responses();
            var flag = true;
            foreach (EquipoEnIncidente eq in equiposSoporte)
            {
                if (eq.idEquipo == id_eq)
                {
                    result = eq.SolucionarEquipo();
                    break;
                }
            }
            foreach (EquipoEnIncidente eq in equiposSoporte)
            {
                if (!(eq.getEstadoActual() is EstadoEnLocacion) & !(eq.getEstadoActual() is EstadoTerminado))
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                //EstadoEquipo estadoLocación = EstadoEquipoFactory.getEstadoPorNombre("En Locación", "Soporte Técnico");
                int idTecnico = 0;
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        foreach (EquipoEnIncidente eq in equiposSoporte)
                        {
                            var eqxinc = (from u in dbc.EquiposXIncidencia
                                          where u.id_equipo == eq.idEquipo
                                          select u).Single();
                            dbc.EquiposXIncidencia.Remove(eqxinc); 
                            
                        }
                        idTecnico = dbc.tecnico.Where(c => c.id_usuario == idUsuario).Single().id_tecnico;
                        dbc.SaveChanges();

                    }
                    IEstadoIncidencia nuevoE= ticket.EstadoActual = EstadoIncidenciaFactory.getEstado("Para Cerrar");
                    IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea("Soporte");
                    this.ticket.cambioArea(areaDestino, areaDestino.getIdTecnicoGenerico(), idUsuario);                        
                    result = ticket.cambioEstado(nuevoE,idTecnico,idUsuario);
                    //result = ticket.cerrarIncidencia(idUsuario);
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return result;
        }

        public override EquipoEnIncidente getEquipoXOblea(string oblea)
        {
            EquipoEnIncidente result = null;
            foreach (EquipoEnIncidente equipo in equiposSoporte)
            {
                if (equipo.nroOblea.Equals(oblea))
                {
                    result = equipo;
                    break;
                }
            }
            return result;
        }

        //public override void notificarEquipo(int idEquipo)
        //{
        //    Responses result = new Responses();
        //    foreach (EquipoEnIncidente eq in equipos)
        //    {
        //        if (eq.idEquipo == idEquipo)
        //        {
        //            result = eq.notificarEquipo();
        //            if (result.Info != "ok")
        //            {
        //                throw new Exception(result.Detail);
        //            }
        //            break;
        //        }
        //    }
        //}

        public override EquipoEnIncidente getEquipoXId(int idEquipo)
        {
            EquipoEnIncidente eq = equiposSoporte.Where(s => s.idEquipo == idEquipo).FirstOrDefault();
            return eq;
        }
        public Responses RegistrarBaseCambioEstadoEquipo()
        {
            throw new NotImplementedException();
        }       

        public override Responses cambioEstadoEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        public override Responses cambioAreaEquipo(int idEquipo)
        {
            throw new NotImplementedException();
        }

        internal override bool tieneEquiposActivos()
        {
            bool flag = false;
            foreach (EquipoEnIncidente eq in equiposSoporte)
            {
                if (!(eq.getEstadoActual()is EstadoTerminado))
                {
                    flag = true;
                }
            }
            return flag;
        }

        internal override string getEquiposSinTerminar()
        {
            string result = "";
            foreach (EquipoEnIncidente eq in equiposSoporte)
            {
                if (!(eq.getEstadoActual() is EstadoTerminado))
                {
                    result += eq.nroOblea + "  ";
                }
            }
            return result;
        }
        public override Responses notificarEquipo(int idEquipo, string[] referentes, string firma, string cuerpo, string subject)
        {
            EquipoEnIncidente eq = equiposSoporte.Where(c => c.idEquipo == idEquipo).First();
            return eq.notificarEquipo(referentes,firma,cuerpo,subject);
        }

        internal override Responses puedeReactivarTicket()
        {
            Responses result = new Responses();
            bool flag = true;
            foreach (EquipoEnIncidente eq in equiposSoporte)
            {
                if (eq.getEstadoActual() is EstadoSuspendido | eq.getEstadoActual() is EstadoNotificar | eq.getEstadoActual() is EstadoUbicar)
                {
                    if (flag) result.Detail = "Los siguientes equipos están suspendidos: ";
                    result.Detail += " " + eq.nroOblea + "  ";
                    flag = false;
                }
            }
            if (flag) result.Info = "ok";            
            return result;
        }

        
    }
}