﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Sofit.DataAccess;
using soporte.Models.Statics;

namespace soporte.Areas.Soporte.Models
{
    public class Tiempos
    {        
        public int tAClon { get; set; }
        public int tEClon { get; set; }
        public int tALab { get; set; }
        public int tELab { get; set; }
        public int tASus { get; set; }
        public int tESus { get; set; }       
        public int tADel { get; set; }
        public int tEDel { get; set; }       
        public int tAAGar { get; set; }
        public int tEAGar { get; set; }
        public int tAEGar { get; set; }
        public int tEEGar { get; set; }
        public int tCriticoIncidente { get; set; }
        public int tMaxSuspension { get; set; }
        public Tiempos()
        {
            using (var dbc = new IncidenciasEntities())
            {
                var ts = from t in dbc.TiemposGral                         
                         select t;
                if (ts != null)
                {
                    tAClon = ts.Where(c => c.nombre == "AlertaClon").Single().duracion;
                    tEClon = ts.Where(c => c.nombre == "CritClon").Single().duracion;
                    tALab = ts.Where(c => c.nombre == "AlertaLab").Single().duracion;
                    tELab = ts.Where(c => c.nombre == "CritLab").Single().duracion;
                    tASus = ts.Where(c => c.nombre == "AlertaSus").Single().duracion;
                    tESus = ts.Where(c => c.nombre == "CritSus").Single().duracion;
                    tADel = ts.Where(c => c.nombre == "AlertaEntrega").Single().duracion;
                    tEDel = ts.Where(c => c.nombre == "CritEntrega").Single().duracion;
                    tAAGar = ts.Where(c => c.nombre == "AlertaPGar").Single().duracion;
                    tEAGar = ts.Where(c => c.nombre == "CritPGar").Single().duracion;
                    tAEGar = ts.Where(c => c.nombre == "AlertaEnGar").Single().duracion;
                    tEEGar = ts.Where(c => c.nombre == "CritEnGar").Single().duracion;
                    tCriticoIncidente = ts.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                    tMaxSuspension = ts.Where(c => c.nombre == "MaxSuspension").Single().duracion;
                }
            }
        }

        internal soporte.Models.Statics.Responses save()
        {
            Responses result = new Responses();
            try
            {                
                using (var dbc = new IncidenciasEntities())
                {
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaClon").Single().duracion = this.tAClon;
                    dbc.TiemposGral.Where(c => c.nombre == "CritClon").Single().duracion = this.tEClon;
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaLab").Single().duracion = this.tALab;
                    dbc.TiemposGral.Where(c => c.nombre == "CritLab").Single().duracion = this.tELab;
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaSus").Single().duracion = this.tASus;
                    dbc.TiemposGral.Where(c => c.nombre == "CritSus").Single().duracion = this.tESus;
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaEntrega").Single().duracion = this.tADel;
                    dbc.TiemposGral.Where(c => c.nombre == "CritEntrega").Single().duracion = this.tEDel;
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaPGar").Single().duracion = this.tAAGar;
                    dbc.TiemposGral.Where(c => c.nombre == "CritPGar").Single().duracion = this.tEAGar;
                    dbc.TiemposGral.Where(c => c.nombre == "AlertaEnGar").Single().duracion = this.tAEGar;
                    dbc.TiemposGral.Where(c => c.nombre == "CritEnGar").Single().duracion = this.tEEGar;
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }catch(Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }
    }
}
