﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class PerfilAdministradorSoporte:Perfil
    {
        public PerfilAdministradorSoporte()
        {
            this.nombre = "Administrador Soporte";
            this.nombreBase = "Administrador Soporte";
        }

        public override Constantes.AccesosPagina getAcceso(string page)
        {
            string[] url = page.Split('/');
            if (url[0].Equals("Soporte")) return soporte.Models.Statics.Constantes.AccesosPagina.Full;
            else return soporte.Models.Statics.Constantes.AccesosPagina.NoAccess;
        }

        public override string getHomePage()
        {
            return "Soporte/Incidentes";
        }
    }
}