﻿using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;


namespace soporte.Areas.Soporte.Models
{
    public class AreaDeposito : AreaEquipo
    {
        public AreaDeposito(int idArea,string nombre)
        {
            base.idArea = idArea;
            base.nombreArea = nombre;            
        }
        public override Responses derivar(AreaEquipo area, int idUsuario, int idTecnico)
        {
            EstadoEquipo nuevoEstado = null;
            
            Responses r=new Responses();
            if (area is AreaLogistica)
            {
                nuevoEstado = EstadoEquipoFactory.getEstadoPorNombre("Para Entregar", "Soporte Técnico");
                
            }
            if (area is AreaClonado | area is AreaLaboratorio)
            {
                nuevoEstado = EstadoEquipoFactory.getEstadoPorNombre("En Reparación", "Soporte Técnico");
            }            
            r=equipo.cambioEstado(nuevoEstado,idTecnico);
            if (r.Info == "ok")
            {
                r = equipo.cambioArea(area, idTecnico);
            }
            return r;
        }

        public override Responses terminarEquipo()
        {
            throw new NotImplementedException();
        }

        public override string getNombreArea()
        {
            return base.nombreArea;
        }

        public override int getIdArea()
        {
            return base.idArea;
        }
        public override Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject)
        {
            Responses result = new Responses();
            EmailSender sender = new EmailSender();
            bool flagNotificacion = true;
            bool huboError=false;
            StringBuilder sb = new StringBuilder();
            NotificacionSoporte noti = new NotificacionSoporte();
            usuarioBean usuarioActual = HttpContext.Current.Session["usuario"] as usuarioBean; 
            foreach (string referente in referentes)
            {                 
                Referentes referenteBuscado;
                string direccionMail = string.Empty;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(referente);
                if (match.Success)
                {
                    direccionMail = referente;
                }
                else
                {
                    try
                    {
                        int idRef = 0;
                        Int32.TryParse(referente, out idRef);
                        using (var dbc = new IncidenciasEntities())
                        {
                            referenteBuscado = (from t in dbc.Referentes
                                                where t.id == idRef
                                                select t).Single();
                        }
                        direccionMail = referenteBuscado.email;
                    }
                    catch (Exception e)
                    {
                        huboError = true;
                        sb.Append(referente).Append(". ");
                        continue;
                    }
                }
                //envio         
                result = sender.sendEmail(new string[] { direccionMail, subject, cuerpo });
                if (result.Info == "ok")
                {                                      
                    using (var dbc = new IncidenciasEntities())
                    {
                        if (flagNotificacion)
                        {
                            //inicializo la noti                
                            noti.asunto = subject;
                            noti.mensaje = cuerpo;
                            noti.id_equipo = equipo.idEquipo;
                            noti.id_incidencia = equipo.incidencia.Id;
                            noti.id_usuario = usuarioActual.IdUsuario;
                            noti.fecha = DateTime.Now;

                            try
                            {
                                dbc.NotificacionSoporte.Add(noti);
                                dbc.SaveChanges();
                                result.Info = "ok";
                                //para no guardar mas de una vez la noti
                                flagNotificacion = false;
                            }
                            catch (Exception e)
                            {
                                result.Info = "error";
                                result.Detail = e.Message;
                            }
                        }
                        NotMailRefSoporte mailxRef = new NotMailRefSoporte
                        {
                            direccion_mail = direccionMail,
                            id_notificacion = noti.id
                        };
                        dbc.NotMailRefSoporte.Add(mailxRef);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }//contexto dbc
                }//envio exitoso

                else
                {
                    //error envío
                    huboError=true;
                    sb.Append(referente).Append(". ");
                }
            }//iteracion referentes
            if (result.Info == "ok")
            {
                equipo.cambioEstado(EstadoEquipoFactory.getEstadoPorNombre("Para Ubicar", "Soporte Técnico"), equipo.areaActual.getTecnicoGenerico());
            }
            if (huboError & result.Info=="ok")
            {
                result.Info = "algunos errores";
                result.Detail = sb.ToString();
            }            
            return result;
        }
        
        public override int getTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                idTecnico = (from u in dbc.tecnico
                             where u.descripcion == "Depósito"
                             select u.id_tecnico).First();
            }
            return idTecnico;
        }

        internal override Responses cargarSolucion(int idUsuario, int tResolucionClonado, int tecnico, int turno, int? imagen, string archivo, string observacion)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    Resoluciones res = new Resoluciones
                    {
                        id_area = this.idArea,
                        fecha = DateTime.Now,
                        id_uc = equipo.idEquipo,
                        id_tecnico = tecnico,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_tipo_resolucion = tResolucionClonado,
                        id_incidencia = this.equipo.incidencia.Id,
                        id_imagen = imagen,
                        path_imagen = archivo == string.Empty ? null : archivo
                    };
                    dbc.Resoluciones.Add(res);                    
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e) { result.Detail = e.Message; }
            return result;
        }

        
    }
}
