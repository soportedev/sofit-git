﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class Articulo
    {
        public int IdArticulo { get; set; }
        public string Descripcion { get; set; }
        public string Rubro { get; set; }
        public string Detalle { get; set; }
        public int IdDependencia { get; set; }
        public int IdEquipo { get; set; }
        public string NroArticulo { get; set; }
        public int IdArmario { get; set; }
        public bool tieneEquipo { get; set; }
    }
}
