﻿using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using Sofit.DataAccess;
using soporte.Models.Statics;
using System;
using System.Linq;


namespace soporte.Areas.Soporte.Models
{
    public class AreaClonado:AreaEquipo
    {        
        public AreaClonado(int idArea,string nombre)
        {
            base.idArea = idArea;
            base.nombreArea = nombre;
        }
        internal override Responses cargarSolucion(int idUsuario, int tResolucionClonado, int tecnico, int turno, int? imagen, string archivo, string observacion)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    Resoluciones res = new Resoluciones
                    {
                        id_area = this.idArea,
                        fecha = DateTime.Now,
                        id_uc = equipo.idEquipo,
                        id_tecnico = tecnico,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_tipo_resolucion = tResolucionClonado,
                        id_incidencia = this.equipo.incidencia.Id,
                        path_imagen = archivo == string.Empty ? null : archivo,
                        id_imagen = imagen
                    };
                    dbc.Resoluciones.Add(res);
                    //si corresponde cambio de area
                    string tres = (from u in dbc.tipo_resolucion
                                   where u.id_tipo_resolucion == tResolucionClonado
                                   select u.descripcion).First();
                    switch (tres.ToLower())
                    {
                        case "suspendido": AreaEquipo areaNueva = AreaEquipoFactory.getAreaXNombre("Depósito");
                            EstadoEquipo estado = EstadoEquipoFactory.getEstadoPorNombre("Para Notificar", "Soporte Técnico");
                            equipo.cambioArea(areaNueva, tecnico);
                            equipo.cambioEstado(estado,tecnico);
                            equipo.incidencia.forceSuspenderIncidencia(idUsuario);
                            break;
                    }
                    //
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e) { result.Detail = e.Message; }
            return result;
        }        
        public override string ToString()
        {
            return "Clonado";
        }
        public override Responses terminarEquipo()
        {
            throw new NotImplementedException();
        }

        public override Responses derivar(AreaEquipo area, int idUsuario,int idTecnico)
        {
            Responses result = new Responses();
            if (area is AreaLogistica)
            {
                equipo.cambioEstado(EstadoEquipoFactory.getEstadoPorNombre("Para Entregar", "Soporte Técnico"), area.getTecnicoGenerico());
            }
            result=equipo.cambioArea(area,idTecnico);
            return result;
        }

        public override string getNombreArea()
        {
            return base.nombreArea;
        }

        public override int getIdArea()
        {
            return base.idArea;
        }

        public override int getTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                idTecnico = (from u in dbc.tecnico
                         where u.descripcion == "Clonado"
                         select u.id_tecnico).First();
            }
            return idTecnico;
        }



        public override Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject)
        {
            throw new NotImplementedException();
        }
    }
}
