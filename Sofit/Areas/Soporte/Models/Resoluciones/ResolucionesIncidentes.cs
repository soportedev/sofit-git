﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Models.ClasesVistas
{
    public class ResolucionesIncidentes:ResolucionesVista
    {
        private List<ResolucionesIncidentesVista> lookupRes(HistoricosParametros par)
        {
            DateTime fechaHastaLastYear;
            bool tablaHistorica = false;
            List<ResolucionesIncidentesVista> resoluciones = new List<ResolucionesIncidentesVista>();
            if (par.FechaDesde < new DateTime(DateTime.Now.Year, 1, 1))
            {
                tablaHistorica = true;
            }
            using (var dbc = new IncidenciasEntities())
            {
                var area=(from u in dbc.area where u.id_area==par.idArea select u).First();
                List<int> idarea = new List<int>();
                idarea.Add(par.idArea);
                if (area.descripcion.Equals("Soporte") | area.descripcion.Equals("Telecomunicaciones"))
                {
                    idarea=(from s in dbc.area where s.id_direccion==area.id_direccion select s.id_area).ToList();
                }
                resoluciones = (from u in dbc.Resoluciones
                                where DateTime.Compare(u.fecha, par.FechaHasta) < 0 && DateTime.Compare(u.fecha, par.FechaDesde) > 0 &&
                                (idarea.Contains(u.id_area)) &&
                                (par.oblea == null || u.UnidadConf.nombre == par.oblea) &&
                                (par.nroIncidente == null || u.Incidencias.numero == par.nroIncidente) &&
                                (par.idTipoResolucion==0 || u.id_tipo_resolucion == par.idTipoResolucion) &&
                                (par.idTecnico==0 || u.id_tecnico == par.idTecnico) &&
                                (par.idJurisdiccion==0 || u.UnidadConf.id_jurisdiccion == par.idJurisdiccion)
                                select new ResolucionesIncidentesVista
                                {
                                    Direccion = u.area.Direcciones.nombre,
                                    Equipo = u.UnidadConf.nombre,
                                    Fecha = u.fecha,
                                    Observaciones = u.observaciones,
                                    Técnico = u.tecnico.descripcion,
                                    TipoResolucion = u.tipo_resolucion.descripcion,
                                    Jurisdiccion = u.Incidencias.Dependencia != null ? u.Incidencias.Dependencia.Jurisdiccion.descripcion : "N/A",
                                    NroIncidente = u.Incidencias.numero                                    
                                }).ToList();
                if (tablaHistorica)
                {
                    fechaHastaLastYear = new DateTime(DateTime.Now.Year - 1, 12, 31);
                    resoluciones.AddRange((from u in dbc.ResolucionesHistorico
                                           where DateTime.Compare(u.fecha, fechaHastaLastYear) < 0 && DateTime.Compare(u.fecha, par.FechaDesde) > 0 &&
                                           (idarea.Contains(u.id_area)) &&
                                           (par.oblea == null || u.UnidadConf.nombre== par.oblea) &&
                                           (par.nroIncidente == null || u.Incidencias.numero == par.nroIncidente) &&
                                           (par.idTipoResolucion==0 || u.id_tipo_resolucion == par.idTipoResolucion) &&
                                           (par.idTecnico==0 || u.id_tecnico == par.idTecnico) &&
                                           (par.idJurisdiccion==0 || u.UnidadConf.id_jurisdiccion == par.idJurisdiccion)
                                           select new ResolucionesIncidentesVista
                                           {
                                               Direccion = u.area.Direcciones.nombre,
                                               Equipo = u.UnidadConf.nombre,
                                               Fecha = u.fecha,
                                               Observaciones = u.observaciones,
                                               Técnico = u.tecnico.descripcion,
                                               TipoResolucion = u.tipo_resolucion.descripcion,
                                               Jurisdiccion = u.Incidencias.Jurisdiccion.descripcion,
                                               NroIncidente = u.Incidencias.numero
                                           }).ToList());
                }
            }
            return resoluciones;
        }

        public override string getResolucionesParametros(HistoricosParametros par)
        {
            return toHtmlTable(lookupRes(par));
        }

        public override string getResolucionesTotalesTecnico(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);

            IEnumerable<TecnicosCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by new { u.Técnico, u.TipoResolucion } into g
                        orderby g.Count() descending
                        select new TecnicosCount
                        {
                            Nombre = g.Key.Técnico,
                            TipoTrabajo = g.Key.TipoResolucion,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTecnico(resGroup);  
        }        

        public override string getResolucionesTotalesJurisdiccion(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);
            IEnumerable<JurisdiccionesCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.Jurisdiccion into g
                        orderby g.Count() descending
                        select new JurisdiccionesCount
                        {
                            Jurisdiccion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesJurisdiccion(resGroup);   
        }
        
        public override string getResolucionesTotalesTrabajo(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);
            IEnumerable<TipoResolucionCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.TipoResolucion into g
                        orderby g.Count() descending
                        select new TipoResolucionCount
                        {
                            TipoResolucion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTrabajo(resGroup);   
        }
        
    }
}