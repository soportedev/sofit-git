﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.AModels.ClasesVistas
{
    public class ResolucionesClonado:ResolucionesVista
    {   
        public override string getResolucionesParametros(HistoricosParametros par)
        {            
            return toHtmlTable(lookupRes(par));
        }
        private List<ResClonadoVista> lookupRes(HistoricosParametros par)
        {
            DateTime fechaHastaLastYear;
            bool tablaHistorica = false;
            List<ResClonadoVista> resoluciones = new List<ResClonadoVista>();
            if (par.FechaDesde < new DateTime(DateTime.Now.Year, 1, 1))
            {
                tablaHistorica = true;
            }            
            using (var dbc = new IncidenciasEntities())
            {

                resoluciones = (from u in dbc.Resoluciones
                                where DateTime.Compare(u.fecha, par.FechaHasta) < 0 && DateTime.Compare(u.fecha, par.FechaDesde) > 0 &&
                                (u.id_area == par.idArea) &&
                                (par.oblea == null || u.UnidadConf.nombre == par.oblea) &&
                                (par.nroIncidente == null || u.Incidencias.numero == par.nroIncidente) &&
                                (par.idTipoResolucion==0 || u.id_tipo_resolucion == par.idTipoResolucion) &&
                                (par.idTecnico==0 || u.id_tecnico == par.idTecnico) &&
                                (par.idJurisdiccion==0 || u.UnidadConf.id_jurisdiccion == par.idJurisdiccion)
                                orderby u.fecha descending
                                select new ResClonadoVista
                                {
                                    Direccion = "Soporte Técnico",
                                    Equipo = u.UnidadConf.nombre,                                    
                                    Fecha = u.fecha,
                                    Observaciones = u.observaciones,
                                    Técnico = u.tecnico.descripcion,
                                    TipoResolucion = u.tipo_resolucion.descripcion,
                                    ImagenUsada = u.Imagenes.nombre,
                                    NroIncidente = u.Incidencias.numero,
                                    Jurisdiccion=u.UnidadConf.Jurisdiccion.descripcion
                                }).ToList();
                if (tablaHistorica)
                {
                    fechaHastaLastYear = new DateTime(DateTime.Now.Year - 1, 12, 31);
                    resoluciones.AddRange((from u in dbc.ResolucionesHistorico
                                           where DateTime.Compare(u.fecha, fechaHastaLastYear) < 0 && DateTime.Compare(u.fecha, par.FechaDesde) > 0 &&
                                           (u.id_area == par.idArea) &&
                                           (par.oblea == null || u.UnidadConf.nombre == par.oblea) &&
                                           (par.nroIncidente == null || u.Incidencias.numero == par.nroIncidente) &&
                                           (par.idTipoResolucion==0 || u.id_tipo_resolucion == par.idTipoResolucion) &&
                                           (par.idTecnico==0 || u.id_tecnico == par.idTecnico) &&
                                           (par.idJurisdiccion==0 || u.UnidadConf.id_jurisdiccion == par.idJurisdiccion)
                                           orderby u.fecha descending
                                           select new ResClonadoVista
                                           {
                                               Direccion = "Soporte Técnico",
                                               Equipo = u.UnidadConf.nombre,
                                               Fecha = u.fecha,
                                               Observaciones = u.observaciones,
                                               Técnico = u.tecnico.descripcion,
                                               TipoResolucion = u.tipo_resolucion.descripcion,
                                               ImagenUsada = u.Imagenes.nombre,
                                               NroIncidente = u.Incidencias.numero,
                                               Jurisdiccion = u.UnidadConf.Jurisdiccion.descripcion
                                           }).ToList());
                }
            }
            return resoluciones;
        }
        public override string getResolucionesTotalesTecnico(HistoricosParametros par)
        {
            List<ResClonadoVista> resoluciones = lookupRes(par);
            
            IEnumerable<TecnicosCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by new { u.Técnico, u.TipoResolucion } into g
                        orderby g.Count() descending
                        select new TecnicosCount
                        {
                            Nombre = g.Key.Técnico,
                            TipoTrabajo = g.Key.TipoResolucion,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTecnico(resGroup);            
        }

        public override string toHtmlTotalesTecnico(IEnumerable<TecnicosCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Técnico</th><th>Tipo de Trabajo</th><th>Total</th></tr>");
                foreach (TecnicosCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.Nombre + "</td>");
                    sb.Append("<td>" + j.TipoTrabajo + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }

        public override string getResolucionesTotalesJurisdiccion(HistoricosParametros par)
        {
            List<ResClonadoVista> resoluciones = lookupRes(par);
            IEnumerable<JurisdiccionesCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.Jurisdiccion into g
                        orderby g.Count() descending
                        select new JurisdiccionesCount
                        {
                            Jurisdiccion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesJurisdiccion(resGroup);   
        }

        public override string toHtmlTotalesJurisdiccion(IEnumerable<JurisdiccionesCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Jurisdicción</th><th>Total</th></tr>");
                foreach (JurisdiccionesCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.Jurisdiccion + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }

        public override string getResolucionesTotalesTrabajo(HistoricosParametros par)
        {
            List<ResClonadoVista> resoluciones = lookupRes(par);
            IEnumerable<TipoResolucionCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.TipoResolucion into g
                        orderby g.Count() descending
                        select new TipoResolucionCount
                        {
                            TipoResolucion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTrabajo(resGroup);   
        }

        public override string toHtmlTotalesTrabajo(IEnumerable<TipoResolucionCount> resGroup)
        {
            StringBuilder sb = new StringBuilder();
            if (resGroup.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Tipo de Trabajo</th><th>Total</th></tr>");
                foreach (TipoResolucionCount j in resGroup)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + j.TipoResolucion + "</td>");
                    sb.Append("<td>" + j.Cantidad + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        private string toHtmlTable(List<ResClonadoVista> resoluciones)
        {            
            StringBuilder sb = new StringBuilder();
            if (resoluciones.Count() == 0) sb.Append("No se encontraron resultados");
            else
            {
                sb.Append("<table class='info'><tr class='ui-state-active'><th>Fecha</th><th>Oblea</th><th>Incidente</th><th>Tipo Resolución</th>");
                sb.Append("<th>Técnico</th><th>Jurisdicción</th><th>Imágen Usada</th><th>Notas</th></tr>");
                foreach (ResClonadoVista r in resoluciones)
                {
                    sb.Append("<tr>");

                    DateTime sDate = r.Fecha;
                    sb.Append("<td>" + sDate.ToShortDateString() + "</td>");
                    sb.Append("<td>" + r.Equipo + "</td>");
                    sb.Append("<td class='verIncidente'>" + r.NroIncidente + "</td>");
                    sb.Append("<td>" + r.TipoResolucion + "</td>");
                    sb.Append("<td>" + r.Técnico + "</td>");
                    sb.Append("<td>" + r.Jurisdiccion + "</td>");
                    sb.Append("<td>" + r.ImagenUsada + "</td>");
                    sb.Append("<td><a href='JavaScript:void(0)' data-res='" + r.Observaciones + "' class='verInfo'>Ver</a></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();        
        }
    }
}