﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Statics
{
    public class Constants
    {
       
        public enum ids
        {
            motherboard=1,
            hd=2,
            ram=3,
            optico=4,
            red=5,
            video=6,
            micro=7,
            pc=8,
            impresora=9,
            monitor=10,
            varios=11
        }
        public enum TMovAlmacen
        {
            Nuevo=1,
            AjusteManual=2,
            Agregacion=3,
            Retiro=4,
            Eliminacion=5
        }
        public enum tecnologiaRed
        {
            Ethernet,
            Wireless
        }
        
        public enum tiposEquipos
        {
            pc=1,
            impresora=2,
            monitor=3,
            varios=4
        }
        public enum areas
        {
            Clonado=1,
            Laboratorio=2,
            AtUsuarios=3,
            Logística=4,
            Soporte=5,
            Deposito=6,
            Garantia=7,
            Baja=8
        }
        public enum dominios
        {
            Gobiernocba,
            RHC,
            Gobernacion
        }
        ////public enum estadoEquipo
        ////{
        ////    Activo=1,
        ////    Baja=2,
        ////    Roto=3,
        ////    Suspendido=4,
        ////    Terminado=5,
        ////    Entregar=6,
        ////    Deposito=7,
        ////    Obsoleto=8,
        ////    AGarantia=9,
        ////    EnGarantia=10,
        ////    Donación=11,
        ////    Stock=12
        ////}
        
        public enum perfiles
        {
            Administrador=1,
            Usuario_Clonado=2,            
            Usuario_Logística=3,
            Atencion_Usuarios=4,
            Usuario_Laboratorio = 5,
            Inventario=6
        }
        public enum ComplejidadPassword
        {
            MuyCorto,
            FaltanNumeros,
            PasswordRepetida,
            Ok
        }        
        public enum NombreVentana
        {
            DivEqClon,
            DivEqDel,
            DivEqLab,
            DivEqSus,
            DivIncAt,
            DivIncIng,
            DivIncLog,
            DivEqParaGar,
            DivEqEnGar
        }
    }
}
