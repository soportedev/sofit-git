﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Areas.Soporte.Models.Statics;

namespace soporte.Areas.Soporte.Models
{
    public class Mensaje
    {
        public int idMensaje { get; set; }
        public int idUsuario { get; set; }
        public string mensaje { get; set; }
        public bool fueLeido { get; set; }
        public DateTime fecha { get; set; }
        public bool repetir { get; set; }

        internal string saveDB()
        {
            string result = string.Empty;
            try
            {
                //MensajesDB.getAdapter().Update(this.idUsuario, this.mensaje, this.fecha, this.fueLeido, this.repetir, this.idMensaje);
                result = "ok";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }
    }
}
