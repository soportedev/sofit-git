﻿using soporte.Models;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class AreaIncDeposito:IAreaIncidencia
    {
        public Incidencia Incidente { get; set; }
        public AreaIncDeposito(int idArea, string nombrea)
        {
            base.IdArea = idArea;
            base.NombreArea = nombrea;
        }
        public override int getIdTecnicoGenerico()
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses derivar(int idArea, int tecnico, int usuario)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses solucionar(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo, DateTime fechaActualizar, string imagen)
        {
            throw new NotImplementedException();
        }
    }
}