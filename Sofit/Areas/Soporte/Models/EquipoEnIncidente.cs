﻿using soporte.Areas.Soporte.Models.Displays;
using soporte.Areas.Soporte.Models.Factory;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
namespace soporte.Areas.Soporte.Models
{
    public class EquipoEnIncidente
    {
        private int idtipo;
        private int idestado;
        private int idarea;
        private EstadoEquipo estadoActual;
        private AreaEquipo backArea;
        public Incidencia incidencia { get; set; }
        public int idEquipo { get; set; }
        public string nroOblea { get; set; }
        public string nroSerie;
        public string Serie { 
            get{
                if (nroSerie == "") return "S/N";
                else return nroSerie;
            }
            set {
                this.nroSerie = value;
            } 
        }
        public string Model
        {
            get
            {
                if (modelo == "") return "N/A";
                else return modelo;
            }
            set
            {
                this.modelo = value;
            }
        }
        public string modelo;
        public int idTipo
        {
            get { return idtipo; }
            set
            {
                this.idtipo = value;
                using (var dbc = new IncidenciasEntities())
                {
                    string descTipo = dbc.Tipo_equipo.Where(c => c.id_tipo == this.idtipo).Single().descripcion;
                    switch (descTipo)
                    {
                        case "Impresora":
                            Impresoras impresora = dbc.Impresoras.Where(c => c.id_uc == this.idEquipo).First();
                            this.Model = impresora.Modelo_Impresora != null ? impresora.Modelo_Impresora.descripcion : "N/A";
                            this.idMarca = impresora.Modelo_Impresora != null ? impresora.Modelo_Impresora.id_marca.Value : 0;
                            this.marca = impresora.Modelo_Impresora != null ? impresora.Modelo_Impresora.Marca.descripcion : "N/A";
                            break;
                        case "Monitor":
                            Monitores esteMon = dbc.Monitores.Where(c => c.id_uc == this.idEquipo).First();
                            this.Model = esteMon.Modelo_Monitor != null ? esteMon.Modelo_Monitor.descripcion : "N/A";
                            this.idMarca = esteMon.Modelo_Monitor != null ? esteMon.Modelo_Monitor.id_marca : 0;
                            this.marca = esteMon.Modelo_Monitor != null ? esteMon.Modelo_Monitor.Marca.descripcion : "N/A";
                            break;
                        case "Pc":
                            Sofit.DataAccess.Pc estepc = dbc.Pc.Where(c => c.id_uc == this.idEquipo).First();
                            this.Model = estepc.modelo;
                            this.idMarca = estepc.id_marca;
                            this.idDependencia = dbc.Pc.Where(c => c.id_uc == this.idEquipo).First().id_dependencia;
                            this.marca = estepc.Marca.descripcion;
                            break;
                        default: Varios v = dbc.Varios.Where(c => c.id_uc == this.idEquipo).FirstOrDefault();
                            if (v != null)
                            {
                                this.idDependencia = v.id_dependencia.Value;
                                this.idMarca = v.Marca.id_marca;
                                this.Model = v.modelo;
                                this.marca = v.Marca.descripcion;
                            }
                            break;
                    }
                }//using
            }
        }
        public int idDependencia { get; set; }
        public string observaciones { get; set; }
        public int idMarca { get; set; }        
        public int idUsuario { get; set; }
        public int idTecnico { get; set; }
        public DateTime fecha { get; set; }
        public string tipo { get; private set; }
        public string dependencia { get; private set; }
        public string marca { get; private set; }
        public int idEstado { 
            get
            {
                return idestado;
            }
            set
            {
                this.idestado = value;
                this.estadoActual = EstadoEquipoFactory.getEstadoPorId(this.idestado);
                this.estadoActual.setEquipo(this);
            }
        }
        public int idArea { 
            get
            {
                return idarea;
            }
            set
            {
                this.idarea = value;
                this.areaActual = AreaEquipoFactory.getAreaXId(this.idarea);
                this.areaActual.setEquipo(this);
            }
        }
        public string observacionDerivacion { get; set; }
        public List<MovAreaEquipo> AreasActuantes;
        public List<MovEstados>EstadosActuantes;
        
        public AreaEquipo areaActual
        {
            get
            {
                return backArea;
            }
            set
            {
                backArea = value;                
            }
        }        
        internal void LoadHistorialEstados()
        {
            this.EstadosActuantes = new List<MovEstados>();
            using (var dbc = new IncidenciasEntities())
            {
                var mov = (from u in dbc.EstadoXEquipoN
                           where u.id_incidencia == this.incidencia.Id & u.id_equipo == this.idEquipo
                           select u).ToList();
                foreach (var v in mov)
                {
                    DateTime fechaHastaParsed = (v.fecha_fin != null ? v.fecha_fin.Value : DateTime.Now);
                    List<ResToDisplay> resoluciones = (from s in dbc.Resoluciones
                                                             where s.id_incidencia == this.incidencia.Id && s.id_uc == this.idEquipo && DateTime.Compare(s.fecha, fechaHastaParsed) < 0 && DateTime.Compare(s.fecha, v.fecha_inicio) > 0
                                                             //where s.id_incidencia == idIncidencia && (s.fecha <= (v.fecha_fin!=null?v.fecha_fin:DateTime.Now) && s.fecha >= v.fecha_inicio)
                                                             select new ResToDisplay
                                                             {
                                                                 Fecha = s.fecha,
                                                                 Observ = s.observaciones,
                                                                 Tecnico = s.tecnico.descripcion,
                                                                 TipoRes = s.tipo_resolucion.descripcion                                                                 
                                                             }).ToList();
                    MovEstados mar = new MovEstados
                    {
                        Estado = EstadoEquipoFactory.getEstadoPorId(v.id_estado),
                        FechaDesde = v.fecha_inicio,
                        FechaHasta = v.fecha_fin,
                        Resoluciones = resoluciones
                    };
                    this.EstadosActuantes.Add(mar);
                }
            }
        }
        public void setEstado(EstadoEquipo estado)
        {
            this.estadoActual=estado;
            estadoActual.setEquipo(this);
            this.idEstado=estadoActual.idEstado;            
        }
        public void setArea(AreaEquipo a)
        {
            this.areaActual = a;
            areaActual.setEquipo(this);
            if (areaActual is AreaConUbicacion)
            {
                AreaConUbicacion estaArea=areaActual as AreaConUbicacion;
                estaArea.setEquipo(this);
                UbicacionManager um = new UbicacionManager();
                //estaArea.setUbicacion(um.GetUbicacionEquipo(this.idEquipo));
            }
            this.idArea = areaActual.getIdArea();
        }
        public Responses cambioEstado(EstadoEquipo estado, int tecnico)
        {
            this.estadoActual = estado;
            estadoActual.setEquipo(this);
            this.idEstado = estadoActual.idEstado;
            return RegistrarBaseCambioEstadoEquipo(tecnico);
        }
        public Responses cambioArea(AreaEquipo a, int idTecnico)
        {
            this.areaActual = a;
            this.idTecnico = idTecnico;
            return this.RegistrarBaseMovimientoArea();
        }
        public AreaEquipo getAreaActual()
        {
            return areaActual;
        }
        public EstadoEquipo getEstadoActual()
        {
            return estadoActual;
        }   

        private Responses RegistrarBaseCambioEstadoEquipo(int idTec)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var eqxinc = (from u in dbc.EquiposXIncidencia
                                  where u.id_equipo == this.idEquipo
                                  select u).Single();
                    //no cambiar el estado del equipo
                    //var equipo = (from u in dbc.UnidadConf
                    //              where u.id_uc == this.idEquipo
                    //              select u).Single();
                    //switch(equipo.Tipo_equipo.descripcion)
                    //{
                    //    case "Pc": equipo.Pc.Where(c => c.id_uc == equipo.id_uc).Single().id_estado = estadoActual.idEstado;
                    //        break;
                    //    case "Impresora": equipo.Impresoras.Where(c => c.id_uc == equipo.id_uc).Single().id_estado = estadoActual.idEstado;
                    //        break;
                    //    case "Monitor": equipo.Monitores.Where(c => c.id_uc == equipo.id_uc).Single().id_estado = estadoActual.idEstado;
                    //        break;
                    //    case "Varios": equipo.Varios.Where(c => c.id_uc == equipo.id_uc).Single().id_estado = estadoActual.idEstado;
                    //        break;
                    //}
                    eqxinc.id_estado = estadoActual.idEstado;
                    eqxinc.fecha_inicio = DateTime.Now;
                    EstadoXEquipoN ex = (from u in dbc.EstadoXEquipoN
                                       where u.id_equipo == this.idEquipo & !u.fecha_fin.HasValue
                                       select u).Single();
                    ex.fecha_fin = DateTime.Now;
                    EstadoXEquipoN exi = new EstadoXEquipoN
                    {
                        id_estado = this.estadoActual.idEstado,
                        id_incidencia = this.incidencia.Id,
                        fecha_inicio = DateTime.Now,
                        id_tecnico = idTec,
                        id_equipo = this.idEquipo
                    };
                    dbc.EstadoXEquipoN.Add(exi);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Info = "Error";
                result.Detail = e.Message;
            }
            return result;
        }
        private Responses RegistrarBaseMovimientoArea()
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var eqxinc = (from u in dbc.EquiposXIncidencia
                                  where u.id_equipo == this.idEquipo
                                  select u).Single();
                    eqxinc.id_area = areaActual.getIdArea();
                    eqxinc.id_tecnico = idTecnico;                    
                    AreaXEquipoN ax = (from u in dbc.AreaXEquipoN
                                          where u.id_equipo == this.idEquipo & !u.fecha_fin.HasValue
                                          select u).Single();
                    ax.fecha_fin = DateTime.Now;
                    AreaXEquipoN axi = new AreaXEquipoN
                    {
                        id_area = this.areaActual.getIdArea(),
                        id_incidencia = this.incidencia.Id,
                        fecha_inicio = DateTime.Now,
                        id_tecnico=this.idTecnico,
                        id_equipo=this.idEquipo
                    };
                    dbc.AreaXEquipoN.Add(axi);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Info = "Error";
                result.Detail = e.Message;
            }
            return result;
        }
        internal Responses SolucionarEquipo()
        {
            Responses result = new Responses();            
            MovAreaEquipo ultimaArea = AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "El equipo no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    try
                    {
                        using (var dbc = new IncidenciasEntities())
                        {
                            EstadoEquipo estadoTerminado = EstadoEquipoFactory.getEstadoPorNombre("Terminado", "Soporte Técnico");
                            this.estadoActual = estadoTerminado;
                            var eqxinc = (from u in dbc.EquiposXIncidencia
                                          where u.id_equipo == this.idEquipo
                                          select u).Single();
                            eqxinc.id_estado = estadoTerminado.idEstado;                            
                            AreaXEquipoN ax = (from u in dbc.AreaXEquipoN
                                               where u.id_equipo == this.idEquipo & !u.fecha_fin.HasValue
                                               select u).Single();
                            ax.fecha_fin = DateTime.Now;
                            EstadoXEquipoN ex = (from u in dbc.EstadoXEquipoN
                                                 where u.id_equipo == this.idEquipo & !u.fecha_fin.HasValue
                                                 select u).Single();
                            ex.fecha_fin = DateTime.Now;
                            //UnidadConf eq = (from u in dbc.UnidadConf
                            //             where u.id_uc == this.idEquipo
                            //             select u).Single();
                            //eq.id_estado = estadoTerminado.idEstado;
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }           
                        
                    }
                    catch (Exception e)
                    {
                        result.Info = "Error";
                        result.Detail = e.Message;
                    }
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar la resolución de " + ultimaArea.Area.nombreArea;
                }
            }
            return result;
        }    
        public override bool Equals(object obj)
        {
            EquipoEnIncidente other = obj as EquipoEnIncidente;
            if (this.idEquipo == other.idEquipo) return true;
            else return false;
        }
        public override int GetHashCode()
        {
            return idEquipo.GetHashCode();
        }
        internal Responses cargarSolucion(int idUsuario, int tipoResolucion, int tecnico, int turno, int? imagen, string archivo, string observacion,bool updateLast)
        {
            Responses result = new Responses();
            if (updateLast)
            {                
                MovAreaEquipo ultimoMov = AreasActuantes.Last();
                ultimoMov.Resoluciones.Sort();
                if (ultimoMov.Resoluciones.Count > 0)
                {
                    ResolucionesEquipo ultimaresolucion = ultimoMov.Resoluciones.Last();
                    if (ultimaresolucion.IdUsuario == idUsuario)
                    {
                        try
                        {
                            using (var dbc = new IncidenciasEntities())
                            {
                                var res = (from u in dbc.Resoluciones
                                           where u.id_resolucion == ultimaresolucion.idResolucion
                                           select u).Single();
                                res.fecha = DateTime.Now;
                                res.observaciones = observacion;
                                res.id_tecnico = tecnico;
                                res.id_turno = turno;
                                res.id_imagen = imagen;
                                res.id_tipo_resolucion = tipoResolucion;
                                res.path_imagen = archivo == string.Empty ? null : archivo;
                                dbc.SaveChanges();
                                result.Info = "ok";
                            }
                        }
                        catch (Exception e) { result.Detail = e.Message; }
                    }
                    else
                    {
                        result.Info = "error";
                        result.Detail = "El Usuario que cargó la última resolución no coincide con el actual!!";
                    }
                }//si tiene resoluciones
                else
                {
                    result.Info = "error";
                    result.Detail = "No hay resoluciones previas cargadas!!!";
                }
            }//si es update
            else
            {
                result = areaActual.cargarSolucion(idUsuario, tipoResolucion, tecnico, turno, imagen,archivo, observacion);
            }
            return result;
        }
        
        internal Responses Derivar(AreaEquipo area,string observaciones,int idUsuario, int idTecnico)
        {
            Responses result = new Responses();            
            MovAreaEquipo ultimaArea = AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "El equipo no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    result = this.areaActual.derivar(area, idUsuario, idTecnico);
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar una resolución.";
                }
            }
            return result;            
        }
        internal string getUbicacion()
        {
            AreaEquipo area = getAreaActual();
            string result = string.Empty;
            if (area is AreaConUbicacion)
            {
                result = (area as AreaConUbicacion).getUbicacion().Nombre;
            }
            else result = "El equipo no tiene ubicación";
            return result;
        }       
        public Responses Ubicar(int idArmario)
        {
            construir();
            AreaEquipo area = getAreaActual();
            Responses result = new Responses();
            try
            {
                result = estadoActual.setUbicacion(idArmario);                
            }
            catch (NotImplementedException)
            {
                result.Detail = "El estado del equipo no corresponde. Verifique si fue Notificado.";
            }
            return result;
        }
        /// <summary>
        /// cuando necesite construir un equipo sin ticket
        /// </summary>
        private void construir()
        {
            using (var dbc = new IncidenciasEntities())
            {
                var equipo = dbc.EquiposXIncidencia.Where(c => c.id_equipo == idEquipo).Single();
                this.nroOblea = equipo.UnidadConf.nombre;
                this.Serie = equipo.UnidadConf.nro_serie;
                this.idEstado = equipo.id_estado;
                this.idTipo = equipo.UnidadConf.id_tipo;
                this.idArea = equipo.id_area;
            }
        }
        internal Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject)
        {        
                return areaActual.notificarEquipo(referentes,firma,cuerpo,subject);            
        }
    }
}
