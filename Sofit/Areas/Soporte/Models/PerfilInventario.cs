﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class PerfilInventario:Perfil
    {
        public PerfilInventario()
        {
            this.nombre = "Inventario";
            this.nombreBase = "Inventario";
        }
        public override Constantes.AccesosPagina getAcceso(string page)
        {
            return Constantes.AccesosPagina.NoAccess;
        }

        public override string getHomePage()
        {
            return "Soporte/Incidentes";
        }
    }
}