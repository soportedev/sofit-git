﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class IncidenteBasic : IComparable
    {
        public int id { get; set; }
        public string NroIncidente { get; set; }
        public DateTime fInicio { get; set; }
        public DateTime fFin { get; set; }
        public string FechaInicio
        {
            get
            {
                return fInicio.ToShortDateString();
            }
            private set{}
        }
        public string FechaFin
        {
            get
            {
                return fFin.ToShortDateString();
            }
            private set { }
        }
        public string Jurisdiccion { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is IncidenteBasic)
            {
                IncidenteBasic other = (IncidenteBasic)obj;
                if (this.id == other.id) return true;
                else return false;
            }
            else return false;
        }
        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            IncidenteBasic other = (IncidenteBasic)obj;
            if (fInicio > other.fInicio)
                return 1;
            else if (fInicio < other.fInicio)
                return -1;
            else return 0;
        }
    }
}
