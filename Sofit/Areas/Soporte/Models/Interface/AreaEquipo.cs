﻿using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Areas.Soporte.Models.Interface
{
    public abstract class AreaEquipo
    {
        public int idArea;
        public string nombreArea;
        public EquipoEnIncidente equipo;        
        public void setEquipo(EquipoEnIncidente equipo)
        {
            this.equipo = equipo;
        }
        public abstract string getNombreArea();
        public abstract int getTecnicoGenerico();
        public abstract int getIdArea();
        internal abstract Responses cargarSolucion(int idUsuario, int tResolucionClonado, int tecnico, int turno, int? imagen, string archivo, string observacion);
        public abstract Responses derivar(AreaEquipo area,int idUsuario,int idTecnico);
        public abstract Responses terminarEquipo();
        public abstract Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject);
    }
}
