﻿
using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.Statics;
namespace soporte.Areas.Soporte.Models
{
    public abstract class EstadoEquipo
    {
        public int idEstado;
        public string nombreEstado;
        public int idDireccion { get; set; }
        public string NombreDireccion { get; set; }
        protected EquipoEnIncidente equipo;
        public void setEquipo(EquipoEnIncidente equipo)
        {
            this.equipo = equipo;
        }
        
        public abstract Responses cargarSolucion(int idUsuario, string fecha, int idTipoResolucion, int tecnico, int turno, int? imagen, string observacion);

        public abstract Responses agregarAIncidente(AreaEquipo area);
        public abstract Responses SolucionarEquipo();
        public abstract Responses pasarArea(AreaEquipo area);
        public abstract Responses suspender();
        public abstract Responses paraGarantia();
        public abstract Responses enGarantia();
        public abstract Responses updateDb();
        public abstract Responses notificarMail();
        public abstract Responses setUbicacion(int idArmario);
    }
}
