﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Areas.Soporte.Models.Interface
{
    public interface ITieneNroEId
    {
        int GetId();
        string GetNumero();
        
    }
}
