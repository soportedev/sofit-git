﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class PerfilGuest:Perfil
    {
        public PerfilGuest()
        {
            this.nombre = "Invitado";
            this.nombreBase = "Guest";
        }
        public override Constantes.AccesosPagina getAcceso(string page)
        {
            return Constantes.AccesosPagina.NoAccess;
        }

        public override string getHomePage()
        {
            return "Soporte/Incidentes";
        }
    }
}