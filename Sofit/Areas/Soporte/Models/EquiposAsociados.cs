﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace soporte.Areas.Soporte.Models
{
    public class EquiposAsociados:System.Object
    {
        public int id { get; set; }
        public string oblea { get; set; }
        public List<MovAreaEquipo> areas;
        public List<MovEstados> estados;

        public EquiposAsociados()
        {
            areas = new List<MovAreaEquipo>();
            estados = new List<MovEstados>();
        }
        public void addAreas(MovAreaEquipo a)
        {
            areas.Add(a);
        }
        public void setAreas(List<MovAreaEquipo> area)
        {
            this.areas = area;
        }
        public void addEstados(MovEstados e)
        {
            estados.Add(e);
        }
        public override bool Equals(Object eq)
        {
            if (eq == null) return false;
            EquiposAsociados equipo = (EquiposAsociados)eq;
            if (this.oblea == equipo.oblea) return true;
            else return false;
        }

        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
        public override int GetHashCode()
        {
            return oblea.GetHashCode();
        }

        #endregion
    }
}
