﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using soporte.Areas.Soporte.Models.Interface;


namespace soporte.Areas.Soporte.Models
{
    public class HistoricoIncidente:ITieneNroEId
    {        
        public int Id { get; set; }
        public string Numero { get; set; }
        public string tipologia { get; set; }
        public string jurisdiccion { get; set; }
        public string Dependencia { get; set; }
        public List<MovAreaIncidente> areas;
        public List<EquiposAsociados> equipos;        
        public HistoricoIncidente(string nro)
        {
            this.Numero = nro;
            areas = new List<MovAreaIncidente>();
            equipos = new List<EquiposAsociados>();           
            Load();
        }
        //public HistoricoIncidente(int id, string nro, string top, string jur):base(nro)
        //{
        //    base.Id = id;
        //    base.Numero = nro;
        //    this.tipologia = top;
        //    this.jurisdiccion = jur;
        //    areas = new List<MovAreaIncidente>();
        //    equipos = new List<EquiposAsociados>();            
        //}
       
        public void addAreas(List<MovAreaIncidente> a)
        {
            areas=a;
        }
        public void addEquipos(List<EquiposAsociados> eq)
        {
            equipos = eq;
        }

        private void Load()
        {
            
        }

        internal void addEquipo(EquiposAsociados equipoAsociado)
        {
            this.equipos.Add(equipoAsociado);
        }

        internal bool tieneEquipo(int idEquipo)
        {
            bool flag = false;
            foreach (EquiposAsociados equipo in equipos)
            {
                if (equipo.id == idEquipo)
                {
                    flag = true;
                    break;
                }

            }
            return flag;
        }

        public int GetId()
        {
            return this.Id;
        }

        public string GetNumero()
        {
            return this.Numero;
        }
    }
}
