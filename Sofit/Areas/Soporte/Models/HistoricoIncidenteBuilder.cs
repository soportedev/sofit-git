﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace soporte.Areas.Soporte.Models
{
    public class HistoricoIncidenteBuilder
    {
        HistoricoIncidente histIncidente;
        public HistoricoIncidenteBuilder(HistoricoIncidente i)
        {
            histIncidente = i;
        }
        public void Construir()
        {
            AddAreas();
            AddEquipos();
        }
        private void AddAreas()
        {
            
        }
        private void AddEquipos()
        {
            
        }
    }
}
