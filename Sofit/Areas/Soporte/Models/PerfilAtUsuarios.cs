﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class PerfilAtUsuarios:Perfil
    {
        public PerfilAtUsuarios()
        {
            this.nombre = "At.Usuarios";
            this.nombreBase = "Usuario At.Usuarios";
        }
        public override Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.ReadOnly;
            switch (page)
            {
                case "Soporte/Incidentes": acceso = Constantes.AccesosPagina.Full;
                    break;               
                case "Soporte/Config": acceso = Constantes.AccesosPagina.Full;
                    break;
                case "Soporte/Inventario": acceso = Constantes.AccesosPagina.Full;
                    break;
                case "Soporte/Historicos": acceso = Constantes.AccesosPagina.Full;
                    break;
                case "Soporte/Deposito": acceso = Constantes.AccesosPagina.NoAccess;
                    break;
                case "Soporte/Tiempos": acceso = Constantes.AccesosPagina.NoAccess;
                    break;
                case "Soporte/Usuarios": acceso = Constantes.AccesosPagina.NoAccess;
                    break;
                case "Soporte/Jurisdicciones": acceso = Constantes.AccesosPagina.NoAccess;
                    break;
                case "Soporte/Tipificaciones": acceso = Constantes.AccesosPagina.NoAccess;
                    break;
                default: acceso = Constantes.AccesosPagina.NoAccess;
                    break;
            }
            return acceso;
        }

        public override string getHomePage()
        {
            return "Soporte/Incidentes";
        }
    }
}