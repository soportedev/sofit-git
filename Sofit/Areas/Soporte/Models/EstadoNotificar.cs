﻿using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class EstadoNotificar:EstadoEquipo
    {
        public EstadoNotificar(int idEstado, string nombre)
        {
            base.idEstado = idEstado;
            base.nombreEstado = nombre;
        }
        public override Responses cargarSolucion(int idUsuario, string fecha, int idTipoResolucion, int tecnico, int turno, int? imagen, string observacion)
        {
            throw new NotImplementedException();
        }

        public override Responses SolucionarEquipo()
        {
            throw new NotImplementedException();
        }

        public override Responses pasarArea(AreaEquipo area)
        {
            throw new NotImplementedException();
        }

        public override Responses suspender()
        {
            throw new NotImplementedException();
        }

        public override Responses paraGarantia()
        {
            throw new NotImplementedException();
        }

        public override Responses enGarantia()
        {
            throw new NotImplementedException();
        }

        public override Responses updateDb()
        {
            throw new NotImplementedException();
        }

        public override Responses notificarMail()
        {
            throw new NotImplementedException();
        }

        public override Responses setUbicacion(int idArmario)
        {
            throw new NotImplementedException();
        }

        public override Responses agregarAIncidente(AreaEquipo area)
        {
            throw new NotImplementedException();
        }
    }
}