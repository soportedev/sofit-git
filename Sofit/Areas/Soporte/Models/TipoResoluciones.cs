﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using soporte.Areas.Soporte.Models.Statics;

namespace soporte.Areas.Soporte.Models
{
    public static class TipoResoluciones
    {
        static List<Tresoluciones> tResoluciones = new List<Tresoluciones>();
        
        static TipoResoluciones()
        {
            
        }
        public static List<Tresoluciones> getTResClonado(){
            var resClonado = from tres in tResoluciones
                                 where tres.idArea == (int)Constants.areas.Clonado
                                 select tres;
            return resClonado.ToList();
        }
        public static Tresoluciones getResolucionXId(int id)
        {
            Tresoluciones t = (from tres in tResoluciones
                               where tres.idArea == id
                               select tres).FirstOrDefault();
            return t;
        }
        public static List<Tresoluciones> getTResLaboratorio()
        {
            var resLaboratorio = from tres in tResoluciones
                                 where tres.idArea == (int)Constants.areas.Laboratorio
                                 select tres;
            return resLaboratorio.ToList();
        }
        public static List<Tresoluciones> getTResAtUsuarios()
        {
            var resAtUsuarios = from tres in tResoluciones
                                where tres.idArea == (int)Constants.areas.AtUsuarios
                                select tres;
            return resAtUsuarios.ToList();
        }
        internal static List<Tresoluciones> getTResLogistica()
        {
            var resAtUsuarios = from tres in tResoluciones
                                where tres.idArea == (int)Constants.areas.Logística
                                select tres;
            return resAtUsuarios.ToList();
        }
        public static string getDescripcion(int val)
        {
            List<string> desc = (from tres in tResoluciones
                                 where tres.idResolucion == val
                                 select tres.descripcion).ToList();
            return desc[0].ToString();
        }
        public class Tresoluciones
        {
            public int idResolucion { get; set; }
            public string descripcion { get; set; }
            public int idArea { get; set; }
        }
        internal static int getResolucionPasoGarantia()
        {
            var res = (from tres in tResoluciones
                                 where tres.descripcion=="En Garantía"
                                 select tres).FirstOrDefault();
            return res.idResolucion;
        }
        internal static int getResolucionDiagGarantia()
        {
            var res = (from tres in tResoluciones
                       where tres.descripcion == "Diagnóstico Garantía"
                                 select tres).FirstOrDefault();
            return res.idResolucion;
        }
    }
}
