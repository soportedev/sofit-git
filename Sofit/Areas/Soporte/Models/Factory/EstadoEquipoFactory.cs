﻿using soporte.Areas.Operaciones.Models;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Factory
{
    public class EstadoEquipoFactory
    {        
        private static List<EstadoEquipoVista> estado;
        static EstadoEquipoFactory()
        {
            using (var db = new IncidenciasEntities())
            {
                estado = (from u in db.Estado_UC
                          select new EstadoEquipoVista
                          {
                             idEstado=u.id_estado,
                             Nombre=u.descripcion,
                             idDireccion=u.Direcciones.id_direccion,
                             NombreDireccion=u.Direcciones.nombre
                          }).ToList();
            }
        }
       
        public static EstadoEquipo getEstadoPorNombre(string nombre,string direccion)
        {
            int idEstado = estado.Where(c => c.Nombre == nombre & c.NombreDireccion==direccion).Single().idEstado;
            return getEPN(idEstado, nombre);
        }
        public static EstadoEquipo getEstadoPorId(int id)
        {
            string nombre = estado.Where(c => c.idEstado == id).Single().Nombre;          
            return getEPN(id, nombre);
        }        
        private static EstadoEquipo getEPN(int idEstado, string nombre)
            {
                EstadoEquipo estado=null;    
                switch (nombre)
                {
                    case "Activo": estado = new EstadoActivo(idEstado, nombre);
                        break;
                    case "En Reparación": estado = new EstadoEnReparacion(idEstado, nombre);
                        break;
                    case "Roto": estado = new EstadoRoto(idEstado, nombre);
                        break;
                    case "Suspendido": estado = new EstadoSuspendido(idEstado, nombre);
                        break;
                    case "Terminado": estado = new EstadoTerminado(idEstado, nombre);
                        break;
                    case "Para Entregar": estado = new EstadoEntregar(idEstado, nombre);
                        break;                    
                    case "Obsoleto": estado = new EstadoObsoleto(idEstado, nombre);
                        break;
                    case "Para Garantia": estado = new EstadoParaGarantia(idEstado, nombre);
                        break;
                    case "En Garantia": estado = new EstadoEnGarantia(idEstado, nombre);
                        break;
                    case "Para Ubicar": estado = new EstadoUbicar(idEstado, nombre);
                        break;
                    case "Para Notificar": estado = new EstadoNotificar(idEstado, nombre);
                        break;
                    case "Stock": estado = new EstadoStock(idEstado, nombre);
                        break;
                    case "En Locación": estado = new EstadoEnLocacion(idEstado, nombre);
                        break;
                    case "En Reaparación": estado = new EstadoEnlaceEnReparacion(idEstado, nombre);
                        break;
                    case "No Productivo": estado = new EstadoEnlaceNoProductivo(idEstado, nombre);
                        break;
                    case "Productivo": estado = new EstadoEnlaceProductivo(idEstado, nombre);
                        break;
                }
                return estado;
            }       
        
    }
}