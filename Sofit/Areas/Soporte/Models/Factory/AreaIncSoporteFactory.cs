﻿using Sofit.DataAccess;
using soporte.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Factory
{
    public class AreaIncSoporteFactory
    {
        public static IAreaIncidencia getArea(int id)
        {
            IAreaIncidencia areaEncontrada = null;
            using (var dbc = new IncidenciasEntities())
            {
                var area = (from u in dbc.area
                             where u.id_area == id
                             select u).First();
                switch (area.descripcion)
                {
                    case "At.Usuarios": areaEncontrada = new AreaIncAtUs(area.id_area,area.descripcion);
                        break;
                    case "Logística": areaEncontrada = new AreaIncLog(area.id_area, area.descripcion);
                        break;
                    case "Soporte": areaEncontrada = new AreaIncSoporte(area.id_area, area.descripcion);
                        break;
                }
            }
            return areaEncontrada;
        }
        public static IDirecciones getDireccion(string nombre)
        {
            IAreaIncidencia areaEncontrada = null;
            IDirecciones direccion = null;
            using (var dbc = new IncidenciasEntities())
            {
                var area = (from u in dbc.area
                            where u.descripcion == nombre
                            select u).First();
                switch (area.descripcion)
                {
                    case "At.Usuarios": areaEncontrada = new AreaIncAtUs(area.id_area, area.descripcion);
                        break;
                    case "Logística": areaEncontrada = new AreaIncLog(area.id_area, area.descripcion);
                        break;
                    case "Soporte": areaEncontrada = new AreaIncSoporte(area.id_area, area.descripcion);
                        break;
                }
            }
            return direccion;
        }
    }
}