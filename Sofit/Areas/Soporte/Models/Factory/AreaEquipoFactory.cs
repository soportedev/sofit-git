﻿using soporte.Areas.Soporte.Models.Interface;
using soporte.Areas.Soporte.Models.Statics;
using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Factory
{
    public class AreaEquipoFactory
    {
        private static List<Area> areas;
        static AreaEquipoFactory()
        {
            using (var db = new IncidenciasEntities())
            {
                areas = (from u in db.area
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion,
                             idDireccion = u.id_direccion.Value
                         }).ToList();
            }
        }
        public static AreaEquipo getAreaXId(int id)
        {
            string nombre= areas.Where(c=>c.idArea==id).Single().Nombre;
            
            return getApN(id, nombre);
        }
        public static AreaEquipo getAreaXNombre(string nombre)
        {
            int id = areas.Where(c => c.Nombre == nombre).Single().idArea;
            return getApN(id, nombre);
        }
   
        private static AreaEquipo getApN(int id,string nombre)
        {
            AreaEquipo areaToReturn = null;
            switch (nombre)
            {
                case "Clonado": areaToReturn = new AreaClonado(id, nombre);
                    break;
                case "Laboratorio": areaToReturn = new AreaLaboratorio(id, nombre);
                    break;
                case "Logística": areaToReturn = new AreaLogistica(id, nombre);
                    break;
                case "Depósito": areaToReturn = new AreaDeposito(id, nombre);
                    //area = new AreaConUbicacion(area1);
                    break;
                case "Garantia": areaToReturn = new AreaGarantia(id, nombre);
                    break;                
            }
            return areaToReturn;
        }        
    }    
}