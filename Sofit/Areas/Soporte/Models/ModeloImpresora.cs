﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using soporte.Areas.Soporte.Models.Displays;

namespace soporte.Areas.Soporte.Models
{
    public class ModeloImpresora
    {
        public string tipo { get; set; }
        public string rendimiento { get; set; }
        public string tecnologia { get; set; }
        public ArrayList insumos { get; set; }
        public ArrayList conectividad { get; set; }

        public ModeloImpresora()
        {
            insumos = new ArrayList();
            conectividad = new ArrayList();
        }

        
        public void addInsumos(InsumoPr insumos)
        {
            this.insumos.Add(insumos);
        }
        public void addConectividad(string conectividad)
        {
            this.conectividad.Add(conectividad);
        }
        
        
        
    }
}
