﻿using System.Data;
using System.Collections.Generic;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Areas.Soporte.Models.Statics;
using System;

namespace soporte.Areas.Soporte.Models
{
    public class MovAreaIncidenteBuilder
    {        
        private List<MovAreaIncidente> area;
        ITieneNroEId incidente;       
        public MovAreaIncidenteBuilder(ITieneNroEId incidente)            
        {
            area = new List<MovAreaIncidente>();
            this.incidente = incidente;
        }

        public void Construir()
        {
            
        }        
        private void agregarResolucionesAtUsuarios(MovAreaIncidente a)
        {
            System.DateTime fechaFinal;
            if (a.FechaH==null)
            {
                fechaFinal = System.DateTime.Now;
            }
            else fechaFinal = a.FechaH.Value;
            
        }

        internal List<MovAreaIncidente> getResult()
        {
            return area;
        }
    }    
}
