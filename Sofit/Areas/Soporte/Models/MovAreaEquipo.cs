﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Web.Script.Serialization;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Models.ClasesVistas;
using soporte.Areas.Soporte.Models.Displays;

namespace soporte.Areas.Soporte.Models
{
    public class MovAreaEquipo:IComparable
    {
        public List<ResolucionesEquipo> Resoluciones = new List<ResolucionesEquipo>();
        public AreaEquipo Area { get; set; }
        [ScriptIgnore]
        public DateTime FechaDesde { get; set; }
        [ScriptIgnore]
        public DateTime? FechaHasta
        {
            get;
            set;
        }
        public string FechaDesdeShStr
        {
            get
            {
                return FechaDesde.ToString();
            }
            set { }
        }
        public string FechaHastaShStr
        {
            get
            {
                if (FechaHasta == null) return "";
                else return FechaHasta.Value.ToString();
            }
            set { }
        }
        public string usuario { get; set; }
        public string tecnico { get; set; }        
        public bool tieneResolucion()
        {
            if (Resoluciones.Count > 0) return true;
            else return false;
        }        
        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            MovAreaEquipo other = (MovAreaEquipo)obj;
            if (FechaDesde > other.FechaDesde)
                return 1;
            else if (FechaDesde < other.FechaDesde)
                return -1;
            else return 0;
        }
        #endregion
    }
}
