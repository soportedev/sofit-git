﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace soporte.Areas.Soporte.Models
{
    public class MovAreaIncidente:IComparable
    {
        public int idIncidente { get; set; }
        public string area { get; set; }
        public string Direccion { get; set; }
        public DateTime FechaD { get; set; }
        public DateTime ? FechaH
        {
            get;
            set;
        }
        public string fechaDesde
        {
            get
            {
                return FechaD.ToShortDateString();
            }
            set{}
        }
        public string fechaHasta 
        {
            get
            {
                if (FechaH == null) return "";
                else return FechaH.Value.ToShortDateString();
            }
            set { }
            
        }
        public string usuario { get; set; }
        public string tecnico { get; set; }                

        public MovAreaIncidente() 
        {
                      
        }
        
        public void addResolucion()
        {
            
        }       

        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            MovAreaIncidente other = (MovAreaIncidente)obj;
            if (FechaD > other.FechaD)
                return 1;
            else if (FechaD < other.FechaD)
                return -1;
            else return 0;
        }

        #endregion
    }
}
