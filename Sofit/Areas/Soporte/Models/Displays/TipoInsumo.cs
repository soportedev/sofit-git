﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class TipoInsumo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}