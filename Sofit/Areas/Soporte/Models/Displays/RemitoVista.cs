﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class RemitoVista
    {
        public string Oblea { get; set; }
        public string NroSerie { get; set; }
        public string NroTicket { get; set; }
        public string TipoDispositivo { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string  UsuarioActual { get; set; }
    }
}