﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class InsumoPr
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int idTipo { get; set; }
        public string Tipo { get; set; }
    }
}