﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class EquipoInfo
    {
        public string Oblea { get; set; }
        public string Area { get; set; }
        public string Estado { get; set; }
        public string Tecnico { get; set; }
    }
}