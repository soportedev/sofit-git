﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class TipoRamVista
    {
        public int idTipo { get; set; }
        public string Tipo { get; set; }
    }
}