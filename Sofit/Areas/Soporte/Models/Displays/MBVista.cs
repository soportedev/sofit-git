﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class MBVista
    {
        public int Id { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Chipset { get; set; }
        public string Socket { get; set; }
        public int CantUsb { get; set; }
        public int cantIde { get; set; }
        public int cantSata { get; set; }
        public int cantRam { get; set; }
        public bool PciX { get; set; }
        public bool Agp { get; set; }
    }
}