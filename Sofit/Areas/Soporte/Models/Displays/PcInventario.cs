﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class PcInventario : IEquipoInventario
    {
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }
        public MarcaVista Marca { get; set; }      
        public string Modelo { get; set; } 
        public string ClaveAdmin { get; set; }
        public string ClaveBios { get; set; }
        public string LicenciaOs { get; set; }
        public OsVista Os { get; set; }
        public MBVista MB { get; set; }
        public CpuVista Cpu { get; set; }
        public List<HdVista> Hd { get; set; }
        public List<RamVista> Ram { get; set; }
        public List<VideoVista> Video { get; set; }
        public List<NicVista> PlacaRed { get; set; }
        public List<DiscoOpticoVista> DiscoOptico { get; set; }
        public bool LectorTarjeta { get; set; }
        public string PuertoTeclado { get; set; }
        public string PuertoMouse { get; set; }
        public List<SoftVista> SoftAdicional { get; set; }
        
        public override void construir(int id)
        {
            base.construir(id);
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var pc = (from u in dbc.Pc
                              where u.id_uc == id
                              select u).First();
                    this.ClaveAdmin = pc.clave_adm;
                    this.ClaveBios = pc.clave_bios;
                    if (pc.Micro != null)
                    {
                        this.Cpu = new CpuVista
                        {
                            Cache = pc.Micro.cache,
                            Frecuencia = pc.Micro.velocidad,
                            Id = pc.Micro.id_Micro,
                            Marca = new MarcaVista
                            {
                                idMarca = pc.Micro.Marca.id_marca,
                                Nombre = pc.Micro.Marca.descripcion
                            },
                            Modelo = pc.Micro.modelo,
                            Socket = new SocketVista
                            {
                                Id = pc.Micro.Socket.id_socket,
                                Nombre = pc.Micro.Socket.descripcion
                            }
                        };
                    }
                    if (pc.Os!=null)
                    {
                        this.Os = new OsVista
                        {
                            Id = pc.Os.id_os,
                            Nombre = pc.Os.descripcion
                        };
                    }
                    if (pc.Motherboard != null)
                    {
                        this.MB = new MBVista
                        {
                            Id = pc.Motherboard.id_mother,
                            Agp = pc.Motherboard.agp.Value == 1,
                            cantRam = pc.Motherboard.nro_slot_mem.Value,
                            cantSata = pc.Motherboard.nro_sata.Value,
                            CantUsb = pc.Motherboard.nro_usb.Value,
                            Chipset = pc.Motherboard.Chipset.descripcion,
                            Marca = pc.Motherboard.Marca.descripcion,
                            Modelo = pc.Motherboard.modelo,
                            PciX = pc.Motherboard.pci_express.Value == 1,
                            Socket = pc.Motherboard.Socket.descripcion
                        };
                    }                   
                    this.Dependencia = new DependenciaVista
                    {
                        idDependencia = pc.Dependencia.id_dependencia,
                        Nombre = pc.Dependencia.descripcion,
                        Jurisdiccion = new JurisdiccionVista { idJurisdiccion = pc.Dependencia.Jurisdiccion.id_jurisdiccion, Nombre = pc.Dependencia.Jurisdiccion.descripcion }
                    };
                    if (pc.Estado_UC != null)
                    {
                        this.Estado = new EstadoEquipoVista 
                        { 
                            Nombre = pc.Estado_UC.descripcion, 
                            idEstado=pc.Estado_UC.id_estado 
                        };
                    }
                    if (pc.HD != null)
                    {
                        this.Hd = (from u in pc.HD
                                   select new HdVista
                                   {
                                       Id = u.id_disco,
                                       Capacidad = u.capacidad,
                                       Interfaz = u.Interfaz_disco.descripcion,
                                       Marca = u.Marca.descripcion

                                   }).ToList();
                    }
                    if (pc.Ram != null)
                    {
                        this.Ram = (from u in pc.Ram
                                    select new RamVista
                                    {
                                        Id = u.id_ram,
                                        Capacidad = u.capacidad,
                                        Marca = u.Marca.descripcion,
                                        Tipo = u.Tipo_ram.descripcion,
                                        UM = u.um
                                    }).ToList();
                    }
                    if (pc.Video != null)
                    {
                        this.Video = (from u in pc.Video
                                      select new VideoVista
                                      {
                                          Id = u.id_video,
                                          Interfaz = u.Interfaz_placa.descripcion,
                                          Marca = u.Marca.descripcion,
                                          Modelo = u.modelo
                                      }).ToList();
                    }
                    if (pc.Placa_red != null)
                    {
                        this.PlacaRed = (from u in pc.Placa_red
                                         select new NicVista
                                         {
                                             Id = u.id_placa,
                                             Marca = u.Marca.descripcion,
                                             Modelo = u.modelo,
                                             Tecnologia = u.tecnologia,
                                             Velocidad = u.velocidad
                                         }).ToList();
                    }
                    if (pc.Optico != null)
                    {
                        this.DiscoOptico = (from u in pc.Optico
                                            select new DiscoOpticoVista
                                            {
                                                Id = u.id_optico,
                                                Interf = u.Interfaz_disco.descripcion,
                                                Marca = new MarcaVista { Nombre = u.Marca.descripcion },
                                                Tipo = new TipoDiscoOpticoVista { Nombre = u.Tipo_disco.descripcion }
                                            }).ToList();
                    }
                    this.LectorTarjeta = pc.l_tarjeta == 1;                    
                    this.PuertoMouse = pc.f_mouse;
                    this.PuertoTeclado = pc.f_teclado;
                    this.SoftAdicional = (from u in pc.Software
                                          select new SoftVista
                                          {
                                              Nombre = u.descripcion
                                          }).ToList();
                    this.Modelo = pc.modelo;
                    Marca = new MarcaVista
                    {
                        idMarca = pc.Marca.id_marca,
                        Nombre = pc.Marca.descripcion
                    };
                }//using
            }
            catch (Exception e)
            {

            }
        }

        internal override Responses SaveLineaBase()
        {
            int counter = 0;
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            sb.Append("ClaveAdmin:").Append(this.ClaveAdmin).AppendLine();
            sb.Append("ClaveBios:").Append(this.ClaveBios).AppendLine();
            sb.Append("Os:").Append(this.Os.Nombre).AppendLine();
            sb.Append("MB:").Append(this.MB.Modelo).AppendLine();
            sb.Append("Cpu:").Append(this.Cpu.Modelo).AppendLine();
            foreach (HdVista h in Hd)
            {
                counter++;
                sb.Append("Hd "+counter+":").Append(h.Capacidad).AppendLine();
            }
            counter = 0;
            foreach (RamVista r in Ram)
            {
                counter++;
                sb.Append("Ram " + counter + ":").Append(r.Capacidad).AppendLine();
            }
            counter = 0;
            foreach(VideoVista v in Video)
            {
                counter++;
                sb.Append("Video " + counter + ":").Append(v.Modelo).AppendLine();
            }
            counter = 0;
            foreach (NicVista v in PlacaRed)
            {
                counter++;
                sb.Append("PlacaRed " + counter + ":").Append(v.Modelo).AppendLine();
            }
            counter = 0;
            foreach (DiscoOpticoVista v in DiscoOptico)
            {
                counter++;
                sb.Append("DiscoOptico " + counter + ":").Append(v.Tipo.Nombre).AppendLine();
            }
            sb.Append("LectorTarjeta:").Append(this.LectorTarjeta?"Si":"No").AppendLine();
            sb.Append("PuertoTeclado:").Append(this.PuertoTeclado).AppendLine();
            sb.Append("PuertoMouse:").Append(this.PuertoMouse).AppendLine();
            counter = 0;
            foreach (SoftVista v in SoftAdicional)
            {
                counter++;
                sb.Append("Software " + counter + ":").Append(v.Nombre).AppendLine();
            }
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}