﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.Statics;
using soporte.Models.ClasesVistas;
using System.Text.RegularExpressions;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class HdVista
    {
        private string capacidad;
        public int Id { get; set; }
        public int Id_Interfaz { get; set; }
        public int Id_Marca { get; set; }
        public string Interfaz { get; set; }        
        public string Marca { get; set; }        
        public string Capacidad { 
            get 
        {            
            Regex pattern = new Regex(@"(?<cantidad>\d+)");
            string cantidad = string.Empty;
            try
            {
                Match match = pattern.Match(capacidad);             
                cantidad = match.Groups["cantidad"].Value;
            }
            catch (Exception) { }
            return cantidad;
        } 
            set {
                this.capacidad = value;
            } 
        }
        public string UM { 
            get 
            {
                Regex pattern = new Regex(@"(?<unidadM>[a-zA-Z]+$)");
                Match match = pattern.Match(capacidad);
                string um = match.Groups["unidadM"].Value;
                return um;
            } 
            set {
                
            }             
        }       
    }
}