﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class NicVista
    {
        public int Id { get; set; }
        public string Tecnologia { get; set; }
        public string Modelo { get; set; }
        public string Velocidad { get; set; }
        public string um { get; set; }
        public string Marca { get; set; }
        public int idMarca { get; set; }
    }
}