﻿using Sofit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class ModeloImpresora
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public TecnologiaPr Tecnologia { get; set; }
        public List<ConectividadPr> Conectividad { get; set; }
        public List<InsumoPr> Insumos { get; set; }
        public int idMarca { get; set; }
        public RendimientoImpresora Rendimiento { get; set; }
        public TipoPr Tipo { get; set; }
    }
}