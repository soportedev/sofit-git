﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class ModeloMonitor
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public TecnologiaMn Tecnologia { get; set; }        
        public RelAspecto RelAspecto { get; set; }
        public string Tamanio { get; set; }
        public int idMarca { get; set; }
    }
}