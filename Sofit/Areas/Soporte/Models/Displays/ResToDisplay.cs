﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class ResToDisplay:IComparable
    
        {
            public DateTime backFecha {get;set;}
            public string FechaString { get {
                return backFecha.ToShortDateString();
            }
                set { }
            }
            public string Equipo { get; set; } 
            public DateTime Fecha
            {
                get 
                {
                    return backFecha;
                } 
                set 
                {
                    backFecha = value;
                } 
            }
            public string TipoRes { get; set; }
            public string Observ { get; set; }
            public string Tecnico { get; set; }
            public string Archivo { get; set; }
            public int CompareTo(object obj)
                {
                    ResToDisplay other = obj as ResToDisplay;
                    return DateTime.Compare(this.backFecha, other.backFecha);
                }
            }
}