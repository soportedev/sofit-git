﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class CpuVista
    {
        public int Id { get; set; }
        public string Modelo { get; set; }
        public MarcaVista Marca { get; set; }
        public string Frecuencia { get; set; }
        public string Cache { get; set; }
        public SocketVista Socket { get; set; }
        public int idMarca { get; set; }
        public int idSocket { get; set; }

    }
}