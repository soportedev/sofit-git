﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class VideoVista
    {
        public int Id { get; set; }
        public string Marca { get; set; }
        public string Interfaz { get; set; }
        public string Modelo { get; set; }
        public int idInterfaz { get; set; }
        public int idMarca { get; set; }

    }
}