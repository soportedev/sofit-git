﻿using soporte.Models.Displays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class InfoNotificacion
    {
        public int idEquipo { get; set; }
        public string oblea { get; set; }
        public string Marca { get; set; }
        public int idMarca { get; set; }
        public int idTipo { get; set; }
        public int idDependencia { get; set; }
        public string Firma { get; set; }
        private string nroSerie;
        private string modelo;
        public string NroSerie
        {
            get
            {
                if (nroSerie == "") return "S/N";
                else return nroSerie;
            }
            set
            {
                this.nroSerie = value;
            }
        }
        public string Modelo
        {
            get
            {
                if (modelo == "") return "N/A";
                else return modelo;
            }
            set
            {
                this.modelo = value;
            }
        }
        public List<ResToDisplay> resoluciones { get; set; }
        public List<ReferentesVista> referentes { get; set; }
    }
}