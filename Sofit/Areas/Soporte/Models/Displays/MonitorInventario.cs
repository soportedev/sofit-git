﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class MonitorInventario:IEquipoInventario
    {
        public TecnologiaMn Tecnologia { get; set; }
        public RelAspecto RelAspecto { get; set; }
        public string Tamanio { get; set; }
        public string Modelo { get; set; }
        public int idModelo { get; set; }
        public MarcaVista Marca { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }

        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var mn = (from u in dbc.Monitores
                          where u.id_uc == id
                          select u).First();
                if (mn.Modelo_Monitor != null)
                {
                    this.Tecnologia = new TecnologiaMn { Nombre = mn.Modelo_Monitor.Tecnologia_monitores.descripcion, Id = mn.Modelo_Monitor.Tecnologia_monitores.id_tecnologia };
                    this.RelAspecto = new RelAspecto { Nombre = mn.Modelo_Monitor.Relacion_aspecto.descripcion, Id = mn.Modelo_Monitor.Relacion_aspecto.id_relAspecto };
                    this.Tamanio = mn.Modelo_Monitor.tamaño;
                    this.Modelo = mn.Modelo_Monitor.descripcion;
                    this.idModelo = mn.Modelo_Monitor.id_modelo;
                    Marca = new MarcaVista
                    {
                        idMarca = mn.Modelo_Monitor.Marca.id_marca,
                        Nombre = mn.Modelo_Monitor.Marca.descripcion
                    };
                }                
                Dependencia = new DependenciaVista
                {
                    idDependencia = mn.Dependencia.id_dependencia,
                    Nombre = mn.Dependencia.descripcion
                };
                Estado = new EstadoEquipoVista
                {
                    idEstado = mn.id_estado,
                    Nombre = mn.Estado_UC.descripcion
                };
            }//using
        }

        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {            
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            sb.Append("Tecnología:").Append(this.Tecnologia.Nombre).AppendLine();
            sb.Append("Rel.Aspecto:").Append(this.RelAspecto.Nombre).AppendLine();
            sb.Append("Tamaño:").Append(this.Tamanio).AppendLine();        
            
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}