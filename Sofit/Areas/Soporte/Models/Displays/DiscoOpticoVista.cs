﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class DiscoOpticoVista
    {
        public int Id { get; set; }
        public string Interf { get; set; }
        public int idInterfaz { get; set; }
        public int idTipo { get; set; }
        public int idMarca { get; set; }
        public TipoDiscoOpticoVista Tipo { get; set; }
        public MarcaVista Marca { get; set; }
        public IfDiscosVista Interfaz { get; set; }
    }
}