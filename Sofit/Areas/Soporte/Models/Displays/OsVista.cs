﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class OsVista
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public FamiliaOsVista FamiliaOs { get; set; }
    }
}