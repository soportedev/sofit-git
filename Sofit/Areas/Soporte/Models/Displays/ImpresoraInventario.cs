﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Soporte.Models.Displays
{
    public class ImpresoraInventario:IEquipoInventario
    {
        public string Tecnologia { get; set; }
        public RendimientoImpresora Rendimiento { get; set; }        
        public string Tipo { get; set; }
        public MarcaVista Marca { get; set; }
        public string Modelo { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public List<InsumoPr> Insumos { get; set; }
        public List<ConectividadPr> Conectividad { get; set; }        
        public EstadoEquipoVista Estado { get; set; }

        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var pr = (from u in dbc.Impresoras
                          where u.id_uc== id
                          select u).First();
                this.Dependencia = new DependenciaVista
                {
                    idDependencia = pr.Dependencia.id_dependencia,
                    Nombre = pr.Dependencia.descripcion,
                    Jurisdiccion = new JurisdiccionVista { idJurisdiccion = pr.Dependencia.Jurisdiccion.id_jurisdiccion, Nombre = pr.Dependencia.Jurisdiccion.descripcion }
                };
                this.Estado = new EstadoEquipoVista { Nombre = pr.Estado_UC.descripcion,idEstado=pr.Estado_UC.id_estado };
                if (pr.Modelo_Impresora != null)
                {
                    this.Tecnologia = pr.Modelo_Impresora.Tecnologia_Impresoras.descripcion;
                    this.Tipo = pr.Modelo_Impresora.Tipo_impresora.descripcion;
                    this.Rendimiento = new RendimientoImpresora
                    {
                        Id = pr.Modelo_Impresora.Rendimiento_impresora.id_rendimiento,
                        Nombre = pr.Modelo_Impresora.Rendimiento_impresora.descripcion
                    };
                    this.Modelo = pr.Modelo_Impresora.descripcion;
                    this.Conectividad = (from u in pr.Modelo_Impresora.Conectividad_impresora
                                         select new ConectividadPr
                                         {
                                             id = u.id_conectividad,
                                             Nombre = u.descripcion
                                         }).ToList();
                    this.Insumos = (from u in pr.Modelo_Impresora.Insumos
                                    select new InsumoPr
                                    {
                                        Nombre = u.descripcion,
                                        Id = u.id_insumo,
                                        idTipo = u.Tipo_insumo.id_tipo_insumo,
                                        Tipo = u.Tipo_insumo.descripcion
                                    }).ToList();
                    Marca = new MarcaVista
                    {
                        idMarca = pr.Modelo_Impresora.Marca.id_marca,
                        Nombre = pr.Modelo_Impresora.Marca.descripcion
                    };                    
                }//using
            }
        }

        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {
            int counter = 0;
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            sb.Append("Tecnología:").Append(this.Tecnologia).AppendLine();
            sb.Append("Rendimiento:").Append(this.Rendimiento.Nombre).AppendLine();
            sb.Append("Tipo:").Append(this.Tipo).AppendLine();
            sb.Append("Estado:").Append(this.Estado.Nombre).AppendLine();
            foreach (InsumoPr h in Insumos)
            {
                counter++;
                sb.Append("Insumo " + counter + ":").Append(h.Nombre).AppendLine();
            }
            counter = 0;
            foreach (ConectividadPr r in Conectividad)
            {
                counter++;
                sb.Append("Conectividad " + counter + ":").Append(r.Nombre).AppendLine();
            }            
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}