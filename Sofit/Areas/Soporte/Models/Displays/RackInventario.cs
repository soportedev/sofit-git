﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Sofit.Areas.Soporte.Models.Displays
{
    public class RackInventario : IEquipoInventario
    {
        public string Modelo { get; set; }
        public MarcaVista Marca { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }
        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var vr = (from u in dbc.Varios
                          where u.id_uc == id
                          select u).First();
                if (vr.Marca != null)
                {
                    Marca = new MarcaVista
                    {
                        idMarca = vr.Marca.id_marca,
                        Nombre = vr.Marca.descripcion
                    };
                }
                if (vr.Dependencia != null)
                {
                    Dependencia = new DependenciaVista
                    {
                        idDependencia = vr.id_dependencia.Value,
                        Nombre = vr.Dependencia.descripcion
                    };
                }

                Estado = new EstadoEquipoVista
                {
                    idEstado = vr.id_estado.Value,
                    Nombre = vr.Estado_UC.descripcion
                };
                Modelo = vr.modelo;

            }//using
        }
        internal override Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            if (Relaciones.Count > 0)
            {
                sb.Append("Relaciones: ");
            }
            foreach (var v in Relaciones)
            {
                sb.Append("-" + v.Oblea).AppendLine();
            }
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}