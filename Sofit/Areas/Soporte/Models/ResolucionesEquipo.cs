﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace soporte.Areas.Soporte.Models
{
    public class ResolucionesEquipo:IComparable
    {
        [ScriptIgnore]
        public DateTime Fecha { get; set; }
        public string FechaStr
        {
            get
            {
                return Fecha.ToString();
            }
            set
            {

            }
        }
        public string Tecnico { get; set; }
        public string Turno { get; set; }
        public string Observaciones { get; set; }
        public string TipoResolucion { get; set; }
        public int idTipoResolucion { get; set; }
        public int idResolucion { get; set; }
        public int IdUsuario { get; set; }

        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            ResolucionesEquipo other = (ResolucionesEquipo)obj;
            if (Fecha > other.Fecha)
                return 1;
            else if (Fecha < other.Fecha)
                return -1;
            else return 0;
        }
        #endregion
    }
}