﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class PerfilHistoricos : Perfil
    {
        public override Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.ReadOnly;
            switch (page)
            {
                case "Soporte/Historicos":
                    acceso = Constantes.AccesosPagina.Full;
                    break;                
                default:
                    acceso = Constantes.AccesosPagina.NoAccess;
                    break;
            }
            return acceso;
        }

        public override string getHomePage()
        {
            return "Soporte/Historicos";
        }
    }
}