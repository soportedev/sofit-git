﻿using soporte.Areas.Soporte.Models.Interface;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class AreaIncAtUs:IAreaIncidencia
    {        
        public AreaIncAtUs(int idArea, string nombreArea)
        {
            base.IdArea = idArea;
            base.NombreArea = nombreArea;
        }        
        public override Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int idTipoResolucion, int? idEquipo, DateTime actualizarFecha, string imagen)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {                    
                    Resoluciones res = new Resoluciones
                    {
                        id_uc = idEquipo == 0 ? null : idEquipo,
                        fecha = DateTime.Now,
                        id_incidencia = this.Incidencia.Id,
                        id_tecnico = tecnico,
                        id_tipo_resolucion = idTipoResolucion,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_area = base.IdArea,
                        path_imagen = (imagen != string.Empty) ? imagen : null
                    };
                    dbc.Resoluciones.Add(res);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }      

        public override Responses derivar(int idArea, int tecnico, int usuario)
        {
            Responses result = new Responses();
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idArea);
            Incidencia.AreasActuantes.Sort();
            MovAreaIncidencia ultimaArea = Incidencia.AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "La Incidencia no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    result = Incidencia.cambioArea(areaDestino, tecnico, usuario);
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar la resolución del Área: " + ultimaArea.Area.NombreArea;
                }
            }
            return result;     
        }

        public override Responses solucionar(int idUsuario)
        {
            throw new NotImplementedException();
        }
        public override int getIdTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                var tec = (from u in dbc.tecnico
                           where u.descripcion == "At.Usuarios"
                           select u.id_tecnico).First();
                idTecnico = tec;
            }
            return idTecnico;
        }
    }
}