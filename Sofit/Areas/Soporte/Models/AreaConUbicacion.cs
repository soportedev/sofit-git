﻿using soporte.Areas.Soporte.Models.Interface;
using soporte.Controllers;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Soporte.Models
{
    public class AreaConUbicacion:AreaEquipo
    {
        AreaEquipo area;
        Ubicacion ubicacion;
        private AreaConUbicacion(AreaEquipo a){
            
            this.area=a;           
            ubicacion = new Ubicacion();
        }        
        public Responses cambioUbicacion(Ubicacion u)
        {
            Responses result = new Responses();
            //try
            //{
            //    result = equipo.getEstadoActual().setUbicacion();

            //    if (result.Info == "ok")
            //    {
            //        UbicacionManager um = new UbicacionManager();
            //        um.ubicarEquipo(equipo.idEquipo, u);
            //        EstadoEquipo eA = equipo.getEstadoActual();
            //        if (eA is EstadoNotificar)
            //        {
            //            equipo.cambioEstado(EstadoEquipoFactory.getInstancia().getEstadoPorNombre("Suspendido"));
            //        }
            //        result.Info = "ok";
            //    }
            //}
            //catch (NotImplementedException)
            //{
            //    result.Detail = "Error con el estado del equipo";
            //}
            return result;
        }
        public Ubicacion getUbicacion()
        {
            return this.ubicacion;
        }
        public override Responses derivar(AreaEquipo area, int idUsuario, int idTecnico)
        {
            return this.area.derivar(area, idUsuario, idTecnico);
        }
        public override Responses terminarEquipo()
        {
            return this.area.terminarEquipo();
        }
        public override string getNombreArea()
        {
            return area.getNombreArea();
        }
        public override int getIdArea()
        {
            return area.getIdArea();
        }       
        public override int getTecnicoGenerico()
        {
            return area.getTecnicoGenerico();
        }
        internal override Responses cargarSolucion(int idUsuario, int tResolucionClonado, int tecnico, int turno, int? imagen,string archivo, string observacion)
        {
            return this.area.cargarSolucion(idUsuario, tResolucionClonado, tecnico, turno, imagen, archivo, observacion);
        }

        public override Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject)
        {
            throw new NotImplementedException();
        }
    }
}