﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using soporte.Areas.Soporte.Models.Interface;
using soporte.Areas.Soporte.Models.Statics;
using soporte.Models.Statics;
using Sofit.DataAccess;
using soporte.Areas.Soporte.Models.Factory;

namespace soporte.Areas.Soporte.Models
{
    public class AreaLogistica:AreaEquipo
    {
        
        public AreaLogistica(int idArea,string nombre)
        {
            base.idArea = idArea;
            base.nombreArea = nombre;
        }
        public override Responses derivar(AreaEquipo area, int idUsuario, int idTecnico)
        {
            Responses result = new Responses();
            EstadoEquipo estadoActual = equipo.getEstadoActual();
            if (estadoActual is EstadoParaGarantia)
            {
                equipo.LoadHistorialEstados();
                equipo.EstadosActuantes.Sort();
                MovEstados ultimo = equipo.EstadosActuantes.Last();
                if (ultimo.tieneResolucion())
                {
                    result.Info = "ok";
                }
                else
                {
                    result.Detail = "Falta cargar la resolución del Aviso a Garantía";
                }
            }
            else
            {
                result.Info = "ok";
            }
            if (result.Info == "ok")
            {
                if (area is AreaClonado | area is AreaLaboratorio)
                {
                    equipo.cambioEstado(EstadoEquipoFactory.getEstadoPorNombre("En Reparación", "Soporte Técnico"), area.getTecnicoGenerico());
                    result = equipo.cambioArea(area, idTecnico);
                }
                if (area is AreaLogistica)
                {
                    result = equipo.cambioEstado(EstadoEquipoFactory.getEstadoPorNombre("Para Entregar", "Soporte Técnico"), area.getTecnicoGenerico());

                }
            }
            return result;            
        }

        public override Responses terminarEquipo()
        {
            throw new NotImplementedException();
        }

        public override string getNombreArea()
        {
            return base.nombreArea;
        }

        public override int getIdArea()
        {
            return base.idArea;
        }

        public override int getTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                idTecnico = (from u in dbc.tecnico
                             where u.descripcion == "Logística"
                             select u.id_tecnico).First();
            }
            return idTecnico;
        }

        internal override Responses cargarSolucion(int idUsuario, int tResolucionClonado, int tecnico, int turno, int? imagen, string archivo, string observacion)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    Resoluciones res = new Resoluciones
                    {
                        id_area = this.idArea,
                        fecha = DateTime.Now,
                        id_uc = equipo.idEquipo,
                        id_tecnico = tecnico,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_tipo_resolucion = tResolucionClonado,
                        id_incidencia = this.equipo.incidencia.Id,
                        id_imagen = imagen,
                        path_imagen = archivo == string.Empty ? null : archivo
                    };
                    dbc.Resoluciones.Add(res);
                    //si corresponde cambio de area
                    string tres = (from u in dbc.tipo_resolucion
                                   where u.id_tipo_resolucion == tResolucionClonado
                                   select u.descripcion).First();
                    AreaEquipo areaNueva = null;
                    EstadoEquipo estadoNuevo = null;
                    switch (tres)
                    {
                        case "Aviso a Garantía":
                            estadoNuevo = EstadoEquipoFactory.getEstadoPorNombre("Para Garantia", "Soporte Técnico");                            
                            equipo.cambioEstado(estadoNuevo, tecnico);
                            break;
                        case "En Garantía": areaNueva = AreaEquipoFactory.getAreaXNombre("Garantia");
                            estadoNuevo = EstadoEquipoFactory.getEstadoPorNombre("En Garantia", "Soporte Técnico");
                            equipo.cambioArea(areaNueva, areaNueva.getTecnicoGenerico());
                            equipo.cambioEstado(estadoNuevo, tecnico);
                            break;                        
                    }
                    //
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e) { result.Detail = e.Message; }
            return result;
        }

        public override Responses notificarEquipo(string[] referentes, string firma, string cuerpo, string subject)
        {
            throw new NotImplementedException();
        }
    }
}
