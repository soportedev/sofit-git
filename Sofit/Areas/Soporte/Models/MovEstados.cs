﻿using soporte.Areas.Soporte.Models.Displays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace soporte.Areas.Soporte.Models
{
    public class MovEstados:IComparable
    {
        public List<ResToDisplay> Resoluciones;
        public EstadoEquipo Estado { get; set; }
        [ScriptIgnore]
        public DateTime FechaDesde { get; set; }
        [ScriptIgnore]
        public DateTime? FechaHasta
        {
            get;
            set;
        }
        public string FechaDesdeShStr
        {
            get
            {
                return FechaDesde.ToString();
            }
            set { }
        }
        public string FechaHastaShStr
        {
            get
            {
                if (FechaHasta == null) return "";
                else return FechaHasta.Value.ToString();
            }
            set { }
        }
        public string usuario { get; set; }
        public string tecnico { get; set; }
        public bool tieneResolucion()
        {
            if (Resoluciones.Count > 0) return true;
            else return false;
        }
        #region Miembros de IComparable

        public int CompareTo(object obj)
        {
            MovEstados other = (MovEstados)obj;
            if (this.FechaDesde > other.FechaDesde)
                return 1;
            else if (this.FechaDesde < other.FechaDesde)
                return -1;
            else return 0;
        }
        #endregion
    }
}
