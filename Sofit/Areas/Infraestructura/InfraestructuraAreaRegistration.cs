﻿using System.Web.Mvc;

namespace soporte.Areas.Infraestructura
{
    public class InfraestructuraAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Infraestructura";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "lineabaseinfraestructura",
                url: "Infraestructura/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambioinfra",
                url: "Infraestructura/Cambios/{startIndex}",
                constraints: new { startIndex = @"\d+" },
                defaults: new { controller = "Cambios", action = "Index" ,startIndex=0}, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8infra",
                url: "Infraestructura/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Infraestructura_default",
                "Infraestructura/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
