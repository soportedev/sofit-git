﻿using Sofit.DataAccess;
using soporte.Areas.Infraestructura.Models;
using soporte.Areas.Infraestructura.Models.ClasesVista;
using soporte.Areas.Soporte.Models.Displays;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace soporte.Areas.Infraestructura.Controllers
{
    [SessionActionFilter]
    //[AllowCrossSiteJson]    
    public class InventarioController : Controller
    {
        //
        // GET: /Infraestructura/Inventario/

        public ActionResult Index()
        {            
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            if (acceso == Constantes.AccesosPagina.ReadOnly)
            {
                ViewBag.acceso = "readonly";
            }
            if (acceso == Constantes.AccesosPagina.Full)
            {
                ViewBag.acceso = "full";
            }
            return View();
        }
        public JsonResult NuevoStorage(StorageInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf
                                  where (u.nombre == e.Oblea |
                                      (e.NroSerie.Length > 0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Storage"))
                                  select u).SingleOrDefault();

                    if (encontrado == null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        UnidadConf uc = new UnidadConf
                        {
                            nombre = e.Oblea,
                            id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion,
                            fecha = DateTime.Now,
                            id_tipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Storage").Single().id_tipo,
                            observaciones = e.Observaciones,
                            nro_serie = e.NroSerie,
                            id_usuario = actual.IdUsuario
                        };
                        dbc.UnidadConf.Add(uc);
                        Sofit.DataAccess.Storage newstorage = new Sofit.DataAccess.Storage();
                        newstorage.id_uc = uc.id_uc;
                        newstorage.id_dependencia = e.Dependencia.idDependencia;
                        newstorage.id_marca = e.Marca.idMarca;
                        newstorage.modelo = e.Modelo;
                        newstorage.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Infraestructura").Single().id_estado;
                        dbc.Storage.Add(newstorage);
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Storage actualizado!!";                        
                    }
                    else
                    {
                        result.Detail = "Un equipo con esa Oblea o Número de serie ya existe";
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }
            return Json(result);
        }
        public JsonResult UpdateStorage(StorageInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Sofit.DataAccess.Storage encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.Storage where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                    if (encontrado != null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        encontrado.UnidadConf.id_jurisdiccion=e.Dependencia.Jurisdiccion.idJurisdiccion;
                        encontrado.UnidadConf.id_tipo=e.TipoEquipo.id;
                        encontrado.UnidadConf.observaciones=e.Observaciones;
                        encontrado.UnidadConf.nro_serie=e.NroSerie;                        
                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        encontrado.id_marca = e.Marca.idMarca;
                        encontrado.modelo = e.Modelo;
                        encontrado.id_estado = e.Estado.idEstado;                        
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Storage actualizado!!";
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                    }
                    else
                    {
                        result.Detail = "No fue encontrado el Tipo!! Si intenta cambiar el tipo de equipo, elimínelo" +
                            " y vuelva a crearlo o contacte al administrador!!";
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }
            return Json(result);
        }
        public JsonResult TiposEquipo()
        {
            List<TiposEquipoVista> tipos = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipos = (from u in dbc.Tipo_equipo
                         where u.Direcciones.nombre == "Infraestructura"                         
                         orderby u.descripcion
                         select new TiposEquipoVista
                         {
                             id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(tipos);
        }       
        public JsonResult UmMemoria()
        {
            List<UnidadMedidaVista> um = new List<UnidadMedidaVista>();
            using(var dbc=new IncidenciasEntities())
            {
                um = (from u in dbc.UnidadMedida
                      where u.Tipo_equipo.descripcion == "RAM"
                      orderby u.descripcion
                      select new UnidadMedidaVista
                      {
                          Id = u.id_um,
                          Nombre = u.descripcion
                      }).ToList();
            }
            return Json(um);
        }
        public JsonResult UmDisco()
        {
            List<UnidadMedidaVista> um = new List<UnidadMedidaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                um = (from u in dbc.UnidadMedida
                      where u.Tipo_equipo.descripcion == "Disco Rígido"
                      orderby u.descripcion
                      select new UnidadMedidaVista
                      {
                          Id = u.id_um,
                          Nombre = u.descripcion
                      }).ToList();
            }
            return Json(um);
        }
        public JsonResult BaseLine(int id)
        {
            List<LineaBaseVista> lineaB = new List<LineaBaseVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var basel = from u in dbc.BaseLines
                            where u.master_id == id
                            orderby u.fecha descending
                            select u;
                foreach (var b in basel)
                {
                    lineaB.Add(new LineaBaseVista
                    {
                        CreadoPor = b.Usuarios.nombre_completo,
                        Data = b.data,
                        Fecha = b.fecha
                    });
                }
            }
            return Json(lineaB);
        }
        public JsonResult SaveBaseLine(int id)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Cluster": equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Físico": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Storage": equipo = new StorageInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Workstation Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Librería": equipo = new LibreriaInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                if (equipo != null)
                {
                    result = equipo.SaveLineaBase();
                }
                return Json(result);
            }
        }
        public JsonResult FamiliaSO()
        {
            List<FamiliaOsVista> fam = new List<FamiliaOsVista>();
            using (var dbc = new IncidenciasEntities())
            {
                fam = (from u in dbc.FamiliaSo                 
                      
                      select new FamiliaOsVista
                      {
                          Id = u.id_fso,
                          Nombre = u.nombre
                      }).ToList();
            }
            return Json(fam);
        }
        public JsonResult VersionSO(int idF, int idTipo)
        {
            List<OsVista> fam = new List<OsVista>();
            using (var dbc = new IncidenciasEntities())
            {
                fam = (from u in dbc.Os
                       where u.id_tipo_equipo == idTipo & u.FamiliaSo.id_fso == idF
                       select new OsVista
                       {
                           Id = u.id_os,
                           Nombre = u.descripcion
                       }).OrderBy(c=>c.Nombre).ToList();
            }
            return Json(fam);
        }
        public JsonResult NuevoCluster(ClusterInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf where (u.nombre == e.Oblea |
                                  (e.NroSerie.Length>0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Cluster")) select u).SingleOrDefault();
                    if (encontrado == null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        UnidadConf uc = new UnidadConf
                        {
                            nombre = e.Oblea,
                            id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion,
                            fecha = DateTime.Now,
                            id_tipo = e.TipoEquipo.id,
                            observaciones = e.Observaciones,
                            nro_serie = e.NroSerie,
                            id_usuario = actual.IdUsuario
                        };
                        dbc.UnidadConf.Add(uc);
                        Cluster cluster = new Cluster();
                        cluster.id_dependencia = e.Dependencia.idDependencia;
                        cluster.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Infraestructura").Single().id_estado;
                        cluster.id_uc = uc.id_uc;
                        foreach (HostCluster h in e.Hosts)
                        {
                            var hos = dbc.Servidor.Where(c => c.id_uc == h.Id).Single();
                            cluster.Servidor.Add(hos);
                        }
                        dbc.Cluster.Add(cluster);
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Cluster agregado!!!";
                    }
                    else
                    {
                        result.Detail = "Un equipo con esa Oblea o Nro. de Serie ya existe";
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }
            return Json(result);
        }
        public JsonResult UpdateCluster(ClusterInventario e)
        {
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            Cluster encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.Cluster where u.UnidadConf.nombre == e.Oblea select u).SingleOrDefault();

                    if (encontrado != null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        encontrado.UnidadConf.nombre = e.Oblea;
                        encontrado.UnidadConf.id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion;
                        encontrado.UnidadConf.fecha = DateTime.Now;
                        encontrado.UnidadConf.id_tipo = e.TipoEquipo.id;
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        encontrado.UnidadConf.id_usuario = actual.IdUsuario;
                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        encontrado.id_estado = e.Estado.idEstado;
                        encontrado.Servidor.Clear();
                        if (e.Hosts!=null)
                        {
                            foreach (HostCluster h in e.Hosts)
                            {
                                var hos = dbc.Servidor.Where(c => c.id_uc == h.Id).Single();
                                encontrado.Servidor.Add(hos);
                            }
                        }
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Servidor actualizado!!!";
                    }
                    else
                    {
                        result.Detail = "No fue encontrado el Tipo!! Si intenta cambiar el tipo de equipo, elimínelo" +
                            " y vuelva a crearlo o contacte al administrador!!";
                    }
                }
            }catch(Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoSo(int idF, int idTipo, string nombre)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var found = dbc.Os.Where(c => c.descripcion == nombre & c.id_tipo_equipo==idTipo).SingleOrDefault();
                if (found == null)
                {
                    Os nuevo = new Os
                    {
                        descripcion = nombre,
                        id_tipo_equipo = idTipo,
                        id_familia = idF
                    };
                    dbc.Os.Add(nuevo);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else result.Info = "El Sistema Operativo ya está agregado!!";
            }
            return Json(result);
        }
        public JsonResult UpdateServer(ServerInventario e)
        {
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            Responses result = new Responses();
            Servidor encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.Servidor where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                    if (encontrado != null)
                    {
                        encontrado.nombreComun = e.NombreComun;
                        encontrado.id_dominio=e.Dominio!=null?e.Dominio.Id:(int?)null;
                        encontrado.id_marca = e.Marca.idMarca;
                        encontrado.modelo = e.Modelo;
                        var tipoe = dbc.Tipo_equipo.Where(c => c.id_tipo == e.TipoEquipo.id).SingleOrDefault();
                        if (tipoe != null & (tipoe.descripcion == "Servidor Físico" | tipoe.descripcion == "Servidor Virtual"))
                        {
                            encontrado.UnidadConf.id_tipo = e.TipoEquipo.id;
                        }
                        encontrado.memoria = e.Memoria.Capacidad;
                        if (e.Memoria.Um.Id == 0)
                        {
                            encontrado.id_um_mem = null;                            
                        }
                        else encontrado.id_um_mem = e.Memoria.Um.Id;
                        //disco viejo
                        if (e.Disco.Um.Id == 0)
                        {
                            encontrado.id_um_disc = null;
                            encontrado.disco = null;
                            int cantidadDiscos = 0;
                            Int32.TryParse(e.Disco.Cantidad, out cantidadDiscos);
                            encontrado.cantidaddiscos = cantidadDiscos;
                        }
                        else
                        {
                            encontrado.id_um_disc = e.Disco.Um.Id;
                            encontrado.disco = e.Disco.Capacidad;
                            int cantidadDiscos = 0;
                            Int32.TryParse(e.Disco.Cantidad, out cantidadDiscos);
                            encontrado.cantidaddiscos = cantidadDiscos;
                        }
                        //disco nuevo
                        if (e.Storage != null)
                        {
                            encontrado.ServerStorage.Clear();
                            encontrado.ServerStorage = e.Storage.Select(c => 
                            new ServerStorage {
                                capacidad = c.Capacidad,
                                datastore = c.DataStore,
                                letra = c.LetraUnidad,
                                UnidadMedida = dbc.UnidadMedida.Where(d=>d.id_um==c.Um.Id).Single()
                            }).ToList();
                        }
                        //fin disco nuevo
                        encontrado.cpu = e.Cpu;                        
                        encontrado.cantidadcpu = e.CantidadCpu;
                        encontrado.criticidad = e.Criticidad;
                        //encontrado.datastore = e.DataStore;
                        //ip nuevo
                        if (e.Ips != null)
                        {
                            encontrado.UnidadConf.Ip.Clear();
                            encontrado.UnidadConf.Ip = e.Ips.Select(c => new Ip { ip_v4 = c.IpV4 }).ToList();
                        }
                        //ip viejo
                        Ip ip = encontrado.Ip1;
                        if (ip != null)
                        {
                            //encontrado.UnidadConf.Ip.Add(new Ip { ip_v4 = ip.ip_v4 });
                            encontrado.Ip1 = null;
                        }                       
                        
                        encontrado.puesta_produccion = e.PuestaProduccion;
                        encontrado.rack = e.Rack;
                        encontrado.unidadesRack = e.UnidadesRack;
                        encontrado.licenciaSO = e.LicenciaOs;
                        if (e.Os.Id == 0)
                        {
                            encontrado.os = null;
                        }
                        else encontrado.os = e.Os.Id;
                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        encontrado.UnidadConf.id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion;
                        encontrado.id_estado = e.Estado.idEstado;                        
                        
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Servidor actualizado!!";
                    }
                    else
                    {
                        result.Detail = "No fue encontrado el Tipo!! Si intenta cambiar el tipo de equipo, elimínelo"+
                            " y vuelva a crearlo o contacte al administrador!!";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;

            }
            return Json(result);
        }
        //public JsonResult NuevoServerBatch(ServerInventario e)
        //{
        //    Responses result = new Responses();
        //    UnidadConf encontrado = null;
        //    Regex pattern = new Regex(@"^(d\d{3})([a-zA-Z0-9]{2,6})(\d{2})$");
        //    string cantidad = string.Empty;
        //    try
        //    {
        //        using (var dbc = new IncidenciasEntities())
        //        {
        //            Match match = pattern.Match(e.Oblea.ToLower());
        //            cantidad = match.Groups["comienzo"].Value;
        //            if (match.Success)
        //            {
        //                ServersLog log = new ServersLog
        //                {
        //                    nombre = e.Oblea,
        //                    descripcion = "Agregado"

        //                };
        //                dbc.ServersLog.Add(log);

        //                encontrado = (from u in dbc.UnidadConf where u.nombre == e.Oblea select u).SingleOrDefault();

        //                if (encontrado == null)
        //                {
        //                    int idusuario = 1;
        //                    int idJurisdiccion = 0;
        //                    int idDependencia = 0;
        //                    int idMarca = 0;
        //                    int idDominio = 0;
        //                    if (e.Dependencia != null)
        //                    {
        //                        var dep = dbc.Dependencia.Where(c => c.descripcion == e.Dependencia.Nombre).SingleOrDefault();
        //                        if (dep == null) { idDependencia = 319; idJurisdiccion = 22; }
        //                        else
        //                        {
        //                            idDependencia = dep.id_dependencia;
        //                            idJurisdiccion = dep.Jurisdiccion.id_jurisdiccion;
        //                        }
        //                    }
        //                    idMarca = dbc.Marca.Where(c => c.descripcion == "N/A" & c.Tipo_equipo.descripcion == "Servidor Virtual").Single().id_marca;
        //                    if (e.Dominio != null)
        //                    {
        //                        var dominio = dbc.Dominios.Where(c => c.nombre == e.Dominio.Nombre).SingleOrDefault();
        //                        if (dominio == null)
        //                        {
        //                            idDominio = 1;
        //                        }
        //                        else
        //                        {
        //                            idDominio = dominio.id_dominio;
        //                        }
        //                    }
        //                    else idDominio = 1;
        //                    UnidadConf uc = new UnidadConf
        //                    {
        //                        nombre = e.Oblea,
        //                        id_jurisdiccion = idJurisdiccion,
        //                        fecha = DateTime.Now,
        //                        id_tipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Servidor Virtual").Single().id_tipo,
        //                        observaciones = e.Observaciones,
        //                        nro_serie = e.NroSerie,
        //                        id_usuario = idusuario
        //                    };
        //                    dbc.UnidadConf.Add(uc);
        //                    Servidor server = new Servidor();
        //                    server.id_uc = uc.id_uc;
        //                    server.id_dominio = idDominio;
        //                    server.id_marca = idMarca;
        //                    server.modelo = e.Modelo;
        //                    //if (e.Memoria != null)
        //                    //{
        //                    //    UnidadMedida um = dbc.UnidadMedida.Where(c => c.id_um == e.Memoria.Um.Id).Single();
        //                    //    server.id_um_mem = um.id_um;
        //                    //    server.memoria = e.Memoria.Capacidad;
        //                    //}
        //                    //if (e.Disco != null)
        //                    //{
        //                    //    UnidadMedida um = dbc.UnidadMedida.Where(c => c.id_um == e.Disco.Um.Id).Single();
        //                    //    server.id_um_mem = um.id_um;
        //                    //    server.disco = e.Disco.Capacidad;
        //                    //    server.cantidaddiscos = Int32.Parse(e.Disco.Cantidad);
        //                    //}
        //                    //if (e.Cpu != null)
        //                    //{
        //                    //    server.cpu = e.Cpu;
        //                    //    server.cantidadcpu = Int32.Parse(e.CantidadCpu);
        //                    //}
        //                    if (e.Ip != null)
        //                    {
        //                        Ip ip = new Ip
        //                        {
        //                            dns = e.Ip.Dns,
        //                            gateway = e.Ip.Dns,
        //                            wins = e.Ip.Wins,
        //                            mascara = e.Ip.Mascara,
        //                            ip_v4 = e.Ip.IpV4,
        //                            ip_v6 = e.Ip.IpV6
        //                        };
        //                        server.Ip1 = ip;
        //                    }
        //                    server.puesta_produccion = e.PuestaProduccion;
        //                    //server.unidadesRack = e.UnidadesRack;
        //                    server.licenciaSO = e.LicenciaOs;
        //                    if (e.Os != null)
        //                    {
        //                        Os os = dbc.Os.Where(c => c.id_os == e.Os.Id).Single();
        //                    }
        //                    server.id_dependencia = idDependencia;
        //                    server.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Infraestructura").Single().id_estado;
        //                    dbc.Servidor.Add(server);
        //                    dbc.SaveChanges();
        //                    result.Info = "ok";
        //                    result.Detail = "Servidor agregado!!!";
        //                }
        //                else
        //                {
        //                    result.Detail = "El equipo con esa Oblea ya existe";

        //                }
        //            }
        //            else
        //            {
        //                ServersLog log = new ServersLog
        //                {
        //                    nombre = e.Oblea,
        //                    descripcion = match.Value
        //                };
        //                dbc.ServersLog.Add(log);
        //                result.Detail = e.Oblea + "No cumple los requisitos";
        //                dbc.SaveChanges();
        //            }
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        result.Detail = exc.Message;
        //    }
        //    return Json(result);
        //}
        public JsonResult NuevoServer(ServerInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf
                                  where (u.nombre == e.Oblea |
                                      (e.NroSerie.Length > 0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.id_tipo == e.TipoEquipo.id))
                                  select u).FirstOrDefault();

                    if (encontrado == null)
                    {                        
                            usuarioBean actual = Session["usuario"] as usuarioBean;
                            UnidadConf uc = new UnidadConf
                            {
                                nombre = e.Oblea,
                                id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion,
                                fecha = DateTime.Now,
                                id_tipo = e.TipoEquipo.id,
                                observaciones = e.Observaciones,
                                nro_serie = e.NroSerie,
                                id_usuario = actual.IdUsuario
                            };
                            dbc.UnidadConf.Add(uc);
                            Servidor server = new Servidor();
                            server.id_uc = uc.id_uc;
                        server.UnidadConf = uc;
                            server.id_dependencia = e.Dependencia.idDependencia;
                            server.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Infraestructura").Single().id_estado;
                            server.id_dominio = e.Dominio != null ? e.Dominio.Id : (int?)null;
                            server.id_marca = e.Marca.idMarca;
                            server.modelo = e.Modelo;
                            server.criticidad = e.Criticidad;
                            server.memoria = e.Memoria.Capacidad;
                            server.nombreComun = e.NombreComun;
                            if (e.Memoria.Um.Id == 0)
                            {
                                server.id_um_mem = null;
                            }
                            else server.id_um_mem = e.Memoria.Um.Id;
                        //disco nuevo
                        if (e.Storage != null)
                        {                            
                            server.ServerStorage = e.Storage.Select(c =>
                            new ServerStorage
                            {
                                capacidad = c.Capacidad,
                                datastore = c.DataStore,
                                letra = c.LetraUnidad,
                                UnidadMedida = dbc.UnidadMedida.Where(d => d.id_um == c.Um.Id).Single()
                            }).ToList();
                        }
                        //ip nuevo
                        if (e.Ips != null)
                        {                            
                            server.UnidadConf.Ip = e.Ips.Select(c => new Ip { ip_v4 = c.IpV4 }).ToList();
                        }
                        if (e.Os.Id == 0)
                            {
                                server.os = null;
                            }
                            else server.os = e.Os.Id;
                            server.cpu = e.Cpu;                            
                            server.cantidadcpu = e.CantidadCpu;
                            
                            server.puesta_produccion = e.PuestaProduccion;
                            server.rack = e.Rack;
                            server.unidadesRack = e.UnidadesRack;
                            server.licenciaSO = e.LicenciaOs;
                            dbc.Servidor.Add(server);
                            dbc.SaveChanges();                            
                            result.Info = "ok";
                            result.Detail = "Servidor agregado!!!";                        
                    }
                    else
                    {
                        result.Detail = "El equipo con esa Oblea o Nro. de serie ya existe";                        
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }

            return Json(result);
        }
        public JsonResult NuevaMarca(string nombre, int idTipo)
        {
            Responses result = new Responses();
            if (nombre.Length == 0)
            {
                result.Detail = "El campo no puede estar vacío";
            }
            else
            {
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var found = (from u in dbc.Marca
                                     where u.descripcion == nombre && u.Tipo_equipo.id_tipo==idTipo
                                     select u).SingleOrDefault();
                        if (found != null)
                        {
                            result.Detail = "La marca ya existe";
                            return Json(result);
                        }
                        else
                        {
                            Marca m = new Marca
                            {
                                descripcion = nombre,
                                dispositivo = idTipo,
                                id_direccion = (from u in dbc.Direcciones where u.nombre == "Infraestructura" select u.id_direccion).SingleOrDefault()
                            };
                            dbc.Marca.Add(m);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult EquipoXIp(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = dbc.Servidor.Where(u => u.Ip1.ip_v4.StartsWith(prefixText)).Select(c =>
                          new EquipoComplete
                          {
                              idEquipo = c.id_uc,
                              Oblea = c.UnidadConf.nombre,
                              NroSerie = c.UnidadConf.nro_serie,
                              Ip4 = c.Ip1.ip_v4
                          }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXNComun(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Servidor
                         where u.nombreComun.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             NroSerie = u.UnidadConf.nro_serie,
                             NombreComun=u.nombreComun
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXOblea(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where (u.Tipo_equipo.Direcciones.nombre == "Infraestructura") & 
                         u.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXSerie(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where u.nro_serie.StartsWith(prefixText) & u.Tipo_equipo.Direcciones.nombre=="Infraestructura"
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult GetEquipo(string oblea)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Servidor Físico": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Servidor Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Workstation Virtual": equipo = new ServerInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Storage": equipo = new StorageInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Cluster": equipo = new ClusterInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        case "Librería": equipo = new LibreriaInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);
        }
        public JsonResult EstadosEquipo()
        {
            List<EstadoEquipoVista> estados = new List<EstadoEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                estados = (from u in dbc.Estado_UC
                           where u.Direcciones.nombre == "Infraestructura"
                           orderby u.descripcion
                           select new EstadoEquipoVista
                           {
                               idEstado = u.id_estado,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(estados);
        }
    }
}
