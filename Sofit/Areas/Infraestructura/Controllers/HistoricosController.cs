﻿using Sofit.DataAccess;
using soporte.Areas.Infraestructura.Models;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Infraestructura.Controllers
{
    [SessionActionFilter]
    public class HistoricosController : Controller
    {
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Historicos");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            ModelHistoricos model = new ModelHistoricos();
            List<Area> areas = new List<Area>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<TipoResolucion> resoluciones = new List<TipoResolucion>();
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<Turno> turnos = new List<Turno>();
            List<TipoTicketVista> tiposTicket = new List<TipoTicketVista>();
            List<DireccionVista> direcciones = new List<DireccionVista>();
            List<PrioridadesTicketVista> prioridadesTicket = new List<PrioridadesTicketVista>();
            List<ServiciosVista> servicios = new List<ServiciosVista>();
            using (var dbc = new IncidenciasEntities())
            {
                servicios = (from u in dbc.Servicios
                             select new ServiciosVista
                             {
                                 Id = u.id_servicio,
                                 Nombre = u.nombre
                             }).ToList();
                tiposTicket = (from u in dbc.TipoTicket
                               select new TipoTicketVista
                               {
                                   Id = u.id,
                                   Nombre = u.nombre
                               }).ToList();
                prioridadesTicket = (from u in dbc.PrioridadTicket
                                     select new PrioridadesTicketVista
                                     {
                                         Id = u.id,
                                         Nombre = u.nombre
                                     }).ToList();
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Infraestructura"
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Infraestructura" & u.activo.Value
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
                turnos = (from u in dbc.turno
                          select new Turno
                          {
                              idTurno = u.id_turno,
                              Nombre = u.descripcion
                          }).ToList();
                direcciones = (from u in dbc.Direcciones
                               select new DireccionVista
                               {
                                   idDireccion = u.id_direccion,
                                   Nombre = u.nombre
                               }).ToList();
                resoluciones = (from u in dbc.tipo_resolucion
                                where u.area.Direcciones.nombre == "Infraestructura"
                                orderby u.descripcion
                                select new TipoResolucion
                                {
                                    idTipoResolucion = u.id_tipo_resolucion,
                                    Nombre = u.descripcion
                                }).ToList();
            }
            List<TipoResolucion> TiposTrabajo = new List<TipoResolucion>();
            foreach (var v in resoluciones)
            {
                TextInfo txtInfo = new CultureInfo("es-ar", false).TextInfo;
                string lower = v.Nombre.ToLower();
                string palabra = txtInfo.ToTitleCase(lower);
                TipoResolucion nuevo = new TipoResolucion
                {
                    idTipoResolucion = v.idTipoResolucion,
                    Nombre = palabra
                };
                if (!TiposTrabajo.Contains(nuevo)) TiposTrabajo.Add(nuevo);
            }
            model.Servicios = servicios;
            model.TiposTicket = tiposTicket;
            model.PrioridadesTicket = prioridadesTicket;
            model.Areas = areas;
            model.Tecnicos = tecnicos;
            model.Resoluciones = TiposTrabajo;
            model.Turnos = turnos;
            model.Jurisdicciones = jurisdicciones;
            model.Direcciones = direcciones;
            return View(model);
        }

        public JsonResult GetIncHistoricoPorNro(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.AreaXIncidencia
                         where u.Incidencias.numero.StartsWith(prefixText)
                         group u by u.Incidencias.numero into g
                         select g.Key).Take(7).ToList();
                if (items.Count() == 0)
                {
                    items = (from u in dbc.AreaXIncidenciaHistorico
                             where u.Incidencias.numero.StartsWith(prefixText)
                             group u by u.Incidencias.numero into g
                             select g.Key).Take(7).ToList();
                }
            }
            return Json(items);
        }
        public JsonResult getHistoricoIncidente(string text)
        {
            HistIncBuilder incBuilder = new HistIncBuilder();
            incBuilder.Construir(text);
            return Json(incBuilder.getResult());
        }
        public JsonResult GetDatos()
        {
            HistoricosParametros param = new HistoricosParametros();
            param.FechaDesde = new DateTime(DateTime.Now.Year, 1, 1);
            param.FechaHasta = DateTime.Now;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();

            //obtengo datos
            if (nvc.Count > 0)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("xtecn")) param.idTecnico = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("xjuri")) param.idJurisdiccion = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("xtrab")) param.idTipoResolucion = Int32.Parse(nvc.GetValues(key)[0]);
                    if (key.StartsWith("rangofechadesde")) param.FechaDesde = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (key.StartsWith("rangofechahasta")) param.FechaHasta = DateTime.ParseExact(nvc.GetValues(key)[0], "dd/MM/yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59);
                    if (key.StartsWith("oblea")) param.oblea = nvc.GetValues(key)[0];
                    if (key.StartsWith("incide")) param.nroIncidente = nvc.GetValues(key)[0];
                    if (key.StartsWith("total"))
                    {
                        string valor = nvc.GetValues(key)[0];
                        switch (valor)
                        {
                            case "xt": param.xTecnico = true;
                                break;
                            case "xj": param.xJurisdiccion = true;
                                break;
                            case "xtr": param.xTrabajo = true;
                                break;
                        }
                    }

                }
                ResolucionesVista controladorResoluciones = new ResolucionesInfraestructura();
                //consulta con parametros
                if (!param.xTrabajo && !param.xJurisdiccion && !param.xTecnico)
                {
                    result.Detail = controladorResoluciones.getResolucionesParametros(param);
                    result.Info = "ok";
                    //guardo en session
                    //saveSource(resoluciones);
                }
                else
                {
                    //son totales
                    if (param.xJurisdiccion)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesJurisdiccion(param);
                        result.Info = "ok";
                    }
                    else if (param.xTecnico)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesTecnico(param);
                        result.Info = "ok";
                    }
                    else if (param.xTrabajo)
                    {
                        result.Detail = controladorResoluciones.getResolucionesTotalesTrabajo(param);
                        result.Info = "ok";
                    }
                }
            }
            return Json(result);
        }

    }
}

