﻿using Sofit.DataAccess;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Infraestructura.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            return View();
        }
        public JsonResult NuevoUsuario(string login, string nombre, string p1, int perfil)
        {
            Responses result = new Responses();
            //usuarioHandler user = new usuarioHandler();
            //IDirecciones soporte = DireccionesFactory.getDireccion("Infraestructura");
            //result = user.nvoUsuario(login, nombre, p1, perfil, soporte);
            return Json(result);
        }
        public ActionResult Usuarios()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Usuarios");
            IDirecciones infraDir = DireccionesFactory.getDireccion("Infraestructura");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesInfraestructura();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getTodosDeDireccion(infraDir);
                return PartialView("Usuarios", model);
            }
        }
       
        public ActionResult Tipificaciones()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Tipificaciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre == "Infraestructura"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Infraestructura");
                model.Tipificaciones = arbol.getResult();
                return View(model);
            }
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre, int idServicio)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Infraestructura" select u).Single();
                    Tipificaciones nueva = new Tipificaciones()
                    {
                        nombre = nombre,
                        descripcion = desc,
                        id_padre = idPadre,
                        id_servicio = idServicio == 0 ? (int?)null : idServicio,
                        id_direccion = direccionSoporte.id_direccion
                    };
                    dbc.Tipificaciones.Add(nueva);

                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas, int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Infraestructura");
            using (var dbc = new IncidenciasEntities())
            {
                var serv = new Servicios
                {
                    nombre = nombre,
                    tiempo_res = totales.Hours,
                    tiempo_margen=totalesMargen.Hours,
                    id_direccion = direccion.IdDireccion,
                    tolerancia = 80,
                    vigente = true
                };
                dbc.Servicios.Add(serv);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }        
       
        public JsonResult quitarServicio(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var serv = dbc.Servicios.Where(c => c.id_servicio == id).Single();
                    dbc.Servicios.Remove(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = "El servicio está siendo utilizado";
            }
            return Json(result);
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t = (from u in dbc.Tipificaciones
                             where u.id_tipificacion == id
                             select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult borrarEquipo(string oblea)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var equipo = dbc.UnidadConf.Where(c => c.nombre == oblea).SingleOrDefault();
                    
                    if (equipo != null)
                    {
                        switch(equipo.Tipo_equipo.descripcion)
                        {                            
                            case "Storage": var storage = equipo.Storage.Where(c => c.id_uc == equipo.id_uc).Single();                                
                                dbc.Storage.Remove(storage);
                                dbc.UnidadConf.Remove(equipo);
                                break;
                            case "Cluster": var cluster = equipo.Cluster.Where(c => c.id_uc == equipo.id_uc).Single();
                                cluster.Servidor.Clear();
                                dbc.Cluster.Remove(cluster);
                                dbc.UnidadConf.Remove(equipo);
                                break;
                            case "Librería": ;
                                break;
                            default: var server = equipo.Servidor.Where(c => c.id_uc == equipo.id_uc).Single();
                                dbc.Servidor.Remove(server);
                                dbc.UnidadConf.Remove(equipo);
                                break;
                                
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {
                        result.Detail="No se encontró el equipo";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
    }
}
