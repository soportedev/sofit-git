﻿using Sofit.DataAccess;
using soporte.Areas.Infraestructura.Models;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Infraestructura.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Infraestructura/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            InfraestructuraModel model = new InfraestructuraModel();
            List<IncTeleVista> incIngresados = new List<IncTeleVista>();
            List<IncTeleVista> incInfra = new List<IncTeleVista>();
            List<IncTeleVista> incSo = new List<IncTeleVista>();
            List<IncTeleVista> incTotales = new List<IncTeleVista>();
            List<IncTeleVista> incSuspendidos = new List<IncTeleVista>();
            List<TipificacVista> tipificaciones = new List<TipificacVista>();
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Area> areas = new List<Area>();
            List<TipoResolucion> tipoResoluciones = new List<TipoResolucion>();
            List<TipoResolucion> tipoResolucionesSo = new List<TipoResolucion>();
            List<TipoResolucion> tipoResolucionesTotales = new List<TipoResolucion>();
            List<TipoResolucion> tipoFalla = new List<TipoResolucion>();
            List<Turno> turnos = new List<Turno>();
            List<DireccionVista> subdir = new List<DireccionVista>();
            TiemposInfraestructura tiempos = new TiemposInfraestructura();
            IDirecciones dirInfraestructura = DireccionesFactory.getDireccion("Infraestructura");
            using (var dbc = new IncidenciasEntities())
            {
                incTotales = (from d in dbc.selectIncidenciasActivas(dirInfraestructura.IdDireccion)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value
                              }).ToList();
                incIngresados = incTotales.Where(c => c.Area.NombreArea == "Entrada Infraestructura" & c.Estado!="Suspendido").ToList();
                incInfra = incTotales.Where(c => c.Area.NombreArea == "Infraestructura" & c.Estado != "Suspendido").ToList();
                incSo = incTotales.Where(c => c.Area.NombreArea == "Sistemas Operativos" & c.Estado != "Suspendido").ToList();
                incSuspendidos = incTotales.Where(c => c.Estado == "Suspendido").ToList();
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Infraestructura"
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Infraestructura" & u.activo.Value==true
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
                turnos = (from u in dbc.turno
                          select new Turno
                          {
                              idTurno = u.id_turno,
                              Nombre = u.descripcion
                          }).ToList();
                tipoResoluciones = (from u in dbc.tipo_resolucion
                                    where u.area.descripcion == "Infraestructura"
                                    orderby u.descripcion
                                    select new TipoResolucion
                                    {
                                        idTipoResolucion = u.id_tipo_resolucion,
                                        Nombre = u.descripcion
                                    }).ToList();
                tipoResolucionesSo = (from u in dbc.tipo_resolucion
                                      where u.area.descripcion == "Sistemas Operativos"
                                      orderby u.descripcion
                                      select new TipoResolucion
                                      {
                                          idTipoResolucion = u.id_tipo_resolucion,
                                          Nombre = u.descripcion
                                      }).ToList();
                tipoResolucionesTotales = (from u in dbc.tipo_resolucion
                                      where u.area.Direcciones.nombre=="Infraestructura"
                                      orderby u.descripcion
                                      select new TipoResolucion
                                      {
                                          idTipoResolucion = u.id_tipo_resolucion,
                                          Nombre = u.descripcion
                                      }).ToList();
                subdir = (from u in dbc.Direcciones
                          select new DireccionVista
                          {
                              idDireccion = u.id_direccion,
                              Nombre = u.nombre
                          }).ToList();
            }
            foreach (IncTeleVista t in incIngresados)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incInfra)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incSo)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incSuspendidos)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }       
            BuilderTipifTree arbol = new BuilderTipifTree();
            arbol.construir("Infraestructura");
            model.IncidentesIngresados = incIngresados;
            model.IncidentesInfraestructura = incInfra;
            model.IncidentesSo = incSo;
            model.IncidentesSuspendidos = incSuspendidos;
            model.TipoResolucion = tipoResoluciones;
            model.TipoResolucionSo = tipoResolucionesSo;
            model.TipoResolucionTodas = tipoResolucionesTotales;
            model.Turnos = turnos;
            model.Areas = areas;
            model.Tecnicos = tecnicos;
            model.Tipificaciones = arbol.getResult();
            model.Jurisdicciones = jurisdicciones;
            model.SubDirecciones = subdir;
            return View(model);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var tecnico = (from u in dbc.tecnico
                                   where u.descripcion == Tecnico
                                   select u).SingleOrDefault();
                    foreach (string strId in idIncidencia)
                    {
                        int idIncidente = Int32.Parse(strId);                        
                        var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                        incActiva.id_tecnico = tecnico.id_tecnico;
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }        
        public JsonResult suspenderIncidentes(int idInc)
        {
            Responses result = new Responses();
            int idUsuarioActual = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.suspenderIncidencia(idUsuarioActual);
            return Json(result);
        }
        public JsonResult cargarSolucion(HttpPostedFileBase imagen)
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int idTipoResolucion = Int32.Parse(nvc.Get("tipoResolucion"));
            int idTurno = Int32.Parse(nvc.Get("idTurno"));
            string observacion = nvc.Get("observaciones");
            //sobrescribir ultima
            bool sobrescribir = false;
            sobrescribir = Boolean.Parse(nvc.Get("sobrescribir"));
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, sobrescribir, false, false);
            //
            Incidencia inc = builder.getResult();
            bool hayImagen = imagen != null;
            string nombreFile = string.Empty;            
            #region foto
            if (hayImagen)
            {
                string pathImagen = string.Empty;
                string nombreArchivo = imagen.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string ran = new Random().Next(10000).ToString("0000");
                nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getResolucionCustom(nombreFile);
                    var stream = imagen.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion            
            //buscar equipos
            bool flag = true;
            List<int> idEquipos = new List<int>();
            bool esParaEquipo = Boolean.Parse(nvc.Get("esParaEquipo"));
            if (esParaEquipo)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("idEquipo")) idEquipos.Add(Int32.Parse(nvc.GetValues(key)[0]));
                }
                if (idEquipos.Count == 0)
                {
                    result.Info = "error";
                    result.Detail = "No se cargaron las obleas del equipo.";
                    flag = false;
                }
                if (flag)
                {
                    foreach (int id in idEquipos)
                    {
                        inc.cargarSolucion(idUsuario, id, idTipoResolucion, idTecnico, idTurno, observacion, DateTime.Now, nombreFile, sobrescribir);
                    }
                    result.Info = "ok";
                }
            }
            else
            {
                result = inc.cargarSolucion(idUsuario, null, idTipoResolucion, idTecnico, idTurno, observacion, DateTime.Now, nombreFile, sobrescribir);
            }
            return Json(result);
        }
        public JsonResult cargaFalla()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));

            int idTipoResolucion = Int32.Parse(nvc.Get("tipoFalla"));
            DateTime fecha = DateTime.Now;
            DateTime.TryParse(nvc.Get("datetimeinput"), out fecha);
            //ids equipo
            List<int> ids = new List<int>();
            foreach (string key in nvc.Keys)
            {
                if (key.StartsWith("idEquipo"))
                {
                    ids.Add(Int32.Parse(nvc.Get(key)));
                }
            }
            //mandatory
            int idTipoTrabajo = Int32.Parse(nvc.Get("idTipoTrabajo"));
            int idTurno = Int32.Parse(nvc.Get("idTurno"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            string observacion = nvc.Get("observaciones");
            string derivarAreas = nvc.Get("derivarAreas");
            int idDireccion = 0;
            string area = nvc.Get("area");
            bool parar = false;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, false, false, false);
            Incidencia inc = builder.getResult();
            string nombreImagen = string.Empty;
            bool nosobrescribir = false;
            if (ids.Count > 0)
            {//agrego el enlace
                int idEnlace = ids[0];
                using (var dbc = new IncidenciasEntities())
                {
                    UnidadConf e = (from u in dbc.UnidadConf where u.id_uc == idEnlace select u).First();
                    if (e.Tipo_equipo.descripcion == "Enlace")
                    {
                        EnlaceComunicaciones enlace = (from u in dbc.EnlaceComunicaciones where u.id_uc == idEnlace select u).First();
                        EnlaceXIncidencia exin = (from u in dbc.EnlaceXIncidencia where u.id_incidencia == idIncidencia select u).FirstOrDefault();
                        if (exin != null)
                        {
                            result.Detail = "El incidente ya tiene cargado el enlace " + exin.EnlaceComunicaciones.UnidadConf.nombre;
                            parar = true;
                        }
                        else
                        {//no tiene enlace y se lo cargo
                            int idEstadoEnlace = (dbc.Estado_UC.Where(c => c.descripcion == "En Reparación").First()).id_estado;
                            //actualizo estado enlace
                            enlace.id_estado = idEstadoEnlace;
                            EnlaceXIncidencia exi = new EnlaceXIncidencia
                            {
                                id_enlace = idEnlace,
                                id_estado = idEstadoEnlace,
                                id_incidencia = idIncidencia,
                                id_tecnico = idTecnico,
                                fecha_inicio = fecha
                            };
                            EstadoXEnlace exe = new EstadoXEnlace
                            {
                                id_enlace = idEnlace,
                                id_estado = idEstadoEnlace,
                                id_incidencia = idIncidencia,
                                id_tecnico = idTecnico,
                                fecha_inicio = fecha
                            };
                            dbc.EnlaceXIncidencia.Add(exi);
                            dbc.EstadoXEnlace.Add(exe);
                            dbc.SaveChanges();
                        }
                    }
                }
            }
            if (!parar)
            {
                int? idEquipo = ids.Count == 0 ? (int?)null : ids[0];
                //cargo el trabajo en el incidente                
                result = inc.cargarSolucion(idUsuario, idEquipo, idTipoResolucion, idTecnico, idTurno, observacion, DateTime.Now, nombreImagen, nosobrescribir);
                if (!(inc.EstadoActual is EstadoIncidenciaDerivado)) inc.cambioEstado(EstadoIncidenciaFactory.getEstado("Derivado"), idTecnico, idUsuario);
                if (derivarAreas == "on")
                {
                    idDireccion = Int32.Parse(nvc.Get("subdireccionDerivacion"));
                    IDirecciones SubDirDestino = DireccionesFactory.getDireccion(idDireccion);
                    IAreaIncidencia areaDestino = SubDirDestino.getAreaEntrada();
                    inc.cambioArea(areaDestino, SubDirDestino.getIdTecnicoGenerico(), idUsuario);
                }
                else
                {
                    result = inc.cambioArea(AreaIncidenciaFactory.getArea("Operaciones"), idTecnico, idUsuario);
                }
            }
            return Json(result);
        }
        public JsonResult derivarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idAreaDestino = Int32.Parse(nvc.Get("idAreaDestino"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idAreaDestino);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, true, false, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.derivar(areaDestino.IdArea, idTecnico == 0 ? areaDestino.getIdTecnicoGenerico() : idTecnico, idUsuario);
            return Json(result);
        }
        public JsonResult cargaTrabajoEnlace()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idTipoResolucion = Int32.Parse(nvc.Get("tipoRes"));
            int idTurno = Int32.Parse(nvc.Get("idTurno"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int idE = Int32.Parse(nvc.Get("idEquipo"));
            string observacion = nvc.Get("observaciones");
            DateTime fecha;
            DateTime.TryParse(nvc.Get("datetimeI"), out fecha);

            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, true, true, false);
            Incidencia inc = builder.getResult();
            result = inc.cargarSolucionEnlace(idUsuario, idE, idTipoResolucion, idTecnico, idTurno, null, observacion, 0, fecha);

            return Json(result);
        }
        public JsonResult cerrarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, true, true, false);
            Incidencia inc = builder.getResult();
            result = inc.cerrarIncidencia(idUsuario);
            return Json(result);
        }
        public JsonResult derivarADireccion(int idDir, int idInc)
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false, false);
            Incidencia inc = builder.getResult();
            result = inc.derivarIncidenciaADireccion(idDir, idUsuario);
            return Json(result);
        }
        public JsonResult eliminarIncidente(int idInc)
        {
            Responses result = new Responses();
            IncidenciasManager mgr = new IncidenciasManager();
            result = mgr.eliminarIncidente(idInc);
            return Json(result);
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {

                items = (from u in dbc.IncidenciasActivas
                         where u.area.Direcciones.nombre == "Infraestructura" & u.Incidencias.numero.StartsWith(prefixText)
                         select u.Incidencias.numero).Take<string>(7).ToList();                               
            }
            return Json(items);
        }
        public JsonResult UbicacionIncidente(string texto)
        {
            DatosUbicacion ubic = new DatosUbicacion();
            using (var dbc = new IncidenciasEntities())
            {
                var dd = (from u in dbc.IncidenciasActivas
                          where u.Incidencias.numero == texto//& u.area.Direcciones.nombre == "Telecomunicaciones"
                          select u).FirstOrDefault();
                if (dd != null)
                {
                    ubic.Estado = dd.Estado_incidente.descripcion;
                    ubic.Area = dd.area.descripcion;
                    ubic.Numero = dd.Incidencias.numero;
                    ubic.SubDireccion = dd.area.Direcciones.nombre;
                }
            }
            return Json(ubic);
        }
        public JsonResult incidentesToLife(string nroInc)
        {
            IncidenciasManager im = new IncidenciasManager();
            IDirecciones operaciones = DireccionesFactory.getDireccion("Operaciones");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            return Json(im.reanimarIncidencia(nroInc, idUsuario, operaciones));
        }

    }
}
