﻿$(function () {
    //Trabajos BandejaEntrada
    $('#Trabajos').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        //position: ['center', 150],
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            var thisdialog = $(this);
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            clearTrabajosPanel(thisdialog);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');            
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);            
            $('.error', this).hide();
        },
        resizeStop: function (ev, ui) {
            var originalSize = ui.originalSize;
            var size = ui.size;
            var textArea = $(ev.target).find('.tSolucion');
            var heightDif = originalSize.height - size.height;
            var widthDif = originalSize.width - size.width;
            textArea.height(textArea.height() - heightDif);
            //textArea.width(textArea.width() - widthDif);
        },
        buttons: {
            Aceptar: {
                class: 'aceptardialog',
                text: 'Aceptar',
                click: function () {
                    var $thisButton = $(this).dialog('widget').find('button.aceptardialog');
                    var fData = new FormData();
                    var $thisButton = $(this).dialog('widget').find('button.aceptardialog');
                    var thisdialog = $(this);
                    var datos = $(this).data('datos');
                    var nombreGrilla = datos.nombreGrilla;
                    $('.error', this).hide();
                    var datos = $(this).data('datos');
                    var nombreGrilla = datos.nombreGrilla;
                    var flagOk = false;
                    if (nombreGrilla == 'grillaBandeja') {
                        var ttrabajo = $('li.bandeja').find('select option:selected').val();
                    }
                    if (nombreGrilla == 'grillaSo') {
                        var ttrabajo = $('li.sistoperativo').find('select option:selected').val();
                    }
                    if (nombreGrilla == 'grillaInfraestructura') {
                        var ttrabajo = $('li.infra').find('select option:selected').val();
                    }
                    if (ttrabajo == 0) {
                        $('#Trabajos').find('.message').html('Seleccione el tipo de Trabajo.');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    var tecnico = $('.dTecnicos option:selected', thisdialog);
                    var obs = $('#observaciones');
                    if ($.trim(obs.val()).length < 10) {
                        $('#Trabajos').find('.message').html('Complete el campo de Observaciones (al menos 10 caracteres)');
                        $('.error, #Trabajos').show();
                        return;
                    }
                    var nData = {
                        idIncidente: datos.idIncidente,
                        nroIncidente: datos.nroIncidente,
                        tipoResolucion: ttrabajo,
                        observaciones: obs.val(),
                        tipoResolucion: ttrabajo,
                        idTecnico: tecnico.val(),
                        url: "Incidentes/cargarSolucion",
                        idTurno: $('.dTurnos option:selected', thisdialog).val()
                    };
                    var inpFile = $('#upFileRes');
                    var img = inpFile.get(0).files[0];
                    fData.append('imagen', img);
                    fData.append('idTecnico', nData.idTecnico);
                    fData.append('idTurno', nData.idTurno);
                    fData.append('observaciones', nData.observaciones);
                    fData.append('tipoResolucion', ttrabajo);
                    fData.append('idIncidente', nData.idIncidente);

                    if ($('#esParaEquipo').is(':visible')) {
                        var valorCheck = $('#esParaEquipo').is(':checked');
                        fData.append('esParaEquipo', valorCheck);
                        if ($('#esParaEquipo').is(':checked')) {
                            var els = $('#Trabajos').find('.idEquipo');
                            var sufix = 0;
                            $.each(els, function () {
                                fData.append('idEquipo.' + sufix, $(this).html());
                                sufix++;
                            });
                        }
                    }
                    fData.append('sobrescribir', $('#sobrescribir').is(':checked'));
                    $.ajax({
                        url: nData.url,
                        data: fData,
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            NProgress.start();
                            $($thisButton).button('disable');
                        },
                        success: function (data) {
                            if (data != null) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarExito('Trabajo cargado!');
                                    $('#Trabajos').dialog('close');
                                    actualizarPaneles('todos');
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            mostrarError(thrownError);
                        },
                        complete: function () {
                            NProgress.done();
                            $($thisButton).button('enable').removeClass("ui-state-focus ui-state-hover");
                        }
                    });
                }
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });//-->TRABAJOS
});//READY

function incidenteToLife() {
    var exito = false;
    if (validarNoVacio($('#tIncToLive'))) {
        var nroInc = $('#tIncToLive').val();
        $.ajax({
            url: "Incidentes/incidentesToLife",
            type: 'POST',
            data: 'nroInc=' + nroInc,
            beforeSend: ajaxReady = false,
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    exito = true;
                    mostrarExito('El Incidente ' + nroInc + ' fue reactivado');
                    $('#tIncToLive').val('');
                }
                else mostrarError(data.Detail);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                if (exito) actualizarPaneles('usuarios');
            }
        });
    }
}
function searchAutocomplete(request, response) {
    var texto = request.term;
    texto = texto.toUpperCase();
    var buscoEquipo = false;
    var buscoIncidente = false;
    if (regEquipoOperaciones.test(texto)) {
        buscoEquipo = true;
    }
    if (regularIncidente.test(texto)) {
        buscoIncidente = true;
    }
    var uri;
    if (buscoEquipo) uri = 'GetEquiposActivos';
    else uri = 'GetIncidentesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSession();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
function notificar() {

}
function derivar() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var context = $(this).parents('ul.fly');
    var origen = grilla.attr('id');
    var idArea = $('.sArea', context).val();
    var textArea = $('.sArea option:selected', context).text();
    var idTecnico = $('.sTecnico', context).val();
    var fData = new FormData();

    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Incidente");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(3)').text();
        fData.append("idAreaDestino", idArea);
        fData.append("idTecnico", idTecnico);
        fData.append("idIncidente", id_inc);
        if (window.ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarIncidente",
                data: fData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    window.ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el Incidente a ' + textArea);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    window.ajaxReady = true;
                    if (exito) {
                        actualizarPaneles("todos");
                        $('p.ajaxGif').hide();
                    }
                }
            });
        }
    }
}
function cargarTrabajos() {
    var t = $(this);
    var nro = [];
    var trs = {};
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Seleccione UN elemento");
    }
    else {
        var datos = nro[0].find('td').slice(1, 7);
        var idIncidente = datos[0].innerHTML;
        var nroIncidente = $(datos[1]).find('span').text();
        var nroEnlace = datos[2].innerHTML;
        var idEquipo = datos[3].innerHTML;
        var data = {};
        data.idIncidente = idIncidente;
        data.nroIncidente = nroIncidente;
        data.nombreGrilla = nombreGrilla;
        data.nroEnlace = nroEnlace;
        data.idEquipo = idEquipo;
        if (nombreGrilla == 'grillaBandeja') {
            $('li.tipoResolucion.infra').hide();
            $('li.tipoResolucion.sistoperativo').hide();
            $('li.tipoResolucion.bandeja').show();
            $('#Trabajos').data('datos', data).dialog('open');
        }
        if (nombreGrilla == 'grillaInfraestructura') {
            $('li.tipoResolucion.infra').show();
            $('li.tipoResolucion.sistoperativo').hide();
            $('li.tipoResolucion.bandeja').hide();
            $('#Trabajos').data('datos', data).dialog('open');
        }
        if (nombreGrilla == 'grillaSo') {
            $('li.tipoResolucion.infra').hide();
            $('li.tipoResolucion.sistoperativo').show();
            $('li.tipoResolucion.bandeja').hide();
            $('#Trabajos').data('datos', data).dialog('open');
        }
        
    }
}
function derivarEq() {

}
function cerrarIncidente() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN incidente");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);

        if (ajaxReady) {
            var divdialog = $('<div>').append("Se va a cerrar el incidente " + $(trsel[1]).text() + ". Está Seguro?").dialog({
                width: '250px',
                show: 'slide',
                //position: 'center',
                open: function () {
                    $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                },
                buttons: {
                    Aceptar: function () {
                        $.ajax({
                            url: "Incidentes/cerrarIncidente",
                            data: { idIncidente: $(trsel[0]).text() },
                            type: 'POST',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarInfo('Se ha Cerrado el Incidente');
                                    exito = true;
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                $(divdialog).dialog('destroy');
                                if (exito) {
                                    actualizarPaneles('todos');
                                }
                            }
                        });               //fin ajax
                    },
                    Cancelar: function () { $(this).dialog('destroy') }
                }

            });


        } //fin ajaxready
    }
}
function terminarEquipo() {

}
function asignarEquipo() {
    var exito = false;
    var nro = [];
    var User = $(this).parent().siblings().find('select.sTecnico option:selected').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));
        }
        asignarTecnicoEnlace(id_enlace, User);
    }
}
function tomarEquipo() {
    var exito = false;
    var nro = [];
    var User = $('#user').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));
        }
    }
    asignarTecnicoEnlace(id_enlace, User);
}
function actualizarPaneles(tabla) {
    if (window.ajaxReady) {
        $.ajax({
            url: "Incidentes",
            type: 'GET',
            beforeSend: function () {
                window.ajaxReady = false;
            },
            success: function (data) {
                var ventanas = window.ventanas;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                var results = new Array();
                results[0] = $(data).find('#grillaBandeja');
                results[1] = $(data).find('#grillaInfraestructura');
                results[2] = $(data).find('#grillaSo');
                results[3] = $(data).find('#grillaSuspendidos');
                
                switch (tabla) {
                    case "todos":
                        ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        ventanas[3].update(results[3]);                        
                        break;
                    default: ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        ventanas[3].update(results[3]);                        
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);

            },
            complete: function () {
                window.ajaxReady = true;
                $('p.ajaxGif').hide();
            }
        });//fin ajax
    }
}
/**Search Autocomplete Cliente**/
function searchAutocompleteCliente(request, response) {
    var texto = request.term;
    var uri = 'GetClientesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR Cliente*/
function buscarCliente(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];

    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/incidenteXCliente",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("No hay Incidentes para ese cliente");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Mesa de Ayuda": if (data.Estado == "Ingresado") doScroll(ventanas[0], data.Numero);
                        else {
                            if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                            else { }
                        }
                            break;
                        default: doScroll(ventanas[2], data.Numero);
                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
function ubicarIncidente(data)
{
    var direccion = data.SubDireccion;
   
    var area = data.Area;
    var estado = data.Estado;
    if (direccion == "Infraestructura") {
        if (estado == "Suspendido") {
            doScroll(ventanas[3], data.Numero);
        } else {
            switch (area) {
                case "Entrada Infraestructura": doScroll(ventanas[0], data.Numero);
                    break;
                case "Infraestructura": doScroll(ventanas[1], data.Numero);
                    break;
                case "Sistemas Operativos": doScroll(ventanas[2], data.Numero);
                    break;

            }
        }
    } else {
        mostrarInfo("El Ticket se encuentra en " + direccion);
    }
}
function buscarEquiposInc(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    resString = resString.toUpperCase();

    if (!regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }
    $.ajax({
        url: "Incidentes/UbicacionIncidente",
        data: datos,
        dataType: 'json',
        type: 'POST',
        beforeSend: function () {
            ajaxReady = false;
        },
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (data.Area == null) {
                mostrarInfo("El equipo/incidente no está activo");
                $('p.ajaxGif').hide();
            }
            else {
                ubicarIncidente(data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            ajaxReady = true;
        }
    });                          //fin ajax

}
function notificarIncidente() { }