﻿var pathServer ='Inventario/';
var NoEditable = new RegExp('^Oblea$|^NroSerie$|^idTipo$|^Marca$|^Modelo$|^Jurisdiccion$|^Ubicacion$|^Estado$|^Observaciones$|' +
    '^CPU$|^CantidadCPU$|^Memoria$|^UMMemoria$|^CapAlmacenamiento$|^UMDiscos$|^CantidadDiscos$|^FamiliaSO$|^VersionSO$|^LicenciaSO$|^FechaProd$|'+
        '^IPv4$|^Critico$|^Host$|^DataStore$|^ElementoRelacion$|^addHost$|^NComun$|^letraUnidadStore$');
$(function () {
    $('body').on('click', 'span.dropStorage', function () {
        var container = $(this).parents('div.row').remove();
    });
    $('#agregarIp').on('click', function () {
        var flag = false;
        var $ipv4 = $('#IPv4');
        var regIp = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
        if (regIp.test($ipv4.val())) {
            $ipv4.removeClass('error');
        } else {
            $ipv4.addClass('error');
            flag = true;
        }
        if (!flag) {
            var ip4 = $ipv4.val();
            agregarIp(0, ip4, true);
            $ipv4.val('');
        }
    }).button();
    
    $('#agregarDs').on('click', function () {       
        var letter = $('#letraUnidadStore');
        var dataSt = $('#DataStore');
        var tamanio = $('#CapAlmacenamiento');
        var um = $('#UMDiscos');
        
            var flag = false;
            if (letter.val()) {                
                letter.removeClass('error');
            }
            else {
                letter.addClass('error');
                flag = true;
            }
            if (dataSt.val()) {
                dataSt.removeClass('error');
            }
            else {
                dataSt.addClass('error');
                flag = true;
            }
            if (tamanio.val()) {
                tamanio.removeClass('error');
            }
            else {
                tamanio.addClass('error');
                flag = true;
            }
            var id_um = um.attr('data-id');
            if (id_um) {
                um.removeClass('error');
            }
            else {
                um.addClass('error');
                flag = true;
            }
            if (!flag) {
                agregarStorage(letter.val(), dataSt.val(), tamanio.val(), um.data('id'), um.val(),true);
                letter.val('');
                dataSt.val('');
                tamanio.val('');
                um.val('');
                um.removeAttr('data-id');
            }        
    }).button();    
   
    $('#searchButton').on('click', function () {
        var oblea = $('#tBuscar').val();
        GetEquipo(oblea);
    });
    // buscar
    $(document).keydown(function (e) {
        e.preventDefault
        if (e.which === 114) {
            $('#tBuscar').focus()
        }
    });    
    //reset errors
    $('input').each(function () {
        $(this).on('focus', function () {
            $(this).removeClass('error');
        })
    });
    //-->
    $('#CancelarCambios').on('click', function () {
        initialState();
    });
    // TRANFORM Uppercase
    $('#tBuscar, #Oblea').keyup(function (e) {
        var key = e.which;
        var val = $(this).val();
        if (key == 8 & val.length == 0) {
            return false;
        }
        $(this).val(this.value.toUpperCase())
    });    
    $("#tBuscar").autocomplete({
        minLength: "2",
        source: searchAutocomplete,
        select: function (e, ui) {
            $('#tBuscar').val(ui.item.oblea);
            GetEquipo(ui.item.oblea);
            return false;
        },
        focus: function (e, ui) {
            $('#tBuscar').val(ui.item.label);
            return false;
        }
    });
    //--->
    $('#editData').click(function () {
        editState();
    });
    $('#idTipo').on('change', function () {
        var texto = $(this).val();
        switch (texto) {
            case "Servidor Físico": showingServer();
                break;
            case "Servidor Virtual": showingServer();
                break;
            case "Workstation Virtual": showingServer();
                break;
            case "Cluster": showingCluster();
                break;            
            default: hidingDetails();
                break;
        }
    });
    //estados
    $.ajax({
        url: 'Inventario/EstadosEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idEstado;
            }
            // Function
            $('#Estado').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus, tres) {
            mostrarError(tres, 'error');
        }
    });
    //-->
    // TIPO autocomlete
    $.ajax({
        url: pathServer+'TiposEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];            
            for (var i = 0; i < raw.length; ++i) {
                var mapping = {};
                mapping.value = raw[i].id;
                mapping.label = raw[i].Nombre;
                source.push(mapping);                
            }           
            // Function
            $('#idTipo').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label).change();
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');

                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //-->Tipo
    //Um memoria
    $.ajax({
        url: pathServer + 'UmMemoria',
        type: 'post',
        dataType: 'json',       
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            for (var i = 0; i < raw.length; ++i) {
                var mapping = {};
                mapping.value = raw[i].Id;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            // Function
            $('#UMMemoria').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //-->Um mem
    //Um disco
    $.ajax({
        url: pathServer + 'UmDisco',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            for (var i = 0; i < raw.length; ++i) {
                var mapping = {};
                mapping.value = raw[i].Id;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            // Function
            $('#UMDiscos').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('').removeAttr('data-id');

                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //-->Um disco
    //familia so
    $.ajax({
        url: pathServer + 'FamiliaSO',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            for (var i = 0; i < raw.length; ++i) {
                var mapping = {};
                mapping.value = raw[i].Id;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            // Function
            $('#FamiliaSO').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    return false;
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable === 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If input is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    //-->familia so
    //Marca
    $('#Marca').click(function (e) {
        var disable = $(this).attr('readonly');
        var marca = $(this);
        // Check input status
        if (disable === 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {
            // If input is writable
            // Clean val
            $(this).val('');
            var idTipo = $('#idTipo').attr('data-id');
            if (idTipo != "" & idTipo != undefined) {
                $.ajax({
                    data: { 'tipo': idTipo },
                    url: '/Service/MarcasPorTipo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        // Show Loading
                        NProgress.start();
                    },
                    success: function (response) {
                        // Show Loading
                        NProgress.done();
                        // Values
                        var raw = response;
                        var marcasS = [];
                        var mapping = {};
                        mapping.value = -1;
                        mapping.label = "Agregar";
                        marcasS.push(mapping);
                        for (var i = 0; i < raw.length; ++i) {
                            mapping = new Object();
                            mapping.value = raw[i].idMarca;
                            mapping.label = raw[i].Nombre;
                            marcasS.push(mapping);
                        }
                        // Function
                        $(marca).autocomplete({
                            source: marcasS,
                            minLength: 0,
                            select: function (event, ui) {
                                if (ui.item.value == -1) {
                                    AgregarNuevaMarca();
                                    return false;
                                } else {
                                    $(this).attr('data-id', ui.item.value);
                                    $(this).val(ui.item.label);
                                    return false;
                                }
                            },
                            close: function () {
                                // Finish selection and make a blur
                                $(this).blur();
                            }
                        }).autocomplete('search');
                    },
                    // On error
                    error: function (response, textStatus, tres) {                        
                        mostrarError(tres);
                    }
                });// Ajax
            } else {
                mostrarError("Seleccione el Tipo de Equipo!!");
            }
        }
    });
    //-->Marca
    //Version so
    $('#VersionSO').click(function (e) {
        var disable = $(this).attr('readonly');
        var vers = $(this);
        // Check input status
        if (disable === 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {
            // If input is writable
            // Clean val
            $(this).val('');
            var idFam = $('#FamiliaSO').attr('data-id');
            var idTipo = $('#idTipo').attr('data-id');
            if (!idTipo) {
                mostrarError("Seleccione el tipo de dispositivo!!");
                return;
            }
            if (!idFam) {
                mostrarError("Seleccione la Familia del Sistema Operativo!!");
                return;
            }
            $.ajax({
                data: { 'idF': idFam, 'idTipo': idTipo },
                url: pathServer + 'VersionSO',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Show Loading
                    NProgress.done();
                    // Values
                    var raw = response;
                    var source = [];
                    var mapping = {};
                    mapping.value = -1;
                    mapping.label = "Agregar";
                    source.push(mapping);
                    for (var i = 0; i < raw.length; ++i) {
                        mapping = new Object();
                        mapping.value = raw[i].Id;
                        mapping.label = raw[i].Nombre;
                        source.push(mapping);
                    }
                    // Function
                    $(vers).autocomplete({
                        source: source,
                        minLength: 0,
                        select: function (event, ui) {
                            if (ui.item.value == -1) {
                                var input = $('<input type="text" id="osname">');
                                $('<div id="versOs">').append(input).dialog({
                                    closeOnEscape: true,
                                    modal: true,
                                    resizable: false,
                                    draggable: false,
                                    //position: ['center', 109],
                                    title: "Agregar SO",
                                    width: '400px',
                                    buttons: {
                                        Aceptar: {
                                            id: 'bTrabajos',
                                            text: 'Aceptar',
                                            click: function () {
                                                var nombreos = $(input).val();
                                                if (nombreos.length < 3) {
                                                    mostrarError("Ingrese el SO");
                                                    return;
                                                }
                                                var idfam = $('#FamiliaSO').attr('data-id');
                                                var idTipo = $('#idTipo').attr('data-id');
                                                if (!idTipo) {
                                                    mostrarError('Seleccione el tipo de Dispositivo!!!');
                                                    return;
                                                }
                                                $.ajax({
                                                    url: pathServer + 'NuevoSo',
                                                    data: { 'idF': idfam, 'nombre': nombreos, 'idTipo': idTipo },
                                                    type: 'POST',
                                                    datatype: 'json',
                                                    success: function (data) {
                                                        if (data.Info == "Login") {
                                                            mostrarFinSesion();
                                                            return;
                                                        }
                                                        if (data.Info == "ok") {
                                                            $("#versOs").dialog('destroy');
                                                            mostrarExito('SO agregado!!!');
                                                        }
                                                        else {
                                                            mostrarError(data.Info);
                                                        }
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        mostrarError(thrownError);
                                                    }
                                                });
                                            }
                                        }
                                    },
                                    Cancelar: function () {
                                        $(this).dialog('close');
                                    }

                                });
                                return false;
                            } else {
                                $(this).attr('data-id', ui.item.value);
                                $(this).val(ui.item.label);
                                return false;
                            }
                        },
                        close: function () {
                            // Finish selection and make a blur
                            $(this).blur();
                        }
                    }).autocomplete('search');
                },
                // On error
                error: function (response, textStatus, tres) {
                    mostrarError(tres);
                }
            });// Ajax
        }
    });
    //-->Version so
    $('#FechaProd').datepicker({
        dateFormat: "dd/mm/yy",
        changeYear: true,
        beforeShow: function () {
            var editable = $(this).attr('readonly');
            if (editable === "readonly") {
                return false;
            }             
        }
    });
    //Jurisdiccion
    $.ajax({
        url: '/Service/Jurisdicciones',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            var mapping = {};
            source.push(mapping);
            for (var i = 0; i < raw.length; ++i) {
                mapping = new Object();
                mapping.value = raw[i].idJurisdiccion;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            $('#Jurisdiccion').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    obtenerDependencias(ui.item.value);
                    return false;                    
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).on('click', function (e) {
                var disable = $(this).attr('readonly');                
                if (disable === 'readonly') {
                    e.preventDefault();
                } else {
                    $(this).val('');
                    $('#Ubicacion').val('');
                    $('#Jurisdiccion').autocomplete('search');
                }
            });
        }
    });
    //dropcombo
    $('span.dropCombo').on('click', function () {
        var sibInput = $(this).closest('div.table').find('input[type="text"]');
        sibInput.val('').removeAttr('data-id');
    });
    $('#AceptarCambios').click(function () {
        // Ventana de confirmacion
        $('.classMarca').addClass('hide');
        $('.classEnviar').removeClass('hide');
        $('span#works_btn').trigger(jQuery.Event("click"));
    });    
    // Altas
    $('#EquipoNuevo').click(function () {       
        newState();
    })
    //-->	
    // Print
    $('#ImprOblea').click(function (e) {
        e.preventDefault()
        var oblea = $('#Oblea').val();
        if (regularInfra.test(oblea)) window.open('/Print?id=' + oblea + '&area=Infraestructura', 'Imprimir')
        else mostrarError('Seleccione un equipo Primero');
    });
    // Print
    //-->
    // Replace jQuery UI Autcomplete scroll
    $('#body').on("CreateNiceScroll", function (event, myName) {
        $('.overflow-auto, .ui-autocomplete').niceScroll({ cursorcolor: "#24890D", zindex: "999", cursorborder: "0", horizrailenabled: "false", bouncescroll: "true" });
    })    
    //-->
    $('#SendMarca').click(function (e) {
        e.preventDefault();
        var vMarca = $('#AddMarca').val()

        // Remove error class
        $('#AddMarca').removeClass('error');

        // Check if a valid value
        if (vMarca != '' && vMarca.length > 1) {
            // Ajax
            $.ajax({
                data: { nombre: vMarca, idTipo: $('#idTipo').attr('data-id') },
                url: pathServer + 'NuevaMarca',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Hide Loading
                    NProgress.done();
                    if (response.Info == 'ok') {
                        mostrarExito("Se agregó la marca");
                        $('#AddMarca').val('');
                    } else {
                        mostrarError(response.Detail);
                    }
                },
                error: function (response, textStatus, thrownError) {
                    // Hide Loading
                    NProgress.done();
                    mostrarError(thrownError);
                }
            });
            // Ajax

            // Get the modal
            var modal = document.getElementById('myModal');
            // Hide modal
            modal.style.display = "none";
        } else {
            mostrarError('Ingrese un valor correcto');
        }
        // Check if a valid value
        //-->
    });
    $('#Critico').autocomplete({
        source: ['Bajo', 'Medio', 'Alto'],
        minLength: 0,
        select: function (event, ui) {
            $(this).attr('data-id', ui.item.value);
            $(this).val(ui.item.label).change();
            return false;
        }
    }).click(function (e) {
        var disable = $(this).attr('readonly');
        // Check input status
        if (disable === 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {
            // If input is writable
            // Clean val
            $(this).val('');
            // Trigger Search values on fucus
            $(this).autocomplete('search');
        }
    });
    $('div.bottomBut button').button().on('click', function () {
        if (window.State == 'edit'|window.State=='nuevo') {
            $(this).siblings('input').toggle();
        } else {
            mostrarError("Habilite la edición!!!");
        }
    });
    $('div.cluster').on('click','img.button.remove', function () {
        if (window.State == 'edit' | window.State == 'nuevo') {
            $(this).parents('div.left.col-3').remove();
        } else {
            mostrarError("Habilite la edición!!!");
        }
    });
    $('input.ucAComp').autocomplete({
        source: searchAutocomplete,
        position: { my: "left bottom", at: "left top", collision: "flip" },
        minLength: 2,
        select: function (event, ui) {
            //var pe = $('div.hostContainer').find('p.info');
            //if (pe) pe.remove();
            var oblea = ui.item.label;
            var id = ui.item.value;
            var inputs = $('div.hostContainer').find('input.idUc');
            var flag = false;
            $.each(inputs, function (i, v) {
                var idu = $(v).val();
                if (idu == ui.item.value) {
                    flag = true;
                }
            });
            if (flag) {
                mostrarError("Ya fue agregado!!");
                return false;
            }
            var divTo = $('div.template').clone().removeClass('hide template');
            $(divTo).find('div.ucnombre').text(oblea);
            $(divTo).find('input[type="hidden"]').val(id);
            $(divTo).appendTo('div.hostContainer');

            return false;
        },
        focus: function () {
            return false;
        }
    }).on('keyup', function () {
        $(this).val(this.value.toUpperCase())
    });
    initialState();
    Modal();
})
function initialState() {
    window.State = 'initial';
    $('#CancelarCambios').hide();
    $('#ImprOblea').hide();
    $('#editData').hide();
    $('#AceptarCambios').hide();
    $('#EquipoNuevo').show();
    $('#LineaBase').hide();
    $('#Estado').removeAttr('disabled').attr('placeholder', 'Campo Obligatorio').attr('readonly', 'readonly');
    //setFieldsDisabled();
    clearFields();
    OcultarRelaciones();
    OcultarHost();
}
function editState() {
    window.State = 'edit';
    $('#AceptarCambios').show();
    $('#CancelarCambios').show();
    $('#ImprOblea').hide();
    $('#EquipoNuevo').hide();
    $('#editData').hide();
    setFieldsEditable();
    OcultarRelaciones();
}   
function newState() {    
    window.State = 'nuevo';    
    $('#editData').hide();
    $('#EquipoNuevo').hide();
    $('#CancelarCambios').show();
    $('#LineaBase').hide();
    $('#AceptarCambios').show();
    $('#ImprOblea').hide();
    $('#Estado').attr('disabled', 'disabled').removeAttr('placeholder');
    OcultarHost();
    clearFields();
    setFieldsEditable();
    OcultarRelaciones();
   
}
function showingState() {
    window.State = 'showing';
    $('#ImprOblea').show();
    $('#editData').show();
    $('#AceptarCambios').hide();
    $('#LineaBase').show();
    clearFields();
    //setFieldsDisabled();
    OcultarRelaciones();
}
function setFieldsEditable() {
    var inputName;    
    $('input, textarea', '#content-box').each(function () {
        inputName = $(this).attr('name');
        if (NoEditable.test(inputName)) {            
            $(this).removeAttr('readonly', 'readonly');            
        }
    });
    $('#content-box .button').each(function () {
        $(this).removeAttr('readonly');
    });
    $('span.dropCombo,span.dropStorage').show();
}
//function setFieldsDisabled() {
//    var inputName;
//    $('input, textarea', '#content-box').each(function () {
//        inputName = $(this).attr('name');
//        if (NoEditable.test(inputName)) {
//            $(this).val('');
//            $(this).attr('readonly', 'readonly');
//            $(this).removeClass('error');
//        }
//    });
//    $('span.dropCombo').hide();
//}
function OcultarHost() {
    var div= $('div.hostContainer').find('div.left').filter(':visible');
    div.remove();
    $('div.cluster').hide();
}
function clearFields() {
    var inputName;
    $('input, textarea', '#content-box').each(function () {
            $(this).val('');
            $(this).attr('readonly', 'readonly');
            $(this).removeAttr('data-id');
            $(this).removeClass('error');
            inputName = $(this).attr('name');
            if (inputName == 'idTipo') $(this).change();
    });
    $('div.storageFields div.row').remove();
    $('div.ipFields div.row').remove();
    $('span.dropCombo').hide();
    $('span.dropStorage').hide();
}
function EdicionDeshabilitadaNotif() {
    mostrarError('Edición deshabilitada');
}
function Modal() {
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = $("span#works_btn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0]; // Only for first close class


    // Get the <send> element that closes the modal
    var send = document.getElementById("SendForm"); // Only for first id	
    send.onclick = function () {
        modal.style.display = "none";
        AceptarCambios();
    }

    // When the user clicks the button, open the modal
    btn.each(function () {
        $(this).on("click", function () {
            modal.style.display = "block";
        });
    });

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks on <span> (x), close the modal
    /*send.onclick = function() {
		modal.style.display = "none";
	}*/

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}
function AceptarCambios() {     
    // Validations
    if ($('#Oblea').val()) {
        var Val = $('#Oblea').val();
        if (regularInfra.test(Val)) {
            var resOblea = 'ok';
            $('#Oblea').removeClass('error');
        } else {
            $('#Oblea').addClass('error');
        }
    } else {
        $('#Oblea').addClass('error');
    }

    if ($('#idTipo').val()) {
        var resTipo = 'ok';
        $('#idTipo').removeClass('error');
    }
    else {
        $('#idTipo').addClass('error');
    }

    if ($('#Marca').val()) {
        var resMarca = 'ok';
        $('#Marca').removeClass('error');
    }
    else {
        $('#Marca').addClass('error');
    }

    if ($('#Jurisdiccion').val()) {
        var resJurisdiccion = 'ok';
        $('#Jurisdiccion').removeClass('error');
    }
    else {
        $('#Jurisdiccion').addClass('error');
    }

    if ($('#Ubicacion').val()) {
        var resUbicacion = 'ok';
        $('#Ubicacion').removeClass('error');
    }
    else {
        $('#Ubicacion').addClass('error');
    }
    // Validations
    //-->

    if ((resOblea && resTipo && resMarca && resJurisdiccion && resUbicacion) === 'ok') {
        var Equipo = new Object();
        var nombrecomun = $('#NComun').val();
        // equipo
        var idEquipo = $('#idEquipo').val();
        if (idEquipo != '') {
            Equipo.idEquipo = $('#idEquipo').val()
        }
        Equipo.NombreComun = nombrecomun;
        Equipo.Dependencia = {};
        Equipo.Jurisdiccion = {};
        Equipo.TipoEquipo = {};
        Equipo.Estado = {};
        Equipo.Estado.idEstado = $('#Estado').attr('data-id');
        Equipo.Oblea = $('#Oblea').val()
        Equipo.NroSerie = $('#NroSerie').val()
        Equipo.Dependencia.Jurisdiccion = {};
        Equipo.Dependencia.Jurisdiccion.idJurisdiccion = $('#Jurisdiccion').attr('data-Id');
        Equipo.Dependencia.idDependencia = $('#Ubicacion').attr('data-Id');
        Equipo.Dependencia.Nombre = $('#Ubicacion').val();
        Equipo.TipoEquipo = {};
        Equipo.TipoEquipo.id = $('#idTipo').attr('data-Id');
        Equipo.Marca = {};
        Equipo.Marca.idMarca = $('#Marca').attr('data-Id');
        Equipo.Marca.Nombre = $('#Marca').val();
        Equipo.Modelo = $('#Modelo').val();
        Equipo.Observaciones = $('#Observaciones').val();        
        var tipoEquipo = $('#idTipo').val();
        switch (tipoEquipo) {
            case 'Storage':
                if (window.State == 'nuevo') {
                    Equipo.Url = pathServer + 'NuevoStorage';
                }
                if (window.State == 'edit') {
                    Equipo.Url = pathServer + 'UpdateStorage';
                }
                break;
            case 'Cluster':
                if (window.State == 'nuevo') {
                    Equipo.Url = pathServer + 'NuevoCluster';
                }
                if (window.State == 'edit') {
                    Equipo.Url = pathServer + 'UpdateCluster';
                }
                var inputs = $('div.hostContainer').find('input.idUc');                
                var flag = false;                
                Equipo.Hosts = [];
                $.each(inputs, function (i, v) {
                    var host = {};
                    var id = $(v).val();
                    if (id != 0) {
                        host.Id = id;
                        host.Nombre = "";
                        Equipo.Hosts.push(host);
                    }
                });
                if (Equipo.Hosts.length == 0) {
                    mostrarError("Complete los host que componen el cluster!!");
                    return;
                }
                break;
            case 'Libreria':;
                break;
            default:
                if (window.State == 'nuevo') {                    
                    Equipo.Url = pathServer + 'NuevoServer';
                }
                if (window.State == 'edit') {                 
                    Equipo.Url = pathServer + 'UpdateServer';
                }
                Equipo.Cpu = $('#CPU').val();
                Equipo.CantidadCpu = $('#CantidadCPU').val();
                Equipo.Memoria = {};
                Equipo.Memoria.Capacidad = $('#Memoria').val();
                Equipo.Memoria.Um = {};
                if ($('#UMMemoria').attr('data-id')) Equipo.Memoria.Um.Id = $('#UMMemoria').attr('data-id');
                else Equipo.Memoria.Um.Id = 0;
                Equipo.Criticidad = $('#Critico').val();
                //disco viejo
                Equipo.Disco = {};
                Equipo.Disco.Um = {};
                Equipo.Criticidad = $('#Critico').val();
                Equipo.Disco.Capacidad = $('#CapAlmacenamiento').val();
                Equipo.DataStore = $('#DataStore').val();
                if ($('#UMDiscos').attr('data-id')) {
                    Equipo.Disco.Um.Id = $('#UMDiscos').attr('data-id');
                }
                else Equipo.Disco.Um.Id = 0;
                Equipo.Disco.Cantidad = $('#CantidadDiscos').val();
                //disco viejo
                Equipo.Storage = new Array();                
                var storages = $('.storageFields input[type="text"]');
                $.each(storages, function (i, v) {
                    var stor = {};
                    stor.LetraUnidad = $(v).data('letra');
                    stor.datastore = $(v).data('store');
                    stor.Capacidad = $(v).data('size');
                    stor.Um = {};
                    stor.Um.Id = $(v).data('id');
                    Equipo.Storage.push(stor);                    
                });
                var ips = $('.ipFields input[type=text]');
                Equipo.Ips = new Array();
                $.each(ips, function (i, v) {
                    var ips = {};
                    ips.IpV4 = $(v).data('ip');
                    Equipo.Ips.push(ips);
                });
                Equipo.Os = {};
                if ($('#VersionSO').attr('data-id')) {
                    Equipo.Os.Id = $('#VersionSO').attr('data-id');
                }
                else Equipo.Os.Id = 0;               
                Equipo.LicenciaOs = $('#LicenciaSO').val();
                Equipo.PuestaProduccion = $('#FechaProd').val();
                Equipo.Ip = {};
                Equipo.Ip.IpV4 = $('#IPv4').val();
                Equipo.Rack = $('#NRack').val();
                Equipo.UnidadesRack = $('#URack').val();                
                break;
           
           
        }   // Send data
        // Ajax
        $.ajax({
            data: JSON.stringify(Equipo),
            url: Equipo.Url,
            contentType: 'application/json; charset=utf-8',
            datatype:'json',
            type: 'post',
            beforeSend: function () {
                // Show Loading
                NProgress.start();
            },
            success: function (response) {
                // Hide Loading
                NProgress.done();
                // create the notification
               if(response.Info=="ok"){
                   mostrarExito(response.Detail);
                   initialState();
                } else {
                    mostrarError(response.Detail);
                }
            },
            error: function (response, textStatus) {
                // Hide Loading
                NProgress.done();
                mostrarError(response.Detail);
            }
        });
        // Ajax		    
    } else {
        // Show Acept buttom
        //$('#editData').trigger('click');
        var message = '<p>Complete los campos obligratorios o<br>revise que los datos ingresados sean correctos.</p>';
        mostrarError(message);
    }
}
function AgregarNuevaMarca(add, thisElem) {
    $('.classEnviar').each(function () {
        $(this).addClass('hide');
    })
    $('.classMarca').each(function () {
        $(this).removeClass('hide');
    })
    $('span#works_btn').trigger(jQuery.Event("click"));
}
function obtenerDependencias(idJur)
{
    $.ajax({
        data: { 'idJur': idJur },
        url: '/Service/Dependencias',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Show Loading
            NProgress.done();
            var raw = response;
            var source = [];
            var mapping = {};
            source.push(mapping);
            for (var i = 0; i < raw.length; ++i) {
                mapping = new Object();
                mapping.value = raw[i].idDependencia;
                mapping.label = raw[i].Nombre;
                source.push(mapping);
            }
            $('#Ubicacion').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    $(this).attr('data-id', ui.item.value);
                    $(this).val(ui.item.label);
                    return false;
                   
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).on('click', function (e) {
                var disable = $(this).attr('readonly');
                if (disable === 'readonly') {
                    e.preventDefault();
                } else {
                    $(this).val('');
                    $('#Ubicacion').autocomplete('search');
                }
            });
        }
    });
}
function searchAutocomplete(request, response) {
    var texto = request.term;    
    texto = texto.toUpperCase();
    var flagOblea, flagIp, flagNc,flagSerie = false;
    var uri;
    if (regularInfraStart.test(texto)) {
        flagOblea = true;
    }    
    var buscoRed = request.term.split('.');
    if (buscoRed.length > 1) {
        for (var i = 0; i < buscoRed.length; i++) {
            var num = Number(buscoRed[i]);
            if (num < 255 & (num > 0 | num==0)) flagIp = true;
            else {
                flagIp = false;
                break;
            }
        }
    }    
    if (flagIp) {
        uri = 'Inventario/EquipoXIp';
    }
    else {
        if (flagOblea) uri = 'Inventario/EquipoXOblea';
        else {
            flagNc = true;
            uri = 'Inventario/EquipoXNComun';
        }
    }
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                if (data.length == 0) {
                    flagSerie = true;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: 'Inventario/EquipoXSerie',
                        data: term,
                        async:false,
                        dataType: "json",
                        success: function (data) {
                            $.each(data, function (i, val) {
                                var obj = {};
                                obj.value = val.idEquipo;                                
                                obj.label = val.NroSerie;
                                obj.oblea = val.Oblea;
                                obj.serie = val.NroSerie;
                                suggestions.push(obj);
                            })
                        }
                    });
                 }else {                    
                    $.each(data, function (i, val) {
                        var obj = {};
                        obj.value = val.idEquipo;
                        if (flagOblea) obj.label = val.Oblea;
                        if (flagIp) obj.label = val.Ip4;
                        if (flagNc) obj.label = val.NombreComun;
                        if (flagSerie) obj.label = val.NroSerie;
                        obj.oblea = val.Oblea;
                        obj.serie = val.NroSerie;
                        suggestions.push(obj);
                    })
                }
                response(suggestions);
            }
        }
    });
}
function GetEquipo(oblea) {
    initialState();
    $.ajax({
        data: { 'oblea': oblea },
        url: pathServer+'GetEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            NProgress.start();
        },
        success: function (response) {
            // Hide Loading
            NProgress.done();
            if (response.Info == 'Login') {
                mostrarFinSesion();
            } else {
                if (response.Info) {
                    mostrarError("No se encontró el equipo!!");
                }
                else {
                    showingState();
                    CompletarBasicInfo(response)
                }
            }
        },
        error: function (response, textStatus) {
            // Hide Loading
            NProgress.done();
            mostrarError(response.Detail);
        }
    });
}
function CompletarBasicInfo(response) {
    $('#idEquipo').val(response.idEquipo);
    $('#Oblea').val(response.Oblea);
    if (response.NroSerie !== null) {
        $('#NroSerie').val(response.NroSerie);
    }
    $('#idTipo').val(response.TipoEquipo.Nombre).attr('data-id', response.TipoEquipo.id).change();
    if (response.Marca !== null) {
        $('#Marca').val(response.Marca.Nombre).attr('data-id', response.Marca.idMarca);
    }
    $('#Modelo').val(response['Modelo'])
    if (response.Jurisdiccion !== null) {
        $('#Jurisdiccion').val(response.Jurisdiccion.Nombre).attr('data-id', response.Jurisdiccion.idJurisdiccion);
    }
    if (response.Dependencia !== null) {
        $('#Ubicacion').val(response.Dependencia.Nombre).attr('data-id', response.Dependencia.idDependencia);
    }
    if (response.Estado!== null) {
        $('#Estado').val(response.Estado.Nombre).attr('data-id', response.Estado.idEstado);
    }
    $('#Observaciones').val(response.Observaciones);
    switch (response.TipoEquipo.Nombre) {
        case "Servidor Virtual":CompletarInfoServer(response);
            break;
        case "Servidor Físico": CompletarInfoServer(response);
            break;
        case "Workstation Virtual": CompletarInfoServer(response);
            break;
        case "Cluster": CompletarInfoCluster(response);
            break;
    }
}
function CompletarInfoStorage(response)
{
    $.each(response.volumenes)
    {

    }
}
function CompletarInfoServer(response)
{
    $('#CPU').val(response.Cpu);
    $('#CantidadCPU').val(response.CantidadCpu);
    if (response.Memoria != null) {
        $('#Memoria').val(response.Memoria.Capacidad);
        $('#UMMemoria').val(response.Memoria.Um.Nombre).attr('data-id',response.Memoria.Um.Id);
    }
    $('#NComun').val(response.NombreComun);
    if (response.Disco != null) {
        $('#CapAlmacenamiento').val(response.Disco.Capacidad);
        $('#UMDiscos').val(response.Disco.Um.Nombre).attr('data-id',response.Disco.Um.Id);
        if(response.Disco.Cantidad!=0)
        $('#CantidadDiscos').val(response.Disco.Cantidad);
    }
    if (response.Os != null) {
        $('#FamiliaSO').val(response.Os.FamiliaOs.Nombre).attr('data-id',response.Os.FamiliaOs.Id);        
        $('#VersionSO').val(response.Os.Nombre).attr('data-id',response.Os.Id);

    }
    $('#DataStore').val(response.DataStore);
    $('#LicenciaSO').val(response.LicenciaOs);
    $('#FechaProd').val(response.ppstring);
    if (response.Ip != null) {
        $('#IPv4').val(response.Ip.IpV4);
    }
    $('#Critico').val(response.Criticidad);
    $('#URack').val(response.UnidadesRack);
    //storage
    $.each(response.Storage, function (i, v) {
        agregarStorage(v.LetraUnidad, v.DataStore, v.Capacidad, v.Um.Id, v.Um.Nombre,false);
    });
    $.each(response.Ips, function (i, v) {
        agregarIp(v.Id, v.IpV4,false)
    })
}
function agregarIp(id,ip,edit) {
    var container = $('div.ipFields');
    var newtable = $('<div>').addClass('table');
    var newrow = $('<div>').addClass('row');
    var newcell = $('<div>').addClass('cell').css('position', 'relative');
    var input = $('<input>').attr({ type: 'text', class: 'inputIp' });

    if (!container) {
        alert("no encontrado container");
    } else {
        var dropSt = $('<span>').addClass('ui-icon ui-icon-trash dropStorage');
        if (!edit) dropSt.css('display', 'none');
        var texto = ip;

        input.attr({ 'data-ip': ip, 'data-id': id });
        input.val(ip);
        newcell.append(input);
        newcell.append(dropSt);
        newrow.append(newcell);
        container.append(newrow);
    }
}
function agregarStorage(unitL, dataSt, size, um_id, um, edit) {
    var container = $('div.storageFields');
    var newtable = $('<div>').addClass('table');
    var newrow = $('<div>').addClass('row');
    var newcell = $('<div>').addClass('cell').css('position', 'relative');
    var input = $('<input>').attr({ type: 'text', class: 'inputSt' });

    if (!container) {
        alert("no encontrado container");
    } else {
        var dropSt = $('<span>').addClass('ui-icon ui-icon-trash dropStorage');
        if (!edit) dropSt.css('display', 'none');
        var texto = unitL;
        texto += '  ';
        texto += dataSt;
        texto += '  ';
        texto += size;
        texto += um;
        input.attr({ 'data-letra': unitL, 'data-store': dataSt, 'data-size': size, 'data-um': um, 'data-id': um_id });
        input.val(texto);
        newcell.append(input);
        newcell.append(dropSt);
        newrow.append(newcell);
        container.append(newrow);
    }
}
function CompletarInfoCluster(response)
{
    $.each(response.Hosts, function (i, v) {
        var oblea = v.Nombre;
        var id = v.Id;
        var inputs = $('div.hostContainer').find('input.idUc');        
        var divTo = $('div.template').clone().removeClass('hide template');
        $(divTo).find('div.ucnombre').text(oblea);
        $(divTo).find('input[type="hidden"]').val(id);
        $(divTo).appendTo('div.hostContainer');
    });
}
function showingServer() {
    $("div.server").show();
    $('div.cluster').hide();
}
function showingCluster() {
    $("div.server").hide();
    $('div.cluster').show();
}
function hidingDetails()
{
    $("div.server").hide();
    $('div.cluster').hide();
}