﻿function bindUsuarios()
{
    $('button').button();
}
function bindDatosPers()
{

}
function deleteEquipo(e)
{
    e.preventDefault();
    var oblea = $.trim($('#tOblea').val());
    var este = $('#removeEquipo');
    if (regularInfra.test(oblea.toUpperCase())) {
        $.ajax({
            url: 'Config/borrarEquipo',
            type: 'POST',
            data: 'oblea=' + oblea,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Equipo Borrado');
                    oblea.val('');
                }
                else {
                    mostrarError(data.Detail)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    else {
        mostrarError("El formato de la oblea no es válido");
    }
}
function actualizarTiempos()
{

}