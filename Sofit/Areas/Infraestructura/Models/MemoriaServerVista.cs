﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Infraestructura.Models
{
    public class MemoriaServerVista
    {
        public string Capacidad { get; set; }
        public UnidadMedidaVista Um { get; set; }
    }
}