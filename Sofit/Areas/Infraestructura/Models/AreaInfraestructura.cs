﻿using Sofit.DataAccess;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Linq;

namespace soporte.Areas.Infraestructura.Models
{
    public class AreaInfraestructura:IAreaIncidencia
    {
        public AreaInfraestructura(int idArea, string nombre)
        {
            base.IdArea = idArea;
            base.NombreArea = nombre;
        }
        public override int getIdTecnicoGenerico()
        {
            int idTecnico = 0;
            using (var dbc = new IncidenciasEntities())
            {
                idTecnico = (from u in dbc.tecnico
                             where u.descripcion == "Infraestructura"
                             select u.id_tecnico).First();
            }
            return idTecnico;
        }

        public override soporte.Models.Statics.Responses derivar(int idArea, int idTecnico, int idUsuario)
        {
            Responses result = new Responses();
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idArea);
            result = Incidencia.cambioArea(areaDestino, idTecnico, idUsuario);
            return result; 
        }

        public override soporte.Models.Statics.Responses solucionar(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo, DateTime fechaActualizar, string imagen)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    //string tipoResolución = (dbc.tipo_resolucion.Where(c => c.id_tipo_resolucion == tResolucionUsuarios).First()).descripcion;
                    int idAreaEntrada = (dbc.area.Where(c => c.descripcion == "Infraestructura").First()).id_area;
                    Resoluciones res = new Resoluciones
                    {
                        id_uc = idEquipo == 0 ? null : idEquipo,
                        fecha = DateTime.Now,
                        id_incidencia = this.Incidencia.Id,
                        id_tecnico = tecnico,
                        id_tipo_resolucion = tResolucionUsuarios,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_area = idAreaEntrada,
                        path_imagen = (imagen != string.Empty) ? imagen : null
                    };
                    dbc.Resoluciones.Add(res);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return result;
        }
    }
}