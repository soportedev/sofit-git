﻿using Sofit.DataAccess;
using soporte.Models.Statics;
using System.Linq;

namespace soporte.Areas.Infraestructura.Models
{
    public class TiemposInfraestructura
    {        
        public int tCriticoIncidente { get; set; }

        public TiemposInfraestructura()
        {
        using (var dbc = new IncidenciasEntities())
            {                
                var ts = from to in dbc.TiemposGral
                         select to;
                if (ts != null)
                {                    
                    tCriticoIncidente = ts.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                }
            }
        }
        internal soporte.Models.Statics.Responses setTiempos(int tAIng, int tEIng, int tAAdm, int tEAdm, int tAClaro, int tEClaro, int tAEpec, int tEEpec, int tATelecom, int tETelecom, int tASes, int tESes)
        {
            Responses result = new Responses();
            
            return result;
        }    
    }
}