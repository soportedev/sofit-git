﻿using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Infraestructura.Models
{
    public class PerfilOperadorInfra:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            switch (page)
            {
                case "Infraestructura/Inventario": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Infraestructura/Historicos": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Infraestructura/Incidentes": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Infraestructura/Config": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Infraestructura/NuevoTicket": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                default: return soporte.Models.Statics.Constantes.AccesosPagina.NoAccess;
            }
        }

        public override string getHomePage()
        {
            return "Infraestructura/Incidentes";
        }
    }
}