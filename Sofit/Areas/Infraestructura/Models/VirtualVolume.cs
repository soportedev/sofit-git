﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Infraestructura.Models
{
    public class VirtualVolume
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Capacidad { get; set; }
        public string NombreDataStore { get; set; }
    }
}