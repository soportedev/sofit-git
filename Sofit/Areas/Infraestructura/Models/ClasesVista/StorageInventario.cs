﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Infraestructura.Models.ClasesVista
{
    public class StorageInventario:IEquipoInventario
    {
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }
        public MarcaVista Marca { get; set; }
        public string Modelo { get; set; }
        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();            
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();           
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var stor = dbc.Storage.Where(c => c.id_uc == id).Single();
                DependenciaVista dep = new DependenciaVista
                {
                    idDependencia = stor.id_dependencia,
                    Nombre = stor.Dependencia.descripcion,
                    Jurisdiccion = new JurisdiccionVista
                    {
                        idJurisdiccion = stor.Dependencia.Jurisdiccion.id_jurisdiccion,
                        Nombre = stor.Dependencia.Jurisdiccion.descripcion
                    }
                };
                MarcaVista marca = new MarcaVista
                {
                    idMarca=stor.Marca.id_marca,
                    Nombre=stor.Marca.descripcion
                };
                this.Marca = marca;
                this.Dependencia = dep;
                this.Modelo = stor.modelo;
                EstadoEquipoVista est = new EstadoEquipoVista
                {
                    idEstado = stor.Estado_UC.id_estado,
                    Nombre = stor.Estado_UC.descripcion
                };
                this.Estado = est;
            }
        }
    }
}