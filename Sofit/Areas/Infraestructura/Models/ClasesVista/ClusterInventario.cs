﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Infraestructura.Models.ClasesVista
{
    public class ClusterInventario : IEquipoInventario
    {
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }
        public MarcaVista Marca { get; set; }
        public string Modelo { get; set; }
        public List<HostCluster> Hosts { get; set; }
        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                var clus = dbc.Cluster.Where(c => c.id_uc == id).Single();
                DependenciaVista dep = new DependenciaVista
                {
                    idDependencia = clus.id_dependencia,
                    Nombre = clus.Dependencia.descripcion,
                    Jurisdiccion = new JurisdiccionVista
                    {
                        idJurisdiccion = clus.Dependencia.Jurisdiccion.id_jurisdiccion,
                        Nombre = clus.Dependencia.Jurisdiccion.descripcion
                    }
                };
                this.Marca = new MarcaVista
                {
                    idMarca = 0,
                    Nombre = "N/A"
                };
                this.Dependencia = dep;
                EstadoEquipoVista est = new EstadoEquipoVista
                {
                    idEstado = clus.Estado_UC.id_estado,
                    Nombre = clus.Estado_UC.descripcion
                };
                Hosts = clus.Servidor.Select(c => new HostCluster
                {
                    Id = c.id_uc,
                    Nombre = c.UnidadConf.nombre
                }).ToList();
                this.Estado = est;
            }
        }
        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();            
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();          
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            if(Hosts.Count>0)
            {
                sb.Append("Hosts:").AppendLine();
            }
            foreach (var v in Hosts)
            {
                sb.Append("Oblea:").Append(v.Nombre).AppendLine();  
            }
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}