﻿using soporte.Areas.Soporte.Models.Displays;
using System;
using System.Linq;
using System.Web;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System.Text;
using soporte.Models;
using Sofit.DataAccess;
using System.Collections.Generic;
using Sofit.Areas.Infraestructura.Models.ClasesVista;

namespace soporte.Areas.Infraestructura.Models.ClasesVista
{
    public class ServerInventario : IEquipoInventario
    {
        public string NombreComun { get; set; }
        public DominioVista Dominio { get; set; }
        public MarcaVista Marca { get; set; }
        public string Modelo { get; set; }
        public MemoriaServerVista Memoria { get; set; }
        public List<StorageServerVista> Storage { get; set; }
        public List<IpVista> Ips { get; set; }
        public string Cpu { get; set; }
        public string CantidadCpu { get; set; }        
        public DateTime? PuestaProduccion { get; set; }
        public DiscoServerVista Disco { get; set; }
        public string DataStore { get; set; }
        public IpVista Ip { get; set; }
        public string ppstring {
            get
            {
                return PuestaProduccion.HasValue ? PuestaProduccion.Value.ToShortDateString() : "";
            }
            set { }
        }
        public int UnidadesRack { get; set; }
        public string Rack { get; set; }
        public string LicenciaOs { get; set; }
        public OsVista Os { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public EstadoEquipoVista Estado { get; set; }
        public string Criticidad { get; set; }
        
        public override void construir(int id)
        {
            base.construir(id);
            using (var dbc = new IncidenciasEntities())
            {
                Servidor server = dbc.Servidor.Where(c => c.id_uc == idEquipo).Single();
                if (server.id_dominio.HasValue)
                {
                    DominioVista dom = new DominioVista
                    {
                        Id = server.Dominios.id_dominio,
                        Nombre = server.Dominios.nombre
                    };
                }
                MarcaVista marca = new MarcaVista
                {
                    idMarca = server.Marca.id_marca,
                    Nombre = server.Marca.descripcion
                };
                this.NombreComun = server.nombreComun;
                this.Marca = marca;
                this.Modelo = server.modelo;
                
                if (server.UnidadMedida != null)
                {
                    MemoriaServerVista memo = new MemoriaServerVista
                    {
                        Capacidad = server.memoria,
                        Um = new UnidadMedidaVista
                        {
                            Id = server.UnidadMedida.id_um,
                            Nombre = server.UnidadMedida.descripcion
                        }
                    };
                    this.Memoria = memo;
                }
                //disco viejo
                if (server.UnidadMedida1 != null)
                {
                    DiscoServerVista disco = new DiscoServerVista
                    {
                        Capacidad = server.disco,
                        Cantidad=server.cantidaddiscos.Value.ToString(),
                        Um = new UnidadMedidaVista
                        {
                            Id = server.UnidadMedida1.id_um,
                            Nombre = server.UnidadMedida1.descripcion
                        }
                    };
                    this.Disco = disco;
                }
                this.DataStore = server.datastore;
                //nuevo storage
                if (server.ServerStorage != null)
                {
                    this.Storage = server.ServerStorage.Select(c => new StorageServerVista {
                        Capacidad = c.capacidad,
                        DataStore = c.datastore,
                        LetraUnidad = c.letra,
                        Um = new UnidadMedidaVista { Nombre = c.UnidadMedida.descripcion, Id=c.UnidadMedida.id_um }
                    }).ToList();
                }
                this.Cpu = server.cpu;
                if(server.cantidadcpu!=null)
                this.CantidadCpu = server.cantidadcpu;
                //nuevo ip
                if (server.UnidadConf.Ip != null)
                {
                    this.Ips = new List<IpVista>();
                    this.Ips = server.UnidadConf.Ip.Select(c => new IpVista {Id=c.id_ip, IpV4 = c.ip_v4 }).ToList();
                }
                //viejo ip
                if (server.ip.HasValue)
                {
                    this.Ips.Add(new IpVista
                    {
                        Id = server.Ip1.id_ip,
                        IpV4 = server.Ip1.ip_v4,
                        IpV6 = server.Ip1.ip_v6,
                        Dns = server.Ip1.dns,
                        Gateway = server.Ip1.gateway,
                        Mascara = server.Ip1.mascara,
                        Wins = server.Ip1.wins
                    });

                }

                this.PuestaProduccion = server.puesta_produccion;
                this.Criticidad = server.criticidad;
                
                this.LicenciaOs = server.licenciaSO;
                if (server.Os1 != null)
                {
                    this.Os = new OsVista
                    {
                        Id = server.Os1.id_os,
                        Nombre = server.Os1.descripcion,
                        FamiliaOs = new FamiliaOsVista
                        {
                            Id = server.Os1.FamiliaSo.id_fso,
                            Nombre = server.Os1.FamiliaSo.nombre
                        }
                    };
                }
                this.Dependencia = new DependenciaVista
                {
                    idDependencia = server.Dependencia.id_dependencia,
                    Nombre = server.Dependencia.descripcion,
                    Jurisdiccion = new JurisdiccionVista
                    {
                        idJurisdiccion = server.Dependencia.Jurisdiccion.id_jurisdiccion,
                        Nombre = server.Dependencia.Jurisdiccion.descripcion
                    }
                };
                this.Estado = new EstadoEquipoVista
                {
                    idEstado = server.Estado_UC.id_estado,
                    Nombre = server.Estado_UC.descripcion
                };
            }
        }

        internal override soporte.Models.Statics.Responses SaveLineaBase()
        {            
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();            
            sb.Append("Os:").Append(this.Os!=null?this.Os.Nombre:"N/A").AppendLine();           
            sb.Append("CPU: ").Append(this.Cpu!=null?this.Cpu:"N/A").AppendLine();
            sb.Append("Cantidad Cpu:").Append(this.CantidadCpu!=null?this.CantidadCpu:"N/A").AppendLine();
            sb.Append("Ram:").Append(this.Memoria != null ? this.Memoria.Capacidad + " " + this.Memoria.Um.Nombre : "N/A").AppendLine();
            usuarioBean usuario = HttpContext.Current.Session["usuario"] as usuarioBean;
            using (var dbc = new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
    }
}