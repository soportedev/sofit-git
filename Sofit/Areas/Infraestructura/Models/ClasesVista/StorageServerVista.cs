﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sofit.Areas.Infraestructura.Models.ClasesVista
{
    public class StorageServerVista
    {
        public string LetraUnidad { get; set; }
        public string DataStore { get; set; }
        public string Capacidad { get; set; }
        public UnidadMedidaVista Um { get; set; }
    }
}