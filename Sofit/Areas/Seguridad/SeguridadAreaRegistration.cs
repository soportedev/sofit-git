﻿using System.Web.Mvc;

namespace soporte.Areas.Seguridad
{
    public class SeguridadAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Seguridad";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {           
            context.MapRoute(
                name: "lineabaseseguridad",
                url: "Seguridad/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiosegu",
                url: "Seguridad/Cambios/{startIndex}",
                defaults: new { controller = "Cambios", action = "Index", startIndex=0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8seguridad",
                url: "Seguridad/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Seguridad_default",
                "Seguridad/{controller}/{action}/{id}",
                new { controller="Seguridad/Incidentes", action = "Index", id = UrlParameter.Optional },
                new string[] {"soporte.Areas.Seguridad.Controllers"}
            );
            
        }
    }
}
