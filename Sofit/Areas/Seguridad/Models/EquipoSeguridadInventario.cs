﻿using soporte.Areas.Infraestructura.Models;
using soporte.Areas.Soporte.Models.Displays;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace soporte.Areas.Seguridad.Models
{
    public class EquipoSeguridadInventario:IEquipoInventario
    {
        public EquipoSeguridadInventario()
        {
            Marca = new MarcaVista();
            Estado = new EstadoEquipoVista();
            Dependencia = new DependenciaVista();
            IP = new IpVista();
            TipoEquipo = new TiposEquipoVista();
        }
        public EquipoComplete Server { get; set; }
        public string Modelo { get; set; }        
        public int? CantidadCpu { get; set; }        
        public string NComun { get; set; }
        public string Cpu { get; set; }
        public MemoriaServerVista Memoria { get; set; }        
        public OsVista Os { get; set; }
        public string Criticidad { get; set; }
        public MarcaVista Marca { get; set; }        
        public EstadoEquipoVista Estado { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public DateTime ? LicenciaOs { get; set; }
        public string VLicenciaStr
        {
            get
            {
                if (LicenciaOs.HasValue) return LicenciaOs.Value.ToShortDateString(); else return "S/D";
            }
            set { }
        }
        public DateTime? PuestaProduccion { get; set; }
        public string PuestaProduccionStr
        {
            get
            {
                if (PuestaProduccion.HasValue) return PuestaProduccion.Value.ToShortDateString(); else return "S/D";
            }
            set { }
        }
        public IpVista IP { get; set; }
        internal override Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();           
            
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();
            sb.Append("Nombre Común:").Append(this.NComun).AppendLine();
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            if (TipoEquipo.Nombre == "Balanceador de carga"|TipoEquipo.Nombre =="Firewall"|TipoEquipo.Nombre =="Dispositivo de Gestion FW")
            {
                sb.Append("Os:").Append(this.Os != null ? this.Os.Nombre : "N/A").AppendLine();
                sb.Append("CPU: ").Append(this.Cpu != null ? this.Cpu : "N/A").AppendLine();                
                sb.Append("Ram:").Append(this.Memoria != null ? this.Memoria.Capacidad + " " + this.Memoria.Um.Nombre : "N/A").AppendLine();
                sb.Append("IP:").Append(this.IP!=null?this.IP.IpV4:"N/A").AppendLine();
                sb.Append("Criticidad:").Append(this.Criticidad != null ? this.Criticidad : "N/A").AppendLine();
            }
                       
            usuarioBean usuario=HttpContext.Current.Session["usuario"]as usuarioBean;
            using(var dbc=new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
        public override void construir(int id)
        {
            base.construir(id);           
            try
            {
                using(var dbc=new IncidenciasEntities())
                {
                    var eqBase = (from u in dbc.EquipoSeguridad
                              where u.UnidadConf.id_uc == id
                              select u).First();
                    this.Dependencia = new DependenciaVista
                    {
                        idDependencia = eqBase.id_dependencia,
                        Nombre = eqBase.Dependencia.descripcion,
                        Jurisdiccion = new JurisdiccionVista
                        {
                            idJurisdiccion = eqBase.Dependencia.Jurisdiccion.id_jurisdiccion,
                            Nombre = eqBase.Dependencia.Jurisdiccion.descripcion
                        }
                    };
                    this.NComun = eqBase.nombre_comun;
                    this.Cpu = eqBase.cpu;
                    this.Estado = new EstadoEquipoVista
                    {
                        idEstado = eqBase.Estado_UC.id_estado,
                        Nombre = eqBase.Estado_UC.descripcion
                    };
                    this.idEquipo = eqBase.UnidadConf.id_uc;
                    this.TipoEquipo = new TiposEquipoVista
                    {
                        id = eqBase.UnidadConf.id_tipo,
                        Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion
                    };
                    if (eqBase.UnidadMedida != null)
                    {
                        MemoriaServerVista memo = new MemoriaServerVista
                        {
                            Capacidad = eqBase.memoria,
                            Um = new UnidadMedidaVista
                            {
                                Id = eqBase.UnidadMedida.id_um,
                                Nombre = eqBase.UnidadMedida.descripcion
                            }
                        };
                        this.Memoria = memo;
                    }
                    if(eqBase.Servidor!=null)
                    {
                        this.Server = new EquipoComplete
                        {
                            idEquipo = eqBase.Servidor.id_servidor,
                            Oblea = eqBase.Servidor.UnidadConf.nombre
                        };
                    }
                    if (eqBase.Ip1 != null)
                    {
                        Ip ipbase = eqBase.Ip1;
                        this.IP = new IpVista
                        {
                            Dns = ipbase.dns,
                            Gateway = ipbase.gateway,
                            IpV4 = ipbase.ip_v4,
                            IpV6 = ipbase.ip_v6,
                            Mascara = ipbase.mascara,
                            Wins = ipbase.wins,
                            IpGestion = ipbase.ipGestion,
                            IpLoopback = ipbase.ipLoopback,
                            IpWan = ipbase.ipWan,
                            Mac = ipbase.mac
                        };
                    }
                    if(eqBase.os!=null)
                    {
                        this.Os = new OsVista
                        {
                            Id = eqBase.Os1.id_os,
                            Nombre = eqBase.Os1.descripcion,
                            FamiliaOs = new FamiliaOsVista
                            {
                                Id = eqBase.Os1.FamiliaSo.id_fso,
                                Nombre = eqBase.Os1.FamiliaSo.nombre
                            }
                        };
                    }
                    this.LicenciaOs = eqBase.venc_licencia.HasValue?eqBase.venc_licencia.Value:(DateTime?)null;
                    this.Marca = new MarcaVista
                    {
                        idMarca = eqBase.id_marca.Value,
                        Nombre = eqBase.Marca.descripcion
                    };
                    this.CantidadCpu = eqBase.cantidadcpu.HasValue?eqBase.cantidadcpu:(int?)null;
                    this.Modelo = eqBase.modelo;
                    this.NroSerie = eqBase.UnidadConf.nro_serie;
                    this.Oblea = eqBase.UnidadConf.nombre;
                    this.Observaciones = eqBase.UnidadConf.observaciones;
                    this.TipoEquipo.id = eqBase.UnidadConf.Tipo_equipo.id_tipo;
                    this.TipoEquipo.Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion;
                    this.Criticidad = eqBase.criticidad;
                }
            }
            catch (Exception e)
            {

            }        
        }
    }
}