﻿using soporte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Seguridad.Models
{
    public class OperadorSeguridad:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            switch (page)
            {
                case "Seguridad/Inventario": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Seguridad/Historicos": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Seguridad/Incidentes": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Seguridad/Config": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                case "Seguridad/NuevoTicket": return soporte.Models.Statics.Constantes.AccesosPagina.Full;
                default: return soporte.Models.Statics.Constantes.AccesosPagina.NoAccess;
            }
        }

        public override string getHomePage()
        {
            return "Seguridad/Incidentes";
        }
    }
}