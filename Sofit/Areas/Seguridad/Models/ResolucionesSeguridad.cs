﻿using Sofit.DataAccess;
using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Seguridad.Models
{
    public class ResolucionesSeguridad:ResolucionesVista
    {
        public override string getResolucionesParametros(HistoricosParametros par)
        {
            var resoluciones = lookupRes(par);
            if (resoluciones.Count > 2000)
            {
                return ErrorCantidadExcesiva();
            }
            else
            {
                resoluciones.Sort();
                return toHtmlTable(resoluciones);
            }
        }
        private List<ResolucionesIncidentesVista> lookupRes(HistoricosParametros par)
        {
            
            List<ResolucionesIncidentesVista> resoluciones = new List<ResolucionesIncidentesVista>();            
            using (var dbc = new IncidenciasEntities())
            {

                resoluciones = (from u in dbc.Resoluciones
                                where DateTime.Compare(u.fecha, par.FechaHasta) < 0 && DateTime.Compare(u.fecha, par.FechaDesde) > 0 &&
                                (u.area.Direcciones.nombre == "Seguridad Informática") &&
                                (par.oblea == null || u.UnidadConf.nombre == par.oblea) &&
                                (par.nroIncidente == null || u.Incidencias.numero == par.nroIncidente) &&
                                (par.idTipoResolucion==0 || u.id_tipo_resolucion == par.idTipoResolucion) &&
                                (par.idTecnico==0|| u.id_tecnico == par.idTecnico) &&
                                (par.idJurisdiccion==0 || u.Incidencias.Dependencia.Jurisdiccion.id_jurisdiccion == par.idJurisdiccion)
                                orderby u.fecha descending
                                select new ResolucionesIncidentesVista
                                {
                                    Direccion = u.area.Direcciones.nombre,
                                    Equipo = u.UnidadConf.nombre,
                                    Fecha = u.fecha,
                                    Observaciones = u.observaciones,
                                    Técnico = u.tecnico.descripcion,
                                    TipoResolucion = u.tipo_resolucion.descripcion,
                                    Jurisdiccion = u.Incidencias.Dependencia != null ? u.Incidencias.Dependencia.Jurisdiccion.descripcion : "N/A",
                                    NroIncidente = u.Incidencias.numero
                                }).ToList();
                
            }
            return resoluciones;
        }
        public override string getResolucionesTotalesTecnico(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);

            IEnumerable<TecnicosCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by new { u.Técnico, u.TipoResolucion } into g
                        orderby g.Count() descending
                        select new TecnicosCount
                        {
                            Nombre = g.Key.Técnico,
                            TipoTrabajo = g.Key.TipoResolucion,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTecnico(resGroup);
        }
        public override string getResolucionesTotalesJurisdiccion(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);
            IEnumerable<JurisdiccionesCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.Jurisdiccion into g
                        orderby g.Count() descending
                        select new JurisdiccionesCount
                        {
                            Jurisdiccion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesJurisdiccion(resGroup);
        }
        public override string getResolucionesTotalesTrabajo(HistoricosParametros par)
        {
            List<ResolucionesIncidentesVista> resoluciones = lookupRes(par);
            IEnumerable<TipoResolucionCount> resGroup = null;

            resGroup = (from u in resoluciones
                        group u by u.TipoResolucion into g
                        orderby g.Count() descending
                        select new TipoResolucionCount
                        {
                            TipoResolucion = g.Key,
                            Cantidad = g.Count()
                        });
            return toHtmlTotalesTrabajo(resGroup);
        }
    }
}