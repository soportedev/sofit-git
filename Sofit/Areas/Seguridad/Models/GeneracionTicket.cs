﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Sofit.Areas.Seguridad.Models
{
    public class GeneracionTicket
    {
        public Responses generacionTicket(string nroTicket, int idTipif, int idTipoTicket, int idPrioridad, int idDep, int idUsuario,string obs,List<String> referentes,
            int idCliente,string pathImagen, string pathFile,string mime)
        {
            bool nvoNro = false;
            string nombreCliente = string.Empty;
            Responses result = new Responses();
            #region numeroNuevo
            if (nroTicket == "nuevo")
            {
                lock (result = IncidenciasManager.getNextNro())
                {
                    nvoNro = true;
                }
                using (var dbc = new IncidenciasEntities())
                {
                    nombreCliente = (from u in dbc.Contactos where u.id_contacto == idCliente select u.nombre).Single();
                    var prefix = (from u in dbc.TipoTicket
                                  where u.id == idTipoTicket
                                  select u.prefijoActual).Single();
                    nroTicket = prefix + result.Detail;
                }
            }
            #endregion
            
            #region creacion incidencia
            if (result.Info == "ok")
            {
                IncidenciasManager manager = new IncidenciasManager();
                IDirecciones seg = DireccionesFactory.getDireccion("Seguridad Informática");
                result = manager.crearIncidencia(nroTicket, idTipif, idDep, idUsuario, obs, seg, idTipoTicket, idPrioridad, pathImagen, pathFile, mime, idCliente, nvoNro);
                if (result.Info == "ok") nroTicket = result.Detail;
            }
            #endregion
            #region mails a referentes
            if (referentes != null & result.Info == "ok")
            {
                string nombreTipificacion = string.Empty;
                using (var dbc = new IncidenciasEntities())
                {
                    nombreTipificacion = (from t in dbc.Tipificaciones
                                          where t.id_tipificacion == idTipif
                                          select t.nombre).Single();
                }
                bool huboError = false;
                StringBuilder sb = new StringBuilder();
                string subject = "Ticket Generado con el número " + nroTicket;
                string body = "Fue Generado un Ticket con la identificación " + nroTicket + " a pedido de " + nombreCliente +
                    ", el día " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " hrs. " +
                    ", tipificado como " + nombreTipificacion + ", y se encuentra pendiente de resolución en la subdirección de Seguridad Informática" +
                    ".\n Puede seguir su evolucion en https://portaldgcit.cba.gov.ar" +
                    ".\n Dirección General de IT.";
                EmailSender sender = new EmailSender();
                foreach (string referente in referentes)
                {
                    Referentes referenteBuscado;
                    string direccionMail = string.Empty;
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(referente);
                    if (match.Success)
                    {
                        direccionMail = referente;
                    }
                    else
                    {
                        try
                        {
                            int idRef = 0;
                            Int32.TryParse(referente, out idRef);
                            using (var dbc = new IncidenciasEntities())
                            {
                                referenteBuscado = (from t in dbc.Referentes
                                                    where t.id == idRef
                                                    select t).Single();
                            }
                            direccionMail = referenteBuscado.email;
                        }
                        catch (Exception e)
                        {
                            huboError = true;
                            sb.Append(referente).Append(". ");
                            continue;
                        }
                    }
                    //envio         
                    result = sender.sendEmail(new string[] { direccionMail, subject, body });
                    if (result.Info == "ok")
                    {
                        using (var dbc = new IncidenciasEntities())
                        {
                            var noti = new NotificacionXIncidente
                            {
                                email = direccionMail,
                                id_incidencia = IncidenciasManager.LastId
                            };
                            dbc.NotificacionXIncidente.Add(noti);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }//contexto dbc
                    }//envio exitoso
                    else
                    {                        //error envío
                        huboError = true;
                        sb.Append(referente).Append(". ");
                    }
                }//iteracion referentes

                if (huboError)
                {
                    result.Info = "con errores";
                    result.Detail = sb.ToString();
                }
                else
                {
                    result.Detail = nroTicket;
                }

            }//envio mails
            #endregion mails
            return result;
        }
        public Responses generacionTicketsPedidos(string nroTicket, int idTipif, int idTipoTicket, int idPrioridad, int idDep, int idUsuario, string obs, List<String> referentes,
            int idCliente, string pathImagen, string pathFile, string mime)
        {
            bool nvoNro = false;
            string nombreCliente = string.Empty;
            Responses result = new Responses();
            #region numeroNuevo
            if (nroTicket == "nuevo")
            {
                lock (result = IncidenciasManager.getNextNro())
                {
                    nvoNro = true;
                }
                using (var dbc = new IncidenciasEntities())
                {
                    nombreCliente = (from u in dbc.Contactos where u.id_contacto == idCliente select u.nombre).Single();
                    var prefix = (from u in dbc.TipoTicket
                                  where u.id == idTipoTicket
                                  select u.prefijoActual).Single();
                    nroTicket = prefix + result.Detail;
                }
            }
            #endregion

            #region creacion incidencia
            if (result.Info == "ok")
            {
                IncidenciasManager manager = new IncidenciasManager();
                IDirecciones seg = DireccionesFactory.getDireccion("Seguridad Informática");
                result = manager.crearIncidenciaPedidos(nroTicket, idTipif, idDep, idUsuario, obs, seg, idTipoTicket, idPrioridad, pathImagen, pathFile, mime, idCliente, nvoNro);
                if (result.Info == "ok") nroTicket = result.Detail;
            }
            #endregion
            #region mails a referentes
            if (referentes != null & result.Info == "ok")
            {
                string nombreTipificacion = string.Empty;
                using (var dbc = new IncidenciasEntities())
                {
                    nombreTipificacion = (from t in dbc.Tipificaciones
                                          where t.id_tipificacion == idTipif
                                          select t.nombre).Single();
                }
                bool huboError = false;
                StringBuilder sb = new StringBuilder();
                string subject = "Ticket Generado con el número " + nroTicket;
                string body = "Fue Generado un Ticket con la identificación <b>" + nroTicket + "</b> a pedido de <b>" + nombreCliente +
                    "</b>, el día " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " hrs. " +
                    ", tipificado como " + nombreTipificacion + ", y se encuentra pendiente de resolución en la subdirección de Seguridad Informática" ;
                EmailSender sender = new EmailSender();
                foreach (string referente in referentes)
                {                    
                    string direccionMail = string.Empty;
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(referente);
                    if (match.Success)
                    {
                        direccionMail = referente;
                    }                    
                    //envio         
                    result = sender.sendEmail(new string[] { direccionMail, subject, body });
                    if (result.Info == "ok")
                    {
                        using (var dbc = new IncidenciasEntities())
                        {
                            var noti = new NotificacionXIncidente
                            {
                                email = direccionMail,
                                id_incidencia = IncidenciasManager.LastId
                            };
                            dbc.NotificacionXIncidente.Add(noti);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }//contexto dbc
                    }//envio exitoso
                    else
                    {                        //error envío
                        huboError = true;
                        sb.Append(referente).Append(". ");
                    }
                }//iteracion referentes

                if (huboError)
                {
                    result.Info = "con errores";
                    result.Detail = sb.ToString();
                }
                else
                {
                    result.Detail = nroTicket;
                }

            }//envio mails
            #endregion mails
            return result;
        }
    }
}