﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Seguridad.Models
{
    public class SeguridadHistoricos
    {
        public List<Tecnico> Tecnicos { get; set; }
        public List<Area> Areas { get; set; }
        public List<TipoResolucion> Resoluciones { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<Jurisdicciones> Jurisdicciones { get; set; }
        public List<DireccionVista> Direcciones { get; set; }
        public List<TipoTicketVista> TiposTicket { get; set; }
        public List<PrioridadesTicketVista> PrioridadesTicket { get; set; }
        public List<ServiciosVista> Servicios { get; set; }
    }
}