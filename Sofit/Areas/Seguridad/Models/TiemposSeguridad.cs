﻿using Sofit.DataAccess;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Seguridad.Models
{
    public class TiemposSeguridad
    {
         public int tCriticoIncidente { get; set; }

        public TiemposSeguridad()
        {
        using (var dbc = new IncidenciasEntities())
            {                
                var ts = from to in dbc.TiemposGral
                         select to;
                if (ts != null)
                {                    
                    tCriticoIncidente = ts.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                }
            }
        }
        internal soporte.Models.Statics.Responses setTiempos(int tAIng, int tEIng, int tAAdm, int tEAdm, int tAClaro, int tEClaro, int tAEpec, int tEEpec, int tATelecom, int tETelecom, int tASes, int tESes)
        {
            Responses result = new Responses();
            
            return result;
        }    
    }
}