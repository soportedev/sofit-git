﻿using soporte.Areas.Infraestructura.Models;
using soporte.Areas.Infraestructura.Models.ClasesVista;
using soporte.Areas.Seguridad.Models;
using soporte.Areas.Soporte.Models.Displays;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Seguridad.Controllers
{
    [SessionActionFilter]
    public class InventarioController : Controller
    {
        //
        // GET: /Seguridad/Inventario/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            if (acceso == Constantes.AccesosPagina.ReadOnly)
            {
                ViewBag.acceso = "readonly";
            }
            if (acceso == Constantes.AccesosPagina.Full)
            {
                ViewBag.acceso = "full";
            }
            return View();
        }
        public JsonResult EquipoXOblea(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.UnidadConf
                         where (u.Tipo_equipo.Direcciones.nombre == "Seguridad Informática") &
                         u.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_uc,
                             Oblea = u.nombre,
                             NroSerie = u.nro_serie
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult ServerInfraestructura(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.Servidor
                         where u.UnidadConf.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.id_servidor,                             
                             Oblea = u.UnidadConf.nombre
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXNombreComun(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoSeguridad
                         where u.nombre_comun.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             NombreComun = u.nombre_comun
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult NuevaMarca(string nombre, int idTipo)
        {
            Responses result = new Responses();
            if (nombre.Length == 0)
            {
                result.Detail = "El campo no puede estar vacío";
            }
            else
            {
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var found = (from u in dbc.Marca
                                     where u.descripcion == nombre && u.Tipo_equipo.id_tipo == idTipo
                                     select u).SingleOrDefault();
                        if (found != null)
                        {
                            result.Detail = "La marca ya existe";
                            return Json(result);
                        }
                        else
                        {
                            Marca m = new Marca
                            {
                                descripcion = nombre,
                                dispositivo = idTipo,
                                id_direccion = (from u in dbc.Direcciones where u.nombre == "Seguridad Informática" select u.id_direccion).SingleOrDefault()
                            };
                            dbc.Marca.Add(m);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult GetEquipo(string oblea)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Cluster Seguridad": equipo = new ClusterSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        default: equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
            return Json(equipo);            
        }
        public JsonResult EquipoXIp(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoTele
                         where u.Ip.Any(c => c.ip_v4.StartsWith(prefixText))
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             Ip = u.Ip.Select(c => new IpVista { IpV4 = c.ip_v4 })
                         }).Take(10).ToList();

            }
            return Json(items);
        }
        public JsonResult TiposEquipo()
        {
            List<TiposEquipoVista> tipos = new List<TiposEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                tipos = (from u in dbc.Tipo_equipo
                         where u.Direcciones.nombre == "Seguridad Informática"
                         orderby u.descripcion
                         select new TiposEquipoVista
                         {
                             id = u.id_tipo,
                             Nombre = u.descripcion
                         }).ToList();
            }
            return Json(tipos);
        }
        public JsonResult UmMemoria()
        {
            List<UnidadMedidaVista> um = new List<UnidadMedidaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                um = (from u in dbc.UnidadMedida
                      where u.Tipo_equipo.descripcion == "RAM"
                      orderby u.descripcion
                      select new UnidadMedidaVista
                      {
                          Id = u.id_um,
                          Nombre = u.descripcion
                      }).ToList();
            }
            return Json(um);
        }
        public JsonResult FamiliaSO()
        {
            List<FamiliaOsVista> fam = new List<FamiliaOsVista>();
            using (var dbc = new IncidenciasEntities())
            {
                fam = (from u in dbc.FamiliaSo
                       select new FamiliaOsVista
                       {
                           Id = u.id_fso,
                           Nombre = u.nombre
                       }).ToList();
            }
            return Json(fam);
        }
        public JsonResult NuevoSo(int idF, int idTipo, string nombre)
        {
            Responses result = new Responses();
            using (var dbc = new IncidenciasEntities())
            {
                var found = dbc.Os.Where(c => c.descripcion == nombre & c.id_tipo_equipo == idTipo).SingleOrDefault();
                if (found == null)
                {
                    Os nuevo = new Os
                    {
                        descripcion = nombre,
                        id_tipo_equipo = idTipo,
                        id_familia = idF
                    };
                    dbc.Os.Add(nuevo);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
                else result.Info = "El Sistema Operativo ya está agregado!!";
            }
            return Json(result);
        }
        public JsonResult VersionSO(int idF, int idTipo)
        {
            List<OsVista> fam = new List<OsVista>();
            using (var dbc = new IncidenciasEntities())
            {
                fam = (from u in dbc.Os
                       where u.id_tipo_equipo == idTipo & u.FamiliaSo.id_fso == idF
                       select new OsVista
                       {
                           Id = u.id_os,
                           Nombre = u.descripcion
                       }).ToList();
            }
            return Json(fam);
        }
        public JsonResult NuevoEquipo(EquipoSeguridadInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf where u.nombre == e.Oblea select u).SingleOrDefault();

                    if (encontrado == null)
                    {
                        encontrado = (from u in dbc.UnidadConf where (u.nro_serie.Length>0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == e.TipoEquipo.Nombre) select u).SingleOrDefault();
                        if (encontrado == null)
                        {
                            usuarioBean actual = Session["usuario"] as usuarioBean;
                            UnidadConf uc = new UnidadConf
                            {
                                nombre = e.Oblea,
                                id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion,
                                fecha = DateTime.Now,
                                id_tipo = e.TipoEquipo.id,
                                observaciones = e.Observaciones,
                                nro_serie = e.NroSerie,
                                id_usuario = actual.IdUsuario
                            };
                            dbc.UnidadConf.Add(uc);
                            EquipoSeguridad eqSeg = new EquipoSeguridad();
                            eqSeg.id_uc = uc.id_uc;
                            eqSeg.id_dependencia = e.Dependencia.idDependencia;
                            eqSeg.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Infraestructura").Single().id_estado;
                            eqSeg.id_marca = e.Marca.idMarca;
                            eqSeg.modelo = e.Modelo;
                            eqSeg.nombre_comun = e.NComun;
                            eqSeg.criticidad = e.Criticidad;
                            eqSeg.memoria = e.Memoria.Capacidad;
                            int? idServer=null;
                            if (e.Server != null)
                            {
                                idServer = e.Server.idEquipo;
                            }                            
                            eqSeg.id_server = idServer;
                            if (e.Memoria.Um.Id == 0)
                            {
                                eqSeg.id_um_mem = null;
                            }
                            else eqSeg.id_um_mem = e.Memoria.Um.Id;
                            
                            if (e.Os.Id == 0)
                            {
                                eqSeg.os = null;
                            }
                            else eqSeg.os = e.Os.Id;
                            eqSeg.cpu = e.Cpu;

                            Ip ip = eqSeg.Ip1;
                            if (ip != null)
                            {
                                ip.ip_v4 = e.IP.IpV4;
                                ip.ip_v6 = e.IP.IpV6;
                                ip.mascara = e.IP.Mascara;
                                ip.wins = e.IP.Wins;
                                ip.gateway = e.IP.Gateway;
                                ip.dns = e.IP.Dns;
                            }
                            else
                            {
                                ip = new Ip
                                {
                                    dns = e.IP.Dns,
                                    gateway = e.IP.Dns,
                                    wins = e.IP.Wins,
                                    mascara = e.IP.Mascara,
                                    ip_v4 = e.IP.IpV4,
                                    ip_v6 = e.IP.IpV6

                                };
                                eqSeg.Ip1 = ip;
                            }
                            eqSeg.puesta_produccion = e.PuestaProduccion;                            
                            dbc.EquipoSeguridad.Add(eqSeg);
                            dbc.SaveChanges();
                            result.Info = "ok";
                            result.Detail = "Equipo agregado!!!";
                        }
                        else
                        {
                            result.Detail = "Un equipo con ese Número de serie ya existe";
                        }
                    }
                    else
                    {
                        result.Detail = "El equipo con esa Oblea ya existe";
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }

            return Json(result);
        }
        public JsonResult UpdateEquipo(EquipoSeguridadInventario e)
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            EquipoSeguridad encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.EquipoSeguridad where u.UnidadConf.id_uc == e.idEquipo select u).SingleOrDefault();

                    if (encontrado != null)
                    {                        
                        encontrado.id_marca = e.Marca.idMarca;
                        encontrado.modelo = e.Modelo;
                        var tipoe = dbc.Tipo_equipo.Where(c => c.id_tipo == e.TipoEquipo.id).SingleOrDefault();
                        encontrado.UnidadConf.id_tipo = e.TipoEquipo.id;
                        encontrado.memoria = e.Memoria.Capacidad;
                        encontrado.nombre_comun = e.NComun;
                        encontrado.cpu = e.Cpu;
                        encontrado.cantidadcpu = e.CantidadCpu;
                        if (e.Server != null)
                        {
                            encontrado.id_server = e.Server.idEquipo;
                        }
                        else encontrado.id_server = null;
                        if (e.Memoria.Um.Id == 0)
                        {
                            encontrado.id_um_mem = null;
                        }
                        else encontrado.id_um_mem = e.Memoria.Um.Id;                        
                        encontrado.cpu = e.Cpu;                        
                        Ip ip = encontrado.Ip1;
                        if (ip != null)
                        {
                            ip.ip_v4 = e.IP.IpV4;
                            ip.ip_v6 = e.IP.IpV6;
                            ip.mascara = e.IP.Mascara;
                            ip.wins = e.IP.Wins;
                            ip.gateway = e.IP.Gateway;
                            ip.dns = e.IP.Dns;
                        }
                        else
                        {
                            ip = new Ip
                            {
                                dns = e.IP.Dns,
                                gateway = e.IP.Dns,
                                wins = e.IP.Wins,
                                mascara = e.IP.Mascara,
                                ip_v4 = e.IP.IpV4,
                                ip_v6 = e.IP.IpV6

                            };
                            encontrado.Ip1 = ip;
                        }
                        encontrado.venc_licencia = e.LicenciaOs;
                        encontrado.puesta_produccion = e.PuestaProduccion;                       
                        if (e.Os.Id == 0)
                        {
                            encontrado.os = null;
                        }
                        else encontrado.os = e.Os.Id;
                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        encontrado.UnidadConf.id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion;
                        encontrado.id_estado = e.Estado.idEstado;
                        encontrado.criticidad = e.Criticidad;
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        //actualización gestión de cambios
                        var ecx = dbc.EcxGc.Where(c => c.id_uc == e.idEquipo & c.fecha_actualizacion == null);
                        if (ecx.Count() > 0)
                        {
                            var exfir = ecx.First();
                            if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                            {
                                ecx.First().fecha_actualizacion = DateTime.Now;
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Equio actualizado!!";
                    }
                    else
                    {
                        result.Detail = "No fue encontrado el Elemento";
                        return Json(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;

            }
            return Json(result);
        }
        public JsonResult NuevoCluster(ClusterSeguridadInventario e)
        {
            Responses result = new Responses();
            UnidadConf encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.UnidadConf
                                  where (u.nombre == e.Oblea |
                                      (e.NroSerie.Length > 0 & u.nro_serie == e.NroSerie & u.Tipo_equipo.descripcion == "Cluster Seguridad"))
                                  select u).SingleOrDefault();
                    if (encontrado == null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        UnidadConf uc = new UnidadConf
                        {
                            nombre = e.Oblea,
                            id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion,
                            fecha = DateTime.Now,
                            id_tipo = e.TipoEquipo.id,
                            observaciones = e.Observaciones,
                            nro_serie = e.NroSerie,
                            id_usuario = actual.IdUsuario
                        };
                        dbc.UnidadConf.Add(uc);
                        ClusterSeguridad cluster = new ClusterSeguridad();
                        cluster.id_dependencia = e.Dependencia.idDependencia;
                        cluster.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Seguridad Informática").Single().id_estado;
                        cluster.id_uc = uc.id_uc;
                        cluster.nombre_comun = e.NComun;
                        foreach (HostCluster h in e.Hosts)
                        {
                            var hos = dbc.EquipoSeguridad.Where(c => c.id_uc == h.Id).Single();
                            cluster.EquipoSeguridad.Add(hos);
                        }
                        dbc.ClusterSeguridad.Add(cluster);
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Cluster agregado!!!";
                    }
                    else
                    {
                        result.Detail = "Un equipo con esa Oblea o Nro. de Serie ya existe";
                    }
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }
            return Json(result);
        }
        public JsonResult UpdateCluster(ClusterSeguridadInventario e)
        {
            Responses result = new Responses();
            ClusterSeguridad encontrado = null;
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    encontrado = (from u in dbc.ClusterSeguridad where u.UnidadConf.nombre == e.Oblea select u).SingleOrDefault();

                    if (encontrado != null)
                    {
                        usuarioBean actual = Session["usuario"] as usuarioBean;
                        encontrado.UnidadConf.nombre = e.Oblea;
                        encontrado.UnidadConf.id_jurisdiccion = e.Dependencia.Jurisdiccion.idJurisdiccion;
                        encontrado.UnidadConf.fecha = DateTime.Now;
                        encontrado.UnidadConf.id_tipo = e.TipoEquipo.id;
                        encontrado.UnidadConf.observaciones = e.Observaciones;
                        encontrado.UnidadConf.nro_serie = e.NroSerie;
                        encontrado.UnidadConf.id_usuario = actual.IdUsuario;
                        encontrado.id_dependencia = e.Dependencia.idDependencia;
                        encontrado.id_estado = e.Estado.idEstado;
                        encontrado.nombre_comun = e.NComun;
                        encontrado.EquipoSeguridad.Clear();
                        if (e.Hosts != null)
                        {
                            foreach (HostCluster h in e.Hosts)
                            {
                                var hos = dbc.EquipoSeguridad.Where(c => c.id_uc == h.Id).Single();
                                encontrado.EquipoSeguridad.Add(hos);
                            }
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                        result.Detail = "Servidor actualizado!!!";
                    }
                    else
                    {
                        result.Detail = "No fue encontrado el Tipo!! Si intenta cambiar el tipo de equipo, elimínelo" +
                            " y vuelva a crearlo o contacte al administrador!!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult SaveBaseLine(int id)
        {
            Responses result = new Responses();
            IEquipoInventario equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.UnidadConf
                              where u.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    switch (eqBase.Tipo_equipo.descripcion)
                    {
                        case "Cluster Seguridad": equipo = new ClusterSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                        default: equipo = new EquipoSeguridadInventario();
                            equipo.construir(eqBase.id_uc);
                            break;
                    }
                }
                if (equipo != null)
                {
                    result = equipo.SaveLineaBase();
                }
                return Json(result);
            }
        }
    }
}
