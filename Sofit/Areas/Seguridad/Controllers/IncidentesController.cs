﻿using Sofit.DataAccess;
using soporte.Areas.Seguridad.Models;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Seguridad.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        //
        // GET: /Seguridad/Incidentes/
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            SeguridadModel model = new SeguridadModel();
            List<IncTeleVista> incIngresados = new List<IncTeleVista>();
            List<IncTeleVista> incPendientes = new List<IncTeleVista>();
            List<IncTeleVista> incTotales = new List<IncTeleVista>();
            List<IncTeleVista> incSuspendidos = new List<IncTeleVista>();
            List<TipificacVista> tipificaciones = new List<TipificacVista>();
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Area> areas = new List<Area>();
            List<TipoResolucion> tipoResoluciones = new List<TipoResolucion>();
            List<TipoResolucion> tipoFalla = new List<TipoResolucion>();
            List<Turno> turnos = new List<Turno>();
            List<DireccionVista> subdir = new List<DireccionVista>();
            TiemposSeguridad tiempos = new TiemposSeguridad();
            IDirecciones dirSeguridad = DireccionesFactory.getDireccion("Seguridad Informática");
            using (var dbc = new IncidenciasEntities())
            {
                incTotales = (from d in dbc.selectIncidenciasActivas(dirSeguridad.IdDireccion)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value
                              }).ToList();
                
                incSuspendidos = incTotales.Where(c => c.Estado == "Suspendido").ToList();
                incIngresados = incTotales.Where(c => c.Area.NombreArea == "Entrada Seguridad" & c.Estado != "Suspendido").ToList();
                incPendientes = incTotales.Where(c => c.Area.NombreArea == "Seguridad" & c.Estado != "Suspendido").ToList();
                
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new Jurisdicciones
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Seguridad Informática"
                         select new Area
                         {
                             idArea = u.id_area,
                             Nombre = u.descripcion
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Seguridad Informática" & u.activo.Value == true
                            select new Tecnico
                            {
                                idTecnico = u.id_tecnico,
                                Nombre = u.descripcion
                            }).ToList();
                turnos = (from u in dbc.turno
                          select new Turno
                          {
                              idTurno = u.id_turno,
                              Nombre = u.descripcion
                          }).ToList();
                tipoResoluciones = (from u in dbc.tipo_resolucion
                                    where u.area.descripcion == "Seguridad"
                                    orderby u.descripcion
                                    select new TipoResolucion
                                    {
                                        idTipoResolucion = u.id_tipo_resolucion,
                                        Nombre = u.descripcion
                                    }).ToList();

                subdir = (from u in dbc.Direcciones
                          select new DireccionVista
                          {
                              idDireccion = u.id_direccion,
                              Nombre = u.nombre
                          }).ToList();
            }
            foreach (IncTeleVista t in incIngresados)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incPendientes)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incSuspendidos)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }           
            BuilderTipifTree arbol = new BuilderTipifTree();
            arbol.construir("Seguridad Informática");
            model.IncidentesIngresados = incIngresados;
            model.IncidentesPendientes = incPendientes;
            model.IncidentesSuspendidos = incSuspendidos;
            model.IncidentesSuspendidos = incSuspendidos;
            model.TipoResolucion = tipoResoluciones;
            model.Turnos = turnos;
            model.Areas = areas;
            model.Tecnicos = tecnicos;
            model.Tipificaciones = arbol.getResult();
            model.Jurisdicciones = jurisdicciones;
            model.SubDirecciones = subdir;
            return View(model);
        }
        public JsonResult derivarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idAreaDestino = Int32.Parse(nvc.Get("idAreaDestino"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idAreaDestino);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia,true,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.derivar(areaDestino.IdArea, idTecnico == 0 ? areaDestino.getIdTecnicoGenerico() : idTecnico, idUsuario);
            return Json(result);
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {
            List<string> items = new List<string>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.Incidencias.numero.StartsWith(prefixText) & u.area.Direcciones.nombre == "Seguridad Informática"
                         select u.Incidencias.numero).Take(5).ToList();
            }
            return Json(items);
        }
        public JsonResult buscarEqOInc(string texto)
        {
            DatosUbicacion resp = new DatosUbicacion();

            using (var dbc = new IncidenciasEntities())
            {
                if (texto.ToLower().StartsWith("cba") | texto.ToLower().StartsWith("ln") |
                    texto.ToLower().StartsWith("sol") | texto.ToLower().StartsWith("inc") |
                    texto.ToLower().StartsWith("prb"))
                {
                    resp = (from u in dbc.IncidenciasActivas
                            where u.Incidencias.numero == texto// & u.area.Direcciones.nombre == "Seguridad Informática"
                            select new DatosUbicacion
                            {
                                SubDireccion = u.area.Direcciones.nombre,
                                Area = u.area.descripcion,
                                Estado = u.Estado_incidente.descripcion,
                                Numero = u.Incidencias.numero,
                                Tipo = "Incidente",
                                Fecha = u.fecha_inicio,
                                Dependencia = u.Incidencias.Dependencia.descripcion,
                                Desc = u.Incidencias.descripcion
                            }).FirstOrDefault();
                }                
            }
            if (resp == null) resp = new DatosUbicacion();
            return Json(resp);
        }
        public JsonResult derivarADireccion(int idDir, int idInc)
        {
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false,false);
            Incidencia inc = builder.getResult();
            result = inc.derivarIncidenciaADireccion(idDir, idUsuario);
            return Json(result);
        }        
        public JsonResult incidentesToLife(string nroInc)
        {
            IncidenciasManager im = new IncidenciasManager();
            IDirecciones soporte = DireccionesFactory.getDireccion("Seguridad Informática");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            return Json(im.reanimarIncidencia(nroInc, idUsuario, soporte));
        }
        public JsonResult eliminarIncidente(int idInc)
        {
            Responses result = new Responses();
            result = verificarPedidosHistoricos(idInc);
            if (result.Info == "ok")
            {
                IncidenciasManager mgr = new IncidenciasManager();
                result = mgr.eliminarIncidente(idInc);
            }
            return Json(result);
        }
        public JsonResult solucionarIncidente()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            int id_inc = Int32.Parse(nvc.GetValues("idInc")[0]);
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(id_inc, true, true, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.cerrarIncidencia(idUsuario);
            return Json(result);
        }
        private Responses verificarPedidosHistoricos(int Id)
        {
            Responses result = new Responses();
            bool flagCheck = false;
            using (var dbc = new IncidenciasEntities())
            {
                flagCheck = dbc.PedidosSeguridad.Any(c => c.id_ticket == Id);
                
            }
            if (flagCheck)
            {
                result.Info = "error";
                result.Detail = "Este ticket tiene pedidos de acceso a servicios informáticos y no puede ser eliminado";
            }
            else
            {
                result.Info = "ok";
            }
            return result;
        }
        
        public JsonResult cargarSoluciones(HttpPostedFileBase imagen)
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();
            string oblea = nvc.Get("oblea");
            string nroIncidente = nvc.Get("nroIncidente");
            int tResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int tecnico = Int32.Parse(nvc.Get("idTecnico"));
            int turno = Int32.Parse(nvc.Get("idTurno"));
            int idIncidente = Int32.Parse(nvc.Get("idIncidente"));
            int idEquipo = 0;
            Int32.TryParse(nvc.Get("idEquipo"), out idEquipo);
            string observacion = nvc.Get("observaciones");
            //Get the Data
            string fecha = DateTime.Now.ToShortDateString();
            string tabla = nvc.Get("nombreGrilla");
            //sobrescribir ultima
            bool sobrescribir = false;
            sobrescribir = Boolean.Parse(nvc.Get("sobrescribir"));
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente, sobrescribir, false, false);
            //
            Incidencia inc = builder.getResult();
            bool hayImagen = imagen != null;
            string nombreFile = string.Empty;            
            #region foto
            if (hayImagen)
            {
                string pathImagen = string.Empty;
                string nombreArchivo = imagen.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string ran = new Random().Next(10000).ToString("0000");
                nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getResolucionCustom(nombreFile);
                    var stream = imagen.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion            
            //buscar equipos
            bool flag = true;
            List<int> idEquipos = new List<int>();
            bool esParaEquipo = Boolean.Parse(nvc.Get("esParaEquipo"));
            if (esParaEquipo)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("idEquipo")) idEquipos.Add(Int32.Parse(nvc.GetValues(key)[0]));
                }
                if (idEquipos.Count == 0)
                {
                    result.Info = "error";
                    result.Detail = "No se cargaron las obleas del equipo.";
                    flag = false;
                }
                if (flag)
                {
                    foreach (int id in idEquipos)
                    {
                        inc.cargarSolucion(usuario.IdUsuario, id, tResolucion, tecnico, turno, observacion, DateTime.Now, nombreFile, sobrescribir);
                    }
                    result.Info = "ok";
                }
            }
            else
            {
                result = inc.cargarSolucion(usuario.IdUsuario, null, tResolucion, tecnico, turno, observacion, DateTime.Now, nombreFile, sobrescribir);
            }
            return Json(result);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var tecnico = (from u in dbc.tecnico
                                   where u.descripcion == Tecnico
                                   select u).SingleOrDefault();
                    foreach (string strId in idIncidencia)
                    {
                        int idIncidente = Int32.Parse(strId);
                        var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                        incActiva.id_tecnico = tecnico.id_tecnico;
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
        public JsonResult suspenderIncidentes(int idInc)
        {
            Responses result = new Responses();
            int idUsuarioActual = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true, false, false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.suspenderIncidencia(idUsuarioActual);
            return Json(result);
        }
    }
}
