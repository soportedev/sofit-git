﻿using Sofit.DataAccess;
using soporte.Controllers;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace soporte.Areas.Seguridad.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
        //
        // GET: /Seguridad/Config/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            return View();
        }
        public JsonResult NuevoUsuario(string login, string nombre, string p1, int perfil)
        {
            Responses result = new Responses();
            //usuarioHandler user = new usuarioHandler();
            //IDirecciones seguridad = DireccionesFactory.getDireccion("Seguridad Informática");
            //result = user.nvoUsuario(login, nombre, p1, perfil, seguridad);
            return Json(result);
        }
        public ActionResult Usuarios()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Usuarios");
            IDirecciones seguridadDir = DireccionesFactory.getDireccion("Seguridad Informática");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesSeguridad();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getTodosDeDireccion(seguridadDir);
                return PartialView("Usuarios", model);
            }
        }
       
        public ActionResult Tipificaciones()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Tipificaciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre == "Seguridad Informática"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Seguridad Informática");
                model.Tipificaciones = arbol.getResult();
                return View(model);
            }
        }
        public PartialViewResult Autorizantes()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Seguridad/Autorizantes");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre == "Seguridad Informática"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Seguridad Informática");
                model.Tipificaciones = arbol.getResult();
                return PartialView(model);
            }
        }
        public JsonResult getAutorizantes()
        {
            List<ReferentesVista> referentes = null;
            using (var dbc = new IncidenciasEntities())
            {
                referentes = dbc.Usuarios.Where(c => c.Roles.Any(d=>d.Nombre == "Autorizante Reglas")).Select(c => new ReferentesVista { id = c.id_usuario, nombre = c.nombre_completo }).ToList();                
            }
            return Json(referentes);
        }
        public JsonResult agregarAutorizante(string nombre, string displayname, string email)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {                    
                    var refExiste=dbc.Usuarios.Where(c => c.nombre==nombre);
                    Roles autReglas = dbc.Roles.Where(c => c.Nombre == "Autorizante Reglas").Single();
                    if (refExiste.Count() > 0)
                    {
                        bool flagTieneElRol = false;
                        Usuarios usuario = refExiste.First();
                        if (usuario.email == null | usuario.email == "")
                        {
                            usuario.email = email;
                        }
                        ICollection<Roles> rolesactuales = usuario.Roles;
                        foreach(Roles r in rolesactuales)
                        {
                            if(r.Nombre=="Autorizante Reglas")
                            {
                                flagTieneElRol = true;
                            }
                        }
                        if (!flagTieneElRol)
                        {                            
                            usuario.Roles.Add(autReglas);
                        }
                    }
                    else
                    {
                        Usuarios nuevo = new Usuarios
                        {
                            nombre = nombre,
                            email = email,
                            nombre_completo=displayname,                            
                            acceso=0,
                            personalizacion=2,
                            es_interno=false,
                            activo=true
                        };
                        nuevo.Roles.Add(autReglas);
                        dbc.Usuarios.Add(nuevo);
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                    result.Detail = "Se agregó a " + displayname + " como autorizante";

                }
            }catch(Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        //public JsonResult agregarAutorizanteAd(string nombre, string email)
        //{
        //    Responses result = new Responses();
        //    try
        //    {
        //        using (var dbc = new IncidenciasEntities())
        //        {
        //            var checking = dbc.Referentes.Where(c => c.nombre == nombre);
        //            if (checking.Count()>0)
        //            {
        //                var refer = checking.Single();
        //                refer.id_rol = dbc.RolesReferentes.Where(c => c.nombre == "Autorizante").Single().id;
        //                dbc.SaveChanges();
        //                result.Info = "ok";
        //                result.Detail = "Se agregó a " + nombre + " como autorizante";
        //            }
        //            else
        //            {
        //                Referentes nuevo = new Referentes
        //                {
        //                    nombre = nombre,
        //                    email = email,
        //                    id_rol = dbc.RolesReferentes.Where(c => c.nombre == "Autorizante").Single().id
        //                };

        //                dbc.Referentes.Add(nuevo);
        //                dbc.SaveChanges();
        //                result.Info = "ok";
        //                result.Detail = "Se agregó a " + nombre + " como autorizante";
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Detail = ex.Message;
        //    }
        //    return Json(result);
        //}
        public JsonResult quitarAutorizante(int idRef)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var usuario = dbc.Usuarios.Where(c => c.id_usuario == idRef).Single();
                    var rolaquitar = usuario.Roles.First(c => c.Nombre == "Autorizante Reglas");
                    usuario.Roles.Remove(rolaquitar);
                    dbc.SaveChanges();
                    result.Info = "ok";
                    result.Detail = "Se quitó a " + usuario.nombre_completo + " como autorizante";

                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre, int idServicio)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Seguridad Informática" select u).Single();
                    Tipificaciones nueva = new Tipificaciones()
                    {
                        nombre = nombre,
                        descripcion = desc,
                        id_padre = idPadre,
                        id_servicio = idServicio == 0 ? (int?)null : idServicio,
                        id_direccion = direccionSoporte.id_direccion
                    };
                    dbc.Tipificaciones.Add(nueva);

                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas, int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Seguridad Informática");
            using (var dbc = new IncidenciasEntities())
            {
                var serv = new Servicios
                {
                    nombre = nombre,
                    tiempo_res = (int)totales.TotalHours,
                    tiempo_margen=(int)totalesMargen.TotalHours,
                    id_direccion = direccion.IdDireccion,
                    tolerancia = 80,
                    vigente = true
                };
                dbc.Servicios.Add(serv);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        
        public JsonResult quitarServicio(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var serv = dbc.Servicios.Where(c => c.id_servicio == id).Single();
                    dbc.Servicios.Remove(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception)
            {
                result.Detail = "El servicio está siendo utilizado";
            }
            return Json(result);
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t = (from u in dbc.Tipificaciones
                             where u.id_tipificacion == id
                             select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult borrarEquipo(string oblea)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var equipo = dbc.UnidadConf.Where(c => c.nombre == oblea).SingleOrDefault();
                    //var rels = dbc.RelacionUC.Where(c => c.UnidadConf.nombre == oblea | c.UnidadConf1.nombre == oblea);
                    //dbc.RelacionUC.RemoveRange(rels);
                    if (equipo != null)
                    {
                        switch (equipo.Tipo_equipo.descripcion)
                        {                            
                            case "Cluster": var cluster = equipo.ClusterSeguridad.Where(c => c.id_uc == equipo.id_uc).Single();
                                cluster.EquipoSeguridad.Clear();
                                dbc.ClusterSeguridad.Remove(cluster);
                                dbc.UnidadConf.Remove(equipo);
                                break;                            
                            default: var server = equipo.EquipoSeguridad.Where(c => c.id_uc == equipo.id_uc).Single();
                                dbc.EquipoSeguridad.Remove(server);
                                dbc.UnidadConf.Remove(equipo);
                                break;

                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {
                        result.Detail = "No se encontró el equipo";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
    }
}
