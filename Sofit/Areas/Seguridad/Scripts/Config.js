﻿$(function () {
    $('body').on('keydown.autocomplete', '#tNombre', function () {
        var url=$(this).attr('data-url');
        $(this).autocomplete({
            minLength: "2",
            source: (
                function (request, response) {
                    var term = '{ prefix: "' + request.term + '"}';
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: url,
                        data: term,
                        dataType: "json",
                        success: function (data) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            else {
                                var suggestions = [];
                                $.each(data, function (i, val) {
                                    var obj = {};
                                    obj.value = val.nombre;
                                    obj.label = val.displayname;
                                    obj.email = val.email;                                    
                                    suggestions.push(obj);
                                })
                                response(suggestions);
                            }
                        }
                    });
                }),
            open: function () { $('.ui-autocomplete').css('font-size', '.9em') },
            focus: function (e, ui) { $(this).val(ui.item.label); return false; },
            select: function (e, ui) {
                var nombre = ui.item.value;
                var dname = ui.item.label;
                var em = ui.item.email;
                //var tel = ui.item.te;                
                $(this).attr('data-nombre', ui.item.value);
                $(this).parents('ul').find('#tEmail').val(em);
               
                $('#aceptarReferente').removeAttr('disabled');
               
                return false;
            }
        });
    });
    //selecction
    $('body').on({
        mouseenter: function () {
            $(this).addClass('ui-state-hover');
        },
        mouseleave: function () {
            $(this).removeClass('ui-state-hover');
        },
        click: function () {
            $('.row', '.refContainer').each(function () {
                $(this).removeClass('ui-state-highlight');
            });
            $(this).addClass('ui-state-highlight');
            var id = $(this).find('td:eq(0)').text();
        }
    }, '#Autorizantes div.ra');    
    $('body').on('click', '#aceptarReferente', function (e) {
        e.preventDefault();
        var displayn = $('#tNombre').val();
        var nombre = $('#tNombre').attr('data-nombre');
        var emeil = $('#tEmail').val();
        if (regEmail.test(emeil)) {


            $.ajax({
                url: 'Config/agregarAutorizante',
                data: { nombre: nombre, displayname: displayn, email: emeil },
                type: 'post',
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    } else {
                        if (data.Info == 'ok') {
                            mostrarExito(data.Detail);
                            loadAutorizantes();
                            clear();
                        }
                        else mostrarError(data.Detail)
                    }
                }
            });
        } else {
           var email = document.getElementById('tEmail');
           email.removeAttribute('disabled');
           mostrarError("Complete el email!!");
        }       
    });  
    $('body').on('click', 'span.trash', function () {
        var div = $(this).parents('div.ra');
        var IdRef = $(div).find('span:first').html();        
        $.ajax({
            url: 'Config/quitarAutorizante',
            data: { 'idRef': IdRef },
            type: 'post',
            success: function (d) {
                if (d.Info == 'ok') {
                    div.remove();
                    mostrarExito(d.Detail)
                }
                else mostrarError(d.Detail);
            },
            error: function (d, e, f) {
                mostrarError(f);
            }
        });
        ;
    });
    $('body').on('click', '#tNombre', function () {
        clear()
    });
    //$('body').on('click', 'div.iconContainer', function () {
    //    var span = $(this).siblings('span.italized');
    //    var input = $(this).siblings('input#tNombre');
    //    if ($(this).hasClass('ui-state-hover')) {
    //        $(this).removeClass('ui-state-hover')
    //        span.html('Búsqueda <br/ >local');
    //        input.attr('data-url','/Config/getReferentesXNombre')
    //    } else {
    //        $(this).addClass('ui-state-hover');            
    //        span.html('Búsqueda <br/ >en AD');  
    //        input.attr('data-url', '/Config/getAdNombres')
    //    }
    //});
});
function clear() {        
    $('#tNombre').removeAttr('data-id').val('');
    $('#tEmail').val('').attr('disabled','disabled');
    $('#tTe').val('');
    $('ul#jurisdicciones').find('li.jur').remove();
    $('#aceptarReferente').attr('disabled', 'disabled');
}
function loadAutorizantes() {
        $.ajax({
            url: 'Config/getAutorizantes',            
            type: 'post',
            success: function (d) {
                var data = d;
                var ul = $('#Autorizantes').find('div.autContainer>ul');
                var li = $(ul).find('li.nam').remove();
                $.each(data, function (i, v) {
                    var spanId = $('<span>').attr('class', 'hide');
                    var spanNombre = $('<span>').attr('class', 'nomJur');
                    var spanTrash = $('<span>').attr('class', 'icon trash');
                    var li = $('<li class="nam">');
                    var div = $('<div>').attr('class', 'ra');
                    spanId.html(v.id);
                    spanNombre.html(v.nombre);
                    div.append(spanId).append(spanNombre).append(spanTrash);
                    li.append(div);
                    ul.append(li);                    
                });
            }
        });
    }
function loadDivJurisdiccion(obj) {
        var tablaJur = $('.tablaJurisd');
        $.each(obj.location, function (i, v) {
            var div = $('<div>').attr('class', 'row');
            var spanId = $('<span>').attr('class', 'hide');
            var spanTipo = $('<span>').attr('class', 'hide');
            var span1 = $('<span>').attr('class', 'nomJur');
            var span2 = $('<span>').attr('class', 'icon trash');
            spanId.html(v.id);
            span1.html(v.nombre);
            spanTipo.html(v.tipo);
            div.append(spanId).append(spanTipo).append(span1).append(span2);
            tablaJur.append(div);
        });
    }
function loadDivDatos(obj) {
        var nombre = $('#tNombre', '.datosRefContainer');
        var email = $('#tEmail', '.datosRefContainer');
        var te = $('#tTe', '.datosRefContainer');
        nombre.val(obj.nombre);
        email.val(obj.email);
        te.val(obj.te);
}
function listadoReferentes(idDep, texto) {
    $.ajax({
        data: 'idDep=' + idDep,
        url: '/Config/referentes',
        type: 'POST',
        beforeSend: function () { },
        success: function (data) {
            var div = $('.referentesListado');
            div.html('');
            var ul = $('<ul>');
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            if (data.length == 0) {
                var p = $('<p>');
                p.append('No hay referentes cargados');
                div.append(p);
            }
            else {
                $.each(data, function (i, item) {
                    var li = $('<em>');
                    li.html(item.nombre);
                    ul.append(li);
                });
                div.append(ul);
            }
            var container = $('.referentesContainer');
            var legend = $(container).find('legend');
            legend.html('Referentes para ' + texto);
            container.show();
        }
    });
}
function bindUsuarios() {
    $('button').button();
}
function bindAut() {
    loadAutorizantes();
}
function deleteEquipo(e) {
    e.preventDefault();
    var oblea = $.trim($('#tOblea').val());
    var este = $('#removeEquipo');
    if (regSeguridad.test(oblea.toUpperCase())) {
        $.ajax({
            url: 'Config/borrarEquipo',
            type: 'POST',
            data: 'oblea=' + oblea,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    mostrarExito('Equipo Borrado');
                    oblea.val('');
                }
                else {
                    mostrarError(data.Detail)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    else {
        mostrarError("El formato de la oblea no es válido");
    }
}
function actualizarTiempos() {

}