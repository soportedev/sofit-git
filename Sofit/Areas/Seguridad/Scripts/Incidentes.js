﻿$(function () {
    //Trabajos BandejaEntrada
    $('#Trabajos').dialog({
        autoOpen: false,
        closeOnEscape: true,
        modal: true,
        resizable: true,
        draggable: true,
        resizeStop: function (ev, ui) {
            var originalSize = ui.originalSize;
            var size = ui.size;
            var textArea = $(ev.target).find('.tSolucion');
            var heightDif = originalSize.height - size.height;
            var widthDif = originalSize.width - size.width;
            textArea.height(textArea.height() - heightDif);
            //textArea.width(textArea.width() - widthDif);
        },        
        title: "Cargar Trabajo",
        width: '600px',
        open: function () {
            $('p.charCounter', thisdialog).removeClass('error2').html('Chars: 0');
            var thisdialog = $(this);
            clearTrabajosPanel(thisdialog);
            $(thisdialog).parent().css('position', 'fixed');
            $(thisdialog).parent().css('top', '0px');
            $(thisdialog).parent().css('z-index', '99');
            var hscr = $('html').height();
            $('.ui-widget-overlay').css('height', hscr);
            $('.ui-widget-overlay').css('z-index', '98');
            var datos = $(this).data('datos');
            $('input[type=text]', '.nroIncidente').val(datos.nroIncidente);
            $('label.idInc', '.nroIncidente').text(datos.idIncidente);
            $('#idTipoResolucion').val(0);
            $('.error', this).hide();
        },
        buttons: {
            Aceptar: function () {
                //
                var fData = new FormData();
                var datos = $(this).data('datos');
                var tresolucion = $('#tResTrabajo option:selected').val() == 0;
                if (tresolucion) {
                    $('#Trabajos').find('.message').html('Seleccione el tipo de Resolución.');
                    $('.error, #Trabajos').show();
                    return;
                }
                datos.url = "Incidentes/cargarSoluciones";
                var tResolucion = $('#tResTrabajo').val();                
                var obs = $('#observaciones');
                if ($.trim(obs.val()).length < 10) {
                    $('#Trabajos').find('.message').html('Complete el campo de Observaciones');
                    $('.error, #Trabajos').show();
                    return;
                }
                var inpFile = $('#upFileRes');
                var img = inpFile.get(0).files[0];
                fData.append('imagen', img);
                fData.append('idTecnico', $('#idTecnico').val());
                fData.append('idTurno', $('#idTurno').val());
                fData.append('observaciones', obs.val());
                fData.append('idTipoResolucion', tResolucion);
                fData.append('idIncidente', datos.idIncidente);
                if ($('#dImagen').is(':visible')) fData.append('idImagen', $('#dImagen').val());
                if ($('#esParaEquipo').is(':visible')) {
                    var valorCheck = $('#esParaEquipo').is(':checked');
                    fData.append('esParaEquipo', valorCheck);
                    if ($('#esParaEquipo').is(':checked')) {
                        var els = $('#Trabajos').find('.idEquipo');
                        var sufix = 0;
                        $.each(els, function () {
                            fData.append('idEquipo.' + sufix, $(this).html());
                            sufix++;
                        });
                    }
                } else {
                    fData.append('esParaEquipo', false);
                }
                fData.append('sobrescribir', $('#sobrescribir').is(':checked'));
                         
                $.ajax({
                    url: datos.url,
                    data: fData,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data != null) {
                            if (data.Info == "Login") {
                                mostrarFinSesion();
                                return;
                            }
                            if (data.Info == "ok") {
                                mostrarExito('Trabajo cargado!');
                                $('#Trabajos').dialog('close');
                                actualizarPaneles('todos');
                            }
                            else {
                                mostrarError(data.Detail);
                            }
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        mostrarError(thrownError);
                    },
                    complete: function () {
                        $(".ajaxGif").css({ 'display': 'none' });
                    }
                });
            },
            Cancelar: function () {
                $(this).dialog('close');
            }
        }
    });//-->TRABAJOS    
});//READY

function incidenteToLife() {
    var exito = false;
    if (validarNoVacio($('#tIncToLive'))) {
        var nroInc = $('#tIncToLive').val();
        $.ajax({
            url: "Incidentes/incidentesToLife",
            type: 'POST',
            data: 'nroInc=' + nroInc,
            beforeSend: ajaxReady = false,
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == 'ok') {
                    exito = true;
                    mostrarExito('El Incidente ' + nroInc + ' fue reactivado');
                    $('#tIncToLive').val('');
                }
                else mostrarError(data.Detail);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
                if (exito) actualizarPaneles('usuarios');
            }
        });
    }
}
function searchAutocomplete(request, response) {
    var texto = request.term;
    texto = texto.toUpperCase();
    var buscoEquipo = false;
    var buscoIncidente = false;
    if (regEquipoOperaciones.test(texto)) {
        buscoEquipo = true;
    }
    if (regularIncidente.test(texto)) {
        buscoIncidente = true;
    }
    var uri;
    if (buscoEquipo) uri = 'GetEquiposActivos';
    else uri = 'GetIncidentesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSession();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
function notificar() {

}
function derivar() {
    var nro = [];
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var context = $(this).parents('ul.fly');
    var origen = grilla.attr('id');
    var idArea = $('.sArea', context).val();
    var textArea = $('.sArea option:selected', context).text();
    var idTecnico = $('.sTecnico', context).val();
    var fData = new FormData();

    var exito = false;
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN Incidente");
    }
    else {
        var id_inc = nro[0].find('td:nth-child(2)').text();
        var nroInc = nro[0].find('td:nth-child(3)').text();
        fData.append("idAreaDestino", idArea);
        fData.append("idTecnico", idTecnico);
        fData.append("idIncidente", id_inc);
        if (window.ajaxReady) {
            $.ajax({
                url: "Incidentes/derivarIncidente",
                data: fData,
                type: 'POST',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    window.ajaxReady = false;
                },
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    if (data.Info == "ok") {
                        mostrarExito('Se ha derivado el Incidente a ' + textArea);
                        exito = true;
                    }
                    else {
                        mostrarError(data.Detail);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    mostrarError(thrownError);
                },
                complete: function () {
                    window.ajaxReady = true;
                    if (exito) {
                        actualizarPaneles("todos");
                        $('p.ajaxGif').hide();
                    }
                }
            });
        }
    }
}
function cargarTrabajos() {
    var t = $(this);
    var nro = [];
    var trs = {};
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("Seleccione UN elemento");
    }
    else {
        var datos = nro[0].find('td').slice(1, 7);
        var idIncidente = datos[0].innerHTML;
        var nroIncidente = $(datos[1]).find('span').text();
        var data = {};
        data.idIncidente = idIncidente;
        data.nroIncidente = nroIncidente;
        data.nombreGrilla = nombreGrilla;
        $('#Trabajos').data('datos', data).dialog('open');
    }
}
function derivarEq() {

}
function cerrarIncidente() {
    var nro = [];
    var exito = false;
    $(this).parents('.botones').siblings('.grilla').find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0 || nro.length > 1) {
        mostrarError("debe seleccionar UN incidente");
    }
    else {
        var datoss = nro[0].find('td').slice(1, 9);
        var trsel = $.makeArray(datoss);

        if (ajaxReady) {
            var divdialog = $('<div>').append("Se va a cerrar el incidente " + $(trsel[1]).text() + ". Está Seguro?").dialog({
                width: '250px',
                show: 'slide',
                //position: 'center',
                open: function () {
                    $(this).parents('.ui-dialog').css('font-size', '.9em').find('div.ui-dialog-titlebar').hide();
                },
                buttons: {
                    Aceptar: function () {
                        $.ajax({
                            url: "Incidentes/solucionarIncidente",
                            data: { idInc: $(trsel[0]).text() },
                            type: 'POST',
                            beforeSend: function () {
                                ajaxReady = false;
                            },
                            success: function (data) {
                                if (data.Info == "Login") {
                                    mostrarFinSesion();
                                    return;
                                }
                                if (data.Info == "ok") {
                                    mostrarInfo('Se ha Cerrado el Incidente');
                                    exito = true;
                                }
                                else {
                                    mostrarError(data.Detail);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                mostrarError(thrownError);
                            },
                            complete: function () {
                                ajaxReady = true;
                                $(divdialog).dialog('destroy');
                                if (exito) {
                                    actualizarPaneles('todos');
                                }
                            }
                        });               //fin ajax
                    },
                    Cancelar: function () { $(this).dialog('destroy') }
                }

            });


        } //fin ajaxready
    }
}
function terminarEquipo() {

}
function asignarEquipo() {
    var exito = false;
    var nro = [];
    var User = $(this).parent().siblings().find('select.sTecnico option:selected').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));
        }
        asignarTecnicoEnlace(id_enlace, User);
    }
}
function tomarEquipo() {
    var exito = false;
    var nro = [];
    var User = $('#user').text();
    var grilla = $(this).parents('.botones').siblings('.grilla');
    var nombreGrilla = grilla.attr('id');
    grilla.find('tr').has('td :checked').each(function (i) {
        var val = $(this);
        nro.push(val);
    });
    if (nro.length == 0) {
        mostrarError("Debe seleccionar al menos UN Enlace");
        return;
    }
    else {
        var id_enlace = [];
        for (i = 0; i < nro.length; i++) {
            id_enlace.push($.trim(nro[i].find('td:nth-child(5)').text()));
        }
    }
    asignarTecnicoEnlace(id_enlace, User);
}
function actualizarPaneles(tabla) {
    if (window.ajaxReady) {
        $.ajax({
            url: "Incidentes",
            type: 'GET',
            beforeSend: function () {
                window.ajaxReady = false;
            },
            success: function (data) {
                var ventanas = window.ventanas;
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                var results = new Array();
                results[0] = $(data).find('#grillaBandeja');
                results[1] = $(data).find('#grillaSeguridad');
                results[2] = $(data).find('#grillaSuspendidos');

                switch (tabla) {
                    case "todos":
                        ventanas[0].update(results[0]);
                        ventanas[1].update(results[1]);
                        ventanas[2].update(results[2]);
                        break;
                    default:;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);

            },
            complete: function () {
                window.ajaxReady = true;
                $('p.ajaxGif').hide();
            }
        });//fin ajax
    }
}
/**Search Autocomplete Cliente**/
function searchAutocompleteCliente(request, response) {
    var texto = request.term;
    var uri = 'GetClientesActivos';
    var term = '{ prefixText: "' + request.term + '" }';
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Incidentes/" + uri,
        data: term,
        dataType: "json",
        success: function (data) {
            if (data.Info == 'Login') {
                mostrarFinSesion();
                return;
            }
            else {
                var suggestions = [];
                $.each(data, function (i, val) {
                    var obj = {};
                    obj.value = val;
                    obj.label = val;
                    suggestions.push(obj);
                })
                response(suggestions);
            }
        }
    });
}
/*FUNCION BUSCAR Cliente*/
function buscarCliente(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];

    if (ajaxReady) {
        $.ajax({
            url: "Incidentes/incidenteXCliente",
            data: datos,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                ajaxReady = false;
            },
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.length == 0) {
                    mostrarInfo("No hay Incidentes para ese cliente");
                    $('p.ajaxGif').hide();
                }
                else {
                    var area = data.Area;
                    switch (area) {
                        case "Mesa de Ayuda": if (data.Estado == "Ingresado") doScroll(ventanas[0], data.Numero);
                        else {
                            if (data.Estado == "Suspendido") doScroll(ventanas[1], data.Numero);
                            else { }
                        }
                            break;
                        default: doScroll(ventanas[2], data.Numero);
                            break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                mostrarError(thrownError);
            },
            complete: function () {
                ajaxReady = true;
            }
        });                          //fin ajax
    } //fin ajaxready
}
function ubicarIncidente(data) {
    var direccion = data.SubDireccion;    
    if (direccion == "Seguridad Informática") {
        var area = data.Area;
        if (data.Estado == "Suspendido") {
            doScroll(ventanas[2], data.Numero);
        } else {
            switch (area) {
                case "Entrada Seguridad": doScroll(ventanas[0], data.Numero);
                    break;
                case "Seguridad": doScroll(ventanas[1], data.Numero);
                    break;
            }
        }
    } else {
        mostrarInfo("El Ticket se encuentra en " + direccion);
    }
}
function buscarEquiposInc(datos) {
    var strObj = new String(datos);
    var arr = strObj.split('=');
    var resString = arr[1];
    resString = resString.toUpperCase();

    if (!regularIncidente.test(resString)) {
        mostrarError("El texto íngresado no parece válido");
        return;
    }
    $.ajax({
        url: "Incidentes/buscarEqOInc",
        data: datos,
        dataType: 'json',
        type: 'POST',
        beforeSend: function () {
            ajaxReady = false;
        },
        success: function (data) {
            if (data.Info == "Login") {
                mostrarFinSesion();
                return;
            }
            if (data.Area == null) {
                mostrarInfo("El equipo/incidente no está activo");
                $('p.ajaxGif').hide();
            }
            else {
                ubicarIncidente(data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mostrarError(thrownError);
        },
        complete: function () {
            ajaxReady = true;
        }
    });                          //fin ajax

}
function notificarIncidente() { }