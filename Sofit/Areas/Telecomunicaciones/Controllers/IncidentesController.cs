﻿using soporte.Areas.Telecomunicaciones.Models;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Telecomunicaciones.Controllers
{
    [SessionActionFilter]
    public class IncidentesController : Controller
    {
        //
        // GET: /Telecomunicaciones/Incidentes/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/Incidentes");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            TeleModel model = new TeleModel();
            List<IncTeleVista> incTotales = null;
            List<IncTeleVista> incIngresados = null;
            List<IncTeleVista> incDatos = null;
            List<IncTeleVista> incTele = null;
            List<IncTeleVista> incSuspendidos = new List<IncTeleVista>();
            List<TipificacVista> tipificaciones = new List<TipificacVista>();
            List<Jurisdicciones> jurisdicciones = new List<Jurisdicciones>();
            List<Tecnico> tecnicos = new List<Tecnico>();
            List<Area> areas = new List<Area>();
            List<TipoResolucion> tipoResoluciones = new List<TipoResolucion>();
            List<Turno> turnos = new List<Turno>();
            List<DireccionVista> subdir = new List<DireccionVista>();
            TiemposTelecomunicaciones tiempos = new TiemposTelecomunicaciones();
            IDirecciones teleDir = DireccionesFactory.getDireccion("Telecomunicaciones");
            using (var dbc = new IncidenciasEntities())
            {
                incTotales = (from d in dbc.selectIncidenciasActivas(teleDir.IdDireccion)
                              select new IncTeleVista
                              {
                                  IdIncidente = d.id,
                                  Numero = d.numero,
                                  FechaDesde = d.fechaI,
                                  Tipificacion = d.tipificacion,
                                  Dependencia = d.dependencia,
                                  Técnico = d.tecnico,
                                  Descripcion = d.observaciones,
                                  Imagen = d.imagen.Value,
                                  Archivo = d.archivo.Value,
                                  idArea = d.idarea,
                                  Servicio = d.nombreServicio,
                                  TiempServicio = TimeSpan.FromHours((double)d.tServ),
                                  sTolerance = d.tolerance,
                                  Prioridad = d.prioridad,
                                  Cliente = d.Cliente,
                                  Estado = d.Estado,
                                  TieneNotificacion = d.noti.Value,
                                  FueSuspendido = d.fuesuspendido.Value
                              }).ToList();
                incIngresados = incTotales.Where(c => c.Area.NombreArea == "Telecomunicaciones" & c.Estado != "Suspendido").ToList();
                incDatos = incTotales.Where(c => c.Area.NombreArea == "Datos" & c.Estado != "Suspendido").ToList();
                incTele = incTotales.Where(c => c.Area.NombreArea == "Telefonía" & c.Estado != "Suspendido").ToList();
                incSuspendidos = incTotales.Where(c => c.Estado == "Suspendido").ToList();
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  orderby u.descripcion
                                  select new Jurisdicciones { 
                                     idJurisdiccion=u.id_jurisdiccion,
                                     Nombre=u.descripcion
                                  }).ToList();
                areas = (from u in dbc.area
                         where u.Direcciones.nombre == "Telecomunicaciones"
                         select new Area
                         {
                             idArea=u.id_area,
                             Nombre=u.descripcion                             
                         }).ToList();
                tecnicos = (from u in dbc.tecnico
                            where u.area.Direcciones.nombre == "Telecomunicaciones" & u.activo.Value
                            select new Tecnico
                            {
                                idTecnico=u.id_tecnico,
                                Nombre=u.descripcion
                            }).ToList();
                turnos=(from u in dbc.turno
                        select new Turno{
                            idTurno=u.id_turno,
                            Nombre=u.descripcion
                        }).ToList();
                tipoResoluciones = (from u in dbc.tipo_resolucion
                                    where u.area.Direcciones.nombre == "Telecomunicaciones"
                                    orderby u.descripcion
                                    select new TipoResolucion
                                    {
                                        idTipoResolucion=u.id_tipo_resolucion,
                                        Nombre=u.descripcion
                                    }).ToList();
                subdir = (from u in dbc.Direcciones
                          select new DireccionVista
                          {
                               idDireccion=u.id_direccion,
                                Nombre=u.nombre
                          }).ToList();
            }
            foreach (IncTeleVista t in incSuspendidos)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incIngresados)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incTele)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            foreach (IncTeleVista t in incDatos)
            {
                t.calcularEstadoServicio(tiempos.tCriticoIncidente);
            }
            BuilderTipifTree arbol = new BuilderTipifTree();
            arbol.construir("Telecomunicaciones");
            model.IncidentesIngresados = incIngresados;
            model.TipoResolucion = tipoResoluciones;
            model.Turnos = turnos;
            model.Areas = areas;
            model.Tecnicos = tecnicos;
            model.IncidentesDatos = incDatos;
            model.IncidentesTele = incTele;
            model.IncidentesSuspendidos = incSuspendidos;
            model.Tipificaciones = arbol.getResult();
            model.Jurisdicciones = jurisdicciones;
            model.SubDirecciones = subdir;
            return View(model);
        }
        public JsonResult nuevoIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            string nroIncidente = nvc.Get("tIncidente");
            string idTipificacion = nvc.Get("idTipif");
            string idDep = nvc.Get("idDep");
            string observaciones = nvc.Get("observaciones");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciasManager manager = new IncidenciasManager();
            //result=manager.crearIncidencia(nroIncidente, Int32.Parse(idTipificacion), Int32.Parse(idDep),idUsuario,observaciones);
            return Json(result);
        }
        public JsonResult cargaTrabajo(HttpPostedFileBase imagen)
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idTecnico = Int32.Parse(nvc.Get("idTecnico"));
            int idTipoResolucion = Int32.Parse(nvc.Get("idTipoResolucion"));
            int idTurno = Int32.Parse(nvc.Get("idTurno"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            string observacion = nvc.Get("observaciones");            
            string area = nvc.Get("area");
            //sobrescribir ultima
            bool sobrescribir = false;
            sobrescribir = Boolean.Parse(nvc.Get("sobrescribir"));
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, sobrescribir, false, false);
            //
            Incidencia inc = builder.getResult();
            bool hayImagen = imagen != null;
            string nombreFile = string.Empty;            
            #region foto
            if (hayImagen)
            {
                string pathImagen = string.Empty;
                string nombreArchivo = imagen.FileName;
                string[] splitN = nombreArchivo.Split('.');
                string ran = new Random().Next(10000).ToString("0000");
                nombreFile = inc.Numero + splitN.First() + ran + '.' + splitN.Last();
                try
                {
                    pathImagen = PathImage.getResolucionCustom(nombreFile);
                    var stream = imagen.InputStream;
                    using (var fileStream = System.IO.File.Create(pathImagen))
                    {
                        stream.CopyTo(fileStream);
                    }
                    result.Info = "ok";
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            #endregion            
            //buscar equipos                        
            bool flag = true;
            List<int> idEquipos = new List<int>();
            bool esParaEquipo = Boolean.Parse(nvc.Get("esParaEquipo"));
            if (esParaEquipo)
            {
                foreach (string key in nvc.Keys)
                {
                    if (key.StartsWith("idEquipo")) idEquipos.Add(Int32.Parse(nvc.GetValues(key)[0]));
                }
                if (idEquipos.Count == 0)
                {
                    result.Info = "error";
                    result.Detail = "No se cargaron las obleas del equipo.";
                    flag = false;
                }
                if (flag)
                {
                    foreach (int id in idEquipos)
                    {
                        inc.cargarSolucion(idUsuario, id, idTipoResolucion, idTecnico, idTurno, observacion, DateTime.Now, nombreFile, sobrescribir);
                    }
                    result.Info = "ok";
                }
            }
            else
            {
                result = inc.cargarSolucion(idUsuario, null, idTipoResolucion, idTecnico, idTurno, observacion, DateTime.Now, nombreFile, sobrescribir);
            }
            return Json(result);
        }
        public JsonResult suspenderIncidentes(int idInc)
        {
            Responses result = new Responses();
            int idUsuarioActual = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idInc, true,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.suspenderIncidencia(idUsuarioActual);
            return Json(result);
        }
        public JsonResult derivarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            int idIncidencia = Int32.Parse(nvc.Get("idIncidente"));
            int idAreaDestino = Int32.Parse(nvc.Get("idAreaDestino"));
            int idTecnico=Int32.Parse(nvc.Get("idTecnico"));
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidencia, true,false,false);
            Incidencia incidencia = builder.getResult();
            result = incidencia.derivar(idAreaDestino, idTecnico, idUsuario);            
            return Json(result);
        }
        public JsonResult derivarADireccion()
        {
            NameValueCollection nvc = Request.Form;
            Responses result = new Responses();

            if (nvc.Count > 1)
            {
                usuarioBean user = Session["usuario"] as usuarioBean;
                int idDireccion = Int32.Parse(nvc.Get("idDir"));
                int id_inc = Int32.Parse(nvc.Get("idInc"));
                IncidenciaBuilder builder = new IncidenciaBuilder();
                builder.Construir(id_inc, true, false, false);
                Incidencia incidencia = builder.getResult();
                result = incidencia.derivarIncidenciaADireccion(idDireccion, user.IdUsuario);
            }

            return Json(result);
        }
        public JsonResult asignarIncidente()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            string[] idIncidencia = nvc.Get("id_inc").Split(',');
            string Tecnico = nvc.Get("tecnico");
            bool accessgranted = false;
            if (Tecnico == usuarioActual.NombreCompleto) accessgranted = true;
            else
            {
                List<Perfil> perfiles = usuarioActual.getPerfiles();
                foreach (Perfil p in perfiles)
                {
                    if (p is PerfilAdministradorTele)
                    {
                        accessgranted = true;
                        break;
                    }
                }
            }
            if (accessgranted)
            {
                
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var tecnico = (from u in dbc.tecnico
                                       where u.descripcion == Tecnico
                                       select u).SingleOrDefault();
                        foreach (string strId in idIncidencia)
                        {
                            int idIncidente = Int32.Parse(strId);
                            var incActiva = dbc.IncidenciasActivas.Where(c => c.id_incidencia == idIncidente).FirstOrDefault();
                            incActiva.id_tecnico = tecnico.id_tecnico;
                        }
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            else
            {
                result.Detail = "Su perfil no permite realizar esta tarea.";
            }
            return Json(result);
        }
        public JsonResult cerrarIncidente(int idIncidente)
        {
            Responses result = new Responses();
            IncidenciaBuilder builder = new IncidenciaBuilder();
            builder.Construir(idIncidente,true,true,false);
            Incidencia incidencia = builder.getResult();
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            result=incidencia.cerrarIncidencia(idUsuario);
            return Json(result);
        }
        public JsonResult incidentesToLife(string nroInc)
        {
            IncidenciasManager im = new IncidenciasManager();
            IDirecciones soporte = DireccionesFactory.getDireccion("Telecomunicaciones");
            int idUsuario = (Session["usuario"] as usuarioBean).IdUsuario;
            return Json(im.reanimarIncidencia(nroInc, idUsuario, soporte));
        }
        public JsonResult GetEquiposActivos(string prefixText)
        {
            List<String> items = new List<String>();
            
            return Json(items);
        }
        public ActionResult getExcel(string win)
        {
            switch (win)
            {
                case "IncTele":
                    using (var dbc = new IncidenciasEntities())
                    {
                        var incTele = (from us in dbc.IncidenciasActivas
                                       where us.area.descripcion == "Telefonía"
                                       select new IncTeleVista
                                       {                                           
                                           Numero = us.Incidencias.numero,
                                           FechaDesde = us.fecha_inicio,
                                           Tipificacion = us.Incidencias.Tipificaciones.nombre,
                                           Dependencia = us.Incidencias.Dependencia.descripcion,
                                           Técnico = us.tecnico.descripcion
                                       });
                        DataTable table = DataTableConverter.ToDataTable(incTele);
                        Session["tableSource"] = table;
                        
                    }
                    break;
                case "IncDatos": using (var dbc = new IncidenciasEntities())
                    {
                        var incTele = (from us in dbc.IncidenciasActivas
                                       where us.area.descripcion == "Datos"
                                       select new IncTeleVista
                                       {                                           
                                           Numero = us.Incidencias.numero,
                                           FechaDesde = us.fecha_inicio,
                                           Tipificacion = us.Incidencias.Tipificaciones.nombre,
                                           Dependencia = us.Incidencias.Dependencia.descripcion,
                                           Técnico = us.tecnico.descripcion
                                       });
                        DataTable table = DataTableConverter.ToDataTable(incTele);
                        Session["tableSource"] = table;

                    }
                    break;
            }
            return RedirectToRoute("Excel");
        }
        public JsonResult GetIncidentesActivos(string prefixText)
        {            
            List<String> items = new List<String>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.IncidenciasActivas
                         where u.area.Direcciones.nombre == "Telecomunicaciones" & u.Incidencias.numero.StartsWith(prefixText)
                         select u.Incidencias.numero).Take<string>(7).ToList();
            }
            return Json(items);   
        }
        public JsonResult UbicacionIncidente(string texto)
        {
            DatosUbicacion ubic = new DatosUbicacion();
            using (var dbc = new IncidenciasEntities())
            {
                var dd = (from u in dbc.IncidenciasActivas
                          where u.Incidencias.numero == texto//& u.area.Direcciones.nombre == "Telecomunicaciones"
                          select u).FirstOrDefault();
                if (dd != null)
                {
                    ubic.Estado = dd.Estado_incidente.descripcion;
                    ubic.Area = dd.area.descripcion;
                    ubic.Numero = dd.Incidencias.numero;
                    ubic.SubDireccion = dd.area.Direcciones.nombre;
                }
            }
            return Json(ubic);
        }
    }
}
