﻿using soporte.Areas.Telecomunicaciones.Models.ClasesVistas;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Telecomunicaciones.Controllers
{
    [SessionActionFilter]
    //[SessionActionFilter]
    public class InventarioController : Controller
    {        
        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/Inventario");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }
            if (acceso == Constantes.AccesosPagina.ReadOnly)
            {
                ViewBag.acceso = "readonly";
            }
            if(acceso==Constantes.AccesosPagina.Full)
            {
                ViewBag.acceso = "full";
            }
            return View();
        }
        public JsonResult EquipoXOblea(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoTele
                         where u.UnidadConf.nombre.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre                             
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXNombreComun(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();            
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoTele
                         where u.nombre_comun.StartsWith(prefixText)
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             NombreComun=u.nombre_comun
                         }).Take(10).ToList();
            }
            return Json(items);
        }
        public JsonResult EquipoXIp(string prefixText)
        {
            List<EquipoComplete> items = new List<EquipoComplete>();
            using (var dbc = new IncidenciasEntities())
            {
                items = (from u in dbc.EquipoTele
                         where u.Ip.Any(c=>c.ip_v4.StartsWith(prefixText))
                         select new EquipoComplete
                         {
                             idEquipo = u.UnidadConf.id_uc,
                             Oblea = u.UnidadConf.nombre,
                             Ip = u.Ip.Select(c => new IpVista { IpV4=c.ip_v4})
                         }).Take(10).ToList();
                
            }
            return Json(items);
        }
        public JsonResult Update()
        {
            Responses result = new Responses();
            NameValueCollection nvc = Request.Form;
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            try
            {
                int idEquipo = Int32.Parse(nvc.Get("idEquipo"));
                string Oblea = nvc.Get("Oblea");
                string NroSerie = nvc.Get("NroSerie");
                int idTipo = Int32.Parse(nvc.Get("idTipo"));
                int idMarca = Int32.Parse(nvc.Get("Marca"));
                string Modelo = nvc.Get("Modelo");
                int idEstado = Int32.Parse(nvc.Get("Estado"));
                int idDependencia = Int16.Parse(nvc.Get("Ubicacion"));
                int idJurisdiccion = Int16.Parse(nvc.Get("Jurisdiccion"));
                string nombreComun = nvc.Get("NComun");
                string Observaciones = nvc.Get("Observaciones");
                string UsuarioAcceso1 = nvc.Get("UserAcc1");
                string PasswordAcceso1 = nvc.Get("UserAccPass1");
                string UsuarioAcceso2 = nvc.Get("UserAcc2");
                string PasswordAcceso2 = nvc.Get("UserAccPass2");
                string UsuarioAcceso3 = nvc.Get("UserAcc3");
                string PasswordAcceso3 = nvc.Get("UserAccPass3");
                string SSID = nvc.Get("SSID");
                string SSIDP = nvc.Get("SSIDPass");
                string ipv4 = nvc.Get("IPV4");
                string ipv6 = nvc.Get("IPV6");
                string mac = nvc.Get("MAC");
                string subnet = nvc.Get("SubNet");
                string gw = nvc.Get("Gateway");
                string iploop = nvc.Get("IPLoop");
                string ipwan = nvc.Get("IPWAN");
                string ipgestion = nvc.Get("IPGestion");
                string ports = nvc.Get("Ports");
                string sfpser = nvc.Get("SfpSerie");
                string sfpmac = nvc.Get("SfpMac");
                string sfptipo = nvc.Get("SfpTipo");
                string snm = nvc.Get("Snmp");
                string snmpVer = nvc.Get("SnmpVer");
                string encriptacion = nvc.Get("Encriptacion");
                using (var dbc = new IncidenciasEntities())
                {
                    var equipo = (from u in dbc.EquipoTele where u.UnidadConf.id_uc == idEquipo select u).Single();
                    if (equipo != null)
                    {
                        equipo.UnidadConf.id_tipo = idTipo;
                        equipo.UnidadConf.id_usuario = usuarioActual.IdUsuario;
                        equipo.UnidadConf.id_jurisdiccion = idJurisdiccion;
                        equipo.UnidadConf.observaciones = Observaciones;
                        equipo.UnidadConf.nro_serie = NroSerie;
                        equipo.id_marca = idMarca;
                        equipo.modelo = Modelo;
                        equipo.id_dependencia = idDependencia;
                        equipo.nombre_comun = nombreComun;
                        equipo.usuario1 = UsuarioAcceso1;
                        equipo.pass_us1 = PasswordAcceso1;
                        equipo.usuario2 = UsuarioAcceso2;
                        equipo.pass_us2 = PasswordAcceso2;
                        equipo.usuario3 = UsuarioAcceso3;
                        equipo.pass_us3 = PasswordAcceso3;
                        equipo.ssid = SSID;
                        equipo.ssidPass = SSIDP;
                        equipo.puertos = ports;
                        equipo.sfpSerie = sfpser;
                        equipo.tipoSfp = sfptipo;
                        equipo.macSfp = sfpmac;
                        equipo.snmp = snm;
                        equipo.snmpVer = snmpVer;
                        equipo.id_estado = idEstado;
                        equipo.encript = encriptacion;
                    };
                    if (ipv4 != string.Empty)
                    {
                        Ip newip = new Ip
                        {
                            ip_v4 = ipv4,
                            ip_v6 = ipv6,
                            mac = mac,
                            gateway = gw,
                            mascara = subnet,
                            ipLoopback = iploop,
                            ipWan = ipwan,
                            ipGestion = ipgestion
                        };
                        equipo.Ip.Clear();
                        equipo.Ip.Add(newip);                        
                    }
                    else
                    {
                        equipo.Ip.Clear();
                    }
                    //actualización gestión de cambios
                    var ecx = dbc.EcxGc.Where(c => c.id_uc == idEquipo & c.fecha_actualizacion == null);
                    if (ecx.Count() > 0)
                    {
                        var exfir = ecx.First();
                        if (exfir.GestionCambios.id_autorizante == usuarioActual.IdUsuario)
                        {
                            ecx.First().fecha_actualizacion = DateTime.Now;
                        }
                    }
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }            
            return Json(result);
        }
        public JsonResult Nuevo()
        {
            Responses result = new Responses();
            usuarioBean usuarioActual = Session["usuario"] as usuarioBean;
            NameValueCollection nvc = Request.Form;
            try
            {
                string Oblea = nvc.Get("Oblea");
                string NroSerie = nvc.Get("NroSerie");
                int idTipo = Int32.Parse(nvc.Get("idTipo"));
                int idMarca = Int32.Parse(nvc.Get("Marca"));
                string Modelo = nvc.Get("Modelo");
                int idDependencia = Int16.Parse(nvc.Get("Ubicacion"));
                int idJurisdiccion = Int16.Parse(nvc.Get("Jurisdiccion"));
                string nombreComun = nvc.Get("NComun");
                string Observaciones = nvc.Get("Observaciones");
                string UsuarioAcceso1 = nvc.Get("UserAcc1");
                string PasswordAcceso1 = nvc.Get("UserAccPass1");
                string UsuarioAcceso2 = nvc.Get("UserAcc2");
                string PasswordAcceso2 = nvc.Get("UserAccPass2");
                string UsuarioAcceso3 = nvc.Get("UserAcc3");
                string PasswordAcceso3 = nvc.Get("UserAccPass3");
                string SSID = nvc.Get("SSID");
                string SSIDP = nvc.Get("SSIDPass");
                string ipv4 = nvc.Get("IPV4");
                string ipv6 = nvc.Get("IPV6");
                string mac = nvc.Get("MAC");
                string subnet = nvc.Get("SubNet");
                string gw = nvc.Get("Gateway");
                string iploop = nvc.Get("IPLoop");
                string ipwan = nvc.Get("IPWAN");
                string ipgestion = nvc.Get("IPGestion");
                string ports = nvc.Get("Ports");
                string sfpser = nvc.Get("SfpSerie");
                string sfpmac = nvc.Get("SfpMac");
                string sfptipo = nvc.Get("SfpTipo");
                string snm = nvc.Get("Snmp");
                string snmpVer = nvc.Get("SnmpVer");
                string encriptacion = nvc.Get("Encriptacion");
                using (var dbc = new IncidenciasEntities())
                {
                    var checking = dbc.EquipoTele.Where(c => c.UnidadConf.nombre == Oblea);
                    if (checking.Count() == 0)
                    {
                        UnidadConf e = new UnidadConf
                        {
                            nombre = Oblea,
                            id_tipo = idTipo,
                            id_usuario = usuarioActual.IdUsuario,
                            id_jurisdiccion = idJurisdiccion,
                            observaciones = Observaciones,
                            fecha = DateTime.Now,
                            nro_serie = NroSerie
                        };
                        dbc.UnidadConf.Add(e);
                        EquipoTele et = new EquipoTele
                        {
                            id_uc = e.id_uc,
                            id_marca = idMarca,
                            modelo = Modelo,
                            id_dependencia = idDependencia,
                            nombre_comun=nombreComun,
                            usuario1 = UsuarioAcceso1,
                            pass_us1 = PasswordAcceso1,
                            usuario2 = UsuarioAcceso2,
                            pass_us2 = PasswordAcceso2,
                            usuario3 = UsuarioAcceso3,
                            pass_us3 = PasswordAcceso3,
                            ssid = SSID,
                            ssidPass = SSIDP,
                            puertos = ports,
                            sfpSerie = sfpser,
                            macSfp = sfpmac,
                            tipoSfp = sfptipo,
                            snmp = snm,
                            snmpVer = snmpVer,
                            encript = encriptacion,
                            id_estado = (from u in dbc.Estado_UC where u.descripcion == "Activo" select u.id_estado).Single()
                        };
                        if (ipv4 != string.Empty)
                        {
                            Ip newip = new Ip
                            {
                                ip_v4 = ipv4,
                                ip_v6 = ipv6,
                                mac = mac,
                                gateway = gw,
                                mascara = subnet,
                                ipLoopback = iploop,
                                ipWan = ipwan,
                                ipGestion = ipgestion
                            };

                            et.Ip.Add(newip);
                        }
                        dbc.EquipoTele.Add(et);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else
                    {

                        result.Detail = "El equipo ya existe";
                    }
                }
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }

            return Json(result);
        }
        public JsonResult GetEquipoTele(string oblea)
        {
            Responses result = new Responses();
            EquipoTelecomunicaciones equipo = new EquipoTelecomunicaciones();
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.EquipoTele
                              where u.UnidadConf.nombre == oblea
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    return Json(GetEquipo(eqBase));
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
        }
        public JsonResult GetEquipoSerie(string serie)
        {
            Responses result = new Responses();

            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.EquipoTele
                              where u.UnidadConf.nro_serie == serie
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    return Json(GetEquipo(eqBase));
                }
                else
                {
                    result.Detail = "No se encontró el equipo";
                    return Json(result);
                }
            }
        }
        private EquipoTelecomunicaciones GetEquipo(EquipoTele eqBase)
        {
            EquipoTelecomunicaciones equipo = new EquipoTelecomunicaciones();
            equipo.Dependencia = new DependenciaVista
            {
                idDependencia = eqBase.id_dependencia.Value,
                Nombre = eqBase.Dependencia.descripcion,
                Jurisdiccion = new JurisdiccionVista
                {
                    idJurisdiccion = eqBase.Dependencia.Jurisdiccion.id_jurisdiccion,
                    Nombre = eqBase.Dependencia.Jurisdiccion.descripcion
                }
            };
            equipo.NComun = eqBase.nombre_comun;
            equipo.Estado = new EstadoEquipoVista
            {
                idEstado = eqBase.Estado_UC.id_estado,
                Nombre = eqBase.Estado_UC.descripcion
            };
            equipo.idEquipo = eqBase.UnidadConf.id_uc;
            equipo.TipoEquipo = new TiposEquipoVista
            {
                id = eqBase.UnidadConf.id_tipo,
                Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion
            };
            if (eqBase.Ip.Count > 0)
            {
                Ip ipbase = eqBase.Ip.Single();
                equipo.IP = new IpVista
                {
                    Dns = ipbase.dns,
                    Gateway = ipbase.gateway,
                    IpV4 = ipbase.ip_v4,
                    IpV6 = ipbase.ip_v6,
                    Mascara = ipbase.mascara,
                    Wins = ipbase.wins,
                    IpGestion = ipbase.ipGestion,
                    IpLoopback = ipbase.ipLoopback,
                    IpWan = ipbase.ipWan,
                    Mac = ipbase.mac
                };
            }
            equipo.Marca = new MarcaVista
            {
                idMarca = eqBase.id_marca.Value,
                Nombre = eqBase.Marca.descripcion
            };
            equipo.Modelo = eqBase.modelo;
            equipo.NroSerie = eqBase.UnidadConf.nro_serie;
            equipo.Oblea = eqBase.UnidadConf.nombre;
            equipo.Observaciones = eqBase.UnidadConf.observaciones;
            equipo.TipoEquipo.id = eqBase.UnidadConf.Tipo_equipo.id_tipo;
            equipo.TipoEquipo.Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion;
            equipo.Puertos = eqBase.puertos;
            equipo.SSID = eqBase.ssid;
            equipo.SSIDPassword = eqBase.ssidPass;
            equipo.Usuario1 = eqBase.usuario1;
            equipo.Password1 = eqBase.pass_us1;
            equipo.Usuario2 = eqBase.usuario2;
            equipo.Password2 = eqBase.pass_us2;
            equipo.Usuario3 = eqBase.usuario3;
            equipo.Password3 = eqBase.pass_us3;
            equipo.Encriptacion = eqBase.encript;
            equipo.Puertos = eqBase.puertos;
            equipo.SfpSerie = eqBase.sfpSerie;
            equipo.MacSfp = eqBase.macSfp;
            equipo.TipoSfp = eqBase.tipoSfp;
            equipo.Snmp = eqBase.snmp;
            equipo.SnmpVer = eqBase.snmpVer;
            return equipo;
        }
        public JsonResult Jurisdicciones()
        {
            List<JurisdiccionVista> jurisdicciones = new List<JurisdiccionVista>();
            using (var dbc = new IncidenciasEntities())
            {
                jurisdicciones = (from u in dbc.Jurisdiccion
                                  select new JurisdiccionVista
                                  {
                                      idJurisdiccion = u.id_jurisdiccion,
                                      Nombre = u.descripcion
                                  }).ToList();
            }
            return Json(jurisdicciones);
        }
        public JsonResult Dependencias(int idJur)
        {
            List<DependenciaVista> dependencias = new List<DependenciaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                dependencias = (from u in dbc.Dependencia
                                where u.id_jurisdiccion == idJur
                                select new DependenciaVista
                                {
                                    idDependencia = u.id_dependencia,
                                    Nombre = u.descripcion
                                }).ToList();
            }
            return Json(dependencias);
        }
        public JsonResult EstadosEquipo()
        {
            List<EstadoEquipoVista> estados = new List<EstadoEquipoVista>();
            using (var dbc = new IncidenciasEntities())
            {
                estados = (from u in dbc.Estado_UC
                           select new EstadoEquipoVista
                           {
                               idEstado = u.id_estado,
                               Nombre = u.descripcion
                           }).ToList();
            }
            return Json(estados);
        }
        public JsonResult MarcasTelecomunicaciones()
        {
            List<MarcaVista> marcas = new List<MarcaVista>();
            using (var dbc = new IncidenciasEntities())
            {
                marcas = (from u in dbc.Marca
                          where u.Direcciones.nombre == "Telecomunicaciones"
                          select new MarcaVista
                          {
                              idMarca = u.id_marca,
                              Nombre = u.descripcion
                          }).ToList();
            }
            return Json(marcas);
        }
        public JsonResult NuevaMarcaTele(string nombre, int idTipo)
        {
            Responses result = new Responses();
            if (nombre.Length == 0)
            {
                result.Detail = "El campo no puede estar vacío";
            }
            else
            {
                try
                {
                    using (var dbc = new IncidenciasEntities())
                    {
                        var found = (from u in dbc.Marca
                                     where u.descripcion == nombre && u.Direcciones.nombre == "Telecomunicaciones"
                                     select u).SingleOrDefault();
                        if (found != null)
                        {
                            result.Detail = "La marca ya existe";
                            return Json(result);
                        }
                        else
                        {
                            Marca m = new Marca
                            {
                                descripcion = nombre,
                                dispositivo = idTipo,
                                id_direccion = (from u in dbc.Direcciones where u.nombre == "Telecomunicaciones" select u.id_direccion).SingleOrDefault()
                            };
                            dbc.Marca.Add(m);
                            dbc.SaveChanges();
                            result.Info = "ok";
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Detail = e.Message;
                }
            }
            return Json(result);
        }
        public JsonResult BaseLine(int id)
        {
            List<LineaBaseVista> lineaB = new List<LineaBaseVista>();
            using (var dbc = new IncidenciasEntities())
            {
                var basel = from u in dbc.BaseLines
                             where u.master_id == id
                             orderby u.fecha descending
                             select u;
                foreach(var b in basel)
                {
                    lineaB.Add(new LineaBaseVista
                    {
                        CreadoPor = b.Usuarios.nombre_completo,
                        Data = b.data,
                        Fecha = b.fecha
                    });
                }
            }
            return Json(lineaB);
        }
        public JsonResult SaveBaseLine(int id)
        {
            Responses result = new Responses();
            EquipoTelecomunicaciones equipo = null;
            using (var dbc = new IncidenciasEntities())
            {
                var eqBase = (from u in dbc.EquipoTele
                              where u.UnidadConf.id_uc == id
                              select u).SingleOrDefault();
                if (eqBase != null)
                {
                    equipo = GetEquipo(eqBase);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            if (equipo != null)
            {
                result = equipo.SaveLineaBase();
            }
            return Json(result);
        }

        public JsonResult NuevoTeleBatch(DispTelecomunicaciones e)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    int idusuario = 1;
                    int idJurisdiccion = 0;
                    int idDependencia = 0;
                    int idMarca = 0;
                    string tipo = string.Empty;
                    int idTipo = 0;
                    Tipo_equipo tipoEquipo = null;
                    string oblea = string.Empty;
                    switch (e.Tipo)
                    {
                        case "APO": tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Access Point").Single();
                            oblea += "APO";
                            break;
                        case "CAM": tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Cámara IP").Single();
                            oblea += "CAM";
                            break;
                        case "SWI": tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Switches").Single();
                            oblea += "SWI";
                            break;
                        case "CTE": tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Central Telefónica").Single();
                            oblea += "CTE";
                            break;
                        case "RTR": tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Router").Single();
                            oblea += "RTR";
                            break;
                        default: tipoEquipo = dbc.Tipo_equipo.Where(c => c.descripcion == "Switches").Single();
                            oblea += "SWI";
                            break;
                    }
                    idTipo = tipoEquipo.id_tipo;
                    tipo = tipoEquipo.descripcion;
                    int nroOblea = 0;
                    var ultimonro = dbc.NrosOblea.Where(c => c.id_dispositivo == idTipo).SingleOrDefault();
                    if (ultimonro != null)
                    {
                        nroOblea = Int32.Parse(ultimonro.ultimonumero);
                        nroOblea++;
                        ultimonro.ultimonumero = nroOblea.ToString();
                        dbc.SaveChanges();
                    }
                    else
                    {
                        nroOblea = 1;
                        NrosOblea nuevo = new NrosOblea
                        {
                            id_dispositivo = idTipo,
                            ultimonumero = nroOblea.ToString()
                        };
                        dbc.NrosOblea.Add(nuevo);
                    }
                    oblea += nroOblea.ToString("D5");
                    if (e.Dependencia != null)
                    {
                        var dep = dbc.Dependencia.Where(c => c.descripcion == e.Dependencia.Nombre).SingleOrDefault();
                        if (dep == null) { idDependencia = 319; idJurisdiccion = 22; }
                        else
                        {
                            idDependencia = dep.id_dependencia;
                            idJurisdiccion = dep.Jurisdiccion.id_jurisdiccion;
                        }
                    }
                    if (e.Marca != null)
                    {
                        var marca = dbc.Marca.Where(c => c.descripcion == e.Marca.Nombre &
                            c.dispositivo == idTipo).SingleOrDefault();
                        if (marca == null)
                        {
                            Marca nuevamarca = new Marca
                            {
                                descripcion = e.Marca.Nombre,
                                dispositivo = idTipo,
                                id_direccion = dbc.Direcciones.Where(f => f.nombre == "Telecomunicaciones").Single().id_direccion
                            };
                            dbc.Marca.Add(nuevamarca);
                            dbc.SaveChanges();
                            idMarca = nuevamarca.id_marca;
                        }
                        else
                        {
                            idMarca = marca.id_marca;
                        }
                    }
                    UnidadConf uc = new UnidadConf
                    {
                        nombre = oblea,
                        id_jurisdiccion = idJurisdiccion,
                        fecha = DateTime.Now,
                        id_tipo = idTipo,
                        observaciones = e.Observaciones,
                        nro_serie = e.NroSerie,
                        id_usuario = idusuario
                    };
                    dbc.UnidadConf.Add(uc);
                    EquipoTele equipo = new EquipoTele();
                    equipo.id_uc = uc.id_uc;
                    equipo.id_marca = idMarca;
                    equipo.modelo = e.Modelo;
                    equipo.nombre_comun = e.NComun;
                    if (e.Ip != null)
                    {
                        Ip ip = new Ip
                        {
                            ip_v4 = e.Ip
                        };
                        equipo.Ip.Add(ip);
                    }
                    equipo.id_dependencia = idDependencia;
                    equipo.id_estado = dbc.Estado_UC.Where(c => c.descripcion == "Productivo" & c.Direcciones.nombre == "Telecomunicaciones").Single().id_estado;
                    dbc.EquipoTele.Add(equipo);
                    dbc.SaveChanges();
                    result.Info = "ok";
                    result.Detail = tipo + " agregado!!!";
                }
            }
            catch (Exception exc)
            {
                result.Detail = exc.Message;
            }
            return Json(result);
        }
    }
}
