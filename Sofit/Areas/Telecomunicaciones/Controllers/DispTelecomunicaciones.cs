﻿using soporte.Models.ClasesVistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace soporte.Areas.Telecomunicaciones.Controllers
{
    public class DispTelecomunicaciones
    {
        public string NroSerie { get; set; }
        public DependenciaVista Dependencia { get; set; }
        public string Observaciones { get; set; }
        public MarcaVista Marca { get; set; }
        public string Modelo { get; set; }
        public string Ip { get; set; }
        public string Tipo { get; set; }
        public string NComun { get; set; }
    }
}
