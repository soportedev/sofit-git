﻿using soporte.Areas.Telecomunicaciones.Models;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Factory;
using soporte.Models.Helpers;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Telecomunicaciones.Controllers
{
    [SessionActionFilter]
    public class ConfigController : Controller
    {        
        usuarioBean usuario = (usuarioBean)System.Web.HttpContext.Current.Session["usuario"];
        //
        // GET: /Telecomunicaciones/Config/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/Config");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }            
            return View();
        }
        public ActionResult Tipificaciones()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/Tipificaciones");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                ModelTipificaciones model = new ModelTipificaciones();
                using (var dbc = new IncidenciasEntities())
                {
                    model.Servicios = (from u in dbc.Servicios
                                       where u.Direcciones.nombre == "Telecomunicaciones"
                                       select new ServiciosVista
                                       {
                                           Id = u.id_servicio,
                                           Nombre = u.nombre,
                                           TiempoHoras = u.tiempo_res.HasValue ? u.tiempo_res.Value : 0,
                                           TiempoMargen = u.tiempo_margen.HasValue ? u.tiempo_margen.Value : 0,
                                           Vigente = u.vigente
                                       }).ToList();
                }
                BuilderTipifTree arbol = new BuilderTipifTree();
                arbol.construir("Telecomunicaciones");
                model.Tipificaciones = arbol.getResult();
                return View(model);
            }
        }        
        
        public JsonResult quitarServicio(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var serv = dbc.Servicios.Where(c => c.id_servicio == id).Single();
                    dbc.Servicios.Remove(serv);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e)
            {
                result.Detail = "El servicio está siendo utilizado";
            }
            return Json(result);
        }
        public JsonResult NuevoServicio(string nombre, int dias, int horas, int diasM, int horasM)
        {
            Responses result = new Responses();
            TimeSpan tHoras = TimeSpan.FromHours(horas);
            TimeSpan tDias = TimeSpan.FromDays(dias);
            TimeSpan totales = tHoras + tDias;
            TimeSpan tHorasM = TimeSpan.FromHours(horasM);
            TimeSpan tdiasM = TimeSpan.FromDays(diasM);
            TimeSpan totalesMargen = tHorasM + tdiasM;
            IDirecciones direccion = DireccionesFactory.getDireccion("Telecomunicaciones");
            using (var dbc = new IncidenciasEntities())
            {
                var serv = new Servicios
                {
                    nombre = nombre,
                    tiempo_res = (int)totales.TotalHours,
                    tiempo_margen=(int)totalesMargen.TotalHours,
                    id_direccion = direccion.IdDireccion,
                    tolerancia=80,
                    vigente = true
                };
                dbc.Servicios.Add(serv);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return Json(result);
        }
        public JsonResult nvaTipificacion(String nombre, String desc, int? idPadre,int idServicio)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var direccionSoporte = (from u in dbc.Direcciones where u.nombre == "Telecomunicaciones" select u).Single();
                    Tipificaciones nueva = new Tipificaciones()
                    {
                        nombre = nombre,
                        descripcion = desc,
                        id_padre = idPadre,
                        id_direccion = direccionSoporte.id_direccion,
                        id_servicio = idServicio == 0 ? (int?)null : idServicio
                    };
                    dbc.Tipificaciones.Add(nueva);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public JsonResult quitarTipificacion(int id)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var t = (from u in dbc.Tipificaciones
                             where u.id_tipificacion == id
                             select u).Single();
                    dbc.Tipificaciones.Remove(t);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (DbUpdateException)
            {
                result.Detail = "Probablemente la tipificación está siendo usada por un Incidente o es padre de otra. Contacte al administrador";
            }
            catch (Exception ex)
            {
                result.Detail = ex.Message;
            }
            return Json(result);
        }
        public ActionResult Usuarios()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/usuarios");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                List<Perfiles> perfiles = PerfilFactory.getInstancia().getPerfilesTelecomunicaciones();
                ViewBag.perfiles = perfiles;
                List<usuarioBean> model = usuario.handler.getUsuariosTelecomunicaciones();
                return PartialView(model);
            }                
        }
        public JsonResult NuevoUsuario(string login, string nombre, string p1, string perfil)
        {
            Responses result = new Responses();
            //usuarioHandler user = new usuarioHandler();
            //int idPerfil = Int32.Parse(perfil);
            //IDirecciones tele = DireccionesFactory.getDireccion("Telecomunicaciones");
            //result = user.nvoUsuario(login, nombre, p1, idPerfil,tele);
            return Json(result);
        }
        public ActionResult Tiempos()
        {
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/tiempos");
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return PartialView("DenyPermission");
            }
            else
            {
                TiemposTelecomunicaciones tiempos = new TiemposTelecomunicaciones();
                return PartialView(tiempos);
            }
        }
        public JsonResult setTiempos(int tAvisoDatos,int tEmergDatos, int tAvisoTele,int tEmergTele,int tAIng,int tEIng)
        {
            Responses result = new Responses();
            TiemposTelecomunicaciones tiempos = new TiemposTelecomunicaciones();
            result = tiempos.setTiempos(tAvisoDatos, tEmergDatos, tAvisoTele, tEmergTele,tAIng,tEIng);
            return Json(result);
        }
        public JsonResult borrarEquipo(string oblea)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {
                    var equipo = (from u in dbc.EquipoTele.Include("Ip").Include("Equipo")
                                  where u.UnidadConf.nombre.Equals(oblea)
                                  select u).SingleOrDefault();
                    if (equipo != null)
                    {                        
                        dbc.EquipoTele.Remove(equipo);
                        dbc.SaveChanges();
                        result.Info = "ok";
                    }
                    else result.Detail = "Equipo no encontrado";
                }                
            }
            catch (Exception e)
            {
                result.Detail = e.Message;
            }
            return Json(result);
        }
    }
}
