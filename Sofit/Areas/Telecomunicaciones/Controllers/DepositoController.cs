﻿using soporte.Areas.Soporte.Controllers;
using soporte.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Helpers;
using soporte.Models.Statics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace soporte.Areas.Telecomunicaciones.Controllers
{
    [SessionActionFilter]
    public class DepositoController : Controller
    {
        //
        // GET: /Telecomunicaciones/Deposito/

        public ActionResult Index()
        {
            usuarioBean usuario = Session["usuario"] as usuarioBean;
            Constantes.AccesosPagina acceso = usuario.getAccess("Telecomunicaciones/Deposito");
            string path = string.Empty;
            if (acceso == Constantes.AccesosPagina.NoAccess)
            {
                return RedirectToRoute("SinAcceso");
            }            
            else
            {
                List<UbicacionProducto> armarios = new UbicacionManager().GetUbicacionesTele();
                List<Rubro> rubros = new RubroManager().GetRubrosPadreTele();
                List<Rubro> rubrosAll = new RubroManager().GetRubrosTele();
                List<RubrosVisualizer> rub = new List<RubrosVisualizer>();
                List<JurisdiccionVista>jurisdicciones=new List<JurisdiccionVista>();
                List<Tecnico> tecnicos = new List<Tecnico>();
                foreach (Rubro rAll in rubrosAll)
                {
                    RubrosVisualizer rV = new RubrosVisualizer();
                    rV.idRubro = rAll.id_rubro;
                    rV.Nombre = rAll.nombre;
                    if (rAll.id_padre == null) rV.NombrePadre = "Es Padre";
                    else rV.NombrePadre = (from l in rubrosAll where l.id_rubro == rAll.id_padre select l.nombre).First().ToString();
                    rub.Add(rV);
                }                
                using (var dbc = new IncidenciasEntities())
                {
                    var tecs = (from u in dbc.tecnico where u.area.Direcciones.nombre == "Telecomunicaciones" select u).ToList();
                    jurisdicciones=(from u in dbc.Jurisdiccion
                                        select new JurisdiccionVista{
                                             idJurisdiccion=u.id_jurisdiccion,
                                              Nombre=u.descripcion
                                        }).ToList();
                    tecnicos=(from u in dbc.tecnico
                     where u.area.Direcciones.nombre == "Telecomunicaciones" & u.activo.Value & u.id_usuario.HasValue
                     orderby u.descripcion
                     select new Tecnico
                     {
                         idTecnico = u.id_tecnico,
                         Nombre = u.descripcion
                     }).ToList();
                }

                ViewBag.Armarios = armarios;
                ViewBag.RubrosPadre = rubros;
                ViewBag.Tecnicos = tecnicos;
                ViewBag.Jurisdicciones = jurisdicciones;
                return View(rub);
            }           
        }
        public ActionResult getTable(jQueryDataTableParamModel param)
        {
            List<Producto> productos = new ProductoManager().getProductosVigentesTele();
            ProductoJson pj;
            List<ProductoJson> prods = new List<ProductoJson>();
            DataTable tablaProds = new DataTable();
            tablaProds.Clear();
            tablaProds.Columns.Add("Nombre");
            tablaProds.Columns.Add("Stock");
            tablaProds.Columns.Add("Ubicación");
            tablaProds.Columns.Add("Rubro");
            tablaProds.Columns.Add("Cantidad");
            foreach (Producto p in productos)
            {
                pj = new ProductoJson()
                {
                    id = p.id_prod,
                    nombre = p.nombre,
                    descripcion = p.descripcion,
                    idDependencia = p.id_dependencia,
                    stock = p.stock,
                    idUbicacion = p.id_ubicacion,
                    nombreUbicacion = p.UbicacionProducto.nombre,
                    nombreRubro = p.Rubro.nombre,
                    codigo = p.codigo,
                    idRubro = p.id_rubro,
                    pathImagen = p.path_imagen
                };
                prods.Add(pj);
                DataRow row = tablaProds.NewRow();
                row["Nombre"] = p.nombre;
                row["Stock"] = p.stock;
                row["Ubicación"] = p.UbicacionProducto.nombre;
                row["Rubro"] = p.Rubro.nombre;
                row["Cantidad"] = p.stock;
                tablaProds.Rows.Add(row);
            }
            Session["tableSource"] = tablaProds;
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = 90,
                iTotalDisplayRecords = prods.Count,
                aaData = prods
            },
        JsonRequestBehavior.AllowGet);
        }
        public JsonResult nvoArmario(String info, string desc)
        {
            UbicacionProducto armario = new UbicacionProducto() { nombre = info, descripcion = desc };
            using (var dbc = new IncidenciasEntities())
            {
                var direccion = (from s in dbc.Direcciones where s.nombre == "Telecomunicaciones" select s).Single();
                armario.id_direccion = direccion.id_direccion;
            }
            return Json(new UbicacionManager().nuevoArmario(armario));

        }
        [HttpPost]

        public JsonResult dropArmario(String id)
        {

            return Json(new UbicacionManager().Delete(id));

        }
        public JsonResult nvoRubro(String nombre, String desc, int? idPadre)
        {
            Rubro rubro = new Rubro() { nombre = nombre, descripcion = desc, id_padre = idPadre };
            using (var dbc = new IncidenciasEntities())
            {
                var dir = (from u in dbc.Direcciones where u.nombre == "Telecomunicaciones" select u).Single();
                rubro.id_direccion = dir.id_direccion;
            }

            return Json(new RubroManager().nuevoRubro(rubro));
        }
        public JsonResult dropRubro(int idRubro)
        {
            return Json(new RubroManager().Delete(idRubro));
        }
        public JsonResult getHijos(int id)
        {
            IEnumerable result = new RubroManager().GetRubrosHijos(id);
            return Json(result);
        }
        [HttpPost]
        public JsonResult RubrosPadre()
        {
            return Json(new RubroManager().GetRubrosPadreSoporte());
        }
        public JsonResult nuevoArticulo(HttpPostedFileBase file)
        {
            Producto p = null;
            Responses result = null;
            try
            {
                //string path = Server.MapPath("~/Content");
                NameValueCollection nvc = Request.Form;
                if (nvc.Keys.Count == 0)
                {
                    StreamReader stream = new StreamReader(Request.InputStream);
                    string bodytext = stream.ReadToEnd();
                    result = new Responses() { Info = "Error", Detail = "No se recibieron datos desde el cliente." };
                }
                var requestStream = Request.InputStream;
                int idRubro;
                int idDependencia;
                int idArmario;
                int cantidad;
                string descripcion = nvc.Get("desc");
                string nombre = nvc.Get("nombre");
                Int32.TryParse(nvc.Get("idRubro").ToString(), out idRubro);
                Int32.TryParse(nvc.Get("idDependencia").ToString(), out idDependencia);
                Int32.TryParse(nvc.Get("idArmario").ToString(), out idArmario);
                string codigo = string.Empty;
                Int32.TryParse(nvc.Get("cantidad").ToString(), out cantidad);
                p = new Producto()
                {
                    id_rubro = idRubro,
                    id_dependencia = idDependencia,
                    id_ubicacion = idArmario,
                    stock = cantidad,
                    nombre = nombre,
                    descripcion = descripcion,
                    path_imagen = (file != null) ? PathImage.getHtmlDirectoryDefault() + file.FileName : PathImage.getHtmlImageDefault(),
                    codigo = codigo
                };
                using (var dbc = new IncidenciasEntities())
                {
                    var dire = (from u in dbc.Direcciones where u.nombre == "Telecomunicaciones" select u).Single();
                    p.id_direccion = dire.id_direccion;
                }
                result = new ProductoManager().nuevoProducto(p, file);
            }
            catch (Exception e)
            {
                result = new Responses() { Info = "Error", Detail = e.Message };

            }
            return Json(result);
        }
        public JsonResult ajustarStock(HttpPostedFileBase file)
        {
            NameValueCollection nvc = Request.Form;
            int cant = Int32.Parse(nvc.Get("cantidad"));
            int id = Int32.Parse(nvc.Get("id"));
            int idArmario = Int32.Parse(nvc.Get("idArmario"));
            string descripcion = nvc.Get("descripcion");
            string observacion = nvc.Get("observacion");
            return Json(new ProductoManager().updateStock(id, cant, idArmario, file, descripcion, observacion));
        }
        public JsonResult addStock()
        {
            NameValueCollection nvc = Request.Form;
            int id = Int32.Parse(nvc.Get("id"));
            int cant = Int32.Parse(nvc.Get("cantidad"));
            string observaciones = nvc.Get("observaciones");
            return Json(new ProductoManager().addStock(id, cant, observaciones));
        }
        [HttpPost]
        public JsonResult retirarArticulo()
        {
            NameValueCollection nvc = Request.Form;
            int id = Int32.Parse(nvc.Get("id"));
            int cant = Int32.Parse(nvc.Get("cantidad"));
            string idEquipo = "na";
            string tecnico = nvc.Get("tecnico");
            string oblea = nvc.Get("equipo");
            string observaciones = nvc.Get("observaciones");
            if (tecnico == null) tecnico = "na";
            if (oblea != null)
            {
                
            }
            return Json(new ProductoManager().retirarProductos(id, cant, idEquipo, tecnico,observaciones));
        }
        [HttpPost]
        public JsonResult dropArticulo(int id)
        {
            return Json(new ProductoManager().dropProducto(id));
        }
        [HttpPost]
        public JsonResult getArticulo(int id)
        {
            Producto p = new ProductoManager().getProdXId(id);
            ProductoJson pj = new ProductoJson()
            {
                id = p.id_prod,
                nombre = p.nombre,
                descripcion = p.descripcion,
                idDependencia = p.id_dependencia,
                stock = p.stock,
                idUbicacion = p.id_ubicacion,
                codigo = p.codigo,
                idRubro = p.id_rubro,
                pathImagen = p.path_imagen
            };
            return (Json(pj));
        }
    }
}
