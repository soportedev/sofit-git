﻿ // JavaScript Document
window.pathServer = '';

// GetEquipo
window.GetEquipo = function(oblea)
{
	$.ajax({
	    data:  {'oblea':oblea},
		url:   pathServer+'Inventario/GetEquipoTele',
		type:  'post',
		dataType: 'json',
		beforeSend: function () {
			// Show Loading
			NProgress.start();
		},
		success: function (response) {
		    // Hide Loading
		    NProgress.done();
		    $('#idEquipo').val(response.idEquipo);
		    $('#Oblea').val(response.Oblea);
		    $('#NroSerie').val(response.NroSerie);
		    $('#idTipo').val(response.TipoEquipo.Nombre).attr('data-id', response.TipoEquipo.id);
		    $('#Marca').val(response.Marca.Nombre).attr('data-id', response.Marca.idMarca);
		    $('#Modelo').val(response.Modelo);
		    $('#Jurisdiccion').val(response.Dependencia.Jurisdiccion.Nombre).attr('data-id', response.Dependencia.Jurisdiccion.idJurisdiccion);
		    $('#Ubicacion').val(response.Dependencia.Nombre).attr('data-id', response.Dependencia.idDependencia);
		    $('#NComun').val(response.NComun);
		    $('#Estado').val(response.Estado.Nombre).attr('data-id', response.Estado.idEstado);
		    $('#Observaciones').val(response['Observaciones'])
		    $('#UserAcc1').val(response.Usuario1);
		    $('#UserAccPass1').val(response.Password1);
		    $('#UserAcc2').val(response.Usuario2);
		    $('#UserAccPass2').val(response.Password2);
		    $('#UserAcc3').val(response.Usuario3);
		    $('#UserAccPass3').val(response.Password3);
		    $('#SSID').val(response.SSID);
		    $('#SSIDPass').val(response.SSIDPassword);
		    $('#Encriptacion').val(response.Encriptacion);
		    if (response.IP) {
		        $('#IPv4').val(response.IP.IpV4);
		        $('#IPv6').val(response.IP.IpV6);
		        $('#SubNet').val(response.IP.Mascara);
		        $('#GateWay').val(response.IP.Gateway);
		        $('#MAC').val(response.IP.Mac);
		        $('#IPLoop').val(response.IP.IpLoopback);
		        $('#IPWAN').val(response.IP.IpWan);
		        $('#IPGestion').val(response.IP.IpGestion);
		    }		    
		    $('#Ports').val(response['Puertos']);
		    $('#SfpSerie').val(response.SfpSerie);
		    $('#SfpMac').val(response.MacSfp);
		    $('#SfpTipo').val(response.TipoSfp);
		    $('#Snmp').val(response.Snmp);
		    $('#SnmpVer').val(response.SnmpVer);
		    showingState();
		},
		error: function (response, textStatus) {
			// Hide Loading
			NProgress.done();

			// create the notification
			var notification = new NotificationFx({
				wrapper: document.body,
				message: textStatus,
				layout: 'growl',
				effect: 'genie',
				type: 'error', // sussess, notice, warning or error 
				ttl: 3000,
				onClose: function () {
					bttn.disabled = false;
				}
			});

			// show the notification
			notification.show();

		}
	});
	// Ajax
	// Send data
}

// GET OBLEAS
window.GetObleas = function(searchVal, URL){	
	$.ajax({
		data:  {prefixText: searchVal},
		url:   pathServer+'Inventario/EquipoXOblea',
		type:  'post',
		dataType: 'json',		
		success:  function(response) {		
				// Values
				var raw = response;
				var source = [];
				for (var i = 0; i < raw.length; ++i) {
					source.push(raw[i].Oblea);
				}
				// Function
				$('#tBuscar').autocomplete({
					source: source,
					minLength: 2,
					select: function (e,ui) {
					    $(e.target).val(ui.item.label);
					    var oblea = ui.item.label;
					    GetEquipo(oblea, URL)
					    $(this).blur()
					},
					close: function () {
						// Finish selection and make a blur
						//var oblea = { oblea: $(this).val()}
						//GetEquipo(oblea, URL)
						//$(this).blur()
					}
				}).click(function (e) {
					$(this).autocomplete('search');
				})
			
		},
		error: function (response, textStatus) {
		}		
	});
	// Ajax
	//-->
}
// GET OBLEAS
//-->
// GET SERIES
window.GetSeries = function(searchVal, URL){
	// Ajax
	// Send data
	$.ajax({
		data:  {prefixText: searchVal},
		url: pathServer + 'Inventario/EquipoXSerie',
		type:  'post',
		dataType: 'json',
		beforeSend: function () {
			
		},
		success:  function(response) {
			
			if(response['Info'] == 'No inicializado'){				
			}else{
				// Values
				var raw = response;
				var source = [];
				for (var i = 0; i < raw.length; ++i) {
					source.push(raw[i].NroSerie);
				}
				// Function
				$('#tBuscar').autocomplete({
					source: source,
					minLength: 2,
					select: function (e,ui) {
					    $(e.target).val(ui.item.label);
					    var serie = { serie: ui.item.label}
					    GetEquipo(serie, URL)
					    $(this).blur()
					},
					close: function () {
						// Finish selection and make a blur
						//var serie = { serie: $(this).val()}
						//GetEquipo(serie, URL)
						//$(this).blur()
					}
				}).click(function (e) {
					$(this).autocomplete('search');
				})
			}
		},
		error: function (response, textStatus) {


		}
	});
	// Ajax
	//-->
}
// GET SERIES
//-->
$(function () {
    //obleas disponibles
    $('.getNumber').on('click', function () {
        var tipo = $('#idTipo').attr('data-id');
        if (!tipo) {
            mostrarError('Seleccione el tipo de elemento primero!!!');
            return;
        }
        if (window.State != 'nuevo') {
            mostrarError('Disponible para nuevos elementos!!');
            return;
        }
        $.ajax({
            url: '/Service/GetNextOblea',
            data: { 'tipo': tipo },
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data.Info == "Login") {
                    mostrarFinSesion();
                    return;
                }
                if (data.Info == "ok") {
                    var par = data.Detail;
                    var p = $('<p>').append(par);
                    var div = $('<div>');
                    $(div).append(p).dialog({
                        title: "Se puede usar",
                        buttons: {
                            Aceptar: function () {
                                $('#Oblea').val(par);
                                $(this).dialog('destroy');
                            },
                            Cancelar: function () {
                                $(this).dialog('destroy');
                            }
                        },

                    });
                }
                else {
                    mostrarMensajeFx(data.Detail, 'notice', 8000);
                }
            },
            error: function (a, b, c) {
                mostrarError(c)
            }
        });
    });
    //Marca
    $('#Marca').click(function (e) {
        var disable = $(this).attr('readonly');
        var marca = $(this);
        // Check input status
        if (disable === 'readonly') {
            // If input is readonly, diable functions
            e.preventDefault();
        }
        else {
            // If input is writable
            // Clean val
            $(this).val('');
            var idTipo = $('#idTipo').attr('data-id');
            if (idTipo != "") {
                $.ajax({
                    data: { 'tipo': idTipo },
                    url: '/Service/MarcasPorTipo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function () {
                        // Show Loading
                        NProgress.start();
                    },
                    success: function (response) {
                        // Show Loading
                        NProgress.done();
                        // Values
                        var raw = response;
                        var source = [];
                        var mapping = {};
                        mapping.value = 0;
                        mapping.label = "Agregar";
                        source.push(mapping);
                        for (var i = 0; i < raw.length; ++i) {
                            mapping = new Object();
                            mapping.value = raw[i].idMarca;
                            mapping.label = raw[i].Nombre;
                            source.push(mapping);
                        }
                        // Function
                        $(marca).autocomplete({
                            source: source,
                            minLength: 0,
                            select: function (event, ui) {
                                if (ui.item.value == 0) {                                    
                                    AgregarNuevaMarca();
                                    return false;
                                } else {
                                    $(this).attr('data-id', ui.item.value);
                                    $(this).val(ui.item.label);
                                    return false;
                                }
                            },
                            close: function () {
                                // Finish selection and make a blur
                                $(this).blur();
                            }
                        }).autocomplete('search');
                    },
                    // On error
                    error: function (response, textStatus, tres) {
                        mostrarError(tres);
                    }
                });// Ajax
            } else {
                mostrarError("Seleccione el Tipo de Equipo!!");
            }
        }
    });
    //-->Marca
    $('#searchButton').on('click', function () {
        var oblea = $('#tBuscar').val();
        GetEquipo(oblea);
    });
	// TRANFORM Uppercase
    $('#tBuscar, #Oblea').keyup(function () {
        $(this).val(this.value.toUpperCase())
    });
    $('#tBuscar').autocomplete({
        minLength: "2",
        source: (
        function (request, response) {
            var flagNombre, flagNc, flagIp = false;
            var buscoRed = request.term.split('.');
            if (buscoRed.length > 1) {
                for (var i = 0; i < buscoRed.length; i++) {
                    var num = Number(buscoRed[i]);
                    if (num < 255 & num > 0) flagIp = true;
                    else {
                        flagIp = false;
                        break;
                    }
                }
            }
            if (flagIp) {
                uri = 'EquipoXIp';
            } else {
                if (regTeleStart.test(request.term)) {
                    uri = 'EquipoXOblea';
                    flagNombre = true;
                } else {
                    uri = 'EquipoXNombreComun';
                    flagNc = true;
                }
            }
            var term = '{ prefixText: "' + request.term + '" }';
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'Inventario/' + uri,
                data: term,
                dataType: "json",
                success: function (data) {
                    if (data.Info == "Login") {
                        mostrarFinSesion();
                        return;
                    }
                    else {
                        var suggestions = [];
                        $.each(data, function (i, val) {
                            var obj = {};
                            obj.value = val.IdEquipo;
                            if (flagNc) obj.label = val.NombreComun;
                            if (flagIp) obj.label = val.Ip[0].IpV4;
                            if (flagNombre) obj.label = val.Oblea;                            
                            obj.Nombre = val.Oblea;                            
                            suggestions.push(obj);
                        })
                        response(suggestions);
                    }
                }
            });
        }),
        select: function (e, ui) {
            e.preventDefault();
            $('#tBuscar').val(ui.item.label);
            GetEquipo(ui.item.Nombre)
            return false;
        },
        focus: function (e, ui) {
            e.preventDefault();
            $('#tBuscar').val(ui.item.label);
            return false;
        }
    });
    $('.showingInfo').on('click', function () {
        var text = $(this).children('span').text();
        mostrarInfo(text);
    });
    //MODAL
    // Get the modal
    var modal = document.getElementById('myModal');
    // Get the button that opens the modal
    var btn = $("span#works_btn");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0]; // Only for first close class
    // Get the <send> element that closes the modal
    var send = document.getElementById("SendForm")[0]; // Only for first id	
    // When the user clicks the button, open the modal
    btn.each(function () {
        $(this).on("click", function () {
            modal.style.display = "block";
        });
    });
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    initialState();
});

// TIPO
$(function () {    
    $.ajax({
        url: pathServer + '/Service/TiposEquipo',
        data: { Direccion: 'Telecomunicaciones' },
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            //NProgress.start();
        },
        success: function (response) {
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].id;
            }
            // Function
            $('#idTipo').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable == 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If inpit is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });    
	//-->
	// Ubicacion
    window.getUbicacion = function () {
        $('#Ubicacion').val('');
        // Send data
        // Ajax
        $.ajax({
            data: { idJur: $("#Jurisdiccion").attr("data-Id") },
            url: pathServer + 'Inventario/Dependencias',
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                // Show Loading
                //NProgress.start();
            },
            success: function (response) {
                // Values
                var raw = response;
                var source = [];
                var mapping = {};
                for (var i = 0; i < raw.length; ++i) {
                    source.push(raw[i].Nombre);
                    mapping[raw[i].Nombre] = raw[i].idDependencia;
                }
                // Function
                $('#Ubicacion').autocomplete({
                    source: source,
                    minLength: 0,
                    select: function (event, ui) {
                        var dataId = mapping[ui.item.value]
                        $(this).attr('data-id', dataId);
                    },
                    close: function () {
                        // Finish selection and make a blur
                        $(this).blur();
                    }
                }).click(function (e) {
                    var disable = $(this).attr('readonly');
                    // Check input status
                    if (disable == 'readonly') {
                        // If input is readonly, diable functions
                        e.preventDefault();
                    }
                    else {
                        // If inpit is writable
                        // Clean val
                        $(this).val('');
                        // Trigger Search values on fucus
                        $(this).autocomplete('search');
                        // Trigger KeyDown on focus
                        //$(this).trigger(jQuery.Event("keydown"));
                    }
                })
            },
            // On error
            error: function (response, textStatus) {
                // NOTIFICAION
            }
        });
        // Ajax
        // Send data
    };
    //-->

    // Jurisdiccion
    // Send data
    // Ajax
    $.ajax({
        url: pathServer + 'Inventario/Jurisdicciones',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            //NProgress.start();
        },
        success: function (response) {
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idJurisdiccion;
            }
            // Function
            $('#Jurisdiccion').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                    $(this).val(ui.item.value)
                    getUbicacion();
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable == 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If inpit is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
            //-->
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    // Ajax
    // Send data
    $.ajax({
        url: pathServer + 'Inventario/EstadosEquipo',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            // Show Loading
            //NProgress.start();
        },
        success: function (response) {
            // Values
            var raw = response;
            var source = [];
            var mapping = {};
            for (var i = 0; i < raw.length; ++i) {
                source.push(raw[i].Nombre);
                mapping[raw[i].Nombre] = raw[i].idEstado;
            }
            // Function
            $('#Estado').autocomplete({
                source: source,
                minLength: 0,
                select: function (event, ui) {
                    var dataId = mapping[ui.item.value]
                    $(this).attr('data-id', dataId);
                },
                close: function () {
                    // Finish selection and make a blur
                    $(this).blur();
                }
            }).click(function (e) {
                var disable = $(this).attr('readonly');
                // Check input status
                if (disable == 'readonly') {
                    // If input is readonly, diable functions
                    e.preventDefault();
                }
                else {
                    // If inpit is writable
                    // Clean val
                    $(this).val('');
                    // Trigger Search values on fucus
                    $(this).autocomplete('search');
                    // Trigger KeyDown on focus
                    //$(this).trigger(jQuery.Event("keydown"));
                }
            })
        },
        // On error
        error: function (response, textStatus) {
            // NOTIFICAION
        }
    });
    // Ajax
    // Send data
    
	// Fullscreen function
	//-->	
    // Actualizar Datos
    $('#SendForm').click(function (e) {
        e.preventDefault();
		// Check if the page is Altas or Update
		if (typeof Altas !== 'undefined') { 
			// If is Altas		
			// Determines the URL to send data
			// URL for new item
		    var urlNewUpdate = pathServer + 'Inventario/Nuevo'
		} else {
			// URL for update
		    var urlNewUpdate = pathServer + 'Inventario/Update'
		}
		//-->
        // Get the modal
        var modal = document.getElementById('myModal');
        // Hide modal
        modal.style.display = "none";

        // Validations
        if ($('#Oblea').val()) {
            var Val = $('#Oblea').val();
            if (regEquipoTele.test(Val)) {
                var resOblea = 'ok';
                $('#Oblea').removeClass('error');
            } else {
                $('#Oblea').addClass('error');
            }
        } else {
            $('#Oblea').addClass('error');
        }

        if ($('#idTipo').val()) {
            var resTipo = 'ok';
            $('#idTipo').removeClass('error');
        }
        else {
            $('#idTipo').addClass('error');
        }

        if ($('#Marca').val()) {
            var resMarca = 'ok';
            $('#Marca').removeClass('error');
        }
        else {
            $('#Marca').addClass('error');
        }

        if ($('#Jurisdiccion').val()) {
            var resJurisdiccion = 'ok';
            $('#Jurisdiccion').removeClass('error');
        }
        else {
            $('#Jurisdiccion').addClass('error');
        }

        if ($('#Ubicacion').val()) {
            var resUbicacion = 'ok';
            $('#Ubicacion').removeClass('error');
        }
        else {
            $('#Ubicacion').addClass('error');
        }        
        // Validations
        //-->

        if ((resOblea && resTipo && resMarca && resJurisdiccion && resUbicacion ) == 'ok') 
		{
            // Get data
            // Declare array
            var toReturn = [];
            // Search inputs
            var els = $('#content-box').find(':input').get();
            $.each(els, function () {
                // Check if are valid input
                if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) ||
                /text|hidden|password/i.test(this.type))) {
                    var val;
                    // Check if select or input
                    if ($(this).attr('data-Id') != '') {
                        // Select
                        val = $(this).attr('data-Id');
                    } else {
                        // Input
                        val = $(this).val();
                    }
                    // Add array value
                    toReturn.push(encodeURIComponent(this.name) + "=" + encodeURIComponent(val));
                }
            });
            // Get data
            //-->
            // Declare and return formatted array
            var ArrayFinal = toReturn.join("&").replace(/%20/g, "+");
            // Send data
            // Ajax
            $.ajax({
                data: ArrayFinal,
                url: urlNewUpdate,
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Hide Loading
                    NProgress.done();
                    // create the notification					
					if(response.Info == 'ok'){
					    mostrarExito('Se actualizaron los datos del equipo');
					    initialState();
					}else{
					    mostrarError(response.Detail);
					}                    
                },
                error: function (response, textStatus,thrownStatus) {
                    // Hide Loading
                    NProgress.done();            
                    mostrarError(thrownStatus);
                }
            });
            // Ajax
            // Send data
            //-->
        } else {
            
        }

    })
    // Actualizar Datos
    //-->
    $('#SendMarca').click(function (e) {
        e.preventDefault();
        var vMarca = $('#AddMarca').val();		
		// Remove error class
		$('#AddMarca').removeClass('error');

        // Check if a valid value
        if (vMarca != '' && vMarca.length > 1) {
            // Ajax
            $.ajax({
                data: { nombre: vMarca , idTipo:$('#idTipo').attr('data-id')},
                url: pathServer + 'Inventario/NuevaMarcaTele',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    // Show Loading
                    NProgress.start();
                },
                success: function (response) {
                    // Hide Loading
                    NProgress.done();
					// Recall Marcas
					FunctionMarca();                   
					if(response.Info == 'ok'){
					    mostrarExito("Se agregó la marca");
					}else{
					    mostrarError(response.Detail);
					}                    
                },
                error: function (response, textStatus,thrownError) {
                    // Hide Loading
                    NProgress.done();
                    mostrarError(thrownError);
                }
            });
            // Ajax

            // Get the modal
            var modal = document.getElementById('myModal');
            // Hide modal
            modal.style.display = "none";
        } else {
            mostrarError('Ingrese un valor correcto');                
        }
        // Check if a valid value
        //-->
    })
    // Habilita edicion de datos    
    $('#editData').click(function () {
        editState();
    });
    $('#AceptarCambios').click(function () {
        // Ventana de confirmacion
        $('.classMarca').addClass('hide');
        $('.classEnviar').removeClass('hide');
        $('span#works_btn').trigger(jQuery.Event("click"));
    });
    $('#CancelarCambios').on('click', initialState);
	// Altas
	$('#EquipoNuevo').click(function(){
		newState();
	})
	//-->	
	// Print
	$('#ImprOblea').click(function(e) {
	    e.preventDefault()
	    var oblea = $('#Oblea').val();
	    if (regEquipoTele.test(oblea)) window.open('/Print?id=' + oblea + '&area=Telecomunicaciones', 'Imprimir')
	    else mostrarError('Seleccione un equipo Primero');
	});
	// Print
	//-->
});
//-->
function initialState() {
    window.State = 'initial';
    $('#CancelarCambios').hide();
    $('#ImprOblea').hide();
    $('#editData').hide();
    $('#AceptarCambios').hide();
    $('#LineaBase').hide();
    $('#EquipoNuevo').show();
    // Clean all inputs/textarea
    $('input, textarea', '#content-box').each(function () {
        $(this).val('');
        $(this).attr('readonly', 'readonly');
        $(this).attr('data-id', '');
        $(this).removeClass('error');
    });
    $('#content-box .button').each(function () {
        $(this).attr('readonly', 'readonly');
    });   
    $('#Estado').removeAttr('disabled').attr('placeholder', 'Campo Obligatorio').attr('readonly', 'readonly');
    OcultarRelaciones();
}
function editState() {
    window.Altas = undefined;
    window.State = 'edit';
    $('#AceptarCambios').show();
    $('#CancelarCambios').show();
    $('#ImprOblea').hide();
    $('#EquipoNuevo').hide();
    //$('#editData').hide();
    var inputName
    var NoEdtiable = /^Oblea$|^PRTipo$|^PRTecnologia$|^PRRendimiento$|^PRConectividad$|^PRInsumosTipo$|^PRInsumosCod$|^MNTecnologia$|^MNTamaño$|^MNRelAs$|^ModMArcaImpresora$|^ModModeloImpresora$/;
    $('input:text').each(function () {
        if (typeof Altas === 'undefined') {
            inputName = $(this).attr('name');
            if (!NoEdtiable.test(inputName)) {
                $(this).removeAttr('readonly');
            }
        } else {
            inputName = $(this).attr('name');
            if (!NoEdtiable.test(inputName)) {
                $(this).removeAttr('readonly');

                // NO COPIAR //
                $('#Oblea').removeAttr('readonly');
                // NO COPIAR //
            }
        }
    });
    $('textarea').each(function () {
        $(this).removeAttr('readonly');
    });
    $('#content-box .button').each(function () {
        $(this).removeAttr('readonly');
    });
    $('input:radio').each(function () {
        $(this).removeAttr('disabled');
    });
    $('#AltaComponentesBttn').removeAttr('readonly');
    $('#AgregarRelacion').removeAttr('readonly');
    OcultarRelaciones();
}
function newState() {
    window.Altas = 'Y';
    window.State = 'nuevo';
    var inputName
    var NoEdtiable = /^ClaveBIOS|^ClaveAdmin$|^PRTipo$|^PRTecnologia$|^PRRendimiento$|^PRConectividad$|^PRInsumosTipo$|^PRInsumosCod$|^MNTecnologia$|^MNTamaño$|^MNRelAs$|^ModMArcaImpresora$|^ModModeloImpresora$/;
    $('input, textarea', '#content-box').each(function () {
        //inputName = $(this).attr('name');
        //if (!NoEdtiable.test(inputName)) {
            $(this).val('');
            $(this).removeAttr('readonly', 'readonly');
            $(this).removeClass('error');
        //}
    });
    $('#content-box .button').each(function () {
        $(this).removeAttr('readonly');
    });
    $('#editData').hide();
    $('#EquipoNuevo').hide();
    $('#LineaBase').hide();
    $('#CancelarCambios').show();
    $('#AceptarCambios').show();
    $('#ImprOblea').hide();
    $('#Estado').attr('disabled', 'disabled').removeAttr('placeholder');
    OcultarRelaciones();
}
function showingState() {
    window.State = 'showing';
    $('#ImprOblea').show();
    $('#editData').show();
    $('#AceptarCambios').hide();
    $('#LineaBase').show();
    OcultarRelaciones();
}
function AgregarNuevaMarca() {    
        $('.classEnviar').each(function () {
            $(this).addClass('hide');
        })
        $('.classMarca').each(function () {
            $(this).removeClass('hide');
        })
        $('span#works_btn').trigger(jQuery.Event("click"));   
}
