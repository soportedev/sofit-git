﻿function deleteEquipo(e) {
    e.preventDefault();
    var oblea = $.trim($('#tOblea').val());
    var este = $('#removeEquipo');
    $('.divError', este).hide();
    if (regEquipoTele.test(oblea.toUpperCase())) {
        $.ajax({
            url: "Config/borrarEquipo",
            type: 'POST',
            data: 'oblea=' + oblea,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }                
                if (data.Info == 'ok') {
                    mostrarExito('Equipo Borrado');
                }
                else {
                    $('.message', este).html(data.Detail);
                    $('.divError', este).show('fast');
                }                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            },
            complete: function () {
                $('.ajaxGif').css({ 'display': 'none' });
            }
        });
    }
    else {
        mostrarError("El formato de la oblea no es válido");
    }
}
function actualizarTiempos(e) {
    e.preventDefault();
    var flag=true;
    var tAvisoDatos = $('#tAvisoDatos');
    var tEmerDatos = $('#tEmergDatos');
    var tAvisoTele = $('#tAvisoTele');
    var tEmerTele = $('#tEmergTele');
    var tAIng = $('#tAIng');
    var tEIng = $('#tEIng');
    tAvisoDatos.removeClass('denied');
    tEmerDatos.removeClass('denied');
    tAvisoTele.removeClass('denied');
    tEmerTele.removeClass('denied');
    if (!regExTest(regTime, tAvisoDatos.val())){
        tAvisoDatos.addClass('denied');
        flag = false;
    }
    if (!regExTest(regTime, tEmerDatos.val())) {
        tEmerDatos.addClass('denied');
        flag = false;
    }
    if (!regExTest(regTime, tAvisoTele.val())) {
        tAvisoTele.addClass('denied');
        flag = false;
    }
    if (!regExTest(regTime, tEmerTele.val())) {
        tEmerTele.addClass('denied');
        flag = false;
    }    
    if(flag)
     {
        var datos = $('#ui-tabs-2 ul').serializeAnything();
        $.ajax({
            url: "Config/setTiempos",
            type: 'POST',
            data: datos,
            beforeSend: function () {
                $('.error').hide('fast');
            },
            success: function (data) {
                if (data.Info == 'Login') {
                    mostrarFinSesion();
                    return;
                }                
                if (data.Info == 'ok') {
                    mostrarExito('Tiempos Actualizados');
                }
                else {
                   mostrarError(data.Detail);                    
                }                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    else {
        mostrarError('Verifique los datos y vuelva a intentar');        
    }
}