﻿using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.ClasesVistas;
using soporte.Models.Displays;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace soporte.Areas.Telecomunicaciones.Models.ClasesVistas
{
    public class EquipoTelecomunicaciones:IEquipoInventario
    {
        public EquipoTelecomunicaciones()
        {
            Marca = new MarcaVista();
            Estado = new EstadoEquipoVista();
            Dependencia = new DependenciaVista();
            IP = new IpVista();
            TipoEquipo = new TiposEquipoVista();
        }
        public string Modelo { get; set; }        
        public string NComun { get; set; }
        public MarcaVista Marca { get; set; }        
        public EstadoEquipoVista Estado { get; set; }
        public DependenciaVista Dependencia { get; set; }        
        public string Usuario1 { get; set; }
        public string Password1 { get; set; }
        public string Usuario2 { get; set; }
        public string Password2 { get; set; }
        public string Usuario3 { get; set; }
        public string Password3 { get; set; }
        public string Encriptacion { get; set; }
        public string SfpSerie { get; set; }
        public string MacSfp { get; set; }
        public string TipoSfp { get; set; }
        public string Snmp { get; set; }
        public string SnmpVer { get; set; }
        public string SSID { get; set; }
        public string SSIDPassword { get; set; }
        public IpVista IP { get; set; }
        
        public string Puertos { get; set; }

        internal override Responses SaveLineaBase()
        {
            Responses result = new Responses();
            StringBuilder sb = new StringBuilder();           
            sb.Append("Oblea:").Append(Oblea).AppendLine();
            sb.Append("Nombre Común:").Append(NComun).AppendLine();
            sb.Append("Modelo:").Append(Modelo).AppendLine();
            sb.Append("NroSerie:").Append(NroSerie).AppendLine();
            sb.Append("Marca:").Append(this.Marca.Nombre).AppendLine();            
            sb.Append("Dependencia:").Append(this.Dependencia.Nombre).AppendLine();
            sb.Append("TipoEquipo:").Append(this.TipoEquipo.Nombre).AppendLine();
            sb.Append("Observaciones:").Append(this.Observaciones).AppendLine();
            //sb.Append("Usuario1:").Append(this.Usuario1).AppendLine();
            //sb.Append("Password1:").Append(this.Password1).AppendLine();
            //sb.Append("Usuario2:").Append(this.Usuario1).AppendLine();
            //sb.Append("Password2:").Append(this.Password1).AppendLine();
            //sb.Append("Usuario3:").Append(this.Usuario1).AppendLine();
            //sb.Append("Password3:").Append(this.Password1).AppendLine();
            //sb.Append("Encriptacion:").Append(this.Encriptacion).AppendLine();
            //sb.Append("SfpSerie:").Append(this.SfpSerie).AppendLine();
            //sb.Append("MacSfp:").Append(this.MacSfp).AppendLine();
            //sb.Append("TipoSfp:").Append(this.TipoSfp).AppendLine();
            //sb.Append("Snmp:").Append(this.Snmp).AppendLine();
            //sb.Append("SnmpVer:").Append(this.SnmpVer).AppendLine();
            //sb.Append("SSID:").Append(this.SSID).AppendLine();
            //sb.Append("SSIDPassword:").Append(this.SSIDPassword).AppendLine();
            sb.Append("IPV4:").Append(this.IP.IpV4).AppendLine();
            sb.Append("Puertos:").Append(this.Puertos).AppendLine();
            usuarioBean usuario=HttpContext.Current.Session["usuario"]as usuarioBean;
            using(var dbc=new IncidenciasEntities())
            {
                var newRecord = new BaseLines
                {
                    data = sb.ToString(),
                    fecha = DateTime.Now,
                    master_id = this.idEquipo,
                    id_creadopor = usuario.IdUsuario
                };
                dbc.BaseLines.Add(newRecord);
                dbc.SaveChanges();
                result.Info = "ok";
            }
            return result;
        }
        public override void construir(int id)
        {
            base.construir(id);           
            try
            {
                using(var dbc=new IncidenciasEntities())
                {
                    var eqBase = (from u in dbc.EquipoTele
                              where u.UnidadConf.id_uc == id
                              select u).First();
                    this.Dependencia = new DependenciaVista
                    {
                        idDependencia = eqBase.id_dependencia.Value,
                        Nombre = eqBase.Dependencia.descripcion,
                        Jurisdiccion = new JurisdiccionVista
                        {
                            idJurisdiccion = eqBase.Dependencia.Jurisdiccion.id_jurisdiccion,
                            Nombre = eqBase.Dependencia.Jurisdiccion.descripcion
                        }
                    };
                    this.NComun = eqBase.nombre_comun;
                    this.Estado = new EstadoEquipoVista
                    {
                        idEstado = eqBase.Estado_UC.id_estado,
                        Nombre = eqBase.Estado_UC.descripcion
                    };
                    this.idEquipo = eqBase.UnidadConf.id_uc;
                    this.TipoEquipo = new TiposEquipoVista
                    {
                        id = eqBase.UnidadConf.id_tipo,
                        Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion
                    };
                    if (eqBase.Ip.Count > 0)
                    {
                        Ip ipbase = eqBase.Ip.Single();
                        this.IP = new IpVista
                        {
                            Dns = ipbase.dns,
                            Gateway = ipbase.gateway,
                            IpV4 = ipbase.ip_v4,
                            IpV6 = ipbase.ip_v6,
                            Mascara = ipbase.mascara,
                            Wins = ipbase.wins,
                            IpGestion = ipbase.ipGestion,
                            IpLoopback = ipbase.ipLoopback,
                            IpWan = ipbase.ipWan,
                            Mac = ipbase.mac
                        };
                    }
                    this.Marca = new MarcaVista
                    {
                        idMarca = eqBase.id_marca.Value,
                        Nombre = eqBase.Marca.descripcion
                    };
                    this.Modelo = eqBase.modelo;
                    this.NroSerie = eqBase.UnidadConf.nro_serie;
                    this.Oblea = eqBase.UnidadConf.nombre;
                    this.Observaciones = eqBase.UnidadConf.observaciones;
                    this.TipoEquipo.id = eqBase.UnidadConf.Tipo_equipo.id_tipo;
                    this.TipoEquipo.Nombre = eqBase.UnidadConf.Tipo_equipo.descripcion;
                    this.Puertos = eqBase.puertos;
                    this.SSID = eqBase.ssid;
                    this.SSIDPassword = eqBase.ssidPass;
                    this.Usuario1 = eqBase.usuario1;
                    this.Password1 = eqBase.pass_us1;
                    this.Usuario2 = eqBase.usuario2;
                    this.Password2 = eqBase.pass_us2;
                    this.Usuario3 = eqBase.usuario3;
                    this.Password3 = eqBase.pass_us3;
                    this.Encriptacion = eqBase.encript;
                    this.Puertos = eqBase.puertos;
                    this.SfpSerie = eqBase.sfpSerie;
                    this.MacSfp = eqBase.macSfp;
                    this.TipoSfp = eqBase.tipoSfp;
                    this.Snmp = eqBase.snmp;
                    this.SnmpVer = eqBase.snmpVer;
                }
            }
            catch (Exception e)
            {

            }        
        }
    }
}