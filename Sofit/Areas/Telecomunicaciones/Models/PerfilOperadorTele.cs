﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Telecomunicaciones.Models
{
    public class PerfilOperadorTele:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.NoAccess;
            string[] res = page.Split('/');
            if (res[0].Equals("Telecomunicaciones"))
            {
                switch (res[1])
                {
                    case "Incidentes": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "NuevoTicket": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Inventario": acceso = Constantes.AccesosPagina.ReadOnly;
                        break;
                    case "Historicos": acceso = Constantes.AccesosPagina.Full;
                        break;
                    case "Deposito": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Tiempos": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Usuarios": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Jurisdicciones": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Tipificaciones": acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                    case "Config": acceso = Constantes.AccesosPagina.Full;
                        break;
                    default: acceso = Constantes.AccesosPagina.NoAccess;
                        break;
                }
            }
            return acceso;
        }
        public override string getHomePage()
        {
            return "Telecomunicaciones/Incidentes";
        }
    }
}