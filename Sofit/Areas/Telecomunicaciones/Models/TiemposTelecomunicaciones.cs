﻿using Sofit.DataAccess;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Telecomunicaciones.Models
{
    public class TiemposTelecomunicaciones
    {
        public int tADatos { get; set; }
        public int tEDatos { get; set; }
        public int tATele { get; set; }
        public int tETele { get; set; }
        public int tAIngresados { get; set; }
        public int tEIngresados { get; set; }
        public int tCriticoIncidente { get; set; }
        public TiemposTelecomunicaciones()
        {
            using (var dbc = new IncidenciasEntities())
            {                
                var ts = from to in dbc.TiemposGral
                         select to;
                if (ts != null)
                {                    
                    tCriticoIncidente = ts.Where(c => c.nombre == "IncidenteCritico").Single().duracion;
                }
            }
            
        }

        internal soporte.Models.Statics.Responses setTiempos(int tAvisoDatos, int tEmergDatos, int tAvisoTele, int tEmergTele,int tAIng, int tEIng)
        {
            Responses result = new Responses();
            
            return result;
        }
    }
    
}