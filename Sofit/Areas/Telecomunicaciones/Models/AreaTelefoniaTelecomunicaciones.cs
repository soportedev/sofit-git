﻿using soporte.Areas.Telecomunicaciones.Controllers;
using Sofit.DataAccess;
using soporte.Models;
using soporte.Models.Factory;
using soporte.Models.Interface;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Telecomunicaciones.Models
{
    public class AreaTelefoniaTelecomunicaciones:IAreaIncidencia
    {
        public AreaTelefoniaTelecomunicaciones(int idArea, string nombre)
        {
            base.IdArea = idArea;
            base.NombreArea = nombre;
        }
        public override soporte.Models.Statics.Responses derivar(int idArea, int idTecnico,int idUsuario)
        {
            Responses result = new Responses();
            IAreaIncidencia areaDestino = AreaIncidenciaFactory.getArea(idArea);
            Incidencia.AreasActuantes.Sort();
            MovAreaIncidencia ultimaArea = Incidencia.AreasActuantes.LastOrDefault();
            if (ultimaArea == null)
            {
                result.Detail = "La Incidencia no tiene movimientos";
            }
            else
            {
                if (ultimaArea.tieneResolucion())
                {
                    result = Incidencia.cambioArea(areaDestino, idTecnico, idUsuario);
                }
                else
                {
                    result.Info = "error";
                    result.Detail = "Falta cargar la resolución del Área: " + ultimaArea.Area.NombreArea;
                }
            }
            return result;          
        }
        public override int getIdTecnicoGenerico()
        {
            int idTecnico = 0;
            
            return idTecnico;
        }
        public override soporte.Models.Statics.Responses solucionar(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public override soporte.Models.Statics.Responses cargarSolucion(string observacion, int tecnico, int turno, int idUsuario, int tResolucionUsuarios, int? idEquipo, DateTime actualizarFecha, string imagen)
        {
            Responses result = new Responses();
            try
            {
                using (var dbc = new IncidenciasEntities())
                {

                    Resoluciones res = new Resoluciones
                    {
                        id_area = this.IdArea,
                        fecha = DateTime.Now,
                        id_uc = idEquipo,
                        id_tecnico = tecnico,
                        id_turno = turno,
                        observaciones = observacion,
                        usuario = idUsuario,
                        id_tipo_resolucion = tResolucionUsuarios,
                        id_incidencia = this.Incidencia.Id,
                        path_imagen = (imagen != string.Empty) ? imagen : null
                    };
                    dbc.Resoluciones.Add(res);
                    dbc.SaveChanges();
                    result.Info = "ok";
                }
            }
            catch (Exception e) { result.Detail = e.Message; }
            return result;
        }

        public void setIncidencia(soporte.Models.Incidencia incidencia)
        {
            throw new NotImplementedException();
        }
    }
}