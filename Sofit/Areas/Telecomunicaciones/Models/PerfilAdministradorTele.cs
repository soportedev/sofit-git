﻿using soporte.Models;
using soporte.Models.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace soporte.Areas.Telecomunicaciones.Models
{
    public class PerfilAdministradorTele:Perfil
    {
        public override soporte.Models.Statics.Constantes.AccesosPagina getAcceso(string page)
        {
            Constantes.AccesosPagina acceso = Constantes.AccesosPagina.ReadOnly;
            string[] res = page.Split('/');
            if (res[0].Equals("Telecomunicaciones")) acceso = Constantes.AccesosPagina.Full;
            else acceso = Constantes.AccesosPagina.NoAccess;
            return acceso;
        }

        public override string getHomePage()
        {
            return "Telecomunicaciones/Incidentes";
        }
    }
}