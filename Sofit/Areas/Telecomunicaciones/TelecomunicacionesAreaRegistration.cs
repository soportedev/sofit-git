﻿using System.Web.Mvc;

namespace soporte.Areas.Telecomunicaciones
{
    public class TelecomunicacionesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Telecomunicaciones";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "lineabasetele",
                url: "Telecomunicaciones/Cmdb",
                defaults: new { controller = "LineaBase", action = "Index" }, namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "cambiotele",
                url: "Telecomunicaciones/Cambios/{startIndex}",
                defaults: new { controller = "Cambios", action = "Index", startIndex=0 }, 
                constraints:new {startIndex= @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                name: "s8tele",
                url: "Telecomunicaciones/Servicio8/{startIndex}",
                defaults: new { controller = "Servicio8", action = "Index", startIndex = 0 },
                constraints: new { startIndex = @"\d+" },
                namespaces: new[] { "soporte.Controllers" }
               );
            context.MapRoute(
                "Telecomunicaciones_default",
                "Telecomunicaciones/{controller}/{action}/{id}",
                new { controller="Telecomunicaciones/Incidentes", action = "Index", id = UrlParameter.Optional },
                new string[] { "soporte.Areas.Telecomunicaciones.Controllers" }
            );
            
        }
    }
}
